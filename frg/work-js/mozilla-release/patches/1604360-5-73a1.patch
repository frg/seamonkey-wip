# HG changeset patch
# User Andrew Halberstadt <ahalberstadt@mozilla.com>
# Date 1576621168 0
# Node ID 6671170fb10a489c6e14a768d5464f642941edb7
# Parent  00281938f05f8bb4d91420bd2c075bf9adbe8f18
Bug 1604360 - [manifestparser] Remove unused 'defaults_only' logic r=egao

This flag is not passed in from anywhere in mozilla-central.

Differential Revision: https://phabricator.services.mozilla.com/D57408

diff --git a/testing/mozbase/manifestparser/manifestparser/ini.py b/testing/mozbase/manifestparser/manifestparser/ini.py
--- a/testing/mozbase/manifestparser/manifestparser/ini.py
+++ b/testing/mozbase/manifestparser/manifestparser/ini.py
@@ -20,24 +20,23 @@ class IniParseError(Exception):
         elif hasattr(fp, 'name'):
             path = fp.name
         else:
             path = getattr(fp, 'path', 'unknown')
         msg = "Error parsing manifest file '{}', line {}: {}".format(path, linenum, msg)
         super(IniParseError, self).__init__(msg)
 
 
-def read_ini(fp, variables=None, default='DEFAULT', defaults_only=False,
-             comments=None, separators=None, strict=True, handle_defaults=True):
+def read_ini(fp, variables=None, default='DEFAULT', comments=None,
+             separators=None, strict=True, handle_defaults=True):
     """
     read an .ini file and return a list of [(section, values)]
     - fp : file pointer or path to read
     - variables : default set of variables
     - default : name of the section for the default section
-    - defaults_only : if True, return the default section only
     - comments : characters that if they start a line denote a comment
     - separators : strings that denote key, value separation in order
     - strict : whether to be strict about parsing
     - handle_defaults : whether to incorporate defaults into each section
     """
 
     # variables
     variables = variables or {}
@@ -131,20 +130,16 @@ def read_ini(fp, variables=None, default
                         assert key not in current_section
 
                 current_section[key] = value
                 break
         else:
             # something bad happened!
             raise IniParseError(fp, linenum, "Unexpected line '{}'".format(stripped))
 
-    # return the default section only if requested
-    if defaults_only:
-        return [(default, variables)]
-
     global_vars = variables if handle_defaults else {}
     sections = [(i, combine_fields(global_vars, j)) for i, j in sections]
     return sections
 
 
 def combine_fields(global_vars, local_vars):
     """
     Combine the given manifest entries according to the semantics of specific fields.
diff --git a/testing/mozbase/manifestparser/manifestparser/manifestparser.py b/testing/mozbase/manifestparser/manifestparser/manifestparser.py
--- a/testing/mozbase/manifestparser/manifestparser/manifestparser.py
+++ b/testing/mozbase/manifestparser/manifestparser/manifestparser.py
@@ -70,17 +70,16 @@ class ManifestParser(object):
                        documented at
                        http://firefox-source-docs.mozilla.org/python/mozpack.html#module-mozpack.files
         :param handle_defaults: If not set, do not propagate manifest defaults to individual
                                 test objects. Callers are expected to manage per-manifest
                                 defaults themselves via the manifest_defaults member
                                 variable in this case.
         """
         self._defaults = defaults or {}
-        self._ancestor_defaults = {}
         self.tests = []
         self.manifest_defaults = {}
         self.source_files = set()
         self.strict = strict
         self.rootdir = rootdir
         self.relativeRoot = None
         self.finder = finder
         self._handle_defaults = handle_defaults
@@ -89,26 +88,23 @@ class ManifestParser(object):
 
     def path_exists(self, path):
         if self.finder:
             return self.finder.get(path) is not None
         return os.path.exists(path)
 
     # methods for reading manifests
 
-    def _read(self, root, filename, defaults, defaults_only=False, parentmanifest=None):
+    def _read(self, root, filename, defaults, parentmanifest=None):
         """
         Internal recursive method for reading and parsing manifests.
         Stores all found tests in self.tests
         :param root: The base path
         :param filename: File object or string path for the base manifest file
         :param defaults: Options that apply to all items
-        :param defaults_only: If True will only gather options, not include
-                              tests. Used for upstream parent includes
-                              (default False)
         :param parentmanifest: Filename of the parent manifest (default None)
         """
         def read_file(type):
             include_file = section.split(type, 1)[-1]
             include_file = normalize_path(include_file)
             if not os.path.isabs(include_file):
                 include_file = os.path.join(here, include_file)
             if not self.path_exists(include_file):
@@ -150,35 +146,28 @@ class ManifestParser(object):
 
         # read the configuration
         sections = read_ini(fp=fp, variables=defaults, strict=self.strict,
                             handle_defaults=self._handle_defaults)
         self.manifest_defaults[filename] = defaults
 
         # get the tests
         for section, data in sections:
-            # In case of defaults only, no section has to be processed.
-            if defaults_only:
-                continue
-
             # a file to include
             # TODO: keep track of included file structure:
             # self.manifests = {'manifest.ini': 'relative/path.ini'}
             if section.startswith('include:'):
                 include_file = read_file('include:')
                 if include_file:
                     include_defaults = data.copy()
                     self._read(root, include_file, include_defaults, parentmanifest=filename)
                 continue
 
             # otherwise an item
-            # apply ancestor defaults, while maintaining current file priority
-            data = dict(self._ancestor_defaults.items() + data.items())
-
-            test = data
+            test = data.copy()
             test['name'] = section
 
             # Will be None if the manifest being read is a file-like object.
             test['manifest'] = filename
 
             # determine the path
             path = test.get('path', section)
             _relpath = path
@@ -218,23 +207,16 @@ class ManifestParser(object):
                 # indicate that in the test object for the sake of identifying
                 # a test, particularly in the case a test file is included by
                 # multiple manifests.
                 test['ancestor-manifest'] = parentmanifest
 
             # append the item
             self.tests.append(test)
 
-        # if no parent: section was found for defaults-only, only read the
-        # defaults section of the manifest without interpreting variables
-        if defaults_only:
-            sections = read_ini(fp=fp, variables=defaults, defaults_only=True,
-                                strict=self.strict)
-            (section, self._ancestor_defaults) = sections[0]
-
     def read(self, *filenames, **defaults):
         """
         read and add manifests from file paths or file-like objects
 
         filenames -- file paths or file-like objects to read as manifests
         defaults -- default variables
         """
 
