# HG changeset patch
# User Jim Blandy <jimb@mozilla.com>
# Date 1520063786 28800
#      Fri Mar 02 23:56:26 2018 -0800
# Node ID bc9778de0b084afaaca48cd5d27d84fc8d259348
# Parent  5934e66f2c778eace5a8cd80ed734c3596119af4
Bug 1438121: Part 13: Make LiveSavedFrameCache::Entry's members const. r=fitzgen

MozReview-Commit-ID: GbJhE9nylaa

diff --git a/js/src/vm/SavedStacks.cpp b/js/src/vm/SavedStacks.cpp
--- a/js/src/vm/SavedStacks.cpp
+++ b/js/src/vm/SavedStacks.cpp
@@ -75,17 +75,17 @@ LiveSavedFrameCache::trace(JSTracer* trc
     for (auto* entry = frames->begin(); entry < frames->end(); entry++) {
         TraceEdge(trc,
                   &entry->savedFrame,
                   "LiveSavedFrameCache::frames SavedFrame");
     }
 }
 
 bool
-LiveSavedFrameCache::insert(JSContext* cx, FramePtr& framePtr, jsbytecode* pc,
+LiveSavedFrameCache::insert(JSContext* cx, FramePtr& framePtr, const jsbytecode* pc,
                             HandleSavedFrame savedFrame)
 {
     MOZ_ASSERT(initialized());
 
     if (!frames->emplaceBack(framePtr, pc, savedFrame)) {
         ReportOutOfMemory(cx);
         return false;
     }
@@ -106,17 +106,17 @@ LiveSavedFrameCache::find(JSContext* cx,
     MOZ_ASSERT(initialized());
     MOZ_ASSERT(!frameIter.done());
     MOZ_ASSERT(frameIter.hasCachedSavedFrame());
 
     Maybe<FramePtr> maybeFramePtr = getFramePtr(frameIter);
     MOZ_ASSERT(maybeFramePtr.isSome());
 
     FramePtr framePtr(*maybeFramePtr);
-    jsbytecode* pc = frameIter.pc();
+    const jsbytecode* pc = frameIter.pc();
     size_t numberStillValid = 0;
 
     frame.set(nullptr);
     for (auto* p = frames->begin(); p < frames->end(); p++) {
         numberStillValid++;
         if (framePtr == p->framePtr && pc == p->pc) {
             frame.set(p->savedFrame);
             break;
diff --git a/js/src/vm/Stack.h b/js/src/vm/Stack.h
--- a/js/src/vm/Stack.h
+++ b/js/src/vm/Stack.h
@@ -1170,21 +1170,21 @@ struct DefaultHasher<AbstractFramePtr> {
 class LiveSavedFrameCache
 {
   public:
     using FramePtr = mozilla::Variant<AbstractFramePtr, jit::CommonFrameLayout*>;
 
   private:
     struct Entry
     {
-        FramePtr             framePtr;
-        jsbytecode*          pc;
+        const FramePtr       framePtr;
+        const jsbytecode*    pc;
         HeapPtr<SavedFrame*> savedFrame;
 
-        Entry(FramePtr& framePtr, jsbytecode* pc, SavedFrame* savedFrame)
+        Entry(const FramePtr& framePtr, const jsbytecode* pc, SavedFrame* savedFrame)
           : framePtr(framePtr)
           , pc(pc)
           , savedFrame(savedFrame)
         { }
     };
 
     using EntryVector = Vector<Entry, 0, SystemAllocPolicy>;
     EntryVector* frames;
@@ -1218,17 +1218,18 @@ class LiveSavedFrameCache
         }
         return true;
     }
 
     static mozilla::Maybe<FramePtr> getFramePtr(const FrameIter& iter);
     void trace(JSTracer* trc);
 
     void find(JSContext* cx, FrameIter& frameIter, MutableHandleSavedFrame frame) const;
-    bool insert(JSContext* cx, FramePtr& framePtr, jsbytecode* pc, HandleSavedFrame savedFrame);
+    bool insert(JSContext* cx, FramePtr& framePtr, const jsbytecode* pc,
+                HandleSavedFrame savedFrame);
 };
 
 static_assert(sizeof(LiveSavedFrameCache) == sizeof(uintptr_t),
               "Every js::Activation has a LiveSavedFrameCache, so we need to be pretty careful "
               "about avoiding bloat. If you're adding members to LiveSavedFrameCache, maybe you "
               "should consider figuring out a way to make js::Activation have a "
               "LiveSavedFrameCache* instead of a Rooted<LiveSavedFrameCache>.");
 
