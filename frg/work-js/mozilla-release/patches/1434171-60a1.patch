# HG changeset patch
# User Makoto Kato <m_kato@ga2.so-net.ne.jp>
# Date 1517287372 -32400
# Node ID 4c575964fb57258533157a0e568385fcc2ecaa2b
# Parent  0c5b658d2c80d1fe01ed95fd4632013f25d9459f
Bug 1434171 - Clean up CheckSelectionStateForAnonymousButtons. r=masayuki

Since CheckSelectionStateForAnonymousButtons is called from selection listener,
We should reduce some QI for this.

MozReview-Commit-ID: 17hhupmnnV5

diff --git a/editor/composer/nsComposerCommands.cpp b/editor/composer/nsComposerCommands.cpp
--- a/editor/composer/nsComposerCommands.cpp
+++ b/editor/composer/nsComposerCommands.cpp
@@ -1036,45 +1036,32 @@ nsAbsolutePositioningCommand::GetCurrent
 
   bool isEnabled = aHTMLEditor->AbsolutePositioningEnabled();
   if (!isEnabled) {
     aParams->SetBooleanValue(STATE_MIXED,false);
     aParams->SetCStringValue(STATE_ATTRIBUTE, "");
     return NS_OK;
   }
 
-  nsCOMPtr<nsINode> container;
-  nsresult rv =
-    aHTMLEditor->GetAbsolutelyPositionedSelectionContainer(
-                   getter_AddRefs(container));
-  NS_ENSURE_SUCCESS(rv, rv);
-
-  nsAutoString outStateString;
-  if (container) {
-    outStateString.AssignLiteral("absolute");
-  }
-
-  aParams->SetBooleanValue(STATE_MIXED,false);
-  aParams->SetCStringValue(STATE_ATTRIBUTE, NS_ConvertUTF16toUTF8(outStateString).get());
+  RefPtr<Element> container =
+    aHTMLEditor->GetAbsolutelyPositionedSelectionContainer();
+  aParams->SetBooleanValue(STATE_MIXED,  false);
+  aParams->SetCStringValue(STATE_ATTRIBUTE, container ? "absolute" : "");
   return NS_OK;
 }
 
 nsresult
 nsAbsolutePositioningCommand::ToggleState(mozilla::HTMLEditor* aHTMLEditor)
 {
   if (NS_WARN_IF(!aHTMLEditor)) {
     return NS_ERROR_INVALID_ARG;
   }
 
-  nsCOMPtr<nsINode> container;
-  nsresult rv =
-    aHTMLEditor->GetAbsolutelyPositionedSelectionContainer(
-                   getter_AddRefs(container));
-  NS_ENSURE_SUCCESS(rv, rv);
-
+  RefPtr<Element> container =
+    aHTMLEditor->GetAbsolutelyPositionedSelectionContainer();
   return aHTMLEditor->AbsolutePositionSelection(!container);
 }
 
 
 NS_IMETHODIMP
 nsDecreaseZIndexCommand::IsCommandEnabled(const char * aCommandName,
                                           nsISupports *refCon,
                                           bool *outCmdEnabled)
diff --git a/editor/libeditor/HTMLAbsPositionEditor.cpp b/editor/libeditor/HTMLAbsPositionEditor.cpp
--- a/editor/libeditor/HTMLAbsPositionEditor.cpp
+++ b/editor/libeditor/HTMLAbsPositionEditor.cpp
@@ -76,54 +76,47 @@ HTMLEditor::AbsolutePositionSelection(bo
   }
 
   return rules->DidDoAction(selection, &ruleInfo, rv);
 }
 
 NS_IMETHODIMP
 HTMLEditor::GetAbsolutelyPositionedSelectionContainer(nsIDOMElement** _retval)
 {
-  nsCOMPtr<nsINode> container;
-  nsresult rv =
-    GetAbsolutelyPositionedSelectionContainer(getter_AddRefs(container));
-  if (NS_WARN_IF(NS_FAILED(rv))) {
+  RefPtr<Element> container =
+    GetAbsolutelyPositionedSelectionContainer();
+  if (NS_WARN_IF(!container)) {
     *_retval = nullptr;
-    return rv;
+    return NS_ERROR_FAILURE;
   }
 
   nsCOMPtr<nsIDOMElement> domContainer = do_QueryInterface(container);
   domContainer.forget(_retval);
   return NS_OK;
 }
 
-nsresult
-HTMLEditor::GetAbsolutelyPositionedSelectionContainer(nsINode** aContainer)
+already_AddRefed<Element>
+HTMLEditor::GetAbsolutelyPositionedSelectionContainer()
 {
-  MOZ_ASSERT(aContainer);
-
   nsAutoString positionStr;
-  nsCOMPtr<nsINode> node = GetSelectionContainer();
-  nsCOMPtr<nsINode> resultNode;
+  RefPtr<Element> element = GetSelectionContainer();
 
-  while (!resultNode && node && !node->IsHTMLElement(nsGkAtoms::html)) {
+  while (element && !element->IsHTMLElement(nsGkAtoms::html)) {
     nsresult rv =
-      mCSSEditUtils->GetComputedProperty(*node, *nsGkAtoms::position,
+      mCSSEditUtils->GetComputedProperty(*element, *nsGkAtoms::position,
                                          positionStr);
     if (NS_WARN_IF(NS_FAILED(rv))) {
-      *aContainer = nullptr;
-      return rv;
+      return nullptr;
     }
-    if (positionStr.EqualsLiteral("absolute"))
-      resultNode = node;
-    else {
-      node = node->GetParentNode();
+    if (positionStr.EqualsLiteral("absolute")) {
+      return element.forget();
     }
+    element = element->GetParentElement();
   }
-  resultNode.forget(aContainer);
-  return NS_OK;
+  return nullptr;
 }
 
 NS_IMETHODIMP
 HTMLEditor::GetSelectionContainerAbsolutelyPositioned(
               bool* aIsSelectionContainerAbsolutelyPositioned)
 {
   *aIsSelectionContainerAbsolutelyPositioned = (mAbsolutelyPositionedObject != nullptr);
   return NS_OK;
@@ -322,38 +315,44 @@ HTMLEditor::HideGrabber()
   return NS_OK;
 }
 
 NS_IMETHODIMP
 HTMLEditor::ShowGrabberOnElement(nsIDOMElement* aElement)
 {
   nsCOMPtr<Element> element = do_QueryInterface(aElement);
   NS_ENSURE_ARG_POINTER(element);
+  return ShowGrabberOnElement(*element);
+}
 
-  if (NS_WARN_IF(!IsDescendantOfEditorRoot(element))) {
+nsresult
+HTMLEditor::ShowGrabberOnElement(Element& aElement)
+{
+  if (NS_WARN_IF(!IsDescendantOfEditorRoot(&aElement))) {
     return NS_ERROR_UNEXPECTED;
   }
 
   if (mGrabber) {
     NS_ERROR("call HideGrabber first");
     return NS_ERROR_UNEXPECTED;
   }
 
   nsAutoString classValue;
-  nsresult rv = CheckPositionedElementBGandFG(aElement, classValue);
+  nsresult rv =
+    GetTemporaryStyleForFocusedPositionedElement(aElement, classValue);
   NS_ENSURE_SUCCESS(rv, rv);
 
-  rv = element->SetAttr(kNameSpaceID_None, nsGkAtoms::_moz_abspos,
+  rv = aElement.SetAttr(kNameSpaceID_None, nsGkAtoms::_moz_abspos,
                         classValue, true);
   NS_ENSURE_SUCCESS(rv, rv);
 
   // first, let's keep track of that element...
-  mAbsolutelyPositionedObject = element;
+  mAbsolutelyPositionedObject = &aElement;
 
-  nsIContent* parentContent = element->GetParent();
+  nsIContent* parentContent = aElement.GetParent();
   if (NS_WARN_IF(!parentContent)) {
     return NS_ERROR_FAILURE;
   }
 
   mGrabber = CreateGrabber(*parentContent);
   NS_ENSURE_TRUE(mGrabber, NS_ERROR_FAILURE);
 
   // and set its position
@@ -635,77 +634,80 @@ HTMLEditor::GetPositionedElement(nsIDOME
 {
   nsCOMPtr<nsIDOMElement> ret =
     static_cast<nsIDOMElement*>(GetAsDOMNode(GetPositionedElement()));
   ret.forget(aReturn);
   return NS_OK;
 }
 
 nsresult
-HTMLEditor::CheckPositionedElementBGandFG(nsIDOMElement* aElement,
-                                          nsAString& aReturn)
+HTMLEditor::GetTemporaryStyleForFocusedPositionedElement(Element& aElement,
+                                                         nsAString& aReturn)
 {
   // we are going to outline the positioned element and bring it to the
   // front to overlap any other element intersecting with it. But
   // first, let's see what's the background and foreground colors of the
   // positioned element.
   // if background-image computed value is 'none,
   //   If the background color is 'auto' and R G B values of the foreground are
   //       each above #d0, use a black background
   //   If the background color is 'auto' and at least one of R G B values of
   //       the foreground is below #d0, use a white background
   // Otherwise don't change background/foreground
-  nsCOMPtr<Element> element = do_QueryInterface(aElement);
-  NS_ENSURE_STATE(element || !aElement);
-
   aReturn.Truncate();
 
   nsAutoString bgImageStr;
   nsresult rv =
-    mCSSEditUtils->GetComputedProperty(*element, *nsGkAtoms::background_image,
+    mCSSEditUtils->GetComputedProperty(aElement, *nsGkAtoms::background_image,
                                        bgImageStr);
   NS_ENSURE_SUCCESS(rv, rv);
-  if (bgImageStr.EqualsLiteral("none")) {
-    nsAutoString bgColorStr;
-    rv =
-      mCSSEditUtils->GetComputedProperty(*element, *nsGkAtoms::backgroundColor,
-                                         bgColorStr);
-    NS_ENSURE_SUCCESS(rv, rv);
-    if (bgColorStr.EqualsLiteral("transparent")) {
-      RefPtr<nsComputedDOMStyle> cssDecl =
-        mCSSEditUtils->GetComputedStyle(element);
-      NS_ENSURE_STATE(cssDecl);
-
-      // from these declarations, get the one we want and that one only
-      ErrorResult error;
-      RefPtr<dom::CSSValue> cssVal = cssDecl->GetPropertyCSSValue(NS_LITERAL_STRING("color"), error);
-      NS_ENSURE_TRUE(!error.Failed(), error.StealNSResult());
-
-      nsROCSSPrimitiveValue* val = cssVal->AsPrimitiveValue();
-      NS_ENSURE_TRUE(val, NS_ERROR_FAILURE);
+  if (!bgImageStr.EqualsLiteral("none")) {
+    return NS_OK;
+  }
 
-      if (nsIDOMCSSPrimitiveValue::CSS_RGBCOLOR == val->PrimitiveType()) {
-        nsDOMCSSRGBColor* rgbVal = val->GetRGBColorValue(error);
-        NS_ENSURE_TRUE(!error.Failed(), error.StealNSResult());
-        float r = rgbVal->Red()->
-          GetFloatValue(nsIDOMCSSPrimitiveValue::CSS_NUMBER, error);
-        NS_ENSURE_TRUE(!error.Failed(), error.StealNSResult());
-        float g = rgbVal->Green()->
-          GetFloatValue(nsIDOMCSSPrimitiveValue::CSS_NUMBER, error);
-        NS_ENSURE_TRUE(!error.Failed(), error.StealNSResult());
-        float b = rgbVal->Blue()->
-          GetFloatValue(nsIDOMCSSPrimitiveValue::CSS_NUMBER, error);
-        NS_ENSURE_TRUE(!error.Failed(), error.StealNSResult());
-        if (r >= BLACK_BG_RGB_TRIGGER &&
-            g >= BLACK_BG_RGB_TRIGGER &&
-            b >= BLACK_BG_RGB_TRIGGER)
-          aReturn.AssignLiteral("black");
-        else
-          aReturn.AssignLiteral("white");
-        return NS_OK;
-      }
-    }
+  nsAutoString bgColorStr;
+  rv =
+    mCSSEditUtils->GetComputedProperty(aElement, *nsGkAtoms::backgroundColor,
+                                       bgColorStr);
+  NS_ENSURE_SUCCESS(rv, rv);
+  if (!bgColorStr.EqualsLiteral("transparent")) {
+    return NS_OK;
   }
 
+  RefPtr<nsComputedDOMStyle> cssDecl =
+    mCSSEditUtils->GetComputedStyle(&aElement);
+  NS_ENSURE_STATE(cssDecl);
+
+  // from these declarations, get the one we want and that one only
+  ErrorResult error;
+  RefPtr<dom::CSSValue> cssVal =
+    cssDecl->GetPropertyCSSValue(NS_LITERAL_STRING("color"), error);
+  NS_ENSURE_TRUE(!error.Failed(), error.StealNSResult());
+
+  nsROCSSPrimitiveValue* val = cssVal->AsPrimitiveValue();
+  NS_ENSURE_TRUE(val, NS_ERROR_FAILURE);
+
+  if (nsIDOMCSSPrimitiveValue::CSS_RGBCOLOR != val->PrimitiveType()) {
+    return NS_OK;
+  }
+
+  nsDOMCSSRGBColor* rgbVal = val->GetRGBColorValue(error);
+  NS_ENSURE_TRUE(!error.Failed(), error.StealNSResult());
+  float r = rgbVal->Red()->
+    GetFloatValue(nsIDOMCSSPrimitiveValue::CSS_NUMBER, error);
+  NS_ENSURE_TRUE(!error.Failed(), error.StealNSResult());
+  float g = rgbVal->Green()->
+    GetFloatValue(nsIDOMCSSPrimitiveValue::CSS_NUMBER, error);
+  NS_ENSURE_TRUE(!error.Failed(), error.StealNSResult());
+  float b = rgbVal->Blue()->
+    GetFloatValue(nsIDOMCSSPrimitiveValue::CSS_NUMBER, error);
+  NS_ENSURE_TRUE(!error.Failed(), error.StealNSResult());
+  if (r >= BLACK_BG_RGB_TRIGGER &&
+      g >= BLACK_BG_RGB_TRIGGER &&
+      b >= BLACK_BG_RGB_TRIGGER) {
+    aReturn.AssignLiteral("black");
+  } else {
+    aReturn.AssignLiteral("white");
+  }
   return NS_OK;
 }
 
 } // namespace mozilla
diff --git a/editor/libeditor/HTMLAnonymousNodeEditor.cpp b/editor/libeditor/HTMLAnonymousNodeEditor.cpp
--- a/editor/libeditor/HTMLAnonymousNodeEditor.cpp
+++ b/editor/libeditor/HTMLAnonymousNodeEditor.cpp
@@ -333,39 +333,33 @@ HTMLEditor::CheckSelectionStateForAnonym
       mIsAbsolutelyPositioningEnabled ||
       mIsInlineTableEditingEnabled, NS_OK);
 
   // Don't change selection state if we're moving.
   if (mIsMoving) {
     return NS_OK;
   }
 
-  nsCOMPtr<nsIDOMElement> focusElement;
   // let's get the containing element of the selection
-  nsresult rv = GetSelectionContainer(getter_AddRefs(focusElement));
+  RefPtr<Element> focusElement = GetSelectionContainer();
   NS_ENSURE_TRUE(focusElement, NS_OK);
-  NS_ENSURE_SUCCESS(rv, rv);
 
   // If we're not in a document, don't try to add resizers
-  nsCOMPtr<dom::Element> focusElementNode = do_QueryInterface(focusElement);
-  NS_ENSURE_STATE(focusElementNode);
-  if (!focusElementNode->IsInUncomposedDoc()) {
+  if (!focusElement->IsInUncomposedDoc()) {
     return NS_OK;
   }
 
   // what's its tag?
-  nsIAtom* focusTagAtom = focusElementNode->NodeInfo()->NameAtom();
+  nsIAtom* focusTagAtom = focusElement->NodeInfo()->NameAtom();
 
-  nsCOMPtr<nsIDOMElement> absPosElement;
+  RefPtr<Element> absPosElement;
   if (mIsAbsolutelyPositioningEnabled) {
     // Absolute Positioning support is enabled, is the selection contained
     // in an absolutely positioned element ?
-    rv =
-      GetAbsolutelyPositionedSelectionContainer(getter_AddRefs(absPosElement));
-    NS_ENSURE_SUCCESS(rv, rv);
+    absPosElement = GetAbsolutelyPositionedSelectionContainer();
   }
 
   RefPtr<Element> cellElement;
   if (mIsObjectResizingEnabled || mIsInlineTableEditingEnabled) {
     // Resizing or Inline Table Editing is enabled, we need to check if the
     // selection is contained in a table cell
     cellElement = GetElementOrParentByTagName(NS_LITERAL_STRING("td"), nullptr);
   }
@@ -373,17 +367,17 @@ HTMLEditor::CheckSelectionStateForAnonym
   if (mIsObjectResizingEnabled && cellElement) {
     // we are here because Resizing is enabled AND selection is contained in
     // a cell
 
     // get the enclosing table
     if (nsGkAtoms::img != focusTagAtom) {
       // the element container of the selection is not an image, so we'll show
       // the resizers around the table
-      focusElement = do_QueryInterface(GetEnclosingTable(cellElement));
+      focusElement = GetEnclosingTable(cellElement);
       focusTagAtom = nsGkAtoms::table;
     }
   }
 
   // we allow resizers only around images, tables, and absolutely positioned
   // elements. If we don't have image/table, let's look at the latter case.
   if (nsGkAtoms::img != focusTagAtom && nsGkAtoms::table != focusTagAtom) {
     focusElement = absPosElement;
@@ -393,67 +387,66 @@ HTMLEditor::CheckSelectionStateForAnonym
   //                cellElement   contains the element for InlineTableEditing
   //                absPosElement contains the element for Positioning
 
   // Note: All the Hide/Show methods below may change attributes on real
   // content which means a DOMAttrModified handler may cause arbitrary
   // side effects while this code runs (bug 420439).
 
   if (mIsAbsolutelyPositioningEnabled && mAbsolutelyPositionedObject &&
-      absPosElement != GetAsDOMNode(mAbsolutelyPositionedObject)) {
-    rv = HideGrabber();
+      absPosElement != mAbsolutelyPositionedObject) {
+    nsresult rv = HideGrabber();
     NS_ENSURE_SUCCESS(rv, rv);
     NS_ASSERTION(!mAbsolutelyPositionedObject, "HideGrabber failed");
   }
 
   if (mIsObjectResizingEnabled && mResizedObject &&
-      GetAsDOMNode(mResizedObject) != focusElement) {
-    rv = HideResizers();
+      mResizedObject != focusElement) {
+    nsresult rv = HideResizers();
     NS_ENSURE_SUCCESS(rv, rv);
     NS_ASSERTION(!mResizedObject, "HideResizers failed");
   }
 
   if (mIsInlineTableEditingEnabled && mInlineEditedCell &&
       mInlineEditedCell != cellElement) {
-    rv = HideInlineTableEditingUI();
+    nsresult rv = HideInlineTableEditingUI();
     NS_ENSURE_SUCCESS(rv, rv);
     NS_ASSERTION(!mInlineEditedCell, "HideInlineTableEditingUI failed");
   }
 
   // now, let's display all contextual UI for good
   nsIContent* hostContent = GetActiveEditingHost();
-  nsCOMPtr<nsIDOMNode> hostNode = do_QueryInterface(hostContent);
 
   if (mIsObjectResizingEnabled && focusElement &&
-      IsModifiableNode(focusElement) && focusElement != hostNode) {
+      IsModifiableNode(focusElement) && focusElement != hostContent) {
     if (nsGkAtoms::img == focusTagAtom) {
       mResizedObjectIsAnImage = true;
     }
     if (mResizedObject) {
       nsresult rv = RefreshResizers();
       if (NS_WARN_IF(NS_FAILED(rv))) {
         return rv;
       }
     } else {
-      nsresult rv = ShowResizers(focusElement);
+      nsresult rv = ShowResizers(*focusElement);
       if (NS_WARN_IF(NS_FAILED(rv))) {
         return rv;
       }
     }
   }
 
   if (mIsAbsolutelyPositioningEnabled && absPosElement &&
-      IsModifiableNode(absPosElement) && absPosElement != hostNode) {
+      IsModifiableNode(absPosElement) && absPosElement != hostContent) {
     if (mAbsolutelyPositionedObject) {
       nsresult rv = RefreshGrabber();
       if (NS_WARN_IF(NS_FAILED(rv))) {
         return rv;
       }
     } else {
-      nsresult rv = ShowGrabberOnElement(absPosElement);
+      nsresult rv = ShowGrabberOnElement(*absPosElement);
       if (NS_WARN_IF(NS_FAILED(rv))) {
         return rv;
       }
     }
   }
 
   if (mIsInlineTableEditingEnabled && cellElement &&
       IsModifiableNode(cellElement) && cellElement != hostContent) {
diff --git a/editor/libeditor/HTMLEditor.h b/editor/libeditor/HTMLEditor.h
--- a/editor/libeditor/HTMLEditor.h
+++ b/editor/libeditor/HTMLEditor.h
@@ -232,17 +232,17 @@ public:
   static bool NodeIsBlockStatic(const nsINode* aElement);
   static nsresult NodeIsBlockStatic(nsIDOMNode *aNode, bool *aIsBlock);
 
   // non-virtual methods of interface methods
   bool AbsolutePositioningEnabled() const
   {
     return mIsAbsolutelyPositioningEnabled;
   }
-  nsresult GetAbsolutelyPositionedSelectionContainer(nsINode** aContainer);
+  already_AddRefed<Element> GetAbsolutelyPositionedSelectionContainer();
   Element* GetPositionedElement() const
   {
     return mAbsolutelyPositionedObject;
   }
   nsresult GetElementZIndex(Element* aElement, int32_t* aZindex);
 
   nsresult SetInlineProperty(nsIAtom* aProperty,
                              nsIAtom* aAttribute,
@@ -1099,16 +1099,22 @@ protected:
   int32_t mWidthIncrementFactor;
   int32_t mHeightIncrementFactor;
 
   int8_t  mInfoXIncrement;
   int8_t  mInfoYIncrement;
 
   nsresult SetAllResizersPosition();
 
+  /**
+   * Shows active resizers around an element's frame
+   * @param aResizedElement [IN] a DOM Element
+   */
+  nsresult ShowResizers(Element& aResizedElement);
+
   ManualNACPtr CreateResizer(int16_t aLocation, nsIContent& aParentContent);
   void SetAnonymousElementPosition(int32_t aX, int32_t aY,
                                    Element* aResizer);
 
   ManualNACPtr CreateShadow(nsIContent& aParentContent,
                             Element& aOriginalObject);
   nsresult SetShadowPosition(Element* aShadow, Element* aOriginalObject,
                              int32_t aOriginalObjectX,
@@ -1142,25 +1148,34 @@ protected:
   int32_t mPositionedObjectBorderTop;
 
   nsCOMPtr<Element> mAbsolutelyPositionedObject;
   ManualNACPtr mGrabber;
   ManualNACPtr mPositioningShadow;
 
   int32_t mGridSize;
 
+  /**
+   * shows a grabber attached to an arbitrary element. The grabber is an image
+   * positioned on the left hand side of the top border of the element. Draggin
+   * and dropping it allows to change the element's absolute position in the
+   * document. See chrome://editor/content/images/grabber.gif
+   * @param aElement [IN] the element
+   */
+  nsresult ShowGrabberOnElement(Element& aElement);
+
   ManualNACPtr CreateGrabber(nsIContent& aParentContent);
   nsresult StartMoving(nsIDOMElement* aHandle);
   nsresult SetFinalPosition(int32_t aX, int32_t aY);
   void AddPositioningOffset(int32_t& aX, int32_t& aY);
   void SnapToGrid(int32_t& newX, int32_t& newY);
   nsresult GrabberClicked();
   nsresult EndMoving();
-  nsresult CheckPositionedElementBGandFG(nsIDOMElement* aElement,
-                                         nsAString& aReturn);
+  nsresult GetTemporaryStyleForFocusedPositionedElement(Element& aElement,
+                                                        nsAString& aReturn);
 
   // inline table editing
   RefPtr<Element> mInlineEditedCell;
 
   ManualNACPtr mAddColumnBeforeButton;
   ManualNACPtr mRemoveColumnButton;
   ManualNACPtr mAddColumnAfterButton;
 
diff --git a/editor/libeditor/HTMLEditorObjectResizer.cpp b/editor/libeditor/HTMLEditorObjectResizer.cpp
--- a/editor/libeditor/HTMLEditorObjectResizer.cpp
+++ b/editor/libeditor/HTMLEditorObjectResizer.cpp
@@ -249,17 +249,23 @@ HTMLEditor::ShowResizers(nsIDOMElement* 
 {
   if (NS_WARN_IF(!aResizedElement)) {
    return NS_ERROR_NULL_POINTER;
   }
   nsCOMPtr<Element> element = do_QueryInterface(aResizedElement);
   if (NS_WARN_IF(!element)) {
     return NS_ERROR_FAILURE;
   }
-  nsresult rv = ShowResizersInner(*element);
+  return ShowResizers(*element);
+}
+
+nsresult
+HTMLEditor::ShowResizers(Element& aResizedElement)
+{
+  nsresult rv = ShowResizersInner(aResizedElement);
   if (NS_FAILED(rv)) {
     HideResizers();
   }
   return rv;
 }
 
 nsresult
 HTMLEditor::ShowResizersInner(Element& aResizedElement)
