# HG changeset patch
# User Andrew Halberstadt <ahalberstadt@mozilla.com>
# Date 1539359864 0
#      Fri Oct 12 15:57:44 2018 +0000
# Node ID 2064477895c3d93ec55cab9dede17ba01a434b21
# Parent  b7e586708ecccea803d5c4b77e3f9d7bbcb912f6
Bug 1494069 - [lint] Explicitly list out objdirs rather than depend on 'obj*' in the global exclude, r=rwood

When using globs in exclude directorives, FileFinder will return every *file*
that gets matched. This is can be thousands of files in the case of an objdir.
While we now collapse these files down to highest possible directories, this
collapse operation can still take a noticeable amount of time (0.6s). This
simply scans topsrcdir for files that start with 'obj' to avoid the glob.

This also moves the '_activate_virtualenv' call to the top of the function
because in CI, this will cause an objdir to be created (to store the
virtualenv). If this happens *after* calculating the global excludes, we won't
catch it since it doesn't exist yet. This will result in the objdir's
virtualenv being linted and erroneous failures.

Depends on D7739

Differential Revision: https://phabricator.services.mozilla.com/D7740

diff --git a/tools/lint/mach_commands.py b/tools/lint/mach_commands.py
--- a/tools/lint/mach_commands.py
+++ b/tools/lint/mach_commands.py
@@ -15,37 +15,47 @@ from mozbuild.base import (
 from mach.decorators import (
     CommandArgument,
     CommandProvider,
     Command,
 )
 
 
 here = os.path.abspath(os.path.dirname(__file__))
+GLOBAL_EXCLUDES = [
+    'tools/lint/test/files',
+]
 
 
 def setup_argument_parser():
     from mozlint import cli
     return cli.MozlintParser()
 
 
+def get_global_excludes(topsrcdir):
+    excludes = GLOBAL_EXCLUDES[:]
+    excludes.extend([name for name in os.listdir(topsrcdir)
+                     if name.startswith('obj') and os.path.isdir(name)])
+    return excludes
+
+
 @CommandProvider
 class MachCommands(MachCommandBase):
 
     @Command(
         'lint', category='devenv',
         description='Run linters.',
         parser=setup_argument_parser)
     def lint(self, *runargs, **lintargs):
         """Run linters."""
         self._activate_virtualenv()
+        from mozlint import cli
 
-        from mozlint import cli
         lintargs.setdefault('root', self.topsrcdir)
-        lintargs['exclude'] = ['obj*', 'tools/lint/test/files']
+        lintargs['exclude'] = get_global_excludes(lintargs['root'])
         cli.SEARCH_PATHS.append(here)
         return cli.run(*runargs, **lintargs)
 
     @Command('eslint', category='devenv',
              description='Run eslint or help configure eslint for optimal development.')
     @CommandArgument('paths', default=None, nargs='*',
                      help="Paths to file or directories to lint, like "
                           "'browser/' Defaults to the "
