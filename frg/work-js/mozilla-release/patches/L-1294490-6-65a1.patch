# HG changeset patch
# User Andrew Osmond <aosmond@mozilla.com>
# Date 1541009777 14400
# Node ID 25aa97e7d1062b21a84f3acb950859de09f40533
# Parent  18e06f96279987a7de2182f01150a13744ce5c45
Bug 1294490 - Part 6. Implement WebP gtests. r=tnikkel

Differential Revision: https://phabricator.services.mozilla.com/D8119

diff --git a/image/test/gtest/Common.cpp b/image/test/gtest/Common.cpp
--- a/image/test/gtest/Common.cpp
+++ b/image/test/gtest/Common.cpp
@@ -33,17 +33,22 @@ AutoInitializeImageLib::AutoInitializeIm
   if (MOZ_LIKELY(sImageLibInitialized)) {
     return;
   }
 
   EXPECT_TRUE(NS_IsMainThread());
   sImageLibInitialized = true;
 
   // Force sRGB to be consistent with reftests.
-  Preferences::SetBool("gfx.color_management.force_srgb", true);
+  nsresult rv = Preferences::SetBool("gfx.color_management.force_srgb", true);
+  EXPECT_TRUE(rv == NS_OK);
+
+  // Ensure WebP is enabled to run decoder tests.
+  rv = Preferences::SetBool("image.webp.enabled", true);
+  EXPECT_TRUE(rv == NS_OK);
 
   // Ensure that ImageLib services are initialized.
   nsCOMPtr<imgITools> imgTools = do_CreateInstance("@mozilla.org/image/tools;1");
   EXPECT_TRUE(imgTools != nullptr);
 
   // Ensure gfxPlatform is initialized.
   gfxPlatform::GetPlatform();
 
@@ -544,28 +549,44 @@ ImageTestCase GreenICOTestCase()
 }
 
 ImageTestCase GreenIconTestCase()
 {
   return ImageTestCase("green.icon", "image/icon", IntSize(100, 100),
                        TEST_CASE_IS_TRANSPARENT);
 }
 
+ImageTestCase GreenWebPTestCase()
+{
+  return ImageTestCase("green.webp", "image/webp", IntSize(100, 100));
+}
+
+ImageTestCase GreenWebPIccSrgbTestCase()
+{
+  return ImageTestCase("green.icc_srgb.webp", "image/webp", IntSize(100, 100));
+}
+
 ImageTestCase GreenFirstFrameAnimatedGIFTestCase()
 {
   return ImageTestCase("first-frame-green.gif", "image/gif", IntSize(100, 100),
                        TEST_CASE_IS_ANIMATED);
 }
 
 ImageTestCase GreenFirstFrameAnimatedPNGTestCase()
 {
   return ImageTestCase("first-frame-green.png", "image/png", IntSize(100, 100),
                        TEST_CASE_IS_TRANSPARENT | TEST_CASE_IS_ANIMATED);
 }
 
+ImageTestCase GreenFirstFrameAnimatedWebPTestCase()
+{
+  return ImageTestCase("first-frame-green.webp", "image/webp", IntSize(100, 100),
+                       TEST_CASE_IS_ANIMATED);
+}
+
 ImageTestCase CorruptTestCase()
 {
   return ImageTestCase("corrupt.jpg", "image/jpeg", IntSize(100, 100),
                        TEST_CASE_HAS_ERROR);
 }
 
 ImageTestCase CorruptBMPWithTruncatedHeader()
 {
@@ -692,16 +713,22 @@ ImageTestCase DownscaledICOTestCase()
 }
 
 ImageTestCase DownscaledIconTestCase()
 {
   return ImageTestCase("downscaled.icon", "image/icon", IntSize(100, 100),
                        IntSize(20, 20), TEST_CASE_IS_TRANSPARENT);
 }
 
+ImageTestCase DownscaledWebPTestCase()
+{
+  return ImageTestCase("downscaled.webp", "image/webp", IntSize(100, 100),
+                       IntSize(20, 20));
+}
+
 ImageTestCase DownscaledTransparentICOWithANDMaskTestCase()
 {
   // This test case is an ICO with AND mask transparency. We want to ensure that
   // we can downscale it without crashing or triggering ASAN failures, but its
   // content isn't simple to verify, so for now we don't check the output.
   return ImageTestCase("transparent-ico-with-and-mask.ico", "image/x-icon",
                        IntSize(32, 32), IntSize(20, 20),
                        TEST_CASE_IS_TRANSPARENT | TEST_CASE_IGNORE_OUTPUT);
diff --git a/image/test/gtest/Common.h b/image/test/gtest/Common.h
--- a/image/test/gtest/Common.h
+++ b/image/test/gtest/Common.h
@@ -375,19 +375,23 @@ void CheckPalettedWritePixels(Decoder* a
 ///////////////////////////////////////////////////////////////////////////////
 
 ImageTestCase GreenPNGTestCase();
 ImageTestCase GreenGIFTestCase();
 ImageTestCase GreenJPGTestCase();
 ImageTestCase GreenBMPTestCase();
 ImageTestCase GreenICOTestCase();
 ImageTestCase GreenIconTestCase();
+ImageTestCase GreenWebPTestCase();
+
+ImageTestCase GreenWebPIccSrgbTestCase();
 
 ImageTestCase GreenFirstFrameAnimatedGIFTestCase();
 ImageTestCase GreenFirstFrameAnimatedPNGTestCase();
+ImageTestCase GreenFirstFrameAnimatedWebPTestCase();
 
 ImageTestCase CorruptTestCase();
 ImageTestCase CorruptBMPWithTruncatedHeader();
 ImageTestCase CorruptICOWithBadBMPWidthTestCase();
 ImageTestCase CorruptICOWithBadBMPHeightTestCase();
 ImageTestCase CorruptICOWithBadBppTestCase();
 
 ImageTestCase TransparentPNGTestCase();
@@ -402,16 +406,17 @@ ImageTestCase RLE4BMPTestCase();
 ImageTestCase RLE8BMPTestCase();
 
 ImageTestCase DownscaledPNGTestCase();
 ImageTestCase DownscaledGIFTestCase();
 ImageTestCase DownscaledJPGTestCase();
 ImageTestCase DownscaledBMPTestCase();
 ImageTestCase DownscaledICOTestCase();
 ImageTestCase DownscaledIconTestCase();
+ImageTestCase DownscaledWebPTestCase();
 ImageTestCase DownscaledTransparentICOWithANDMaskTestCase();
 
 ImageTestCase TruncatedSmallGIFTestCase();
 
 ImageTestCase LargeICOWithBMPTestCase();
 ImageTestCase LargeICOWithPNGTestCase();
 ImageTestCase GreenMultipleSizesICOTestCase();
 
diff --git a/image/test/gtest/TestDecodeToSurface.cpp b/image/test/gtest/TestDecodeToSurface.cpp
--- a/image/test/gtest/TestDecodeToSurface.cpp
+++ b/image/test/gtest/TestDecodeToSurface.cpp
@@ -120,16 +120,17 @@ protected:
 };
 
 TEST_F(ImageDecodeToSurface, PNG) { RunDecodeToSurface(GreenPNGTestCase()); }
 TEST_F(ImageDecodeToSurface, GIF) { RunDecodeToSurface(GreenGIFTestCase()); }
 TEST_F(ImageDecodeToSurface, JPG) { RunDecodeToSurface(GreenJPGTestCase()); }
 TEST_F(ImageDecodeToSurface, BMP) { RunDecodeToSurface(GreenBMPTestCase()); }
 TEST_F(ImageDecodeToSurface, ICO) { RunDecodeToSurface(GreenICOTestCase()); }
 TEST_F(ImageDecodeToSurface, Icon) { RunDecodeToSurface(GreenIconTestCase()); }
+TEST_F(ImageDecodeToSurface, WebP) { RunDecodeToSurface(GreenWebPTestCase()); }
 
 TEST_F(ImageDecodeToSurface, AnimatedGIF)
 {
   RunDecodeToSurface(GreenFirstFrameAnimatedGIFTestCase());
 }
 
 TEST_F(ImageDecodeToSurface, AnimatedPNG)
 {
diff --git a/image/test/gtest/TestDecoders.cpp b/image/test/gtest/TestDecoders.cpp
--- a/image/test/gtest/TestDecoders.cpp
+++ b/image/test/gtest/TestDecoders.cpp
@@ -199,16 +199,257 @@ CheckDownscaleDuringDecode(const ImageTe
     // small amount of fuzz; this is just the nature of Lanczos downscaling.
     EXPECT_TRUE(RowsAreSolidColor(surface, 0, 4, BGRAColor::Green(), /* aFuzz = */ 47));
     EXPECT_TRUE(RowsAreSolidColor(surface, 6, 3, BGRAColor::Red(), /* aFuzz = */ 27));
     EXPECT_TRUE(RowsAreSolidColor(surface, 11, 3, BGRAColor::Green(), /* aFuzz = */ 47));
     EXPECT_TRUE(RowsAreSolidColor(surface, 16, 4, BGRAColor::Red(), /* aFuzz = */ 27));
   });
 }
 
+static void
+CheckDecoderFrameFirst(const ImageTestCase& aTestCase)
+{
+  // Verify that we can decode this test case and retrieve the first frame using
+  // imgIContainer::FRAME_FIRST. This ensures that we correctly trigger a
+  // single-frame decode rather than an animated decode when
+  // imgIContainer::FRAME_FIRST is requested.
+
+  // Create an image.
+  RefPtr<Image> image =
+    ImageFactory::CreateAnonymousImage(nsDependentCString(aTestCase.mMimeType));
+  ASSERT_TRUE(!image->HasError());
+
+  nsCOMPtr<nsIInputStream> inputStream = LoadFile(aTestCase.mPath);
+  ASSERT_TRUE(inputStream);
+
+  // Figure out how much data we have.
+  uint64_t length;
+  nsresult rv = inputStream->Available(&length);
+  ASSERT_TRUE(NS_SUCCEEDED(rv));
+
+  // Write the data into the image.
+  rv = image->OnImageDataAvailable(nullptr, nullptr, inputStream, 0,
+                                   static_cast<uint32_t>(length));
+  ASSERT_TRUE(NS_SUCCEEDED(rv));
+
+  // Let the image know we've sent all the data.
+  rv = image->OnImageDataComplete(nullptr, nullptr, NS_OK, true);
+  ASSERT_TRUE(NS_SUCCEEDED(rv));
+
+  RefPtr<ProgressTracker> tracker = image->GetProgressTracker();
+  tracker->SyncNotifyProgress(FLAG_LOAD_COMPLETE);
+
+  // Lock the image so its surfaces don't disappear during the test.
+  image->LockImage();
+
+  auto unlock = mozilla::MakeScopeExit([&] {
+    image->UnlockImage();
+  });
+
+  // Use GetFrame() to force a sync decode of the image, specifying FRAME_FIRST
+  // to ensure that we don't get an animated decode.
+  RefPtr<SourceSurface> surface =
+    image->GetFrame(imgIContainer::FRAME_FIRST,
+                    imgIContainer::FLAG_SYNC_DECODE);
+
+  // Ensure that the image's metadata meets our expectations.
+  IntSize imageSize(0, 0);
+  rv = image->GetWidth(&imageSize.width);
+  EXPECT_TRUE(NS_SUCCEEDED(rv));
+  rv = image->GetHeight(&imageSize.height);
+  EXPECT_TRUE(NS_SUCCEEDED(rv));
+
+  EXPECT_EQ(aTestCase.mSize.width, imageSize.width);
+  EXPECT_EQ(aTestCase.mSize.height, imageSize.height);
+
+  Progress imageProgress = tracker->GetProgress();
+
+  EXPECT_TRUE(bool(imageProgress & FLAG_HAS_TRANSPARENCY) == false);
+  EXPECT_TRUE(bool(imageProgress & FLAG_IS_ANIMATED) == true);
+
+  // Ensure that we decoded the static version of the image.
+  {
+    LookupResult result =
+      SurfaceCache::Lookup(ImageKey(image.get()),
+                           RasterSurfaceKey(imageSize,
+                                            DefaultSurfaceFlags(),
+                                            PlaybackType::eStatic),
+                           /* aMarkUsed = */ false);
+    ASSERT_EQ(MatchType::EXACT, result.Type());
+    EXPECT_TRUE(bool(result.Surface()));
+  }
+
+  // Ensure that we didn't decode the animated version of the image.
+  {
+    LookupResult result =
+      SurfaceCache::Lookup(ImageKey(image.get()),
+                           RasterSurfaceKey(imageSize,
+                                            DefaultSurfaceFlags(),
+                                            PlaybackType::eAnimated),
+                           /* aMarkUsed = */ false);
+    ASSERT_EQ(MatchType::NOT_FOUND, result.Type());
+  }
+
+  // Use GetFrame() to force a sync decode of the image, this time specifying
+  // FRAME_CURRENT to ensure that we get an animated decode.
+  RefPtr<SourceSurface> animatedSurface =
+    image->GetFrame(imgIContainer::FRAME_CURRENT,
+                    imgIContainer::FLAG_SYNC_DECODE);
+
+  // Ensure that we decoded both frames of the animated version of the image.
+  {
+    LookupResult result =
+      SurfaceCache::Lookup(ImageKey(image.get()),
+                           RasterSurfaceKey(imageSize,
+                                            DefaultSurfaceFlags(),
+                                            PlaybackType::eAnimated),
+                           /* aMarkUsed = */ true);
+    ASSERT_EQ(MatchType::EXACT, result.Type());
+
+    EXPECT_TRUE(NS_SUCCEEDED(result.Surface().Seek(0)));
+    EXPECT_TRUE(bool(result.Surface()));
+
+    RefPtr<imgFrame> partialFrame = result.Surface().GetFrame(1);
+    EXPECT_TRUE(bool(partialFrame));
+  }
+
+  // Ensure that the static version is still around.
+  {
+    LookupResult result =
+      SurfaceCache::Lookup(ImageKey(image.get()),
+                           RasterSurfaceKey(imageSize,
+                                            DefaultSurfaceFlags(),
+                                            PlaybackType::eStatic),
+                           /* aMarkUsed = */ true);
+    ASSERT_EQ(MatchType::EXACT, result.Type());
+    EXPECT_TRUE(bool(result.Surface()));
+  }
+}
+
+static void
+CheckDecoderFrameCurrent(const ImageTestCase& aTestCase)
+{
+  // Verify that we can decode this test case and retrieve the entire sequence
+  // of frames using imgIContainer::FRAME_CURRENT. This ensures that we
+  // correctly trigger an animated decode rather than a single-frame decode when
+  // imgIContainer::FRAME_CURRENT is requested.
+
+  // Create an image.
+  RefPtr<Image> image =
+    ImageFactory::CreateAnonymousImage(nsDependentCString(aTestCase.mMimeType));
+  ASSERT_TRUE(!image->HasError());
+
+  nsCOMPtr<nsIInputStream> inputStream = LoadFile(aTestCase.mPath);
+  ASSERT_TRUE(inputStream);
+
+  // Figure out how much data we have.
+  uint64_t length;
+  nsresult rv = inputStream->Available(&length);
+  ASSERT_TRUE(NS_SUCCEEDED(rv));
+
+  // Write the data into the image.
+  rv = image->OnImageDataAvailable(nullptr, nullptr, inputStream, 0,
+                                   static_cast<uint32_t>(length));
+  ASSERT_TRUE(NS_SUCCEEDED(rv));
+
+  // Let the image know we've sent all the data.
+  rv = image->OnImageDataComplete(nullptr, nullptr, NS_OK, true);
+  ASSERT_TRUE(NS_SUCCEEDED(rv));
+
+  RefPtr<ProgressTracker> tracker = image->GetProgressTracker();
+  tracker->SyncNotifyProgress(FLAG_LOAD_COMPLETE);
+
+  // Lock the image so its surfaces don't disappear during the test.
+  image->LockImage();
+
+  // Use GetFrame() to force a sync decode of the image, specifying
+  // FRAME_CURRENT to ensure we get an animated decode.
+  RefPtr<SourceSurface> surface =
+    image->GetFrame(imgIContainer::FRAME_CURRENT,
+                    imgIContainer::FLAG_SYNC_DECODE);
+
+  // Ensure that the image's metadata meets our expectations.
+  IntSize imageSize(0, 0);
+  rv = image->GetWidth(&imageSize.width);
+  EXPECT_TRUE(NS_SUCCEEDED(rv));
+  rv = image->GetHeight(&imageSize.height);
+  EXPECT_TRUE(NS_SUCCEEDED(rv));
+
+  EXPECT_EQ(aTestCase.mSize.width, imageSize.width);
+  EXPECT_EQ(aTestCase.mSize.height, imageSize.height);
+
+  Progress imageProgress = tracker->GetProgress();
+
+  EXPECT_TRUE(bool(imageProgress & FLAG_HAS_TRANSPARENCY) == false);
+  EXPECT_TRUE(bool(imageProgress & FLAG_IS_ANIMATED) == true);
+
+  // Ensure that we decoded both frames of the animated version of the image.
+  {
+    LookupResult result =
+      SurfaceCache::Lookup(ImageKey(image.get()),
+                           RasterSurfaceKey(imageSize,
+                                            DefaultSurfaceFlags(),
+                                            PlaybackType::eAnimated),
+                           /* aMarkUsed = */ true);
+    ASSERT_EQ(MatchType::EXACT, result.Type());
+
+    EXPECT_TRUE(NS_SUCCEEDED(result.Surface().Seek(0)));
+    EXPECT_TRUE(bool(result.Surface()));
+
+    RefPtr<imgFrame> partialFrame = result.Surface().GetFrame(1);
+    EXPECT_TRUE(bool(partialFrame));
+  }
+
+  // Ensure that we didn't decode the static version of the image.
+  {
+    LookupResult result =
+      SurfaceCache::Lookup(ImageKey(image.get()),
+                           RasterSurfaceKey(imageSize,
+                                            DefaultSurfaceFlags(),
+                                            PlaybackType::eStatic),
+                           /* aMarkUsed = */ false);
+    ASSERT_EQ(MatchType::NOT_FOUND, result.Type());
+  }
+
+  // Use GetFrame() to force a sync decode of the image, this time specifying
+  // FRAME_FIRST to ensure that we get a single-frame decode.
+  RefPtr<SourceSurface> animatedSurface =
+    image->GetFrame(imgIContainer::FRAME_FIRST,
+                    imgIContainer::FLAG_SYNC_DECODE);
+
+  // Ensure that we decoded the static version of the image.
+  {
+    LookupResult result =
+      SurfaceCache::Lookup(ImageKey(image.get()),
+                           RasterSurfaceKey(imageSize,
+                                            DefaultSurfaceFlags(),
+                                            PlaybackType::eStatic),
+                           /* aMarkUsed = */ true);
+    ASSERT_EQ(MatchType::EXACT, result.Type());
+    EXPECT_TRUE(bool(result.Surface()));
+  }
+
+  // Ensure that both frames of the animated version are still around.
+  {
+    LookupResult result =
+      SurfaceCache::Lookup(ImageKey(image.get()),
+                           RasterSurfaceKey(imageSize,
+                                            DefaultSurfaceFlags(),
+                                            PlaybackType::eAnimated),
+                           /* aMarkUsed = */ true);
+    ASSERT_EQ(MatchType::EXACT, result.Type());
+
+    EXPECT_TRUE(NS_SUCCEEDED(result.Surface().Seek(0)));
+    EXPECT_TRUE(bool(result.Surface()));
+
+    RefPtr<imgFrame> partialFrame = result.Surface().GetFrame(1);
+    EXPECT_TRUE(bool(partialFrame));
+  }
+}
+
 class ImageDecoders : public ::testing::Test
 {
 protected:
   AutoInitializeImageLib mInit;
 };
 
 TEST_F(ImageDecoders, PNGSingleChunk)
 {
@@ -300,16 +541,36 @@ TEST_F(ImageDecoders, IconMultiChunk)
   CheckDecoderMultiChunk(GreenIconTestCase());
 }
 
 TEST_F(ImageDecoders, IconDownscaleDuringDecode)
 {
   CheckDownscaleDuringDecode(DownscaledIconTestCase());
 }
 
+TEST_F(ImageDecoders, WebPSingleChunk)
+{
+  CheckDecoderSingleChunk(GreenWebPTestCase());
+}
+
+TEST_F(ImageDecoders, WebPMultiChunk)
+{
+  CheckDecoderMultiChunk(GreenWebPTestCase());
+}
+
+TEST_F(ImageDecoders, WebPDownscaleDuringDecode)
+{
+  CheckDownscaleDuringDecode(DownscaledWebPTestCase());
+}
+
+TEST_F(ImageDecoders, WebPIccSrgbMultiChunk)
+{
+  CheckDecoderMultiChunk(GreenWebPIccSrgbTestCase());
+}
+
 TEST_F(ImageDecoders, AnimatedGIFSingleChunk)
 {
   CheckDecoderSingleChunk(GreenFirstFrameAnimatedGIFTestCase());
 }
 
 TEST_F(ImageDecoders, AnimatedGIFMultiChunk)
 {
   CheckDecoderMultiChunk(GreenFirstFrameAnimatedGIFTestCase());
@@ -320,16 +581,31 @@ TEST_F(ImageDecoders, AnimatedPNGSingleC
   CheckDecoderSingleChunk(GreenFirstFrameAnimatedPNGTestCase());
 }
 
 TEST_F(ImageDecoders, AnimatedPNGMultiChunk)
 {
   CheckDecoderMultiChunk(GreenFirstFrameAnimatedPNGTestCase());
 }
 
+TEST_F(ImageDecoders, AnimatedWebPSingleChunk)
+{
+  CheckDecoderSingleChunk(GreenFirstFrameAnimatedWebPTestCase());
+}
+
+TEST_F(ImageDecoders, AnimatedWebPMultiChunk)
+{
+  CheckDecoderMultiChunk(GreenFirstFrameAnimatedWebPTestCase());
+}
+
+TEST_F(ImageDecoders, AnimatedWebPWithBlendedFrames)
+{
+  CheckAnimationDecoderSingleChunk(GreenFirstFrameAnimatedWebPTestCase());
+}
+
 TEST_F(ImageDecoders, CorruptSingleChunk)
 {
   CheckDecoderSingleChunk(CorruptTestCase());
 }
 
 TEST_F(ImageDecoders, CorruptMultiChunk)
 {
   CheckDecoderMultiChunk(CorruptTestCase());
@@ -367,243 +643,22 @@ TEST_F(ImageDecoders, CorruptICOWithBadB
 
 TEST_F(ImageDecoders, CorruptICOWithBadBppSingleChunk)
 {
   CheckDecoderSingleChunk(CorruptICOWithBadBppTestCase());
 }
 
 TEST_F(ImageDecoders, AnimatedGIFWithFRAME_FIRST)
 {
-  ImageTestCase testCase = GreenFirstFrameAnimatedGIFTestCase();
-
-  // Verify that we can decode this test case and retrieve the first frame using
-  // imgIContainer::FRAME_FIRST. This ensures that we correctly trigger a
-  // single-frame decode rather than an animated decode when
-  // imgIContainer::FRAME_FIRST is requested.
-
-  // Create an image.
-  RefPtr<Image> image =
-    ImageFactory::CreateAnonymousImage(nsDependentCString(testCase.mMimeType));
-  ASSERT_TRUE(!image->HasError());
-
-  nsCOMPtr<nsIInputStream> inputStream = LoadFile(testCase.mPath);
-  ASSERT_TRUE(inputStream);
-
-  // Figure out how much data we have.
-  uint64_t length;
-  nsresult rv = inputStream->Available(&length);
-  ASSERT_TRUE(NS_SUCCEEDED(rv));
-
-  // Write the data into the image.
-  rv = image->OnImageDataAvailable(nullptr, nullptr, inputStream, 0,
-                                   static_cast<uint32_t>(length));
-  ASSERT_TRUE(NS_SUCCEEDED(rv));
-
-  // Let the image know we've sent all the data.
-  rv = image->OnImageDataComplete(nullptr, nullptr, NS_OK, true);
-  ASSERT_TRUE(NS_SUCCEEDED(rv));
-
-  RefPtr<ProgressTracker> tracker = image->GetProgressTracker();
-  tracker->SyncNotifyProgress(FLAG_LOAD_COMPLETE);
-
-  // Lock the image so its surfaces don't disappear during the test.
-  image->LockImage();
-
-  // Use GetFrame() to force a sync decode of the image, specifying FRAME_FIRST
-  // to ensure that we don't get an animated decode.
-  RefPtr<SourceSurface> surface =
-    image->GetFrame(imgIContainer::FRAME_FIRST,
-                    imgIContainer::FLAG_SYNC_DECODE);
-
-  // Ensure that the image's metadata meets our expectations.
-  IntSize imageSize(0, 0);
-  rv = image->GetWidth(&imageSize.width);
-  EXPECT_TRUE(NS_SUCCEEDED(rv));
-  rv = image->GetHeight(&imageSize.height);
-  EXPECT_TRUE(NS_SUCCEEDED(rv));
-
-  EXPECT_EQ(testCase.mSize.width, imageSize.width);
-  EXPECT_EQ(testCase.mSize.height, imageSize.height);
-
-  Progress imageProgress = tracker->GetProgress();
-
-  EXPECT_TRUE(bool(imageProgress & FLAG_HAS_TRANSPARENCY) == false);
-  EXPECT_TRUE(bool(imageProgress & FLAG_IS_ANIMATED) == true);
-
-  // Ensure that we decoded the static version of the image.
-  {
-    LookupResult result =
-      SurfaceCache::Lookup(ImageKey(image.get()),
-                           RasterSurfaceKey(imageSize,
-                                            DefaultSurfaceFlags(),
-                                            PlaybackType::eStatic));
-    ASSERT_EQ(MatchType::EXACT, result.Type());
-    EXPECT_TRUE(bool(result.Surface()));
-  }
-
-  // Ensure that we didn't decode the animated version of the image.
-  {
-    LookupResult result =
-      SurfaceCache::Lookup(ImageKey(image.get()),
-                           RasterSurfaceKey(imageSize,
-                                            DefaultSurfaceFlags(),
-                                            PlaybackType::eAnimated));
-    ASSERT_EQ(MatchType::NOT_FOUND, result.Type());
-  }
-
-  // Use GetFrame() to force a sync decode of the image, this time specifying
-  // FRAME_CURRENT to ensure that we get an animated decode.
-  RefPtr<SourceSurface> animatedSurface =
-    image->GetFrame(imgIContainer::FRAME_CURRENT,
-                    imgIContainer::FLAG_SYNC_DECODE);
-
-  // Ensure that we decoded both frames of the animated version of the image.
-  {
-    LookupResult result =
-      SurfaceCache::Lookup(ImageKey(image.get()),
-                           RasterSurfaceKey(imageSize,
-                                            DefaultSurfaceFlags(),
-                                            PlaybackType::eAnimated));
-    ASSERT_EQ(MatchType::EXACT, result.Type());
-
-    EXPECT_TRUE(NS_SUCCEEDED(result.Surface().Seek(0)));
-    EXPECT_TRUE(bool(result.Surface()));
-
-    EXPECT_TRUE(NS_SUCCEEDED(result.Surface().Seek(1)));
-    EXPECT_TRUE(bool(result.Surface()));
-  }
-
-  // Ensure that the static version is still around.
-  {
-    LookupResult result =
-      SurfaceCache::Lookup(ImageKey(image.get()),
-                           RasterSurfaceKey(imageSize,
-                                            DefaultSurfaceFlags(),
-                                            PlaybackType::eStatic));
-    ASSERT_EQ(MatchType::EXACT, result.Type());
-    EXPECT_TRUE(bool(result.Surface()));
-  }
+  CheckDecoderFrameFirst(GreenFirstFrameAnimatedGIFTestCase());
 }
 
 TEST_F(ImageDecoders, AnimatedGIFWithFRAME_CURRENT)
 {
-  ImageTestCase testCase = GreenFirstFrameAnimatedGIFTestCase();
-
-  // Verify that we can decode this test case and retrieve the entire sequence
-  // of frames using imgIContainer::FRAME_CURRENT. This ensures that we
-  // correctly trigger an animated decode rather than a single-frame decode when
-  // imgIContainer::FRAME_CURRENT is requested.
-
-  // Create an image.
-  RefPtr<Image> image =
-    ImageFactory::CreateAnonymousImage(nsDependentCString(testCase.mMimeType));
-  ASSERT_TRUE(!image->HasError());
-
-  nsCOMPtr<nsIInputStream> inputStream = LoadFile(testCase.mPath);
-  ASSERT_TRUE(inputStream);
-
-  // Figure out how much data we have.
-  uint64_t length;
-  nsresult rv = inputStream->Available(&length);
-  ASSERT_TRUE(NS_SUCCEEDED(rv));
-
-  // Write the data into the image.
-  rv = image->OnImageDataAvailable(nullptr, nullptr, inputStream, 0,
-                                   static_cast<uint32_t>(length));
-  ASSERT_TRUE(NS_SUCCEEDED(rv));
-
-  // Let the image know we've sent all the data.
-  rv = image->OnImageDataComplete(nullptr, nullptr, NS_OK, true);
-  ASSERT_TRUE(NS_SUCCEEDED(rv));
-
-  RefPtr<ProgressTracker> tracker = image->GetProgressTracker();
-  tracker->SyncNotifyProgress(FLAG_LOAD_COMPLETE);
-
-  // Lock the image so its surfaces don't disappear during the test.
-  image->LockImage();
-
-  // Use GetFrame() to force a sync decode of the image, specifying
-  // FRAME_CURRENT to ensure we get an animated decode.
-  RefPtr<SourceSurface> surface =
-    image->GetFrame(imgIContainer::FRAME_CURRENT,
-                    imgIContainer::FLAG_SYNC_DECODE);
-
-  // Ensure that the image's metadata meets our expectations.
-  IntSize imageSize(0, 0);
-  rv = image->GetWidth(&imageSize.width);
-  EXPECT_TRUE(NS_SUCCEEDED(rv));
-  rv = image->GetHeight(&imageSize.height);
-  EXPECT_TRUE(NS_SUCCEEDED(rv));
-
-  EXPECT_EQ(testCase.mSize.width, imageSize.width);
-  EXPECT_EQ(testCase.mSize.height, imageSize.height);
-
-  Progress imageProgress = tracker->GetProgress();
-
-  EXPECT_TRUE(bool(imageProgress & FLAG_HAS_TRANSPARENCY) == false);
-  EXPECT_TRUE(bool(imageProgress & FLAG_IS_ANIMATED) == true);
-
-  // Ensure that we decoded both frames of the animated version of the image.
-  {
-    LookupResult result =
-      SurfaceCache::Lookup(ImageKey(image.get()),
-                           RasterSurfaceKey(imageSize,
-                                            DefaultSurfaceFlags(),
-                                            PlaybackType::eAnimated));
-    ASSERT_EQ(MatchType::EXACT, result.Type());
-
-    EXPECT_TRUE(NS_SUCCEEDED(result.Surface().Seek(0)));
-    EXPECT_TRUE(bool(result.Surface()));
-
-    EXPECT_TRUE(NS_SUCCEEDED(result.Surface().Seek(1)));
-    EXPECT_TRUE(bool(result.Surface()));
-  }
-
-  // Ensure that we didn't decode the static version of the image.
-  {
-    LookupResult result =
-      SurfaceCache::Lookup(ImageKey(image.get()),
-                           RasterSurfaceKey(imageSize,
-                                            DefaultSurfaceFlags(),
-                                            PlaybackType::eStatic));
-    ASSERT_EQ(MatchType::NOT_FOUND, result.Type());
-  }
-
-  // Use GetFrame() to force a sync decode of the image, this time specifying
-  // FRAME_FIRST to ensure that we get a single-frame decode.
-  RefPtr<SourceSurface> animatedSurface =
-    image->GetFrame(imgIContainer::FRAME_FIRST,
-                    imgIContainer::FLAG_SYNC_DECODE);
-
-  // Ensure that we decoded the static version of the image.
-  {
-    LookupResult result =
-      SurfaceCache::Lookup(ImageKey(image.get()),
-                           RasterSurfaceKey(imageSize,
-                                            DefaultSurfaceFlags(),
-                                            PlaybackType::eStatic));
-    ASSERT_EQ(MatchType::EXACT, result.Type());
-    EXPECT_TRUE(bool(result.Surface()));
-  }
-
-  // Ensure that both frames of the animated version are still around.
-  {
-    LookupResult result =
-      SurfaceCache::Lookup(ImageKey(image.get()),
-                           RasterSurfaceKey(imageSize,
-                                            DefaultSurfaceFlags(),
-                                            PlaybackType::eAnimated));
-    ASSERT_EQ(MatchType::EXACT, result.Type());
-
-    EXPECT_TRUE(NS_SUCCEEDED(result.Surface().Seek(0)));
-    EXPECT_TRUE(bool(result.Surface()));
-
-    EXPECT_TRUE(NS_SUCCEEDED(result.Surface().Seek(1)));
-    EXPECT_TRUE(bool(result.Surface()));
-  }
+  CheckDecoderFrameCurrent(GreenFirstFrameAnimatedGIFTestCase());
 }
 
 TEST_F(ImageDecoders, AnimatedGIFWithExtraImageSubBlocks)
 {
   ImageTestCase testCase = ExtraImageSubBlocksAnimatedGIFTestCase();
 
   // Verify that we can decode this test case and get two frames, even though
   // there are extra image sub blocks between the first and second frame. The
@@ -664,16 +719,26 @@ TEST_F(ImageDecoders, AnimatedGIFWithExt
 
   EXPECT_TRUE(NS_SUCCEEDED(result.Surface().Seek(0)));
   EXPECT_TRUE(bool(result.Surface()));
 
   EXPECT_TRUE(NS_SUCCEEDED(result.Surface().Seek(1)));
   EXPECT_TRUE(bool(result.Surface()));
 }
 
+TEST_F(ImageDecoders, AnimatedWebPWithFRAME_FIRST)
+{
+  CheckDecoderFrameFirst(GreenFirstFrameAnimatedWebPTestCase());
+}
+
+TEST_F(ImageDecoders, AnimatedWebPWithFRAME_CURRENT)
+{
+  CheckDecoderFrameCurrent(GreenFirstFrameAnimatedWebPTestCase());
+}
+
 TEST_F(ImageDecoders, TruncatedSmallGIFSingleChunk)
 {
   CheckDecoderSingleChunk(TruncatedSmallGIFTestCase());
 }
 
 TEST_F(ImageDecoders, LargeICOWithBMPSingleChunk)
 {
   CheckDecoderSingleChunk(LargeICOWithBMPTestCase());
diff --git a/image/test/gtest/TestMetadata.cpp b/image/test/gtest/TestMetadata.cpp
--- a/image/test/gtest/TestMetadata.cpp
+++ b/image/test/gtest/TestMetadata.cpp
@@ -147,16 +147,17 @@ protected:
 TEST_F(ImageDecoderMetadata, PNG) { CheckMetadata(GreenPNGTestCase()); }
 TEST_F(ImageDecoderMetadata, TransparentPNG) { CheckMetadata(TransparentPNGTestCase()); }
 TEST_F(ImageDecoderMetadata, GIF) { CheckMetadata(GreenGIFTestCase()); }
 TEST_F(ImageDecoderMetadata, TransparentGIF) { CheckMetadata(TransparentGIFTestCase()); }
 TEST_F(ImageDecoderMetadata, JPG) { CheckMetadata(GreenJPGTestCase()); }
 TEST_F(ImageDecoderMetadata, BMP) { CheckMetadata(GreenBMPTestCase()); }
 TEST_F(ImageDecoderMetadata, ICO) { CheckMetadata(GreenICOTestCase()); }
 TEST_F(ImageDecoderMetadata, Icon) { CheckMetadata(GreenIconTestCase()); }
+TEST_F(ImageDecoderMetadata, WebP) { CheckMetadata(GreenWebPTestCase()); }
 
 TEST_F(ImageDecoderMetadata, AnimatedGIF)
 {
   CheckMetadata(GreenFirstFrameAnimatedGIFTestCase());
 }
 
 TEST_F(ImageDecoderMetadata, AnimatedPNG)
 {
diff --git a/image/test/gtest/downscaled.webp b/image/test/gtest/downscaled.webp
new file mode 100644
index e69de29bb2d1d6434b8b29ae775ad8c2e48c5391..c2db6d6446b5524579a094b7276d721e7b160087
GIT binary patch
literal 56
zc$^FJbaOLcU|<M$bqWXzu<%g<vh|Y>NHFkk`2Sh{v%F}>mfU9v|E|uH<#+p1&y-QI
L{qBVidAk__@n{v=

diff --git a/image/test/gtest/first-frame-green.webp b/image/test/gtest/first-frame-green.webp
new file mode 100644
index e69de29bb2d1d6434b8b29ae775ad8c2e48c5391..44db5c71c374086aaf00054c19bd269c50acc41c
GIT binary patch
literal 154
zc$^FJbaR`;z`zjh>J$(bU=hIuWHSMAG8j4fdHS*ec?|!75Xkd&(_?@Eh>8@Ta-cpR
zQJ{!^@&O43_6z?z{;@Z-e5l_qaPfbL5?Gf3rY;7ME^&me{r|qM|IetP_~*X9%NP5p
G91H+?{UqlA

diff --git a/image/test/gtest/green.icc_srgb.webp b/image/test/gtest/green.icc_srgb.webp
new file mode 100644
index e69de29bb2d1d6434b8b29ae775ad8c2e48c5391..2a869b447bb78bb79421c0f0e68b8550b2a36b08
GIT binary patch
literal 3092
zc$~GCXH-+!7RS%M>5Ue;l+c@@NeLiQLJLI@={7<VAd~<h0Ynr<L<L3^5os2XQHlc&
zj(`jzB8r7UuqzgjK|vV@N3o&i-QWkE^=7TN-g@i(SI)`r?zMNa&psdSKbYm^MJ59f
z?&%&B7Q~1k0sv^p;w*evOlHt933&vhVd)MdD_I~C279~HA|j(`xW`CH7A%O(Nlyj;
zF6bM+z5Vs+23@*4%d|ae*}K1|I?iyYn8+OZhuZHY$K|CX9YG$_=3<aPN4NoD#h8#_
zCc@nS;K^~{vG6<QCUdy}5H2D4JK`2<i9yoZBylnyVXl;udF*tAvk=ygNlX<XTqBJO
zzviEivWUltN4Otha)y|P)Dfj~*`6tq=940%Ih<6Xl#>v)=fty-98x{Pi@)~2AVNAa
zKx<1YoBwd;|60;J`AYhS0g%lHp#2(0EX?>CLrXUdAR;%W2-JWU&;!PR4y=Jaa00Ht
z6ZnEa5CS5>TIBBIK{616OppV%fC5kic7uJO3LFCUpb@lyHqZgO!8vddTm{4678nI%
zU=q9lv*0b5had<CQ6MU$25CbEkQrnRF`#9TC*%(WLy=H46bA_)F_Z&sg^Hk3s0ylu
z8lj(|)6hBSGBg6+gT|q0=r8CajDpFqBCH7;!WJ+Cc7y%k5O@t72dBZ=a6Y^Xu7K;{
z<8V8C4!#QCg2&(&@Ei)DNGN5L4vLOqpqQvYR1_)}B}8pP6{5;fb*L6p7is`?12u-4
zL481D(Nr`IO-DPSebAw34mu6J30;h?LN}n>(f#Nf=yCKc`U{4F(ZHBt95F0R1SSrX
zi7CL8V~%1@VJ={9VJ0zeu~@7!)(C5l^}$AB6R;bw#n=PbR%|bJ1UrG9!{KmhI5V6x
zE)d7YiEstDO5AbWS=@EpB<?+)gxAJf<Gt`vcmX~SUyg6W_u#MNpW){Tas&f{BO#E$
zC9Efu5b6nCglmLJ!aPx)Xhd`-h7c2oTZk3J7UBis1LA8EiKI(%Agv(rNt;O(q!Xk|
zq;b**vI5zZ%p|WSi^;pl$H=|pd*s&?3dNA(N{OO~D7z?)l=GBFl=m`J8FLwy3|A&s
zrdp<5=DN&FS%R#Atefmw*=*Tz*^{!@WM||Ea)xqDIkwy;xoWx7a(CqB<f-yj@`3Wn
z^2PE^@|WeO6mSZL3Z4pFg?xo03g;Cjs3@u)l}Y7Lw^Hk=7pRkpSVbd6mSTcpk>XE^
zLyEIX3Q9IgVM<v_)k<fS#*|UYhRQ7EB;^w2Hs#yOA5}C}+*EifJ5^d#uB*ILU8L%&
z%2O>=ZB@Oc`caLh=Bbvbwnwc)?V&na-Bf*rdZzk8^$Y4Vixd|*E#fRHT-3Jcz6Pve
zsu8TQLF0(VpvIi0mZrC6s%E9;dCeIuWi3~&1g$cy9<6C@s`fH%zIK`RS?%XEC7K&8
ziB>`Dr_JhU==kV}b!v5nbmn!9bVGIXbX#>F>JjxAda-(CdVPAc`r7&d`Z@Z|`u7b8
z1`LCEgZ%~r2JZ}w48sk#8+I5@8L1kvjIxcIjUE_Nj9rY=jO&bVnqW;BCW$5oOomNi
zQ(Mz`(`wUeX0Vx^8Q<)H*~ntdVu!_o#dVAC(8+W+dIr6jK5njT?r)xF-eLaILf<0F
zVvogu#TQFk%OuNs%TX%@D_^TTt1hd*tWB*s*45TGmr#~?Ey-Qdx#X3NnGMh8pv_%d
zs%?PncH2JN&vy29Lc12bX?uP9X!{!bI}Ale5TlrJ$pPcw?vU%y?eM|T&Qavp=J=-*
z-6_$j$!Th-(Nf;h`lS=jy3TCpTIaE4v}MuD4lf&X(Q#qB)VVxy)pO;#9(8@@X5yCM
zcHHfyyQRC({gnGVrXzDBv)2RV;q6iAG32S>8R}W(`N&JxE6%IgYu4M=dxLkc55~vO
zXOGVvmL`kCYGlp&+WT(w?e`=5t@Nw%d*W~EFZAyUfCGF3_69syZm?Xiydw|<vI6%8
zJ`6GnN(=gR1$ITyis}_lgRO#df(KSoSFT&xwDN6;dq_#hXsB_hIJ7TJHf(iRW7xZJ
zkMPp)u?UNZjS*KP)g$?lol&@`u&AR^bE`a8?OQdu+IDrp>f39K)?}@@yjFc}(%LiY
zWY@9Ros7mrhetO>e_{u+>)3NKteBdZSq_u4pYww2$}Qtg^PG95yl1h_v8Az7an5mj
z<EG<X;>+V__)LBk|5bud!l8tBiGhj75+zCDNv+BF<mlwn0tG>$pg%<`B`akl)hx9j
zbu7&>ZC~1~&`)?Y9ZFx5-YHT<UL=EJWAQfec*e4f>Wuf9VVNhh<g)}>gX>M#?_B>Z
z+bjFW4d@M=4ShK}Ia_kZH@a=C-2`ol+0>V-mz$qEx!HSj!xrL}#4W$&S>~1Iz1bSR
zwJTpUe{=rCHt%iC1u_M~f;-!twjbJo-of87v~$VM%AH>dxrJAXEQ`vEJ{5C{ul!*3
zL&Xo0U9r1{O6*Dw?#AvG?7qFnWluw?d}(&+#NL3t?PavG!m>B}*6q7oZc~0}KXJc!
z|5$~8MQ5dc<?hN)Rq<7~s@<zwYcy(h*1S8wJ#hV?>%o>onum%GeLT!Re7DxSw!O}<
zuKWo0NXC(;^<nh`KQex7JgR=Q=;-HTf@5P1!43V5_Kl5A8cik5aI?61`uOVO!#{ca
z)Y(FBscltmEjj^Ch)+EKIr`_@ZGLTiCmAPOelhr^<`nf*VLQ~mzJ0bMzGJL2vUB9L
z@9DlSr>;}Kn*VyNTc^AFjMABs9%4^^kK}Cj*}2}--kEc8=O+5r^^Km7IDfN$MgPzR
zzYCWxdR;s};5KmflJlkR%Z`^%UtwJ7xN3K`eb9FB)Ni)Gof@(mYQJWGt#jC6xNBtT
zNY8bb>*sEG+_-p?b@S@2<+ny|hu*$>XZ4-2yWG1|_mb|tx}R}>esuE#%!9&*au3TN
zX*{ZXZ1T8u%x>(=xX1Y5la)_KCpZ%?eoz1X(`5cr%G2^^+RqxNtf#uCy{3nsuX;ZD
zBK5`mOu--Wf7HA*e)-Fvu73{BM$AsW626lBRs34*^|3cLZ~Eqf<{rIGemnoJ@V(mm
zh7XJn10TabKAX?{g!@$S+2nKA7r!qLB*_xVcOJx?<;kSkSvlC*(XHuLG!I@ZTbv}K
zNu3=tB`HNLq^(S4b9gj5O=?#DZ$2fa5`d0v0I1&o7DE*D_3NuATMz{PS@3st2azT9
zTHC+HQtAL;%mIMfoz9P2h%tjhXy46&9?Xm733&qKs9}6wCSMSToL|7@i})!58ec&B
z+nE0MlmA`c`uygNMZ7H0LZ2zA>xKNdcoB^y5b=ZpwzMO5(!%Wsr9Gyn2t|Bx@?sj|
zBy9mmk6mKD@KX>O@yM9R63J^r0B~ghd@hkl=4&OA&&VuLqX6`Z5vT93grr(<dQ|~#
UShC{_Zgc)@?pe7JNr*1^7gp-yL;wH)

diff --git a/image/test/gtest/green.webp b/image/test/gtest/green.webp
new file mode 100644
index e69de29bb2d1d6434b8b29ae775ad8c2e48c5391..04b7f003b463948d248755c4d6b19567941f8781
GIT binary patch
literal 42
wc$^FJbaPW;U|<M$bqWXzu<#KDvh|Y>NHDNp_}}r5y`kkp{eFRq|3j1*0P(*L3;+NC

diff --git a/image/test/gtest/moz.build b/image/test/gtest/moz.build
--- a/image/test/gtest/moz.build
+++ b/image/test/gtest/moz.build
@@ -38,29 +38,33 @@ TEST_HARNESS_FILES.gtest += [
     'corrupt-with-bad-ico-bpp.ico',
     'corrupt.jpg',
     'downscaled.bmp',
     'downscaled.gif',
     'downscaled.ico',
     'downscaled.icon',
     'downscaled.jpg',
     'downscaled.png',
+    'downscaled.webp',
     'first-frame-green.gif',
     'first-frame-green.png',
+    'first-frame-green.webp',
     'first-frame-padding.gif',
     'green-1x1-truncated.gif',
     'green-large-bmp.ico',
     'green-large-png.ico',
     'green-multiple-sizes.ico',
     'green.bmp',
     'green.gif',
+    'green.icc_srgb.webp',
     'green.ico',
     'green.icon',
     'green.jpg',
     'green.png',
+    'green.webp',
     'invalid-truncated-metadata.bmp',
     'no-frame-delay.gif',
     'rle4.bmp',
     'rle8.bmp',
     'transparent-ico-with-and-mask.ico',
     'transparent-if-within-ico.bmp',
     'transparent.gif',
     'transparent.png',
