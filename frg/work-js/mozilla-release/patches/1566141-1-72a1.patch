# HG changeset patch
# User yulia <ystartsev@mozilla.com>
# Date 1572542191 0
#      Thu Oct 31 17:16:31 2019 +0000
# Node ID 28aa763e7834023b28c2462a078f1bd91baa7f7b
# Parent  8601cca70bbfdde3f9fbfc7f498a4eb7c885dcd4
Bug 1566141 - Nullish coalesce operator tokens r=jorendorff

Fake it till you make it

Differential Revision: https://phabricator.services.mozilla.com/D50056

diff --git a/js/src/builtin/ReflectParse.cpp b/js/src/builtin/ReflectParse.cpp
--- a/js/src/builtin/ReflectParse.cpp
+++ b/js/src/builtin/ReflectParse.cpp
@@ -2487,17 +2487,18 @@ ASTSerializer::classMethod(ParseNode* pn
 
 bool
 ASTSerializer::leftAssociate(ParseNode* pn, MutableHandleValue dst)
 {
     MOZ_ASSERT(pn->isArity(PN_LIST));
     MOZ_ASSERT(pn->pn_count >= 1);
 
     ParseNodeKind kind = pn->getKind();
-    bool lor = kind == ParseNodeKind::Or;
+    bool lor =
+      kind == ParseNodeKind::Or || kind == ParseNodeKind::CoalesceExpr;
     bool logop = lor || (kind == ParseNodeKind::And);
 
     ParseNode* head = pn->pn_head;
     RootedValue left(cx);
     if (!expression(head, &left))
         return false;
     for (ParseNode* next = head->pn_next; next; next = next->pn_next) {
         RootedValue right(cx);
diff --git a/js/src/frontend/BytecodeEmitter.cpp b/js/src/frontend/BytecodeEmitter.cpp
--- a/js/src/frontend/BytecodeEmitter.cpp
+++ b/js/src/frontend/BytecodeEmitter.cpp
@@ -48,16 +48,17 @@
 
 #include "frontend/ParseNode-inl.h"
 #include "vm/JSObject-inl.h"
 
 using namespace js;
 using namespace js::gc;
 using namespace js::frontend;
 
+using mozilla::ArrayLength;
 using mozilla::AssertedCast;
 using mozilla::DebugOnly;
 using mozilla::Maybe;
 using mozilla::Nothing;
 using mozilla::NumberEqualsInt32;
 using mozilla::NumberIsInt32;
 using mozilla::PodCopy;
 using mozilla::Some;
@@ -1135,16 +1136,17 @@ BytecodeEmitter::checkSideEffects(ParseN
       case ParseNodeKind::SetThis:
         MOZ_ASSERT(pn->isArity(PN_BINARY));
         *answer = true;
         return true;
 
       case ParseNodeKind::StatementList:
       // Strict equality operations and logical operators are well-behaved and
       // perform no conversions.
+      case ParseNodeKind::CoalesceExpr:
       case ParseNodeKind::Or:
       case ParseNodeKind::And:
       case ParseNodeKind::StrictEq:
       case ParseNodeKind::StrictNe:
       // Any subexpression of a comma expression could be effectful.
       case ParseNodeKind::Comma:
         MOZ_ASSERT(pn->pn_count > 0);
         MOZ_FALLTHROUGH;
@@ -6694,20 +6696,25 @@ BytecodeEmitter::emitCallOrNew(ParseNode
         uint32_t lineNum = parser->errorReporter().lineAt(pn->pn_pos.begin);
         if (!emitUint32Operand(JSOP_LINENO, lineNum))
             return false;
     }
 
     return true;
 }
 
+// This list must be kept in the same order in several places:
+//   - The binary operators in ParseNode.h ,
+//   - the binary operators in TokenKind.h
+//   - the precedence list in Parser.cpp
 static const JSOp ParseNodeKindToJSOp[] = {
     // JSOP_NOP is for pipeline operator which does not emit its own JSOp
     // but has highest precedence in binary operators
     JSOP_NOP,
+    JSOP_NOP,
     JSOP_OR,
     JSOP_AND,
     JSOP_BITOR,
     JSOP_BITXOR,
     JSOP_BITAND,
     JSOP_STRICTEQ,
     JSOP_EQ,
     JSOP_STRICTNE,
@@ -6724,22 +6731,27 @@ static const JSOp ParseNodeKindToJSOp[] 
     JSOP_ADD,
     JSOP_SUB,
     JSOP_MUL,
     JSOP_DIV,
     JSOP_MOD,
     JSOP_POW
 };
 
-static inline JSOp
-BinaryOpParseNodeKindToJSOp(ParseNodeKind pnk)
-{
-    MOZ_ASSERT(pnk >= ParseNodeKind::BinOpFirst);
-    MOZ_ASSERT(pnk <= ParseNodeKind::BinOpLast);
-    return ParseNodeKindToJSOp[size_t(pnk) - size_t(ParseNodeKind::BinOpFirst)];
+static inline JSOp BinaryOpParseNodeKindToJSOp(ParseNodeKind pnk) {
+  MOZ_ASSERT(pnk >= ParseNodeKind::BinOpFirst);
+  MOZ_ASSERT(pnk <= ParseNodeKind::BinOpLast);
+  int parseNodeFirst = size_t(ParseNodeKind::BinOpFirst);
+#ifdef DEBUG
+  int jsopArraySize = ArrayLength(ParseNodeKindToJSOp);
+  int parseNodeKindListSize =
+      size_t(ParseNodeKind::BinOpLast) - parseNodeFirst + 1;
+  MOZ_ASSERT(jsopArraySize == parseNodeKindListSize);
+#endif
+  return ParseNodeKindToJSOp[size_t(pnk) - parseNodeFirst];
 }
 
 bool
 BytecodeEmitter::emitRightAssociative(ParseNode* pn)
 {
     // ** is the only right-associative operator.
     MOZ_ASSERT(pn->isKind(ParseNodeKind::Pow));
     MOZ_ASSERT(pn->isArity(PN_LIST));
@@ -6774,17 +6786,19 @@ BytecodeEmitter::emitLeftAssociative(Par
     } while ((nextExpr = nextExpr->pn_next));
     return true;
 }
 
 bool
 BytecodeEmitter::emitLogical(ParseNode* pn)
 {
     MOZ_ASSERT(pn->isArity(PN_LIST));
-    MOZ_ASSERT(pn->isKind(ParseNodeKind::Or) || pn->isKind(ParseNodeKind::And));
+    MOZ_ASSERT(pn->isKind(ParseNodeKind::Or) ||
+               pn->isKind(ParseNodeKind::CoalesceExpr) ||
+               pn->isKind(ParseNodeKind::And));
 
     /*
      * JSOP_OR converts the operand on the stack to boolean, leaves the original
      * value on the stack and jumps if true; otherwise it falls into the next
      * bytecode, which pops the left operand and then evaluates the right operand.
      * The jump goes around the right operand evaluation.
      *
      * JSOP_AND converts the operand on the stack to boolean and jumps if false;
@@ -6792,17 +6806,20 @@ BytecodeEmitter::emitLogical(ParseNode* 
      */
 
     TDZCheckCache tdzCache(this);
 
     /* Left-associative operator chain: avoid too much recursion. */
     ParseNode* pn2 = pn->pn_head;
     if (!emitTree(pn2))
         return false;
-    JSOp op = pn->isKind(ParseNodeKind::Or) ? JSOP_OR : JSOP_AND;
+    JSOp op = (pn->isKind(ParseNodeKind::Or) ||
+               pn->isKind(ParseNodeKind::CoalesceExpr))
+                  ? JSOP_OR
+                  : JSOP_AND;
     JumpList jump;
     if (!emitJump(op, &jump))
         return false;
     if (!emit1(JSOP_POP))
         return false;
 
     /* Emit nodes between the head and the tail. */
     while ((pn2 = pn2->pn_next)->pn_next) {
@@ -8064,16 +8081,17 @@ BytecodeEmitter::emitTree(ParseNode* pn,
             return false;
         break;
 
       case ParseNodeKind::Conditional:
         if (!emitConditionalExpression(pn->as<ConditionalExpression>(), valueUsage))
             return false;
         break;
 
+      case ParseNodeKind::CoalesceExpr:
       case ParseNodeKind::Or:
       case ParseNodeKind::And:
         if (!emitLogical(pn))
             return false;
         break;
 
       case ParseNodeKind::Add:
       case ParseNodeKind::Sub:
diff --git a/js/src/frontend/FoldConstants.cpp b/js/src/frontend/FoldConstants.cpp
--- a/js/src/frontend/FoldConstants.cpp
+++ b/js/src/frontend/FoldConstants.cpp
@@ -299,16 +299,17 @@ ContainsHoistedDeclaration(JSContext* cx
       case ParseNodeKind::DeleteElem:
       case ParseNodeKind::DeleteExpr:
       case ParseNodeKind::Pos:
       case ParseNodeKind::Neg:
       case ParseNodeKind::PreIncrement:
       case ParseNodeKind::PostIncrement:
       case ParseNodeKind::PreDecrement:
       case ParseNodeKind::PostDecrement:
+      case ParseNodeKind::CoalesceExpr:
       case ParseNodeKind::Or:
       case ParseNodeKind::And:
       case ParseNodeKind::BitOr:
       case ParseNodeKind::BitXor:
       case ParseNodeKind::BitAnd:
       case ParseNodeKind::StrictEq:
       case ParseNodeKind::Eq:
       case ParseNodeKind::StrictNe:
@@ -717,20 +718,24 @@ FoldIncrementDecrement(JSContext* cx, Pa
     return true;
 }
 
 static bool
 FoldAndOr(JSContext* cx, ParseNode** nodePtr, PerHandlerParser<FullParseHandler>& parser)
 {
     ParseNode* node = *nodePtr;
 
-    MOZ_ASSERT(node->isKind(ParseNodeKind::And) || node->isKind(ParseNodeKind::Or));
+    MOZ_ASSERT(node->isKind(ParseNodeKind::And) ||
+               node->isKind(ParseNodeKind::CoalesceExpr) ||
+               node->isKind(ParseNodeKind::Or));
+
     MOZ_ASSERT(node->isArity(PN_LIST));
 
-    bool isOrNode = node->isKind(ParseNodeKind::Or);
+    bool isOrNode = node->isKind(ParseNodeKind::Or) ||
+                    node->isKind(ParseNodeKind::CoalesceExpr);
     ParseNode** elem = &node->pn_head;
     do {
         if (!Fold(cx, elem, parser))
             return false;
 
         Truthiness t = Boolish(*elem);
 
         // If we don't know the constant-folded node's truthiness, we can't
diff --git a/js/src/frontend/ParseNode.h b/js/src/frontend/ParseNode.h
--- a/js/src/frontend/ParseNode.h
+++ b/js/src/frontend/ParseNode.h
@@ -140,21 +140,26 @@ class ObjectBox;
     /* Unary operators. */ \
     F(TypeOfName) \
     F(TypeOfExpr) \
     F(Void) \
     F(Not) \
     F(BitNot) \
     F(Await) \
     \
-    /* \
-     * Binary operators. \
-     * These must be in the same order as TOK_OR and friends in TokenStream.h. \
-     */ \
+    /*                                                             \
+     * Binary operators.                                           \
+     * This list must be kept in the same order in several places: \
+     *   - The binary operators in ParseNode.h                     \
+     *   - the binary operators in TokenKind.h                     \
+     *   - the precedence list in Parser.cpp                       \
+     *   - the JSOp code list in BytecodeEmitter.cpp               \
+     */                                                            \
     F(Pipeline) \
+    F(CoalesceExpr) \
     F(Or) \
     F(And) \
     F(BitOr) \
     F(BitXor) \
     F(BitAnd) \
     F(StrictEq) \
     F(Eq) \
     F(StrictNe) \
@@ -333,19 +338,25 @@ IsTypeofKind(ParseNodeKind kind)
  * LshAssign,
  * RshAssign,
  * UrshAssign,
  * MulAssign,
  * DivAssign,
  * ModAssign,
  * PowAssign
  * Conditional ternary  (cond ? trueExpr : falseExpr)
- *                          pn_kid1: cond, pn_kid2: then, pn_kid3: else
- * Or,      list        pn_head; list of pn_count subexpressions
- * And,                 All of these operators are left-associative except (**).
+ *                          pn_kid1: cond,
+ *                          pn_kid2: then,
+ *                          pn_kid3: else
+ *                          list pn_head; list of pn_count subexpressions
+ * All of these operators are left-associative except (**).
+ * PipelineExpr,
+ * CoalesceExpr
+ * Or,
+ * And,
  * BitOr,
  * BitXor,
  * BitAnd,
  * Eq,
  * Ne,
  * StrictEq,
  * StrictNe,
  * Lt,
diff --git a/js/src/frontend/Parser.cpp b/js/src/frontend/Parser.cpp
--- a/js/src/frontend/Parser.cpp
+++ b/js/src/frontend/Parser.cpp
@@ -8109,43 +8109,44 @@ static ParseNodeKind
 BinaryOpTokenKindToParseNodeKind(TokenKind tok)
 {
     MOZ_ASSERT(TokenKindIsBinaryOp(tok));
     return ParseNodeKind(size_t(ParseNodeKind::BinOpFirst) + (size_t(tok) - size_t(TokenKind::BinOpFirst)));
 }
 
 static const int PrecedenceTable[] = {
     1, /* ParseNodeKind::Pipeline */
-    2, /* ParseNodeKind::Or */
-    3, /* ParseNodeKind::And */
-    4, /* ParseNodeKind::BitOr */
-    5, /* ParseNodeKind::BitXor */
-    6, /* ParseNodeKind::BitAnd */
-    7, /* ParseNodeKind::StrictEq */
-    7, /* ParseNodeKind::Eq */
-    7, /* ParseNodeKind::StrictNe */
-    7, /* ParseNodeKind::Ne */
-    8, /* ParseNodeKind::Lt */
-    8, /* ParseNodeKind::Le */
-    8, /* ParseNodeKind::Gt */
-    8, /* ParseNodeKind::Ge */
-    8, /* ParseNodeKind::InstanceOf */
-    8, /* ParseNodeKind::In */
-    9, /* ParseNodeKind::Lsh */
-    9, /* ParseNodeKind::Rsh */
-    9, /* ParseNodeKind::Ursh */
-    10, /* ParseNodeKind::Add */
-    10, /* ParseNodeKind::Sub */
-    11, /* ParseNodeKind::Star */
-    11, /* ParseNodeKind::Div */
-    11, /* ParseNodeKind::Mod */
-    12  /* ParseNodeKind::Pow */
+    2,  /* ParseNodeKind::Coalesce */
+    3,  /* ParseNodeKind::Or */
+    4,  /* ParseNodeKind::And */
+    5,  /* ParseNodeKind::BitOr */
+    6,  /* ParseNodeKind::BitXor */
+    7,  /* ParseNodeKind::BitAnd */
+    8,  /* ParseNodeKind::StrictEq */
+    8,  /* ParseNodeKind::Eq */
+    8,  /* ParseNodeKind::StrictNe */
+    8,  /* ParseNodeKind::Ne */
+    9,  /* ParseNodeKind::Lt */
+    9,  /* ParseNodeKind::Le */
+    9,  /* ParseNodeKind::Gt */
+    9,  /* ParseNodeKind::Ge */
+    9,  /* ParseNodeKind::InstanceOf */
+    9,  /* ParseNodeKind::In */
+    10, /* ParseNodeKind::Lsh */
+    10, /* ParseNodeKind::Rsh */
+    10, /* ParseNodeKind::Ursh */
+    11, /* ParseNodeKind::Add */
+    11, /* ParseNodeKind::Sub */
+    12, /* ParseNodeKind::Star */
+    12, /* ParseNodeKind::Div */
+    12, /* ParseNodeKind::Mod */
+    13  /* ParseNodeKind::Pow */
 };
 
-static const int PRECEDENCE_CLASSES = 12;
+static const int PRECEDENCE_CLASSES = 13;
 
 static int
 Precedence(ParseNodeKind pnk) {
     // Everything binds tighter than ParseNodeKind::Limit, because we want
     // to reduce all nodes to a single node when we reach a token that is not
     // another binary operator.
     if (pnk == ParseNodeKind::Limit)
         return 0;
diff --git a/js/src/frontend/TokenKind.h b/js/src/frontend/TokenKind.h
--- a/js/src/frontend/TokenKind.h
+++ b/js/src/frontend/TokenKind.h
@@ -153,18 +153,26 @@
     /* \
      * The following token types occupy contiguous ranges to enable easy \
      * range-testing. \
      */ \
     /* \
      * Binary operators tokens, Or thru Pow. These must be in the same \
      * order as F(OR) and friends in FOR_EACH_PARSE_NODE_KIND in ParseNode.h. \
      */ \
+     /* \
+      * Binary operators.                                                 \
+      * This list must be kept in the same order in several places:       \
+      *   - the binary operators in ParseNode.h                           \
+      *   - the precedence list in Parser.cpp                             \
+      *   - the JSOp code list in BytecodeEmitter.cpp                     \
+     */ \
     macro(Pipeline,     "'|>'") \
     range(BinOpFirst,   Pipeline) \
+    macro(Coalesce,     "'\?\?'")  /* escapes to avoid trigraphs warning */ \
     macro(Or,           "'||'")   /* logical or */ \
     macro(And,          "'&&'")   /* logical and */ \
     macro(BitOr,        "'|'")    /* bitwise-or */ \
     macro(BitXor,       "'^'")    /* bitwise-xor */ \
     macro(BitAnd,       "'&'")    /* bitwise-and */ \
     \
     /* Equality operation tokens, per TokenKindIsEquality. */ \
     macro(StrictEq,     "'==='") \
diff --git a/js/src/frontend/TokenStream.cpp b/js/src/frontend/TokenStream.cpp
--- a/js/src/frontend/TokenStream.cpp
+++ b/js/src/frontend/TokenStream.cpp
@@ -1954,63 +1954,61 @@ enum FirstCharKind {
     String,
     EOL,
     ZeroDigit,
     Other,
 
     LastCharKind = Other
 };
 
-// OneChar: 40,  41,  44,  58,  59,  63,  91,  93,  123, 125, 126:
-//          '(', ')', ',', ':', ';', '?', '[', ']', '{', '}', '~'
+// OneChar: 40,  41,  44,  58,  59,  91,  93,  123, 125, 126:
+//          '(', ')', ',', ':', ';', '[', ']', '{', '}', '~'
 // Ident:   36, 65..90, 95, 97..122: '$', 'A'..'Z', '_', 'a'..'z'
 // Dot:     46: '.'
 // Equals:  61: '='
 // String:  34, 39, 96: '"', '\'', '`'
 // Dec:     49..57: '1'..'9'
 // Plus:    43: '+'
 // ZeroDigit:  48: '0'
 // Space:   9, 11, 12, 32: '\t', '\v', '\f', ' '
 // EOL:     10, 13: '\n', '\r'
 //
 #define T_COMMA     size_t(TokenKind::Comma)
 #define T_COLON     size_t(TokenKind::Colon)
 #define T_BITNOT    size_t(TokenKind::BitNot)
 #define T_LP        size_t(TokenKind::LeftParen)
 #define T_RP        size_t(TokenKind::RightParen)
 #define T_SEMI      size_t(TokenKind::Semi)
-#define T_HOOK      size_t(TokenKind::Hook)
 #define T_LB        size_t(TokenKind::LeftBracket)
 #define T_RB        size_t(TokenKind::RightBracket)
 #define T_LC        size_t(TokenKind::LeftCurly)
 #define T_RC        size_t(TokenKind::RightCurly)
 #define _______     Other
 static const uint8_t firstCharKinds[] = {
 /*         0        1        2        3        4        5        6        7        8        9    */
 /*   0+ */ _______, _______, _______, _______, _______, _______, _______, _______, _______,   Space,
 /*  10+ */     EOL,   Space,   Space,     EOL, _______, _______, _______, _______, _______, _______,
 /*  20+ */ _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
 /*  30+ */ _______, _______,   Space, _______,  String, _______,   Ident, _______, _______,  String,
 /*  40+ */    T_LP,    T_RP, _______, _______, T_COMMA, _______, _______, _______,ZeroDigit,    Dec,
 /*  50+ */     Dec,     Dec,     Dec,     Dec,     Dec,     Dec,     Dec,     Dec, T_COLON,  T_SEMI,
-/*  60+ */ _______, _______, _______,  T_HOOK, _______,   Ident,   Ident,   Ident,   Ident,   Ident,
+/*  60+ */ _______, _______, _______, _______, _______,   Ident,   Ident,   Ident,   Ident,   Ident,
 /*  70+ */   Ident,   Ident,   Ident,   Ident,   Ident,   Ident,   Ident,   Ident,   Ident,   Ident,
 /*  80+ */   Ident,   Ident,   Ident,   Ident,   Ident,   Ident,   Ident,   Ident,   Ident,   Ident,
 /*  90+ */   Ident,    T_LB, _______,    T_RB, _______,   Ident,  String,   Ident,   Ident,   Ident,
 /* 100+ */   Ident,   Ident,   Ident,   Ident,   Ident,   Ident,   Ident,   Ident,   Ident,   Ident,
 /* 110+ */   Ident,   Ident,   Ident,   Ident,   Ident,   Ident,   Ident,   Ident,   Ident,   Ident,
 /* 120+ */   Ident,   Ident,   Ident,    T_LC, _______,    T_RC,T_BITNOT, _______
 };
 #undef T_COMMA
 #undef T_COLON
 #undef T_BITNOT
 #undef T_LP
 #undef T_RP
 #undef T_SEMI
-#undef T_HOOK
 #undef T_LB
 #undef T_RB
 #undef T_LC
 #undef T_RC
 #undef _______
 
 static_assert(LastCharKind < (1 << (sizeof(firstCharKinds[0]) * 8)),
               "Elements of firstCharKinds[] are too small");
@@ -2626,16 +2624,20 @@ TokenStreamSpecific<CharT, AnyCharsAcces
 
           case '&':
             if (matchCodeUnit('&'))
                 simpleKind = TokenKind::And;
             else
                 simpleKind = matchCodeUnit('=') ? TokenKind::BitAndAssign : TokenKind::BitAnd;
             break;
 
+          case '?':
+            simpleKind = matchCodeUnit('?') ? TokenKind::Coalesce : TokenKind::Hook;
+            break;
+
           case '!':
             if (matchCodeUnit('='))
                 simpleKind = matchCodeUnit('=') ? TokenKind::StrictNe : TokenKind::Ne;
             else
                 simpleKind = TokenKind::Not;
             break;
 
           case '<':
