# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1517456016 21600
# Node ID 593fe6b692c74531899bf721c1e73868f56a6c8a
# Parent  3d6c4ebef5e358637a7a6a8f136c57b7e6f07814
servo: Merge #19912 - style: Temporarily use OrderMap on Gecko (from emilio:ordermap); r=Manishearth

This will allow us to discard std hash map as a source of crashes.

Source-Repo: https://github.com/servo/servo
Source-Revision: e6d9c251d1b9770aa520bf358b79d906a00d71bb

diff --git a/Cargo.lock b/Cargo.lock
--- a/Cargo.lock
+++ b/Cargo.lock
@@ -697,16 +697,17 @@ version = "0.2.11"
 source = "registry+https://github.com/rust-lang/crates.io-index"
 checksum = "8be18de09a56b60ed0edf84bc9df007e30040691af7acd1c41874faac5895bfb"
 
 [[package]]
 name = "hashglobe"
 version = "0.1.0"
 dependencies = [
  "libc",
+ "ordermap",
 ]
 
 [[package]]
 name = "ident_case"
 version = "1.0.0"
 source = "registry+https://github.com/rust-lang/crates.io-index"
 checksum = "3c9826188e666f2ed92071d2dadef6edc430b11b158b5b2b3f4babbcc891eaaa"
 
@@ -863,16 +864,17 @@ dependencies = [
 [[package]]
 name = "malloc_size_of"
 version = "0.0.1"
 dependencies = [
  "app_units",
  "cssparser",
  "euclid",
  "hashglobe",
+ "ordermap",
  "selectors",
  "servo_arc",
  "smallbitvec",
  "smallvec",
 ]
 
 [[package]]
 name = "malloc_size_of_derive"
@@ -1085,16 +1087,22 @@ version = "0.4.0"
 source = "registry+https://github.com/rust-lang/crates.io-index"
 checksum = "da12c96037889ae0be29dd2bdd260e5a62a7df24e6466d5a15bb8131c1c200a8"
 dependencies = [
  "num-traits 0.1.41",
  "unreachable",
 ]
 
 [[package]]
+name = "ordermap"
+version = "0.3.5"
+source = "registry+https://github.com/rust-lang/crates.io-index"
+checksum = "a86ed3f5f244b372d6b1a00b72ef7f8876d0bc6a78a4c9985c53614041512063"
+
+[[package]]
 name = "owning_ref"
 version = "0.3.3"
 source = "registry+https://github.com/rust-lang/crates.io-index"
 checksum = "cdf84f41639e037b484f93433aa3897863b561ed65c6e59c7073d7c561710f37"
 dependencies = [
  "stable_deref_trait",
 ]
 
diff --git a/servo/components/hashglobe/Cargo.toml b/servo/components/hashglobe/Cargo.toml
--- a/servo/components/hashglobe/Cargo.toml
+++ b/servo/components/hashglobe/Cargo.toml
@@ -6,11 +6,12 @@ license = "MIT/Apache-2.0"
 description = "Fork of std::HashMap with stable fallible allocation."
 documentation = "https://docs.rs/hashglobe"
 repository = "https://github.com/Manishearth/hashglobe"
 
 readme = "README.md"
 
 [dependencies]
 libc = "0.2"
+ordermap = "0.3"
 
 [dev-dependencies]
 rand = "0.3"
diff --git a/servo/components/hashglobe/src/lib.rs b/servo/components/hashglobe/src/lib.rs
--- a/servo/components/hashglobe/src/lib.rs
+++ b/servo/components/hashglobe/src/lib.rs
@@ -3,23 +3,26 @@
 // http://rust-lang.org/COPYRIGHT.
 //
 // Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 // http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 // <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
 // option. This file may not be copied, modified, or distributed
 // except according to those terms.
 
+extern crate ordermap;
+
 pub mod alloc;
 pub mod hash_map;
 pub mod hash_set;
 mod shim;
 mod table;
 
 pub mod fake;
+pub mod order;
 
 use std::{error, fmt};
 
 trait Recover<Q: ?Sized> {
     type Key;
 
     fn get(&self, key: &Q) -> Option<&Self::Key>;
     fn take(&mut self, key: &Q) -> Option<Self::Key>;
diff --git a/servo/components/hashglobe/src/order.rs b/servo/components/hashglobe/src/order.rs
new file mode 100644
--- /dev/null
+++ b/servo/components/hashglobe/src/order.rs
@@ -0,0 +1,246 @@
+/* This Source Code Form is subject to the terms of the Mozilla Public
+ * License, v. 2.0. If a copy of the MPL was not distributed with this
+ * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
+
+//! This module contains shims around the ordermap crate that add fallible
+//! methods.
+//!
+//! These methods are a lie. They are not actually fallible. This is just to
+//! make it smooth to switch between hashmap impls the codebase.
+
+use ordermap::{OrderMap, OrderSet};
+use std::fmt;
+use std::hash::{BuildHasher, Hash};
+use std::ops::{Deref, DerefMut};
+
+pub use std::collections::hash_map::RandomState;
+pub use ordermap::{Entry, Iter as MapIter, IterMut as MapIterMut};
+pub use ordermap::set::{Iter as SetIter, IntoIter as SetIntoIter};
+
+#[derive(Clone)]
+pub struct HashMap<K, V, S = RandomState>(OrderMap<K, V, S>);
+
+
+use FailedAllocationError;
+
+impl<K, V, S> Deref for HashMap<K, V, S> {
+    type Target = OrderMap<K, V, S>;
+    fn deref(&self) -> &Self::Target {
+        &self.0
+    }
+}
+
+impl<K, V, S> DerefMut for HashMap<K, V, S> {
+    fn deref_mut(&mut self) -> &mut Self::Target {
+        &mut self.0
+    }
+}
+
+impl<K, V, S> HashMap<K, V, S>
+    where K: Eq + Hash,
+          S: BuildHasher
+{
+    #[inline]
+    pub fn try_with_hasher(hash_builder: S) -> Result<HashMap<K, V, S>, FailedAllocationError> {
+        Ok(HashMap(OrderMap::with_hasher(hash_builder)))
+    }
+
+    #[inline]
+    pub fn try_with_capacity_and_hasher(capacity: usize,
+                                        hash_builder: S)
+        -> Result<HashMap<K, V, S>, FailedAllocationError> {
+        Ok(HashMap(OrderMap::with_capacity_and_hasher(capacity, hash_builder)))
+    }
+
+    pub fn with_capacity_and_hasher(capacity: usize, hash_builder: S) -> HashMap<K, V, S> {
+        HashMap(OrderMap::with_capacity_and_hasher(capacity, hash_builder))
+    }
+
+
+    #[inline]
+    pub fn try_reserve(&mut self, additional: usize) -> Result<(), FailedAllocationError> {
+        Ok(self.reserve(additional))
+    }
+
+    #[inline]
+    pub fn try_entry(&mut self, key: K) -> Result<Entry<K, V, S>, FailedAllocationError> {
+        Ok(self.entry(key))
+    }
+
+    #[inline]
+    pub fn try_insert(&mut self, k: K, v: V) -> Result<Option<V>, FailedAllocationError> {
+        Ok(self.insert(k, v))
+    }
+}
+
+#[derive(Clone)]
+pub struct HashSet<T, S = RandomState>(OrderSet<T, S>);
+
+
+impl<T, S> Deref for HashSet<T, S> {
+    type Target = OrderSet<T, S>;
+    fn deref(&self) -> &Self::Target {
+        &self.0
+    }
+}
+
+impl<T, S> DerefMut for HashSet<T, S> {
+    fn deref_mut(&mut self) -> &mut Self::Target {
+        &mut self.0
+    }
+}
+
+impl<T: Hash + Eq> HashSet<T, RandomState> {
+    #[inline]
+    pub fn new() -> HashSet<T, RandomState> {
+        HashSet(OrderSet::new())
+    }
+
+    #[inline]
+    pub fn with_capacity(capacity: usize) -> HashSet<T, RandomState> {
+        HashSet(OrderSet::with_capacity(capacity))
+    }
+}
+
+
+impl<T, S> HashSet<T, S>
+    where T: Eq + Hash,
+          S: BuildHasher
+{
+    #[inline]
+    pub fn with_hasher(hasher: S) -> HashSet<T, S> {
+        HashSet(OrderSet::with_hasher(hasher))
+    }
+
+
+    #[inline]
+    pub fn with_capacity_and_hasher(capacity: usize, hasher: S) -> HashSet<T, S> {
+        HashSet(OrderSet::with_capacity_and_hasher(capacity, hasher))
+    }
+
+    #[inline]
+    pub fn try_reserve(&mut self, additional: usize) -> Result<(), FailedAllocationError> {
+        Ok(self.reserve(additional))
+    }
+
+    #[inline]
+    pub fn try_insert(&mut self, value: T) -> Result<bool, FailedAllocationError> {
+        Ok(self.insert(value))
+    }
+}
+
+// Pass through trait impls
+// We can't derive these since the bounds are not obvious to the derive macro
+
+impl<K: Hash + Eq, V, S: BuildHasher + Default> Default for HashMap<K, V, S> {
+    fn default() -> Self {
+        HashMap(Default::default())
+    }
+}
+
+impl<K, V, S> fmt::Debug for HashMap<K, V, S>
+    where K: Eq + Hash + fmt::Debug,
+          V: fmt::Debug,
+          S: BuildHasher {
+    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
+        self.0.fmt(f)
+    }
+}
+
+impl<K, V, S> PartialEq for HashMap<K, V, S>
+    where K: Eq + Hash,
+          V: PartialEq,
+          S: BuildHasher
+{
+    fn eq(&self, other: &HashMap<K, V, S>) -> bool {
+        self.0.eq(&other.0)
+    }
+}
+
+impl<K, V, S> Eq for HashMap<K, V, S>
+    where K: Eq + Hash,
+          V: Eq,
+          S: BuildHasher
+{
+}
+
+impl<'a, K, V, S> IntoIterator for &'a HashMap<K, V, S>
+    where K: Eq + Hash,
+          S: BuildHasher
+{
+    type Item = (&'a K, &'a V);
+    type IntoIter = MapIter<'a, K, V>;
+
+    fn into_iter(self) -> MapIter<'a, K, V> {
+        self.0.iter()
+    }
+}
+
+impl<'a, K, V, S> IntoIterator for &'a mut HashMap<K, V, S>
+    where K: Eq + Hash,
+          S: BuildHasher
+{
+    type Item = (&'a K, &'a mut V);
+    type IntoIter = MapIterMut<'a, K, V>;
+
+    fn into_iter(self) -> MapIterMut<'a, K, V> {
+        self.0.iter_mut()
+    }
+}
+
+impl<T: Eq + Hash, S: BuildHasher + Default> Default for HashSet<T, S> {
+    fn default() -> Self {
+        HashSet(Default::default())
+    }
+}
+
+impl<T, S> fmt::Debug for HashSet<T, S>
+    where T: Eq + Hash + fmt::Debug,
+          S: BuildHasher
+{
+    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
+        self.0.fmt(f)
+    }
+}
+
+impl<T, S> PartialEq for HashSet<T, S>
+    where T: Eq + Hash,
+          S: BuildHasher
+{
+    fn eq(&self, other: &HashSet<T, S>) -> bool {
+        self.0.eq(&other.0)
+    }
+}
+
+impl<T, S> Eq for HashSet<T, S>
+    where T: Eq + Hash,
+          S: BuildHasher
+{
+}
+
+impl<'a, T, S> IntoIterator for &'a HashSet<T, S>
+    where T: Eq + Hash,
+          S: BuildHasher
+{
+    type Item = &'a T;
+    type IntoIter = SetIter<'a, T>;
+
+    fn into_iter(self) -> SetIter<'a, T> {
+        self.0.iter()
+    }
+}
+
+impl<T, S> IntoIterator for HashSet<T, S>
+    where T: Eq + Hash,
+          S: BuildHasher
+{
+    type Item = T;
+    type IntoIter = SetIntoIter<T>;
+
+
+    fn into_iter(self) -> SetIntoIter<T> {
+        self.0.into_iter()
+    }
+}
+
+
diff --git a/servo/components/malloc_size_of/Cargo.toml b/servo/components/malloc_size_of/Cargo.toml
--- a/servo/components/malloc_size_of/Cargo.toml
+++ b/servo/components/malloc_size_of/Cargo.toml
@@ -12,16 +12,17 @@ path = "lib.rs"
 servo = ["mozjs", "string_cache", "url", "webrender_api", "xml5ever"]
 
 [dependencies]
 app_units = "0.6"
 cssparser = "0.23.0"
 euclid = "0.16"
 hashglobe = { path = "../hashglobe" }
 mozjs = { version = "0.1.8", features = ["promises"], optional = true }
+ordermap = "0.3"
 selectors = { path = "../selectors" }
 servo_arc = { path = "../servo_arc" }
 smallbitvec = "1.0.3"
 smallvec = "0.6"
 string_cache = { version = "0.7", optional = true }
 url = { version = "1.2", optional = true }
 webrender_api = { git = "https://github.com/servo/webrender", features = ["ipc"], optional = true }
 xml5ever = { version = "0.12", optional = true }
diff --git a/servo/components/malloc_size_of/lib.rs b/servo/components/malloc_size_of/lib.rs
--- a/servo/components/malloc_size_of/lib.rs
+++ b/servo/components/malloc_size_of/lib.rs
@@ -44,16 +44,17 @@
 //!   `<Box<_> as MallocSizeOf>::size_of(field, ops)`.
 
 extern crate app_units;
 extern crate cssparser;
 extern crate euclid;
 extern crate hashglobe;
 #[cfg(feature = "servo")]
 extern crate mozjs as js;
+extern crate ordermap;
 extern crate selectors;
 extern crate servo_arc;
 extern crate smallbitvec;
 extern crate smallvec;
 #[cfg(feature = "servo")]
 extern crate string_cache;
 #[cfg(feature = "servo")]
 extern crate url;
@@ -404,16 +405,43 @@ impl<T, S> MallocShallowSizeOf for hashg
         if ops.has_malloc_enclosing_size_of() {
             self.iter().next().map_or(0, |t| unsafe { ops.malloc_enclosing_size_of(t) })
         } else {
             self.capacity() * (size_of::<T>() + size_of::<usize>())
         }
     }
 }
 
+impl<T, S> MallocSizeOf for ordermap::OrderSet<T, S>
+    where T: Eq + Hash + MallocSizeOf,
+          S: BuildHasher,
+{
+    fn size_of(&self, ops: &mut MallocSizeOfOps) -> usize {
+        let mut n = self.shallow_size_of(ops);
+        for t in self.iter() {
+            n += t.size_of(ops);
+        }
+        n
+    }
+}
+
+impl<T, S> MallocShallowSizeOf for ordermap::OrderSet<T, S>
+    where T: Eq + Hash,
+          S: BuildHasher
+{
+    fn shallow_size_of(&self, ops: &mut MallocSizeOfOps) -> usize {
+        // See the implementation for std::collections::HashSet for details.
+        if ops.has_malloc_enclosing_size_of() {
+            self.iter().next().map_or(0, |t| unsafe { ops.malloc_enclosing_size_of(t) })
+        } else {
+            self.capacity() * (size_of::<T>() + size_of::<usize>())
+        }
+    }
+}
+
 impl<T, S> MallocSizeOf for hashglobe::hash_set::HashSet<T, S>
     where T: Eq + Hash + MallocSizeOf,
           S: BuildHasher,
 {
     fn size_of(&self, ops: &mut MallocSizeOfOps) -> usize {
         let mut n = self.shallow_size_of(ops);
         for t in self.iter() {
             n += t.size_of(ops);
@@ -437,16 +465,36 @@ impl<T, S> MallocSizeOf for hashglobe::f
           S: BuildHasher,
 {
     fn size_of(&self, ops: &mut MallocSizeOfOps) -> usize {
         use std::ops::Deref;
         self.deref().size_of(ops)
     }
 }
 
+impl<T, S> MallocShallowSizeOf for hashglobe::order::HashSet<T, S>
+    where T: Eq + Hash,
+          S: BuildHasher,
+{
+    fn shallow_size_of(&self, ops: &mut MallocSizeOfOps) -> usize {
+        use std::ops::Deref;
+        self.deref().shallow_size_of(ops)
+    }
+}
+
+impl<T, S> MallocSizeOf for hashglobe::order::HashSet<T, S>
+    where T: Eq + Hash + MallocSizeOf,
+          S: BuildHasher,
+{
+    fn size_of(&self, ops: &mut MallocSizeOfOps) -> usize {
+        use std::ops::Deref;
+        self.deref().size_of(ops)
+    }
+}
+
 impl<K, V, S> MallocShallowSizeOf for std::collections::HashMap<K, V, S>
     where K: Eq + Hash,
           S: BuildHasher
 {
     fn shallow_size_of(&self, ops: &mut MallocSizeOfOps) -> usize {
         // See the implementation for std::collections::HashSet for details.
         if ops.has_malloc_enclosing_size_of() {
             self.values().next().map_or(0, |v| unsafe { ops.malloc_enclosing_size_of(v) })
@@ -466,16 +514,45 @@ impl<K, V, S> MallocSizeOf for std::coll
         for (k, v) in self.iter() {
             n += k.size_of(ops);
             n += v.size_of(ops);
         }
         n
     }
 }
 
+impl<K, V, S> MallocShallowSizeOf for ordermap::OrderMap<K, V, S>
+    where K: Eq + Hash,
+          S: BuildHasher
+{
+    fn shallow_size_of(&self, ops: &mut MallocSizeOfOps) -> usize {
+        // See the implementation for std::collections::HashSet for details.
+        if ops.has_malloc_enclosing_size_of() {
+            self.values().next().map_or(0, |v| unsafe { ops.malloc_enclosing_size_of(v) })
+        } else {
+            self.capacity() * (size_of::<V>() + size_of::<K>() + size_of::<usize>())
+        }
+    }
+}
+
+impl<K, V, S> MallocSizeOf for ordermap::OrderMap<K, V, S>
+    where K: Eq + Hash + MallocSizeOf,
+          V: MallocSizeOf,
+          S: BuildHasher,
+{
+    fn size_of(&self, ops: &mut MallocSizeOfOps) -> usize {
+        let mut n = self.shallow_size_of(ops);
+        for (k, v) in self.iter() {
+            n += k.size_of(ops);
+            n += v.size_of(ops);
+        }
+        n
+    }
+}
+
 impl<K, V, S> MallocShallowSizeOf for hashglobe::hash_map::HashMap<K, V, S>
     where K: Eq + Hash,
           S: BuildHasher
 {
     fn shallow_size_of(&self, ops: &mut MallocSizeOfOps) -> usize {
         // See the implementation for std::collections::HashSet for details.
         if ops.has_malloc_enclosing_size_of() {
             self.values().next().map_or(0, |v| unsafe { ops.malloc_enclosing_size_of(v) })
@@ -516,16 +593,37 @@ impl<K, V, S> MallocSizeOf for hashglobe
           S: BuildHasher,
 {
     fn size_of(&self, ops: &mut MallocSizeOfOps) -> usize {
         use std::ops::Deref;
         self.deref().size_of(ops)
     }
 }
 
+impl<K, V, S> MallocShallowSizeOf for hashglobe::order::HashMap<K, V, S>
+    where K: Eq + Hash,
+          S: BuildHasher,
+{
+    fn shallow_size_of(&self, ops: &mut MallocSizeOfOps) -> usize {
+        use std::ops::Deref;
+        self.deref().shallow_size_of(ops)
+    }
+}
+
+impl<K, V, S> MallocSizeOf for hashglobe::order::HashMap<K, V, S>
+    where K: Eq + Hash + MallocSizeOf,
+          V: MallocSizeOf,
+          S: BuildHasher,
+{
+    fn size_of(&self, ops: &mut MallocSizeOfOps) -> usize {
+        use std::ops::Deref;
+        self.deref().size_of(ops)
+    }
+}
+
 // PhantomData is always 0.
 impl<T> MallocSizeOf for std::marker::PhantomData<T> {
     fn size_of(&self, _ops: &mut MallocSizeOfOps) -> usize {
         0
     }
 }
 
 // XXX: we don't want MallocSizeOf to be defined for Rc and Arc. If negative
diff --git a/servo/components/style/hash.rs b/servo/components/style/hash.rs
--- a/servo/components/style/hash.rs
+++ b/servo/components/style/hash.rs
@@ -4,29 +4,36 @@
 
 //! Reexports of hashglobe types in Gecko mode, and stdlib hashmap shims in Servo mode
 //!
 //! Can go away when the stdlib gets fallible collections
 //! https://github.com/rust-lang/rfcs/pull/2116
 
 use fnv;
 
+// #[cfg(feature = "gecko")]
+// pub use hashglobe::hash_map::HashMap;
+// #[cfg(feature = "gecko")]
+// pub use hashglobe::hash_set::HashSet;
 #[cfg(feature = "gecko")]
-pub use hashglobe::hash_map::HashMap;
+pub use hashglobe::order::HashMap;
 #[cfg(feature = "gecko")]
-pub use hashglobe::hash_set::HashSet;
+pub use hashglobe::order::HashSet;
 
 
 #[cfg(feature = "servo")]
 pub use hashglobe::fake::{HashMap, HashSet};
 
+
 /// Appropriate reexports of hash_map types
 pub mod map {
+    // #[cfg(feature = "gecko")]
+    // pub use hashglobe::hash_map::{Entry, Iter};
     #[cfg(feature = "gecko")]
-    pub use hashglobe::hash_map::{Entry, Iter};
+    pub use hashglobe::order::{Entry, MapIter as Iter};
     #[cfg(feature = "servo")]
     pub use std::collections::hash_map::{Entry, Iter};
 }
 
 /// Hash map that uses the FNV hasher
 pub type FnvHashMap<K, V> = HashMap<K, V, fnv::FnvBuildHasher>;
 /// Hash set that uses the FNV hasher
 pub type FnvHashSet<T> = HashSet<T, fnv::FnvBuildHasher>;
diff --git a/servo/components/style/invalidation/element/invalidation_map.rs b/servo/components/style/invalidation/element/invalidation_map.rs
--- a/servo/components/style/invalidation/element/invalidation_map.rs
+++ b/servo/components/style/invalidation/element/invalidation_map.rs
@@ -268,27 +268,27 @@ impl InvalidationMap {
                 index += 1; // Account for the simple selector.
             }
 
             self.has_id_attribute_selectors |= compound_visitor.has_id_attribute_selectors;
             self.has_class_attribute_selectors |= compound_visitor.has_class_attribute_selectors;
 
             for class in compound_visitor.classes {
                 self.class_to_selector
-                    .entry(class, quirks_mode)
+                    .try_entry(class, quirks_mode)?
                     .or_insert_with(SmallVec::new)
                     .try_push(Dependency {
                         selector: selector.clone(),
                         selector_offset: sequence_start,
                     })?;
             }
 
             for id in compound_visitor.ids {
                 self.id_to_selector
-                    .entry(id, quirks_mode)
+                    .try_entry(id, quirks_mode)?
                     .or_insert_with(SmallVec::new)
                     .try_push(Dependency {
                         selector: selector.clone(),
                         selector_offset: sequence_start,
                     })?;
             }
 
             if !compound_visitor.state.is_empty() {
diff --git a/servo/components/style/selector_map.rs b/servo/components/style/selector_map.rs
--- a/servo/components/style/selector_map.rs
+++ b/servo/components/style/selector_map.rs
@@ -507,30 +507,39 @@ pub struct MaybeCaseInsensitiveHashMap<K
 // our HashMap fork (hashglobe) is able to use NonZero,
 // or when stdlib gets fallible collections
 impl<V: 'static> MaybeCaseInsensitiveHashMap<Atom, V> {
     /// Empty map
     pub fn new() -> Self {
         MaybeCaseInsensitiveHashMap(PrecomputedHashMap::default())
     }
 
-    /// HashMap::entry
-    pub fn entry(&mut self, mut key: Atom, quirks_mode: QuirksMode) -> hash_map::Entry<Atom, V> {
+    /// HashMap::try_entry
+    #[cfg(not(feature = "gecko"))]
+    pub fn try_entry(
+        &mut self,
+        mut key: Atom,
+        quirks_mode: QuirksMode,
+    ) -> Result<hash_map::Entry<Atom, V>, FailedAllocationError> {
         if quirks_mode == QuirksMode::Quirks {
             key = key.to_ascii_lowercase()
         }
-        self.0.entry(key)
+        self.0.try_entry(key)
     }
 
     /// HashMap::try_entry
+    ///
+    /// FIXME(emilio): Remove the extra Entry parameter and unify when ordermap
+    /// 0.4 is released.
+    #[cfg(feature = "gecko")]
     pub fn try_entry(
         &mut self,
         mut key: Atom,
-        quirks_mode: QuirksMode
-    ) -> Result<hash_map::Entry<Atom, V>, FailedAllocationError> {
+        quirks_mode: QuirksMode,
+    ) -> Result<hash_map::Entry<Atom, V, BuildHasherDefault<PrecomputedHasher>>, FailedAllocationError> {
         if quirks_mode == QuirksMode::Quirks {
             key = key.to_ascii_lowercase()
         }
         self.0.try_entry(key)
     }
 
     /// HashMap::iter
     pub fn iter(&self) -> hash_map::Iter<Atom, V> {
