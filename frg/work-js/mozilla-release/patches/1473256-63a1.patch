# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1531225971 -7200
# Node ID 238948fa4dd6f8b94e3d44c00d922830fbfbb0f7
# Parent  49addf2ffc711112af5c6a937ad66216b89caa05
Bug 1473256 - Don't add a length property in AddLengthProperty if it's already present. r=anba

diff --git a/js/src/builtin/Array.cpp b/js/src/builtin/Array.cpp
--- a/js/src/builtin/Array.cpp
+++ b/js/src/builtin/Array.cpp
@@ -1000,26 +1000,29 @@ ObjectMayHaveExtraIndexedProperties(JSOb
         if (obj->as<NativeObject>().getDenseInitializedLength() != 0)
             return true;
     } while (true);
 }
 
 static bool
 AddLengthProperty(JSContext* cx, HandleArrayObject obj)
 {
-    /*
-     * Add the 'length' property for a newly created array,
-     * and update the elements to be an empty array owned by the object.
-     * The shared emptyObjectElements singleton cannot be used for slow arrays,
-     * as accesses to 'length' will use the elements header.
-     */
+    // Add the 'length' property for a newly created array. Shapes are shared
+    // across realms within a zone and because we update the initial shape with
+    // a Shape that contains the length-property (in NewArray), it's possible
+    // the length property has already been defined.
+
+    Shape* shape = obj->lastProperty();
+    if (!shape->isEmptyShape()) {
+        MOZ_ASSERT(JSID_IS_ATOM(shape->propidRaw(), cx->names().length));
+        MOZ_ASSERT(shape->previous()->isEmptyShape());
+        return true;
+    }
 
     RootedId lengthId(cx, NameToId(cx->names().length));
-    MOZ_ASSERT(!obj->lookup(cx, lengthId));
-
     return NativeObject::addAccessorProperty(cx, obj, lengthId,
                                              array_length_getter, array_length_setter,
                                              JSPROP_PERMANENT | JSPROP_SHADOWABLE);
 }
 
 static bool
 IsArrayConstructor(const JSObject* obj)
 {
diff --git a/js/src/jit-test/tests/basic/bug1473256.js b/js/src/jit-test/tests/basic/bug1473256.js
new file mode 100644
--- /dev/null
+++ b/js/src/jit-test/tests/basic/bug1473256.js
@@ -0,0 +1,5 @@
+var a1 = Reflect.construct(Array, [], Object);
+var g = newGlobal({sameZoneAs: this});
+var a2 = new g.Array(1, 2, 3);
+assertEq(a1.length, 0);
+assertEq(a2.length, 3);
