# HG changeset patch
# User Nicolas B. Pierron <nicolas.b.pierron@gmail.com>
# Date 1521033411 0
#      Wed Mar 14 13:16:51 2018 +0000
# Node ID 55b8fe1015cb42340e3270bd9ad1f06efa5f7006
# Parent  056a27f8fd5469a5f74893bfc857f7164bbbbecf
Bug 1445619 - It is not longer acceptable to leak the world. r=Waldo,mccr8

diff --git a/js/src/jsfriendapi.cpp b/js/src/jsfriendapi.cpp
--- a/js/src/jsfriendapi.cpp
+++ b/js/src/jsfriendapi.cpp
@@ -1,16 +1,17 @@
 /* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
  * vim: set ts=8 sts=4 et sw=4 tw=99:
  * This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "jsfriendapi.h"
 
+#include "mozilla/Atomics.h"
 #include "mozilla/PodOperations.h"
 
 #include <stdint.h>
 
 #include "builtin/Promise.h"
 #include "builtin/TestingFunctions.h"
 #include "gc/GCInternals.h"
 #include "gc/PublicIterators.h"
@@ -1547,8 +1548,34 @@ js::SetCooperativeYieldCallback(JSContex
 }
 
 JS_FRIEND_API(bool)
 js::SystemZoneAvailable(JSContext* cx)
 {
     CooperatingContext& owner = cx->runtime()->gc.systemZoneGroup->ownerContext();
     return owner.context() == nullptr;
 }
+
+static LogCtorDtor sLogCtor = nullptr;
+static LogCtorDtor sLogDtor = nullptr;
+
+JS_FRIEND_API(void)
+js::SetLogCtorDtorFunctions(LogCtorDtor ctor, LogCtorDtor dtor)
+{
+    MOZ_ASSERT(!sLogCtor && !sLogDtor);
+    MOZ_ASSERT(ctor && dtor);
+    sLogCtor = ctor;
+    sLogDtor = dtor;
+}
+
+JS_FRIEND_API(void)
+js::LogCtor(void* self, const char* type, uint32_t sz)
+{
+    if (LogCtorDtor fun = sLogCtor)
+        fun(self, type, sz);
+}
+
+JS_FRIEND_API(void)
+js::LogDtor(void* self, const char* type, uint32_t sz)
+{
+    if (LogCtorDtor fun = sLogDtor)
+        fun(self, type, sz);
+}
diff --git a/js/src/jsfriendapi.h b/js/src/jsfriendapi.h
--- a/js/src/jsfriendapi.h
+++ b/js/src/jsfriendapi.h
@@ -3139,11 +3139,33 @@ typedef void
 extern JS_FRIEND_API(void)
 SetCooperativeYieldCallback(JSContext* cx, YieldCallback callback);
 
 // Returns true if the system zone is available (i.e., if no cooperative contexts
 // are using it now).
 extern JS_FRIEND_API(bool)
 SystemZoneAvailable(JSContext* cx);
 
+typedef void
+(* LogCtorDtor)(void* self, const char* type, uint32_t sz);
+
+/**
+ * Set global function used to monitor a few internal classes to highlight
+ * leaks, and to hint at the origin of the leaks.
+ */
+extern JS_FRIEND_API(void)
+SetLogCtorDtorFunctions(LogCtorDtor ctor, LogCtorDtor dtor);
+
+extern JS_FRIEND_API(void)
+LogCtor(void* self, const char* type, uint32_t sz);
+
+extern JS_FRIEND_API(void)
+LogDtor(void* self, const char* type, uint32_t sz);
+
+#define JS_COUNT_CTOR(Class)                            \
+    LogCtor((void*) this, #Class, sizeof(Class))
+
+#define JS_COUNT_DTOR(Class)                            \
+    LogDtor((void*) this, #Class, sizeof(Class))
+
 } /* namespace js */
 
 #endif /* jsfriendapi_h */
diff --git a/js/src/vm/Runtime.cpp b/js/src/vm/Runtime.cpp
--- a/js/src/vm/Runtime.cpp
+++ b/js/src/vm/Runtime.cpp
@@ -17,16 +17,17 @@
 # include <sys/resource.h>
 #endif // defined(XP_DARWIN) || defined(XP_UNIX) || defined(XP_WIN)
 #include <locale.h>
 #include <string.h>
 #ifdef JS_CAN_CHECK_THREADSAFE_ACCESSES
 # include <sys/mman.h>
 #endif
 
+#include "jsfriendapi.h"
 #include "jsmath.h"
 
 #include "builtin/Promise.h"
 #include "gc/FreeOp.h"
 #include "gc/GCInternals.h"
 #include "gc/PublicIterators.h"
 #include "jit/arm/Simulator-arm.h"
 #include "jit/arm64/vixl/Simulator-vixl.h"
@@ -174,26 +175,28 @@ JSRuntime::JSRuntime(JSRuntime* parentRu
     oomCallback(nullptr),
     debuggerMallocSizeOf(ReturnZeroSize),
     lastAnimationTime(0),
     performanceMonitoring_(),
     stackFormat_(parentRuntime ? js::StackFormat::Default
                                : js::StackFormat::SpiderMonkey),
     wasmInstances(mutexid::WasmRuntimeInstances)
 {
+    JS_COUNT_CTOR(JSRuntime);
     liveRuntimesCount++;
 
     /* Initialize infallibly first, so we can goto bad and JS_DestroyRuntime. */
 
     PodZero(&asmJSCacheOps);
     lcovOutput().init();
 }
 
 JSRuntime::~JSRuntime()
 {
+    JS_COUNT_DTOR(JSRuntime);
     MOZ_ASSERT(!initialized_);
 
     DebugOnly<size_t> oldCount = liveRuntimesCount--;
     MOZ_ASSERT(oldCount > 0);
 
     MOZ_ASSERT(wasmInstances.lock()->empty());
 }
 
diff --git a/js/xpconnect/src/nsXPConnect.cpp b/js/xpconnect/src/nsXPConnect.cpp
--- a/js/xpconnect/src/nsXPConnect.cpp
+++ b/js/xpconnect/src/nsXPConnect.cpp
@@ -114,16 +114,22 @@ nsXPConnect::~nsXPConnect()
     gSelf = nullptr;
     gOnceAliveNowDead = true;
 }
 
 // static
 void
 nsXPConnect::InitStatics()
 {
+#ifdef NS_BUILD_REFCNT_LOGGING
+    // These functions are used for reporting leaks, so we register them as early
+    // as possible to avoid missing any classes' creations.
+    js::SetLogCtorDtorFunctions(NS_LogCtor, NS_LogDtor);
+#endif
+
     gSelf = new nsXPConnect();
     gOnceAliveNowDead = false;
 
     // Initial extra ref to keep the singleton alive
     // balanced by explicit call to ReleaseXPConnectSingleton()
     NS_ADDREF(gSelf);
 
     // Fire up the SSM.
