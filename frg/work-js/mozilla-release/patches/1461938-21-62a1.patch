# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1527066315 -7200
#      Wed May 23 11:05:15 2018 +0200
# Node ID 19295db05a9244b1d65802a7756f69e966a56c88
# Parent  10df3e7394cb9491d9b6feb6125e87dabe1305e6
Bug 1461938 part 21 - Move isSelfHosting and selfHostingScriptSource from JSCompartment to JS::Realm. r=evilpie

diff --git a/js/src/jsexn.cpp b/js/src/jsexn.cpp
--- a/js/src/jsexn.cpp
+++ b/js/src/jsexn.cpp
@@ -633,20 +633,20 @@ js::GetErrorTypeName(JSContext* cx, int1
 
 void
 js::ErrorToException(JSContext* cx, JSErrorReport* reportp,
                      JSErrorCallback callback, void* userRef)
 {
     MOZ_ASSERT(reportp);
     MOZ_ASSERT(!JSREPORT_IS_WARNING(reportp->flags));
 
-    // We cannot throw a proper object inside the self-hosting compartment, as
-    // we cannot construct the Error constructor without self-hosted code. Just
+    // We cannot throw a proper object inside the self-hosting realm, as we
+    // cannot construct the Error constructor without self-hosted code. Just
     // print the error to stderr to help debugging.
-    if (cx->runtime()->isSelfHostingCompartment(cx->compartment())) {
+    if (cx->realm()->isSelfHostingRealm()) {
         PrintError(cx, stderr, JS::ConstUTF8CharsZ(), reportp, true);
         return;
     }
 
     // Find the exception index associated with this error.
     JSErrNum errorNumber = static_cast<JSErrNum>(reportp->errorNumber);
     if (!callback)
         callback = GetErrorMessage;
diff --git a/js/src/vm/JSCompartment.cpp b/js/src/vm/JSCompartment.cpp
--- a/js/src/vm/JSCompartment.cpp
+++ b/js/src/vm/JSCompartment.cpp
@@ -39,23 +39,21 @@ using namespace js;
 using namespace js::gc;
 using namespace js::jit;
 
 using mozilla::PodArrayZero;
 
 JSCompartment::JSCompartment(Zone* zone)
   : zone_(zone),
     runtime_(zone->runtimeFromAnyThread()),
-    isSelfHosting(false),
     performanceMonitoring(runtime_),
     data(nullptr),
     regExps(),
     globalWriteBarriered(0),
     detachedTypedObjects(0),
-    selfHostingScriptSource(nullptr),
     objectMetadataTable(nullptr),
     innerViews(zone),
     lazyArrayBuffers(nullptr),
     nonSyntacticLexicalEnvironments_(nullptr),
     gcIncomingGrayPointers(nullptr),
     debugModeBits(0),
     validAccessPtr(nullptr),
     randomKeyGenerator_(runtime_->forkRandomKeyGenerator()),
@@ -760,17 +758,17 @@ JSCompartment::sweepSavedStacks()
 void
 Realm::sweepGlobalObject()
 {
     if (global_ && IsAboutToBeFinalized(&global_))
         global_.set(nullptr);
 }
 
 void
-JSCompartment::sweepSelfHostingScriptSource()
+Realm::sweepSelfHostingScriptSource()
 {
     if (selfHostingScriptSource.unbarrieredGet() &&
         IsAboutToBeFinalized(&selfHostingScriptSource))
     {
         selfHostingScriptSource.set(nullptr);
     }
 }
 
diff --git a/js/src/vm/JSCompartment.h b/js/src/vm/JSCompartment.h
--- a/js/src/vm/JSCompartment.h
+++ b/js/src/vm/JSCompartment.h
@@ -550,19 +550,16 @@ class WeakMapBase;
 } // namespace js
 
 struct JSCompartment
 {
   protected:
     JS::Zone*                    zone_;
     JSRuntime*                   runtime_;
 
-  public:
-    bool                         isSelfHosting;
-
   private:
     friend struct JSRuntime;
     friend struct JSContext;
 
   public:
     js::PerformanceGroupHolder performanceMonitoring;
 
     JS::Zone* zone() { return zone_; }
@@ -626,23 +623,16 @@ struct JSCompartment
 
   public:
     // Object group tables and other state in the compartment.
     js::ObjectGroupCompartment   objectGroups;
 
 #ifdef JSGC_HASH_TABLE_CHECKS
     void checkWrapperMapAfterMovingGC();
 #endif
-
-    /*
-     * Lazily initialized script source object to use for scripts cloned
-     * from the self-hosting global.
-     */
-    js::ReadBarrieredScriptSourceObject selfHostingScriptSource;
-
     // Keep track of the metadata objects which can be associated with each JS
     // object. Both keys and values are in this compartment.
     js::ObjectWeakMap* objectMetadataTable;
 
     // Map from array buffers to views sharing that storage.
     JS::WeakCache<js::InnerViewTable> innerViews;
 
     // Inline transparent typed objects do not initially have an array buffer,
@@ -763,17 +753,16 @@ struct JSCompartment
      */
     void traceOutgoingCrossCompartmentWrappers(JSTracer* trc);
     static void traceIncomingCrossCompartmentEdgesForZoneGC(JSTracer* trc);
 
     void sweepAfterMinorGC(JSTracer* trc);
 
     void sweepCrossCompartmentWrappers();
     void sweepSavedStacks();
-    void sweepSelfHostingScriptSource();
     void sweepJitCompartment();
     void sweepRegExps();
     void sweepDebugEnvironments();
     void sweepNativeIterators();
     void sweepTemplateObjects();
 
     static void fixupCrossCompartmentWrappersAfterMovingGC(JSTracer* trc);
     void fixupAfterMovingGC();
@@ -983,31 +972,38 @@ class JS::Realm : public JSCompartment
     JS::RealmStats* realmStats_ = nullptr;
 
     const js::AllocationMetadataBuilder* allocationMetadataBuilder_ = nullptr;
     void* realmPrivate_ = nullptr;
 
     unsigned enterRealmDepth_ = 0;
 
     bool isAtomsRealm_ = false;
+    bool isSelfHostingRealm_ = false;
     bool marked_ = true;
     bool isSystem_ = false;
 
   public:
     // WebAssembly state for the realm.
     js::wasm::Realm wasm;
 
     js::DtoaCache dtoaCache;
     js::NewProxyCache newProxyCache;
     js::ArraySpeciesLookup arraySpeciesLookup;
 
     js::UniquePtr<js::ScriptCountsMap> scriptCountsMap;
     js::UniquePtr<js::ScriptNameMap> scriptNameMap;
     js::UniquePtr<js::DebugScriptMap> debugScriptMap;
 
+    /*
+     * Lazily initialized script source object to use for scripts cloned
+     * from the self-hosting global.
+     */
+    js::ReadBarrieredScriptSourceObject selfHostingScriptSource { nullptr };
+
     // Last time at which an animation was played for this realm.
     int64_t lastAnimationTime = 0;
 
     uint32_t warnedAboutStringGenericsMethods = 0;
 #ifdef DEBUG
     bool firedOnNewGlobalObject = false;
 #endif
 
@@ -1044,16 +1040,23 @@ class JS::Realm : public JSCompartment
 
     bool isAtomsRealm() const {
         return isAtomsRealm_;
     }
     void setIsAtomsRealm() {
         isAtomsRealm_ = true;
     }
 
+    bool isSelfHostingRealm() const {
+        return isSelfHostingRealm_;
+    }
+    void setIsSelfHostingRealm() {
+        isSelfHostingRealm_ = true;
+    }
+
     /* The global object for this realm.
      *
      * This returns nullptr if this is the atoms realm.  (The global_ field is
      * also null briefly during GC, after the global object is collected; but
      * when that happens the Realm is destroyed during the same GC.)
      *
      * In contrast, JSObject::global() is infallible because marking a JSObject
      * always marks its global as well.
@@ -1082,16 +1085,18 @@ class JS::Realm : public JSCompartment
      * regardless of whether the realm's global is still live.
      */
     void traceRoots(JSTracer* trc, js::gc::GCRuntime::TraceOrMarkRuntime traceOrMark);
     /*
      * This method clears out tables of roots in preparation for the final GC.
      */
     void finishRoots();
 
+    void sweepSelfHostingScriptSource();
+
     void clearScriptCounts();
     void clearScriptNames();
 
     void purge();
 
     void fixupScriptMapsAfterMovingGC();
 
 #ifdef JSGC_HASH_TABLE_CHECKS
diff --git a/js/src/vm/JSFunction.cpp b/js/src/vm/JSFunction.cpp
--- a/js/src/vm/JSFunction.cpp
+++ b/js/src/vm/JSFunction.cpp
@@ -1683,17 +1683,17 @@ JSFunction::maybeRelazify(JSRuntime* rt)
 
     // Don't relazify functions in realms that are active.
     Realm* realm = this->realm();
     if (realm->hasBeenEntered() && !rt->allowRelazificationForTesting)
         return;
 
     // The caller should have checked we're not in the self-hosting zone (it's
     // shared with worker runtimes so relazifying functions in it will race).
-    MOZ_ASSERT(!realm->isSelfHosting);
+    MOZ_ASSERT(!realm->isSelfHostingRealm());
 
     // Don't relazify if the realm is being debugged.
     if (realm->isDebuggee())
         return;
 
     // Don't relazify if the realm and/or runtime is instrumented to
     // collect code coverage for analysis.
     if (realm->collectCoverageForDebug())
@@ -2283,17 +2283,17 @@ js::CloneAsmJSModuleFunction(JSContext* 
     clone->setGroup(fun->group());
     return clone;
 }
 
 JSFunction*
 js::CloneSelfHostingIntrinsic(JSContext* cx, HandleFunction fun)
 {
     MOZ_ASSERT(fun->isNative());
-    MOZ_ASSERT(fun->compartment()->isSelfHosting);
+    MOZ_ASSERT(fun->realm()->isSelfHostingRealm());
     MOZ_ASSERT(!fun->isExtended());
     MOZ_ASSERT(cx->compartment() != fun->compartment());
 
     JSFunction* clone = NewFunctionClone(cx, fun, SingletonObject, AllocKind::FUNCTION,
                                          /* proto = */ nullptr);
     if (!clone)
         return nullptr;
 
diff --git a/js/src/vm/JSScript.cpp b/js/src/vm/JSScript.cpp
--- a/js/src/vm/JSScript.cpp
+++ b/js/src/vm/JSScript.cpp
@@ -3594,27 +3594,27 @@ static JSScript*
 CreateEmptyScriptForClone(JSContext* cx, HandleScript src)
 {
     /*
      * Wrap the script source object as needed. Self-hosted scripts may be
      * in another runtime, so lazily create a new script source object to
      * use for them.
      */
     RootedObject sourceObject(cx);
-    if (cx->runtime()->isSelfHostingCompartment(src->compartment())) {
-        if (!cx->compartment()->selfHostingScriptSource) {
+    if (src->realm()->isSelfHostingRealm()) {
+        if (!cx->realm()->selfHostingScriptSource) {
             CompileOptions options(cx);
             FillSelfHostingCompileOptions(options);
 
             ScriptSourceObject* obj = frontend::CreateScriptSourceObject(cx, options);
             if (!obj)
                 return nullptr;
-            cx->compartment()->selfHostingScriptSource.set(obj);
+            cx->realm()->selfHostingScriptSource.set(obj);
         }
-        sourceObject = cx->compartment()->selfHostingScriptSource;
+        sourceObject = cx->realm()->selfHostingScriptSource;
     } else {
         sourceObject = src->sourceObject();
         if (!cx->compartment()->wrap(cx, &sourceObject))
             return nullptr;
     }
 
     CompileOptions options(cx);
     options.setMutedErrors(src->mutedErrors())
@@ -4430,39 +4430,39 @@ JSScript::mayReadFrameArgsDirectly()
 {
     return argumentsHasVarBinding() || hasRest();
 }
 
 void
 JSScript::AutoDelazify::holdScript(JS::HandleFunction fun)
 {
     if (fun) {
-        if (fun->compartment()->isSelfHosting) {
-            // The self-hosting compartment is shared across runtimes, so we
-            // can't use JSAutoRealm: it could cause races. Functions in the
-            // self-hosting compartment will never be lazy, so we can safely
-            // assume we don't have to delazify.
+        if (fun->realm()->isSelfHostingRealm()) {
+            // The self-hosting realm is shared across runtimes, so we can't use
+            // JSAutoRealm: it could cause races. Functions in the self-hosting
+            // realm will never be lazy, so we can safely assume we don't have
+            // to delazify.
             script_ = fun->nonLazyScript();
         } else {
             JSAutoRealm ar(cx_, fun);
             script_ = JSFunction::getOrCreateScript(cx_, fun);
             if (script_) {
                 oldDoNotRelazify_ = script_->bitFields_.doNotRelazify_;
                 script_->setDoNotRelazify(true);
             }
         }
     }
 }
 
 void
 JSScript::AutoDelazify::dropScript()
 {
-    // Don't touch script_ if it's in the self-hosting compartment, see the
-    // comment in holdScript.
-    if (script_ && !script_->compartment()->isSelfHosting)
+    // Don't touch script_ if it's in the self-hosting realm, see the comment
+    // in holdScript.
+    if (script_ && !script_->realm()->isSelfHostingRealm())
         script_->setDoNotRelazify(oldDoNotRelazify_);
     script_ = nullptr;
 }
 
 JS::ubi::Node::Size
 JS::ubi::Concrete<JSScript>::size(mozilla::MallocSizeOf mallocSizeOf) const
 {
     Size size = Arena::thingSize(get().asTenured().getAllocKind());
diff --git a/js/src/vm/Runtime.h b/js/src/vm/Runtime.h
--- a/js/src/vm/Runtime.h
+++ b/js/src/vm/Runtime.h
@@ -580,17 +580,16 @@ struct JSRuntime : public js::MallocProv
     }
 
     bool initSelfHosting(JSContext* cx);
     void finishSelfHosting();
     void traceSelfHostingGlobal(JSTracer* trc);
     bool isSelfHostingGlobal(JSObject* global) {
         return global == selfHostingGlobal_;
     }
-    bool isSelfHostingCompartment(JSCompartment* comp) const;
     bool isSelfHostingZone(const JS::Zone* zone) const;
     bool createLazySelfHostedFunctionClone(JSContext* cx, js::HandlePropertyName selfHostedName,
                                            js::HandleAtom name, unsigned nargs,
                                            js::HandleObject proto,
                                            js::NewObjectKind newKind,
                                            js::MutableHandleFunction fun);
     bool cloneSelfHostedFunctionScript(JSContext* cx, js::Handle<js::PropertyName*> name,
                                        js::Handle<JSFunction*> targetFun);
diff --git a/js/src/vm/SelfHosting.cpp b/js/src/vm/SelfHosting.cpp
--- a/js/src/vm/SelfHosting.cpp
+++ b/js/src/vm/SelfHosting.cpp
@@ -2826,17 +2826,17 @@ JSRuntime::createSelfHostingGlobal(JSCon
     };
 
     AutoRealmUnchecked ar(cx, realm);
     Rooted<GlobalObject*> shg(cx, GlobalObject::createInternal(cx, &shgClass));
     if (!shg)
         return nullptr;
 
     cx->runtime()->selfHostingGlobal_ = shg;
-    realm->isSelfHosting = true;
+    realm->setIsSelfHostingRealm();
     realm->setIsSystem(true);
 
     if (!GlobalObject::initSelfHostingBuiltins(cx, shg, intrinsic_functions))
         return nullptr;
 
     JS_FireOnNewGlobalObject(cx, shg);
 
     return shg;
@@ -2998,22 +2998,16 @@ JSRuntime::finishSelfHosting()
 void
 JSRuntime::traceSelfHostingGlobal(JSTracer* trc)
 {
     if (selfHostingGlobal_ && !parentRuntime)
         TraceRoot(trc, const_cast<NativeObject**>(&selfHostingGlobal_.ref()), "self-hosting global");
 }
 
 bool
-JSRuntime::isSelfHostingCompartment(JSCompartment* comp) const
-{
-    return selfHostingGlobal_->compartment() == comp;
-}
-
-bool
 JSRuntime::isSelfHostingZone(const JS::Zone* zone) const
 {
     return selfHostingGlobal_ && selfHostingGlobal_->zoneFromAnyThread() == zone;
 }
 
 static bool
 CloneValue(JSContext* cx, HandleValue selfHostedValue, MutableHandleValue vp);
 
