# HG changeset patch
# User Mike Conley <mconley@mozilla.com>
# Date 1504718928 14400
#      Wed Sep 06 13:28:48 2017 -0400
# Node ID ad5c7dbaa4d6c2f31c3b5a89c159f781a2f92902
# Parent  1c0f9d069aded626bdd92b80996470ea778c8d9d
Bug 1387130 - Use original tabstrip scrolling behaviour when using scrollbuttons. r=dao

In bug 1356705, we switched scrollbox to use CSS smooth scroll when
the scrollbox is configured to scroll smoothly. This caused the tab
strip to scroll with a "pulse" when using the arrow scrollbuttons.
This is because we scroll by a single tab each time, as opposed to
scrolling by pixels.

This reverts part of bug 1356705 so that we use instant scrolling
instead of smooth scrolling in the scrollbuttons case, which returns
the original behaviour of the strip without the pulse.

MozReview-Commit-ID: D8QQ8kQ7AjM

diff --git a/toolkit/content/widgets/scrollbox.xml b/toolkit/content/widgets/scrollbox.xml
--- a/toolkit/content/widgets/scrollbox.xml
+++ b/toolkit/content/widgets/scrollbox.xml
@@ -655,24 +655,52 @@
           if (!document)
             aTimer.cancel();
 
           this.scrollByIndex(this._scrollIndex);
         ]]>
         </body>
       </method>
 
+      <field name="_arrowScrollAnim"><![CDATA[({
+        scrollbox: this,
+        requestHandle: 0, /* 0 indicates there is no pending request */
+        start: function arrowSmoothScroll_start() {
+          this.lastFrameTime = window.performance.now();
+          if (!this.requestHandle)
+            this.requestHandle = window.requestAnimationFrame(this.sample.bind(this));
+        },
+        stop: function arrowSmoothScroll_stop() {
+          window.cancelAnimationFrame(this.requestHandle);
+          this.requestHandle = 0;
+        },
+        sample: function arrowSmoothScroll_handleEvent(timeStamp) {
+          const scrollIndex = this.scrollbox._scrollIndex;
+          const timePassed = timeStamp - this.lastFrameTime;
+          this.lastFrameTime = timeStamp;
+
+          const scrollDelta = 0.5 * timePassed * scrollIndex;
+          this.scrollbox.scrollByPixels(scrollDelta, true);
+          this.requestHandle = window.requestAnimationFrame(this.sample.bind(this));
+        }
+      })]]></field>
+
       <method name="_startScroll">
         <parameter name="index"/>
         <body><![CDATA[
           if (this._isRTLScrollbox)
             index *= -1;
           this._scrollIndex = index;
           this._mousedown = true;
 
+          if (this.smoothScroll) {
+            this._arrowScrollAnim.start();
+            return;
+          }
+
           if (!this._scrollTimer)
             this._scrollTimer =
               Components.classes["@mozilla.org/timer;1"]
                         .createInstance(Components.interfaces.nsITimer);
           else
             this._scrollTimer.cancel();
 
           this._scrollTimer.initWithCallback(this, this._scrollDelay,
@@ -682,17 +710,23 @@
         </body>
       </method>
 
       <method name="_stopScroll">
         <body><![CDATA[
           if (this._scrollTimer)
             this._scrollTimer.cancel();
           this._mousedown = false;
+          if (!this._scrollIndex || !this.smoothScroll)
+            return;
+
+          this.scrollByIndex(this._scrollIndex);
           this._scrollIndex = 0;
+
+          this._arrowScrollAnim.stop();
         ]]></body>
       </method>
 
       <method name="_pauseScroll">
         <body><![CDATA[
           if (this._mousedown) {
             this._stopScroll();
             this._mousedown = true;
