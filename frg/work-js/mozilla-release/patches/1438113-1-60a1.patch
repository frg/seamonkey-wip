# HG changeset patch
# User Lars T Hansen <lhansen@mozilla.com>
# Date 1518599130 -3600
#      Wed Feb 14 10:05:30 2018 +0100
# Node ID 6e46ff4ff446e5548326e9d3468485c15934e088
# Parent  b24d7a6d720a9035690114de4b89badfb1bdf700
Bug 1438113 - Make it possible to have wasm baseline without ion. r=luke

For ARM64 it will be the case initially that we'll have baseline but
not ion.  Thus we need to be able to disable asm.js support in some
cases when wasm is supported (since baseline does not support asm.js)
and to make the logic that selects the compiler to use slightly more
subtle.

As a result the old logic of "just use Ion as a fallback" needs to go,
but we can now move that into HasCompilerSupport() and the result is
cleaner overall.

diff --git a/js/src/wasm/AsmJS.cpp b/js/src/wasm/AsmJS.cpp
--- a/js/src/wasm/AsmJS.cpp
+++ b/js/src/wasm/AsmJS.cpp
@@ -37,16 +37,17 @@
 #include "vm/ErrorReporting.h"
 #include "vm/SelfHosting.h"
 #include "vm/StringBuffer.h"
 #include "vm/Time.h"
 #include "vm/TypedArrayObject.h"
 #include "wasm/WasmCompile.h"
 #include "wasm/WasmGenerator.h"
 #include "wasm/WasmInstance.h"
+#include "wasm/WasmIonCompile.h"
 #include "wasm/WasmJS.h"
 #include "wasm/WasmSerialize.h"
 #include "wasm/WasmValidate.h"
 
 #include "frontend/ParseNode-inl.h"
 #include "vm/ArrayBufferObject-inl.h"
 #include "vm/JSObject-inl.h"
 
@@ -8721,17 +8722,18 @@ TypeFailureWarning(AsmJSParser& parser, 
     // the return value below.
     Unused << parser.warningNoOffset(JSMSG_USE_ASM_TYPE_FAIL, str ? str : "");
     return false;
 }
 
 static bool
 EstablishPreconditions(JSContext* cx, AsmJSParser& parser)
 {
-    if (!HasCompilerSupport(cx))
+    // asm.js requires Ion.
+    if (!HasCompilerSupport(cx) || !IonCanCompile())
         return TypeFailureWarning(parser, "Disabled by lack of compiler support");
 
     switch (parser.options().asmJSOption) {
       case AsmJSOption::Disabled:
         return TypeFailureWarning(parser, "Disabled by 'asmjs' runtime option");
       case AsmJSOption::DisabledByDebugger:
         return TypeFailureWarning(parser, "Disabled by debugger");
       case AsmJSOption::Enabled:
@@ -8903,17 +8905,17 @@ js::IsAsmJSStrictModeModuleOrFunction(JS
 }
 
 bool
 js::IsAsmJSCompilationAvailable(JSContext* cx, unsigned argc, Value* vp)
 {
     CallArgs args = CallArgsFromVp(argc, vp);
 
     // See EstablishPreconditions.
-    bool available = HasCompilerSupport(cx) && cx->options().asmJS();
+    bool available = HasCompilerSupport(cx) && IonCanCompile() && cx->options().asmJS();
 
     args.rval().set(BooleanValue(available));
     return true;
 }
 
 static JSFunction*
 MaybeWrappedNativeFunction(const Value& v)
 {
diff --git a/js/src/wasm/WasmCompile.cpp b/js/src/wasm/WasmCompile.cpp
--- a/js/src/wasm/WasmCompile.cpp
+++ b/js/src/wasm/WasmCompile.cpp
@@ -20,16 +20,17 @@
 
 #include "mozilla/Maybe.h"
 #include "mozilla/Unused.h"
 
 #include "jit/ProcessExecutableMemory.h"
 #include "wasm/WasmBaselineCompile.h"
 #include "wasm/WasmBinaryIterator.h"
 #include "wasm/WasmGenerator.h"
+#include "wasm/WasmIonCompile.h"
 #include "wasm/WasmSignalHandlers.h"
 #include "wasm/WasmValidate.h"
 
 using namespace js;
 using namespace js::jit;
 using namespace js::wasm;
 
 template <class DecoderT>
@@ -387,19 +388,23 @@ InitialCompileFlags(const CompileArgs& a
                     DebugEnabled* debug)
 {
     uint32_t codeSectionSize = 0;
 
     SectionRange range;
     if (StartsCodeSection(d.begin(), d.end(), &range))
         codeSectionSize = range.size;
 
+    // Attempt to default to ion if baseline is disabled.
     bool baselineEnabled = BaselineCanCompile() && (args.baselineEnabled || args.testTiering);
     bool debugEnabled = BaselineCanCompile() && args.debugEnabled;
-    bool ionEnabled = args.ionEnabled || !baselineEnabled || args.testTiering;
+    bool ionEnabled = IonCanCompile() && (args.ionEnabled || !baselineEnabled || args.testTiering);
+
+    // HasCompilerSupport() should prevent failure here
+    MOZ_RELEASE_ASSERT(baselineEnabled || ionEnabled);
 
     if (baselineEnabled && ionEnabled && !debugEnabled &&
         (TieringBeneficial(codeSectionSize) || args.testTiering))
     {
         *mode = CompileMode::Tier1;
         *tier = Tier::Baseline;
     } else {
         *mode = CompileMode::Once;
diff --git a/js/src/wasm/WasmIonCompile.cpp b/js/src/wasm/WasmIonCompile.cpp
--- a/js/src/wasm/WasmIonCompile.cpp
+++ b/js/src/wasm/WasmIonCompile.cpp
@@ -4437,8 +4437,18 @@ wasm::IonCompileFunctions(const ModuleEn
     }
 
     masm.finish();
     if (masm.oom())
         return false;
 
     return code->swap(masm);
 }
+
+bool
+js::wasm::IonCanCompile()
+{
+#if !defined(JS_CODEGEN_NONE) && !defined(JS_CODEGEN_ARM64)
+    return true;
+#else
+    return false;
+#endif
+}
diff --git a/js/src/wasm/WasmIonCompile.h b/js/src/wasm/WasmIonCompile.h
--- a/js/src/wasm/WasmIonCompile.h
+++ b/js/src/wasm/WasmIonCompile.h
@@ -21,16 +21,20 @@
 
 #include "mozilla/Attributes.h"
 
 #include "wasm/WasmGenerator.h"
 
 namespace js {
 namespace wasm {
 
+// Return whether IonCompileFunction() can generate code on the current device.
+bool
+IonCanCompile();
+
 // Generates very fast code at the expense of compilation time.
 MOZ_MUST_USE bool
 IonCompileFunctions(const ModuleEnvironment& env, LifoAlloc& lifo,
                     const FuncCompileInputVector& inputs, CompiledCode* code,
                     UniqueChars* error);
 
 } // namespace wasm
 } // namespace js
diff --git a/js/src/wasm/WasmJS.cpp b/js/src/wasm/WasmJS.cpp
--- a/js/src/wasm/WasmJS.cpp
+++ b/js/src/wasm/WasmJS.cpp
@@ -80,17 +80,18 @@ wasm::HasCompilerSupport(JSContext* cx)
 #ifdef JS_SIMULATOR
     if (!Simulator::supportsAtomics())
         return false;
 #endif
 
 #if defined(JS_CODEGEN_NONE) || defined(JS_CODEGEN_ARM64)
     return false;
 #else
-    return true;
+    return (cx->options().wasmBaseline() && BaselineCanCompile()) ||
+           (cx->options().wasmIon() && IonCanCompile());
 #endif
 }
 
 bool
 wasm::HasSupport(JSContext* cx)
 {
     return cx->options().wasm() && HasCompilerSupport(cx);
 }
