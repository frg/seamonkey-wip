# HG changeset patch
# User Sylvestre Ledru <sledru@mozilla.com>
# Date 1556700430 0
# Node ID e1993a1f09ac53cd1a04fdf6a87f8cad8e44f73e
# Parent  46a7f55e9e0836a254f9d050046a115b419464b5
Bug 1547143 - Format the tree: Be prescriptive with the pointer style (left) r=Ehsan

# ignore-this-changeset

Depends on D28954

Differential Revision: https://phabricator.services.mozilla.com/D28956

diff --git a/toolkit/crashreporter/LoadLibraryRemote.cpp b/toolkit/crashreporter/LoadLibraryRemote.cpp
--- a/toolkit/crashreporter/LoadLibraryRemote.cpp
+++ b/toolkit/crashreporter/LoadLibraryRemote.cpp
@@ -17,17 +17,17 @@
 #include <winnt.h>
 #include <stdlib.h>
 #ifdef DEBUG_OUTPUT
 #  include <stdio.h>
 #endif
 
 #include "nsWindowsHelpers.h"
 
-typedef const unsigned char *FileView;
+typedef const unsigned char* FileView;
 
 template <>
 class nsAutoRefTraits<FileView> {
  public:
   typedef FileView RawRef;
   static FileView Void() { return nullptr; }
 
   static void Release(RawRef aView) {
@@ -39,65 +39,65 @@ class nsAutoRefTraits<FileView> {
 // Vista SDKs no longer define IMAGE_SIZEOF_BASE_RELOCATION!?
 #  define IMAGE_SIZEOF_BASE_RELOCATION (sizeof(IMAGE_BASE_RELOCATION))
 #endif
 
 #include "LoadLibraryRemote.h"
 
 typedef struct {
   PIMAGE_NT_HEADERS headers;
-  unsigned char *localCodeBase;
-  unsigned char *remoteCodeBase;
-  HMODULE *modules;
+  unsigned char* localCodeBase;
+  unsigned char* remoteCodeBase;
+  HMODULE* modules;
   int numModules;
 } MEMORYMODULE, *PMEMORYMODULE;
 
-typedef BOOL(WINAPI *DllEntryProc)(HINSTANCE hinstDLL, DWORD fdwReason,
+typedef BOOL(WINAPI* DllEntryProc)(HINSTANCE hinstDLL, DWORD fdwReason,
                                    LPVOID lpReserved);
 
 #define GET_HEADER_DICTIONARY(module, idx) \
   &(module)->headers->OptionalHeader.DataDirectory[idx]
 
 #ifdef DEBUG_OUTPUT
-static void OutputLastError(const char *msg) {
-  char *tmp;
-  char *tmpmsg;
+static void OutputLastError(const char* msg) {
+  char* tmp;
+  char* tmpmsg;
   FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM |
                      FORMAT_MESSAGE_IGNORE_INSERTS,
                  nullptr, GetLastError(),
                  MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR) &tmp, 0,
                  nullptr);
-  tmpmsg = (char *)LocalAlloc(LPTR, strlen(msg) + strlen(tmp) + 3);
+  tmpmsg = (char*)LocalAlloc(LPTR, strlen(msg) + strlen(tmp) + 3);
   sprintf(tmpmsg, "%s: %s", msg, tmp);
   OutputDebugStringA(tmpmsg);
   LocalFree(tmpmsg);
   LocalFree(tmp);
 }
 #endif
 
-static void CopySections(const unsigned char *data,
+static void CopySections(const unsigned char* data,
                          PIMAGE_NT_HEADERS old_headers, PMEMORYMODULE module) {
   int i;
-  unsigned char *codeBase = module->localCodeBase;
-  unsigned char *dest;
+  unsigned char* codeBase = module->localCodeBase;
+  unsigned char* dest;
   PIMAGE_SECTION_HEADER section = IMAGE_FIRST_SECTION(module->headers);
   for (i = 0; i < module->headers->FileHeader.NumberOfSections;
        i++, section++) {
     dest = codeBase + section->VirtualAddress;
     memset(dest, 0, section->Misc.VirtualSize);
     if (section->SizeOfRawData) {
       memcpy(dest, data + section->PointerToRawData, section->SizeOfRawData);
     }
     // section->Misc.PhysicalAddress = (POINTER_TYPE) module->remoteCodeBase +
     //                                                section->VirtualAddress;
   }
 }
 
-static bool CopyRegion(HANDLE hRemoteProcess, void *remoteAddress,
-                       void *localAddress, DWORD size, DWORD protect) {
+static bool CopyRegion(HANDLE hRemoteProcess, void* remoteAddress,
+                       void* localAddress, DWORD size, DWORD protect) {
   if (size > 0) {
     // Copy the data from local->remote and set the memory protection
     if (!VirtualAllocEx(hRemoteProcess, remoteAddress, size, MEM_COMMIT,
                         PAGE_READWRITE))
       return false;
 
     if (!WriteProcessMemory(hRemoteProcess, remoteAddress, localAddress, size,
                             nullptr)) {
@@ -158,18 +158,18 @@ static bool FinalizeSections(PMEMORYMODU
     int writeable = (section->Characteristics & IMAGE_SCN_MEM_WRITE) != 0;
 
     // determine protection flags based on characteristics
     protect = ProtectionFlags[executable][readable][writeable];
     if (section->Characteristics & IMAGE_SCN_MEM_NOT_CACHED) {
       protect |= PAGE_NOCACHE;
     }
 
-    void *remoteAddress = module->remoteCodeBase + section->VirtualAddress;
-    void *localAddress = module->localCodeBase + section->VirtualAddress;
+    void* remoteAddress = module->remoteCodeBase + section->VirtualAddress;
+    void* localAddress = module->localCodeBase + section->VirtualAddress;
 
     // determine size of region
     size = section->Misc.VirtualSize;
 #ifdef DEBUG_OUTPUT
     fprintf(stderr,
             "Copying section %s to %p, size %x, executable %i readable %i "
             "writeable %i\n",
             section->Name, remoteAddress, size, executable, readable,
@@ -178,112 +178,111 @@ static bool FinalizeSections(PMEMORYMODU
     if (!CopyRegion(hRemoteProcess, remoteAddress, localAddress, size, protect))
       return false;
   }
   return true;
 }
 
 static void PerformBaseRelocation(PMEMORYMODULE module, SIZE_T delta) {
   DWORD i;
-  unsigned char *codeBase = module->localCodeBase;
+  unsigned char* codeBase = module->localCodeBase;
 
   PIMAGE_DATA_DIRECTORY directory =
       GET_HEADER_DICTIONARY(module, IMAGE_DIRECTORY_ENTRY_BASERELOC);
   if (directory->Size > 0) {
     PIMAGE_BASE_RELOCATION relocation =
         (PIMAGE_BASE_RELOCATION)(codeBase + directory->VirtualAddress);
     for (; relocation->VirtualAddress > 0;) {
-      unsigned char *dest = codeBase + relocation->VirtualAddress;
-      unsigned short *relInfo =
-          (unsigned short *)((unsigned char *)relocation +
-                             IMAGE_SIZEOF_BASE_RELOCATION);
+      unsigned char* dest = codeBase + relocation->VirtualAddress;
+      unsigned short* relInfo = (unsigned short*)((unsigned char*)relocation +
+                                                  IMAGE_SIZEOF_BASE_RELOCATION);
       for (i = 0;
            i < ((relocation->SizeOfBlock - IMAGE_SIZEOF_BASE_RELOCATION) / 2);
            i++, relInfo++) {
-        DWORD *patchAddrHL;
+        DWORD* patchAddrHL;
 #ifdef _WIN64
-        ULONGLONG *patchAddr64;
+        ULONGLONG* patchAddr64;
 #endif
         int type, offset;
 
         // the upper 4 bits define the type of relocation
         type = *relInfo >> 12;
         // the lower 12 bits define the offset
         offset = *relInfo & 0xfff;
 
         switch (type) {
           case IMAGE_REL_BASED_ABSOLUTE:
             // skip relocation
             break;
 
           case IMAGE_REL_BASED_HIGHLOW:
             // change complete 32 bit address
-            patchAddrHL = (DWORD *)(dest + offset);
+            patchAddrHL = (DWORD*)(dest + offset);
             *patchAddrHL += delta;
             break;
 
 #ifdef _WIN64
           case IMAGE_REL_BASED_DIR64:
-            patchAddr64 = (ULONGLONG *)(dest + offset);
+            patchAddr64 = (ULONGLONG*)(dest + offset);
             *patchAddr64 += delta;
             break;
 #endif
 
           default:
             // printf("Unknown relocation: %d\n", type);
             break;
         }
       }
 
       // advance to next relocation block
-      relocation = (PIMAGE_BASE_RELOCATION)(((char *)relocation) +
+      relocation = (PIMAGE_BASE_RELOCATION)(((char*)relocation) +
                                             relocation->SizeOfBlock);
     }
   }
 }
 
 static int BuildImportTable(PMEMORYMODULE module) {
   int result = 1;
-  unsigned char *codeBase = module->localCodeBase;
+  unsigned char* codeBase = module->localCodeBase;
 
   PIMAGE_DATA_DIRECTORY directory =
       GET_HEADER_DICTIONARY(module, IMAGE_DIRECTORY_ENTRY_IMPORT);
   if (directory->Size > 0) {
     PIMAGE_IMPORT_DESCRIPTOR importDesc =
         (PIMAGE_IMPORT_DESCRIPTOR)(codeBase + directory->VirtualAddress);
     PIMAGE_IMPORT_DESCRIPTOR importEnd = (PIMAGE_IMPORT_DESCRIPTOR)(
         codeBase + directory->VirtualAddress + directory->Size);
 
     for (; importDesc < importEnd && importDesc->Name; importDesc++) {
-      POINTER_TYPE *thunkRef;
-      FARPROC *funcRef;
+      POINTER_TYPE* thunkRef;
+      FARPROC* funcRef;
       HMODULE handle = GetModuleHandleA((LPCSTR)(codeBase + importDesc->Name));
       if (handle == nullptr) {
 #if DEBUG_OUTPUT
         OutputLastError("Can't load library");
 #endif
         result = 0;
         break;
       }
 
-      module->modules = (HMODULE *)realloc(
+      module->modules = (HMODULE*)realloc(
           module->modules, (module->numModules + 1) * (sizeof(HMODULE)));
       if (module->modules == nullptr) {
         result = 0;
         break;
       }
 
       module->modules[module->numModules++] = handle;
       if (importDesc->OriginalFirstThunk) {
-        thunkRef = (POINTER_TYPE *)(codeBase + importDesc->OriginalFirstThunk);
-        funcRef = (FARPROC *)(codeBase + importDesc->FirstThunk);
+        thunkRef = (POINTER_TYPE*)(codeBase + importDesc->OriginalFirstThunk);
+        funcRef = (FARPROC*)(codeBase + importDesc->FirstThunk);
       } else {
         // no hint table
-        thunkRef = (POINTER_TYPE *)(codeBase + importDesc->FirstThunk);
-        funcRef = (FARPROC *)(codeBase + importDesc->FirstThunk);
+        thunkRef = (POINTER_TYPE*)(codeBase + importDesc->FirstThunk);
+        funcRef = (FARPROC*)(codeBase + importDesc->FirstThunk);
       }
       for (; *thunkRef; thunkRef++, funcRef++) {
         if (IMAGE_SNAP_BY_ORDINAL(*thunkRef)) {
           *funcRef =
               (FARPROC)GetProcAddress(handle, (LPCSTR)IMAGE_ORDINAL(*thunkRef));
         } else {
           PIMAGE_IMPORT_BY_NAME thunkData =
               (PIMAGE_IMPORT_BY_NAME)(codeBase + (*thunkRef));
@@ -299,20 +298,20 @@ static int BuildImportTable(PMEMORYMODUL
         break;
       }
     }
   }
 
   return result;
 }
 
-static void *MemoryGetProcAddress(PMEMORYMODULE module, const char *name);
+static void* MemoryGetProcAddress(PMEMORYMODULE module, const char* name);
 
-void *LoadRemoteLibraryAndGetAddress(HANDLE hRemoteProcess,
-                                     const WCHAR *library, const char *symbol) {
+void* LoadRemoteLibraryAndGetAddress(HANDLE hRemoteProcess,
+                                     const WCHAR* library, const char* symbol) {
   // Map the DLL into memory
   nsAutoHandle hLibrary(CreateFile(library, GENERIC_READ, FILE_SHARE_READ,
                                    nullptr, OPEN_EXISTING,
                                    FILE_ATTRIBUTE_NORMAL, nullptr));
   if (INVALID_HANDLE_VALUE == hLibrary) {
 #if DEBUG_OUTPUT
     OutputLastError("Couldn't CreateFile the library.\n");
 #endif
@@ -324,17 +323,17 @@ void *LoadRemoteLibraryAndGetAddress(HAN
   if (!hMapping) {
 #if DEBUG_OUTPUT
     OutputLastError("Couldn't CreateFileMapping.\n");
 #endif
     return nullptr;
   }
 
   nsAutoRef<FileView> data(
-      (const unsigned char *)MapViewOfFile(hMapping, FILE_MAP_READ, 0, 0, 0));
+      (const unsigned char*)MapViewOfFile(hMapping, FILE_MAP_READ, 0, 0, 0));
   if (!data) {
 #if DEBUG_OUTPUT
     OutputLastError("Couldn't MapViewOfFile.\n");
 #endif
     return nullptr;
   }
 
   SIZE_T locationDelta;
@@ -352,26 +351,26 @@ void *LoadRemoteLibraryAndGetAddress(HAN
   if (old_header->Signature != IMAGE_NT_SIGNATURE) {
 #if DEBUG_OUTPUT
     OutputDebugStringA("No PE header found.\n");
 #endif
     return nullptr;
   }
 
   // reserve memory for image of library in this process and the target process
-  unsigned char *localCode = (unsigned char *)VirtualAlloc(
+  unsigned char* localCode = (unsigned char*)VirtualAlloc(
       nullptr, old_header->OptionalHeader.SizeOfImage, MEM_RESERVE | MEM_COMMIT,
       PAGE_READWRITE);
   if (!localCode) {
 #if DEBUG_OUTPUT
     OutputLastError("Can't reserve local memory.");
 #endif
   }
 
-  unsigned char *remoteCode = (unsigned char *)VirtualAllocEx(
+  unsigned char* remoteCode = (unsigned char*)VirtualAllocEx(
       hRemoteProcess, nullptr, old_header->OptionalHeader.SizeOfImage,
       MEM_RESERVE, PAGE_EXECUTE_READ);
   if (!remoteCode) {
 #if DEBUG_OUTPUT
     OutputLastError("Can't reserve remote memory.");
 #endif
   }
 
@@ -408,41 +407,41 @@ void *LoadRemoteLibraryAndGetAddress(HAN
   // sections that are marked as "discardable"
   if (!FinalizeSections(&result, hRemoteProcess)) {
     return nullptr;
   }
 
   return MemoryGetProcAddress(&result, symbol);
 }
 
-static void *MemoryGetProcAddress(PMEMORYMODULE module, const char *name) {
-  unsigned char *localCodeBase = module->localCodeBase;
+static void* MemoryGetProcAddress(PMEMORYMODULE module, const char* name) {
+  unsigned char* localCodeBase = module->localCodeBase;
   int idx = -1;
   DWORD i, *nameRef;
-  WORD *ordinal;
+  WORD* ordinal;
   PIMAGE_EXPORT_DIRECTORY exports;
   PIMAGE_DATA_DIRECTORY directory =
       GET_HEADER_DICTIONARY(module, IMAGE_DIRECTORY_ENTRY_EXPORT);
   if (directory->Size == 0) {
     // no export table found
     return nullptr;
   }
 
   exports =
       (PIMAGE_EXPORT_DIRECTORY)(localCodeBase + directory->VirtualAddress);
   if (exports->NumberOfNames == 0 || exports->NumberOfFunctions == 0) {
     // DLL doesn't export anything
     return nullptr;
   }
 
   // search function name in list of exported names
-  nameRef = (DWORD *)(localCodeBase + exports->AddressOfNames);
-  ordinal = (WORD *)(localCodeBase + exports->AddressOfNameOrdinals);
+  nameRef = (DWORD*)(localCodeBase + exports->AddressOfNames);
+  ordinal = (WORD*)(localCodeBase + exports->AddressOfNameOrdinals);
   for (i = 0; i < exports->NumberOfNames; i++, nameRef++, ordinal++) {
-    if (stricmp(name, (const char *)(localCodeBase + (*nameRef))) == 0) {
+    if (stricmp(name, (const char*)(localCodeBase + (*nameRef))) == 0) {
       idx = *ordinal;
       break;
     }
   }
 
   if (idx == -1) {
     // exported symbol not found
     return nullptr;
@@ -450,10 +449,10 @@ static void *MemoryGetProcAddress(PMEMOR
 
   if ((DWORD)idx > exports->NumberOfFunctions) {
     // name <-> ordinal number don't match
     return nullptr;
   }
 
   // AddressOfFunctions contains the RVAs to the "real" functions
   return module->remoteCodeBase +
-         (*(DWORD *)(localCodeBase + exports->AddressOfFunctions + (idx * 4)));
+         (*(DWORD*)(localCodeBase + exports->AddressOfFunctions + (idx * 4)));
 }
diff --git a/toolkit/crashreporter/rust/rust_demangle.h b/toolkit/crashreporter/rust/rust_demangle.h
--- a/toolkit/crashreporter/rust/rust_demangle.h
+++ b/toolkit/crashreporter/rust/rust_demangle.h
@@ -1,15 +1,15 @@
 #ifndef __RUST_DEMANGLE_H__
 #define __RUST_DEMANGLE_H__
 
 #ifdef __cplusplus
 extern "C" {
 #endif
 
-extern char *rust_demangle(const char *);
-extern void free_rust_demangled_name(char *);
+extern char* rust_demangle(const char*);
+extern void free_rust_demangled_name(char*);
 
 #ifdef __cplusplus
 }
 #endif
 
 #endif /* __RUST_DEMANGLE_H__ */
