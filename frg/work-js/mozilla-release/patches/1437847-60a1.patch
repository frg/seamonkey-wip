# HG changeset patch
# User Michael Ratcliffe <mratcliffe@mozilla.com>
# Date 1519828807 0
# Node ID ce41e79ade937c7a5d8c06a0cb89063f9aae5261
# Parent  a9eb1163066670fc5fd6abe093d66110173647e1
Bug 1437847 - Enable browser_console_error_source_click.js in new frontend r=jdescottes

MozReview-Commit-ID: E3WBqjwoUdH

diff --git a/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini b/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini
--- a/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini
+++ b/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini
@@ -171,17 +171,16 @@ skip-if = true # Bug 1437807
 [browser_console_clear_method.js]
 skip-if = true # Bug 1437843
 [browser_console_consolejsm_output.js]
 skip-if = true # Bug 1437844
 [browser_console_context_menu_entries.js]
 [browser_console_dead_objects.js]
 skip-if = true # Bug 1437845
 [browser_console_error_source_click.js]
-skip-if = true # Bug 1437847
 [browser_console_filters.js]
 [browser_console_nsiconsolemessage.js]
 [browser_console_open_or_focus.js]
 [browser_console_restore.js]
 [browser_console_webconsole_ctrlw_close_tab.js]
 [browser_console_webconsole_iframe_messages.js]
 [browser_console_webconsole_private_browsing.js]
 skip-if = true #       Bug 1403188
diff --git a/devtools/client/webconsole/new-console-output/test/mochitest/browser_console_error_source_click.js b/devtools/client/webconsole/new-console-output/test/mochitest/browser_console_error_source_click.js
--- a/devtools/client/webconsole/new-console-output/test/mochitest/browser_console_error_source_click.js
+++ b/devtools/client/webconsole/new-console-output/test/mochitest/browser_console_error_source_click.js
@@ -1,79 +1,58 @@
 /* -*- indent-tabs-mode: nil; js-indent-level: 2 -*- */
 /* vim: set ft=javascript ts=2 et sw=2 tw=80: */
 /* Any copyright is dedicated to the Public Domain.
  * http://creativecommons.org/publicdomain/zero/1.0/ */
 
+/* import-globals-from head.js */
+
 // Check that JS errors and CSS warnings open view source when their source link
-// is clicked in the Browser Console. See bug 877778.
+// is clicked in the Browser Console.
 
 "use strict";
 
-const TEST_URI = "data:text/html;charset=utf8,<p>hello world from bug 877778 " +
+const TEST_URI = "data:text/html;charset=utf8,<p>hello world" +
                  "<button onclick='foobar.explode()' " +
                  "style='test-color: green-please'>click!</button>";
 
-add_task(function* () {
-  yield new Promise(resolve => {
-    SpecialPowers.pushPrefEnv({"set": [
-      ["devtools.browserconsole.filter.cssparser", true]
-    ]}, resolve);
+add_task(async function () {
+  await addTab(TEST_URI);
+  let hud = await HUDService.toggleBrowserConsole();
+  ok(hud, "browser console opened");
+
+  // Enable CSS warnings and errors.
+  await setFilterState(hud, {
+    css: true
   });
 
-  yield loadTab(TEST_URI);
-  let hud = yield HUDService.toggleBrowserConsole();
-  ok(hud, "browser console opened");
-
   // On e10s, the exception is triggered in child process
   // and is ignored by test harness
   if (!Services.appinfo.browserTabsRemoteAutostart) {
     expectUncaughtException();
   }
 
   info("generate exception and wait for the message");
-  ContentTask.spawn(gBrowser.selectedBrowser, {}, function* () {
+  ContentTask.spawn(gBrowser.selectedBrowser, {}, () => {
     let button = content.document.querySelector("button");
     button.click();
   });
 
-  let results = yield waitForMessages({
-    webconsole: hud,
-    messages: [
-      {
-        text: "ReferenceError: foobar is not defined",
-        category: CATEGORY_JS,
-        severity: SEVERITY_ERROR,
-      },
-      {
-        text: "Unknown property \u2018test-color\u2019",
-        category: CATEGORY_CSS,
-        severity: SEVERITY_WARNING,
-      },
-    ],
-  });
-
-  let viewSourceCalled = false;
+  await waitForMessageAndViewSource(hud,
+    "ReferenceError: foobar is not defined");
+  await waitForMessageAndViewSource(hud,
+    "Unknown property \u2018test-color\u2019.");
+  await resetFilters(hud);
+});
 
-  let viewSource = hud.viewSource;
-  hud.viewSource = () => {
-    viewSourceCalled = true;
-  };
+async function waitForMessageAndViewSource(hud, message) {
+  let msg = await waitFor(() => findMessage(hud, message));
+  ok(msg, `Message found: "${message}"`);
 
-  for (let result of results) {
-    viewSourceCalled = false;
+  let locationNode = msg.querySelector(".message-location .frame-link-source");
+  ok(locationNode, "Message location link element found");
 
-    let msg = [...result.matched][0];
-    ok(msg, "message element found for: " + result.text);
-    ok(!msg.classList.contains("filtered-by-type"), "message element is not filtered");
-    let selector = ".message .message-location .frame-link-source";
-    let locationNode = msg.querySelector(selector);
-    ok(locationNode, "message location element found");
-
-    locationNode.click();
-
-    ok(viewSourceCalled, "view source opened");
-  }
-
-  hud.viewSource = viewSource;
-
-  yield finishTest();
-});
+  let onTabOpen = BrowserTestUtils.waitForNewTab(gBrowser, null, true);
+  locationNode.click();
+  let newTab = await onTabOpen;
+  ok(true, "The view source tab was opened in response to clicking the link");
+  await BrowserTestUtils.removeTab(newTab);
+}
