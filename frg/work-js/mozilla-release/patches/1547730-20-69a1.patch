# HG changeset patch
# User Justin Wood <Callek@gmail.com>
# Date 1559053714 0
# Node ID d89d2754488ec74c628efa4da471f8497405ce01
# Parent  768d2d842b8fe0d1a77c5a1fdff10892ef390f8a
Bug 1547730 - Use new inspection methods introduced in py3 but work in py2.7 for functions r=glandium

Differential Revision: https://phabricator.services.mozilla.com/D28112

diff --git a/python/mozbuild/mozbuild/configure/__init__.py b/python/mozbuild/mozbuild/configure/__init__.py
--- a/python/mozbuild/mozbuild/configure/__init__.py
+++ b/python/mozbuild/mozbuild/configure/__init__.py
@@ -1042,17 +1042,17 @@ class ConfigureSandbox(dict):
         '''
         if not inspect.isfunction(func):
             raise TypeError("Unexpected type: '%s'" % type(func).__name__)
         if func in self._prepared_functions:
             return func
 
         glob = SandboxedGlobal(
             (k, v)
-            for k, v in six.iteritems(func.func_globals)
+            for k, v in six.iteritems(func.__globals__)
             if (inspect.isfunction(v) and v not in self._templates) or (
                 inspect.isclass(v) and issubclass(v, Exception))
         )
         glob.update(
             __builtins__=self.BUILTINS,
             __file__=self._paths[-1] if self._paths else '',
             __name__=self._paths[-1] if self._paths else '',
             os=self.OS,
@@ -1065,30 +1065,30 @@ class ConfigureSandbox(dict):
         # order will always be the same for a given function, and if it uses
         # variables from a closure that are changed after the function is
         # declared, depending when the function is executed, the value of the
         # variable can differ. For consistency, we force the function to use
         # the value from the earliest it can be run, which is at declaration.
         # Note this is not entirely bullet proof (if the value is e.g. a list,
         # the list contents could have changed), but covers the bases.
         closure = None
-        if func.func_closure:
+        if func.__closure__:
             def makecell(content):
                 def f():
                     content
-                return f.func_closure[0]
+                return f.__closure__[0]
 
             closure = tuple(makecell(cell.cell_contents)
-                            for cell in func.func_closure)
+                            for cell in func.__closure__)
 
         new_func = self.wraps(func)(types.FunctionType(
-            func.func_code,
+            func.__code__,
             glob,
             func.__name__,
-            func.func_defaults,
+            func.__defaults__,
             closure
         ))
         @self.wraps(new_func)
         def wrapped(*args, **kwargs):
             if func in self._imports:
                 self._apply_imports(func, glob)
             return new_func(*args, **kwargs)
 
diff --git a/python/mozbuild/mozbuild/configure/lint.py b/python/mozbuild/mozbuild/configure/lint.py
--- a/python/mozbuild/mozbuild/configure/lint.py
+++ b/python/mozbuild/mozbuild/configure/lint.py
@@ -141,17 +141,17 @@ class LintSandbox(ConfigureSandbox):
         if isinstance(obj, DependsFunction):
             if obj in (self._always, self._never):
                 return False
             func, glob = self.unwrap(obj._func)
             # We allow missing --help dependencies for functions that:
             # - don't use @imports
             # - don't have a closure
             # - don't use global variables
-            if func in self._imports or func.func_closure:
+            if func in self._has_imports or func.__closure__:
                 return True
             for op, arg, _ in disassemble_as_iter(func):
                 if op in ('LOAD_GLOBAL', 'STORE_GLOBAL'):
                     # There is a fake os module when one is not imported,
                     # and it's allowed for functions without a --help
                     # dependency.
                     if arg == 'os' and glob.get('os') is self.OS:
                         continue
@@ -179,20 +179,20 @@ class LintSandbox(ConfigureSandbox):
         result = super(LintSandbox, self).option_impl(*args, **kwargs)
         when = self._conditions.get(result)
         if when:
             self._value_for(when)
 
         return result
 
     def unwrap(self, func):
-        glob = func.func_globals
+        glob = func.__globals__
         while func in self._wrapped:
-            if isinstance(func.func_globals, SandboxedGlobal):
-                glob = func.func_globals
+            if isinstance(func.__globals__, SandboxedGlobal):
+                glob = func.__globals__
             func = self._wrapped[func]
         return func, glob
 
     def wraps(self, func):
         def do_wraps(wrapper):
             self._wrapped[wrapper] = func
             return wraps(func)(wrapper)
         return do_wraps
