# HG changeset patch
# User stransky <stransky@redhat.com>
# Date 1646676407 0
#      Mon Mar 07 18:06:47 2022 +0000
# Node ID 0c6b7084cdb596a7704efc258fecd6f760170618
# Parent  ff6d7976101a9a4fc50367708b102949f5abd92b
Bug 1750760 Update audio and video decoders to ffmpeg 5.0 r=alwu

FFmpeg 5.0 removed some deprecated symbols so we need to update our decoding code for it.

Differential Revision: https://phabricator.services.mozilla.com/D139699

diff --git a/dom/media/platforms/ffmpeg/FFmpegAudioDecoder.cpp b/dom/media/platforms/ffmpeg/FFmpegAudioDecoder.cpp
--- a/dom/media/platforms/ffmpeg/FFmpegAudioDecoder.cpp
+++ b/dom/media/platforms/ffmpeg/FFmpegAudioDecoder.cpp
@@ -2,16 +2,17 @@
 /* vim:set ts=2 sw=2 sts=2 et cindent: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "mozilla/TaskQueue.h"
 
 #include "FFmpegAudioDecoder.h"
+#include "FFmpegLog.h"
 #include "TimeUnits.h"
 #include "VideoUtils.h"
 
 namespace mozilla {
 
 FFmpegAudioDecoder<LIBAV_VER>::FFmpegAudioDecoder(FFmpegLibWrapper* aLib,
   TaskQueue* aTaskQueue, const AudioInfo& aConfig)
   : FFmpegDataDecoder(aLib, aTaskQueue, GetCodecId(aConfig.mMimeType))
@@ -200,25 +201,59 @@ FFmpegAudioDecoder<LIBAV_VER>::DoDecode(
       NS_ERROR_OUT_OF_MEMORY,
       RESULT_DETAIL("FFmpeg audio decoder failed to allocate frame"));
   }
 
   int64_t samplePosition = aSample->mOffset;
   media::TimeUnit pts = aSample->mTime;
 
   while (packet.size > 0) {
-    int decoded;
-    int bytesConsumed =
+    int decoded = false;
+    int bytesConsumed = -1;
+#if LIBAVCODEC_VERSION_MAJOR < 59
+    bytesConsumed =
       mLib->avcodec_decode_audio4(mCodecContext, mFrame, &decoded, &packet);
-
     if (bytesConsumed < 0) {
       NS_WARNING("FFmpeg audio decoder error.");
       return MediaResult(NS_ERROR_DOM_MEDIA_DECODE_ERR,
                          RESULT_DETAIL("FFmpeg audio error:%d", bytesConsumed));
     }
+#else
+#  define AVRESULT_OK 0
+
+    int ret = mLib->avcodec_receive_frame(mCodecContext, mFrame);
+    switch (ret) {
+      case AVRESULT_OK:
+        decoded = true;
+        break;
+      case AVERROR(EAGAIN):
+        break;
+      case AVERROR_EOF: {
+        FFMPEG_LOG("  End of stream.");
+        return MediaResult(NS_ERROR_DOM_MEDIA_END_OF_STREAM,
+                           RESULT_DETAIL("End of stream"));
+      }
+    }
+    ret = mLib->avcodec_send_packet(mCodecContext, &packet);
+    switch (ret) {
+      case AVRESULT_OK:
+        bytesConsumed = packet.size;
+        break;
+      case AVERROR(EAGAIN):
+        break;
+      case AVERROR_EOF:
+        FFMPEG_LOG("  End of stream.");
+        return MediaResult(NS_ERROR_DOM_MEDIA_END_OF_STREAM,
+                           RESULT_DETAIL("End of stream"));
+      default:
+        NS_WARNING("FFmpeg audio decoder error.");
+        return MediaResult(NS_ERROR_DOM_MEDIA_DECODE_ERR,
+                           RESULT_DETAIL("FFmpeg audio error"));
+    }
+#endif
 
     if (decoded) {
       if (mFrame->format != AV_SAMPLE_FMT_FLT &&
           mFrame->format != AV_SAMPLE_FMT_FLTP &&
           mFrame->format != AV_SAMPLE_FMT_S16 &&
           mFrame->format != AV_SAMPLE_FMT_S16P &&
           mFrame->format != AV_SAMPLE_FMT_S32 &&
           mFrame->format != AV_SAMPLE_FMT_S32P) {
diff --git a/dom/media/platforms/ffmpeg/FFmpegVideoDecoder.cpp b/dom/media/platforms/ffmpeg/FFmpegVideoDecoder.cpp
--- a/dom/media/platforms/ffmpeg/FFmpegVideoDecoder.cpp
+++ b/dom/media/platforms/ffmpeg/FFmpegVideoDecoder.cpp
@@ -178,16 +178,24 @@ FFmpegVideoDecoder<LIBAV_VER>::InitCodec
       mCodecContext->thread_type = FF_THREAD_SLICE | FF_THREAD_FRAME;
     }
   }
 
   // FFmpeg will call back to this to negotiate a video pixel format.
   mCodecContext->get_format = ChoosePixelFormat;
 }
 
+static int64_t GetFramePts(AVFrame* aFrame) {
+#if LIBAVCODEC_VERSION_MAJOR > 58
+  return aFrame->pts;
+#else
+  return aFrame->pkt_pts;
+#endif
+}
+
 MediaResult
 FFmpegVideoDecoder<LIBAV_VER>::DoDecode(MediaRawData* aSample,
                                         uint8_t* aData, int aSize,
                                         bool* aGotFrame,
                                         MediaDataDecoder::DecodedData& aResults)
 {
   AVPacket packet;
   mLib->av_init_packet(&packet);
@@ -226,18 +234,19 @@ FFmpegVideoDecoder<LIBAV_VER>::DoDecode(
     if (res == AVERROR(EAGAIN)) {
       return NS_OK;
     }
     if (res < 0) {
       FFMPEG_LOG("avcodec_receive_frame error: %d", res);
       return MediaResult(NS_ERROR_DOM_MEDIA_DECODE_ERR,
                          RESULT_DETAIL("avcodec_receive_frame error: %d", res));
     }
-    MediaResult rv = CreateImage(mFrame->pkt_pos, mFrame->pkt_pts,
+    MediaResult rv = CreateImage(mFrame->pkt_pos, GetFramePts(mFrame),
                                  mFrame->pkt_duration, aResults);
+
     if (NS_FAILED(rv)) {
       return rv;
     }
     if (aGotFrame) {
       *aGotFrame = true;
     }
   } while (true);
 #else
@@ -258,19 +267,19 @@ FFmpegVideoDecoder<LIBAV_VER>::DoDecode(
   mFrame->reordered_opaque = AV_NOPTS_VALUE;
 
   int decoded;
   int bytesConsumed =
     mLib->avcodec_decode_video2(mCodecContext, mFrame, &decoded, &packet);
 
   FFMPEG_LOG("DoDecodeFrame:decode_video: rv=%d decoded=%d "
              "(Input: pts(%" PRId64 ") dts(%" PRId64 ") Output: pts(%" PRId64 ") "
-             "opaque(%" PRId64 ") pkt_pts(%" PRId64 ") pkt_dts(%" PRId64 "))",
+             "opaque(%" PRId64 ") pts(%" PRId64 ") pkt_dts(%" PRId64 "))",
              bytesConsumed, decoded, packet.pts, packet.dts, mFrame->pts,
-             mFrame->reordered_opaque, mFrame->pkt_pts, mFrame->pkt_dts);
+             mFrame->reordered_opaque, mFrame->pts, mFrame->pkt_dts);
 
   if (bytesConsumed < 0) {
     return MediaResult(NS_ERROR_DOM_MEDIA_DECODE_ERR,
                        RESULT_DETAIL("FFmpeg video error:%d", bytesConsumed));
   }
 
   if (!decoded) {
     if (aGotFrame) {
@@ -289,17 +298,18 @@ FFmpegVideoDecoder<LIBAV_VER>::DoDecode(
                             != layers::LayersBackend::LAYERS_BASIC &&
                             mImageAllocator->GetCompositorBackendType()
                             != layers::LayersBackend::LAYERS_OPENGL))) {
     return MediaResult(NS_ERROR_DOM_MEDIA_FATAL_ERR,
                        RESULT_DETAIL("unsupported format type (hdr)"));
   }
 
   // If we've decoded a frame then we need to output it
-  int64_t pts = mPtsContext.GuessCorrectPts(mFrame->pkt_pts, mFrame->pkt_dts);
+  int64_t pts =
+      mPtsContext.GuessCorrectPts(GetFramePts(mFrame), mFrame->pkt_dts);
   // Retrieve duration from dts.
   // We use the first entry found matching this dts (this is done to
   // handle damaged file with multiple frames with the same dts)
 
   int64_t duration;
   if (!mDurationMap.Find(mFrame->pkt_dts, duration)) {
     NS_WARNING("Unable to retrieve duration from map");
     duration = aSample->mDuration.ToMicroseconds();
diff --git a/dom/media/platforms/ffmpeg/FFmpegVideoDecoder.cpp.1750760.later b/dom/media/platforms/ffmpeg/FFmpegVideoDecoder.cpp.1750760.later
new file mode 100644
--- /dev/null
+++ b/dom/media/platforms/ffmpeg/FFmpegVideoDecoder.cpp.1750760.later
@@ -0,0 +1,56 @@
+--- FFmpegVideoDecoder.cpp
++++ FFmpegVideoDecoder.cpp
+@@ -34,16 +34,19 @@
+ #  define AV_PIX_FMT_YUVJ420P PIX_FMT_YUVJ420P
+ #  define AV_PIX_FMT_YUV420P10LE PIX_FMT_YUV420P10LE
+ #  define AV_PIX_FMT_YUV422P PIX_FMT_YUV422P
+ #  define AV_PIX_FMT_YUV422P10LE PIX_FMT_YUV422P10LE
+ #  define AV_PIX_FMT_YUV444P PIX_FMT_YUV444P
+ #  define AV_PIX_FMT_YUV444P10LE PIX_FMT_YUV444P10LE
+ #  define AV_PIX_FMT_NONE PIX_FMT_NONE
+ #endif
++#if LIBAVCODEC_VERSION_MAJOR > 58
++#  define AV_PIX_FMT_VAAPI_VLD AV_PIX_FMT_VAAPI
++#endif
+ #include "mozilla/PodOperations.h"
+ #include "mozilla/StaticPrefs_media.h"
+ #include "mozilla/TaskQueue.h"
+ #include "nsThreadUtils.h"
+ #include "prsystem.h"
+ 
+ // Forward declare from va.h
+ #ifdef MOZ_WAYLAND_USE_VAAPI
+@@ -760,16 +763,24 @@ void FFmpegVideoDecoder<LIBAV_VER>::Init
+@@ -825,31 +836,31 @@ MediaResult FFmpegVideoDecoder<LIBAV_VER
+       FFMPEG_LOG("  avcodec_receive_frame error: %d", res);
+       return MediaResult(NS_ERROR_DOM_MEDIA_DECODE_ERR,
+                          RESULT_DETAIL("avcodec_receive_frame error: %d", res));
+     }
+ 
+     MediaResult rv;
+ #  ifdef MOZ_WAYLAND_USE_VAAPI
+     if (IsHardwareAccelerated()) {
+-      rv = CreateImageVAAPI(mFrame->pkt_pos, mFrame->pkt_pts,
++      rv = CreateImageVAAPI(mFrame->pkt_pos, GetFramePts(mFrame),
+                             mFrame->pkt_duration, aResults);
+       // If VA-API playback failed, just quit. Decoder is going to be restarted
+       // without VA-API.
+       if (NS_FAILED(rv)) {
+         // Explicitly remove dmabuf surface pool as it's configured
+         // for VA-API support.         mVideoFramePool = nullptr;
+         return rv;
+       }
+     } else
+ #  endif
+     {
+       rv = CreateImage(mFrame->pkt_pos, GetFramePts(mFrame),
+                        mFrame->pkt_duration, aResults);
+     }
+     if (NS_FAILED(rv)) {
+       return rv;
+     }
+     if (aGotFrame) {
+       *aGotFrame = true;
+     }
+   } while (true);
+
