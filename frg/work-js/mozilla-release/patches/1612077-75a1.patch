# HG changeset patch
# User Mark Banner <standard8@mozilla.com>
# Date 1583185918 0
# Node ID 5024580528108cbff5b06dfefb7e7a44b9d26df1
# Parent  f6dc6b38288cfcb9f5f70932830d4a3d2b3759b2
Bug 1612077 - Use parallel directory processing for Lint when no path is specified or a support file is touched. r=ahal

Differential Revision: https://phabricator.services.mozilla.com/D61219

diff --git a/python/mozlint/mozlint/roller.py b/python/mozlint/mozlint/roller.py
--- a/python/mozlint/mozlint/roller.py
+++ b/python/mozlint/mozlint/roller.py
@@ -197,27 +197,30 @@ class LintRoller(object):
         if self.result.failed_setup:
             print("error: problem with lint setup, skipping {}".format(
                     ', '.join(sorted(self.result.failed_setup))))
             self.linters = [l for l in self.linters if l['name'] not in self.result.failed_setup]
             return 1
         return 0
 
     def _generate_jobs(self, paths, vcs_paths, num_procs):
+        def __get_current_paths(path=self.root):
+            return [os.path.join(path, p) for p in os.listdir(path)]
+
         """A job is of the form (<linter:dict>, <paths:list>)."""
         for linter in self.linters:
             if any(os.path.isfile(p) and mozpath.match(p, pattern)
                     for pattern in linter.get('support-files', []) for p in vcs_paths):
-                lpaths = [self.root]
+                lpaths = __get_current_paths()
                 print("warning: {} support-file modified, linting entire tree "
                       "(press ctrl-c to cancel)".format(linter['name']))
             else:
                 lpaths = paths.union(vcs_paths)
 
-            lpaths = list(lpaths) or [os.getcwd()]
+            lpaths = list(lpaths) or __get_current_paths(os.getcwd())
             chunk_size = min(self.MAX_PATHS_PER_JOB, int(ceil(len(lpaths) / num_procs))) or 1
             if linter['type'] == 'global':
                 # Global linters lint the entire tree in one job.
                 chunk_size = len(lpaths) or 1
             assert chunk_size > 0
 
             while lpaths:
                 yield linter, lpaths[:chunk_size]
diff --git a/python/mozlint/test/test_roller.py b/python/mozlint/test/test_roller.py
--- a/python/mozlint/test/test_roller.py
+++ b/python/mozlint/test/test_roller.py
@@ -9,16 +9,17 @@ import subprocess
 import sys
 import time
 
 import mozunit
 import pytest
 
 from mozlint.errors import LintersNotConfigured
 from mozlint.result import Issue, ResultSummary
+from itertools import chain
 
 
 here = os.path.abspath(os.path.dirname(__file__))
 
 
 def test_roll_no_linters_configured(lint, files):
     with pytest.raises(LintersNotConfigured):
         lint.roll(files)
@@ -233,52 +234,59 @@ def test_keyboard_interrupt():
 
     out = proc.communicate()[0]
     print(out)
     assert 'warning: not all files were linted' in out
     assert '2 problems' in out
     assert 'Traceback' not in out
 
 
-def test_support_files(lint, linters, filedir, monkeypatch):
+def test_support_files(lint, linters, filedir, monkeypatch, files):
     jobs = []
 
     # Replace the original _generate_jobs with a new one that simply
     # adds jobs to a list (and then doesn't return anything).
     orig_generate_jobs = lint._generate_jobs
 
     def fake_generate_jobs(*args, **kwargs):
         jobs.extend([job[1] for job in orig_generate_jobs(*args, **kwargs)])
         return []
 
     monkeypatch.setattr(lint, '_generate_jobs', fake_generate_jobs)
 
     linter_path = linters('support_files')[0]
     lint.read(linter_path)
+    lint.root = filedir
 
     # Modified support files only lint entire root if --outgoing or --workdir
     # are used.
     path = os.path.join(filedir, 'foobar.js')
     lint.mock_vcs([os.path.join(filedir, 'foobar.py')])
     lint.roll(path)
-    assert jobs[0] == [path]
+    actual_files = sorted(chain(*jobs))
+    assert actual_files == [path]
+
+    expected_files = sorted(files)
 
     jobs = []
     lint.roll(path, workdir=True)
-    assert jobs[0] == [lint.root]
+    actual_files = sorted(chain(*jobs))
+    assert actual_files == expected_files
 
     jobs = []
     lint.roll(path, outgoing=True)
-    assert jobs[0] == [lint.root]
+    actual_files = sorted(chain(*jobs))
+    assert actual_files == expected_files
 
     # Lint config file is implicitly added as a support file
     lint.mock_vcs([linter_path])
     jobs = []
     lint.roll(path, outgoing=True, workdir=True)
-    assert jobs[0] == [lint.root]
+    actual_files = sorted(chain(*jobs))
+    assert actual_files == expected_files
 
 
 def test_setup(lint, linters, filedir, capfd):
     with pytest.raises(LintersNotConfigured):
         lint.setup()
 
     lint.read(linters('setup', 'setupfailed', 'setupraised'))
     lint.setup()

