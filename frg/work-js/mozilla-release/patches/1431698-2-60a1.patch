# HG changeset patch
# User Aanchal <aanchal138@gmail.com>
# Date 1517374280 -19800
# Node ID 0a419cddb091c644ddeb03abb42970651b2ab3f8
# Parent  fd3b6a513b2716b764a3d7a8fbcdd4d29f990e54
Bug 1431698 - Change the IntConversion enumerations to enum classes. r=bbouvier

diff --git a/js/src/jit/IonTypes.h b/js/src/jit/IonTypes.h
--- a/js/src/jit/IonTypes.h
+++ b/js/src/jit/IonTypes.h
@@ -386,30 +386,30 @@ class SimdConstant {
         uint32_t hash = mozilla::HashBytes(&val.u, sizeof(val.u));
         return mozilla::AddToHash(hash, val.type_);
     }
     static bool match(const SimdConstant& lhs, const SimdConstant& rhs) {
         return lhs == rhs;
     }
 };
 
-enum IntConversionBehavior {
+enum class IntConversionBehavior {
     // These two try to convert the input to an int32 using ToNumber and
     // will fail if the resulting int32 isn't strictly equal to the input.
-    IntConversion_Normal,
-    IntConversion_NegativeZeroCheck,
+    Normal,
+    NegativeZeroCheck,
     // These two will convert the input to an int32 with loss of precision.
-    IntConversion_Truncate,
-    IntConversion_ClampToUint8,
+    Truncate,
+    ClampToUint8,
 };
 
-enum IntConversionInputKind {
-    IntConversion_NumbersOnly,
-    IntConversion_NumbersOrBoolsOnly,
-    IntConversion_Any
+enum class IntConversionInputKind {
+    NumbersOnly,
+    NumbersOrBoolsOnly,
+    Any
 };
 
 // The ordering of this enumeration is important: Anything < Value is a
 // specialized type. Furthermore, anything < String has trivial conversion to
 // a number.
 enum class MIRType
 {
     Undefined,
diff --git a/js/src/jit/Lowering.cpp b/js/src/jit/Lowering.cpp
--- a/js/src/jit/Lowering.cpp
+++ b/js/src/jit/Lowering.cpp
@@ -2194,23 +2194,23 @@ LIRGenerator::visitToNumberInt32(MToNumb
             new(alloc()) LValueToInt32(useBox(opd), tempDouble(), temp(), LValueToInt32::NORMAL);
         assignSnapshot(lir, Bailout_NonPrimitiveInput);
         define(lir, convert);
         assignSafepoint(lir, convert);
         break;
       }
 
       case MIRType::Null:
-        MOZ_ASSERT(convert->conversion() == IntConversion_Any);
+        MOZ_ASSERT(convert->conversion() == IntConversionInputKind::Any);
         define(new(alloc()) LInteger(0), convert);
         break;
 
       case MIRType::Boolean:
-        MOZ_ASSERT(convert->conversion() == IntConversion_Any ||
-                   convert->conversion() == IntConversion_NumbersOrBoolsOnly);
+        MOZ_ASSERT(convert->conversion() == IntConversionInputKind::Any ||
+                   convert->conversion() == IntConversionInputKind::NumbersOrBoolsOnly);
         redefine(convert, opd);
         break;
 
       case MIRType::Int32:
         redefine(convert, opd);
         break;
 
       case MIRType::Float32:
diff --git a/js/src/jit/MIR.cpp b/js/src/jit/MIR.cpp
--- a/js/src/jit/MIR.cpp
+++ b/js/src/jit/MIR.cpp
@@ -4318,21 +4318,21 @@ MToNumberInt32::foldsTo(TempAllocator& a
 {
     MDefinition* input = getOperand(0);
 
     // Fold this operation if the input operand is constant.
     if (input->isConstant()) {
         DebugOnly<IntConversionInputKind> convert = conversion();
         switch (input->type()) {
           case MIRType::Null:
-            MOZ_ASSERT(convert == IntConversion_Any);
+            MOZ_ASSERT(convert.value == IntConversionInputKind::Any);
             return MConstant::New(alloc, Int32Value(0));
           case MIRType::Boolean:
-            MOZ_ASSERT(convert == IntConversion_Any ||
-                       convert == IntConversion_NumbersOrBoolsOnly);
+            MOZ_ASSERT(convert.value == IntConversionInputKind::Any ||
+                       convert.value == IntConversionInputKind::NumbersOrBoolsOnly);
             return MConstant::New(alloc, Int32Value(input->toConstant()->toBoolean()));
           case MIRType::Int32:
             return MConstant::New(alloc, Int32Value(input->toConstant()->toInt32()));
           case MIRType::Float32:
           case MIRType::Double:
             int32_t ival;
             // Only the value within the range of Int32 can be substituted as constant.
             if (mozilla::NumberIsInt32(input->toConstant()->numberToDouble(), &ival))
diff --git a/js/src/jit/MIR.h b/js/src/jit/MIR.h
--- a/js/src/jit/MIR.h
+++ b/js/src/jit/MIR.h
@@ -5682,17 +5682,17 @@ class MInt64ToFloatingPoint
 class MToNumberInt32
   : public MUnaryInstruction,
     public ToInt32Policy::Data
 {
     bool canBeNegativeZero_;
     IntConversionInputKind conversion_;
 
     explicit MToNumberInt32(MDefinition* def, IntConversionInputKind conversion
-                                              = IntConversion_Any)
+                                              = IntConversionInputKind::Any)
       : MUnaryInstruction(classOpcode, def),
         canBeNegativeZero_(true),
         conversion_(conversion)
     {
         setResultType(MIRType::Int32);
         setMovable();
 
         // An object might have "valueOf", which means it is effectful.
diff --git a/js/src/jit/MacroAssembler.cpp b/js/src/jit/MacroAssembler.cpp
--- a/js/src/jit/MacroAssembler.cpp
+++ b/js/src/jit/MacroAssembler.cpp
@@ -2250,65 +2250,65 @@ MacroAssembler::outOfLineTruncateSlow(Fl
 }
 
 void
 MacroAssembler::convertDoubleToInt(FloatRegister src, Register output, FloatRegister temp,
                                    Label* truncateFail, Label* fail,
                                    IntConversionBehavior behavior)
 {
     switch (behavior) {
-      case IntConversion_Normal:
-      case IntConversion_NegativeZeroCheck:
-        convertDoubleToInt32(src, output, fail, behavior == IntConversion_NegativeZeroCheck);
+      case IntConversionBehavior::Normal:
+      case IntConversionBehavior::NegativeZeroCheck:
+        convertDoubleToInt32(src, output, fail, behavior == IntConversionBehavior::NegativeZeroCheck);
         break;
-      case IntConversion_Truncate:
+      case IntConversionBehavior::Truncate:
         branchTruncateDoubleMaybeModUint32(src, output, truncateFail ? truncateFail : fail);
         break;
-      case IntConversion_ClampToUint8:
+      case IntConversionBehavior::ClampToUint8:
         // Clamping clobbers the input register, so use a temp.
         moveDouble(src, temp);
         clampDoubleToUint8(temp, output);
         break;
     }
 }
 
 void
 MacroAssembler::convertValueToInt(ValueOperand value, MDefinition* maybeInput,
                                   Label* handleStringEntry, Label* handleStringRejoin,
                                   Label* truncateDoubleSlow,
                                   Register stringReg, FloatRegister temp, Register output,
                                   Label* fail, IntConversionBehavior behavior,
                                   IntConversionInputKind conversion)
 {
     Register tag = splitTagForTest(value);
-    bool handleStrings = (behavior == IntConversion_Truncate ||
-                          behavior == IntConversion_ClampToUint8) &&
+    bool handleStrings = (behavior == IntConversionBehavior::Truncate ||
+                          behavior == IntConversionBehavior::ClampToUint8) &&
                          handleStringEntry &&
                          handleStringRejoin;
 
-    MOZ_ASSERT_IF(handleStrings, conversion == IntConversion_Any);
+    MOZ_ASSERT_IF(handleStrings, conversion == IntConversionInputKind::Any);
 
     Label done, isInt32, isBool, isDouble, isNull, isString;
 
     maybeBranchTestType(MIRType::Int32, maybeInput, tag, &isInt32);
-    if (conversion == IntConversion_Any || conversion == IntConversion_NumbersOrBoolsOnly)
+    if (conversion == IntConversionInputKind::Any || conversion == IntConversionInputKind::NumbersOrBoolsOnly)
         maybeBranchTestType(MIRType::Boolean, maybeInput, tag, &isBool);
     maybeBranchTestType(MIRType::Double, maybeInput, tag, &isDouble);
 
-    if (conversion == IntConversion_Any) {
+    if (conversion == IntConversionInputKind::Any) {
         // If we are not truncating, we fail for anything that's not
         // null. Otherwise we might be able to handle strings and objects.
         switch (behavior) {
-          case IntConversion_Normal:
-          case IntConversion_NegativeZeroCheck:
+          case IntConversionBehavior::Normal:
+          case IntConversionBehavior::NegativeZeroCheck:
             branchTestNull(Assembler::NotEqual, tag, fail);
             break;
 
-          case IntConversion_Truncate:
-          case IntConversion_ClampToUint8:
+          case IntConversionBehavior::Truncate:
+          case IntConversionBehavior::ClampToUint8:
             maybeBranchTestType(MIRType::Null, maybeInput, tag, &isNull);
             if (handleStrings)
                 maybeBranchTestType(MIRType::String, maybeInput, tag, &isString);
             maybeBranchTestType(MIRType::Object, maybeInput, tag, fail);
             branchTestUndefined(Assembler::NotEqual, tag, fail);
             break;
         }
     } else {
@@ -2348,52 +2348,52 @@ MacroAssembler::convertValueToInt(ValueO
         unboxBoolean(value, output);
         jump(&done);
     }
 
     // Integers can be unboxed.
     if (isInt32.used()) {
         bind(&isInt32);
         unboxInt32(value, output);
-        if (behavior == IntConversion_ClampToUint8)
+        if (behavior == IntConversionBehavior::ClampToUint8)
             clampIntToUint8(output);
     }
 
     bind(&done);
 }
 
 bool
 MacroAssembler::convertValueToInt(JSContext* cx, const Value& v, Register output, Label* fail,
                                   IntConversionBehavior behavior)
 {
-    bool handleStrings = (behavior == IntConversion_Truncate ||
-                          behavior == IntConversion_ClampToUint8);
+    bool handleStrings = (behavior == IntConversionBehavior::Truncate ||
+                          behavior == IntConversionBehavior::ClampToUint8);
 
     if (v.isNumber() || (handleStrings && v.isString())) {
         double d;
         if (v.isNumber())
             d = v.toNumber();
         else if (!StringToNumber(cx, v.toString(), &d))
             return false;
 
         switch (behavior) {
-          case IntConversion_Normal:
-          case IntConversion_NegativeZeroCheck: {
+          case IntConversionBehavior::Normal:
+          case IntConversionBehavior::NegativeZeroCheck: {
             // -0 is checked anyways if we have a constant value.
             int i;
             if (mozilla::NumberIsInt32(d, &i))
                 move32(Imm32(i), output);
             else
                 jump(fail);
             break;
           }
-          case IntConversion_Truncate:
+          case IntConversionBehavior::Truncate:
             move32(Imm32(ToInt32(d)), output);
             break;
-          case IntConversion_ClampToUint8:
+          case IntConversionBehavior::ClampToUint8:
             move32(Imm32(ClampDoubleToUint8(d)), output);
             break;
         }
 
         return true;
     }
 
     if (v.isBoolean()) {
@@ -2439,17 +2439,17 @@ MacroAssembler::convertTypedOrValueToInt
       case MIRType::Undefined:
       case MIRType::Null:
         move32(Imm32(0), output);
         break;
       case MIRType::Boolean:
       case MIRType::Int32:
         if (src.typedReg().gpr() != output)
             move32(src.typedReg().gpr(), output);
-        if (src.type() == MIRType::Int32 && behavior == IntConversion_ClampToUint8)
+        if (src.type() == MIRType::Int32 && behavior == IntConversionBehavior::ClampToUint8)
             clampIntToUint8(output);
         break;
       case MIRType::Double:
         convertDoubleToInt(src.typedReg().fpu(), output, temp, nullptr, fail, behavior);
         break;
       case MIRType::Float32:
         // Conversion to Double simplifies implementation at the expense of performance.
         convertFloat32ToDouble(src.typedReg().fpu(), temp);
diff --git a/js/src/jit/MacroAssembler.h b/js/src/jit/MacroAssembler.h
--- a/js/src/jit/MacroAssembler.h
+++ b/js/src/jit/MacroAssembler.h
@@ -2434,17 +2434,17 @@ class MacroAssembler : public MacroAssem
     // is truncation or clamping. The subroutine, usually an OOL call, is
     // passed the unboxed string in |stringReg| and should convert it to a
     // double store into |temp|.
     void convertValueToInt(ValueOperand value, MDefinition* input,
                            Label* handleStringEntry, Label* handleStringRejoin,
                            Label* truncateDoubleSlow,
                            Register stringReg, FloatRegister temp, Register output,
                            Label* fail, IntConversionBehavior behavior,
-                           IntConversionInputKind conversion = IntConversion_Any);
+                           IntConversionInputKind conversion = IntConversionInputKind::Any);
     void convertValueToInt(ValueOperand value, FloatRegister temp, Register output, Label* fail,
                            IntConversionBehavior behavior)
     {
         convertValueToInt(value, nullptr, nullptr, nullptr, nullptr, InvalidReg, temp, output,
                           fail, behavior);
     }
     MOZ_MUST_USE bool convertValueToInt(JSContext* cx, const Value& v, Register output, Label* fail,
                                         IntConversionBehavior behavior);
@@ -2455,66 +2455,66 @@ class MacroAssembler : public MacroAssem
     void convertTypedOrValueToInt(TypedOrValueRegister src, FloatRegister temp, Register output,
                                   Label* fail, IntConversionBehavior behavior);
 
     // This carries over the MToNumberInt32 operation on the ValueOperand
     // input; see comment at the top of this class.
     void convertValueToInt32(ValueOperand value, MDefinition* input,
                              FloatRegister temp, Register output, Label* fail,
                              bool negativeZeroCheck,
-                             IntConversionInputKind conversion = IntConversion_Any)
+                             IntConversionInputKind conversion = IntConversionInputKind::Any)
     {
         convertValueToInt(value, input, nullptr, nullptr, nullptr, InvalidReg, temp, output, fail,
                           negativeZeroCheck
-                          ? IntConversion_NegativeZeroCheck
-                          : IntConversion_Normal,
+                          ? IntConversionBehavior::NegativeZeroCheck
+                          : IntConversionBehavior::Normal,
                           conversion);
     }
 
     // This carries over the MTruncateToInt32 operation on the ValueOperand
     // input; see the comment at the top of this class.
     void truncateValueToInt32(ValueOperand value, MDefinition* input,
                               Label* handleStringEntry, Label* handleStringRejoin,
                               Label* truncateDoubleSlow,
                               Register stringReg, FloatRegister temp, Register output, Label* fail)
     {
         convertValueToInt(value, input, handleStringEntry, handleStringRejoin, truncateDoubleSlow,
-                          stringReg, temp, output, fail, IntConversion_Truncate);
+                          stringReg, temp, output, fail, IntConversionBehavior::Truncate);
     }
 
     void truncateValueToInt32(ValueOperand value, FloatRegister temp, Register output, Label* fail)
     {
         truncateValueToInt32(value, nullptr, nullptr, nullptr, nullptr, InvalidReg, temp, output,
                              fail);
     }
 
     MOZ_MUST_USE bool truncateConstantOrRegisterToInt32(JSContext* cx,
                                                         const ConstantOrRegister& src,
                                                         FloatRegister temp, Register output,
                                                         Label* fail)
     {
-        return convertConstantOrRegisterToInt(cx, src, temp, output, fail, IntConversion_Truncate);
+        return convertConstantOrRegisterToInt(cx, src, temp, output, fail, IntConversionBehavior::Truncate);
     }
 
     // Convenience functions for clamping values to uint8.
     void clampValueToUint8(ValueOperand value, MDefinition* input,
                            Label* handleStringEntry, Label* handleStringRejoin,
                            Register stringReg, FloatRegister temp, Register output, Label* fail)
     {
         convertValueToInt(value, input, handleStringEntry, handleStringRejoin, nullptr,
-                          stringReg, temp, output, fail, IntConversion_ClampToUint8);
+                          stringReg, temp, output, fail, IntConversionBehavior::ClampToUint8);
     }
 
     MOZ_MUST_USE bool clampConstantOrRegisterToUint8(JSContext* cx,
                                                      const ConstantOrRegister& src,
                                                      FloatRegister temp, Register output,
                                                      Label* fail)
     {
         return convertConstantOrRegisterToInt(cx, src, temp, output, fail,
-                                              IntConversion_ClampToUint8);
+                                              IntConversionBehavior::ClampToUint8);
     }
 
   public:
     class AfterICSaveLive {
         friend class MacroAssembler;
         explicit AfterICSaveLive(uint32_t initialStack)
 #ifdef JS_DEBUG
           : initialStack(initialStack)
diff --git a/js/src/jit/TypePolicy.cpp b/js/src/jit/TypePolicy.cpp
--- a/js/src/jit/TypePolicy.cpp
+++ b/js/src/jit/TypePolicy.cpp
@@ -230,22 +230,22 @@ ComparePolicy::adjustInputs(TempAllocato
             if (compare->compareType() == MCompare::Compare_DoubleMaybeCoerceLHS && i == 0)
                 convert = MToFPInstruction::NonNullNonStringPrimitives;
             else if (compare->compareType() == MCompare::Compare_DoubleMaybeCoerceRHS && i == 1)
                 convert = MToFPInstruction::NonNullNonStringPrimitives;
             replace = MToFloat32::New(alloc, in, convert);
             break;
           }
           case MIRType::Int32: {
-            IntConversionInputKind convert = IntConversion_NumbersOnly;
+            IntConversionInputKind convert = IntConversionInputKind::NumbersOnly;
             if (compare->compareType() == MCompare::Compare_Int32MaybeCoerceBoth ||
                 (compare->compareType() == MCompare::Compare_Int32MaybeCoerceLHS && i == 0) ||
                 (compare->compareType() == MCompare::Compare_Int32MaybeCoerceRHS && i == 1))
             {
-                convert = IntConversion_NumbersOrBoolsOnly;
+                convert = IntConversionInputKind::NumbersOrBoolsOnly;
             }
             replace = MToNumberInt32::New(alloc, in, convert);
             break;
           }
           case MIRType::Object:
             replace = MUnbox::New(alloc, in, MIRType::Object, MUnbox::Infallible);
             break;
           case MIRType::String:
@@ -723,17 +723,17 @@ ToDoublePolicy::staticAdjustInputs(TempA
     return true;
 }
 
 bool
 ToInt32Policy::staticAdjustInputs(TempAllocator& alloc, MInstruction* ins)
 {
     MOZ_ASSERT(ins->isToNumberInt32() || ins->isTruncateToInt32());
 
-    IntConversionInputKind conversion = IntConversion_Any;
+    IntConversionInputKind conversion = IntConversionInputKind::Any;
     if (ins->isToNumberInt32())
         conversion = ins->toToNumberInt32()->conversion();
 
     MDefinition* in = ins->getOperand(0);
     switch (in->type()) {
       case MIRType::Int32:
       case MIRType::Float32:
       case MIRType::Double:
@@ -742,24 +742,24 @@ ToInt32Policy::staticAdjustInputs(TempAl
         return true;
       case MIRType::Undefined:
         // No need for boxing when truncating.
         if (ins->isTruncateToInt32())
             return true;
         break;
       case MIRType::Null:
         // No need for boxing, when we will convert.
-        if (conversion == IntConversion_Any)
+        if (conversion == IntConversionInputKind::Any)
             return true;
         break;
       case MIRType::Boolean:
         // No need for boxing, when we will convert.
-        if (conversion == IntConversion_Any)
+        if (conversion == IntConversionInputKind::Any)
             return true;
-        if (conversion == IntConversion_NumbersOrBoolsOnly)
+        if (conversion == IntConversionInputKind::NumbersOrBoolsOnly)
             return true;
         break;
       case MIRType::Object:
       case MIRType::String:
       case MIRType::Symbol:
         // Objects might be effectful. Symbols give TypeError.
         break;
       default:
@@ -852,17 +852,17 @@ SimdShufflePolicy::adjustInputs(TempAllo
         MOZ_ASSERT(ins->getOperand(i)->type() == ins->typePolicySpecialization());
 
     // Next inputs are the lanes, which need to be int32
     for (unsigned i = 0; i < s->numLanes(); i++) {
         MDefinition* in = ins->getOperand(s->numVectors() + i);
         if (in->type() == MIRType::Int32)
             continue;
 
-        auto* replace = MToNumberInt32::New(alloc, in, IntConversion_NumbersOnly);
+        auto* replace = MToNumberInt32::New(alloc, in, IntConversionInputKind::NumbersOnly);
         ins->block()->insertBefore(ins, replace);
         ins->replaceOperand(s->numVectors() + i, replace);
         if (!replace->typePolicy()->adjustInputs(alloc, replace))
             return false;
     }
 
     return true;
 }
