# HG changeset patch
# User Jim Blandy <jimb@mozilla.com>
# Date 1519596365 28800
#      Sun Feb 25 14:06:05 2018 -0800
# Node ID 6f555aced81dac6cde601db7ca6589e42efe5f57
# Parent  6121332c9aaf00e14c363877d168a21795c8ebfa
Bug 1438121: Part 1: Add job dispatch primitive to shell. r=fitzgen

See JS_FN documentation for details.

MozReview-Commit-ID: 8ZLyOX85qqh

diff --git a/js/src/jsfriendapi.h b/js/src/jsfriendapi.h
--- a/js/src/jsfriendapi.h
+++ b/js/src/jsfriendapi.h
@@ -441,16 +441,25 @@ ForgetSourceHook(JSContext* cx);
  *
  * Note that the embedding still has to trigger processing of job queues at
  * right time(s), such as after evaluation of a script has run to completion.
  */
 extern JS_FRIEND_API(bool)
 UseInternalJobQueues(JSContext* cx, bool cooperative = false);
 
 /**
+ * Enqueue job on the run queue.
+ *
+ * This is useful in tests for creating situations where a call occurs with no
+ * other JavaScript on the stack.
+ */
+extern JS_FRIEND_API(bool)
+EnqueueJob(JSContext* cx, JS::HandleObject job);
+
+/**
  * Instruct the runtime to stop draining the internal job queue.
  *
  * Useful if the embedding is in the process of quitting in reaction to a
  * builtin being called, or if it wants to resume executing jobs later on.
  */
 extern JS_FRIEND_API(void)
 StopDrainingJobQueue(JSContext* cx);
 
diff --git a/js/src/shell/js.cpp b/js/src/shell/js.cpp
--- a/js/src/shell/js.cpp
+++ b/js/src/shell/js.cpp
@@ -913,16 +913,32 @@ RunModule(JSContext* cx, const char* fil
     JS::AutoValueArray<1> args(cx);
     args[0].setString(path);
 
     RootedValue value(cx);
     return JS_CallFunction(cx, loaderObj, importFun, args, &value);
 }
 
 static bool
+EnqueueJob(JSContext* cx, unsigned argc, Value* vp)
+{
+    CallArgs args = CallArgsFromVp(argc, vp);
+
+    if (!IsCallable(args.get(0))) {
+        JS_ReportErrorASCII(cx, "EnqueueJob's first argument must be callable");
+        return false;
+    }
+
+    args.rval().setUndefined();
+
+    RootedObject job(cx, &args[0].toObject());
+    return js::EnqueueJob(cx, job);
+}
+
+static bool
 DrainJobQueue(JSContext* cx, unsigned argc, Value* vp)
 {
     CallArgs args = CallArgsFromVp(argc, vp);
 
     if (GetShellContext(cx)->quitting) {
         JS_ReportErrorASCII(cx, "Mustn't drain the job queue when the shell is quitting");
         return false;
     }
@@ -7122,16 +7138,20 @@ static const JSFunctionSpecWithHelp shel
 "{ eval }: Apply JS::Evaluate to |params.eval|.\n"
 "\n"
 "The return value is an array of strings, with one element for each\n"
 "JavaScript invocation that occurred as a result of the given\n"
 "operation. Each element is the name of the function invoked, or the\n"
 "string 'eval:FILENAME' if the code was invoked by 'eval' or something\n"
 "similar.\n"),
 
+    JS_FN_HELP("enqueueJob", EnqueueJob, 1, 0,
+"enqueueJob(fn)",
+"  Enqueue 'fn' on the shell's job queue."),
+
     JS_FN_HELP("drainJobQueue", DrainJobQueue, 0, 0,
 "drainJobQueue()",
 "Take jobs from the shell's job queue in FIFO order and run them until the\n"
 "queue is empty.\n"),
 
     JS_FN_HELP("setPromiseRejectionTrackerCallback", SetPromiseRejectionTrackerCallback, 1, 0,
 "setPromiseRejectionTrackerCallback()",
 "Sets the callback to be invoked whenever a Promise rejection is unhandled\n"
diff --git a/js/src/vm/JSContext.cpp b/js/src/vm/JSContext.cpp
--- a/js/src/vm/JSContext.cpp
+++ b/js/src/vm/JSContext.cpp
@@ -1155,16 +1155,28 @@ js::UseInternalJobQueues(JSContext* cx, 
         cx->runtime()->offThreadPromiseState.ref().initInternalDispatchQueue();
     MOZ_ASSERT(cx->runtime()->offThreadPromiseState.ref().initialized());
 
     JS::SetEnqueuePromiseJobCallback(cx, InternalEnqueuePromiseJobCallback);
 
     return true;
 }
 
+JS_FRIEND_API(bool)
+js::EnqueueJob(JSContext* cx, JS::HandleObject job)
+{
+    MOZ_ASSERT(cx->jobQueue);
+    if (!cx->jobQueue->append(job)) {
+        ReportOutOfMemory(cx);
+        return false;
+    }
+
+    return true;
+}
+
 JS_FRIEND_API(void)
 js::StopDrainingJobQueue(JSContext* cx)
 {
     MOZ_ASSERT(cx->jobQueue);
     cx->stopDrainingJobQueue = true;
 }
 
 JS_FRIEND_API(void)
