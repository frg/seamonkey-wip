# HG changeset patch
# User Andrew Halberstadt <ahalberstadt@mozilla.com>
# Date 1550870302 0
#      Fri Feb 22 21:18:22 2019 +0000
# Node ID ba172b704def575675d1303eef8f0b1c8ca33491
# Parent  ef9a57429d594ef1f75d070784764fc1b70d8595
Bug 1367092 - [lint] Move py2/py3 linter's exludes logic into mozlint, r=egao

This will be re-used by the flake8 linter, so moving it into mozlint for
re-useability.

Depends on D20493

Differential Revision: https://phabricator.services.mozilla.com/D20494

diff --git a/python/mozlint/mozlint/pathutils.py b/python/mozlint/mozlint/pathutils.py
--- a/python/mozlint/mozlint/pathutils.py
+++ b/python/mozlint/mozlint/pathutils.py
@@ -237,8 +237,51 @@ def get_ancestors_by_name(name, path, ro
     configs = []
     for path in ancestors(path):
         config = os.path.join(path, name)
         if os.path.isfile(config):
             configs.append(config)
         if path == root:
             break
     return configs
+
+
+def expand_exclusions(paths, config, root):
+    """Returns all files that match patterns and aren't excluded.
+
+    This is used by some external linters who receive 'batch' files (e.g dirs)
+    but aren't capable of applying their own exclusions. There is an argument
+    to be made that this step should just apply to all linters no matter what.
+
+    Args:
+        paths (list): List of candidate paths to lint.
+        config (dict): Linter's config object.
+        root (str): Root of the repository.
+
+    Returns:
+        Generator which generates list of paths that weren't excluded.
+    """
+    extensions = [e.lstrip('.') for e in config['extensions']]
+
+    def normalize(path):
+        path = mozpath.normpath(path)
+        if os.path.isabs(path):
+            return path
+        return mozpath.join(root, path)
+
+    exclude = map(normalize, config.get('exclude', []))
+    for path in paths:
+        path = mozpath.normsep(path)
+        if os.path.isfile(path):
+            if not any(path.startswith(e) for e in exclude):
+                yield path
+            continue
+
+        ignore = [e[len(path):].lstrip('/') for e in exclude
+                  if mozpath.commonprefix((path, e)) == path]
+        finder = FileFinder(path, ignore=ignore)
+
+        _, ext = os.path.splitext(path)
+        ext.lstrip('.')
+
+        for ext in extensions:
+            for p, f in finder.find("**/*.{}".format(ext)):
+                yield os.path.join(path, p)
diff --git a/tools/lint/python/compat.py b/tools/lint/python/compat.py
--- a/tools/lint/python/compat.py
+++ b/tools/lint/python/compat.py
@@ -4,21 +4,20 @@
 
 from __future__ import absolute_import, print_function
 
 import json
 import os
 from distutils.spawn import find_executable
 
 import mozfile
-import mozpack.path as mozpath
-from mozpack.files import FileFinder
 from mozprocess import ProcessHandlerMixin
 
 from mozlint import result
+from mozlint.pathutils import expand_exclusions
 
 here = os.path.abspath(os.path.dirname(__file__))
 
 results = []
 
 
 class PyCompatProcess(ProcessHandlerMixin):
     def __init__(self, config, *args, **kwargs):
@@ -51,30 +50,17 @@ def run_linter(python, paths, config, **
     binary = find_executable(python)
     if not binary:
         # If we're in automation, this is fatal. Otherwise, the warning in the
         # setup method was already printed.
         if 'MOZ_AUTOMATION' in os.environ:
             return 1
         return []
 
-    root = lintargs['root']
-    pattern = "**/*.py"
-    exclude = [mozpath.join(root, e) for e in config.get('exclude', [])]
-    files = []
-    for path in paths:
-        path = mozpath.normsep(path)
-        if os.path.isfile(path):
-            files.append(path)
-            continue
-
-        ignore = [e[len(path):].lstrip('/') for e in exclude
-                  if mozpath.commonprefix((path, e)) == path]
-        finder = FileFinder(path, ignore=ignore)
-        files.extend([os.path.join(path, p) for p, f in finder.find(pattern)])
+    files = expand_exclusions(paths, config, lintargs['root'])
 
     with mozfile.NamedTemporaryFile(mode='w') as fh:
         fh.write('\n'.join(files))
         fh.flush()
 
         cmd = [binary, os.path.join(here, 'check_compat.py'), fh.name]
 
         proc = PyCompatProcess(config, cmd)
