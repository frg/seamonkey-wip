# HG changeset patch
# User Matthew Gaudet <mgaudet@mozilla.com>
# Date 1536181807 14400
#      Wed Sep 05 17:10:07 2018 -0400
# Node ID 9661ee2667d6a6251df86c4b0ddf3a939c0af9ce
# Parent  494e23ba027e71fe4e498384ed4e2871ff4bb041
Bug 1488775 - Add String x Number concatenation IC r=tcampbell

Also addresses a pre-existing bug in emitCallStringConcatResult.

Differential Revision: https://phabricator.services.mozilla.com/D5084

diff --git a/js/src/jit-test/tests/cacheir/binaryarith.js b/js/src/jit-test/tests/cacheir/binaryarith.js
--- a/js/src/jit-test/tests/cacheir/binaryarith.js
+++ b/js/src/jit-test/tests/cacheir/binaryarith.js
@@ -40,16 +40,21 @@ obj1 = new D('A');
 var funAdd4 = (a, b) => { return a + b; }
 warmup(funAdd4, [["x", obj1, "xA"], [obj1, "bba", "Abba"]]);
 
 // Add: Int32 Boolean
 var funAdd5 = (a, b) => { return a + b; }
 warmup(funAdd5, [[true, 10, 11], [false, 1, 1], [10, true, 11], [1, false, 1],
                  [2147483647, true, 2147483648],[true, 2147483647, 2147483648]]);
 
+// Add: String Number Concat
+var funAdd6 = (a, b) => { return a + b; }
+warmup(funAdd6, [["x", 10, "x10"], [10, "bba", "10bba"], ["x", 1.2, "x1.2"],
+                 [1.2, "bba", "1.2bba"]]);
+
 // Sub Int32
 var funSub1 = (a, b) => { return a - b; }
 warmup(funSub1, [[7, 0, 7], [7, 8, -1], [4294967295, 2, 4294967293], [0,0,0]]);
 
 // Sub Double
 var funSub2 = (a, b) => { return a - b; }
 warmup(funSub2, [[7.5, 0, 7.5], [7, 8.125, -1.125], [4294967295.3125, 2, 4294967293.3125], [NaN,10,NaN]]);
 
@@ -205,9 +210,40 @@ warmup(funURsh4, [[54772703898, 11, 1578
 for (var k=0; k < 30; k++) {
     A="01234567";
     res =""
     for (var i = 0; i < 8; ++i) {
         var v = A[7 - i];
         res+=v;
     }
     assertEq(res, "76543210");
-}
\ No newline at end of file
+}
+
+// Begin OOM testing:
+if (!('oomTest' in this))
+    quit();
+
+// Add: String Number Concat OOM test
+var addOom = (a, b) => { return a + b; }
+
+function generate_digits(prefix, start) {
+    digits = []
+    number = ""+start+".25";
+    for (var i = 1; i < 7; i++) {
+        number = i + number;
+        digits.push([prefix, Number(number), prefix + number]);
+    }
+    return digits;
+}
+
+// Trying to defeat dtoacache: Warm the IC with one set of digits, then actually oomTest
+// using another set.
+var warmup_digits = generate_digits("x", 1);
+var test_digits = generate_digits("x", 2);
+
+function ot(digits) {
+    warmup(addOom, digits);
+}
+
+// Ensure ICs are warmed
+ot(warmup_digits);
+
+oomTest(() => { ot(test_digits); });
\ No newline at end of file
diff --git a/js/src/jit/BaselineCacheIRCompiler.cpp b/js/src/jit/BaselineCacheIRCompiler.cpp
--- a/js/src/jit/BaselineCacheIRCompiler.cpp
+++ b/js/src/jit/BaselineCacheIRCompiler.cpp
@@ -2370,16 +2370,18 @@ ICCacheIR_Updated::Clone(JSContext* cx, 
 bool
 BaselineCacheIRCompiler::emitCallStringConcatResult()
 {
     AutoOutputRegister output(*this);
     Register lhs = allocator.useRegister(masm, reader.stringOperandId());
     Register rhs = allocator.useRegister(masm, reader.stringOperandId());
     AutoScratchRegisterMaybeOutput scratch(allocator, masm, output);
 
+    allocator.discardStack(masm);
+
     AutoStubFrame stubFrame(*this);
     stubFrame.enter(masm, scratch);
 
     masm.push(rhs);
     masm.push(lhs);
 
     if (!callVM(masm, ConcatStringsInfo)) {
         return false;
diff --git a/js/src/jit/BaselineInspector.cpp b/js/src/jit/BaselineInspector.cpp
--- a/js/src/jit/BaselineInspector.cpp
+++ b/js/src/jit/BaselineInspector.cpp
@@ -387,16 +387,18 @@ ParseCacheIRStub(ICStub* stub)
     switch (reader.readOp()) {
       case CacheOp::LoadUndefinedResult:
         return MIRType::Undefined;
       case CacheOp::LoadBooleanResult:
         return MIRType::Boolean;
       case CacheOp::LoadStringResult:
       case CacheOp::CallStringConcatResult:
       case CacheOp::CallStringObjectConcatResult:
+      case CacheOp::CallInt32ToString:
+      case CacheOp::CallNumberToString:
         return MIRType::String;
       case CacheOp::DoubleAddResult:
       case CacheOp::DoubleSubResult:
       case CacheOp::DoubleMulResult:
       case CacheOp::DoubleDivResult:
       case CacheOp::DoubleModResult:
       case CacheOp::DoubleNegationResult:
         return MIRType::Double;
diff --git a/js/src/jit/CacheIR.cpp b/js/src/jit/CacheIR.cpp
--- a/js/src/jit/CacheIR.cpp
+++ b/js/src/jit/CacheIR.cpp
@@ -5820,16 +5820,19 @@ BinaryArithIRGenerator::tryAttachStub()
         return true;
     }
 
     // String x Object
     if (tryAttachStringObjectConcat()) {
         return true;
     }
 
+    if (tryAttachStringNumberConcat())
+        return true;
+
 
     trackAttached(IRGenerator::NotAttached);
     return false;
 }
 
 bool
 BinaryArithIRGenerator::tryAttachBitwise()
 {
@@ -6016,16 +6019,58 @@ BinaryArithIRGenerator::tryAttachInt32()
       default:
         MOZ_CRASH("Unhandled op in tryAttachInt32");
     }
 
     writer.returnFromIC();
     return true;
 }
 
+
+bool
+BinaryArithIRGenerator::tryAttachStringNumberConcat()
+{
+    // Only Addition
+    if (op_ != JSOP_ADD)
+        return false;
+
+    if (!(lhs_.isString() && rhs_.isNumber()) &&
+        !(lhs_.isNumber() && rhs_.isString()))
+    {
+        return false;
+    }
+
+    ValOperandId lhsId(writer.setInputOperandId(0));
+    ValOperandId rhsId(writer.setInputOperandId(1));
+
+    auto guardToString = [&](ValOperandId id, HandleValue v) {
+        if (v.isString()) {
+            return writer.guardIsString(id);
+        }
+        if (v.isInt32()) {
+            Int32OperandId intId = writer.guardIsInt32(id);
+            return writer.callInt32ToString(intId);
+        }
+        // At this point we are creating an IC that will handle
+        // both Int32 and Double cases.
+        MOZ_ASSERT(v.isNumber());
+        writer.guardIsNumber(id);
+        return writer.callNumberToString(id);
+    };
+
+    StringOperandId lhsStrId = guardToString(lhsId, lhs_);
+    StringOperandId rhsStrId = guardToString(rhsId, rhs_);
+
+    writer.callStringConcatResult(lhsStrId, rhsStrId);
+
+    writer.returnFromIC();
+    trackAttached("BinaryArith.StringNumberConcat");
+    return true;
+}
+
 bool
 BinaryArithIRGenerator::tryAttachStringConcat()
 {
     // Only Addition
     if (op_ != JSOP_ADD) {
         return false;
     }
 
diff --git a/js/src/jit/CacheIR.h b/js/src/jit/CacheIR.h
--- a/js/src/jit/CacheIR.h
+++ b/js/src/jit/CacheIR.h
@@ -261,16 +261,18 @@ extern const char* const CacheKindNames[
     _(ArrayPush)                          \
     _(ArrayJoinResult)                    \
     _(StoreTypedElement)                  \
     _(CallNativeSetter)                   \
     _(CallScriptedSetter)                 \
     _(CallSetArrayLength)                 \
     _(CallProxySet)                       \
     _(CallProxySetByValue)                \
+    _(CallInt32ToString)                  \
+    _(CallNumberToString)                 \
                                           \
     /* The *Result ops load a value into the cache's result register. */ \
     _(LoadFixedSlotResult)                \
     _(LoadDynamicSlotResult)              \
     _(LoadUnboxedPropertyResult)          \
     _(LoadTypedObjectResult)              \
     _(LoadDenseElementResult)             \
     _(LoadDenseElementHoleResult)         \
@@ -1028,16 +1030,28 @@ class MOZ_RAII CacheIRWriter : public JS
         buffer_.writeByte(uint32_t(strict));
     }
     void callProxySetByValue(ObjOperandId obj, ValOperandId id, ValOperandId rhs, bool strict) {
         writeOpWithOperandId(CacheOp::CallProxySetByValue, obj);
         writeOperandId(id);
         writeOperandId(rhs);
         buffer_.writeByte(uint32_t(strict));
     }
+    StringOperandId callInt32ToString(Int32OperandId id) {
+        StringOperandId res(nextOperandId_++);
+        writeOpWithOperandId(CacheOp::CallInt32ToString, id);
+        writeOperandId(res);
+        return res;
+    }
+    StringOperandId callNumberToString(ValOperandId id) {
+        StringOperandId res(nextOperandId_++);
+        writeOpWithOperandId(CacheOp::CallNumberToString, id);
+        writeOperandId(res);
+        return res;
+    }
 
     void megamorphicLoadSlotResult(ObjOperandId obj, PropertyName* name, bool handleMissing) {
         writeOpWithOperandId(CacheOp::MegamorphicLoadSlotResult, obj);
         addStubField(uintptr_t(name), StubField::Type::String);
         buffer_.writeByte(uint32_t(handleMissing));
     }
     void megamorphicLoadSlotByValueResult(ObjOperandId obj, ValOperandId id, bool handleMissing) {
         writeOpWithOperandId(CacheOp::MegamorphicLoadSlotByValueResult, obj);
@@ -1970,16 +1984,17 @@ class MOZ_RAII BinaryArithIRGenerator : 
 
     void trackAttached(const char* name);
 
     bool tryAttachInt32();
     bool tryAttachDouble();
     bool tryAttachBitwise();
     bool tryAttachStringConcat();
     bool tryAttachStringObjectConcat();
+    bool tryAttachStringNumberConcat();
 
   public:
     BinaryArithIRGenerator(JSContext* cx, HandleScript, jsbytecode* pc, ICState::Mode,
                            JSOp op, HandleValue lhs, HandleValue rhs, HandleValue res);
 
     bool tryAttachStub();
 
 };
diff --git a/js/src/jit/CacheIRCompiler.cpp b/js/src/jit/CacheIRCompiler.cpp
--- a/js/src/jit/CacheIRCompiler.cpp
+++ b/js/src/jit/CacheIRCompiler.cpp
@@ -4036,16 +4036,73 @@ bool
 CacheIRCompiler::emitLoadObject()
 {
     Register reg = allocator.defineRegister(masm, reader.objOperandId());
     StubFieldOffset obj(reader.stubOffset(), StubField::Type::JSObject);
     emitLoadStubField(obj, reg);
     return true;
 }
 
+bool
+CacheIRCompiler::emitCallInt32ToString() {
+    Register input = allocator.useRegister(masm, reader.int32OperandId());
+    Register result = allocator.defineRegister(masm, reader.stringOperandId());
+
+    FailurePath* failure;
+    if (!addFailurePath(&failure))
+        return false;
+
+    LiveRegisterSet volatileRegs(GeneralRegisterSet::Volatile(), liveVolatileFloatRegs());
+    volatileRegs.takeUnchecked(result);
+    masm.PushRegsInMask(volatileRegs);
+
+    masm.setupUnalignedABICall(result);
+    masm.loadJSContext(result);
+    masm.passABIArg(result);
+    masm.passABIArg(input);
+    masm.callWithABI(JS_FUNC_TO_DATA_PTR(void*, (js::Int32ToStringHelper)));
+
+    masm.mov(ReturnReg, result);
+    masm.PopRegsInMask(volatileRegs);
+
+    masm.branchPtr(Assembler::Equal, result, ImmPtr(0), failure->label());
+    return true;
+}
+
+bool
+CacheIRCompiler::emitCallNumberToString() {
+    // Float register must be preserved. The BinaryArith ICs use
+    // the fact that baseline has them available, as well as fixed temps on
+    // LBinaryCache.
+    allocator.ensureDoubleRegister(masm, reader.valOperandId(), FloatReg0);
+    Register result = allocator.defineRegister(masm, reader.stringOperandId());
+
+    FailurePath* failure;
+    if (!addFailurePath(&failure))
+        return false;
+
+    LiveRegisterSet volatileRegs(GeneralRegisterSet::Volatile(), liveVolatileFloatRegs());
+    volatileRegs.takeUnchecked(result);
+    volatileRegs.addUnchecked(FloatReg0);
+    masm.PushRegsInMask(volatileRegs);
+
+    masm.setupUnalignedABICall(result);
+    masm.loadJSContext(result);
+    masm.passABIArg(result);
+    masm.passABIArg(FloatReg0, MoveOp::DOUBLE);
+    masm.callWithABI(JS_FUNC_TO_DATA_PTR(void*, (js::NumberToStringHelper)));
+
+    masm.mov(ReturnReg, result);
+    masm.PopRegsInMask(volatileRegs);
+
+    masm.branchPtr(Assembler::Equal, result, ImmPtr(0), failure->label());
+    return true;
+}
+
+
 void
 js::jit::LoadTypedThingData(MacroAssembler& masm, TypedThingLayout layout, Register obj, Register result)
 {
     switch (layout) {
       case Layout_TypedArray:
         masm.loadPtr(Address(obj, TypedArrayObject::dataOffset()), result);
         break;
       case Layout_OutlineTypedObject:
diff --git a/js/src/jit/CacheIRCompiler.h b/js/src/jit/CacheIRCompiler.h
--- a/js/src/jit/CacheIRCompiler.h
+++ b/js/src/jit/CacheIRCompiler.h
@@ -107,16 +107,18 @@ namespace jit {
     _(ArrayJoinResult)                    \
     _(CallPrintString)                    \
     _(Breakpoint)                         \
     _(MegamorphicLoadSlotResult)          \
     _(MegamorphicLoadSlotByValueResult)   \
     _(MegamorphicStoreSlot)               \
     _(MegamorphicHasPropResult)           \
     _(CallObjectHasSparseElementResult)   \
+    _(CallInt32ToString)                  \
+    _(CallNumberToString)                 \
     _(WrapResult)
 
 // Represents a Value on the Baseline frame's expression stack. Slot 0 is the
 // value on top of the stack (the most recently pushed value), slot 1 is the
 // value pushed before that, etc.
 class BaselineFrameSlot
 {
     uint32_t slot_;
diff --git a/js/src/jsnum.cpp b/js/src/jsnum.cpp
--- a/js/src/jsnum.cpp
+++ b/js/src/jsnum.cpp
@@ -714,16 +714,27 @@ js::Int32ToString(JSContext* cx, int32_t
 }
 
 template JSFlatString*
 js::Int32ToString<CanGC>(JSContext* cx, int32_t si);
 
 template JSFlatString*
 js::Int32ToString<NoGC>(JSContext* cx, int32_t si);
 
+JSFlatString*
+js::Int32ToStringHelper(JSContext* cx, int32_t si)
+{
+    AutoUnsafeCallWithABI unsafe;
+    JSFlatString* res = Int32ToString<NoGC>(cx, si);
+    if (!res) {
+        cx->recoverFromOutOfMemory();
+    }
+    return res;
+}
+
 JSAtom*
 js::Int32ToAtom(JSContext* cx, int32_t si)
 {
     if (JSFlatString* str = LookupInt32ToString(cx, si)) {
         return js::AtomizeString(cx, str);
     }
 
     char buffer[JSFatInlineString::MAX_LENGTH_TWO_BYTE + 1];
@@ -1513,16 +1524,27 @@ js::NumberToString(JSContext* cx, double
 }
 
 template JSString*
 js::NumberToString<CanGC>(JSContext* cx, double d);
 
 template JSString*
 js::NumberToString<NoGC>(JSContext* cx, double d);
 
+JSString*
+js::NumberToStringHelper(JSContext* cx, double d)
+{
+    AutoUnsafeCallWithABI unsafe;
+    JSString* res = NumberToString<NoGC>(cx, d);
+    if (!res) {
+        cx->recoverFromOutOfMemory();
+    }
+    return res;
+}
+
 JSAtom*
 js::NumberToAtom(JSContext* cx, double d)
 {
     int32_t si;
     if (mozilla::NumberEqualsInt32(d, &si)) {
         return Int32ToAtom(cx, si);
     }
 
diff --git a/js/src/jsnum.h b/js/src/jsnum.h
--- a/js/src/jsnum.h
+++ b/js/src/jsnum.h
@@ -55,23 +55,29 @@ InitNumberClass(JSContext* cx, Handle<Gl
  * When base == 10, this function implements ToString() as specified by
  * ECMA-262-5 section 9.8.1; but note that it handles integers specially for
  * performance.  See also js::NumberToCString().
  */
 template <AllowGC allowGC>
 extern JSString*
 NumberToString(JSContext* cx, double d);
 
+extern JSString*
+NumberToStringHelper(JSContext* cx, double d);
+
 extern JSAtom*
 NumberToAtom(JSContext* cx, double d);
 
 template <AllowGC allowGC>
 extern JSFlatString*
 Int32ToString(JSContext* cx, int32_t i);
 
+extern JSFlatString*
+Int32ToStringHelper(JSContext* cx, int32_t i);
+
 extern JSAtom*
 Int32ToAtom(JSContext* cx, int32_t si);
 
 // ES6 15.7.3.12
 extern bool
 IsInteger(const Value& val);
 
 /*
