# HG changeset patch
# User Andrea Marchesini <amarchesini@mozilla.com>
# Date 1515416705 -3600
# Node ID 0918da01e55435aee12708d155e1832103dcd9f0
# Parent  60d038987cde6efdc75849358e51e913a51981d3
Bug 1419771 - Introduce DOMPrefs, a thread-safe access to preferences for DOM - part 16 - Webkit/Blink directory picker enabled, r=asuth

diff --git a/dom/base/DOMPrefs.cpp b/dom/base/DOMPrefs.cpp
--- a/dom/base/DOMPrefs.cpp
+++ b/dom/base/DOMPrefs.cpp
@@ -44,16 +44,17 @@ PREF(NotificationRIEnabled, "dom.webnoti
 PREF(ServiceWorkersEnabled, "dom.serviceWorkers.enabled")
 PREF(ServiceWorkersTestingEnabled, "dom.serviceWorkers.testing.enabled")
 PREF(StorageManagerEnabled, "dom.storageManager.enabled")
 PREF(PromiseRejectionEventsEnabled, "dom.promise_rejection_events.enabled")
 PREF(PushEnabled, "dom.push.enabled")
 PREF(StreamsEnabled, "dom.streams.enabled")
 PREF(RequestContextEnabled, "dom.requestcontext.enabled")
 PREF(OffscreenCanvasEnabled, "gfx.offscreencanvas.enabled")
+PREF(WebkitBlinkDirectoryPickerEnabled, "dom.webkitBlink.dirPicker.enabled")
 
 #undef PREF
 
 #define PREF_WEBIDL(name)                        \
   /* static */ bool                              \
   DOMPrefs::name(JSContext* aCx, JSObject* aObj) \
   {                                              \
     return DOMPrefs::name();                     \
@@ -65,13 +66,14 @@ PREF_WEBIDL(NotificationEnabledInService
 PREF_WEBIDL(NotificationRIEnabled)
 PREF_WEBIDL(ServiceWorkersEnabled)
 PREF_WEBIDL(StorageManagerEnabled)
 PREF_WEBIDL(PromiseRejectionEventsEnabled)
 PREF_WEBIDL(PushEnabled)
 PREF_WEBIDL(StreamsEnabled)
 PREF_WEBIDL(RequestContextEnabled)
 PREF_WEBIDL(OffscreenCanvasEnabled)
+PREF_WEBIDL(WebkitBlinkDirectoryPickerEnabled)
 
 #undef PREF_WEBIDL
 
 } // dom namespace
 } // mozilla namespace
diff --git a/dom/base/DOMPrefs.h b/dom/base/DOMPrefs.h
--- a/dom/base/DOMPrefs.h
+++ b/dom/base/DOMPrefs.h
@@ -70,14 +70,18 @@ public:
 
   // Returns true if the dom.requestcontext.enabled pref is set.
   static bool RequestContextEnabled();
   static bool RequestContextEnabled(JSContext* aCx, JSObject* aObj);
 
   // Returns true if the gfx.offscreencanvas.enabled pref is set.
   static bool OffscreenCanvasEnabled();
   static bool OffscreenCanvasEnabled(JSContext* aCx, JSObject* aObj);
+
+  // Returns true if the dom.webkitBlink.dirPicker.enabled pref is set.
+  static bool WebkitBlinkDirectoryPickerEnabled();
+  static bool WebkitBlinkDirectoryPickerEnabled(JSContext* aCx, JSObject* aObj);
 };
 
 } // dom namespace
 } // mozilla namespace
 
 #endif // mozilla_dom_DOMPrefs_h
diff --git a/dom/filesystem/Directory.cpp b/dom/filesystem/Directory.cpp
--- a/dom/filesystem/Directory.cpp
+++ b/dom/filesystem/Directory.cpp
@@ -42,34 +42,16 @@ NS_IMPL_CYCLE_COLLECTION_TRACE_WRAPPERCA
 
 NS_IMPL_CYCLE_COLLECTING_ADDREF(Directory)
 NS_IMPL_CYCLE_COLLECTING_RELEASE(Directory)
 NS_INTERFACE_MAP_BEGIN_CYCLE_COLLECTION(Directory)
   NS_WRAPPERCACHE_INTERFACE_MAP_ENTRY
   NS_INTERFACE_MAP_ENTRY(nsISupports)
 NS_INTERFACE_MAP_END
 
-/* static */ bool
-Directory::WebkitBlinkDirectoryPickerEnabled(JSContext* aCx, JSObject* aObj)
-{
-  if (NS_IsMainThread()) {
-    return Preferences::GetBool("dom.webkitBlink.dirPicker.enabled", false);
-  }
-
-  // aCx can be null when this function is called by something else than WebIDL
-  // binding code.
-  workers::WorkerPrivate* workerPrivate =
-    workers::GetCurrentThreadWorkerPrivate();
-  if (!workerPrivate) {
-    return false;
-  }
-
-  return workerPrivate->WebkitBlinkDirectoryPickerEnabled();
-}
-
 /* static */ already_AddRefed<Directory>
 Directory::Constructor(const GlobalObject& aGlobal,
                        const nsAString& aRealPath,
                        ErrorResult& aRv)
 {
   nsCOMPtr<nsIFile> path;
   aRv = NS_NewLocalFile(aRealPath, true, getter_AddRefs(path));
   if (NS_WARN_IF(aRv.Failed())) {
diff --git a/dom/filesystem/Directory.h b/dom/filesystem/Directory.h
--- a/dom/filesystem/Directory.h
+++ b/dom/filesystem/Directory.h
@@ -24,19 +24,16 @@ class StringOrFileOrDirectory;
 class Directory final
   : public nsISupports
   , public nsWrapperCache
 {
 public:
   NS_DECL_CYCLE_COLLECTING_ISUPPORTS
   NS_DECL_CYCLE_COLLECTION_SCRIPT_HOLDER_CLASS(Directory)
 
-  static bool
-  WebkitBlinkDirectoryPickerEnabled(JSContext* aCx, JSObject* aObj);
-
   static already_AddRefed<Directory>
   Constructor(const GlobalObject& aGlobal,
               const nsAString& aRealPath,
               ErrorResult& aRv);
 
   static already_AddRefed<Directory>
   Create(nsISupports* aParent, nsIFile* aDirectory,
          FileSystemBase* aFileSystem = 0);
diff --git a/dom/html/HTMLFormSubmission.cpp b/dom/html/HTMLFormSubmission.cpp
--- a/dom/html/HTMLFormSubmission.cpp
+++ b/dom/html/HTMLFormSubmission.cpp
@@ -32,16 +32,17 @@
 #include "nsIConsoleService.h"
 #include "nsIScriptError.h"
 #include "nsIStringBundle.h"
 #include "nsCExternalHandlerService.h"
 #include "nsIFileStreams.h"
 #include "nsContentUtils.h"
 
 #include "mozilla/dom/Directory.h"
+#include "mozilla/dom/DOMPrefs.h"
 #include "mozilla/dom/File.h"
 
 namespace mozilla {
 namespace dom {
 
 namespace {
 
 void
@@ -487,17 +488,17 @@ FSMultipartFormData::AddNameBlobOrNullPa
 
   if (aBlob) {
     nsAutoString filename16;
 
     RefPtr<File> file = aBlob->ToFile();
     if (file) {
       nsAutoString relativePath;
       file->GetRelativePath(relativePath);
-      if (Directory::WebkitBlinkDirectoryPickerEnabled(nullptr, nullptr) &&
+      if (DOMPrefs::WebkitBlinkDirectoryPickerEnabled() &&
           !relativePath.IsEmpty()) {
         filename16 = relativePath;
       }
 
       if (filename16.IsEmpty()) {
         RetrieveFileName(aBlob, filename16);
       }
     }
@@ -546,17 +547,17 @@ FSMultipartFormData::AddNameBlobOrNullPa
   AddDataChunk(nameStr, filename, contentType, fileStream, size);
   return NS_OK;
 }
 
 nsresult
 FSMultipartFormData::AddNameDirectoryPair(const nsAString& aName,
                                           Directory* aDirectory)
 {
-  if (!Directory::WebkitBlinkDirectoryPickerEnabled(nullptr, nullptr)) {
+  if (!DOMPrefs::WebkitBlinkDirectoryPickerEnabled()) {
     return NS_OK;
   }
 
   // Encode the control name
   nsAutoCString nameStr;
   nsresult rv = EncodeVal(aName, nameStr, true);
   NS_ENSURE_SUCCESS(rv, rv);
 
diff --git a/dom/html/HTMLInputElement.cpp b/dom/html/HTMLInputElement.cpp
--- a/dom/html/HTMLInputElement.cpp
+++ b/dom/html/HTMLInputElement.cpp
@@ -6,16 +6,17 @@
 
 #include "mozilla/dom/HTMLInputElement.h"
 
 #include "mozilla/ArrayUtils.h"
 #include "mozilla/AsyncEventDispatcher.h"
 #include "mozilla/DebugOnly.h"
 #include "mozilla/dom/Date.h"
 #include "mozilla/dom/Directory.h"
+#include "mozilla/dom/DOMPrefs.h"
 #include "mozilla/dom/HTMLFormSubmission.h"
 #include "mozilla/dom/FileSystemUtils.h"
 #include "mozilla/dom/GetFilesHelper.h"
 #include "nsAttrValueInlines.h"
 #include "nsCRTGlue.h"
 
 #include "nsIDOMHTMLInputElement.h"
 #include "nsITextControlElement.h"
@@ -681,17 +682,17 @@ HTMLInputElement::nsFilePickerShownCallb
   // The text control frame (if there is one) isn't going to send a change
   // event because it will think this is done by a script.
   // So, we can safely send one by ourself.
   mInput->SetFilesOrDirectories(newFilesOrDirectories, true);
 
   RefPtr<DispatchChangeEventCallback> dispatchChangeEventCallback =
     new DispatchChangeEventCallback(mInput);
 
-  if (IsWebkitDirPickerEnabled() &&
+  if (DOMPrefs::WebkitBlinkDirectoryPickerEnabled() &&
       mInput->HasAttr(kNameSpaceID_None, nsGkAtoms::webkitdirectory)) {
     ErrorResult error;
     GetFilesHelper* helper = mInput->GetOrCreateGetFilesHelper(true, error);
     if (NS_WARN_IF(error.Failed())) {
       return error.StealNSResult();
     }
 
     helper->AddCallback(dispatchChangeEventCallback);
@@ -2742,17 +2743,17 @@ HTMLInputElement::GetDisplayFileName(nsA
     GetDOMFileOrDirectoryName(mFileData->mFilesOrDirectories[0], aValue);
     return;
   }
 
   nsAutoString value;
 
   if (mFileData->mFilesOrDirectories.IsEmpty()) {
     if ((IsDirPickerEnabled() && Allowdirs()) ||
-        (IsWebkitDirPickerEnabled() &&
+        (DOMPrefs::WebkitBlinkDirectoryPickerEnabled() &&
          HasAttr(kNameSpaceID_None, nsGkAtoms::webkitdirectory))) {
       nsContentUtils::GetLocalizedString(nsContentUtils::eFORMS_PROPERTIES,
                                          "NoDirSelected", value);
     } else if (HasAttr(kNameSpaceID_None, nsGkAtoms::multiple)) {
       nsContentUtils::GetLocalizedString(nsContentUtils::eFORMS_PROPERTIES,
                                          "NoFilesSelected", value);
     } else {
       nsContentUtils::GetLocalizedString(nsContentUtils::eFORMS_PROPERTIES,
@@ -2835,17 +2836,17 @@ HTMLInputElement::MozSetDndFilesAndDirec
 
   if (IsWebkitFileSystemEnabled()) {
     UpdateEntries(aFilesOrDirectories);
   }
 
   RefPtr<DispatchChangeEventCallback> dispatchChangeEventCallback =
     new DispatchChangeEventCallback(this);
 
-  if (IsWebkitDirPickerEnabled() &&
+  if (DOMPrefs::WebkitBlinkDirectoryPickerEnabled() &&
       HasAttr(kNameSpaceID_None, nsGkAtoms::webkitdirectory)) {
     ErrorResult rv;
     GetFilesHelper* helper = GetOrCreateGetFilesHelper(true /* recursionFlag */,
                                                        rv);
     if (NS_WARN_IF(rv.Failed())) {
       rv.SuppressException();
       return;
     }
@@ -2917,17 +2918,17 @@ HTMLInputElement::FireChangeEventIfNeede
 FileList*
 HTMLInputElement::GetFiles()
 {
   if (mType != NS_FORM_INPUT_FILE) {
     return nullptr;
   }
 
   if (IsDirPickerEnabled() && Allowdirs() &&
-      (!IsWebkitDirPickerEnabled() ||
+      (!DOMPrefs::WebkitBlinkDirectoryPickerEnabled() ||
        !HasAttr(kNameSpaceID_None, nsGkAtoms::webkitdirectory))) {
     return nullptr;
   }
 
   if (!mFileData->mFileList) {
     mFileData->mFileList = new FileList(static_cast<nsIContent*>(this));
     UpdateFileList();
   }
@@ -4169,17 +4170,17 @@ HTMLInputElement::MaybeInitPickers(Event
     // If the user clicked on the "Choose folder..." button we open the
     // directory picker, else we open the file picker.
     FilePickerType type = FILE_PICKER_FILE;
     nsCOMPtr<nsIContent> target =
       do_QueryInterface(aVisitor.mEvent->mOriginalTarget);
     if (target &&
         target->FindFirstNonChromeOnlyAccessContent() == this &&
         ((IsDirPickerEnabled() && Allowdirs()) ||
-         (IsWebkitDirPickerEnabled() &&
+         (DOMPrefs::WebkitBlinkDirectoryPickerEnabled() &&
           HasAttr(kNameSpaceID_None, nsGkAtoms::webkitdirectory)))) {
       type = FILE_PICKER_DIRECTORY;
     }
     return InitFilePicker(type);
   }
   if (mType == NS_FORM_INPUT_COLOR) {
     return InitColorPicker();
   }
@@ -5632,31 +5633,16 @@ HTMLInputElement::IsDateTimeTypeSupporte
           (IsInputDateTimeEnabled() || IsExperimentalFormsEnabled())) ||
          ((aDateTimeInputType == NS_FORM_INPUT_MONTH ||
            aDateTimeInputType == NS_FORM_INPUT_WEEK ||
            aDateTimeInputType == NS_FORM_INPUT_DATETIME_LOCAL) &&
           IsInputDateTimeOthersEnabled());
 }
 
 /* static */ bool
-HTMLInputElement::IsWebkitDirPickerEnabled()
-{
-  static bool sWebkitDirPickerEnabled = false;
-  static bool sWebkitDirPickerPrefCached = false;
-  if (!sWebkitDirPickerPrefCached) {
-    sWebkitDirPickerPrefCached = true;
-    Preferences::AddBoolVarCache(&sWebkitDirPickerEnabled,
-                                 "dom.webkitBlink.dirPicker.enabled",
-                                 false);
-  }
-
-  return sWebkitDirPickerEnabled;
-}
-
-/* static */ bool
 HTMLInputElement::IsWebkitFileSystemEnabled()
 {
   static bool sWebkitFileSystemEnabled = false;
   static bool sWebkitFileSystemPrefCached = false;
   if (!sWebkitFileSystemPrefCached) {
     sWebkitFileSystemPrefCached = true;
     Preferences::AddBoolVarCache(&sWebkitFileSystemEnabled,
                                  "dom.webkitBlink.filesystem.enabled",
diff --git a/dom/html/HTMLInputElement.h b/dom/html/HTMLInputElement.h
--- a/dom/html/HTMLInputElement.h
+++ b/dom/html/HTMLInputElement.h
@@ -1723,23 +1723,16 @@ private:
   /**
    * Checks if aDateTimeInputType should be supported based on "dom.forms.datetime",
    * and "dom.experimental_forms".
    */
   static bool
   IsDateTimeTypeSupported(uint8_t aDateTimeInputType);
 
   /**
-   * Checks preference "dom.webkitBlink.dirPicker.enabled" to determine if
-   * webkitdirectory should be supported.
-   */
-  static bool
-  IsWebkitDirPickerEnabled();
-
-  /**
    * Checks preference "dom.webkitBlink.filesystem.enabled" to determine if
    * webkitEntries should be supported.
    */
   static bool
   IsWebkitFileSystemEnabled();
 
   /**
    * Checks preference "dom.input.dirpicker" to determine if file and directory
diff --git a/dom/webidl/File.webidl b/dom/webidl/File.webidl
--- a/dom/webidl/File.webidl
+++ b/dom/webidl/File.webidl
@@ -29,17 +29,17 @@ dictionary ChromeFilePropertyBag : FileP
   boolean existenceCheck = true;
 };
 
 // Mozilla extensions
 partial interface File {
   [GetterThrows, Deprecated="FileLastModifiedDate"]
   readonly attribute Date lastModifiedDate;
 
-  [BinaryName="relativePath", Func="mozilla::dom::Directory::WebkitBlinkDirectoryPickerEnabled"]
+  [BinaryName="relativePath", Func="mozilla::dom::DOMPrefs::WebkitBlinkDirectoryPickerEnabled"]
   readonly attribute USVString webkitRelativePath;
 
   [GetterThrows, ChromeOnly, NeedsCallerType]
   readonly attribute DOMString mozFullPath;
 };
 
 // Mozilla extensions
 // These 2 methods can be used only in these conditions:
diff --git a/dom/workers/WorkerPrefs.h b/dom/workers/WorkerPrefs.h
--- a/dom/workers/WorkerPrefs.h
+++ b/dom/workers/WorkerPrefs.h
@@ -16,17 +16,16 @@
 //     macro in Workers.h.
 //   * The name of the function that updates the new value of a pref.
 //
 //   WORKER_PREF("foo.bar", UpdaterFunction)
 //
 //   * First argument is the name of the pref.
 //   * The name of the function that updates the new value of a pref.
 
-WORKER_SIMPLE_PREF("dom.webkitBlink.dirPicker.enabled", WebkitBlinkDirectoryPickerEnabled, DOM_WEBKITBLINK_DIRPICKER_WEBKITBLINK)
 WORKER_SIMPLE_PREF("dom.netinfo.enabled", NetworkInformationEnabled, NETWORKINFORMATION_ENABLED)
 WORKER_SIMPLE_PREF("dom.fetchObserver.enabled", FetchObserverEnabled, FETCHOBSERVER_ENABLED)
 WORKER_SIMPLE_PREF("privacy.resistFingerprinting", ResistFingerprintingEnabled, RESISTFINGERPRINTING_ENABLED)
 WORKER_SIMPLE_PREF("devtools.enabled", DevToolsEnabled, DEVTOOLS_ENABLED)
 WORKER_PREF("intl.accept_languages", PrefLanguagesChanged)
 WORKER_PREF("general.appname.override", AppNameOverrideChanged)
 WORKER_PREF("general.appversion.override", AppVersionOverrideChanged)
 WORKER_PREF("general.platform.override", PlatformOverrideChanged)
diff --git a/layout/forms/nsFileControlFrame.cpp b/layout/forms/nsFileControlFrame.cpp
--- a/layout/forms/nsFileControlFrame.cpp
+++ b/layout/forms/nsFileControlFrame.cpp
@@ -7,16 +7,17 @@
 #include "nsFileControlFrame.h"
 
 #include "nsGkAtoms.h"
 #include "nsCOMPtr.h"
 #include "nsIDocument.h"
 #include "mozilla/dom/NodeInfo.h"
 #include "mozilla/dom/Element.h"
 #include "mozilla/dom/DataTransfer.h"
+#include "mozilla/dom/DOMPrefs.h"
 #include "mozilla/dom/HTMLButtonElement.h"
 #include "mozilla/dom/HTMLInputElement.h"
 #include "mozilla/Preferences.h"
 #include "nsNodeInfoManager.h"
 #include "nsContentCreatorFunctions.h"
 #include "nsContentUtils.h"
 #include "mozilla/EventStates.h"
 #include "mozilla/dom/DOMStringList.h"
@@ -348,17 +349,17 @@ nsresult
 nsFileControlFrame::DnDListener::GetBlobImplForWebkitDirectory(nsIDOMFileList* aFileList,
                                                                BlobImpl** aBlobImpl)
 {
   *aBlobImpl = nullptr;
 
   HTMLInputElement* inputElement =
     HTMLInputElement::FromContent(mFrame->GetContent());
   bool webkitDirPicker =
-    Preferences::GetBool("dom.webkitBlink.dirPicker.enabled", false) &&
+    DOMPrefs::WebkitBlinkDirectoryPickerEnabled() &&
     inputElement->HasAttr(kNameSpaceID_None, nsGkAtoms::webkitdirectory);
   if (!webkitDirPicker) {
     return NS_OK;
   }
 
   if (!aFileList) {
     return NS_ERROR_FAILURE;
   }
