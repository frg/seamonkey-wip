# HG changeset patch
# User Toshihito Kikuchi <tkikuchi@mozilla.com>
# Date 1620581841 0
# Node ID 80e97d09f4228dd90ef55ae0bc430df51702919d
# Parent  cf2bf2c5713983a1326b89e76c86a395ff5a9fcf
Bug 1706501 - Make HandleCommandLine take nsACString. r=mossop, a=RyanVM

Differential Revision: https://phabricator.services.mozilla.com/D114691

diff --git a/toolkit/components/remote/nsWinRemoteServer.cpp b/toolkit/components/remote/nsWinRemoteServer.cpp
--- a/toolkit/components/remote/nsWinRemoteServer.cpp
+++ b/toolkit/components/remote/nsWinRemoteServer.cpp
@@ -42,106 +42,115 @@ static nsresult GetMostRecentWindow(mozI
       do_GetService(NS_WINDOWMEDIATOR_CONTRACTID, &rv));
   if (NS_FAILED(rv)) return rv;
 
   if (med) return med->GetMostRecentWindow(nullptr, aWindow);
 
   return NS_ERROR_FAILURE;
 }
 
-void HandleCommandLine(const char* aCmdLineString, nsIFile* aWorkingDir,
-                       uint32_t aState) {
+static void HandleCommandLine(const nsACString& aCmdLineString,
+                              nsIFile* aWorkingDir, uint32_t aState) {
+  if (aCmdLineString.IsEmpty()) {
+    return;
+  }
+
   nsresult rv;
 
   int justCounting = 1;
   char** argv = 0;
   // Flags, etc.
   int init = 1;
   int between, quoted, bSlashCount;
   int argc;
   const char* p;
+  const char* const pEnd = aCmdLineString.EndReading();
   nsAutoCString arg;
 
   nsCOMPtr<nsICommandLineRunner> cmdLine(new nsCommandLine());
 
   // Parse command line args according to MS spec
   // (see "Parsing C++ Command-Line Arguments" at
   // http://msdn.microsoft.com/library/devprods/vs6/visualc/vclang/_pluslang_parsing_c.2b2b_.command.2d.line_arguments.htm).
   // We loop if we've not finished the second pass through.
   while (1) {
     // Initialize if required.
     if (init) {
-      p = aCmdLineString;
+      p = aCmdLineString.BeginReading();
       between = 1;
       argc = quoted = bSlashCount = 0;
 
       init = 0;
     }
+
+    const char charCurr = (p < pEnd) ? *p : 0;
+    const char charNext = (p + 1 < pEnd) ? *(p + 1) : 0;
+
     if (between) {
       // We are traversing whitespace between args.
       // Check for start of next arg.
-      if (*p != 0 && !isspace(*p)) {
+      if (charCurr != 0 && !isspace(charCurr)) {
         // Start of another arg.
         between = 0;
         arg = "";
-        switch (*p) {
+        switch (charCurr) {
           case '\\':
             // Count the backslash.
             bSlashCount = 1;
             break;
           case '"':
             // Remember we're inside quotes.
             quoted = 1;
             break;
           default:
             // Add character to arg.
-            arg += *p;
+            arg += charCurr;
             break;
         }
       } else {
         // Another space between args, ignore it.
       }
     } else {
       // We are processing the contents of an argument.
       // Check for whitespace or end.
-      if (*p == 0 || (!quoted && isspace(*p))) {
+      if (charCurr == 0 || (!quoted && isspace(charCurr))) {
         // Process pending backslashes (interpret them
         // literally since they're not followed by a ").
         while (bSlashCount) {
           arg += '\\';
           bSlashCount--;
         }
         // End current arg.
         if (!justCounting) {
           argv[argc] = new char[arg.Length() + 1];
           strcpy(argv[argc], arg.get());
         }
         argc++;
         // We're now between args.
         between = 1;
       } else {
         // Still inside argument, process the character.
-        switch (*p) {
+        switch (charCurr) {
           case '"':
             // First, digest preceding backslashes (if any).
             while (bSlashCount > 1) {
               // Put one backsplash in arg for each pair.
               arg += '\\';
               bSlashCount -= 2;
             }
             if (bSlashCount) {
               // Quote is literal.
               arg += '"';
               bSlashCount = 0;
             } else {
               // Quote starts or ends a quoted section.
               if (quoted) {
                 // Check for special case of consecutive double
                 // quotes inside a quoted section.
-                if (*(p + 1) == '"') {
+                if (charNext == '"') {
                   // This implies a literal double-quote.  Fake that
                   // out by causing next double-quote to look as
                   // if it was preceded by a backslash.
                   bSlashCount = 1;
                 } else {
                   quoted = 0;
                 }
               } else {
@@ -155,23 +164,23 @@ void HandleCommandLine(const char* aCmdL
             break;
           default:
             // Accept any preceding backslashes literally.
             while (bSlashCount) {
               arg += '\\';
               bSlashCount--;
             }
             // Just add next char to the current arg.
-            arg += *p;
+            arg += charCurr;
             break;
         }
       }
     }
     // Check for end of input.
-    if (*p) {
+    if (charCurr) {
       // Go to next character.
       p++;
     } else {
       // If on first pass, go on to second.
       if (justCounting) {
         // Allocate argv array.
         argv = new char*[argc];
 
@@ -197,34 +206,34 @@ void HandleCommandLine(const char* aCmdL
     NS_ERROR("Error initializing command line.");
     return;
   }
 
   cmdLine->Run();
 }
 
 LRESULT CALLBACK WindowProc(HWND msgWindow, UINT msg, WPARAM wp, LPARAM lp) {
-  if (msg == WM_COPYDATA) {
+  if (msg == WM_COPYDATA && lp) {
     // This is an incoming request.
-    COPYDATASTRUCT* cds = (COPYDATASTRUCT*)lp;
+    const COPYDATASTRUCT* cds = reinterpret_cast<const COPYDATASTRUCT*>(lp);
+    const nsDependentCSubstring messageData(reinterpret_cast<char*>(cds->lpData), cds->cbData);
+
     nsCOMPtr<nsIFile> workingDir;
 
     if (1 >= cds->dwData) {
-      char* wdpath = (char*)cds->lpData;
       // skip the command line, and get the working dir of the
       // other process, which is after the first null char
-      while (*wdpath) ++wdpath;
-
-      ++wdpath;
-
-      NS_NewLocalFile(NS_ConvertUTF8toUTF16(wdpath), false,
-                      getter_AddRefs(workingDir));
+      int32_t posNullChar = messageData.FindChar('\0');
+      if (posNullChar != kNotFound) {
+        NS_NewLocalFile(
+            NS_ConvertUTF8toUTF16(Substring(messageData, posNullChar + 1)),
+            false, getter_AddRefs(workingDir));
+      }
     }
-    HandleCommandLine((char*)cds->lpData, workingDir,
-                      nsICommandLine::STATE_REMOTE_AUTO);
+    HandleCommandLine(messageData, workingDir, nsICommandLine::STATE_REMOTE_AUTO);
 
     // Get current window and return its window handle.
     nsCOMPtr<mozIDOMWindowProxy> win;
     GetMostRecentWindow(getter_AddRefs(win));
     return win ? (LRESULT)hwndForDOMWindow(win) : 0;
   }
   return DefWindowProc(msgWindow, msg, wp, lp);
 }

