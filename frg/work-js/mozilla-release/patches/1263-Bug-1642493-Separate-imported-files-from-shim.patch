From 3af873b4118f16cb16533c8c4d9e4957aedb7bfb Mon Sep 17 00:00:00 2001
From: hawkeye116477 <hawkeye116477@gmail.com>
Date: Fri, 1 Oct 2021 18:31:00 +0200
Subject: [PATCH 1263/1328] Bug 1642493: Separate imported files from shim

This patch moves the files that are directly imported from V8 into their own subdirectory of `irregexp`, to make it more obvious which code is part of V8 and which code is part of the shim. I also took the opportunity to rename the shim files to follow our naming conventions (FileName.cpp) instead of V8's (file-name.cc).

Most of this code is mechanical. The mildly interesting parts are:

1. In `import-irregexp.py`, when figuring out where the regexp shim include should go, we no longer have to work quite as hard to alphabetize it, because `irregexp/RegExpShim.h` will always come after `irregexp/imported/*`. Instead, we just keep scanning until we find a newline.

2. Some of the V8 code #includes "regexp-macro-assembler-arch.h". Up until now, that's the name we used for the header file for our RegExpMacroAssembler implementation. Now that the implementation lives in a separate directory from the V8 code, I've renamed the header to `RegExpNativeMacroAssembler.h` (to match the .cpp file), and added a stub `regexp-macro-assembler-arch.h` header in `imported` that #includes the real thing.

In the next patch, we take advantage of the new directory structure to get more precise with clang-format-ignore.
---
 js/src/irregexp/RegExpAPI.cpp                 | 16 ++++-----
 ...bler.cc => RegExpNativeMacroAssembler.cpp} |  4 +--
 ...er-arch.h => RegExpNativeMacroAssembler.h} |  2 +-
 .../{regexp-shim.cc => RegExpShim.cpp}        |  7 ++--
 .../irregexp/{regexp-shim.h => RegExpShim.h}  | 14 ++++----
 js/src/irregexp/import-irregexp.py            | 20 +++++------
 .../{ => imported}/gen-regexp-special-case.cc |  2 +-
 .../{ => imported}/property-sequences.cc      |  2 +-
 .../{ => imported}/property-sequences.h       |  2 +-
 js/src/irregexp/{ => imported}/regexp-ast.cc  |  2 +-
 js/src/irregexp/{ => imported}/regexp-ast.h   |  2 +-
 .../regexp-bytecode-generator-inl.h           |  4 +--
 .../regexp-bytecode-generator.cc              | 10 +++---
 .../regexp-bytecode-generator.h               |  2 +-
 .../regexp-bytecode-peephole.cc               |  4 +--
 .../{ => imported}/regexp-bytecode-peephole.h |  2 +-
 .../{ => imported}/regexp-bytecodes.cc        |  2 +-
 .../{ => imported}/regexp-bytecodes.h         |  2 +-
 .../{ => imported}/regexp-compiler-tonode.cc  |  6 ++--
 .../{ => imported}/regexp-compiler.cc         |  6 ++--
 .../irregexp/{ => imported}/regexp-compiler.h |  2 +-
 .../{ => imported}/regexp-dotprinter.cc       |  4 +--
 .../{ => imported}/regexp-dotprinter.h        |  2 +-
 .../irregexp/{ => imported}/regexp-error.cc   |  2 +-
 js/src/irregexp/{ => imported}/regexp-error.h |  2 +-
 .../{ => imported}/regexp-interpreter.cc      | 10 +++---
 .../{ => imported}/regexp-interpreter.h       |  2 +-
 .../imported/regexp-macro-assembler-arch.h    |  7 ++++
 .../regexp-macro-assembler-tracer.cc          |  2 +-
 .../regexp-macro-assembler-tracer.h           |  2 +-
 .../{ => imported}/regexp-macro-assembler.cc  |  4 +--
 .../{ => imported}/regexp-macro-assembler.h   |  6 ++--
 js/src/irregexp/{ => imported}/regexp-nodes.h |  2 +-
 .../irregexp/{ => imported}/regexp-parser.cc  |  8 ++---
 .../irregexp/{ => imported}/regexp-parser.h   |  4 +--
 .../irregexp/{ => imported}/regexp-stack.cc   |  2 +-
 js/src/irregexp/{ => imported}/regexp-stack.h |  2 +-
 js/src/irregexp/{ => imported}/regexp.h       |  4 +--
 .../irregexp/{ => imported}/special-case.cc   |  2 +-
 js/src/irregexp/{ => imported}/special-case.h |  2 +-
 js/src/irregexp/moz.build                     | 34 +++++++++----------
 js/src/irregexp/util/{flags.h => FlagsShim.h} |  0
 .../util/{unicode.cc => UnicodeShim.cpp}      |  2 +-
 .../irregexp/util/{vector.h => VectorShim.h}  |  2 +-
 js/src/irregexp/util/{zone.h => ZoneShim.h}   |  2 +-
 js/src/moz.build                              |  2 +-
 46 files changed, 116 insertions(+), 108 deletions(-)
 rename js/src/irregexp/{regexp-native-macro-assembler.cc => RegExpNativeMacroAssembler.cpp} (99%)
 rename js/src/irregexp/{regexp-macro-assembler-arch.h => RegExpNativeMacroAssembler.h} (99%)
 rename js/src/irregexp/{regexp-shim.cc => RegExpShim.cpp} (98%)
 rename js/src/irregexp/{regexp-shim.h => RegExpShim.h} (99%)
 rename js/src/irregexp/{ => imported}/gen-regexp-special-case.cc (99%)
 rename js/src/irregexp/{ => imported}/property-sequences.cc (99%)
 rename js/src/irregexp/{ => imported}/property-sequences.h (94%)
 rename js/src/irregexp/{ => imported}/regexp-ast.cc (99%)
 rename js/src/irregexp/{ => imported}/regexp-ast.h (99%)
 rename js/src/irregexp/{ => imported}/regexp-bytecode-generator-inl.h (92%)
 rename js/src/irregexp/{ => imported}/regexp-bytecode-generator.cc (97%)
 rename js/src/irregexp/{ => imported}/regexp-bytecode-generator.h (98%)
 rename js/src/irregexp/{ => imported}/regexp-bytecode-peephole.cc (99%)
 rename js/src/irregexp/{ => imported}/regexp-bytecode-peephole.h (96%)
 rename js/src/irregexp/{ => imported}/regexp-bytecodes.cc (95%)
 rename js/src/irregexp/{ => imported}/regexp-bytecodes.h (99%)
 rename js/src/irregexp/{ => imported}/regexp-compiler-tonode.cc (99%)
 rename js/src/irregexp/{ => imported}/regexp-compiler.cc (99%)
 rename js/src/irregexp/{ => imported}/regexp-compiler.h (99%)
 rename js/src/irregexp/{ => imported}/regexp-dotprinter.cc (98%)
 rename js/src/irregexp/{ => imported}/regexp-dotprinter.h (93%)
 rename js/src/irregexp/{ => imported}/regexp-error.cc (92%)
 rename js/src/irregexp/{ => imported}/regexp-error.h (98%)
 rename js/src/irregexp/{ => imported}/regexp-interpreter.cc (99%)
 rename js/src/irregexp/{ => imported}/regexp-interpreter.h (98%)
 create mode 100644 js/src/irregexp/imported/regexp-macro-assembler-arch.h
 rename js/src/irregexp/{ => imported}/regexp-macro-assembler-tracer.cc (99%)
 rename js/src/irregexp/{ => imported}/regexp-macro-assembler-tracer.h (98%)
 rename js/src/irregexp/{ => imported}/regexp-macro-assembler.cc (99%)
 rename js/src/irregexp/{ => imported}/regexp-macro-assembler.h (99%)
 rename js/src/irregexp/{ => imported}/regexp-nodes.h (99%)
 rename js/src/irregexp/{ => imported}/regexp-parser.cc (99%)
 rename js/src/irregexp/{ => imported}/regexp-parser.h (99%)
 rename js/src/irregexp/{ => imported}/regexp-stack.cc (98%)
 rename js/src/irregexp/{ => imported}/regexp-stack.h (99%)
 rename js/src/irregexp/{ => imported}/regexp.h (98%)
 rename js/src/irregexp/{ => imported}/special-case.cc (97%)
 rename js/src/irregexp/{ => imported}/special-case.h (99%)
 rename js/src/irregexp/util/{flags.h => FlagsShim.h} (100%)
 rename js/src/irregexp/util/{unicode.cc => UnicodeShim.cpp} (99%)
 rename js/src/irregexp/util/{vector.h => VectorShim.h} (99%)
 rename js/src/irregexp/util/{zone.h => ZoneShim.h} (99%)

diff --git a/js/src/irregexp/RegExpAPI.cpp b/js/src/irregexp/RegExpAPI.cpp
index 44d049a0d8b8..7a023698487f 100644
--- a/js/src/irregexp/RegExpAPI.cpp
+++ b/js/src/irregexp/RegExpAPI.cpp
@@ -12,14 +12,14 @@
 #include "mozilla/ArrayUtils.h"
 #include "mozilla/Casting.h"
 
-#include "irregexp/regexp-bytecode-generator.h"
-#include "irregexp/regexp-compiler.h"
-#include "irregexp/regexp-interpreter.h"
-#include "irregexp/regexp-macro-assembler-arch.h"
-#include "irregexp/regexp-parser.h"
-#include "irregexp/regexp-shim.h"
-#include "irregexp/regexp-stack.h"
-#include "irregexp/regexp.h"
+#include "irregexp/imported/regexp-bytecode-generator.h"
+#include "irregexp/imported/regexp-compiler.h"
+#include "irregexp/imported/regexp-interpreter.h"
+#include "irregexp/imported/regexp-macro-assembler-arch.h"
+#include "irregexp/imported/regexp-parser.h"
+#include "irregexp/imported/regexp-stack.h"
+#include "irregexp/imported/regexp.h"
+#include "irregexp/RegExpShim.h"
 #include "jit/JitCommon.h"
 
 #include "util/Text.h"
diff --git a/js/src/irregexp/regexp-native-macro-assembler.cc b/js/src/irregexp/RegExpNativeMacroAssembler.cpp
similarity index 99%
rename from js/src/irregexp/regexp-native-macro-assembler.cc
rename to js/src/irregexp/RegExpNativeMacroAssembler.cpp
index 7667958e54dd..d534be4df075 100644
--- a/js/src/irregexp/regexp-native-macro-assembler.cc
+++ b/js/src/irregexp/RegExpNativeMacroAssembler.cpp
@@ -7,10 +7,10 @@
 // Use of this source code is governed by a BSD-style license that can be
 // found in the LICENSE file.
 
+#include "irregexp/imported/regexp-macro-assembler-arch.h"
+#include "irregexp/imported/regexp-stack.h"
 #include "jit/Linker.h"
 #include "gc/Zone.h"
-#include "irregexp/regexp-macro-assembler-arch.h"
-#include "irregexp/regexp-stack.h"
 #include "vm/MatchPairs.h"
 
 #include "jit/MacroAssembler-inl.h"
diff --git a/js/src/irregexp/regexp-macro-assembler-arch.h b/js/src/irregexp/RegExpNativeMacroAssembler.h
similarity index 99%
rename from js/src/irregexp/regexp-macro-assembler-arch.h
rename to js/src/irregexp/RegExpNativeMacroAssembler.h
index bd38e5ab0f19..b2982ffd29b8 100644
--- a/js/src/irregexp/regexp-macro-assembler-arch.h
+++ b/js/src/irregexp/RegExpNativeMacroAssembler.h
@@ -14,7 +14,7 @@
 #ifndef RegexpMacroAssemblerArch_h
 #define RegexpMacroAssemblerArch_h
 
-#include "irregexp/regexp-macro-assembler.h"
+#include "irregexp/imported/regexp-macro-assembler.h"
 #include "jit/MacroAssembler.h"
 
 namespace v8 {
diff --git a/js/src/irregexp/regexp-shim.cc b/js/src/irregexp/RegExpShim.cpp
similarity index 98%
rename from js/src/irregexp/regexp-shim.cc
rename to js/src/irregexp/RegExpShim.cpp
index 84402b31e35f..399e4c47903a 100644
--- a/js/src/irregexp/regexp-shim.cc
+++ b/js/src/irregexp/RegExpShim.cpp
@@ -7,11 +7,12 @@
 // Use of this source code is governed by a BSD-style license that can be
 // found in the LICENSE file.
 
+#include "irregexp/RegExpShim.h"
+
 #include <iostream>
 
-#include "irregexp/regexp-macro-assembler.h"
-#include "irregexp/regexp-shim.h"
-#include "irregexp/regexp-stack.h"
+#include "irregexp/imported/regexp-macro-assembler.h"
+#include "irregexp/imported/regexp-stack.h"
 
 #include "vm/NativeObject-inl.h"
 
diff --git a/js/src/irregexp/regexp-shim.h b/js/src/irregexp/RegExpShim.h
similarity index 99%
rename from js/src/irregexp/regexp-shim.h
rename to js/src/irregexp/RegExpShim.h
index def18de880cb..051c53daef13 100644
--- a/js/src/irregexp/regexp-shim.h
+++ b/js/src/irregexp/RegExpShim.h
@@ -23,9 +23,9 @@
 #include <iostream> // needed for gcc 10
 
 #include "irregexp/RegExpTypes.h"
-#include "irregexp/util/flags.h"
-#include "irregexp/util/vector.h"
-#include "irregexp/util/zone.h"
+#include "irregexp/util/FlagsShim.h"
+#include "irregexp/util/VectorShim.h"
+#include "irregexp/util/ZoneShim.h"
 #include "jit/Label.h"
 #include "jit/shared/Assembler-shared.h"
 #include "js/Value.h"
@@ -47,7 +47,7 @@ class RegExpStack;
 
 #define V8_WARN_UNUSED_RESULT MOZ_MUST_USE
 #define V8_EXPORT_PRIVATE MOZ_EXPORT
-#define V8_FALLTHROUGH MOZ_FALLTHROUGH
+#define V8_FALLTHROUGH [[fallthrough]]
 
 #define FATAL(x) MOZ_CRASH(x)
 #define UNREACHABLE() MOZ_CRASH("unreachable code")
@@ -962,9 +962,9 @@ class JSRegExp : public HeapObject {
   static constexpr int kNoBacktrackLimit = 0;
 
 private:
-  js::RegExpShared* inner() const {
-    return reinterpret_cast<js::RegExpShared*>(value().toGCThing());
-  }
+ js::RegExpShared* inner() const {
+   return value().toGCThing()->as<js::RegExpShared>();
+ }
 };
 
 class Histogram {
diff --git a/js/src/irregexp/import-irregexp.py b/js/src/irregexp/import-irregexp.py
index d0849f32c94b..691adab8800c 100644
--- a/js/src/irregexp/import-irregexp.py
+++ b/js/src/irregexp/import-irregexp.py
@@ -7,12 +7,12 @@
 # This script handles all the mechanical steps of importing irregexp from v8:
 #
 # 1. Acquire the source: either from github, or optionally from a local copy of v8.
-# 2. Copy the contents of v8/src/regexp into js/src/irregexp
+# 2. Copy the contents of v8/src/regexp into js/src/irregexp/imported
 #    - Exclude files that we have chosen not to import.
 # 3. While doing so, update #includes:
-#    - Change "src/regexp/*" to "irregexp/*".
+#    - Change "src/regexp/*" to "irregexp/imported/*".
 #    - Remove other v8-specific headers completely.
-# 4. Add '#include "irregexp/regexp-shim.h" in the necessary places.
+# 4. Add '#include "irregexp/RegExpShim.h" in the necessary places.
 # 5. Update the IRREGEXP_VERSION file to include the correct git hash.
 #
 # Usage:
@@ -60,25 +60,25 @@ def copy_and_update_includes(src_path, dst_path):
 
     # 1. Rewrite includes of V8 regexp headers:
     regexp_include = re.compile('#include "src/regexp')
-    regexp_include_new = '#include "irregexp'
+    regexp_include_new = '#include "irregexp/imported'
 
     # 2. Remove includes of other V8 headers
     other_include = re.compile('#include "src/')
 
-    # 3. If needed, add '#include "irregexp/regexp-shim.h"'.
+    # 3. If needed, add '#include "irregexp/RegExpShim.h"'.
     #    Note: We get a little fancy to ensure that header files are
     #    in alphabetic order. `need_to_add_shim` is true if we still
     #    have to add the shim header in this file. `adding_shim_now`
     #    is true if we have found a '#include "src/*' and we are just
-    #    waiting to find something alphabetically smaller (or an empty
-    #    line) so that we can insert the shim header in the right place.
+    #    waiting to find an empty line so that we can insert the shim
+    #    header in the right place.
     need_to_add_shim = src_path.name in need_shim
     adding_shim_now = False
 
     for line in src:
         if adding_shim_now:
-            if (line == '\n' or line > '#include "src/regexp/regexp-shim.h"'):
-                dst.write('#include "irregexp/regexp-shim.h"\n')
+            if line == '\n':
+                dst.write('#include "irregexp/RegExpShim.h"\n')
                 need_to_add_shim = False
                 adding_shim_now = False
 
@@ -103,7 +103,7 @@ def import_from(srcdir, dstdir):
             continue
         if str(file.name) in excluded:
             continue
-        copy_and_update_includes(file, dstdir / file.name)
+        copy_and_update_includes(file, dstdir / "imported" / file.name)
 
     # Update IRREGEXP_VERSION file
     hash = get_hash(srcdir)
diff --git a/js/src/irregexp/gen-regexp-special-case.cc b/js/src/irregexp/imported/gen-regexp-special-case.cc
similarity index 99%
rename from js/src/irregexp/gen-regexp-special-case.cc
rename to js/src/irregexp/imported/gen-regexp-special-case.cc
index 13b9a118acbf..89bd863509f3 100644
--- a/js/src/irregexp/gen-regexp-special-case.cc
+++ b/js/src/irregexp/imported/gen-regexp-special-case.cc
@@ -7,7 +7,7 @@
 #include <iostream>
 #include <sstream>
 
-#include "irregexp/special-case.h"
+#include "irregexp/imported/special-case.h"
 
 namespace v8 {
 namespace internal {
diff --git a/js/src/irregexp/property-sequences.cc b/js/src/irregexp/imported/property-sequences.cc
similarity index 99%
rename from js/src/irregexp/property-sequences.cc
rename to js/src/irregexp/imported/property-sequences.cc
index 6eb1122f2475..f1a6180b4ba5 100644
--- a/js/src/irregexp/property-sequences.cc
+++ b/js/src/irregexp/imported/property-sequences.cc
@@ -4,7 +4,7 @@
 
 #ifdef V8_INTL_SUPPORT
 
-#include "irregexp/property-sequences.h"
+#include "irregexp/imported/property-sequences.h"
 
 namespace v8 {
 namespace internal {
diff --git a/js/src/irregexp/property-sequences.h b/js/src/irregexp/imported/property-sequences.h
similarity index 94%
rename from js/src/irregexp/property-sequences.h
rename to js/src/irregexp/imported/property-sequences.h
index 5f141bf004a6..204f28c24ef8 100644
--- a/js/src/irregexp/property-sequences.h
+++ b/js/src/irregexp/imported/property-sequences.h
@@ -7,7 +7,7 @@
 
 #ifdef V8_INTL_SUPPORT
 
-#include "irregexp/regexp-shim.h"
+#include "irregexp/RegExpShim.h"
 
 namespace v8 {
 namespace internal {
diff --git a/js/src/irregexp/regexp-ast.cc b/js/src/irregexp/imported/regexp-ast.cc
similarity index 99%
rename from js/src/irregexp/regexp-ast.cc
rename to js/src/irregexp/imported/regexp-ast.cc
index 19e16d46cf84..4b89e7fef27d 100644
--- a/js/src/irregexp/regexp-ast.cc
+++ b/js/src/irregexp/imported/regexp-ast.cc
@@ -2,7 +2,7 @@
 // Use of this source code is governed by a BSD-style license that can be
 // found in the LICENSE file.
 
-#include "irregexp/regexp-ast.h"
+#include "irregexp/imported/regexp-ast.h"
 
 namespace v8 {
 namespace internal {
diff --git a/js/src/irregexp/regexp-ast.h b/js/src/irregexp/imported/regexp-ast.h
similarity index 99%
rename from js/src/irregexp/regexp-ast.h
rename to js/src/irregexp/imported/regexp-ast.h
index e8c48789f5ec..d94be3893fa0 100644
--- a/js/src/irregexp/regexp-ast.h
+++ b/js/src/irregexp/imported/regexp-ast.h
@@ -5,7 +5,7 @@
 #ifndef V8_REGEXP_REGEXP_AST_H_
 #define V8_REGEXP_REGEXP_AST_H_
 
-#include "irregexp/regexp-shim.h"
+#include "irregexp/RegExpShim.h"
 
 namespace v8 {
 namespace internal {
diff --git a/js/src/irregexp/regexp-bytecode-generator-inl.h b/js/src/irregexp/imported/regexp-bytecode-generator-inl.h
similarity index 92%
rename from js/src/irregexp/regexp-bytecode-generator-inl.h
rename to js/src/irregexp/imported/regexp-bytecode-generator-inl.h
index 28dc76721123..c7cf212d0ff4 100644
--- a/js/src/irregexp/regexp-bytecode-generator-inl.h
+++ b/js/src/irregexp/imported/regexp-bytecode-generator-inl.h
@@ -5,9 +5,9 @@
 #ifndef V8_REGEXP_REGEXP_BYTECODE_GENERATOR_INL_H_
 #define V8_REGEXP_REGEXP_BYTECODE_GENERATOR_INL_H_
 
-#include "irregexp/regexp-bytecode-generator.h"
+#include "irregexp/imported/regexp-bytecode-generator.h"
 
-#include "irregexp/regexp-bytecodes.h"
+#include "irregexp/imported/regexp-bytecodes.h"
 
 namespace v8 {
 namespace internal {
diff --git a/js/src/irregexp/regexp-bytecode-generator.cc b/js/src/irregexp/imported/regexp-bytecode-generator.cc
similarity index 97%
rename from js/src/irregexp/regexp-bytecode-generator.cc
rename to js/src/irregexp/imported/regexp-bytecode-generator.cc
index 492c528ce55f..143a141f7a70 100644
--- a/js/src/irregexp/regexp-bytecode-generator.cc
+++ b/js/src/irregexp/imported/regexp-bytecode-generator.cc
@@ -2,12 +2,12 @@
 // Use of this source code is governed by a BSD-style license that can be
 // found in the LICENSE file.
 
-#include "irregexp/regexp-bytecode-generator.h"
+#include "irregexp/imported/regexp-bytecode-generator.h"
 
-#include "irregexp/regexp-bytecode-generator-inl.h"
-#include "irregexp/regexp-bytecode-peephole.h"
-#include "irregexp/regexp-bytecodes.h"
-#include "irregexp/regexp-macro-assembler.h"
+#include "irregexp/imported/regexp-bytecode-generator-inl.h"
+#include "irregexp/imported/regexp-bytecode-peephole.h"
+#include "irregexp/imported/regexp-bytecodes.h"
+#include "irregexp/imported/regexp-macro-assembler.h"
 
 namespace v8 {
 namespace internal {
diff --git a/js/src/irregexp/regexp-bytecode-generator.h b/js/src/irregexp/imported/regexp-bytecode-generator.h
similarity index 98%
rename from js/src/irregexp/regexp-bytecode-generator.h
rename to js/src/irregexp/imported/regexp-bytecode-generator.h
index 3a2bd62db5d1..648edd293724 100644
--- a/js/src/irregexp/regexp-bytecode-generator.h
+++ b/js/src/irregexp/imported/regexp-bytecode-generator.h
@@ -5,7 +5,7 @@
 #ifndef V8_REGEXP_REGEXP_BYTECODE_GENERATOR_H_
 #define V8_REGEXP_REGEXP_BYTECODE_GENERATOR_H_
 
-#include "irregexp/regexp-macro-assembler.h"
+#include "irregexp/imported/regexp-macro-assembler.h"
 
 namespace v8 {
 namespace internal {
diff --git a/js/src/irregexp/regexp-bytecode-peephole.cc b/js/src/irregexp/imported/regexp-bytecode-peephole.cc
similarity index 99%
rename from js/src/irregexp/regexp-bytecode-peephole.cc
rename to js/src/irregexp/imported/regexp-bytecode-peephole.cc
index a82149c6895c..9daab1e08e29 100644
--- a/js/src/irregexp/regexp-bytecode-peephole.cc
+++ b/js/src/irregexp/imported/regexp-bytecode-peephole.cc
@@ -2,9 +2,9 @@
 // Use of this source code is governed by a BSD-style license that can be
 // found in the LICENSE file.
 
-#include "irregexp/regexp-bytecode-peephole.h"
+#include "irregexp/imported/regexp-bytecode-peephole.h"
 
-#include "irregexp/regexp-bytecodes.h"
+#include "irregexp/imported/regexp-bytecodes.h"
 
 namespace v8 {
 namespace internal {
diff --git a/js/src/irregexp/regexp-bytecode-peephole.h b/js/src/irregexp/imported/regexp-bytecode-peephole.h
similarity index 96%
rename from js/src/irregexp/regexp-bytecode-peephole.h
rename to js/src/irregexp/imported/regexp-bytecode-peephole.h
index d64adc078807..5b8a0c7b4bc2 100644
--- a/js/src/irregexp/regexp-bytecode-peephole.h
+++ b/js/src/irregexp/imported/regexp-bytecode-peephole.h
@@ -5,7 +5,7 @@
 #ifndef V8_REGEXP_REGEXP_BYTECODE_PEEPHOLE_H_
 #define V8_REGEXP_REGEXP_BYTECODE_PEEPHOLE_H_
 
-#include "irregexp/regexp-shim.h"
+#include "irregexp/RegExpShim.h"
 
 namespace v8 {
 namespace internal {
diff --git a/js/src/irregexp/regexp-bytecodes.cc b/js/src/irregexp/imported/regexp-bytecodes.cc
similarity index 95%
rename from js/src/irregexp/regexp-bytecodes.cc
rename to js/src/irregexp/imported/regexp-bytecodes.cc
index 507a90acca3a..1db70112031c 100644
--- a/js/src/irregexp/regexp-bytecodes.cc
+++ b/js/src/irregexp/imported/regexp-bytecodes.cc
@@ -2,7 +2,7 @@
 // Use of this source code is governed by a BSD-style license that can be
 // found in the LICENSE file.
 
-#include "irregexp/regexp-bytecodes.h"
+#include "irregexp/imported/regexp-bytecodes.h"
 
 #include <cctype>
 
diff --git a/js/src/irregexp/regexp-bytecodes.h b/js/src/irregexp/imported/regexp-bytecodes.h
similarity index 99%
rename from js/src/irregexp/regexp-bytecodes.h
rename to js/src/irregexp/imported/regexp-bytecodes.h
index a3a7f1c1ffb7..d9fcfaeb1c5d 100644
--- a/js/src/irregexp/regexp-bytecodes.h
+++ b/js/src/irregexp/imported/regexp-bytecodes.h
@@ -5,7 +5,7 @@
 #ifndef V8_REGEXP_REGEXP_BYTECODES_H_
 #define V8_REGEXP_REGEXP_BYTECODES_H_
 
-#include "irregexp/regexp-shim.h"
+#include "irregexp/RegExpShim.h"
 
 namespace v8 {
 namespace internal {
diff --git a/js/src/irregexp/regexp-compiler-tonode.cc b/js/src/irregexp/imported/regexp-compiler-tonode.cc
similarity index 99%
rename from js/src/irregexp/regexp-compiler-tonode.cc
rename to js/src/irregexp/imported/regexp-compiler-tonode.cc
index 163bf3d09f3f..2a4c5edbc86c 100644
--- a/js/src/irregexp/regexp-compiler-tonode.cc
+++ b/js/src/irregexp/imported/regexp-compiler-tonode.cc
@@ -2,11 +2,11 @@
 // Use of this source code is governed by a BSD-style license that can be
 // found in the LICENSE file.
 
-#include "irregexp/regexp-compiler.h"
+#include "irregexp/imported/regexp-compiler.h"
 
-#include "irregexp/regexp.h"
+#include "irregexp/imported/regexp.h"
 #ifdef V8_INTL_SUPPORT
-#include "irregexp/special-case.h"
+#include "irregexp/imported/special-case.h"
 #endif  // V8_INTL_SUPPORT
 
 #ifdef V8_INTL_SUPPORT
diff --git a/js/src/irregexp/regexp-compiler.cc b/js/src/irregexp/imported/regexp-compiler.cc
similarity index 99%
rename from js/src/irregexp/regexp-compiler.cc
rename to js/src/irregexp/imported/regexp-compiler.cc
index d5bd1ead8708..9eb2ce5aa543 100644
--- a/js/src/irregexp/regexp-compiler.cc
+++ b/js/src/irregexp/imported/regexp-compiler.cc
@@ -2,11 +2,11 @@
 // Use of this source code is governed by a BSD-style license that can be
 // found in the LICENSE file.
 
-#include "irregexp/regexp-compiler.h"
+#include "irregexp/imported/regexp-compiler.h"
 
-#include "irregexp/regexp-macro-assembler-arch.h"
+#include "irregexp/imported/regexp-macro-assembler-arch.h"
 #ifdef V8_INTL_SUPPORT
-#include "irregexp/special-case.h"
+#include "irregexp/imported/special-case.h"
 #endif  // V8_INTL_SUPPORT
 
 #ifdef V8_INTL_SUPPORT
diff --git a/js/src/irregexp/regexp-compiler.h b/js/src/irregexp/imported/regexp-compiler.h
similarity index 99%
rename from js/src/irregexp/regexp-compiler.h
rename to js/src/irregexp/imported/regexp-compiler.h
index 8b3868ce035a..290718095423 100644
--- a/js/src/irregexp/regexp-compiler.h
+++ b/js/src/irregexp/imported/regexp-compiler.h
@@ -7,7 +7,7 @@
 
 #include <bitset>
 
-#include "irregexp/regexp-nodes.h"
+#include "irregexp/imported/regexp-nodes.h"
 
 namespace v8 {
 namespace internal {
diff --git a/js/src/irregexp/regexp-dotprinter.cc b/js/src/irregexp/imported/regexp-dotprinter.cc
similarity index 98%
rename from js/src/irregexp/regexp-dotprinter.cc
rename to js/src/irregexp/imported/regexp-dotprinter.cc
index 67bc29b26107..baedd566f8ed 100644
--- a/js/src/irregexp/regexp-dotprinter.cc
+++ b/js/src/irregexp/imported/regexp-dotprinter.cc
@@ -2,9 +2,9 @@
 // Use of this source code is governed by a BSD-style license that can be
 // found in the LICENSE file.
 
-#include "irregexp/regexp-dotprinter.h"
+#include "irregexp/imported/regexp-dotprinter.h"
 
-#include "irregexp/regexp-compiler.h"
+#include "irregexp/imported/regexp-compiler.h"
 
 namespace v8 {
 namespace internal {
diff --git a/js/src/irregexp/regexp-dotprinter.h b/js/src/irregexp/imported/regexp-dotprinter.h
similarity index 93%
rename from js/src/irregexp/regexp-dotprinter.h
rename to js/src/irregexp/imported/regexp-dotprinter.h
index b53e8f16f1ae..7fcece6e1af3 100644
--- a/js/src/irregexp/regexp-dotprinter.h
+++ b/js/src/irregexp/imported/regexp-dotprinter.h
@@ -5,7 +5,7 @@
 #ifndef V8_REGEXP_REGEXP_DOTPRINTER_H_
 #define V8_REGEXP_REGEXP_DOTPRINTER_H_
 
-#include "irregexp/regexp-shim.h"
+#include "irregexp/RegExpShim.h"
 
 namespace v8 {
 namespace internal {
diff --git a/js/src/irregexp/regexp-error.cc b/js/src/irregexp/imported/regexp-error.cc
similarity index 92%
rename from js/src/irregexp/regexp-error.cc
rename to js/src/irregexp/imported/regexp-error.cc
index 1a29f156cbcc..d0b4c263a493 100644
--- a/js/src/irregexp/regexp-error.cc
+++ b/js/src/irregexp/imported/regexp-error.cc
@@ -2,7 +2,7 @@
 // Use of this source code is governed by a BSD-style license that can be
 // found in the LICENSE file.
 
-#include "irregexp/regexp-error.h"
+#include "irregexp/imported/regexp-error.h"
 
 namespace v8 {
 namespace internal {
diff --git a/js/src/irregexp/regexp-error.h b/js/src/irregexp/imported/regexp-error.h
similarity index 98%
rename from js/src/irregexp/regexp-error.h
rename to js/src/irregexp/imported/regexp-error.h
index 4b495f07d16b..1f347306b78a 100644
--- a/js/src/irregexp/regexp-error.h
+++ b/js/src/irregexp/imported/regexp-error.h
@@ -5,7 +5,7 @@
 #ifndef V8_REGEXP_REGEXP_ERROR_H_
 #define V8_REGEXP_REGEXP_ERROR_H_
 
-#include "regexp-shim.h"
+#include "irregexp/RegExpShim.h"
 
 namespace v8 {
 namespace internal {
diff --git a/js/src/irregexp/regexp-interpreter.cc b/js/src/irregexp/imported/regexp-interpreter.cc
similarity index 99%
rename from js/src/irregexp/regexp-interpreter.cc
rename to js/src/irregexp/imported/regexp-interpreter.cc
index 5816e5defa3a..6cff2f480732 100644
--- a/js/src/irregexp/regexp-interpreter.cc
+++ b/js/src/irregexp/imported/regexp-interpreter.cc
@@ -4,12 +4,12 @@
 
 // A simple interpreter for the Irregexp byte code.
 
-#include "irregexp/regexp-interpreter.h"
+#include "irregexp/imported/regexp-interpreter.h"
 
-#include "irregexp/regexp-bytecodes.h"
-#include "irregexp/regexp-macro-assembler.h"
-#include "irregexp/regexp-stack.h"  // For kMaximumStackSize.
-#include "irregexp/regexp.h"
+#include "irregexp/imported/regexp-bytecodes.h"
+#include "irregexp/imported/regexp-macro-assembler.h"
+#include "irregexp/imported/regexp-stack.h"  // For kMaximumStackSize.
+#include "irregexp/imported/regexp.h"
 
 #ifdef V8_INTL_SUPPORT
 #include "unicode/uchar.h"
diff --git a/js/src/irregexp/regexp-interpreter.h b/js/src/irregexp/imported/regexp-interpreter.h
similarity index 98%
rename from js/src/irregexp/regexp-interpreter.h
rename to js/src/irregexp/imported/regexp-interpreter.h
index 7b1661206cf8..4fa29b6cb7ec 100644
--- a/js/src/irregexp/regexp-interpreter.h
+++ b/js/src/irregexp/imported/regexp-interpreter.h
@@ -7,7 +7,7 @@
 #ifndef V8_REGEXP_REGEXP_INTERPRETER_H_
 #define V8_REGEXP_REGEXP_INTERPRETER_H_
 
-#include "irregexp/regexp.h"
+#include "irregexp/imported/regexp.h"
 
 namespace v8 {
 namespace internal {
diff --git a/js/src/irregexp/imported/regexp-macro-assembler-arch.h b/js/src/irregexp/imported/regexp-macro-assembler-arch.h
new file mode 100644
index 000000000000..a755e7c1b3ce
--- /dev/null
+++ b/js/src/irregexp/imported/regexp-macro-assembler-arch.h
@@ -0,0 +1,7 @@
+/* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
+ * vim: set ts=8 sts=2 et sw=2 tw=80:
+ * This Source Code Form is subject to the terms of the Mozilla Public
+ * License, v. 2.0. If a copy of the MPL was not distributed with this
+ * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
+
+#include "irregexp/RegExpNativeMacroAssembler.h"
diff --git a/js/src/irregexp/regexp-macro-assembler-tracer.cc b/js/src/irregexp/imported/regexp-macro-assembler-tracer.cc
similarity index 99%
rename from js/src/irregexp/regexp-macro-assembler-tracer.cc
rename to js/src/irregexp/imported/regexp-macro-assembler-tracer.cc
index 2ae336a2893d..8db4b583827b 100644
--- a/js/src/irregexp/regexp-macro-assembler-tracer.cc
+++ b/js/src/irregexp/imported/regexp-macro-assembler-tracer.cc
@@ -2,7 +2,7 @@
 // Use of this source code is governed by a BSD-style license that can be
 // found in the LICENSE file.
 
-#include "irregexp/regexp-macro-assembler-tracer.h"
+#include "irregexp/imported/regexp-macro-assembler-tracer.h"
 
 
 namespace v8 {
diff --git a/js/src/irregexp/regexp-macro-assembler-tracer.h b/js/src/irregexp/imported/regexp-macro-assembler-tracer.h
similarity index 98%
rename from js/src/irregexp/regexp-macro-assembler-tracer.h
rename to js/src/irregexp/imported/regexp-macro-assembler-tracer.h
index c77e6404bc3d..9e8b681b26e7 100644
--- a/js/src/irregexp/regexp-macro-assembler-tracer.h
+++ b/js/src/irregexp/imported/regexp-macro-assembler-tracer.h
@@ -5,7 +5,7 @@
 #ifndef V8_REGEXP_REGEXP_MACRO_ASSEMBLER_TRACER_H_
 #define V8_REGEXP_REGEXP_MACRO_ASSEMBLER_TRACER_H_
 
-#include "irregexp/regexp-macro-assembler.h"
+#include "irregexp/imported/regexp-macro-assembler.h"
 
 namespace v8 {
 namespace internal {
diff --git a/js/src/irregexp/regexp-macro-assembler.cc b/js/src/irregexp/imported/regexp-macro-assembler.cc
similarity index 99%
rename from js/src/irregexp/regexp-macro-assembler.cc
rename to js/src/irregexp/imported/regexp-macro-assembler.cc
index 74090a5a626d..0d3d8b8f3f68 100644
--- a/js/src/irregexp/regexp-macro-assembler.cc
+++ b/js/src/irregexp/imported/regexp-macro-assembler.cc
@@ -2,9 +2,9 @@
 // Use of this source code is governed by a BSD-style license that can be
 // found in the LICENSE file.
 
-#include "irregexp/regexp-macro-assembler.h"
+#include "irregexp/imported/regexp-macro-assembler.h"
 
-#include "irregexp/regexp-stack.h"
+#include "irregexp/imported/regexp-stack.h"
 
 #ifdef V8_INTL_SUPPORT
 #include "unicode/uchar.h"
diff --git a/js/src/irregexp/regexp-macro-assembler.h b/js/src/irregexp/imported/regexp-macro-assembler.h
similarity index 99%
rename from js/src/irregexp/regexp-macro-assembler.h
rename to js/src/irregexp/imported/regexp-macro-assembler.h
index 12caa534e046..3d63c88ff87c 100644
--- a/js/src/irregexp/regexp-macro-assembler.h
+++ b/js/src/irregexp/imported/regexp-macro-assembler.h
@@ -5,9 +5,9 @@
 #ifndef V8_REGEXP_REGEXP_MACRO_ASSEMBLER_H_
 #define V8_REGEXP_REGEXP_MACRO_ASSEMBLER_H_
 
-#include "irregexp/regexp-ast.h"
-#include "irregexp/regexp-shim.h"
-#include "irregexp/regexp.h"
+#include "irregexp/imported/regexp-ast.h"
+#include "irregexp/imported/regexp.h"
+#include "irregexp/RegExpShim.h"
 
 namespace v8 {
 namespace internal {
diff --git a/js/src/irregexp/regexp-nodes.h b/js/src/irregexp/imported/regexp-nodes.h
similarity index 99%
rename from js/src/irregexp/regexp-nodes.h
rename to js/src/irregexp/imported/regexp-nodes.h
index 027109d51877..3c5a067bb91d 100644
--- a/js/src/irregexp/regexp-nodes.h
+++ b/js/src/irregexp/imported/regexp-nodes.h
@@ -5,7 +5,7 @@
 #ifndef V8_REGEXP_REGEXP_NODES_H_
 #define V8_REGEXP_REGEXP_NODES_H_
 
-#include "irregexp/regexp-macro-assembler.h"
+#include "irregexp/imported/regexp-macro-assembler.h"
 
 namespace v8 {
 namespace internal {
diff --git a/js/src/irregexp/regexp-parser.cc b/js/src/irregexp/imported/regexp-parser.cc
similarity index 99%
rename from js/src/irregexp/regexp-parser.cc
rename to js/src/irregexp/imported/regexp-parser.cc
index 4612f4ae36e9..4da306203d00 100644
--- a/js/src/irregexp/regexp-parser.cc
+++ b/js/src/irregexp/imported/regexp-parser.cc
@@ -2,13 +2,13 @@
 // Use of this source code is governed by a BSD-style license that can be
 // found in the LICENSE file.
 
-#include "irregexp/regexp-parser.h"
+#include "irregexp/imported/regexp-parser.h"
 
 #include <vector>
 
-#include "irregexp/property-sequences.h"
-#include "irregexp/regexp-macro-assembler.h"
-#include "irregexp/regexp.h"
+#include "irregexp/imported/property-sequences.h"
+#include "irregexp/imported/regexp-macro-assembler.h"
+#include "irregexp/imported/regexp.h"
 
 #ifdef V8_INTL_SUPPORT
 #include "unicode/uniset.h"
diff --git a/js/src/irregexp/regexp-parser.h b/js/src/irregexp/imported/regexp-parser.h
similarity index 99%
rename from js/src/irregexp/regexp-parser.h
rename to js/src/irregexp/imported/regexp-parser.h
index 72180b2adc43..1b281e70a7a5 100644
--- a/js/src/irregexp/regexp-parser.h
+++ b/js/src/irregexp/imported/regexp-parser.h
@@ -5,8 +5,8 @@
 #ifndef V8_REGEXP_REGEXP_PARSER_H_
 #define V8_REGEXP_REGEXP_PARSER_H_
 
-#include "irregexp/regexp-ast.h"
-#include "irregexp/regexp-error.h"
+#include "irregexp/imported/regexp-ast.h"
+#include "irregexp/imported/regexp-error.h"
 
 namespace v8 {
 namespace internal {
diff --git a/js/src/irregexp/regexp-stack.cc b/js/src/irregexp/imported/regexp-stack.cc
similarity index 98%
rename from js/src/irregexp/regexp-stack.cc
rename to js/src/irregexp/imported/regexp-stack.cc
index 1cb2dcf4d9e1..752bb51d66f8 100644
--- a/js/src/irregexp/regexp-stack.cc
+++ b/js/src/irregexp/imported/regexp-stack.cc
@@ -2,7 +2,7 @@
 // Use of this source code is governed by a BSD-style license that can be
 // found in the LICENSE file.
 
-#include "irregexp/regexp-stack.h"
+#include "irregexp/imported/regexp-stack.h"
 
 
 namespace v8 {
diff --git a/js/src/irregexp/regexp-stack.h b/js/src/irregexp/imported/regexp-stack.h
similarity index 99%
rename from js/src/irregexp/regexp-stack.h
rename to js/src/irregexp/imported/regexp-stack.h
index 3db1bdad95c8..2a0f29326f35 100644
--- a/js/src/irregexp/regexp-stack.h
+++ b/js/src/irregexp/imported/regexp-stack.h
@@ -5,7 +5,7 @@
 #ifndef V8_REGEXP_REGEXP_STACK_H_
 #define V8_REGEXP_REGEXP_STACK_H_
 
-#include "irregexp/regexp-shim.h"
+#include "irregexp/RegExpShim.h"
 
 namespace v8 {
 namespace internal {
diff --git a/js/src/irregexp/regexp.h b/js/src/irregexp/imported/regexp.h
similarity index 98%
rename from js/src/irregexp/regexp.h
rename to js/src/irregexp/imported/regexp.h
index f60ec65774ac..9c189af06c52 100644
--- a/js/src/irregexp/regexp.h
+++ b/js/src/irregexp/imported/regexp.h
@@ -5,8 +5,8 @@
 #ifndef V8_REGEXP_REGEXP_H_
 #define V8_REGEXP_REGEXP_H_
 
-#include "irregexp/regexp-error.h"
-#include "irregexp/regexp-shim.h"
+#include "irregexp/imported/regexp-error.h"
+#include "irregexp/RegExpShim.h"
 
 namespace v8 {
 namespace internal {
diff --git a/js/src/irregexp/special-case.cc b/js/src/irregexp/imported/special-case.cc
similarity index 97%
rename from js/src/irregexp/special-case.cc
rename to js/src/irregexp/imported/special-case.cc
index 867792d55267..d40ada6bb962 100644
--- a/js/src/irregexp/special-case.cc
+++ b/js/src/irregexp/imported/special-case.cc
@@ -11,7 +11,7 @@
 // Semantics: Canonicalize) step 3.
 
 #ifdef V8_INTL_SUPPORT
-#include "irregexp/special-case.h"
+#include "irregexp/imported/special-case.h"
 
 #include "unicode/uniset.h"
 namespace v8 {
diff --git a/js/src/irregexp/special-case.h b/js/src/irregexp/imported/special-case.h
similarity index 99%
rename from js/src/irregexp/special-case.h
rename to js/src/irregexp/imported/special-case.h
index 82d0e7075cf6..050d72a064e7 100644
--- a/js/src/irregexp/special-case.h
+++ b/js/src/irregexp/imported/special-case.h
@@ -6,7 +6,7 @@
 #define V8_REGEXP_SPECIAL_CASE_H_
 
 #ifdef V8_INTL_SUPPORT
-#include "irregexp/regexp-shim.h"
+#include "irregexp/RegExpShim.h"
 
 #include "unicode/uchar.h"
 #include "unicode/uniset.h"
diff --git a/js/src/irregexp/moz.build b/js/src/irregexp/moz.build
index f7d11184ce3c..faa9cbcfb248 100644
--- a/js/src/irregexp/moz.build
+++ b/js/src/irregexp/moz.build
@@ -12,29 +12,29 @@ FINAL_LIBRARY = "js"
 LOCAL_INCLUDES += ["!..", ".."]
 
 SOURCES += [
-    'regexp-ast.cc',
-    'regexp-bytecode-generator.cc',
-    'regexp-bytecode-peephole.cc',
-    'regexp-bytecodes.cc',
-    'regexp-compiler-tonode.cc',
-    'regexp-compiler.cc',
-    'regexp-dotprinter.cc',
-    'regexp-interpreter.cc',
-    'regexp-macro-assembler-tracer.cc',
-    'regexp-macro-assembler.cc',
-    'regexp-native-macro-assembler.cc',
-    'regexp-parser.cc',
-    'regexp-shim.cc',
-    'regexp-stack.cc',
+    'imported/regexp-ast.cc',
+    'imported/regexp-bytecode-generator.cc',
+    'imported/regexp-bytecode-peephole.cc',
+    'imported/regexp-bytecodes.cc',
+    'imported/regexp-compiler-tonode.cc',
+    'imported/regexp-compiler.cc',
+    'imported/regexp-dotprinter.cc',
+    'imported/regexp-interpreter.cc',
+    'imported/regexp-macro-assembler-tracer.cc',
+    'imported/regexp-macro-assembler.cc',
+    'imported/regexp-parser.cc',
+    'imported/regexp-stack.cc',
     'RegExpAPI.cpp',
-    'util/unicode.cc'
+    'RegExpNativeMacroAssembler.cpp',
+    'RegExpShim.cpp',
+    'util/UnicodeShim.cpp'
 ]
 
 if CONFIG['ENABLE_INTL_API']:
     CXXFLAGS += ['-DV8_INTL_SUPPORT']
     SOURCES += [
-        'property-sequences.cc',
-        'special-case.cc'
+        'imported/property-sequences.cc',
+        'imported/special-case.cc'
     ]
 
 if CONFIG['_MSC_VER']:
diff --git a/js/src/irregexp/util/flags.h b/js/src/irregexp/util/FlagsShim.h
similarity index 100%
rename from js/src/irregexp/util/flags.h
rename to js/src/irregexp/util/FlagsShim.h
diff --git a/js/src/irregexp/util/unicode.cc b/js/src/irregexp/util/UnicodeShim.cpp
similarity index 99%
rename from js/src/irregexp/util/unicode.cc
rename to js/src/irregexp/util/UnicodeShim.cpp
index 4dc6b0692a6f..231cfb028737 100644
--- a/js/src/irregexp/util/unicode.cc
+++ b/js/src/irregexp/util/UnicodeShim.cpp
@@ -5,7 +5,7 @@
 // This file is a subset of:
 //  https://github.com/v8/v8/blob/master/src/strings/unicode.cc
 
-#include "irregexp/regexp-shim.h"
+#include "irregexp/RegExpShim.h"
 
 #ifdef V8_INTL_SUPPORT
 #include "unicode/uchar.h"
diff --git a/js/src/irregexp/util/vector.h b/js/src/irregexp/util/VectorShim.h
similarity index 99%
rename from js/src/irregexp/util/vector.h
rename to js/src/irregexp/util/VectorShim.h
index 838e67b50948..bbb876fdcc48 100644
--- a/js/src/irregexp/util/vector.h
+++ b/js/src/irregexp/util/VectorShim.h
@@ -10,7 +10,7 @@
 #include <iterator>
 #include <memory>
 
-#include "js/AllocPolicy.h"
+#include "jsalloc.h"
 #include "js/Utility.h"
 #include "js/Vector.h"
 
diff --git a/js/src/irregexp/util/zone.h b/js/src/irregexp/util/ZoneShim.h
similarity index 99%
rename from js/src/irregexp/util/zone.h
rename to js/src/irregexp/util/ZoneShim.h
index 75a983ec75ec..04ba9693f692 100644
--- a/js/src/irregexp/util/zone.h
+++ b/js/src/irregexp/util/ZoneShim.h
@@ -13,7 +13,7 @@
 
 #include "ds/LifoAlloc.h"
 #include "ds/Sort.h"
-#include "irregexp/util/vector.h"
+#include "irregexp/util/VectorShim.h"
 
 namespace v8 {
 namespace internal {
diff --git a/js/src/moz.build b/js/src/moz.build
index 4dbbee1ddd23..5de58e786326 100644
--- a/js/src/moz.build
+++ b/js/src/moz.build
@@ -113,7 +113,7 @@ DIRS += [
     'ds',
     'frontend',
     'gc',
-    'irregexp'
+    'irregexp',
     'jit',
     'perf',
     'proxy',
-- 
2.33.0.windows.2

