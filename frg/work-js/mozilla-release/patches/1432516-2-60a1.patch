# HG changeset patch
# User Ryan Hunt <rhunt@eqrion.net>
# Date 1516745872 21600
# Node ID c71022b03df808e4c176f269ec727a3338767dfe
# Parent  51c675fdc27c1bb8ebf9e6148a9d26776b5f1dfb
Add paint worker count to crash report notes. (bug 1432516, r=milan)

diff --git a/gfx/src/gfxCrashReporterUtils.cpp b/gfx/src/gfxCrashReporterUtils.cpp
--- a/gfx/src/gfxCrashReporterUtils.cpp
+++ b/gfx/src/gfxCrashReporterUtils.cpp
@@ -92,30 +92,37 @@ public:
     return NS_OK;
   }
 
 private:
   nsAutoCString mFeatureString;
 };
 
 void
-ScopedGfxFeatureReporter::WriteAppNote(char statusChar)
+ScopedGfxFeatureReporter::WriteAppNote(char statusChar, int32_t statusNumber)
 {
   StaticMutexAutoLock al(gFeaturesAlreadyReportedMutex);
 
   if (!gFeaturesAlreadyReported) {
     gFeaturesAlreadyReported = new nsTArray<nsCString>;
     nsCOMPtr<nsIRunnable> r = new RegisterObserverRunnable();
     SystemGroup::Dispatch(TaskCategory::Other, r.forget());
   }
 
   nsAutoCString featureString;
-  featureString.AppendPrintf("%s%c ",
-                             mFeature,
-                             statusChar);
+  if (statusNumber == 0) {
+    featureString.AppendPrintf("%s%c ",
+                               mFeature,
+                               statusChar);
+  } else {
+    featureString.AppendPrintf("%s%c%d ",
+                               mFeature,
+                               statusChar,
+                               statusNumber);
+  }
 
   if (!gFeaturesAlreadyReported->Contains(featureString)) {
     gFeaturesAlreadyReported->AppendElement(featureString);
     AppNote(featureString);
   }
 }
 
 void
diff --git a/gfx/src/gfxCrashReporterUtils.h b/gfx/src/gfxCrashReporterUtils.h
--- a/gfx/src/gfxCrashReporterUtils.h
+++ b/gfx/src/gfxCrashReporterUtils.h
@@ -21,32 +21,38 @@ namespace mozilla {
   * This ScopedGfxFeatureReporter class is designed to be fool-proof to use in functions that
   * have many exit points. We don't want to encourage having function with many exit points.
   * It just happens that our graphics features initialization functions are like that.
   */
 class ScopedGfxFeatureReporter
 {
 public:
   explicit ScopedGfxFeatureReporter(const char *aFeature, bool aForce = false)
-    : mFeature(aFeature), mStatusChar('-')
+    : mFeature(aFeature), mStatusChar('-'), mStatusNumber(0)
   {
-    WriteAppNote(aForce ? '!' : '?');
+    WriteAppNote(aForce ? '!' : '?', 0);
   }
   ~ScopedGfxFeatureReporter() {
-    WriteAppNote(mStatusChar);
+    WriteAppNote(mStatusChar, mStatusNumber);
   }
   void SetSuccessful() { mStatusChar = '+'; }
+  void SetSuccessful(int32_t aNumber)
+  {
+    mStatusChar = '+';
+    mStatusNumber = aNumber;
+  }
 
   static void AppNote(const nsACString& aMessage);
 
   class AppNoteWritingRunnable;
 
 protected:
   const char *mFeature;
   char mStatusChar;
+  int32_t mStatusNumber;
 
 private:
-  void WriteAppNote(char statusChar);
+  void WriteAppNote(char statusChar, int32_t statusNumber);
 };
 
 } // end namespace mozilla
 
 #endif // gfxCrashReporterUtils_h__
diff --git a/gfx/thebes/gfxPlatform.cpp b/gfx/thebes/gfxPlatform.cpp
--- a/gfx/thebes/gfxPlatform.cpp
+++ b/gfx/thebes/gfxPlatform.cpp
@@ -2564,23 +2564,24 @@ gfxPlatform::InitWebRenderConfig()
 }
 
 void
 gfxPlatform::InitOMTPConfig()
 {
   ScopedGfxFeatureReporter reporter("OMTP");
 
   FeatureState& omtp = gfxConfig::GetFeature(Feature::OMTP);
+  int32_t paintWorkerCount = PaintThread::CalculatePaintWorkerCount();
 
   if (!XRE_IsParentProcess()) {
     // The parent process runs through all the real decision-making code
     // later in this function. For other processes we still want to report
     // the state of the feature for crash reports.
     if (gfxVars::UseOMTP()) {
-      reporter.SetSuccessful();
+      reporter.SetSuccessful(paintWorkerCount);
     }
     return;
   }
 
   omtp.SetDefaultFromPref(
     "layers.omtp.enabled",
     true,
     Preferences::GetBool("layers.omtp.enabled", false, PrefValueKind::Default));
@@ -2595,17 +2596,17 @@ gfxPlatform::InitOMTPConfig()
                       NS_LITERAL_CSTRING("FEATURE_FAILURE_COMP_SAFEMODE"));
   } else if (gfxPlatform::UsesTiling() && gfxPrefs::TileEdgePaddingEnabled()) {
     omtp.ForceDisable(FeatureStatus::Blocked, "OMTP does not yet support tiling with edge padding",
                       NS_LITERAL_CSTRING("FEATURE_FAILURE_OMTP_TILING"));
   }
 
   if (omtp.IsEnabled()) {
     gfxVars::SetUseOMTP(true);
-    reporter.SetSuccessful();
+    reporter.SetSuccessful(paintWorkerCount);
   }
 }
 
 bool
 gfxPlatform::CanUseHardwareVideoDecoding()
 {
   // this function is called from the compositor thread, so it is not
   // safe to init the prefs etc. from here.
