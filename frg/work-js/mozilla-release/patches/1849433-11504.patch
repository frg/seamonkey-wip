# HG changeset patch
# User Kelsey Gilbert <kelsey.gilbert@mozilla.com>
# Date 1693518335 0
# Node ID 44c386281c90fcc0d6c134435eee932467ae6b08
# Parent  ffacc61c815046ca680e8f05cd3753f1833e0af8
Bug 1849433 - Add webgl.max-vert-ids-per-draw, default 30M. r=gfx-reviewers,gw a=RyanVM

diff --git a/dom/canvas/WebGLContext.h b/dom/canvas/WebGLContext.h
--- a/dom/canvas/WebGLContext.h
+++ b/dom/canvas/WebGLContext.h
@@ -3,16 +3,17 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef WEBGLCONTEXT_H_
 #define WEBGLCONTEXT_H_
 
 #include <stdarg.h>
 
+#include "gfxPrefs.h"
 #include "GLContextTypes.h"
 #include "GLDefs.h"
 #include "mozilla/Attributes.h"
 #include "mozilla/CheckedInt.h"
 #include "mozilla/dom/BindingDeclarations.h"
 #include "mozilla/dom/HTMLCanvasElement.h"
 #include "mozilla/dom/TypedArray.h"
 #include "mozilla/EnumeratedArray.h"
@@ -1452,16 +1453,18 @@ private:
 protected:
     WebGLVertexAttrib0Status WhatDoesVertexAttrib0Need() const;
     bool DoFakeVertexAttrib0(const char* funcName, GLuint vertexCount);
     void UndoFakeVertexAttrib0();
 
     CheckedUint32 mGeneration;
 
     WebGLContextOptions mOptions;
+    const uint32_t mMaxVertIdsPerDraw =
+       gfxPrefs::WebGLMaxVertIDsPerDraw();
 
     bool mInvalidated;
     bool mCapturedFrameInvalidated;
     bool mResetLayer;
     bool mOptionsFrozen;
     bool mDisableExtensions;
     bool mIsMesa;
     bool mLoseContextOnMemoryPressure;
diff --git a/dom/canvas/WebGLContextDraw.cpp b/dom/canvas/WebGLContextDraw.cpp
--- a/dom/canvas/WebGLContextDraw.cpp
+++ b/dom/canvas/WebGLContextDraw.cpp
@@ -513,29 +513,39 @@ WebGLContext::DrawArrays_check(const cha
             return false;
         }
         *out_lastVert = Some(lastVert_checked.value());
     }
     return true;
 }
 
 void
-WebGLContext::DrawArraysInstanced(GLenum mode, GLint first, GLsizei vertCount,
+WebGLContext::DrawArraysInstanced(GLenum mode, GLint first, GLsizei iVertCount,
                                   GLsizei instanceCount, const char* const funcName)
 {
     AUTO_PROFILER_LABEL("WebGLContext::DrawArraysInstanced", GRAPHICS);
     if (IsContextLost())
         return;
 
     const gl::GLContext::TlsScope inTls(gl);
 
+    const auto vertCount = AssertedCast<uint32_t>(iVertCount);
+
     Maybe<uint32_t> lastVert;
     if (!DrawArrays_check(funcName, first, vertCount, instanceCount, &lastVert))
         return;
 
+    if (vertCount > mMaxVertIdsPerDraw) {
+      ErrorOutOfMemory(
+          "Context's max vertCount is %u, but %u requested. "
+          "[webgl.max-vert-ids-per-draw]",
+          mMaxVertIdsPerDraw, vertCount);
+      return;
+    }
+
     bool error = false;
     const ScopedDrawHelper scopedHelper(this, funcName, mode, lastVert, instanceCount,
                                         &error);
     if (error)
         return;
 
     const ScopedResolveTexturesForDraw scopedResolve(this, funcName, &error);
     if (error)
@@ -669,33 +679,43 @@ HandleDrawElementsErrors(WebGLContext* w
     if (err) {
         webgl->ErrorImplementationBug("%s: Unexpected driver error during indexed draw"
                                       " call. Please file a bug.",
                                       funcName);
         return;
     }
 }
 
-void
-WebGLContext::DrawElementsInstanced(GLenum mode, GLsizei indexCount, GLenum type,
-                                    WebGLintptr byteOffset, GLsizei instanceCount,
-                                    const char* const funcName)
-{
+void WebGLContext::DrawElementsInstanced(const GLenum mode,
+                                         const GLsizei iIndexCount,
+                                         const GLenum type,
+                                         const WebGLintptr byteOffset,
+                                         const GLsizei instanceCount,
+                                         const char* const funcName) {
     AUTO_PROFILER_LABEL("WebGLContext::DrawElementsInstanced", GRAPHICS);
     if (IsContextLost())
         return;
 
     const gl::GLContext::TlsScope inTls(gl);
 
     Maybe<uint32_t> lastVert;
-    if (!DrawElements_check(funcName, indexCount, type, byteOffset, instanceCount,
+    if (!DrawElements_check(funcName, iIndexCount, type, byteOffset, instanceCount,
                             &lastVert))
     {
         return;
     }
+    const auto indexCount = AssertedCast<uint32_t>(iIndexCount);
+
+    if (indexCount > mMaxVertIdsPerDraw) {
+      ErrorOutOfMemory(
+          "Context's max indexCount is %u, but %u requested. "
+          "[webgl.max-vert-ids-per-draw]",
+          mMaxVertIdsPerDraw, indexCount);
+      return;
+    }
 
     bool error = false;
     const ScopedDrawHelper scopedHelper(this, funcName, mode, lastVert, instanceCount,
                                         &error);
     if (error)
         return;
 
     const ScopedResolveTexturesForDraw scopedResolve(this, funcName, &error);
diff --git a/gfx/thebes/gfxPrefs.h b/gfx/thebes/gfxPrefs.h
--- a/gfx/thebes/gfxPrefs.h
+++ b/gfx/thebes/gfxPrefs.h
@@ -747,17 +747,18 @@ private:
   DECL_GFX_PREF(Live, "webgl.enable-webgl2",                   WebGL2Enabled, bool, true);
   DECL_GFX_PREF(Live, "webgl.force-enabled",                   WebGLForceEnabled, bool, false);
   DECL_GFX_PREF(Once, "webgl.force-layers-readback",           WebGLForceLayersReadback, bool, false);
   DECL_GFX_PREF(Live, "webgl.force-index-validation",          WebGLForceIndexValidation, int32_t, 0);
   DECL_GFX_PREF(Live, "webgl.lose-context-on-memory-pressure", WebGLLoseContextOnMemoryPressure, bool, false);
   DECL_GFX_PREF(Live, "webgl.max-contexts",                    WebGLMaxContexts, uint32_t, 32);
   DECL_GFX_PREF(Live, "webgl.max-contexts-per-principal",      WebGLMaxContextsPerPrincipal, uint32_t, 16);
   DECL_GFX_PREF(Live, "webgl.max-warnings-per-context",        WebGLMaxWarningsPerContext, uint32_t, 32);
-  DECL_GFX_PREF(Live, "webgl.max-size-per-texture-mb",         WebGLMaxSizePerTextureMB, uint32_t, 1024); 
+  DECL_GFX_PREF(Live, "webgl.max-size-per-texture-mib",        WebGLMaxSizePerTextureMiB, uint32_t, 1024); 
+  DECL_GFX_PREF(Live, "webgl.max-vert-ids-per-draw",           WebGLMaxVertIDsPerDraw, uint32_t, 30*1000*1000); 
   DECL_GFX_PREF(Live, "webgl.min_capability_mode",             WebGLMinCapabilityMode, bool, false);
   DECL_GFX_PREF(Live, "webgl.msaa-force",                      WebGLForceMSAA, bool, false);
   DECL_GFX_PREF(Live, "webgl.msaa-samples",                    WebGLMsaaSamples, uint32_t, 4);
   DECL_GFX_PREF(Live, "webgl.prefer-16bpp",                    WebGLPrefer16bpp, bool, false);
   DECL_GFX_PREF(Live, "webgl.restore-context-when-visible",    WebGLRestoreWhenVisible, bool, true);
   DECL_GFX_PREF(Live, "webgl.allow-immediate-queries",         WebGLImmediateQueries, bool, false);
   DECL_GFX_PREF(Live, "webgl.allow-fb-invalidation",           WebGLFBInvalidation, bool, false);
 
diff --git a/modules/libpref/init/all.js b/modules/libpref/init/all.js
--- a/modules/libpref/init/all.js
+++ b/modules/libpref/init/all.js
@@ -4964,16 +4964,17 @@ pref("webgl.restore-context-when-visible
 pref("webgl.max-contexts", 16);
 pref("webgl.max-contexts-per-principal", 8);
 #else
 pref("webgl.max-contexts", 32);
 pref("webgl.max-contexts-per-principal", 16);
 #endif
 pref("webgl.max-warnings-per-context", 32);
 pref("webgl.max-size-per-texture-mib", 1024);
+pref("webgl.max-vert-ids-per-draw", 30000000);
 pref("webgl.enable-draft-extensions", false);
 pref("webgl.enable-privileged-extensions", false);
 pref("webgl.bypass-shader-validation", false);
 pref("webgl.disable-fail-if-major-performance-caveat", false);
 pref("webgl.disable-DOM-blit-uploads", false);
 pref("webgl.allow-fb-invalidation", false);
 pref("webgl.webgl2-compat-mode", false);
 
