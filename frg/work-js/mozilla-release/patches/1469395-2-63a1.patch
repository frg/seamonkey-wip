# HG changeset patch
# User Luke Wagner <luke@mozilla.com>
# Date 1533748478 18000
#      Wed Aug 08 12:14:38 2018 -0500
# Node ID 2ed9cbebf30e401db82950fc0cca2bb48efb9fa2
# Parent  b0f4e67b3ae1bd2e084e12a6dd6cd816636c3a8c
Bug 1469395 - Baldr: remove dead JS::WasmModule::notifyWhenCompilationComplete() support (r=lth)

diff --git a/js/src/builtin/TestingFunctions.cpp b/js/src/builtin/TestingFunctions.cpp
--- a/js/src/builtin/TestingFunctions.cpp
+++ b/js/src/builtin/TestingFunctions.cpp
@@ -791,17 +791,17 @@ WasmHasTier2CompilationCompleted(JSConte
 
     JSObject* unwrapped = CheckedUnwrap(&args.get(0).toObject());
     if (!unwrapped || !unwrapped->is<WasmModuleObject>()) {
         JS_ReportErrorASCII(cx, "argument is not a WebAssembly.Module");
         return false;
     }
 
     Rooted<WasmModuleObject*> module(cx, &unwrapped->as<WasmModuleObject>());
-    args.rval().set(BooleanValue(module->module().compilationComplete()));
+    args.rval().set(BooleanValue(!module->module().testingTier2Active()));
     return true;
 }
 
 static bool
 IsLazyFunction(JSContext* cx, unsigned argc, Value* vp)
 {
     CallArgs args = CallArgsFromVp(argc, vp);
     if (args.length() != 1) {
diff --git a/js/src/vm/MutexIDs.h b/js/src/vm/MutexIDs.h
--- a/js/src/vm/MutexIDs.h
+++ b/js/src/vm/MutexIDs.h
@@ -45,17 +45,16 @@
   _(IcuTimeZoneStateMutex,       500) \
   _(ProcessExecutableRegion,     500) \
   _(OffThreadPromiseState,       500) \
   _(BufferStreamState,           500) \
   _(SharedArrayGrow,             500) \
   _(RuntimeScriptData,           500) \
   _(WasmFuncTypeIdSet,           500) \
   _(WasmCodeProfilingLabels,     500) \
-  _(WasmModuleTieringLock,       500) \
   _(WasmCompileTaskState,        500) \
   _(WasmCodeStreamEnd,           500) \
   _(WasmTailBytesPtr,            500) \
   _(WasmStreamStatus,            500) \
   _(WasmRuntimeInstances,        500) \
                                       \
   _(ThreadId,                    600) \
   _(WasmCodeSegmentMap,          600) \
diff --git a/js/src/wasm/AsmJS.cpp b/js/src/wasm/AsmJS.cpp
--- a/js/src/wasm/AsmJS.cpp
+++ b/js/src/wasm/AsmJS.cpp
@@ -6486,18 +6486,17 @@ struct ScopedCacheEntryOpenedForRead
 
 static JS::AsmJSCacheResult
 StoreAsmJSModuleInCache(AsmJSParser& parser, Module& module, JSContext* cx)
 {
     ModuleCharsForStore moduleChars;
     if (!moduleChars.init(parser))
         return JS::AsmJSCache_InternalError;
 
-    size_t bytecodeSize = module.bytecodeSerializedSize();
-    MOZ_RELEASE_ASSERT(bytecodeSize == 0);
+    MOZ_RELEASE_ASSERT(module.bytecode().length() == 0);
 
     size_t compiledSize = module.compiledSerializedSize();
     MOZ_RELEASE_ASSERT(compiledSize <= UINT32_MAX);
 
     size_t serializedSize = sizeof(uint32_t) +
                             compiledSize +
                             moduleChars.serializedSize();
 
diff --git a/js/src/wasm/WasmCompile.cpp b/js/src/wasm/WasmCompile.cpp
--- a/js/src/wasm/WasmCompile.cpp
+++ b/js/src/wasm/WasmCompile.cpp
@@ -445,42 +445,46 @@ wasm::CompileBuffer(const CompileArgs& a
         return nullptr;
 
     if (!DecodeModuleTail(d, &env))
         return nullptr;
 
     return mg.finishModule(bytecode);
 }
 
-bool
+void
 wasm::CompileTier2(const CompileArgs& args, Module& module, Atomic<bool>* cancelled)
 {
     MOZ_RELEASE_ASSERT(wasm::HaveSignalHandlers());
 
     UniqueChars error;
     Decoder d(module.bytecode().bytes, 0, &error);
 
     MOZ_ASSERT(args.gcTypesEnabled == HasGcTypes::False, "can't ion-compile with gc types yet");
 
     ModuleEnvironment env(CompileMode::Tier2, Tier::Ion, DebugEnabled::False, HasGcTypes::False,
                           args.sharedMemoryEnabled ? Shareable::True : Shareable::False);
     if (!DecodeModuleEnvironment(d, &env))
-        return false;
+        return;
 
     ModuleGenerator mg(args, &env, cancelled, &error);
     if (!mg.init())
-        return false;
+        return;
 
     if (!DecodeCodeSection(env, d, mg))
-        return false;
+        return;
 
     if (!DecodeModuleTail(d, &env))
-        return false;
+        return;
 
-    return mg.finishTier2(module);
+    if (!mg.finishTier2(module))
+        return;
+
+    // The caller doesn't care about success or failure; only that compilation
+    // is inactive, so there is no success to return here.
 }
 
 class StreamingDecoder
 {
     Decoder d_;
     const ExclusiveStreamEnd& streamEnd_;
     const Atomic<bool>& cancelled_;
 
diff --git a/js/src/wasm/WasmCompile.h b/js/src/wasm/WasmCompile.h
--- a/js/src/wasm/WasmCompile.h
+++ b/js/src/wasm/WasmCompile.h
@@ -82,20 +82,19 @@ EstimateCompiledCodeSize(Tier tier, size
 //  - *error is null and the caller should report out-of-memory.
 
 SharedModule
 CompileBuffer(const CompileArgs& args,
               const ShareableBytes& bytecode,
               UniqueChars* error,
               UniqueCharsVector* warnings);
 
-// Attempt to compile the second tier of the given wasm::Module, returning whether
-// tier-2 compilation succeeded and Module::finishTier2 was called.
+// Attempt to compile the second tier of the given wasm::Module.
 
-bool
+void
 CompileTier2(const CompileArgs& args, Module& module, Atomic<bool>* cancelled);
 
 // Compile the given WebAssembly module which has been broken into three
 // partitions:
 //  - envBytes contains a complete ModuleEnvironment that has already been
 //    copied in from the stream.
 //  - codeBytes is pre-sized to hold the complete code section when the stream
 //    completes.
diff --git a/js/src/wasm/WasmModule.cpp b/js/src/wasm/WasmModule.cpp
--- a/js/src/wasm/WasmModule.cpp
+++ b/js/src/wasm/WasmModule.cpp
@@ -170,83 +170,53 @@ LinkData::sizeOfExcludingThis(MallocSize
     return sum;
 }
 
 class Module::Tier2GeneratorTaskImpl : public Tier2GeneratorTask
 {
     SharedModule            module_;
     SharedCompileArgs       compileArgs_;
     Atomic<bool>            cancelled_;
-    bool                    finished_;
 
   public:
     Tier2GeneratorTaskImpl(Module& module, const CompileArgs& compileArgs)
       : module_(&module),
         compileArgs_(&compileArgs),
-        cancelled_(false),
-        finished_(false)
+        cancelled_(false)
     {}
 
     ~Tier2GeneratorTaskImpl() override {
-        if (!finished_)
-            module_->notifyCompilationListeners();
+        module_->testingTier2Active_ = false;
     }
 
     void cancel() override {
         cancelled_ = true;
     }
 
     void execute() override {
-        MOZ_ASSERT(!finished_);
-        finished_ = CompileTier2(*compileArgs_, *module_, &cancelled_);
+        CompileTier2(*compileArgs_, *module_, &cancelled_);
     }
 };
 
 void
 Module::startTier2(const CompileArgs& args)
 {
-    MOZ_ASSERT(!tiering_.lock()->active);
-
-    // If a Module initiates tier-2 compilation, we must ensure that eventually
-    // notifyCompilationListeners() is called. Since we must ensure
-    // Tier2GeneratorTaskImpl objects are destroyed *anyway*, we use
-    // ~Tier2GeneratorTaskImpl() to call notifyCompilationListeners() if it
-    // hasn't been already.
+    MOZ_ASSERT(!testingTier2Active_);
 
     auto task = MakeUnique<Tier2GeneratorTaskImpl>(*this, args);
     if (!task)
         return;
 
-    tiering_.lock()->active = true;
+    // This flag will be cleared asynchronously by ~Tier2GeneratorTaskImpl()
+    // on success or failure.
+    testingTier2Active_ = true;
 
     StartOffThreadWasmTier2Generator(std::move(task));
 }
 
-void
-Module::notifyCompilationListeners()
-{
-    // Notify listeners without holding the lock to avoid deadlocks if the
-    // listener takes their own lock or reenters this Module.
-
-    Tiering::ListenerVector listeners;
-    {
-        auto tiering = tiering_.lock();
-
-        MOZ_ASSERT(tiering->active);
-        tiering->active = false;
-
-        Swap(listeners, tiering->listeners);
-
-        tiering.notify_all(/* inactive */);
-    }
-
-    for (RefPtr<JS::WasmModuleListener>& listener : listeners)
-        listener->onCompilationComplete();
-}
-
 bool
 Module::finishTier2(UniqueLinkDataTier linkData2, UniqueCodeTier tier2Arg, ModuleEnvironment* env2)
 {
     MOZ_ASSERT(code().bestTier() == Tier::Baseline && tier2Arg->tier() == Tier::Ion);
 
     // Install the data in the data structures. They will not be visible
     // until commitTier2().
 
@@ -291,24 +261,18 @@ Module::finishTier2(UniqueLinkDataTier l
         if (!stubs2->createTier2(gcTypesEnabled, funcExportIndices, tier2, &stub2Index))
             return false;
 
         // Now that we can't fail or otherwise abort tier2, make it live.
 
         MOZ_ASSERT(!code().hasTier2());
         code().commitTier2();
 
-        // Now tier2 is committed and we can update jump tables entries to
-        // start making tier2 live.  Because lazy stubs are protected by a lock
-        // and notifyCompilationListeners should be called without any lock
-        // held, do it before.
-
         stubs2->setJitEntries(stub2Index, code());
     }
-    notifyCompilationListeners();
 
     // And we update the jump vector.
 
     uint8_t* base = code().segment(Tier::Ion).base();
     for (const CodeRange& cr : metadata(Tier::Ion).codeRanges) {
         // These are racy writes that we just want to be visible, atomically,
         // eventually.  All hardware we care about will do this right.  But
         // we depend on the compiler not splitting the stores hidden inside the
@@ -318,75 +282,26 @@ Module::finishTier2(UniqueLinkDataTier l
         else if (cr.isJitEntry())
             code().setJitEntry(cr.funcIndex(), base + cr.begin());
     }
 
     return true;
 }
 
 void
-Module::blockOnTier2Complete() const
-{
-    auto tiering = tiering_.lock();
-    while (tiering->active)
-        tiering.wait(/* inactive */);
-}
-
-/* virtual */ size_t
-Module::bytecodeSerializedSize() const
-{
-    return bytecode_->bytes.length();
-}
-
-/* virtual */ void
-Module::bytecodeSerialize(uint8_t* bytecodeBegin, size_t bytecodeSize) const
+Module::testingBlockOnTier2Complete() const
 {
-    MOZ_ASSERT(!!bytecodeBegin == !!bytecodeSize);
-
-    // Bytecode deserialization is not guarded by Assumptions and thus must not
-    // change incompatibly between builds. For simplicity, the format of the
-    // bytecode file is a .wasm file which ensures backwards compatibility.
-
-    const Bytes& bytes = bytecode_->bytes;
-    uint8_t* bytecodeEnd = WriteBytes(bytecodeBegin, bytes.begin(), bytes.length());
-    MOZ_RELEASE_ASSERT(bytecodeEnd == bytecodeBegin + bytecodeSize);
-}
-
-/* virtual */ bool
-Module::compilationComplete() const
-{
-    // For the purposes of serialization, if there is not an active tier-2
-    // compilation in progress, compilation is "complete" in that
-    // compiledSerialize() can be called. Now, tier-2 compilation may have
-    // failed or never started in the first place, but in such cases, a
-    // zero-byte compilation is serialized, triggering recompilation on upon
-    // deserialization. Basically, we only want serialization to wait if waiting
-    // would eventually produce tier-2 code.
-    return !tiering_.lock()->active;
-}
-
-/* virtual */ bool
-Module::notifyWhenCompilationComplete(JS::WasmModuleListener* listener)
-{
-    {
-        auto tiering = tiering_.lock();
-        if (tiering->active)
-            return tiering->listeners.append(listener);
-    }
-
-    // Notify the listener without holding the lock to avoid deadlocks if the
-    // listener takes their own lock or reenters this Module.
-    listener->onCompilationComplete();
-    return true;
+    while (testingTier2Active_)
+        std::this_thread::sleep_for(std::chrono::milliseconds(1));
 }
 
 /* virtual */ size_t
 Module::compiledSerializedSize() const
 {
-    MOZ_ASSERT(!tiering_.lock()->active);
+    MOZ_ASSERT(!testingTier2Active_);
 
     // The compiled debug code must not be saved, set compiled size to 0,
     // so Module::assumptionsMatch will return false during assumptions
     // deserialization.
     if (metadata().debugEnabled)
         return 0;
 
     if (!code_->hasTier(Tier::Serialized))
@@ -400,17 +315,17 @@ Module::compiledSerializedSize() const
            SerializedVectorSize(elemSegments_) +
            SerializedVectorSize(structTypes_) +
            code_->serializedSize();
 }
 
 /* virtual */ void
 Module::compiledSerialize(uint8_t* compiledBegin, size_t compiledSize) const
 {
-    MOZ_ASSERT(!tiering_.lock()->active);
+    MOZ_ASSERT(!testingTier2Active_);
 
     if (metadata().debugEnabled) {
         MOZ_RELEASE_ASSERT(compiledSize == 0);
         return;
     }
 
     if (!code_->hasTier(Tier::Serialized)) {
         MOZ_RELEASE_ASSERT(compiledSize == 0);
@@ -627,17 +542,17 @@ bool
 Module::extractCode(JSContext* cx, Tier tier, MutableHandleValue vp) const
 {
     RootedPlainObject result(cx, NewBuiltinClassInstance<PlainObject>(cx));
     if (!result)
         return false;
 
     // This function is only used for testing purposes so we can simply
     // block on tiered compilation to complete.
-    blockOnTier2Complete();
+    testingBlockOnTier2Complete();
 
     if (!code_->hasTier(tier)) {
         vp.setNull();
         return true;
     }
 
     const ModuleSegment& moduleSegment = code_->segment(tier);
     RootedObject code(cx, JS_NewUint8Array(cx, moduleSegment.length()));
@@ -1299,12 +1214,12 @@ Module::instantiate(JSContext* cx,
         if (!instance->instance().callExport(cx, *metadata().startFuncIndex, args))
             return false;
     }
 
     JSUseCounter useCounter = metadata().isAsmJS() ? JSUseCounter::ASMJS : JSUseCounter::WASM;
     cx->runtime()->setUseCounter(instance, useCounter);
 
     if (cx->options().testWasmAwaitTier2())
-        blockOnTier2Complete();
+        testingBlockOnTier2Complete();
 
     return true;
 }
diff --git a/js/src/wasm/WasmModule.h b/js/src/wasm/WasmModule.h
--- a/js/src/wasm/WasmModule.h
+++ b/js/src/wasm/WasmModule.h
@@ -90,33 +90,16 @@ class LinkData
     explicit LinkData(UniqueLinkDataTier tier) : tier1_(std::move(tier)) {}
 
     void setTier2(UniqueLinkDataTier linkData) const;
     const LinkDataTier& tier(Tier tier) const;
 
     WASM_DECLARE_SERIALIZABLE(LinkData)
 };
 
-// Contains the locked tiering state of a Module: whether there is an active
-// background tier-2 compilation in progress and, if so, the list of listeners
-// waiting for the tier-2 compilation to complete.
-
-struct Tiering
-{
-    typedef Vector<RefPtr<JS::WasmModuleListener>, 0, SystemAllocPolicy> ListenerVector;
-
-    Tiering() : active(false) {}
-    ~Tiering() { MOZ_ASSERT(listeners.empty()); MOZ_ASSERT(!active); }
-
-    ListenerVector listeners;
-    bool active;
-};
-
-typedef ExclusiveWaitableData<Tiering> ExclusiveTiering;
-
 // Module represents a compiled wasm module and primarily provides two
 // operations: instantiation and serialization. A Module can be instantiated any
 // number of times to produce new Instance objects. A Module can be serialized
 // any number of times such that the serialized bytes can be deserialized later
 // to produce a new, equivalent Module.
 //
 // Fully linked-and-instantiated code (represented by Code and its owned
 // ModuleSegment) can be shared between instances, provided none of those
@@ -132,17 +115,21 @@ class Module : public JS::WasmModule
     const UniqueConstBytes  unlinkedCodeForDebugging_;
     const LinkData          linkData_;
     const ImportVector      imports_;
     const ExportVector      exports_;
     const DataSegmentVector dataSegments_;
     const ElemSegmentVector elemSegments_;
     const StructTypeVector  structTypes_;
     const SharedBytes       bytecode_;
-    ExclusiveTiering        tiering_;
+
+    // This flag is only used for testing purposes and is racily updated as soon
+    // as tier-2 compilation finishes (in success or failure).
+
+    mutable Atomic<bool>    testingTier2Active_;
 
     // `codeIsBusy_` is set to false initially and then to true when `code_` is
     // already being used for an instance and can't be shared because it may be
     // patched by the debugger. Subsequent instances must then create copies
     // by linking the `unlinkedCodeForDebugging_`.
 
     mutable Atomic<bool>    codeIsBusy_;
 
@@ -155,17 +142,16 @@ class Module : public JS::WasmModule
                             WasmGlobalObjectVector& globalObjs) const;
     bool initSegments(JSContext* cx,
                       HandleWasmInstanceObject instance,
                       Handle<FunctionVector> funcImports,
                       HandleWasmMemoryObject memory,
                       HandleValVector globalImportValues) const;
 
     class Tier2GeneratorTaskImpl;
-    void notifyCompilationListeners();
 
   public:
     Module(Assumptions&& assumptions,
            const Code& code,
            UniqueConstBytes unlinkedCodeForDebugging,
            LinkData&& linkData,
            ImportVector&& imports,
            ExportVector&& exports,
@@ -178,17 +164,17 @@ class Module : public JS::WasmModule
         unlinkedCodeForDebugging_(std::move(unlinkedCodeForDebugging)),
         linkData_(std::move(linkData)),
         imports_(std::move(imports)),
         exports_(std::move(exports)),
         dataSegments_(std::move(dataSegments)),
         elemSegments_(std::move(elemSegments)),
         structTypes_(std::move(structTypes)),
         bytecode_(&bytecode),
-        tiering_(mutexid::WasmModuleTieringLock),
+        testingTier2Active_(false),
         codeIsBusy_(false)
     {
         MOZ_ASSERT_IF(metadata().debugEnabled, unlinkedCodeForDebugging_);
     }
     ~Module() override { /* Note: can be called on any thread */ }
 
     const Code& code() const { return *code_; }
     const ModuleSegment& moduleSegment(Tier t) const { return code_->segment(t); }
@@ -208,32 +194,29 @@ class Module : public JS::WasmModule
                      HandleWasmTableObject tableImport,
                      HandleWasmMemoryObject memoryImport,
                      HandleValVector globalImportValues,
                      WasmGlobalObjectVector& globalObjs,
                      HandleObject instanceProto,
                      MutableHandleWasmInstanceObject instanceObj) const;
 
     // Tier-2 compilation may be initiated after the Module is constructed at
-    // most once, ideally before any client can attempt to serialize the Module.
-    // When tier-2 compilation completes, ModuleGenerator calls finishTier2()
-    // from a helper thread, passing tier-variant data which will be installed
-    // and made visible.
+    // most once. When tier-2 compilation completes, ModuleGenerator calls
+    // finishTier2() from a helper thread, passing tier-variant data which will
+    // be installed and made visible.
 
     void startTier2(const CompileArgs& args);
     bool finishTier2(UniqueLinkDataTier linkData2, UniqueCodeTier tier2, ModuleEnvironment* env2);
-    void blockOnTier2Complete() const;
+
+    void testingBlockOnTier2Complete() const;
+    bool testingTier2Active() const { return testingTier2Active_; }
 
     // Currently dead, but will be ressurrected with shell tests (bug 1330661)
     // and HTTP cache integration.
 
-    size_t bytecodeSerializedSize() const;
-    void bytecodeSerialize(uint8_t* bytecodeBegin, size_t bytecodeSize) const;
-    bool compilationComplete() const;
-    bool notifyWhenCompilationComplete(JS::WasmModuleListener* listener);
     size_t compiledSerializedSize() const;
     void compiledSerialize(uint8_t* compiledBegin, size_t compiledSize) const;
 
     static bool assumptionsMatch(const Assumptions& current, const uint8_t* compiledBegin,
                                  size_t remain);
     static RefPtr<Module> deserialize(const uint8_t* bytecodeBegin, size_t bytecodeSize,
                                       const uint8_t* compiledBegin, size_t compiledSize,
                                       Metadata* maybeMetadata = nullptr);
