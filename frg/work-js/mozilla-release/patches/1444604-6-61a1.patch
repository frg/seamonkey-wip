# HG changeset patch
# User Jim Blandy <jimb@mozilla.com>
# Date 1521156369 25200
#      Thu Mar 15 16:26:09 2018 -0700
# Node ID 00c6c3c10a3aed6e3dacc35727ce97628c91a4bd
# Parent  f92b2938922f0eac575701127544bd0f6f584c8b
Bug 1444604: Part 6: Add LiveSavedFrameCache::findWithoutInvalidation. r=jorendorff

diff --git a/js/src/vm/SavedStacks.cpp b/js/src/vm/SavedStacks.cpp
--- a/js/src/vm/SavedStacks.cpp
+++ b/js/src/vm/SavedStacks.cpp
@@ -151,16 +151,34 @@ LiveSavedFrameCache::find(JSContext* cx,
         frames->popBack();
         frame.set(nullptr);
         return;
     }
 
     frame.set(frames->back().savedFrame);
 }
 
+void
+LiveSavedFrameCache::findWithoutInvalidation(const FramePtr& framePtr,
+                                             MutableHandleSavedFrame frame) const
+{
+    MOZ_ASSERT(initialized());
+    MOZ_ASSERT(framePtr.hasCachedSavedFrame());
+
+    Key key(framePtr);
+    for (auto& entry : (*frames)) {
+        if (entry.key == key) {
+            frame.set(entry.savedFrame);
+            return;
+        }
+    }
+
+    frame.set(nullptr);
+}
+
 struct SavedFrame::Lookup {
     Lookup(JSAtom* source, uint32_t line, uint32_t column,
            JSAtom* functionDisplayName, JSAtom* asyncCause, SavedFrame* parent,
            JSPrincipals* principals,
            const Maybe<LiveSavedFrameCache::FramePtr>& framePtr = Nothing(),
            jsbytecode* pc = nullptr, Activation* activation = nullptr)
       : source(source),
         line(line),
diff --git a/js/src/vm/Stack.h b/js/src/vm/Stack.h
--- a/js/src/vm/Stack.h
+++ b/js/src/vm/Stack.h
@@ -1352,16 +1352,21 @@ class LiveSavedFrameCache
     // still removed. The next frame, if any, will be a cache hit.
     //
     // This may also set |frame| to nullptr if the cache was populated with
     // SavedFrame objects for a different compartment than cx's current
     // compartment. In this case, the entire cache is flushed.
     void find(JSContext* cx, FramePtr& framePtr, const jsbytecode* pc,
               MutableHandleSavedFrame frame) const;
 
+    // Search the cache for a frame matching |framePtr|, without removing any
+    // entries. Return the matching saved frame, or nullptr if none is found.
+    // This is used for resolving |evalInFramePrev| links.
+    void findWithoutInvalidation(const FramePtr& framePtr, MutableHandleSavedFrame frame) const;
+
     // Push a cache entry mapping |framePtr| and |pc| to |savedFrame| on the top
     // of the cache's stack. You must insert entries for frames from oldest to
     // youngest. They must all be younger than the frame that the |find| method
     // found a hit for; or you must have cleared the entire cache with the
     // |clear| method.
     bool insert(JSContext* cx, FramePtr& framePtr, const jsbytecode* pc,
                 HandleSavedFrame savedFrame);
 
