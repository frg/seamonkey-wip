# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1525269011 -7200
#      Wed May 02 15:50:11 2018 +0200
# Node ID daa8466df43e3f44a5e3cd603c2f903ce4100f6f
# Parent  2ef2f23e9c1d348c5b98aa4de8ee8aae6f70bd4d
Bug 1458173 - Fix some issues reported by TSan. r=jonco

diff --git a/js/src/jit/MIR.cpp b/js/src/jit/MIR.cpp
--- a/js/src/jit/MIR.cpp
+++ b/js/src/jit/MIR.cpp
@@ -1281,18 +1281,21 @@ MConstant::valueToBoolean(bool* res) con
         return true;
       case MIRType::Symbol:
         *res = true;
         return true;
       case MIRType::String:
         *res = toString()->length() != 0;
         return true;
       case MIRType::Object:
-        *res = !EmulatesUndefined(&toObject());
-        return true;
+        // We have to call EmulatesUndefined but that reads obj->group->clasp
+        // and so it's racy when the object has a lazy group. The main callers
+        // of this (MTest, MNot) already know how to fold the object case, so
+        // just give up.
+        return false;
       default:
         MOZ_ASSERT(IsMagicType(type()));
         return false;
     }
 }
 
 HashNumber
 MWasmFloatConstant::valueHash() const
diff --git a/js/src/jit/MIR.h b/js/src/jit/MIR.h
--- a/js/src/jit/MIR.h
+++ b/js/src/jit/MIR.h
@@ -1636,21 +1636,22 @@ class MConstant : public MNullaryInstruc
     static MConstant* NewFloat32(TempAllocator& alloc, double d);
     static MConstant* NewInt64(TempAllocator& alloc, int64_t i);
     static MConstant* NewConstraintlessObject(TempAllocator& alloc, JSObject* v);
     static MConstant* Copy(TempAllocator& alloc, MConstant* src) {
         return new(alloc) MConstant(*src);
     }
 
     // Try to convert this constant to boolean, similar to js::ToBoolean.
-    // Returns false if the type is MIRType::Magic*.
+    // Returns false if the type is MIRType::Magic* or MIRType::Object.
     bool MOZ_MUST_USE valueToBoolean(bool* res) const;
 
     // Like valueToBoolean, but returns the result directly instead of using
-    // an outparam. Should not be used if this constant might be a magic value.
+    // an outparam. Should not be used if this constant might be a magic value
+    // or an object.
     bool valueToBooleanInfallible() const {
         bool res;
         MOZ_ALWAYS_TRUE(valueToBoolean(&res));
         return res;
     }
 
 #ifdef JS_JITSPEW
     void printOpcode(GenericPrinter& out) const override;
diff --git a/js/src/vm/ObjectGroup.h b/js/src/vm/ObjectGroup.h
--- a/js/src/vm/ObjectGroup.h
+++ b/js/src/vm/ObjectGroup.h
@@ -250,23 +250,23 @@ class ObjectGroup : public gc::TenuredCe
                maybePreliminaryObjectsDontCheckGeneration();
     }
 
     inline UnboxedLayout* maybeUnboxedLayout(const AutoSweepObjectGroup& sweep);
     inline UnboxedLayout& unboxedLayout(const AutoSweepObjectGroup& sweep);
 
     UnboxedLayout* maybeUnboxedLayoutDontCheckGeneration() const {
         if (addendumKind() == Addendum_UnboxedLayout)
-            return reinterpret_cast<UnboxedLayout*>(addendum_);
+            return &unboxedLayoutDontCheckGeneration();
         return nullptr;
     }
 
     UnboxedLayout& unboxedLayoutDontCheckGeneration() const {
         MOZ_ASSERT(addendumKind() == Addendum_UnboxedLayout);
-        return *maybeUnboxedLayoutDontCheckGeneration();
+        return *reinterpret_cast<UnboxedLayout*>(addendum_);
     }
 
     void setUnboxedLayout(UnboxedLayout* layout) {
         setAddendum(Addendum_UnboxedLayout, layout);
     }
 
     ObjectGroup* maybeOriginalUnboxedGroup() const {
         if (addendumKind() == Addendum_OriginalUnboxedGroup)
@@ -277,23 +277,23 @@ class ObjectGroup : public gc::TenuredCe
     void setOriginalUnboxedGroup(ObjectGroup* group) {
         setAddendum(Addendum_OriginalUnboxedGroup, group);
     }
 
     TypeDescr* maybeTypeDescr() {
         // Note: there is no need to sweep when accessing the type descriptor
         // of an object, as it is strongly held and immutable.
         if (addendumKind() == Addendum_TypeDescr)
-            return reinterpret_cast<TypeDescr*>(addendum_);
+            return &typeDescr();
         return nullptr;
     }
 
     TypeDescr& typeDescr() {
         MOZ_ASSERT(addendumKind() == Addendum_TypeDescr);
-        return *maybeTypeDescr();
+        return *reinterpret_cast<TypeDescr*>(addendum_);
     }
 
     void setTypeDescr(TypeDescr* descr) {
         setAddendum(Addendum_TypeDescr, descr);
     }
 
     JSFunction* maybeInterpretedFunction() {
         // Note: as with type descriptors, there is no need to sweep when
