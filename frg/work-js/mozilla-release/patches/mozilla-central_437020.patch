# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1537307308 0
#      Tue Sep 18 21:48:28 2018 +0000
# Node ID 303fb0953da3d720582705879415680d33270838
# Parent  dc263497b3391bc08472307a8f3f5d5340c582f9
Bug 1491236 - Always enable unwind tables on *nix r=froydnj

We were already always enabling it on builds with frame pointers on
Android. We now extend this to builds without frame pointers, helping
with client-side stacktracing for crashes.

It's also the default on many platforms, such as x86-64, whether frame
pointer are enabled or not and on x86 with GCC. Somehow, it's not the
case for x86 with clang, so this makes clang builds the same as GCC
builds in that regard.

On ARM, we were always omitting frame pointers because of a GCC bug, but
we've not been using GCC for Android builds for a long time (and the GCC
bug was actually fixed in 4.7 and backported to 4.6, making it fixed in
all versions of GCC we support).

Differential Revision: https://phabricator.services.mozilla.com/D6110

diff --git a/build/autoconf/frameptr.m4 b/build/autoconf/frameptr.m4
--- a/build/autoconf/frameptr.m4
+++ b/build/autoconf/frameptr.m4
@@ -2,28 +2,19 @@ dnl This Source Code Form is subject to 
 dnl License, v. 2.0. If a copy of the MPL was not distributed with this
 dnl file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 dnl Set MOZ_FRAMEPTR_FLAGS to the flags that should be used for enabling or
 dnl disabling frame pointers in this architecture based on the configure
 dnl options
 
 AC_DEFUN([MOZ_SET_FRAMEPTR_FLAGS], [
-  case "$target" in
-  *android*)
-    unwind_tables="-funwind-tables"
-    ;;
-  esac
   if test "$GNU_CC"; then
-    MOZ_ENABLE_FRAME_PTR="-fno-omit-frame-pointer $unwind_tables"
-    MOZ_DISABLE_FRAME_PTR="-fomit-frame-pointer"
-    if test "$CPU_ARCH" = arm; then
-      # http://gcc.gnu.org/bugzilla/show_bug.cgi?id=54398
-      MOZ_ENABLE_FRAME_PTR="$unwind_tables"
-    fi
+    MOZ_ENABLE_FRAME_PTR="-fno-omit-frame-pointer -funwind-tables"
+    MOZ_DISABLE_FRAME_PTR="-fomit-frame-pointer -funwind-tables"
   else
     case "$target" in
     dnl Oy (Frame-Pointer Omission) is only support on x86 compilers
     *-mingw32*)
       MOZ_ENABLE_FRAME_PTR="-Oy-"
       MOZ_DISABLE_FRAME_PTR="-Oy"
     ;;
     esac
