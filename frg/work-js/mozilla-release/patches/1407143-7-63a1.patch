# HG changeset patch
# User Paul Bone <pbone@mozilla.com>
# Date 1529552326 -36000
#      Thu Jun 21 13:38:46 2018 +1000
# Node ID 91f49041e74406c74d2e0e05f4dd51111d414b8f
# Parent  dc5765ae836853ec3ba2fb9a4572c0a06703ee20
Bug 1407143 (Part 7) - Add some nursery().isEmpty() assertions r=jonco

diff --git a/js/src/gc/GC.cpp b/js/src/gc/GC.cpp
--- a/js/src/gc/GC.cpp
+++ b/js/src/gc/GC.cpp
@@ -7076,16 +7076,24 @@ GCRuntime::incrementalCollectSlice(Slice
         stats().writeLogMessage(
             "Incremental: %d, lastMarkSlice: %d, useZeal: %d, budget: %s",
             bool(isIncremental), bool(lastMarkSlice), bool(useZeal),
             budgetBuffer);
     }
 #endif
     MOZ_ASSERT_IF(isIncrementalGCInProgress(), isIncremental || lastMarkSlice);
 
+    /*
+     * It's possible to be in the middle of an incremental collection, but not
+     * doing an incremental collection IF we had to return and re-enter the GC
+     * in order to collect the nursery.
+     */
+    MOZ_ASSERT_IF(isIncrementalGCInProgress() && !isIncremental,
+         lastMarkSlice && nursery().isEmpty());
+
     isIncremental = !budget.isUnlimited();
 
     if (useZeal && hasIncrementalTwoSliceZealMode()) {
         /*
          * Yields between slices occurs at predetermined points in these modes;
          * the budget is not used.
          */
         stats().writeLogMessage(
@@ -7184,16 +7192,17 @@ GCRuntime::incrementalCollectSlice(Slice
 
         incrementalState = State::Sweep;
         lastMarkSlice = false;
         beginSweepPhase(reason, session);
 
         MOZ_FALLTHROUGH;
 
       case State::Sweep:
+        MOZ_ASSERT(nursery().isEmpty());
         if (performSweepActions(budget) == NotFinished)
             break;
 
         endSweepPhase(destroyingRuntime);
 
         incrementalState = State::Finalize;
 
         MOZ_FALLTHROUGH;
@@ -7228,16 +7237,17 @@ GCRuntime::incrementalCollectSlice(Slice
         // Always yield before compacting since it is not incremental.
         if (isCompacting && !budget.isUnlimited())
             break;
 
         MOZ_FALLTHROUGH;
 
       case State::Compact:
         if (isCompacting) {
+            MOZ_ASSERT(nursery().isEmpty());
             if (!startedCompacting)
                 beginCompactPhase();
 
             if (compactPhase(reason, budget, session) == NotFinished)
                 break;
 
             endCompactPhase();
         }
