# HG changeset patch
# User Sean Stangl <sean.stangl@gmail.com>
# Date 1532348100 -10800
#      Mon Jul 23 15:15:00 2018 +0300
# Node ID 65ea32c6a5be230bc1e7c5f5a013bfd4b68d14ef
# Parent  16f5d9f58faab689ee88ebf568c5c504b2ac72ba
Bug 1476124 - Implement enough ARM64 Ion code to run a simple script. r=tcampbell

diff --git a/js/src/jit/CodeGenerator.cpp b/js/src/jit/CodeGenerator.cpp
--- a/js/src/jit/CodeGenerator.cpp
+++ b/js/src/jit/CodeGenerator.cpp
@@ -5193,36 +5193,28 @@ CodeGenerator::generateArgumentsChecks(b
     Label miss;
     for (uint32_t i = info.startArgSlot(); i < info.endArgSlot(); i++) {
         // All initial parameters are guaranteed to be MParameters.
         MParameter* param = rp->getOperand(i)->toParameter();
         const TypeSet* types = param->resultTypeSet();
         if (!types || types->unknown())
             continue;
 
-#ifndef JS_CODEGEN_ARM64
         // Calculate the offset on the stack of the argument.
         // (i - info.startArgSlot())    - Compute index of arg within arg vector.
         // ... * sizeof(Value)          - Scale by value size.
         // ArgToStackOffset(...)        - Compute displacement within arg vector.
         int32_t offset = ArgToStackOffset((i - info.startArgSlot()) * sizeof(Value));
         Address argAddr(masm.getStackPointer(), offset);
 
         // guardObjectType will zero the stack pointer register on speculative
         // paths.
-        Register spectreRegToZero = masm.getStackPointer();
+        Register spectreRegToZero = AsRegister(masm.getStackPointer());
         masm.guardTypeSet(argAddr, types, BarrierKind::TypeSet, temp1, temp2,
                           spectreRegToZero, &miss);
-#else
-        // On ARM64, the stack pointer situation is more complicated. When we
-        // enable Ion, we should figure out how to mitigate Spectre there.
-        mozilla::Unused << temp1;
-        mozilla::Unused << temp2;
-        MOZ_CRASH("NYI");
-#endif
     }
 
     if (miss.used()) {
         if (assert) {
 #ifdef DEBUG
             Label success;
             masm.jump(&success);
             masm.bind(&miss);
diff --git a/js/src/jit/Registers.h b/js/src/jit/Registers.h
--- a/js/src/jit/Registers.h
+++ b/js/src/jit/Registers.h
@@ -136,16 +136,22 @@ IsHiddenSP(RegisterOrSP r)
 
 static inline Register
 AsRegister(RegisterOrSP r)
 {
     MOZ_ASSERT(!IsHiddenSP(r));
     return Register::FromCode(r.code);
 }
 
+static inline Register
+AsRegister(Register r)
+{
+    return r;
+}
+
 inline bool
 operator == (Register r, RegisterOrSP e) {
     return r.code() == e.code;
 }
 
 inline bool
 operator != (Register r, RegisterOrSP e) {
     return !(r == e);
diff --git a/js/src/jit/arm64/Assembler-arm64.h b/js/src/jit/arm64/Assembler-arm64.h
--- a/js/src/jit/arm64/Assembler-arm64.h
+++ b/js/src/jit/arm64/Assembler-arm64.h
@@ -89,18 +89,18 @@ static constexpr Register IntArgReg7 { R
 static constexpr Register HeapReg { Registers::x21 };
 
 // Define unsized Registers.
 #define DEFINE_UNSIZED_REGISTERS(N)  \
 static constexpr Register r##N { Registers::x##N };
 REGISTER_CODE_LIST(DEFINE_UNSIZED_REGISTERS)
 #undef DEFINE_UNSIZED_REGISTERS
 static constexpr Register ip0 { Registers::x16 };
-static constexpr Register ip1 { Registers::x16 };
-static constexpr Register fp  { Registers::x30 };
+static constexpr Register ip1 { Registers::x17 };
+static constexpr Register fp  { Registers::x29 };
 static constexpr Register lr  { Registers::x30 };
 static constexpr Register rzr { Registers::xzr };
 
 // Import VIXL registers into the js::jit namespace.
 #define IMPORT_VIXL_REGISTERS(N)  \
 static constexpr ARMRegister w##N = vixl::w##N;  \
 static constexpr ARMRegister x##N = vixl::x##N;
 REGISTER_CODE_LIST(IMPORT_VIXL_REGISTERS)
diff --git a/js/src/jit/arm64/CodeGenerator-arm64.cpp b/js/src/jit/arm64/CodeGenerator-arm64.cpp
--- a/js/src/jit/arm64/CodeGenerator-arm64.cpp
+++ b/js/src/jit/arm64/CodeGenerator-arm64.cpp
@@ -34,29 +34,48 @@ using JS::GenericNaN;
 CodeGeneratorARM64::CodeGeneratorARM64(MIRGenerator* gen, LIRGraph* graph, MacroAssembler* masm)
   : CodeGeneratorShared(gen, graph, masm)
 {
 }
 
 bool
 CodeGeneratorARM64::generateOutOfLineCode()
 {
-    MOZ_CRASH("generateOutOfLineCode");
+    if (!CodeGeneratorShared::generateOutOfLineCode())
+        return false;
+
+    if (deoptLabel_.used()) {
+        // All non-table-based bailouts will go here.
+        masm.bind(&deoptLabel_);
+
+        // Store the frame size, so the handler can recover the IonScript.
+        masm.Mov(x30, frameSize());
+
+        TrampolinePtr handler = gen->jitRuntime()->getGenericBailoutHandler();
+        masm.jump(handler);
+    }
+
+    return !masm.oom();
 }
 
 void
 CodeGeneratorARM64::emitBranch(Assembler::Condition cond, MBasicBlock* mirTrue, MBasicBlock* mirFalse)
 {
-    MOZ_CRASH("emitBranch");
+    if (isNextBlock(mirFalse->lir())) {
+        jumpToBlock(mirTrue, cond);
+    } else {
+        jumpToBlock(mirFalse, Assembler::InvertCondition(cond));
+        jumpToBlock(mirTrue);
+    }
 }
 
 void
 OutOfLineBailout::accept(CodeGeneratorARM64* codegen)
 {
-    MOZ_CRASH("accept");
+    codegen->visitOutOfLineBailout(this);
 }
 
 void
 CodeGenerator::visitTestIAndBranch(LTestIAndBranch* test)
 {
     MOZ_CRASH("visitTestIAndBranch");
 }
 
@@ -64,41 +83,83 @@ void
 CodeGenerator::visitCompare(LCompare* comp)
 {
     MOZ_CRASH("visitCompare");
 }
 
 void
 CodeGenerator::visitCompareAndBranch(LCompareAndBranch* comp)
 {
-    MOZ_CRASH("visitCompareAndBranch");
+    const MCompare* mir = comp->cmpMir();
+    const MCompare::CompareType type = mir->compareType();
+    const LAllocation* left = comp->left();
+    const LAllocation* right = comp->right();
+
+    if (type == MCompare::Compare_Object || type == MCompare::Compare_Symbol) {
+        masm.cmpPtr(ToRegister(left), ToRegister(right));
+    } else if (right->isConstant()) {
+        masm.cmp32(ToRegister(left), Imm32(ToInt32(right)));
+    } else {
+        masm.cmp32(ToRegister(left), ToRegister(right));
+    }
+
+    Assembler::Condition cond = JSOpToCondition(type, comp->jsop());
+    emitBranch(cond, comp->ifTrue(), comp->ifFalse());
 }
 
 void
 CodeGeneratorARM64::bailoutIf(Assembler::Condition condition, LSnapshot* snapshot)
 {
-    MOZ_CRASH("bailoutIf");
+    encode(snapshot);
+
+    // Though the assembler doesn't track all frame pushes, at least make sure
+    // the known value makes sense.
+    MOZ_ASSERT_IF(frameClass_ != FrameSizeClass::None() && deoptTable_,
+                  frameClass_.frameSize() == masm.framePushed());
+
+    // ARM64 doesn't use a bailout table.
+    InlineScriptTree* tree = snapshot->mir()->block()->trackedTree();
+    OutOfLineBailout* ool = new(alloc()) OutOfLineBailout(snapshot);
+    addOutOfLineCode(ool, new(alloc()) BytecodeSite(tree, tree->script()->code()));
+
+    masm.B(ool->entry(), condition);
 }
 
 void
 CodeGeneratorARM64::bailoutFrom(Label* label, LSnapshot* snapshot)
 {
-    MOZ_CRASH("bailoutFrom");
+    MOZ_ASSERT(label->used());
+    MOZ_ASSERT(!label->bound());
+
+    encode(snapshot);
+
+    // Though the assembler doesn't track all frame pushes, at least make sure
+    // the known value makes sense.
+    MOZ_ASSERT_IF(frameClass_ != FrameSizeClass::None() && deoptTable_,
+                  frameClass_.frameSize() == masm.framePushed());
+
+    // ARM64 doesn't use a bailout table.
+    InlineScriptTree* tree = snapshot->mir()->block()->trackedTree();
+    OutOfLineBailout* ool = new(alloc()) OutOfLineBailout(snapshot);
+    addOutOfLineCode(ool, new(alloc()) BytecodeSite(tree, tree->script()->code()));
+
+    masm.retarget(label, ool->entry());
 }
 
 void
 CodeGeneratorARM64::bailout(LSnapshot* snapshot)
 {
     MOZ_CRASH("bailout");
 }
 
 void
 CodeGeneratorARM64::visitOutOfLineBailout(OutOfLineBailout* ool)
 {
-    MOZ_CRASH("visitOutOfLineBailout");
+    masm.push(Imm32(ool->snapshot()->snapshotOffset()));
+    masm.B(&deoptLabel_);
 }
 
 void
 CodeGenerator::visitMinMaxD(LMinMaxD* ins)
 {
     MOZ_CRASH("visitMinMaxD");
 }
 
@@ -146,35 +207,55 @@ ARMRegister
 toXRegister(const T* a)
 {
     return ARMRegister(ToRegister(a), 64);
 }
 
 js::jit::Operand
 toWOperand(const LAllocation* a)
 {
-    MOZ_CRASH("toWOperand");
+    if (a->isConstant())
+        return js::jit::Operand(ToInt32(a));
+    return js::jit::Operand(toWRegister(a));
 }
 
 vixl::CPURegister
 ToCPURegister(const LAllocation* a, Scalar::Type type)
 {
-    MOZ_CRASH("ToCPURegister");
+    if (a->isFloatReg() && type == Scalar::Float64)
+        return ARMFPRegister(ToFloatRegister(a), 64);
+    if (a->isFloatReg() && type == Scalar::Float32)
+        return ARMFPRegister(ToFloatRegister(a), 32);
+    if (a->isGeneralReg())
+        return ARMRegister(ToRegister(a), 32);
+    MOZ_CRASH("Unknown LAllocation");
 }
 
 vixl::CPURegister
 ToCPURegister(const LDefinition* d, Scalar::Type type)
 {
     return ToCPURegister(d->output(), type);
 }
 
 void
 CodeGenerator::visitAddI(LAddI* ins)
 {
-    MOZ_CRASH("visitAddI");
+    const LAllocation* lhs = ins->getOperand(0);
+    const LAllocation* rhs = ins->getOperand(1);
+    const LDefinition* dest = ins->getDef(0);
+
+    // Platforms with three-operand arithmetic ops don't need recovery.
+    MOZ_ASSERT(!ins->recoversInput());
+
+    if (ins->snapshot()) {
+        masm.Adds(toWRegister(dest), toWRegister(lhs), toWOperand(rhs));
+        bailoutIf(Assembler::Overflow, ins->snapshot());
+    } else {
+        masm.Add(toWRegister(dest), toWRegister(lhs), toWOperand(rhs));
+    }
 }
 
 void
 CodeGenerator::visitSubI(LSubI* ins)
 {
     MOZ_CRASH("visitSubI");
 }
 
@@ -423,23 +504,85 @@ void
 CodeGenerator::visitValue(LValue* value)
 {
     MOZ_CRASH("visitValue");
 }
 
 void
 CodeGenerator::visitBox(LBox* box)
 {
-    MOZ_CRASH("visitBox");
+    const LAllocation* in = box->getOperand(0);
+    ValueOperand result = ToOutValue(box);
+
+    masm.moveValue(TypedOrValueRegister(box->type(), ToAnyRegister(in)), result);
 }
 
 void
 CodeGenerator::visitUnbox(LUnbox* unbox)
 {
-    MOZ_CRASH("visitUnbox");
+    MUnbox* mir = unbox->mir();
+
+    if (mir->fallible()) {
+        const ValueOperand value = ToValue(unbox, LUnbox::Input);
+        Assembler::Condition cond;
+        switch (mir->type()) {
+          case MIRType::Int32:
+            cond = masm.testInt32(Assembler::NotEqual, value);
+            break;
+          case MIRType::Boolean:
+            cond = masm.testBoolean(Assembler::NotEqual, value);
+            break;
+          case MIRType::Object:
+            cond = masm.testObject(Assembler::NotEqual, value);
+            break;
+          case MIRType::String:
+            cond = masm.testString(Assembler::NotEqual, value);
+            break;
+          case MIRType::Symbol:
+            cond = masm.testSymbol(Assembler::NotEqual, value);
+            break;
+          default:
+            MOZ_CRASH("Given MIRType cannot be unboxed.");
+        }
+        bailoutIf(cond, unbox->snapshot());
+    } else {
+#ifdef DEBUG
+        JSValueTag tag = MIRTypeToTag(mir->type());
+        Label ok;
+
+        ValueOperand input = ToValue(unbox, LUnbox::Input);
+        ScratchTagScope scratch(masm, input);
+        masm.splitTagForTest(input, scratch);
+        masm.branchTest32(Assembler::Condition::Equal, scratch, Imm32(tag), &ok);
+        masm.assumeUnreachable("Infallible unbox type mismatch");
+        masm.bind(&ok);
+#endif
+    }
+
+    ValueOperand input = ToValue(unbox, LUnbox::Input);
+    Register result = ToRegister(unbox->output());
+    switch (mir->type()) {
+      case MIRType::Int32:
+        masm.unboxInt32(input, result);
+        break;
+      case MIRType::Boolean:
+        masm.unboxBoolean(input, result);
+        break;
+      case MIRType::Object:
+        masm.unboxObject(input, result);
+        break;
+      case MIRType::String:
+        masm.unboxString(input, result);
+        break;
+      case MIRType::Symbol:
+        masm.unboxSymbol(input, result);
+        break;
+      default:
+        MOZ_CRASH("Given MIRType cannot be unboxed.");
+    }
 }
 
 void
 CodeGenerator::visitDouble(LDouble* ins)
 {
     MOZ_CRASH("visitDouble");
 }
 
@@ -562,17 +705,32 @@ CodeGeneratorARM64::storeElementTyped(co
                                       const LAllocation* index)
 {
     MOZ_CRASH("CodeGeneratorARM64::storeElementTyped");
 }
 
 void
 CodeGeneratorARM64::generateInvalidateEpilogue()
 {
-    MOZ_CRASH("generateInvalidateEpilogue");
+    // Ensure that there is enough space in the buffer for the OsiPoint patching
+    // to occur. Otherwise, we could overwrite the invalidation epilogue.
+    for (size_t i = 0; i < sizeof(void*); i += Assembler::NopSize())
+        masm.nop();
+
+    masm.bind(&invalidate_);
+
+    // Push the Ion script onto the stack (when we determine what that pointer is).
+    invalidateEpilogueData_ = masm.pushWithPatch(ImmWord(uintptr_t(-1)));
+
+    TrampolinePtr thunk = gen->jitRuntime()->getInvalidationThunk();
+    masm.call(thunk);
+
+    // We should never reach this point in JIT code -- the invalidation thunk
+    // should pop the invalidated JS frame and return directly to its caller.
+    masm.assumeUnreachable("Should have returned directly to its caller instead of here.");
 }
 
 template <class U>
 Register
 getBase(U* mir)
 {
     switch (mir->base()) {
       case U::Heap: return HeapReg;
diff --git a/js/src/jit/arm64/Lowering-arm64.cpp b/js/src/jit/arm64/Lowering-arm64.cpp
--- a/js/src/jit/arm64/Lowering-arm64.cpp
+++ b/js/src/jit/arm64/Lowering-arm64.cpp
@@ -38,44 +38,92 @@ LAllocation
 LIRGeneratorARM64::useByteOpRegisterOrNonDoubleConstant(MDefinition* mir)
 {
     MOZ_CRASH("useByteOpRegisterOrNonDoubleConstant");
 }
 
 void
 LIRGenerator::visitBox(MBox* box)
 {
-    MOZ_CRASH("visitBox");
+    MDefinition* opd = box->getOperand(0);
+
+    // If the operand is a constant, emit near its uses.
+    if (opd->isConstant() && box->canEmitAtUses()) {
+        emitAtUses(box);
+        return;
+    }
+
+    if (opd->isConstant()) {
+        define(new(alloc()) LValue(opd->toConstant()->toJSValue()), box, LDefinition(LDefinition::BOX));
+    } else {
+        LBox* ins = new(alloc()) LBox(useRegister(opd), opd->type());
+        define(ins, box, LDefinition(LDefinition::BOX));
+    }
 }
 
 void
 LIRGenerator::visitUnbox(MUnbox* unbox)
 {
-    MOZ_CRASH("visitUnbox");
+    MDefinition* box = unbox->getOperand(0);
+
+    if (box->type() == MIRType::ObjectOrNull) {
+        LUnboxObjectOrNull* lir = new(alloc()) LUnboxObjectOrNull(useRegisterAtStart(box));
+        if (unbox->fallible())
+            assignSnapshot(lir, unbox->bailoutKind());
+        defineReuseInput(lir, unbox, 0);
+        return;
+    }
+
+    MOZ_ASSERT(box->type() == MIRType::Value);
+
+    LUnboxBase* lir;
+    if (IsFloatingPointType(unbox->type())) {
+        lir = new(alloc()) LUnboxFloatingPoint(useRegisterAtStart(box), unbox->type());
+    } else if (unbox->fallible()) {
+        // If the unbox is fallible, load the Value in a register first to
+        // avoid multiple loads.
+        lir = new(alloc()) LUnbox(useRegisterAtStart(box));
+    } else {
+        lir = new(alloc()) LUnbox(useAtStart(box));
+    }
+
+    if (unbox->fallible())
+        assignSnapshot(lir, unbox->bailoutKind());
+
+    define(lir, unbox);
 }
 
 void
 LIRGenerator::visitReturn(MReturn* ret)
 {
-    MOZ_CRASH("visitReturn");
+    MDefinition* opd = ret->getOperand(0);
+    MOZ_ASSERT(opd->type() == MIRType::Value);
+
+    LReturn* ins = new(alloc()) LReturn;
+    ins->setOperand(0, useFixed(opd, JSReturnReg));
+    add(ins);
 }
 
 // x = !y
 void
 LIRGeneratorARM64::lowerForALU(LInstructionHelper<1, 1, 0>* ins, MDefinition* mir, MDefinition* input)
 {
-    MOZ_CRASH("lowerForALU");
+    ins->setOperand(0, ins->snapshot() ? useRegister(input) : useRegisterAtStart(input));
+    define(ins, mir, LDefinition(LDefinition::TypeFrom(mir->type()), LDefinition::REGISTER));
 }
 
 // z = x+y
 void
 LIRGeneratorARM64::lowerForALU(LInstructionHelper<1, 2, 0>* ins, MDefinition* mir,
                                MDefinition* lhs, MDefinition* rhs)
 {
-    MOZ_CRASH("lowerForALU");
+    ins->setOperand(0, ins->snapshot() ? useRegister(lhs) : useRegisterAtStart(lhs));
+    ins->setOperand(1, ins->snapshot() ? useRegisterOrConstant(rhs) :
+                                         useRegisterOrConstantAtStart(rhs));
+    define(ins, mir, LDefinition(LDefinition::TypeFrom(mir->type()), LDefinition::REGISTER));
 }
 
 void
 LIRGeneratorARM64::lowerForFPU(LInstructionHelper<1, 1, 0>* ins, MDefinition* mir, MDefinition* input)
 {
     MOZ_CRASH("lowerForFPU");
 }
 
