# HG changeset patch
# User Anmol Agarwal <anmol.agarwal001@gmail.com>
# Date 1569244765 0
# Node ID 51e40b81f6828da34c2f6f01d9874d41a1911338
# Parent  d19df021704a14b0c60de07291df48582c0d9416
Bug 1554657 - Add a verbose mode to |mach lint| to display log output r=ahal

Differential Revision: https://phabricator.services.mozilla.com/D43507

diff --git a/python/mozlint/mozlint/cli.py b/python/mozlint/mozlint/cli.py
--- a/python/mozlint/mozlint/cli.py
+++ b/python/mozlint/mozlint/cli.py
@@ -36,16 +36,22 @@ class MozlintParser(ArgumentParser):
           'help': "List all available linters and exit.",
           }],
         [['-W', '--warnings'],
          {'dest': 'show_warnings',
           'default': False,
           'action': 'store_true',
           'help': "Display and fail on warnings in addition to errors.",
           }],
+        [['-v', '--verbose'],
+         {'dest': 'show_verbose',
+          'default': False,
+          'action': 'store_true',
+          'help': "Enable verbose logging.",
+          }],
         [['-f', '--format'],
          {'dest': 'formats',
           'action': 'append',
           'help': "Formatter to use. Defaults to 'stylish' on stdout. "
                   "You can specify an optional path as --format formatter:path "
                   "that will be used instead of stdout. "
                   "You can also use multiple formatters at the same time. "
                   "Formatters available: {}.".format(', '.join(all_formatters.keys())),
diff --git a/python/mozlint/mozlint/pathutils.py b/python/mozlint/mozlint/pathutils.py
--- a/python/mozlint/mozlint/pathutils.py
+++ b/python/mozlint/mozlint/pathutils.py
@@ -254,17 +254,17 @@ def expand_exclusions(paths, config, roo
     Args:
         paths (list): List of candidate paths to lint.
         config (dict): Linter's config object.
         root (str): Root of the repository.
 
     Returns:
         Generator which generates list of paths that weren't excluded.
     """
-    extensions = [e.lstrip('.') for e in config['extensions']]
+    extensions = [e.lstrip('.') for e in config.get('extensions', [])]
 
     def normalize(path):
         path = mozpath.normpath(path)
         if os.path.isabs(path):
             return path
         return mozpath.join(root, path)
 
     exclude = list(map(normalize, config.get('exclude', [])))
diff --git a/python/mozlint/mozlint/roller.py b/python/mozlint/mozlint/roller.py
--- a/python/mozlint/mozlint/roller.py
+++ b/python/mozlint/mozlint/roller.py
@@ -1,17 +1,19 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 from __future__ import absolute_import, print_function, unicode_literals
 
 import copy
+import logging
 import os
 import signal
 import sys
+import time
 import traceback
 from concurrent.futures import ProcessPoolExecutor
 from itertools import chain
 from math import ceil
 from multiprocessing import cpu_count
 from multiprocessing.queues import Queue
 from subprocess import CalledProcessError
 
@@ -27,44 +29,60 @@ from .errors import LintersNotConfigured
 from .parser import Parser
 from .pathutils import findobject
 from .result import ResultSummary
 from .types import supported_types
 
 SHUTDOWN = False
 orig_sigint = signal.getsignal(signal.SIGINT)
 
+logger = logging.getLogger("mozlint")
+handler = logging.StreamHandler()
+formatter = logging.Formatter("%(asctime)s.%(msecs)d %(lintname)s (%(pid)s) | %(message)s",
+                              "%H:%M:%S")
+handler.setFormatter(formatter)
+logger.addHandler(handler)
+
 
 def _run_worker(config, paths, **lintargs):
+    log = logging.LoggerAdapter(logger, {
+        "lintname": config.get("name"),
+        "pid": os.getpid()
+    })
+    lintargs['log'] = log
     result = ResultSummary(lintargs['root'])
 
     if SHUTDOWN:
         return result
 
     func = supported_types[config['type']]
+    start_time = time.time()
     try:
         res = func(paths, config, **lintargs) or []
     except Exception:
         traceback.print_exc()
         res = 1
     except (KeyboardInterrupt, SystemExit):
         return result
     finally:
+        end_time = time.time()
+        log.debug("Finished in {:.2f} seconds".format(end_time - start_time))
         sys.stdout.flush()
 
     if not isinstance(res, (list, tuple)):
         if res:
             result.failed_run.add(config['name'])
     else:
         for r in res:
             if not lintargs.get('show_warnings') and r.level == 'warning':
                 result.suppressed_warnings[r.path] += 1
                 continue
 
             result.issues[r.path].append(r)
+
     return result
 
 
 class InterruptableQueue(Queue):
     """A multiprocessing.Queue that catches KeyboardInterrupt when a worker is
     blocking on it and returns None.
 
     This is needed to gracefully handle KeyboardInterrupts when a worker is
@@ -115,16 +133,21 @@ class LintRoller(object):
         self.lintargs['root'] = root
 
         # result state
         self.result = ResultSummary(root)
 
         self.root = root
         self.exclude = exclude or []
 
+        if lintargs.get('show_verbose'):
+            logger.setLevel(logging.DEBUG)
+        else:
+            logger.setLevel(logging.WARNING)
+
     def read(self, paths):
         """Parse one or more linters and add them to the registry.
 
         :param paths: A path or iterable of paths to linter definitions.
         """
         if isinstance(paths, str):
             paths = (paths,)
 
diff --git a/python/mozlint/mozlint/types.py b/python/mozlint/mozlint/types.py
--- a/python/mozlint/mozlint/types.py
+++ b/python/mozlint/mozlint/types.py
@@ -27,35 +27,42 @@ class BaseType(object):
         """Run linter defined by `config` against `paths` with `lintargs`.
 
         :param paths: Paths to lint. Can be a file or directory.
         :param config: Linter config the paths are being linted against.
         :param lintargs: External arguments to the linter not defined in
                          the definition, but passed in by a consumer.
         :returns: A list of :class:`~result.Issue` objects.
         """
+        log = lintargs['log']
+
         if lintargs.get('use_filters', True):
             paths, exclude = filterpaths(
                 lintargs['root'],
                 paths,
                 config['include'],
                 config.get('exclude', []),
                 config.get('extensions', []),
             )
             config['exclude'] = exclude
         elif config.get('exclude'):
             del config['exclude']
 
         if not paths:
-            return
+            return []
+
+        log.debug("Passing the following paths:\n{paths}".format(
+            paths="  \n".join(paths),
+        ))
 
         if self.batch:
             return self._lint(paths, config, **lintargs)
 
         errors = []
+
         try:
             for p in paths:
                 result = self._lint(p, config, **lintargs)
                 if result:
                     errors.extend(result)
         except KeyboardInterrupt:
             pass
         return errors
diff --git a/tools/lint/eslint/__init__.py b/tools/lint/eslint/__init__.py
--- a/tools/lint/eslint/__init__.py
+++ b/tools/lint/eslint/__init__.py
@@ -44,16 +44,17 @@ def setup(root, **lintargs):
     if not setup_helper.check_node_executables_valid():
         return 1
 
     return setup_helper.eslint_maybe_setup()
 
 
 def lint(paths, config, binary=None, fix=None, setup=None, **lintargs):
     """Run eslint."""
+    log = lintargs['log']
     setup_helper.set_project_root(lintargs['root'])
     module_path = setup_helper.get_project_root()
 
     # Valid binaries are:
     #  - Any provided by the binary argument.
     #  - Any pointed at by the ESLINT environmental variable.
     #  - Those provided by |mach lint --setup|.
 
@@ -70,16 +71,17 @@ def lint(paths, config, binary=None, fix
         exclude_args.extend(['--ignore-pattern', os.path.relpath(path, lintargs['root'])])
 
     cmd_args = [binary,
                 os.path.join(module_path, "node_modules", "eslint", "bin", "eslint.js"),
                 # This keeps ext as a single argument.
                 '--ext', '[{}]'.format(','.join(config['extensions'])),
                 '--format', 'json',
                 ] + extra_args + exclude_args + paths
+    log.debug("Command: {}".format(' '.join(cmd_args)))
 
     # eslint requires that --fix be set before the --ext argument.
     if fix:
         cmd_args.insert(2, '--fix')
 
     shell = False
     if os.environ.get('MSYSTEM') in ('MINGW32', 'MINGW64'):
         # The eslint binary needs to be run from a shell with msys
diff --git a/tools/lint/python/compat.py b/tools/lint/python/compat.py
--- a/tools/lint/python/compat.py
+++ b/tools/lint/python/compat.py
@@ -42,31 +42,33 @@ def setup(python):
     """
     binary = find_executable(python)
     if not binary:
         # TODO Bootstrap python2/python3 if not available
         print('warning: {} not detected, skipping py-compat check'.format(python))
 
 
 def run_linter(python, paths, config, **lintargs):
+    log = lintargs['log']
     binary = find_executable(python)
     if not binary:
         # If we're in automation, this is fatal. Otherwise, the warning in the
         # setup method was already printed.
         if 'MOZ_AUTOMATION' in os.environ:
             return 1
         return []
 
     files = expand_exclusions(paths, config, lintargs['root'])
 
     with mozfile.NamedTemporaryFile(mode='w') as fh:
         fh.write('\n'.join(files))
         fh.flush()
 
         cmd = [binary, os.path.join(here, 'check_compat.py'), fh.name]
+        log.debug("Command: {}".format(' '.join(cmd)))
 
         proc = PyCompatProcess(config, cmd)
         proc.run()
         try:
             proc.wait()
         except KeyboardInterrupt:
             proc.kill()
 
diff --git a/tools/lint/python/flake8.py b/tools/lint/python/flake8.py
--- a/tools/lint/python/flake8.py
+++ b/tools/lint/python/flake8.py
@@ -74,16 +74,17 @@ def setup(root, **lintargs):
     if not pip.reinstall_program(FLAKE8_REQUIREMENTS_PATH):
         print(FLAKE8_INSTALL_ERROR)
         return 1
 
 
 def lint(paths, config, **lintargs):
     from flake8.main.application import Application
 
+    log = lintargs['log']
     root = lintargs['root']
     config_path = os.path.join(root, '.flake8')
 
     if lintargs.get('fix'):
         fix_cmd = [
             os.path.join(bindir, 'autopep8'),
             '--global-config', config_path,
             '--in-place', '--recursive',
@@ -100,16 +101,17 @@ def lint(paths, config, **lintargs):
     output_file = mozfile.NamedTemporaryFile(mode='r')
     flake8_cmd = [
         '--config', config_path,
         '--output-file', output_file.name,
         '--format', '{"path":"%(path)s","lineno":%(row)s,'
                     '"column":%(col)s,"rule":"%(code)s","message":"%(text)s"}',
         '--filename', ','.join(['*.{}'.format(e) for e in config['extensions']]),
     ]
+    log.debug("Command: {}".format(' '.join(flake8_cmd)))
 
     orig_make_file_checker_manager = app.make_file_checker_manager
 
     def wrap_make_file_checker_manager(self):
         """Flake8 is very inefficient when it comes to applying exclusion
         rules, using `expand_exclusions` to turn directories into a list of
         relevant python files is an order of magnitude faster.
 
diff --git a/tools/lint/rst/__init__.py.1554657.later b/tools/lint/rst/__init__.py.1554657.later
new file mode 100644
--- /dev/null
+++ b/tools/lint/rst/__init__.py.1554657.later
@@ -0,0 +1,32 @@
+--- __init__.py
++++ __init__.py
+@@ -64,26 +64,29 @@ def parse_with_split(errors):
+     level = filtered_output[2][0:idx].split("/")[1]
+     message = filtered_output[2][idx+2:].split("\n")[0]
+ 
+     return filename, lineno, level, message
+ 
+ 
+ def lint(files, config, **lintargs):
+ 
++    log = lintargs['log']
+     config['root'] = lintargs['root']
+     paths = expand_exclusions(files, config, config['root'])
+     paths = list(paths)
+     chunk_size = 50
+     binary = get_rstcheck_binary()
+ 
+     while paths:
+         cmdargs = [
+             binary,
+         ] + paths[:chunk_size]
++        log.debug("Command: {}".format(' '.join(cmdargs)))
++
+         proc = subprocess.Popen(
+             cmdargs, stdout=subprocess.PIPE,
+             stderr=subprocess.PIPE,
+             env=os.environ,
+             universal_newlines=True,
+         )
+         all_errors = proc.communicate()[1]
+         for errors in all_errors.split("\n"):
diff --git a/tools/lint/rust/__init__.py b/tools/lint/rust/__init__.py
--- a/tools/lint/rust/__init__.py
+++ b/tools/lint/rust/__init__.py
@@ -122,39 +122,40 @@ def is_old_rustfmt(binary):
         output = e.output
 
     if "This version of rustfmt is deprecated" in output:
         return True
 
     return False
 
 
-def run_rustfmt(config, paths, fix=None):
+def run_rustfmt(config, paths, log, fix=None):
     binary = get_rustfmt_binary()
 
     if is_old_rustfmt(binary):
         print(RUSTFMT_DEPRECATED_VERSION)
         return 1
 
     if not binary:
         print(RUSTFMT_NOT_FOUND)
         if "MOZ_AUTOMATION" in os.environ:
             return 1
         return []
 
     cmd_args = [binary]
     if not fix:
         cmd_args.append("--check")
     base_command = cmd_args + paths
+    log.debug("Command: {}".format(' '.join(cmd_args)))
     return parse_issues(config, run_process(config, base_command), paths)
 
 
 def lint(paths, config, fix=None, **lintargs):
-
+    log = lintargs['log']
     files = list(expand_exclusions(paths, config, lintargs['root']))
 
     # to retrieve the future changes
-    results = run_rustfmt(config, files, fix=False)
+    results = run_rustfmt(config, files, log, fix=False)
 
     if fix and results:
         # To do the actual change
         run_rustfmt(config, files, fix=True)
     return results
diff --git a/tools/lint/shell/__init__.py b/tools/lint/shell/__init__.py
--- a/tools/lint/shell/__init__.py
+++ b/tools/lint/shell/__init__.py
@@ -134,17 +134,17 @@ def get_shellcheck_binary():
     binary = os.environ.get('SHELLCHECK')
     if binary:
         return binary
 
     return which('shellcheck')
 
 
 def lint(paths, config, **lintargs):
-
+    log = lintargs['log']
     binary = get_shellcheck_binary()
 
     if not binary:
         print(SHELLCHECK_NOT_FOUND)
         if 'MOZ_AUTOMATION' in os.environ:
             return 1
         return []
 
@@ -154,10 +154,11 @@ def lint(paths, config, **lintargs):
 
     base_command = [binary, '-f', 'json']
     if config.get('excludecodes'):
         base_command.extend(['-e', ','.join(config.get('excludecodes'))])
 
     for f in files:
         cmd = list(base_command)
         cmd.extend(['-s', files[f], f])
+        log.debug("Command: {}".format(cmd))
         run_process(config, cmd)
     return results
diff --git a/tools/lint/spell/__init__.py b/tools/lint/spell/__init__.py
--- a/tools/lint/spell/__init__.py
+++ b/tools/lint/spell/__init__.py
@@ -99,18 +99,18 @@ def get_codespell_binary():
 
 def setup(root, **lintargs):
     if not pip.reinstall_program(CODESPELL_REQUIREMENTS_PATH):
         print(CODESPELL_INSTALL_ERROR)
         return 1
 
 
 def lint(paths, config, fix=None, **lintargs):
+    log = lintargs['log']
     binary = get_codespell_binary()
-
     if not binary:
         print(CODESPELL_NOT_FOUND)
         if 'MOZ_AUTOMATION' in os.environ:
             return 1
         return []
 
     config['root'] = lintargs['root']
     exclude_list = os.path.join(here, 'exclude-list.txt')
@@ -124,13 +124,14 @@ def lint(paths, config, fix=None, **lint
                 '--quiet-level=4',
                 '--ignore-words=' + exclude_list,
                 # Ignore dictonnaries
                 '--skip=*.dic',
                 ]
 
     if fix:
         cmd_args.append('--write-changes')
+    log.debug("Command: {}".format(' '.join(cmd_args)))
 
     base_command = cmd_args + paths
 
     run_process(config, base_command)
     return results
diff --git a/tools/lint/test/conftest.py b/tools/lint/test/conftest.py
--- a/tools/lint/test/conftest.py
+++ b/tools/lint/test/conftest.py
@@ -1,25 +1,27 @@
 from __future__ import absolute_import, print_function
 
+import logging
 import os
 import sys
 from collections import defaultdict
 
 from mozbuild.base import MozbuildObject
 from mozlint.pathutils import findobject
 from mozlint.parser import Parser
 
 import pytest
 
 here = os.path.abspath(os.path.dirname(__file__))
 build = MozbuildObject.from_environment(cwd=here)
 
 lintdir = os.path.dirname(here)
 sys.path.insert(0, lintdir)
+logger = logging.getLogger("mozlint")
 
 
 @pytest.fixture(scope='module')
 def root(request):
     """Return the root directory for the files of the linter under test.
 
     For example, with LINTER=flake8 this would be tools/lint/test/files/flake8.
     """
@@ -83,16 +85,20 @@ def lint(config, root):
     specified.
     """
     try:
         func = findobject(config['payload'])
     except (ImportError, ValueError):
         pytest.fail("could not resolve a lint function from '{}'".format(config['payload']))
 
     def wrapper(paths, config=config, root=root, collapse_results=False, **lintargs):
+        lintargs['log'] = logging.LoggerAdapter(logger, {
+            "lintname": config.get("name"),
+            "pid": os.getpid()
+        })
         results = func(paths, config, root=root, **lintargs)
         if not collapse_results:
             return results
 
         ret = defaultdict(list)
         for r in results:
             ret[r.path].append(r)
         return ret
diff --git a/tools/lint/yamllint_/__init__.py b/tools/lint/yamllint_/__init__.py
--- a/tools/lint/yamllint_/__init__.py
+++ b/tools/lint/yamllint_/__init__.py
@@ -116,26 +116,28 @@ def gen_yamllint_args(cmdargs, paths=Non
     if isinstance(paths, string_types):
         paths = [paths]
     if conf_file and conf_file != 'default':
         return args + ['-c', conf_file] + paths
     return args + paths
 
 
 def lint(files, config, **lintargs):
+    log = lintargs['log']
     if not reinstall_yamllint():
         print(YAMLLINT_INSTALL_ERROR)
         return 1
 
     binary = get_yamllint_binary()
 
     cmdargs = [
         binary,
         '-f', 'parsable'
     ]
+    log.debug("Command: {}".format(' '.join(cmdargs)))
 
     config = config.copy()
     config['root'] = lintargs['root']
 
     # Run any paths with a .yamllint file in the directory separately so
     # it gets picked up. This means only .yamllint files that live in
     # directories that are explicitly included will be considered.
     paths_by_config = defaultdict(list)
