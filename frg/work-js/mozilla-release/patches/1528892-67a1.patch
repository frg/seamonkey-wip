# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1550725792 0
# Node ID b47190a0ad7390da69776b56b8c7792b2f6b2ee0
# Parent  20b082e934b8cbc159774618771372743d209a72
Bug 1528892 - Make preprocessor output more reproducible. r=froydnj

On CI, Windows builds start from different directories on every build,
except when sccache is enabled. This affects many build types, such as
l10n repacks, and the preprocessor likes to put full paths in its
output, which means it includes those different directories, making the
builds non reproducible.

This changes the preprocessor to replace the source and object
directories with generic strings.

Differential Revision: https://phabricator.services.mozilla.com/D20421

diff --git a/python/mozbuild/mozbuild/preprocessor.py b/python/mozbuild/mozbuild/preprocessor.py
--- a/python/mozbuild/mozbuild/preprocessor.py
+++ b/python/mozbuild/mozbuild/preprocessor.py
@@ -40,16 +40,25 @@ if sys.platform == "win32":
 __all__ = [
   'Context',
   'Expression',
   'Preprocessor',
   'preprocess'
 ]
 
 
+def path_starts_with(path, prefix):
+    if os.altsep:
+        prefix = prefix.replace(os.altsep, os.sep)
+        path = path.replace(os.altsep, os.sep)
+    prefix = [os.path.normcase(p) for p in prefix.split(os.sep)]
+    path = [os.path.normcase(p) for p in path.split(os.sep)]
+    return path[:len(prefix)] == prefix
+
+
 class Expression:
     def __init__(self, expression_string):
         """
         Create a new expression with this string.
         The expression will already be parsed into an Abstract Syntax Tree.
         """
         self.content = expression_string
         self.offset = 0
@@ -275,16 +284,25 @@ class Preprocessor:
             RuntimeError.__init__(self, (self.file, self.line, self.key, context))
 
     def __init__(self, defines=None, marker='#'):
         self.context = Context()
         for k,v in {'FILE': '',
                     'LINE': 0,
                     'DIRECTORY': os.path.abspath('.')}.iteritems():
             self.context[k] = v
+        try:
+            # Can import globally because of bootstrapping issues.
+            from buildconfig import topsrcdir, topobjdir
+        except ImportError:
+            # Allow this script to still work independently of a configured objdir.
+            topsrcdir = topobjdir = None
+        self.topsrcdir = topsrcdir
+        self.topobjdir = topobjdir
+        self.curdir = '.'
         self.actionLevel = 0
         self.disableLevel = 0
         # ifStates can be
         #  0: hadTrue
         #  1: wantsTrue
         #  2: #else found
         self.ifStates = []
         self.checkLineNumbers = False
@@ -742,49 +760,57 @@ class Preprocessor:
         oldCheckLineNumbers = self.checkLineNumbers
         self.checkLineNumbers = False
         if isName:
             try:
                 args = str(args)
                 if filters:
                     args = self.applyFilters(args)
                 if not os.path.isabs(args):
-                    args = os.path.join(self.context['DIRECTORY'], args)
+                    args = os.path.join(self.curdir, args)
                 args = open(args, 'rU')
             except Preprocessor.Error:
                 raise
             except:
                 raise Preprocessor.Error(self, 'FILE_NOT_FOUND', str(args))
         self.checkLineNumbers = bool(re.search('\.(js|jsm|java|webidl)(?:\.in)?$', args.name))
         oldFile = self.context['FILE']
         oldLine = self.context['LINE']
         oldDir = self.context['DIRECTORY']
+        oldCurdir = self.curdir
         self.noteLineInfo()
 
         if args.isatty():
             # we're stdin, use '-' and '' for file and dir
             self.context['FILE'] = '-'
             self.context['DIRECTORY'] = ''
+            self.curdir = '.'
         else:
             abspath = os.path.abspath(args.name)
+            self.curdir = os.path.dirname(abspath)
             self.includes.add(abspath)
+            if self.topobjdir and path_starts_with(abspath, self.topobjdir):
+                abspath = '$OBJDIR' + abspath[len(self.topobjdir):]
+            elif self.topsrcdir and path_starts_with(abspath, self.topsrcdir):
+                abspath = '$SRCDIR' + abspath[len(self.topsrcdir):]
             self.context['FILE'] = abspath
             self.context['DIRECTORY'] = os.path.dirname(abspath)
         self.context['LINE'] = 0
 
         for l in args:
             self.context['LINE'] += 1
             self.handleLine(l)
         if isName:
             args.close()
 
         self.context['FILE'] = oldFile
         self.checkLineNumbers = oldCheckLineNumbers
         self.context['LINE'] = oldLine
         self.context['DIRECTORY'] = oldDir
+        self.curdir = oldCurdir
     def do_includesubst(self, args):
         args = self.filter_substitution(args)
         self.do_include(args)
     def do_error(self, args):
         raise Preprocessor.Error(self, 'Error: ', str(args))
 
 
 def preprocess(includes=[sys.stdin], defines={},
diff --git a/python/mozbuild/mozbuild/test/backend/test_build.py b/python/mozbuild/mozbuild/test/backend/test_build.py
--- a/python/mozbuild/mozbuild/test/backend/test_build.py
+++ b/python/mozbuild/mozbuild/test/backend/test_build.py
@@ -142,18 +142,18 @@ class TestBuild(unittest.TestCase):
                                                        'faster'),
                                 target=overrides, silent=False,
                                 line_handler=handle_make_line)
 
             self.validate(config)
 
     def validate(self, config):
         self.maxDiff = None
-        test_path = os.path.join(os.path.dirname(os.path.abspath(__file__)),
-                                 'data', 'build') + os.sep
+        test_path = os.sep.join(('$SRCDIR', 'python', 'mozbuild', 'mozbuild',
+                                 'test', 'backend', 'data', 'build')) + os.sep
 
         # We want unicode instances out of the files, because having plain str
         # makes assertEqual diff output in case of error extra verbose because
         # of the difference in type.
         result = {
             p: f.open().read().decode('utf-8')
             for p, f in FileFinder(mozpath.join(config.topobjdir, 'dist'))
         }
diff --git a/python/mozbuild/mozbuild/test/test_preprocessor.py b/python/mozbuild/mozbuild/test/test_preprocessor.py
--- a/python/mozbuild/mozbuild/test/test_preprocessor.py
+++ b/python/mozbuild/mozbuild/test/test_preprocessor.py
@@ -562,63 +562,77 @@ class TestPreprocessor(unittest.TestCase
         }
 
         with MockedOpen(files):
             self.pp.do_include('f')
             self.assertEqual(self.pp.out.getvalue(), 'foobarbaz\nbarfoobaz\n')
 
     def test_include_line(self):
         files = {
-            'test.js': '\n'.join([
+            'srcdir/test.js': '\n'.join([
                 '#define foo foobarbaz',
                 '#include @inc@',
                 '@bar@',
                 '',
             ]),
-            'bar.js': '\n'.join([
+            'srcdir/bar.js': '\n'.join([
                 '#define bar barfoobaz',
                 '@foo@',
                 '',
             ]),
-            'foo.js': '\n'.join([
+            'srcdir/foo.js': '\n'.join([
                 'bazfoobar',
                 '#include bar.js',
                 'bazbarfoo',
                 '',
             ]),
-            'baz.js': 'baz\n',
-            'f.js': '\n'.join([
+            'objdir/baz.js': 'baz\n',
+            'srcdir/f.js': '\n'.join([
                 '#include foo.js',
                 '#filter substitution',
                 '#define inc bar.js',
                 '#include test.js',
-                '#include baz.js',
+                '#include ../objdir/baz.js',
                 'fin',
                 '',
             ]),
         }
 
+        preprocessed = ('//@line 1 "$SRCDIR/foo.js"\n'
+                        'bazfoobar\n'
+                        '//@line 2 "$SRCDIR/bar.js"\n'
+                        '@foo@\n'
+                        '//@line 3 "$SRCDIR/foo.js"\n'
+                        'bazbarfoo\n'
+                        '//@line 2 "$SRCDIR/bar.js"\n'
+                        'foobarbaz\n'
+                        '//@line 3 "$SRCDIR/test.js"\n'
+                        'barfoobaz\n'
+                        '//@line 1 "$OBJDIR/baz.js"\n'
+                        'baz\n'
+                        '//@line 6 "$SRCDIR/f.js"\n'
+                        'fin\n').replace('DIR/', 'DIR' + os.sep)
+
+        # Try with separate srcdir/objdir
         with MockedOpen(files):
-            self.pp.do_include('f.js')
-            self.assertEqual(self.pp.out.getvalue(),
-                             ('//@line 1 "CWD/foo.js"\n'
-                              'bazfoobar\n'
-                              '//@line 2 "CWD/bar.js"\n'
-                              '@foo@\n'
-                              '//@line 3 "CWD/foo.js"\n'
-                              'bazbarfoo\n'
-                              '//@line 2 "CWD/bar.js"\n'
-                              'foobarbaz\n'
-                              '//@line 3 "CWD/test.js"\n'
-                              'barfoobaz\n'
-                              '//@line 1 "CWD/baz.js"\n'
-                              'baz\n'
-                              '//@line 6 "CWD/f.js"\n'
-                              'fin\n').replace('CWD/',
-                                               os.getcwd() + os.path.sep))
+            self.pp.topsrcdir = os.path.abspath('srcdir')
+            self.pp.topobjdir = os.path.abspath('objdir')
+            self.pp.do_include('srcdir/f.js')
+            self.assertEqual(self.pp.out.getvalue(), preprocessed)
+
+        # Try again with relative objdir
+        self.setUp()
+        files['srcdir/objdir/baz.js'] = files['objdir/baz.js']
+        del files['objdir/baz.js']
+        files['srcdir/f.js'] = files['srcdir/f.js'].replace('../', '')
+        with MockedOpen(files):
+            self.pp.topsrcdir = os.path.abspath('srcdir')
+            self.pp.topobjdir = os.path.abspath('srcdir/objdir')
+            self.pp.do_include('srcdir/f.js')
+            self.assertEqual(self.pp.out.getvalue(), preprocessed)
 
     def test_include_missing_file(self):
         with MockedOpen({'f': '#include foo\n'}):
             with self.assertRaises(Preprocessor.Error) as e:
                 self.pp.do_include('f')
             self.assertEqual(e.exception.key, 'FILE_NOT_FOUND')
 
     def test_include_undefined_variable(self):
diff --git a/python/mozbuild/mozpack/packager/__init__.py b/python/mozbuild/mozpack/packager/__init__.py
--- a/python/mozbuild/mozpack/packager/__init__.py
+++ b/python/mozbuild/mozpack/packager/__init__.py
@@ -171,18 +171,17 @@ class PreprocessorOutputWrapper(object):
     File-like helper to handle the preprocessor output and send it to a parser.
     The parser's handle_line method is called in the relevant errors.context.
     '''
     def __init__(self, preprocessor, parser):
         self._parser = parser
         self._pp = preprocessor
 
     def write(self, str):
-        file = os.path.normpath(os.path.abspath(self._pp.context['FILE']))
-        with errors.context(file, self._pp.context['LINE']):
+        with errors.context(self._pp.context['FILE'], self._pp.context['LINE']):
             self._parser.handle_line(str)
 
 
 def preprocess(input, parser, defines={}):
     '''
     Preprocess the file-like input with the given defines, and send the
     preprocessed output line by line to the given parser.
     '''
diff --git a/python/mozbuild/mozpack/test/test_packager.py b/python/mozbuild/mozpack/test/test_packager.py
--- a/python/mozbuild/mozpack/test/test_packager.py
+++ b/python/mozbuild/mozpack/test/test_packager.py
@@ -1,15 +1,16 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 import unittest
 import mozunit
 import os
+from buildconfig import topobjdir
 from mozpack.packager import (
     preprocess_manifest,
     CallDeque,
     Component,
     SimplePackager,
     SimpleManifestSink,
 )
 from mozpack.files import GeneratedFile
@@ -38,17 +39,17 @@ foo/zot
 #ifdef baz
 [baz]
 baz@SUFFIX@
 #endif
 '''
 
 
 class TestPreprocessManifest(unittest.TestCase):
-    MANIFEST_PATH = os.path.join(os.path.abspath(os.curdir), 'manifest')
+    MANIFEST_PATH = os.path.join('$OBJDIR', 'manifest')
 
     EXPECTED_LOG = [
         ((MANIFEST_PATH, 2), 'add', '', 'bar/*'),
         ((MANIFEST_PATH, 4), 'add', 'foo', 'foo/*'),
         ((MANIFEST_PATH, 5), 'remove', 'foo', 'foo/bar'),
         ((MANIFEST_PATH, 6), 'add', 'foo', 'chrome.manifest'),
         ((MANIFEST_PATH, 8), 'add', 'zot destdir="destdir"', 'foo/zot'),
     ]
@@ -63,16 +64,21 @@ class TestPreprocessManifest(unittest.Te
 
             def remove(self, component, path):
                 self._log(errors.get_context(), 'remove', repr(component), path)
 
             def _log(self, *args):
                 self.log.append(args)
 
         self.sink = MockSink()
+        self.cwd = os.getcwd()
+        os.chdir(topobjdir)
+
+    def tearDown(self):
+        os.chdir(self.cwd)
 
     def test_preprocess_manifest(self):
         with MockedOpen({'manifest': MANIFEST}):
             preprocess_manifest(self.sink, 'manifest')
         self.assertEqual(self.sink.log, self.EXPECTED_LOG)
 
     def test_preprocess_manifest_missing_define(self):
         with MockedOpen({'manifest': MANIFEST}):
