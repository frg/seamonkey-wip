# HG changeset patch
# User Andre Bargull <andre.bargull@gmail.com>
# Date 1531153471 25200
#      Mon Jul 09 09:24:31 2018 -0700
# Node ID 8719529f5d4d8e626e023700df2bad191baaf0b6
# Parent  0b2b4b7fec99ab1ab584036e1c1dafae683709cd
Bug 1473970 - Part 4: Skip creating the resolving functions in EnqueuePromiseResolveThenableBuiltinJob. r=arai

diff --git a/js/src/builtin/Promise.cpp b/js/src/builtin/Promise.cpp
--- a/js/src/builtin/Promise.cpp
+++ b/js/src/builtin/Promise.cpp
@@ -394,18 +394,19 @@ enum ReactionRecordSlots {
     // flag must be set.
     ReactionRecordSlot_Promise = 0,
 
     // The [[Handler]] field(s) of a PromiseReaction record. We create a
     // single reaction record for fulfillment and rejection, therefore our
     // PromiseReaction implementation needs two [[Handler]] fields.
     //
     // The slot value is either a callable object, an integer constant from
-    // the |PromiseHandler| enum, or null. If the value is null, the
-    // REACTION_FLAG_DEBUGGER_DUMMY flag must be set.
+    // the |PromiseHandler| enum, or null. If the value is null, either the
+    // REACTION_FLAG_DEBUGGER_DUMMY or the
+    // REACTION_FLAG_DEFAULT_RESOLVING_HANDLER flag must be set.
     ReactionRecordSlot_OnFulfilled,
     ReactionRecordSlot_OnRejected,
 
     // The functions to resolve or reject the promise. Matches the
     // [[Capability]].[[Resolve]] and [[Capability]].[[Reject]] fields from
     // the spec.
     //
     // The slot values are either callable objects or null, but the latter
@@ -422,26 +423,30 @@ enum ReactionRecordSlots {
 
     // Argument when calling the function stored in OnFulfilled or OnRejected.
     ReactionRecordSlot_HandlerArg,
 
     // Additional slot to store extra data for specific reaction record types.
     //
     // - When the REACTION_FLAG_ASYNC_GENERATOR flag is set, this slot store
     //   the async generator function for this promise reaction.
-    ReactionRecordSlot_Generator,
+    // - When the REACTION_FLAG_DEFAULT_RESOLVING_HANDLER flag is set, this
+    //   slot stores the promise to resolve when conceptually "calling" the
+    //   OnFulfilled or OnRejected handlers.
+    ReactionRecordSlot_GeneratorOrPromiseToResolve,
 
     ReactionRecordSlots,
 };
 
 // ES2016, 25.4.1.2.
 class PromiseReactionRecord : public NativeObject
 {
   static constexpr size_t REACTION_FLAG_RESOLVED = 0x1;
   static constexpr size_t REACTION_FLAG_FULFILLED = 0x2;
+  static constexpr size_t REACTION_FLAG_DEFAULT_RESOLVING_HANDLER = 0x4;
   static constexpr size_t REACTION_FLAG_ASYNC_FUNCTION = 0x8;
   static constexpr size_t REACTION_FLAG_ASYNC_GENERATOR = 0x10;
   static constexpr size_t REACTION_FLAG_DEBUGGER_DUMMY = 0x20;
 
   void setFlagOnInitialState(size_t flag) {
       int32_t flags = this->flags();
       MOZ_ASSERT(flags == 0, "Can't modify with non-default flags");
       flags |= flag;
@@ -465,34 +470,47 @@ class PromiseReactionRecord : public Nat
         int32_t flags = this->flags();
         MOZ_ASSERT(!(flags & REACTION_FLAG_RESOLVED));
         MOZ_ASSERT(state != JS::PromiseState::Pending, "Can't revert a reaction to pending.");
         flags |= REACTION_FLAG_RESOLVED;
         if (state == JS::PromiseState::Fulfilled)
             flags |= REACTION_FLAG_FULFILLED;
         setFixedSlot(ReactionRecordSlot_Flags, Int32Value(flags));
     }
+    void setIsDefaultResolvingHandler(PromiseObject* promiseToResolve) {
+        setFlagOnInitialState(REACTION_FLAG_DEFAULT_RESOLVING_HANDLER);
+        setFixedSlot(ReactionRecordSlot_GeneratorOrPromiseToResolve, ObjectValue(*promiseToResolve));
+    }
+    bool isDefaultResolvingHandler() {
+        int32_t flags = this->flags();
+        return flags & REACTION_FLAG_DEFAULT_RESOLVING_HANDLER;
+    }
+    PromiseObject* defaultResolvingPromise() {
+        MOZ_ASSERT(isDefaultResolvingHandler());
+        const Value& promiseToResolve = getFixedSlot(ReactionRecordSlot_GeneratorOrPromiseToResolve);
+        return &promiseToResolve.toObject().as<PromiseObject>();
+    }
     void setIsAsyncFunction() {
         setFlagOnInitialState(REACTION_FLAG_ASYNC_FUNCTION);
     }
     bool isAsyncFunction() {
         int32_t flags = this->flags();
         return flags & REACTION_FLAG_ASYNC_FUNCTION;
     }
     void setIsAsyncGenerator(AsyncGeneratorObject* asyncGenObj) {
         setFlagOnInitialState(REACTION_FLAG_ASYNC_GENERATOR);
-        setFixedSlot(ReactionRecordSlot_Generator, ObjectValue(*asyncGenObj));
+        setFixedSlot(ReactionRecordSlot_GeneratorOrPromiseToResolve, ObjectValue(*asyncGenObj));
     }
     bool isAsyncGenerator() {
         int32_t flags = this->flags();
         return flags & REACTION_FLAG_ASYNC_GENERATOR;
     }
     AsyncGeneratorObject* asyncGenerator() {
         MOZ_ASSERT(isAsyncGenerator());
-        const Value& generator = getFixedSlot(ReactionRecordSlot_Generator);
+        const Value& generator = getFixedSlot(ReactionRecordSlot_GeneratorOrPromiseToResolve);
         return &generator.toObject().as<AsyncGeneratorObject>();
     }
     void setIsDebuggerDummy() {
         setFlagOnInitialState(REACTION_FLAG_DEBUGGER_DUMMY);
     }
     bool isDebuggerDummy() {
         int32_t flags = this->flags();
         return flags & REACTION_FLAG_DEBUGGER_DUMMY;
@@ -1193,16 +1211,64 @@ TriggerPromiseReactions(JSContext* cx, H
         reaction = &reactionVal.toObject();
         if (!EnqueuePromiseReactionJob(cx, reaction, valueOrReason, state))
             return false;
     }
 
     return true;
 }
 
+// Implements PromiseReactionJob optimized for the case when the reaction
+// handler is one of the default resolving functions as created by the
+// CreateResolvingFunctions abstract operation.
+static MOZ_MUST_USE bool
+DefaultResolvingPromiseReactionJob(JSContext* cx, Handle<PromiseReactionRecord*> reaction,
+                                   MutableHandleValue rval)
+{
+    MOZ_ASSERT(reaction->targetState() != JS::PromiseState::Pending);
+
+    Rooted<PromiseObject*> promiseToResolve(cx, reaction->defaultResolvingPromise());
+
+    // Testing functions allow to directly settle a promise without going
+    // through the resolving functions. In that case the normal bookkeeping to
+    // ensure only pending promises can be resolved doesn't apply and we need
+    // to manually check for already settled promises. We still call
+    // RunResolutionFunction for consistency with PromiseReactionJob.
+    ResolutionMode resolutionMode = ResolveMode;
+    RootedValue handlerResult(cx, UndefinedValue());
+    if (promiseToResolve->state() == JS::PromiseState::Pending) {
+        RootedValue argument(cx, reaction->handlerArg());
+
+        // Step 6.
+        bool ok;
+        if (reaction->targetState() == JS::PromiseState::Fulfilled)
+            ok = ResolvePromiseInternal(cx, promiseToResolve, argument);
+        else
+            ok = RejectMaybeWrappedPromise(cx, promiseToResolve, argument);
+
+        if (!ok) {
+            resolutionMode = RejectMode;
+            if (!MaybeGetAndClearException(cx, &handlerResult))
+                return false;
+        }
+    }
+
+    // Steps 7-9.
+    size_t hookSlot = resolutionMode == RejectMode
+                      ? ReactionRecordSlot_Reject
+                      : ReactionRecordSlot_Resolve;
+    RootedObject callee(cx, reaction->getFixedSlot(hookSlot).toObjectOrNull());
+    RootedObject promiseObj(cx, reaction->promise());
+    if (!RunResolutionFunction(cx, callee, handlerResult, resolutionMode, promiseObj))
+        return false;
+
+    rval.setUndefined();
+    return true;
+}
+
 static MOZ_MUST_USE bool
 AsyncFunctionPromiseReactionJob(JSContext* cx, Handle<PromiseReactionRecord*> reaction,
                                 MutableHandleValue rval)
 {
     MOZ_ASSERT(reaction->isAsyncFunction());
 
     RootedValue handlerVal(cx, reaction->handler());
     RootedValue argument(cx, reaction->handlerArg());
@@ -1313,16 +1379,18 @@ PromiseReactionJob(JSContext* cx, unsign
             return false;
         }
         MOZ_RELEASE_ASSERT(reactionObj->is<PromiseReactionRecord>());
         ar.emplace(cx, reactionObj);
     }
 
     // Steps 1-2.
     Rooted<PromiseReactionRecord*> reaction(cx, &reactionObj->as<PromiseReactionRecord>());
+    if (reaction->isDefaultResolvingHandler())
+        return DefaultResolvingPromiseReactionJob(cx, reaction, args.rval());
     if (reaction->isAsyncFunction())
         return AsyncFunctionPromiseReactionJob(cx, reaction, args.rval());
     if (reaction->isAsyncGenerator())
         return AsyncGeneratorPromiseReactionJob(cx, reaction, args.rval());
     if (reaction->isDebuggerDummy())
         return true;
 
     // Step 3.
@@ -1434,16 +1502,20 @@ PromiseResolveThenableJob(JSContext* cx,
 
     FixedInvokeArgs<1> rejectArgs(cx);
     rejectArgs[0].set(rval);
 
     RootedValue rejectVal(cx, ObjectValue(*rejectFn));
     return Call(cx, rejectVal, UndefinedHandleValue, rejectArgs, &rval);
 }
 
+static MOZ_MUST_USE bool
+OriginalPromiseThenWithoutSettleHandlers(JSContext* cx, Handle<PromiseObject*> promise,
+                                         Handle<PromiseObject*> promiseToResolve);
+
 /**
  * Specialization of PromiseResolveThenableJob when the `thenable` is a
  * built-in Promise object and the `then` property is the built-in
  * `Promise.prototype.then` function.
  *
  * A PromiseResolveBuiltinThenableJob is set as the native function of an
  * extended JSFunction object, with all information required for the job's
  * execution stored in the function's extended slots.
@@ -1459,43 +1531,40 @@ PromiseResolveBuiltinThenableJob(JSConte
     RootedFunction job(cx, &args.callee().as<JSFunction>());
     RootedObject promise(cx, &job->getExtendedSlot(BuiltinThenableJobSlot_Promise).toObject());
     RootedObject thenable(cx, &job->getExtendedSlot(BuiltinThenableJobSlot_Thenable).toObject());
 
     assertSameCompartment(cx, promise, thenable);
     MOZ_ASSERT(promise->is<PromiseObject>());
     MOZ_ASSERT(thenable->is<PromiseObject>());
 
-    // Step 1.
-    RootedObject resolveFn(cx);
-    RootedObject rejectFn(cx);
-    if (!CreateResolvingFunctions(cx, promise, &resolveFn, &rejectFn))
-        return false;
+    // Step 1 (Skipped).
 
     // Step 2.
-    RootedValue resolveVal(cx, ObjectValue(*resolveFn));
-    RootedValue rejectVal(cx, ObjectValue(*rejectFn));
-
     // In difference to the usual pattern, we return immediately on success.
-    RootedObject resultPromise(cx);
-    if (OriginalPromiseThen(cx, thenable.as<PromiseObject>(), resolveVal, rejectVal,
-                            &resultPromise, CreateDependentPromise::SkipIfCtorUnobservable))
+    if (OriginalPromiseThenWithoutSettleHandlers(cx, thenable.as<PromiseObject>(),
+                                                 promise.as<PromiseObject>()))
     {
         return true;
     }
 
     // Steps 3-4.
     RootedValue exception(cx);
     if (!MaybeGetAndClearException(cx, &exception))
         return false;
 
-    FixedInvokeArgs<1> rejectArgs(cx);
-    rejectArgs[0].set(exception);
-
-    return Call(cx, rejectVal, UndefinedHandleValue, rejectArgs, &exception);
+    // Testing functions allow to directly settle a promise without going
+    // through the resolving functions. In that case the normal bookkeeping to
+    // ensure only pending promises can be resolved doesn't apply and we need
+    // to manually check for already settled promises. The exception is simply
+    // dropped when this case happens.
+    if (promise->as<PromiseObject>().state() != JS::PromiseState::Pending)
+        return true;
+
+    return RejectMaybeWrappedPromise(cx, promise, exception);
 }
 
 /**
  * Tells the embedding to enqueue a Promise resolve thenable job, based on
  * three parameters:
  * promiseToResolve_ - The promise to resolve, obviously.
  * thenable_ - The thenable to resolve the Promise with.
  * thenVal - The `then` function to invoke with the `thenable` as the receiver.
@@ -1961,16 +2030,22 @@ Promise_static_all(JSContext* cx, unsign
     return true;
 }
 
 static MOZ_MUST_USE bool PerformPromiseThen(JSContext* cx, Handle<PromiseObject*> promise,
                                             HandleValue onFulfilled_, HandleValue onRejected_,
                                             HandleObject resultPromise,
                                             HandleObject resolve, HandleObject reject);
 
+static MOZ_MUST_USE bool
+PerformPromiseThenWithoutSettleHandlers(JSContext* cx, Handle<PromiseObject*> promise,
+                                        Handle<PromiseObject*> promiseToResolve,
+                                        HandleObject resultPromise, HandleObject resolve,
+                                        HandleObject reject);
+
 static bool PromiseAllResolveElementFunction(JSContext* cx, unsigned argc, Value* vp);
 
 // Unforgeable version of ES2016, 25.4.4.1.
 MOZ_MUST_USE JSObject*
 js::GetWaitForAllPromise(JSContext* cx, const JS::AutoObjectVector& promises)
 {
 #ifdef DEBUG
     for (size_t i = 0, len = promises.length(); i < len; i++) {
@@ -2679,55 +2754,91 @@ NewReactionRecord(JSContext* cx, HandleO
 }
 
 static bool
 IsPromiseSpecies(JSContext* cx, JSFunction* species)
 {
     return species->maybeNative() == Promise_static_species;
 }
 
+static bool
+PromiseThenNewPromiseCapability(JSContext* cx, HandleObject promiseObj,
+                                CreateDependentPromise createDependent,
+                                MutableHandleObject resultPromise,
+                                MutableHandleObject resolve, MutableHandleObject reject)
+{
+    if (createDependent != CreateDependentPromise::Never) {
+        // Step 3.
+        RootedObject C(cx, SpeciesConstructor(cx, promiseObj, JSProto_Promise, IsPromiseSpecies));
+        if (!C)
+            return false;
+
+        if (createDependent == CreateDependentPromise::Always ||
+            !IsNativeFunction(C, PromiseConstructor))
+        {
+            // Step 4.
+            if (!NewPromiseCapability(cx, C, resultPromise, resolve, reject, true))
+                return false;
+        }
+    }
+
+    return true;
+}
+
 // ES2016, 25.4.5.3., steps 3-5.
 MOZ_MUST_USE bool
 js::OriginalPromiseThen(JSContext* cx, Handle<PromiseObject*> promise,
                         HandleValue onFulfilled, HandleValue onRejected,
                         MutableHandleObject dependent, CreateDependentPromise createDependent)
 {
     RootedObject promiseObj(cx, promise);
     if (promise->compartment() != cx->compartment()) {
         if (!cx->compartment()->wrap(cx, &promiseObj))
             return false;
     }
 
+    // Steps 3-4.
     RootedObject resultPromise(cx);
     RootedObject resolve(cx);
     RootedObject reject(cx);
-
-    if (createDependent != CreateDependentPromise::Never) {
-        // Step 3.
-        RootedObject C(cx, SpeciesConstructor(cx, promiseObj, JSProto_Promise, IsPromiseSpecies));
-        if (!C)
-            return false;
-
-        if (createDependent == CreateDependentPromise::Always ||
-            !IsNativeFunction(C, PromiseConstructor))
-        {
-            // Step 4.
-            if (!NewPromiseCapability(cx, C, &resultPromise, &resolve, &reject, true))
-                return false;
-        }
+    if (!PromiseThenNewPromiseCapability(cx, promiseObj, createDependent, &resultPromise,
+                                         &resolve, &reject))
+    {
+        return false;
     }
 
     // Step 5.
     if (!PerformPromiseThen(cx, promise, onFulfilled, onRejected, resultPromise, resolve, reject))
         return false;
 
     dependent.set(resultPromise);
     return true;
 }
 
+static MOZ_MUST_USE bool
+OriginalPromiseThenWithoutSettleHandlers(JSContext* cx, Handle<PromiseObject*> promise,
+                                         Handle<PromiseObject*> promiseToResolve)
+{
+    assertSameCompartment(cx, promise);
+
+    // Steps 3-4.
+    RootedObject resultPromise(cx);
+    RootedObject resolve(cx);
+    RootedObject reject(cx);
+    if (!PromiseThenNewPromiseCapability(cx, promise, CreateDependentPromise::SkipIfCtorUnobservable,
+                                         &resultPromise, &resolve, &reject))
+    {
+        return false;
+    }
+
+    // Step 5.
+    return PerformPromiseThenWithoutSettleHandlers(cx, promise, promiseToResolve, resultPromise,
+                                                   resolve, reject);
+}
+
 static MOZ_MUST_USE bool PerformPromiseThenWithReaction(JSContext* cx,
                                                         Handle<PromiseObject*> promise,
                                                         Handle<PromiseReactionRecord*> reaction);
 
 // Some async/await functions are implemented here instead of
 // js/src/builtin/AsyncFunction.cpp, to call Promise internal functions.
 
 // ES 2018 draft 14.6.11 and 14.7.14 step 1.
@@ -3407,16 +3518,44 @@ PerformPromiseThen(JSContext* cx, Handle
                                                                   IncumbentGlobalObject::Yes));
     if (!reaction)
         return false;
 
     return PerformPromiseThenWithReaction(cx, promise, reaction);
 }
 
 static MOZ_MUST_USE bool
+PerformPromiseThenWithoutSettleHandlers(JSContext* cx, Handle<PromiseObject*> promise,
+                                        Handle<PromiseObject*> promiseToResolve,
+                                        HandleObject resultPromise, HandleObject resolve,
+                                        HandleObject reject)
+{
+    // Step 1 (implicit).
+    // Step 2 (implicit).
+
+    // Step 3.
+    HandleValue onFulfilled = NullHandleValue;
+
+    // Step 4.
+    HandleValue onRejected = NullHandleValue;
+
+    // Step 7.
+    Rooted<PromiseReactionRecord*> reaction(cx, NewReactionRecord(cx, resultPromise,
+                                                                  onFulfilled, onRejected,
+                                                                  resolve, reject,
+                                                                  IncumbentGlobalObject::Yes));
+    if (!reaction)
+        return false;
+
+    reaction->setIsDefaultResolvingHandler(promiseToResolve);
+
+    return PerformPromiseThenWithReaction(cx, promise, reaction);
+}
+
+static MOZ_MUST_USE bool
 PerformPromiseThenWithReaction(JSContext* cx, Handle<PromiseObject*> promise,
                                Handle<PromiseReactionRecord*> reaction)
 {
     JS::PromiseState state = promise->state();
     int32_t flags = promise->getFixedSlot(PromiseSlot_Flags).toInt32();
     if (state == JS::PromiseState::Pending) {
         // Steps 5,6 (reordered).
         // Instead of creating separate reaction records for fulfillment and
