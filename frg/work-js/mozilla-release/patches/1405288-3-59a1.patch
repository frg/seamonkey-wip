# HG changeset patch
# User Patrick Brosset <pbrosset@mozilla.com>
# Date 1508404343 -7200
# Node ID 531086ab7eba004afb1217b6e5ac46369094339b
# Parent  6a0db18be4c13930e141ab959044fc2c85d97fa9
Bug 1405288 - Remove scrollIntoView actor method check from the inspector r=ochameau

The scrollIntoView actor method was added in bug 901250 3 years ago and
shipped with FF 40.
We support all the way back to the latest ESR (52 now).
So let's drop this backward compat check code.

MozReview-Commit-ID: 5FKRpiOXfzd

diff --git a/devtools/client/inspector/inspector.js b/devtools/client/inspector/inspector.js
--- a/devtools/client/inspector/inspector.js
+++ b/devtools/client/inspector/inspector.js
@@ -126,17 +126,16 @@ function Inspector(toolbox) {
   this.onShowBoxModelHighlighterForNode =
     this.onShowBoxModelHighlighterForNode.bind(this);
   this.onSidebarHidden = this.onSidebarHidden.bind(this);
   this.onSidebarResized = this.onSidebarResized.bind(this);
   this.onSidebarSelect = this.onSidebarSelect.bind(this);
   this.onSidebarShown = this.onSidebarShown.bind(this);
 
   this._target.on("will-navigate", this._onBeforeNavigate);
-  this._detectingActorFeatures = this._detectActorFeatures();
 }
 
 Inspector.prototype = {
   /**
    * open is effectively an asynchronous constructor
    */
   init: Task.async(function* () {
     // Localize all the nodes containing a data-localization attribute.
@@ -210,33 +209,16 @@ Inspector.prototype = {
    * while still initializing (and making protocol requests).
    */
   _handleRejectionIfNotDestroyed: function (e) {
     if (!this._panelDestroyer) {
       console.error(e);
     }
   },
 
-  /**
-   * Figure out what features the backend supports
-   */
-  _detectActorFeatures: function () {
-    this._supportsScrollIntoView = false;
-
-    // Use getActorDescription first so that all actorHasMethod calls use
-    // a cached response from the server.
-    return this._target.getActorDescription("domwalker").then(desc => {
-      return promise.all([
-        this._target.actorHasMethod("domnode", "scrollIntoView").then(value => {
-          this._supportsScrollIntoView = value;
-        }).catch(console.error)
-      ]);
-    });
-  },
-
   _deferredOpen: async function (defaultSelection) {
     this.breadcrumbs = new HTMLBreadcrumbs(this);
 
     this.walker.on("new-root", this.onNewRoot);
 
     this.selection.on("new-node-front", this.onNewSelection);
     this.selection.on("detached-front", this.onDetached);
 
@@ -1354,17 +1336,16 @@ Inspector.prototype = {
       type: "separator",
     }));
 
     menu.append(new MenuItem({
       id: "node-menu-scrollnodeintoview",
       label: INSPECTOR_L10N.getStr("inspectorScrollNodeIntoView.label"),
       accesskey:
         INSPECTOR_L10N.getStr("inspectorScrollNodeIntoView.accesskey"),
-      hidden: !this._supportsScrollIntoView,
       disabled: !isSelectionElement,
       click: () => this.scrollNodeIntoView(),
     }));
     menu.append(new MenuItem({
       id: "node-menu-screenshotnode",
       label: INSPECTOR_L10N.getStr("inspectorScreenshotNode.label"),
       disabled: !isScreenshotable,
       click: () => this.screenshotNode().catch(console.error),
diff --git a/devtools/client/inspector/markup/test/browser_markup_links_04.js b/devtools/client/inspector/markup/test/browser_markup_links_04.js
--- a/devtools/client/inspector/markup/test/browser_markup_links_04.js
+++ b/devtools/client/inspector/markup/test/browser_markup_links_04.js
@@ -88,22 +88,16 @@ add_task(function* () {
     info("Simulating a context click on the popupNode");
     let allMenuItems = openContextMenuAndGetAllItems(inspector, {
       target: popupNode,
     });
 
     let linkFollow = allMenuItems.find(i => i.id === "node-menu-link-follow");
     let linkCopy = allMenuItems.find(i => i.id === "node-menu-link-copy");
 
-    // The contextual menu setup is async, because it needs to know if the
-    // inspector has the resolveRelativeURL method first. So call actorHasMethod
-    // here too to make sure the first call resolves first and the menu is
-    // properly setup.
-    yield inspector.target.actorHasMethod("inspector", "resolveRelativeURL");
-
     is(linkFollow.visible, test.isLinkFollowItemVisible,
       "The follow-link item display is correct");
     is(linkCopy.visible, test.isLinkCopyItemVisible,
       "The copy-link item display is correct");
 
     if (test.isLinkFollowItemVisible) {
       is(linkFollow.label, test.linkFollowItemLabel,
         "the follow-link label is correct");
diff --git a/devtools/client/inspector/test/shared-head.js b/devtools/client/inspector/test/shared-head.js
--- a/devtools/client/inspector/test/shared-head.js
+++ b/devtools/client/inspector/test/shared-head.js
@@ -25,19 +25,16 @@ var openInspector = Task.async(function*
                                         hostType);
   let inspector = toolbox.getPanel("inspector");
 
   if (inspector._updateProgress) {
     info("Need to wait for the inspector to update");
     yield inspector.once("inspector-updated");
   }
 
-  info("Waiting for actor features to be detected");
-  yield inspector._detectingActorFeatures;
-
   yield registerTestActor(toolbox.target.client);
   let testActor = yield getTestActor(toolbox);
 
   return {toolbox, inspector, testActor};
 });
 
 /**
  * Open the toolbox, with the inspector tool visible, and the one of the sidebar
