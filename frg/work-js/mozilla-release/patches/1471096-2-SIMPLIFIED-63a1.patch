# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1529706506 -32400
# Node ID 909312d0fc69e078cd26c545df63067baada1816
# Parent  5e23778a222e2b1cca7d36faab8e28950f86f085
Bug 1471096 - Choose OOM hooking version to use at build time rather than configure time. r=froydnj

When I originally implemented bug 1458161, this is how it was done, but
it was suggested to use a configure-time check. This turned out to not
be great, because the rust compiler changes regularly, and we don't run
the configure tests when the version changes. When people upgraded their
rust compiler to 1.27, the code subsequently failed to build because the
features were still set for the previous version they had installed.

diff --git a/build/moz.configure/rust.configure b/build/moz.configure/rust.configure
--- a/build/moz.configure/rust.configure
+++ b/build/moz.configure/rust.configure
@@ -250,18 +250,16 @@ def rust_target_env_name(triple):
 # We need this to form various Cargo environment variables, as there is no
 # uppercase function in make, and we don't want to shell out just for
 # converting a string to uppercase.
 set_config('RUST_TARGET_ENV_NAME', rust_target_env_name)
 
 # This is used for putting source info into symbol files.
 set_config('RUSTC_COMMIT', depends(rustc_info)(lambda i: i.commit))
 
-set_config('RUSTC_VERSION', depends(rustc_info)(lambda i: str(i.version)))
-
 # Until we remove all the other Rust checks in old-configure.
 add_old_configure_assignment('RUSTC', rustc)
 add_old_configure_assignment('RUST_TARGET', rust_target_triple)
 
 # Rustdoc is required by Rust tests below.
 rustdoc = check_prog('RUSTDOC', add_rustup_path('rustdoc'), allow_missing=True)
 
 # This option is separate from --enable-tests because Rust tests are particularly
diff --git a/toolkit/library/gtest/rust/Cargo.toml b/toolkit/library/gtest/rust/Cargo.toml
--- a/toolkit/library/gtest/rust/Cargo.toml
+++ b/toolkit/library/gtest/rust/Cargo.toml
@@ -8,18 +8,16 @@ description = "Testing code for libgkrus
 [features]
 bindgen = ["gkrust-shared/bindgen"]
 servo = ["gkrust-shared/servo"]
 quantum_render = ["gkrust-shared/quantum_render"]
 cubeb-remoting = ["gkrust-shared/cubeb-remoting"]
 cubeb_pulse_rust = ["gkrust-shared/cubeb_pulse_rust"]
 gecko_debug = ["gkrust-shared/gecko_debug"]
 simd-accel = ["gkrust-shared/simd-accel"]
-oom_with_global_alloc = ["gkrust-shared/oom_with_global_alloc"]
-oom_with_hook = ["gkrust-shared/oom_with_hook"]
 moz_memory = ["gkrust-shared/moz_memory"]
 
 [dependencies]
 mp4parse-gtest = { path = "../../../../dom/media/gtest" }
 nsstring-gtest = { path = "../../../../xpcom/rust/gtest/nsstring" }
 gkrust-shared = { path = "../../rust/shared" }
 
 [lib]
diff --git a/toolkit/library/rust/Cargo.toml b/toolkit/library/rust/Cargo.toml
--- a/toolkit/library/rust/Cargo.toml
+++ b/toolkit/library/rust/Cargo.toml
@@ -8,18 +8,16 @@ description = "Rust code for libxul"
 [features]
 bindgen = ["gkrust-shared/bindgen"]
 servo = ["gkrust-shared/servo"]
 quantum_render = ["gkrust-shared/quantum_render"]
 cubeb-remoting = ["gkrust-shared/cubeb-remoting"]
 cubeb_pulse_rust = ["gkrust-shared/cubeb_pulse_rust"]
 gecko_debug = ["gkrust-shared/gecko_debug"]
 simd-accel = ["gkrust-shared/simd-accel"]
-oom_with_global_alloc = ["gkrust-shared/oom_with_global_alloc"]
-oom_with_hook = ["gkrust-shared/oom_with_hook"]
 moz_memory = ["gkrust-shared/moz_memory"]
 
 [dependencies]
 gkrust-shared = { path = "shared" }
 
 [lib]
 path = "lib.rs"
 crate-type = ["staticlib"]
diff --git a/toolkit/library/rust/gkrust-features.mozbuild b/toolkit/library/rust/gkrust-features.mozbuild
--- a/toolkit/library/rust/gkrust-features.mozbuild
+++ b/toolkit/library/rust/gkrust-features.mozbuild
@@ -25,20 +25,8 @@ if CONFIG['MOZ_RUST_SIMD']:
 
 # This feature is only supported on Linux and macOS, and this check needs to
 # match MOZ_CUBEB_REMOTING in CubebUtils.cpp.
 if CONFIG['OS_ARCH'] == 'Linux' or CONFIG['OS_ARCH'] == 'Darwin':
     gkrust_features += ['cubeb-remoting']
 
 if CONFIG['MOZ_MEMORY']:
     gkrust_features += ['moz_memory']
-
-# See details in toolkit/library/rust/shared/lib.rs
-# A string test is not the best thing, but it works well enough here.
-if CONFIG['RUSTC_VERSION'] < "1.27":
-    gkrust_features += ['oom_with_global_alloc']
-elif CONFIG['RUSTC_VERSION'] >= "1.28" and CONFIG['RUSTC_VERSION'] < "1.29":
-    gkrust_features += ['oom_with_hook']
-elif CONFIG['MOZ_AUTOMATION']:
-    # We don't want builds on automation to unwittingly stop annotating OOM
-    # crash reports from rust.
-    error('Builds on automation must use a version of rust that supports OOM '
-          'hooking')
diff --git a/toolkit/library/rust/shared/Cargo.toml b/toolkit/library/rust/shared/Cargo.toml
--- a/toolkit/library/rust/shared/Cargo.toml
+++ b/toolkit/library/rust/shared/Cargo.toml
@@ -33,18 +33,16 @@ rustc_version = "=0.2.1"
 default = []
 bindgen = ["geckoservo/bindgen"]
 servo = ["geckoservo"]
 quantum_render = ["webrender_bindings"]
 cubeb-remoting = ["cubeb-sys", "audioipc-client", "audioipc-server"]
 cubeb_pulse_rust = ["cubeb-sys", "cubeb-pulse"]
 gecko_debug = ["geckoservo/gecko_debug", "nsstring/gecko_debug"]
 simd-accel = ["encoding_c/simd-accel", "encoding_glue/simd-accel"]
-oom_with_global_alloc = []
-oom_with_hook = []
 moz_memory = ["mp4parse_capi/mp4parse_fallible"]
 
 [lib]
 path = "lib.rs"
 test = false
 doctest = false
 bench = false
 doc = false
diff --git a/toolkit/library/rust/shared/build.rs b/toolkit/library/rust/shared/build.rs
--- a/toolkit/library/rust/shared/build.rs
+++ b/toolkit/library/rust/shared/build.rs
@@ -1,8 +1,16 @@
+extern crate rustc_version;
+
+use rustc_version::{version, Version};
+
 fn main() {
+    let ver = version().unwrap();
+
+    // Version checking removed. Lets just assume the oom hooks are always available.
+    
     // This is a rather awful thing to do, but we're only doing it on
-    // versions of rustc, >= 1.24 < 1.27, that are not going to change
-    // the unstable APIs we use from under us (1.26 being a beta as of
-    // writing, and close to release).
-    #[cfg(any(feature = "oom_with_global_alloc", feature = "oom_with_hook"))]
-    println!("cargo:rustc-env=RUSTC_BOOTSTRAP=1");
+    // versions of rustc that are not going to change the unstable APIs
+    // we use from under us, all being already released or beta.
+    if ver < Version::parse("1.50.0").unwrap() {
+        println!("cargo:rustc-env=RUSTC_BOOTSTRAP=1");
+    }
 }
