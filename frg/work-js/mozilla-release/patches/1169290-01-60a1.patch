# HG changeset patch
# User Andreas Tolfsen <ato@sny.no>
# Date 1517075853 0
# Node ID e92ad5128f3c9292650a0f9f52fc28f1df1e91b5
# Parent  11abeb666c561238a3db5f2cbe96e421b4c1e831
Bug 1169290 - Improve getPrefVal readability. r=maja_zf

Instead of assigning each preference value to prefValue and breaking,
they can be returned immediately.  This improves readability.

We also have no need to expose PREF_STRING, PREF_BOOL et al. in
the global scope, so these are now scoped to the getPrefVal function.

MozReview-Commit-ID: 7LZBgkZ8r08

diff --git a/testing/marionette/components/marionette.js b/testing/marionette/components/marionette.js
--- a/testing/marionette/components/marionette.js
+++ b/testing/marionette/components/marionette.js
@@ -65,44 +65,37 @@ const ENV_ENABLED = "MOZ_MARIONETTE";
 // a different profile in order to test things like Firefox refresh.
 // The environment variable itself, if present, is interpreted as a
 // JSON structure, with the keys mapping to preference names in the
 // "marionette." branch, and the values to the values of those prefs. So
 // something like {"port": 4444} would result in the marionette.port
 // pref being set to 4444.
 const ENV_PRESERVE_PREFS = "MOZ_MARIONETTE_PREF_STATE_ACROSS_RESTARTS";
 
-const {PREF_STRING, PREF_BOOL, PREF_INT, PREF_INVALID} = Ci.nsIPrefBranch;
 
 function getPrefVal(pref) {
-  let prefType = Services.prefs.getPrefType(pref);
-  let prefValue;
-  switch (prefType) {
+  const {PREF_STRING, PREF_BOOL, PREF_INT, PREF_INVALID} = Ci.nsIPrefBranch;
+
+  let type = Services.prefs.getPrefType(pref);
+  switch (type) {
     case PREF_STRING:
-      prefValue = Services.prefs.getStringPref(pref);
-      break;
+      return Services.prefs.getStringPref(pref);
 
     case PREF_BOOL:
-      prefValue = Services.prefs.getBoolPref(pref);
-      break;
+      return Services.prefs.getBoolPref(pref);
 
     case PREF_INT:
-      prefValue = Services.prefs.getIntPref(pref);
-      break;
+      return Services.prefs.getIntPref(pref);
 
     case PREF_INVALID:
-      prefValue = undefined;
-      break;
+      return undefined;
 
     default:
-      throw new TypeError(`Unexpected preference type (${prefType}) for ` +
-                          `${pref}`);
+      throw new TypeError(`Unexpected preference type (${type}) for ${pref}`);
   }
-
-  return prefValue;
 }
 
 // Get preference value of |preferred|, falling back to |fallback|
 // if |preferred| is not user-modified and |fallback| exists.
 function getPref(preferred, fallback) {
   if (!Services.prefs.prefHasUserValue(preferred) &&
       Services.prefs.getPrefType(fallback) != Ci.nsIPrefBranch.PREF_INVALID) {
     return getPrefVal(fallback, getPrefVal(preferred));
