# HG changeset patch
# User yulia <ystartsev@mozilla.com>
# Date 1573580035 0
#      Tue Nov 12 17:33:55 2019 +0000
# Node ID 9b150dd56999e1c2b7aa47cf9c0e8d96c6c8eff3
# Parent  0dcad24b1dd23da8b32f291bdf158a0b887ce2cc
Bug 1566141 - make interpreter work with JSOP code for coalesce r=jorendorff,jandem

Differential Revision: https://phabricator.services.mozilla.com/D51638

diff --git a/js/src/debugger/Script.cpp.1566141needs1576781.later b/js/src/debugger/Script.cpp.1566141needs1576781.later
new file mode 100644
--- /dev/null
+++ b/js/src/debugger/Script.cpp.1566141needs1576781.later
@@ -0,0 +1,20 @@
+--- Script.cpp
++++ Script.cpp
+@@ -1505,16 +1505,17 @@ static bool BytecodeIsEffectful(JSOp op)
+     case JSOP_LABEL:
+     case JSOP_UNDEFINED:
+     case JSOP_IFNE:
+     case JSOP_IFEQ:
+     case JSOP_RETURN:
+     case JSOP_RETRVAL:
+     case JSOP_AND:
+     case JSOP_OR:
++    case JSOP_COALESCE:
+     case JSOP_TRY:
+     case JSOP_THROW:
+     case JSOP_GOTO:
+     case JSOP_CONDSWITCH:
+     case JSOP_TABLESWITCH:
+     case JSOP_CASE:
+     case JSOP_DEFAULT:
+     case JSOP_BITNOT:
diff --git a/js/src/frontend/BytecodeEmitter.cpp b/js/src/frontend/BytecodeEmitter.cpp
--- a/js/src/frontend/BytecodeEmitter.cpp
+++ b/js/src/frontend/BytecodeEmitter.cpp
@@ -1138,18 +1138,18 @@ BytecodeEmitter::checkSideEffects(ParseN
       case ParseNodeKind::ModAssign:
       case ParseNodeKind::PowAssign:
       case ParseNodeKind::SetThis:
         MOZ_ASSERT(pn->isArity(PN_BINARY));
         *answer = true;
         return true;
 
       case ParseNodeKind::StatementList:
-      // Strict equality operations and logical operators are well-behaved and
-      // perform no conversions.
+      // Strict equality operations and short circuit operators are well-behaved
+      // and perform no conversions.
       case ParseNodeKind::CoalesceExpr:
       case ParseNodeKind::Or:
       case ParseNodeKind::And:
       case ParseNodeKind::StrictEq:
       case ParseNodeKind::StrictNe:
       // Any subexpression of a comma expression could be effectful.
       case ParseNodeKind::Comma:
         MOZ_ASSERT(pn->pn_count > 0);
@@ -6707,17 +6707,17 @@ BytecodeEmitter::emitCallOrNew(ParseNode
 // This list must be kept in the same order in several places:
 //   - The binary operators in ParseNode.h ,
 //   - the binary operators in TokenKind.h
 //   - the precedence list in Parser.cpp
 static const JSOp ParseNodeKindToJSOp[] = {
     // JSOP_NOP is for pipeline operator which does not emit its own JSOp
     // but has highest precedence in binary operators
     JSOP_NOP,
-    JSOP_NOP,
+    JSOP_COALESCE,
     JSOP_OR,
     JSOP_AND,
     JSOP_BITOR,
     JSOP_BITXOR,
     JSOP_BITAND,
     JSOP_STRICTEQ,
     JSOP_EQ,
     JSOP_STRICTNE,
@@ -6785,85 +6785,22 @@ BytecodeEmitter::emitLeftAssociative(Par
         if (!emitTree(nextExpr))
             return false;
         if (!emit1(op))
             return false;
     } while ((nextExpr = nextExpr->pn_next));
     return true;
 }
 
-bool BytecodeEmitter::emitNullCoalesce(ParseNode* pn) {
-  MOZ_ASSERT(pn->isArity(PN_LIST));
-  MOZ_ASSERT(pn->isKind(ParseNodeKind::CoalesceExpr));
-
-  /*
-   * CoalesceExpr converts the operand on the stack to boolean depending on an
-   * equality check for undefined and null. If true, it leaves the original
-   * value on the stack and jumps; otherwise it falls into the next bytecode,
-   * which pops the left operand and then evaluates the right operand.
-   * The jump goes around the right operand evaluation.
-   */
-
-  TDZCheckCache tdzCache(this);
-
-  JumpList jump;
-  /* Left-associative operator chain: avoid too much recursion. */
-  for (ParseNode* expr = pn->pn_head;; expr = expr->pn_next) {
-    if (!emitTree(expr)) {
-      //            [stack] LHS
-      return false;
-    }
-
-    // if there are no nodes after this, break so that we don't emit
-    // unnecessary bytecode instructions
-    if (!expr->pn_next) {
-      break;
-    }
-
-    if (!emitPushNotUndefinedOrNull()) {
-      //            [stack] LHS NOT-UNDEF-OR-NULL
-      return false;
-    }
-
-    // We are using JSOP_IFEQ, so we need to invert the boolean
-    // pushed onto the stack by emitPushNotUndefinedOrNull.
-    // This is to address a constraint in Ion Monkey which throws
-    // if JSOP_IFNE is encountered.
-    if (!emit1(JSOP_NOT)) {
-      //              [stack] LHS UNDEF-OR-NULL
-      return false;
-    }
-
-    // Emit an annotated branch-if-false around the then part.
-    if(!this->newSrcNote(SRC_IF)) {
-      return false;
-    }
-
-    if (!emitJump(JSOP_IFEQ, &jump)) {
-      //              [stack] LHS
-      return false;
-    }
-
-    if (!emit1(JSOP_POP)) {
-      return false;
-    }
-  }
-
-  if (!emitJumpTargetAndPatch(jump)) {
-    return false;
-  }
-
-  return true;
-}
-
-bool
-BytecodeEmitter::emitLogical(ParseNode* pn)
+bool
+BytecodeEmitter::emitShortCircuit(ParseNode* pn)
 {
     MOZ_ASSERT(pn->isArity(PN_LIST));
     MOZ_ASSERT(pn->isKind(ParseNodeKind::Or) ||
+               pn->isKind(ParseNodeKind::CoalesceExpr) ||
                pn->isKind(ParseNodeKind::And));
 
     /*
      * JSOP_OR converts the operand on the stack to boolean, leaves the original
      * value on the stack and jumps if true; otherwise it falls into the next
      * bytecode, which pops the left operand and then evaluates the right operand.
      * The jump goes around the right operand evaluation.
      *
@@ -6874,17 +6811,31 @@ BytecodeEmitter::emitLogical(ParseNode* 
     TDZCheckCache tdzCache(this);
 
     /* Left-associative operator chain: avoid too much recursion. */
     ParseNode* pn2 = pn->pn_head;
 
     if (!emitTree(pn2))
         return false;
 
-    JSOp op = pn->isKind(ParseNodeKind::Or) ? JSOP_OR : JSOP_AND;
+    JSOp op;
+    switch (pn->getKind()) {
+      case ParseNodeKind::Or:
+        op = JSOP_OR;
+        break;
+      case ParseNodeKind::CoalesceExpr:
+        op = JSOP_COALESCE;
+        break;
+      case ParseNodeKind::And:
+        op = JSOP_AND;
+        break;
+      default:
+        MOZ_CRASH("Unexpected ParseNodeKind");
+    }
+
     JumpList jump;
     if (!emitJump(op, &jump))
         return false;
     if (!emit1(JSOP_POP))
         return false;
 
     /* Emit nodes between the head and the tail. */
     while ((pn2 = pn2->pn_next)->pn_next) {
@@ -8144,25 +8095,20 @@ BytecodeEmitter::emitTree(ParseNode* pn,
             return false;
         break;
 
       case ParseNodeKind::Conditional:
         if (!emitConditionalExpression(pn->as<ConditionalExpression>(), valueUsage))
             return false;
         break;
 
+      case ParseNodeKind::Or:
       case ParseNodeKind::CoalesceExpr:
-        if (!emitNullCoalesce(pn)) {
-          return false;
-        }
-        break;
-
-      case ParseNodeKind::Or:
       case ParseNodeKind::And:
-        if (!emitLogical(pn))
+        if (!emitShortCircuit(pn))
             return false;
         break;
 
       case ParseNodeKind::Add:
       case ParseNodeKind::Sub:
       case ParseNodeKind::BitOr:
       case ParseNodeKind::BitXor:
       case ParseNodeKind::BitAnd:
diff --git a/js/src/frontend/BytecodeEmitter.h b/js/src/frontend/BytecodeEmitter.h
--- a/js/src/frontend/BytecodeEmitter.h
+++ b/js/src/frontend/BytecodeEmitter.h
@@ -790,18 +790,17 @@ struct MOZ_STACK_CLASS BytecodeEmitter
     MOZ_MUST_USE bool emitDeleteExpression(ParseNode* pn);
 
     // |op| must be JSOP_TYPEOF or JSOP_TYPEOFEXPR.
     MOZ_MUST_USE bool emitTypeof(ParseNode* node, JSOp op);
 
     MOZ_MUST_USE bool emitUnary(ParseNode* pn);
     MOZ_MUST_USE bool emitRightAssociative(ParseNode* pn);
     MOZ_MUST_USE bool emitLeftAssociative(ParseNode* pn);
-    MOZ_MUST_USE bool emitNullCoalesce(ParseNode* pn);
-    MOZ_MUST_USE bool emitLogical(ParseNode* pn);
+    MOZ_MUST_USE bool emitShortCircuit(ParseNode* pn);
     MOZ_MUST_USE bool emitSequenceExpr(ParseNode* pn,
                                        ValueUsage valueUsage = ValueUsage::WantValue);
 
     MOZ_NEVER_INLINE MOZ_MUST_USE bool emitIncOrDec(ParseNode* pn);
 
     MOZ_MUST_USE bool emitConditionalExpression(ConditionalExpression& conditional,
                                                 ValueUsage valueUsage = ValueUsage::WantValue);
 
diff --git a/js/src/jit/BaselineCompiler.h b/js/src/jit/BaselineCompiler.h
--- a/js/src/jit/BaselineCompiler.h
+++ b/js/src/jit/BaselineCompiler.h
@@ -350,16 +350,17 @@ class BaselineCompiler : public Baseline
     // Handles JSOP_LT, JSOP_GT, and friends
     MOZ_MUST_USE bool emitCompare();
 
     MOZ_MUST_USE bool emitReturn();
 
     MOZ_MUST_USE bool emitToBoolean();
     MOZ_MUST_USE bool emitTest(bool branchIfTrue);
     MOZ_MUST_USE bool emitAndOr(bool branchIfTrue);
+    MOZ_MUST_USE bool emitCoalesce();
     MOZ_MUST_USE bool emitCall();
     MOZ_MUST_USE bool emitSpreadCall();
 
     MOZ_MUST_USE bool emitInitPropGetterSetter();
     MOZ_MUST_USE bool emitInitElemGetterSetter();
 
     MOZ_MUST_USE bool emitFormalArgAccess(uint32_t arg, bool get);
 
diff --git a/js/src/tests/non262/reflect-parse/basicBuilder.js b/js/src/tests/non262/reflect-parse/basicBuilder.js
--- a/js/src/tests/non262/reflect-parse/basicBuilder.js
+++ b/js/src/tests/non262/reflect-parse/basicBuilder.js
@@ -1,10 +1,10 @@
 // |reftest| skip-if(!xulRuntime.shell)
-function test() { 
+function test() {
 // Builder tests
 
 Pattern("program").match(Reflect.parse("42", {builder:{program:() => "program"}}));
 
 assertGlobalStmt("throw 42", 1, { throwStatement: () => 1 });
 assertGlobalStmt("for (;;);", 2, { forStatement: () => 2 });
 assertGlobalStmt("for (x in y);", 3, { forInStatement: () => 3 });
 assertGlobalStmt("{ }", 4, { blockStatement: () => 4 });
diff --git a/js/src/vm/Interpreter.cpp b/js/src/vm/Interpreter.cpp
--- a/js/src/vm/Interpreter.cpp
+++ b/js/src/vm/Interpreter.cpp
@@ -2298,16 +2298,25 @@ END_CASE(JSOP_IFNE)
 CASE(JSOP_OR)
 {
     bool cond = ToBoolean(REGS.stackHandleAt(-1));
     if (cond)
         ADVANCE_AND_DISPATCH(GET_JUMP_OFFSET(REGS.pc));
 }
 END_CASE(JSOP_OR)
 
+CASE(JSOP_COALESCE) {
+  MutableHandleValue res = REGS.stackHandleAt(-1);
+  bool cond = !res.isNullOrUndefined();
+  if (cond) {
+    ADVANCE_AND_DISPATCH(GET_JUMP_OFFSET(REGS.pc));
+  }
+}
+END_CASE(JSOP_COALESCE)
+
 CASE(JSOP_AND)
 {
     bool cond = ToBoolean(REGS.stackHandleAt(-1));
     if (!cond)
         ADVANCE_AND_DISPATCH(GET_JUMP_OFFSET(REGS.pc));
 }
 END_CASE(JSOP_AND)
 
diff --git a/js/src/vm/Opcodes.h b/js/src/vm/Opcodes.h
--- a/js/src/vm/Opcodes.h
+++ b/js/src/vm/Opcodes.h
@@ -2360,24 +2360,33 @@ 1234567890123456789012345678901234567890
     /*
      * Push "import.meta"
      *
      *   Category: Variables and Scopes
      *   Type: Modules
      *   Operands:
      *   Stack: => import.meta
      */ \
-    macro(JSOP_IMPORTMETA,    232, "importmeta", NULL,      1,  0,  1,  JOF_BYTE)
+    macro(JSOP_IMPORTMETA,    232, "importmeta", NULL,      1,  0,  1,  JOF_BYTE) \
+    /*
+     * If the value on top of the stack is not null or undefined, jumps to a 32-bit offset from the
+     * current bytecode.
+     *
+     *   Category: Statements
+     *   Type: Jumps
+     *   Operands: int32_t offset
+     *   Stack: cond => cond
+     */ \
+    macro(JSOP_COALESCE, 233, "coalesce", NULL, 5, 1, 1, JOF_JUMP|JOF_DETECTING)
 
 /*
  * In certain circumstances it may be useful to "pad out" the opcode space to
  * a power of two.  Use this macro to do so.
  */
 #define FOR_EACH_TRAILING_UNUSED_OPCODE(macro) \
-    macro(233) \
     macro(234) \
     macro(235) \
     macro(236) \
     macro(237) \
     macro(238) \
     macro(239) \
     macro(240) \
     macro(241) \
