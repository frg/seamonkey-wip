# HG changeset patch
# User Julian Descottes <jdescottes@mozilla.com>
# Date 1515522501 -3600
# Node ID 252d8b06f90f86e9a9c38f986260009d79586ca6
# Parent  2b58fcdfc6d8bf23a598b870c9272a498305cd63
Bug 1349483 - extract tooltip hidden callback from SwatchColorPickerTooltip;r=pbro

This allows subclasses to override the implementation,a nd avoids leaking ColorPicker
specific implementation details to the base class.

MozReview-Commit-ID: 6KHaFPd5xHt

diff --git a/devtools/client/shared/widgets/tooltip/SwatchBasedEditorTooltip.js b/devtools/client/shared/widgets/tooltip/SwatchBasedEditorTooltip.js
--- a/devtools/client/shared/widgets/tooltip/SwatchBasedEditorTooltip.js
+++ b/devtools/client/shared/widgets/tooltip/SwatchBasedEditorTooltip.js
@@ -104,38 +104,42 @@ class SwatchBasedEditorTooltip {
    */
   show() {
     let tooltipAnchor = this.useInline ?
       this.activeSwatch.closest(`.${INLINE_TOOLTIP_CLASS}`) :
       this.activeSwatch;
 
     if (tooltipAnchor) {
       let onShown = this.tooltip.once("shown");
+
       this.tooltip.show(tooltipAnchor, "topcenter bottomleft");
-
-      // When the tooltip is closed by clicking outside the panel we want to
-      // commit any changes.
-      this.tooltip.once("hidden", () => {
-        if (!this._reverted && !this.eyedropperOpen) {
-          this.commit();
-        }
-        this._reverted = false;
-
-        // Once the tooltip is hidden we need to clean up any remaining objects.
-        if (!this.eyedropperOpen) {
-          this.activeSwatch = null;
-        }
-      });
+      this.tooltip.once("hidden", () => this.onTooltipHidden());
 
       return onShown;
     }
 
     return Promise.resolve();
   }
 
+  /**
+   * Can be overridden by subclasses if implementation specific behavior is needed on
+   * tooltip hidden.
+   */
+  onTooltipHidden() {
+    // When the tooltip is closed by clicking outside the panel we want to commit any
+    // changes.
+    if (!this._reverted) {
+      this.commit();
+    }
+    this._reverted = false;
+
+    // Once the tooltip is hidden we need to clean up any remaining objects.
+    this.activeSwatch = null;
+  }
+
   hide() {
     this.tooltip.hide();
   }
 
   /**
    * Add a new swatch DOM element to the list of swatch elements this editor
    * tooltip knows about. That means from now on, clicking on that swatch will
    * toggle the editor.
diff --git a/devtools/client/shared/widgets/tooltip/SwatchColorPickerTooltip.js b/devtools/client/shared/widgets/tooltip/SwatchColorPickerTooltip.js
--- a/devtools/client/shared/widgets/tooltip/SwatchColorPickerTooltip.js
+++ b/devtools/client/shared/widgets/tooltip/SwatchColorPickerTooltip.js
@@ -152,16 +152,29 @@ class SwatchColorPickerTooltip extends S
       this.preview(color);
 
       if (this.eyedropperOpen) {
         this.commit();
       }
     }
   }
 
+  /**
+   * Override the implementation from SwatchBasedEditorTooltip.
+   */
+  onTooltipHidden() {
+    // If the tooltip is hidden while the eyedropper is being used, we should not commit
+    // the changes.
+    if (this.eyedropperOpen) {
+      return;
+    }
+
+    super.onTooltipHidden();
+  }
+
   _openEyeDropper() {
     let {inspector, toolbox, telemetry} = this.inspector;
     telemetry.toolOpened("pickereyedropper");
 
     // cancelling picker(if it is already selected) on opening eye-dropper
     toolbox.highlighterUtils.cancelPicker();
 
     inspector.pickColorFromPage(toolbox, {copyOnSelect: false}).then(() => {
