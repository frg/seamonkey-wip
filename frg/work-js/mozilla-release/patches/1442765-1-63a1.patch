# HG changeset patch
# User Eric Rahm <erahm@mozilla.com>
# Date 1521593536 25200
# Node ID b60b9d07842ed57911c896477b37defd9c00fb8b
# Parent  b9193e72be193ca5d706a16d8c7ab314b458cf9f
Bug 1442765 - Part 1: Add intptr_t hashkey type. r=froydnj

This adds a hashkey that operates on a uintptr_t.

diff --git a/xpcom/ds/nsHashKeys.h b/xpcom/ds/nsHashKeys.h
--- a/xpcom/ds/nsHashKeys.h
+++ b/xpcom/ds/nsHashKeys.h
@@ -15,16 +15,17 @@
 #include "PLDHashTable.h"
 #include <new>
 
 #include "nsString.h"
 #include "nsCRTGlue.h"
 #include "nsUnicharUtils.h"
 #include "nsPointerHashKeys.h"
 
+#include <stdint.h>
 #include <stdlib.h>
 #include <string.h>
 
 #include "mozilla/HashFunctions.h"
 #include "mozilla/Move.h"
 
 namespace mozilla {
 
@@ -49,16 +50,17 @@ HashString(const nsACString& aStr)
  * classes follows the nsTHashtable::EntryType specification
  *
  * Lightweight keytypes provided here:
  * nsStringHashKey
  * nsCStringHashKey
  * nsUint32HashKey
  * nsUint64HashKey
  * nsFloatHashKey
+ * IntPtrHashKey
  * nsPtrHashKey
  * nsClearingPtrHashKey
  * nsVoidPtrHashKey
  * nsClearingVoidPtrHashKey
  * nsISupportsHashKey
  * nsIDHashKey
  * nsDepCharHashKey
  * nsCharPtrHashKey
@@ -280,16 +282,45 @@ public:
   }
   enum { ALLOW_MEMMOVE = true };
 
 private:
   const float mValue;
 };
 
 /**
+ * hashkey wrapper using intptr_t KeyType
+ *
+ * @see nsTHashtable::EntryType for specification
+ */
+class IntPtrHashKey : public PLDHashEntryHdr
+{
+public:
+  typedef const intptr_t& KeyType;
+  typedef const intptr_t* KeyTypePointer;
+
+  explicit IntPtrHashKey(KeyTypePointer aKey) : mValue(*aKey) {}
+  IntPtrHashKey(const IntPtrHashKey& aToCopy) : mValue(aToCopy.mValue) {}
+  ~IntPtrHashKey() {}
+
+  KeyType GetKey() const { return mValue; }
+  bool KeyEquals(KeyTypePointer aKey) const { return *aKey == mValue; }
+
+  static KeyTypePointer KeyToPointer(KeyType aKey) { return &aKey; }
+  static PLDHashNumber HashKey(KeyTypePointer aKey)
+  {
+    return mozilla::HashGeneric(*aKey);
+  }
+  enum { ALLOW_MEMMOVE = true };
+
+private:
+  const intptr_t mValue;
+};
+
+/**
  * hashkey wrapper using nsISupports* KeyType
  *
  * @see nsTHashtable::EntryType for specification
  */
 class nsISupportsHashKey : public PLDHashEntryHdr
 {
 public:
   typedef nsISupports* KeyType;

