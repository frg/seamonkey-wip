# HG changeset patch
# User Ted Mielczarek <ted@mielczarek.org>
# Date 1547560567 0
#      Tue Jan 15 13:56:07 2019 +0000
# Node ID 33247481200e0d9b4ddddaf9ce99e4a86b50c854
# Parent  471d9fab61b34671f974dfa349e7972c200faa2b
bug 1459991 - embed Rust's natvis files into xul.pdb on Windows. r=froydnj

Rust ships with some natvis files that enable nicer display of Rust standard
library types in Microsoft's debuggers. rustc will add the right linker
options to include them when it invokes the linker but since we don't link
libxul with rustc we need to explicitly pass them to the linker ourselves.

This change locates all natvis files in the Rust sysroot and adds them
to the libxul link line.

Differential Revision: https://phabricator.services.mozilla.com/D16544

diff --git a/build/moz.configure/rust.configure b/build/moz.configure/rust.configure
--- a/build/moz.configure/rust.configure
+++ b/build/moz.configure/rust.configure
@@ -304,8 +304,24 @@ def win64_cargo_linker_config(linker, en
         [target.x86_64-pc-windows-msvc]
         linker = "{objdir}/build/win64/cargo-linker.bat"
         '''.format(objdir=env.topobjdir))
     # We want an empty string here so we don't leave the @ variable in the config file.
     return ''
 
 
 set_config('WIN64_CARGO_LINKER_CONFIG', win64_cargo_linker_config)
+
+
+@depends(target, c_compiler, rustc)
+@imports('os')
+def rustc_natvis_ldflags(target, compiler_info, rustc):
+    if target.kernel == 'WINNT' and compiler_info.type in ('msvc', 'clang-cl'):
+        sysroot = check_cmd_output(rustc, '--print', 'sysroot').strip()
+        etc = os.path.join(sysroot, 'lib/rustlib/etc')
+        ldflags = []
+        for f in os.listdir(etc):
+            if f.endswith('.natvis'):
+                ldflags.append('-NATVIS:' + normsep(os.path.join(etc, f)))
+        return ldflags
+
+
+set_config('RUSTC_NATVIS_LDFLAGS', rustc_natvis_ldflags)
diff --git a/toolkit/library/moz.build b/toolkit/library/moz.build
--- a/toolkit/library/moz.build
+++ b/toolkit/library/moz.build
@@ -77,16 +77,18 @@ def Libxul(name, output_category=None):
     # This option should go away in bug 1290972, but we need to wait until
     # Rust 1.12 has been released.
     # We're also linking against libresolv to solve bug 1367932.
     if CONFIG['OS_ARCH'] == 'Darwin':
         LDFLAGS += ['-lresolv']
 
     if CONFIG['MOZ_DEBUG_SYMBOLS'] and CONFIG['CC_TYPE'] in ('msvc', 'clang-cl'):
         LDFLAGS += ['-NATVIS:%s/toolkit/library/gecko.natvis' % TOPSRCDIR]
+    if CONFIG['RUSTC_NATVIS_LDFLAGS']:
+        LDFLAGS += CONFIG['RUSTC_NATVIS_LDFLAGS']
 
 Libxul('xul')
 
 FORCE_STATIC_LIB = True
 
 STATIC_LIBRARY_NAME = 'xul_s'
 
 if CONFIG['OS_ARCH'] == 'WINNT':
