# HG changeset patch
# User Oriol Brufau <oriol-bugzilla@hotmail.com>
# Date 1511882901 -3600
# Node ID 8b81d2b60acf8a95f0773bf7823ec6e0e9da82a3
# Parent  98f0f498fe04f7fb2cf125f55b36cd84846eeee8
Bug 1417039 - Do not defer loading JSON Viewer. r=Honza

MozReview-Commit-ID: BtnH41N1w8P

diff --git a/devtools/client/jsonview/components/LiveText.js b/devtools/client/jsonview/components/LiveText.js
new file mode 100644
--- /dev/null
+++ b/devtools/client/jsonview/components/LiveText.js
@@ -0,0 +1,45 @@
+/* -*- indent-tabs-mode: nil; js-indent-level: 2 -*- */
+/* vim: set ft=javascript ts=2 et sw=2 tw=80: */
+/* This Source Code Form is subject to the terms of the Mozilla Public
+ * License, v. 2.0. If a copy of the MPL was not distributed with this
+ * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
+
+"use strict";
+
+define(function (require, exports, module) {
+  const { Component } = require("devtools/client/shared/vendor/react");
+  const PropTypes = require("devtools/client/shared/vendor/react-prop-types");
+  const { findDOMNode } = require("devtools/client/shared/vendor/react-dom");
+  const { pre } = require("devtools/client/shared/vendor/react-dom-factories");
+
+  /**
+   * This object represents a live DOM text node in a <pre>.
+   */
+  class LiveText extends Component {
+    static get propTypes() {
+      return {
+        data: PropTypes.instanceOf(Text),
+      };
+    }
+
+    componentDidMount() {
+      this.componentDidUpdate();
+    }
+
+    componentDidUpdate() {
+      let el = findDOMNode(this);
+      if (el.firstChild === this.props.data) {
+        return;
+      }
+      el.textContent = "";
+      el.append(this.props.data);
+    }
+
+    render() {
+      return pre({className: "data"});
+    }
+  }
+
+  // Exports from this module
+  exports.LiveText = LiveText;
+});
diff --git a/devtools/client/jsonview/components/MainTabbedArea.js b/devtools/client/jsonview/components/MainTabbedArea.js
--- a/devtools/client/jsonview/components/MainTabbedArea.js
+++ b/devtools/client/jsonview/components/MainTabbedArea.js
@@ -17,17 +17,17 @@ define(function (require, exports, modul
 
   /**
    * This object represents the root application template
    * responsible for rendering the basic tab layout.
    */
   class MainTabbedArea extends Component {
     static get propTypes() {
       return {
-        jsonText: PropTypes.string,
+        jsonText: PropTypes.instanceOf(Text),
         tabActive: PropTypes.number,
         actions: PropTypes.object,
         headers: PropTypes.object,
         searchFilter: PropTypes.string,
         json: PropTypes.oneOfType([
           PropTypes.string,
           PropTypes.object,
           PropTypes.array,
@@ -37,18 +37,18 @@ define(function (require, exports, modul
         expandedNodes: PropTypes.instanceOf(Set),
       };
     }
 
     constructor(props) {
       super(props);
 
       this.state = {
-        json: {},
-        headers: {},
+        json: props.json,
+        expandedNodes: props.expandedNodes,
         jsonText: props.jsonText,
         tabActive: props.tabActive
       };
 
       this.onTabChanged = this.onTabChanged.bind(this);
     }
 
     onTabChanged(index) {
@@ -59,27 +59,28 @@ define(function (require, exports, modul
       return (
         Tabs({
           tabActive: this.state.tabActive,
           onAfterChange: this.onTabChanged},
           TabPanel({
             className: "json",
             title: JSONView.Locale.$STR("jsonViewer.tab.JSON")},
             JsonPanel({
-              data: this.props.json,
+              data: this.state.json,
               expandedNodes: this.props.expandedNodes,
               actions: this.props.actions,
               searchFilter: this.state.searchFilter
             })
           ),
           TabPanel({
             className: "rawdata",
             title: JSONView.Locale.$STR("jsonViewer.tab.RawData")},
             TextPanel({
-              isValidJson: !(this.props.json instanceof Error),
+              isValidJson: !(this.state.json instanceof Error) &&
+                           document.readyState != "loading",
               data: this.state.jsonText,
               actions: this.props.actions
             })
           ),
           TabPanel({
             className: "headers",
             title: JSONView.Locale.$STR("jsonViewer.tab.Headers")},
             HeadersPanel({
diff --git a/devtools/client/jsonview/components/TextPanel.js b/devtools/client/jsonview/components/TextPanel.js
--- a/devtools/client/jsonview/components/TextPanel.js
+++ b/devtools/client/jsonview/components/TextPanel.js
@@ -7,48 +7,46 @@
 "use strict";
 
 define(function (require, exports, module) {
   const { Component } = require("devtools/client/shared/vendor/react");
   const PropTypes = require("devtools/client/shared/vendor/react-prop-types");
   const dom = require("devtools/client/shared/vendor/react-dom-factories");
   const { createFactories } = require("devtools/client/shared/react-utils");
   const { TextToolbar } = createFactories(require("./TextToolbar"));
-
-  const { div, pre } = dom;
+  const { LiveText } = createFactories(require("./LiveText"));
+  const { div } = dom;
 
   /**
    * This template represents the 'Raw Data' panel displaying
    * JSON as a text received from the server.
    */
   class TextPanel extends Component {
     static get propTypes() {
       return {
         isValidJson: PropTypes.bool,
         actions: PropTypes.object,
-        data: PropTypes.string
+        data: PropTypes.instanceOf(Text),
       };
     }
 
     constructor(props) {
       super(props);
       this.state = {};
     }
 
     render() {
       return (
         div({className: "textPanelBox tab-panel-inner"},
           TextToolbar({
             actions: this.props.actions,
             isValidJson: this.props.isValidJson
           }),
           div({className: "panelContent"},
-            pre({className: "data"},
-              this.props.data
-            )
+            LiveText({data: this.props.data})
           )
         )
       );
     }
   }
 
   // Exports from this module
   exports.TextPanel = TextPanel;
diff --git a/devtools/client/jsonview/components/moz.build b/devtools/client/jsonview/components/moz.build
--- a/devtools/client/jsonview/components/moz.build
+++ b/devtools/client/jsonview/components/moz.build
@@ -9,13 +9,14 @@ DIRS += [
 ]
 
 DevToolsModules(
     'Headers.js',
     'HeadersPanel.js',
     'HeadersToolbar.js',
     'JsonPanel.js',
     'JsonToolbar.js',
+    'LiveText.js',
     'MainTabbedArea.js',
     'SearchBox.js',
     'TextPanel.js',
     'TextToolbar.js'
 )
diff --git a/devtools/client/jsonview/converter-child.js b/devtools/client/jsonview/converter-child.js
--- a/devtools/client/jsonview/converter-child.js
+++ b/devtools/client/jsonview/converter-child.js
@@ -163,16 +163,18 @@ function exportData(win, request) {
   let data = Cu.createObjectIn(win, {
     defineAs: "JSONView"
   });
 
   data.debug = debug;
 
   data.json = new win.Text();
 
+  data.readyState = "uninitialized";
+
   let Locale = {
     $STR: key => {
       try {
         return jsonViewStrings.GetStringFromName(key);
       } catch (err) {
         console.error(err);
         return undefined;
       }
@@ -236,27 +238,26 @@ function initialHTML(doc) {
       "dir": Services.locale.isAppLocaleRTL ? "rtl" : "ltr"
     }, [
       element("head", {}, [
         element("link", {
           rel: "stylesheet",
           type: "text/css",
           href: baseURI + "css/main.css",
         }),
-        element("script", {
-          src: baseURI + "lib/require.js",
-          "data-main": baseURI + "viewer-config.js",
-          defer: true,
-        })
       ]),
       element("body", {}, [
         element("div", {"id": "content"}, [
           element("div", {"id": "json"})
-        ])
-      ])
+        ]),
+        element("script", {
+          src: baseURI + "lib/require.js",
+          "data-main": baseURI + "viewer-config.js",
+        }),
+      ]),
     ]).outerHTML;
 }
 
 // We insert the received data into a text node, which should be appended into
 // the #json element so that the JSON is still displayed even if JS is disabled.
 // However, the HTML parser is not synchronous, so this function uses a mutation
 // observer to detect the creation of the element. Then the text node is appended.
 function insertJsonData(win, json) {
diff --git a/devtools/client/jsonview/json-viewer.js b/devtools/client/jsonview/json-viewer.js
--- a/devtools/client/jsonview/json-viewer.js
+++ b/devtools/client/jsonview/json-viewer.js
@@ -14,51 +14,36 @@ define(function (require, exports, modul
 
   const AUTO_EXPAND_MAX_SIZE = 100 * 1024;
   const AUTO_EXPAND_MAX_LEVEL = 7;
 
   let prettyURL;
 
   // Application state object.
   let input = {
-    jsonText: JSONView.json.textContent,
+    jsonText: JSONView.json,
     jsonPretty: null,
     headers: JSONView.headers,
     tabActive: 0,
     prettified: false
   };
 
-  try {
-    input.json = JSON.parse(input.jsonText);
-  } catch (err) {
-    input.json = err;
-  }
-
-  // Expand the document by default if its size isn't bigger than 100KB.
-  if (!(input.json instanceof Error) && input.jsonText.length <= AUTO_EXPAND_MAX_SIZE) {
-    input.expandedNodes = TreeViewClass.getExpandedNodes(
-      input.json,
-      {maxLevel: AUTO_EXPAND_MAX_LEVEL}
-    );
-  } else {
-    input.expandedNodes = new Set();
-  }
-
   /**
    * Application actions/commands. This list implements all commands
    * available for the JSON viewer.
    */
   input.actions = {
     onCopyJson: function () {
-      copyString(input.prettified ? input.jsonPretty : input.jsonText);
+      let text = input.prettified ? input.jsonPretty : input.jsonText;
+      copyString(text.textContent);
     },
 
     onSaveJson: function () {
       if (input.prettified && !prettyURL) {
-        prettyURL = URL.createObjectURL(new window.Blob([input.jsonPretty]));
+        prettyURL = URL.createObjectURL(new window.Blob([input.jsonPretty.textContent]));
       }
       dispatchEvent("save", input.prettified ? prettyURL : null);
     },
 
     onCopyHeaders: function () {
       let value = "";
       let isWinNT = document.documentElement.getAttribute("platform") === "win";
       let eol = isWinNT ? "\r\n" : "\n";
@@ -88,17 +73,17 @@ define(function (require, exports, modul
       if (input.json instanceof Error) {
         // Cannot prettify invalid JSON
         return;
       }
       if (input.prettified) {
         theApp.setState({jsonText: input.jsonText});
       } else {
         if (!input.jsonPretty) {
-          input.jsonPretty = JSON.stringify(input.json, null, "  ");
+          input.jsonPretty = new Text(JSON.stringify(input.json, null, "  "));
         }
         theApp.setState({jsonText: input.jsonPretty});
       }
 
       input.prettified = !input.prettified;
     },
   };
 
@@ -134,16 +119,57 @@ define(function (require, exports, modul
     window.dispatchEvent(contentMessageEvent);
   }
 
   /**
    * Render the main application component. It's the main tab bar displayed
    * at the top of the window. This component also represents ReacJS root.
    */
   let content = document.getElementById("content");
+  let promise = (async function parseJSON() {
+    if (document.readyState == "loading") {
+      // If the JSON has not been loaded yet, render the Raw Data tab first.
+      input.json = {};
+      input.expandedNodes = new Set();
+      input.tabActive = 1;
+      return new Promise(resolve => {
+        document.addEventListener("DOMContentLoaded", resolve, {once: true});
+      }).then(parseJSON).then(() => {
+        // Now update the state and switch to the JSON tab.
+        theApp.setState({
+          tabActive: 0,
+          json: input.json,
+          expandedNodes: input.expandedNodes,
+        });
+      });
+    }
+
+    // If the JSON has been loaded, parse it immediately before loading the app.
+    let jsonString = input.jsonText.textContent;
+    try {
+      input.json = JSON.parse(jsonString);
+    } catch (err) {
+      input.json = err;
+    }
+
+    // Expand the document by default if its size isn't bigger than 100KB.
+    if (!(input.json instanceof Error) && jsonString.length <= AUTO_EXPAND_MAX_SIZE) {
+      input.expandedNodes = TreeViewClass.getExpandedNodes(
+        input.json,
+        {maxLevel: AUTO_EXPAND_MAX_LEVEL}
+      );
+    }
+    return undefined;
+  })();
+
   let theApp = render(MainTabbedArea(input), content);
 
-  // Send notification event to the window. Can be useful for
+  // Send readyState change notification event to the window. Can be useful for
   // tests as well as extensions.
-  let event = new CustomEvent("JSONViewInitialized", {});
-  JSONView.initialized = true;
-  window.dispatchEvent(event);
+  JSONView.readyState = "interactive";
+  window.dispatchEvent(new CustomEvent("AppReadyStateChange"));
+
+  promise.then(() => {
+    // Another readyState change notification event.
+    JSONView.readyState = "complete";
+    window.dispatchEvent(new CustomEvent("AppReadyStateChange"));
+  });
 });
diff --git a/devtools/client/jsonview/test/browser.ini b/devtools/client/jsonview/test/browser.ini
--- a/devtools/client/jsonview/test/browser.ini
+++ b/devtools/client/jsonview/test/browser.ini
@@ -17,38 +17,41 @@ support-files =
   simple_json.json
   simple_json.json^headers^
   valid_json.json
   valid_json.json^headers^
   !/devtools/client/commandline/test/head.js
   !/devtools/client/framework/test/head.js
   !/devtools/client/framework/test/shared-head.js
 
+[browser_json_refresh.js]
 [browser_jsonview_bug_1380828.js]
-[browser_jsonview_ignore_charset.js]
+[browser_jsonview_chunked_json.js]
+support-files =
+  chunked_json.sjs
 [browser_jsonview_content_type.js]
 [browser_jsonview_copy_headers.js]
 subsuite = clipboard
 skip-if = (os == 'linux' && bits == 32 && debug) # bug 1328915, disable linux32 debug devtools for timeouts
 [browser_jsonview_copy_json.js]
 subsuite = clipboard
 skip-if = (os == 'linux' && bits == 32 && debug) # bug 1328915, disable linux32 debug devtools for timeouts
 [browser_jsonview_copy_rawdata.js]
 subsuite = clipboard
 skip-if = (os == 'linux' && bits == 32 && debug) # bug 1328915, disable linux32 debug devtools for timeouts
 [browser_jsonview_csp_json.js]
 [browser_jsonview_empty_object.js]
 [browser_jsonview_encoding.js]
 [browser_jsonview_filter.js]
+[browser_jsonview_ignore_charset.js]
 [browser_jsonview_invalid_json.js]
 [browser_jsonview_manifest.js]
 [browser_jsonview_nojs.js]
 [browser_jsonview_nul.js]
 [browser_jsonview_object-type.js]
 [browser_jsonview_row_selection.js]
 [browser_jsonview_save_json.js]
 support-files =
   !/toolkit/content/tests/browser/common/mockTransfer.js
+[browser_jsonview_serviceworker.js]
+[browser_jsonview_slash.js]
 [browser_jsonview_theme.js]
-[browser_jsonview_slash.js]
 [browser_jsonview_valid_json.js]
-[browser_json_refresh.js]
-[browser_jsonview_serviceworker.js]
diff --git a/devtools/client/jsonview/test/browser_jsonview_chunked_json.js b/devtools/client/jsonview/test/browser_jsonview_chunked_json.js
new file mode 100644
--- /dev/null
+++ b/devtools/client/jsonview/test/browser_jsonview_chunked_json.js
@@ -0,0 +1,84 @@
+/* -*- indent-tabs-mode: nil; js-indent-level: 2 -*- */
+/* vim: set ts=2 et sw=2 tw=80: */
+/* Any copyright is dedicated to the Public Domain.
+ * http://creativecommons.org/publicdomain/zero/1.0/ */
+
+"use strict";
+
+const TEST_JSON_URL = URL_ROOT + "chunked_json.sjs";
+
+add_task(async function () {
+  info("Test chunked JSON started");
+
+  await addJsonViewTab(TEST_JSON_URL, {
+    appReadyState: "interactive",
+    docReadyState: "loading",
+  });
+
+  is(await getElementCount(".rawdata.is-active"), 1,
+    "The Raw Data tab is selected.");
+
+  // Write some text and check that it is displayed.
+  await write("[");
+  await checkText();
+
+  // Repeat just in case.
+  await write("1,");
+  await checkText();
+
+  is(await getElementCount("button.prettyprint"), 0,
+    "There is no pretty print button during load");
+
+  await selectJsonViewContentTab("json");
+  is(await getElementText(".jsonPanelBox > .panelContent"), "", "There is no JSON tree");
+
+  await selectJsonViewContentTab("headers");
+  ok(await getElementText(".headersPanelBox .netInfoHeadersTable"),
+    "The headers table has been filled.");
+
+  // Write some text without being in Raw Data, then switch tab and check.
+  await write("2");
+  await selectJsonViewContentTab("rawdata");
+  await checkText();
+
+  // Another text check.
+  await write("]");
+  await checkText();
+
+  // Close the connection.
+  let appReady = waitForContentMessage("Test:JsonView:AppReadyStateChange");
+  await server("close");
+  await appReady;
+
+  is(await getElementCount(".json.is-active"), 1, "The JSON tab is selected.");
+
+  is(await getElementCount(".jsonPanelBox .treeTable .treeRow"), 2,
+    "There is a tree with 2 rows.");
+
+  await selectJsonViewContentTab("rawdata");
+  await checkText();
+
+  is(await getElementCount("button.prettyprint"), 1, "There is a pretty print button.");
+  await clickJsonNode("button.prettyprint");
+  await checkText(JSON.stringify(JSON.parse(data), null, 2));
+});
+
+let data = " ";
+async function write(text) {
+  data += text;
+  let dataReceived = waitForContentMessage("Test:JsonView:NewDataReceived");
+  await server("write", text);
+  await dataReceived;
+}
+async function checkText(text = data) {
+  is(await getElementText(".textPanelBox .data"), text, "Got the right text.");
+}
+
+function server(action, value) {
+  return new Promise(resolve => {
+    let xhr = new XMLHttpRequest();
+    xhr.open("GET", TEST_JSON_URL + "?" + action + "=" + value);
+    xhr.addEventListener("load", resolve, {once: true});
+    xhr.send();
+  });
+}
diff --git a/devtools/client/jsonview/test/browser_jsonview_nojs.js b/devtools/client/jsonview/test/browser_jsonview_nojs.js
--- a/devtools/client/jsonview/test/browser_jsonview_nojs.js
+++ b/devtools/client/jsonview/test/browser_jsonview_nojs.js
@@ -1,25 +1,25 @@
 /* -*- indent-tabs-mode: nil; js-indent-level: 2 -*- */
 /* vim: set ts=2 et sw=2 tw=80: */
 /* Any copyright is dedicated to the Public Domain.
  * http://creativecommons.org/publicdomain/zero/1.0/ */
 
 "use strict";
 
-add_task(function* () {
+add_task(async function () {
   info("Test JSON without JavaScript started.");
 
   let oldPref = SpecialPowers.getBoolPref("javascript.enabled");
   SpecialPowers.setBoolPref("javascript.enabled", false);
 
   const TEST_JSON_URL = "data:application/json,[1,2,3]";
-  yield addJsonViewTab(TEST_JSON_URL, 0).catch(() => {
-    info("JSON Viewer did not load");
-    return executeInContent("Test:JsonView:GetElementVisibleText", {selector: "html"})
-    .then(result => {
-      info("Checking visible text contents.");
-      is(result.text, "[1,2,3]", "The raw source should be visible.");
-    });
-  });
+
+  // "uninitialized" will be the last app readyState because JS is disabled.
+  await addJsonViewTab(TEST_JSON_URL, {appReadyState: "uninitialized"});
+
+  info("Checking visible text contents.");
+  let {text} = await executeInContent("Test:JsonView:GetElementVisibleText",
+    {selector: "html"});
+  is(text, "[1,2,3]", "The raw source should be visible.");
 
   SpecialPowers.setBoolPref("javascript.enabled", oldPref);
 });
diff --git a/devtools/client/jsonview/test/chunked_json.sjs b/devtools/client/jsonview/test/chunked_json.sjs
new file mode 100644
--- /dev/null
+++ b/devtools/client/jsonview/test/chunked_json.sjs
@@ -0,0 +1,38 @@
+/* -*- indent-tabs-mode: nil; js-indent-level: 2 -*- */
+/* vim: set ts=2 et sw=2 tw=80: */
+/* Any copyright is dedicated to the Public Domain.
+ * http://creativecommons.org/publicdomain/zero/1.0/ */
+
+const key = "json-viewer-chunked-response";
+function setResponse(response) {
+  setObjectState(key, response);
+}
+function getResponse() {
+  let response;
+  getObjectState(key, v => { response = v });
+  return response;
+}
+
+function handleRequest(request, response) {
+  let {queryString} = request;
+  if (!queryString) {
+    response.processAsync();
+    setResponse(response);
+    response.setHeader("Content-Type", "application/json");
+    // Write something so that the JSON viewer app starts loading.
+    response.write(" ");
+    return;
+  }
+  let [command, value] = queryString.split('=');
+  switch (command) {
+    case "write":
+      getResponse().write(value);
+      break;
+    case "close":
+      getResponse().finish();
+      setResponse(null);
+      break;
+  }
+  response.setHeader("Content-Type", "text/plain");
+  response.write("ok");
+}
diff --git a/devtools/client/jsonview/test/doc_frame_script.js b/devtools/client/jsonview/test/doc_frame_script.js
--- a/devtools/client/jsonview/test/doc_frame_script.js
+++ b/devtools/client/jsonview/test/doc_frame_script.js
@@ -18,29 +18,38 @@ EventUtils.window = content;
 EventUtils.parent = EventUtils.window;
 EventUtils.navigator = content.navigator;
 EventUtils.KeyboardEvent = content.KeyboardEvent;
 
 Services.scriptloader.loadSubScript(
   "chrome://mochikit/content/tests/SimpleTest/EventUtils.js", EventUtils);
 
 /**
- * When the JSON View is done rendering it triggers custom event
- * "JSONViewInitialized", then the Test:TestPageProcessingDone message
- * will be sent to the parent process for tests to wait for this event
- * if needed.
+ * When the ready state of the JSON View app changes, it triggers custom event
+ * "AppReadyStateChange", then the "Test:JsonView:AppReadyStateChange" message
+ * will be sent to the parent process for tests to wait for this event if needed.
  */
-content.addEventListener("JSONViewInitialized", () => {
-  sendAsyncMessage("Test:JsonView:JSONViewInitialized");
+content.addEventListener("AppReadyStateChange", () => {
+  sendAsyncMessage("Test:JsonView:AppReadyStateChange");
 });
 
-content.addEventListener("load", () => {
-  sendAsyncMessage("Test:JsonView:load");
+/**
+ * Analogous for the standard "readystatechange" event of the document.
+ */
+content.document.addEventListener("readystatechange", () => {
+  sendAsyncMessage("Test:JsonView:DocReadyStateChange");
 });
 
+/**
+ * Send a message whenever the server sends a new chunk of JSON data.
+ */
+new content.MutationObserver(function (mutations, observer) {
+  sendAsyncMessage("Test:JsonView:NewDataReceived");
+}).observe(content.wrappedJSObject.JSONView.json, {characterData: true});
+
 addMessageListener("Test:JsonView:GetElementCount", function (msg) {
   let {selector} = msg.data;
   let nodeList = content.document.querySelectorAll(selector);
   sendAsyncMessage(msg.name, {count: nodeList.length});
 });
 
 addMessageListener("Test:JsonView:GetElementText", function (msg) {
   let {selector} = msg.data;
diff --git a/devtools/client/jsonview/test/head.js b/devtools/client/jsonview/test/head.js
--- a/devtools/client/jsonview/test/head.js
+++ b/devtools/client/jsonview/test/head.js
@@ -21,63 +21,99 @@ registerCleanupFunction(() => {
 });
 
 // XXX move some API into devtools/framework/test/shared-head.js
 
 /**
  * Add a new test tab in the browser and load the given url.
  * @param {String} url
  *   The url to be loaded in the new tab.
- * @param {Number} timeout [optional]
- *   The maximum number of milliseconds allowed before the initialization of the
- *   JSON Viewer once the tab has been loaded. If exceeded, the initialization
- *   will be considered to have failed, and the returned promise will be rejected.
- *   If this parameter is not passed or is negative, it will be ignored.
+ *
+ * @param {Object} [optional]
+ *   An object with the following optional properties:
+ *   - appReadyState: The readyState of the JSON Viewer app that you want to
+ *     wait for. Its value can be one of:
+ *      - "uninitialized": The converter has started the request.
+ *        If JavaScript is disabled, there will be no more readyState changes.
+ *      - "loading": RequireJS started loading the scripts for the JSON Viewer.
+ *        If the load timeouts, there will be no more readyState changes.
+ *      - "interactive": The JSON Viewer app loaded, but possibly not all the JSON
+ *        data has been received.
+ *      - "complete" (default): The app is fully loaded with all the JSON.
+ *   - docReadyState: The standard readyState of the document that you want to
+ *     wait for. Its value can be one of:
+ *      - "loading": The JSON data has not been completely loaded (but the app might).
+ *      - "interactive": All the JSON data has been received.
+ *      - "complete" (default): Since there aren't sub-resources like images,
+ *        behaves as "interactive". Note the app might not be loaded yet.
  */
-async function addJsonViewTab(url, timeout = -1) {
+async function addJsonViewTab(url, {
+  appReadyState = "complete",
+  docReadyState = "complete",
+} = {}) {
   info("Adding a new JSON tab with URL: '" + url + "'");
-
-  let tab = await addTab(url);
+  let tabLoaded = addTab(url);
+  let tab = gBrowser.selectedTab;
   let browser = tab.linkedBrowser;
+  await Promise.race([tabLoaded, new Promise(resolve => {
+    browser.webProgress.addProgressListener({
+      QueryInterface: XPCOMUtils.generateQI(["nsIWebProgressListener",
+                                             "nsISupportsWeakReference"]),
+      onLocationChange(webProgress) {
+        // Fires when the tab is ready but before completely loaded.
+        webProgress.removeProgressListener(this);
+        resolve();
+      },
+    }, Ci.nsIWebProgress.NOTIFY_LOCATION);
+  })]);
 
   // Load devtools/shared/frame-script-utils.js
   getFrameScript();
-
-  // Load frame script with helpers for JSON View tests.
   let rootDir = getRootDirectory(gTestPath);
-  let frameScriptUrl = rootDir + "doc_frame_script.js";
-  browser.messageManager.loadFrameScript(frameScriptUrl, false);
 
-  // Check if there is a JSONView object.
-  if (!content.window.wrappedJSObject.JSONView) {
-    throw new Error("JSON Viewer did not load.");
-  }
+  let data = {rootDir, appReadyState, docReadyState};
+  // eslint-disable-next-line no-shadow
+  await ContentTask.spawn(browser, data, async function (data) {
+    // Check if there is a JSONView object.
+    let {JSONView} = content.window.wrappedJSObject;
+    if (!JSONView) {
+      throw new Error("The JSON Viewer did not load.");
+    }
 
-  // Resolve if the JSONView is fully loaded.
-  if (content.window.wrappedJSObject.JSONView.initialized) {
-    return tab;
-  }
+    // Load frame script with helpers for JSON View tests.
+    let frameScriptUrl = data.rootDir + "doc_frame_script.js";
+    Services.scriptloader.loadSubScript(frameScriptUrl, {}, "UTF-8");
 
-  // Otherwise wait for an initialization event, possibly with a time limit.
-  const onJSONViewInitialized =
-    waitForContentMessage("Test:JsonView:JSONViewInitialized")
-    .then(() => tab);
-
-  if (!(timeout >= 0)) {
-    return onJSONViewInitialized;
-  }
+    let docReadyStates = ["loading", "interactive", "complete"];
+    let docReadyIndex = docReadyStates.indexOf(data.docReadyState);
+    let appReadyStates = ["uninitialized", ...docReadyStates];
+    let appReadyIndex = appReadyStates.indexOf(data.appReadyState);
+    if (docReadyIndex < 0 || appReadyIndex < 0) {
+      throw new Error("Invalid app or doc readyState parameter.");
+    }
 
-  if (content.window.document.readyState !== "complete") {
-    await waitForContentMessage("Test:JsonView:load");
-  }
+    // Wait until the document readyState suffices.
+    let {document} = content.window;
+    while (docReadyStates.indexOf(document.readyState) < docReadyIndex) {
+      info(`DocReadyState is "${document.readyState}". Await "${data.docReadyState}"`);
+      await new Promise(resolve => {
+        document.addEventListener("readystatechange", resolve, {once: true});
+      });
+    }
 
-  let onTimeout = new Promise((_, reject) =>
-    setTimeout(() => reject(new Error("JSON Viewer did not load.")), timeout));
+    // Wait until the app readyState suffices.
+    while (appReadyStates.indexOf(JSONView.readyState) < appReadyIndex) {
+      info(`AppReadyState is "${JSONView.readyState}". Await "${data.appReadyState}"`);
+      await new Promise(resolve => {
+        content.addEventListener("AppReadyStateChange", resolve, {once: true});
+      });
+    }
+  });
 
-  return Promise.race([onJSONViewInitialized, onTimeout]);
+  return tab;
 }
 
 /**
  * Expanding a node in the JSON tree
  */
 function clickJsonNode(selector) {
   info("Expanding node: '" + selector + "'");
 
diff --git a/devtools/client/jsonview/viewer-config.js b/devtools/client/jsonview/viewer-config.js
--- a/devtools/client/jsonview/viewer-config.js
+++ b/devtools/client/jsonview/viewer-config.js
@@ -2,16 +2,20 @@
 /* vim: set ft=javascript ts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 /* global requirejs */
 
 "use strict";
 
+// Send readyState change notification event to the window. It's useful for tests.
+JSONView.readyState = "loading";
+window.dispatchEvent(new CustomEvent("AppReadyStateChange"));
+
 /**
  * RequireJS configuration for JSON Viewer.
  *
  * ReactJS library is shared among DevTools. The minified (production) version
  * of the library is always available, and is used by default.
  *
  * In order to use the developer version you need to specify the following
  * in your .mozconfig (see also bug 1181646):
