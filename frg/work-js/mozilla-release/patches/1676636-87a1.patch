# HG changeset patch
# User Jeff Gilbert <jgilbert@mozilla.com>
# Date 1612312550 0
#      Wed Feb 03 00:35:50 2021 +0000
# Node ID 845c92efe125902c699f6030429473c02789f3df
# Parent  fed9ba8403759f08de818c35991b96d6d0b6c817
Bug 1676636 - [angle] Cherry-pick compressed tex depth stride fix. r=lsalzman

Differential Revision: https://phabricator.services.mozilla.com/D102523

diff --git a/gfx/angle/checkout/out/gen/angle/id/commit.h b/gfx/angle/checkout/out/gen/angle/id/commit.h
--- a/gfx/angle/checkout/out/gen/angle/id/commit.h
+++ b/gfx/angle/checkout/out/gen/angle/id/commit.h
@@ -1,3 +1,3 @@
-#define ANGLE_COMMIT_HASH "3a998d51928f"
+#define ANGLE_COMMIT_HASH "fa5e0bac2b3b"
 #define ANGLE_COMMIT_HASH_SIZE 12
-#define ANGLE_COMMIT_DATE "2020-11-09 16:14:24 -0800"
+#define ANGLE_COMMIT_DATE "2021-01-20 18:25:34 -0800"
diff --git a/gfx/angle/checkout/src/libANGLE/formatutils.cpp b/gfx/angle/checkout/src/libANGLE/formatutils.cpp
--- a/gfx/angle/checkout/src/libANGLE/formatutils.cpp
+++ b/gfx/angle/checkout/src/libANGLE/formatutils.cpp
@@ -1190,21 +1190,33 @@ bool InternalFormat::computeRowPitch(GLe
     return CheckedMathResult(aligned, resultOut);
 }
 
 bool InternalFormat::computeDepthPitch(GLsizei height,
                                        GLint imageHeight,
                                        GLuint rowPitch,
                                        GLuint *resultOut) const
 {
-    GLuint rows =
-        (imageHeight > 0 ? static_cast<GLuint>(imageHeight) : static_cast<GLuint>(height));
+    CheckedNumeric<GLuint> pixelsHeight(imageHeight > 0 ? static_cast<GLuint>(imageHeight)
+                                                        : static_cast<GLuint>(height));
+
+    CheckedNumeric<GLuint> rowCount;
+    if (compressed)
+    {
+        CheckedNumeric<GLuint> checkedBlockHeight(compressedBlockHeight);
+        rowCount = (pixelsHeight + checkedBlockHeight - 1u) / checkedBlockHeight;
+    }
+    else
+    {
+        rowCount = pixelsHeight;
+    }
+
     CheckedNumeric<GLuint> checkedRowPitch(rowPitch);
 
-    return CheckedMathResult(checkedRowPitch * rows, resultOut);
+    return CheckedMathResult(checkedRowPitch * rowCount, resultOut);
 }
 
 bool InternalFormat::computeDepthPitch(GLenum formatType,
                                        GLsizei width,
                                        GLsizei height,
                                        GLint alignment,
                                        GLint rowLength,
                                        GLint imageHeight,
diff --git a/gfx/angle/cherry_picks.txt b/gfx/angle/cherry_picks.txt
--- a/gfx/angle/cherry_picks.txt
+++ b/gfx/angle/cherry_picks.txt
@@ -1,8 +1,28 @@
+commit fa5e0bac2b3b087e14a06b468e354fc6a54b264a
+Author: Geoff Lang <geofflang@chromium.org>
+Date:   Wed Sep 18 16:42:28 2019 -0400
+
+    Fix depth pitch calculations for compressed textures.
+    
+    Depth pitch computations were not taking into account the block size
+    and simply multiplying the row pitch with the pixel height.  This caused
+    our load functions to use a very high depth pitch, reading past the end
+    of the user-supplied buffer.
+    
+    BUG=angleproject:3190
+    BUG=angleproject:3920
+    
+    Change-Id: I4ef4763b542735993568c51ae4b5a235659b9094
+    Reviewed-on: https://chromium-review.googlesource.com/c/angle/angle/+/1811837
+    Reviewed-by: Tim Van Patten <timvp@google.com>
+    Reviewed-by: Ian Elliott <ianelliott@google.com>
+    Commit-Queue: Geoff Lang <geofflang@chromium.org>
+
 commit 3a998d51928fb5856be5299fc189434d2da2e946
 Author: Jeff Muizelaar <jrmuizel@gmail.com>
 Date:   Mon Nov 9 17:09:12 2020 -0500
 
     Bug 1620075. Add a feature flag to allow ES3 on 10.0 devices. (#26)
     
     This lets us run WebRender on devices that only support D3D 10.0
 
