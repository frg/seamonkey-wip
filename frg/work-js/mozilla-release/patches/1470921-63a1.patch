# HG changeset patch
# User Steve Fink <sfink@mozilla.com>
# Date 1534463365 25200
# Node ID a546df23e27d5247cdad10198fac095aadb7a1dd
# Parent  e68495f68b70b9ced1a5030ebfb6a9668d2a69e7
Bug 1470921 - Re-check whether nursery strings are enabled after collecting during allocation, r=jonco

diff --git a/js/src/gc/Allocator.cpp b/js/src/gc/Allocator.cpp
--- a/js/src/gc/Allocator.cpp
+++ b/js/src/gc/Allocator.cpp
@@ -146,18 +146,19 @@ GCRuntime::tryNewNurseryString(JSContext
 
     Cell* cell = cx->nursery().allocateString(cx->zone(), thingSize, kind);
     if (cell)
         return static_cast<JSString*>(cell);
 
     if (allowGC && !cx->suppressGC) {
         cx->runtime()->gc.minorGC(JS::gcreason::OUT_OF_NURSERY);
 
-        // Exceeding gcMaxBytes while tenuring can disable the Nursery.
-        if (cx->nursery().isEnabled())
+        // Exceeding gcMaxBytes while tenuring can disable the Nursery, and
+        // other heuristics can disable nursery strings for this zone.
+        if (cx->nursery().isEnabled() && cx->zone()->allocNurseryStrings)
             return static_cast<JSString*>(cx->nursery().allocateString(cx->zone(), thingSize, kind));
     }
     return nullptr;
 }
 
 template <typename StringAllocT, AllowGC allowGC /* = CanGC */>
 StringAllocT*
 js::AllocateString(JSContext* cx, InitialHeap heap)
diff --git a/js/src/jit/CodeGenerator.cpp b/js/src/jit/CodeGenerator.cpp
--- a/js/src/jit/CodeGenerator.cpp
+++ b/js/src/jit/CodeGenerator.cpp
@@ -8688,17 +8688,18 @@ JitRealm::generateStringConcatStub(JSCon
     }
     masm.bind(&notInline);
 
     // Keep AND'ed flags in temp1.
 
     // Ensure result length <= JSString::MAX_LENGTH.
     masm.branch32(Assembler::Above, temp2, Imm32(JSString::MAX_LENGTH), &failure);
 
-    // Allocate a new rope, guaranteed to be in the nursery.
+    // Allocate a new rope, guaranteed to be in the nursery if
+    // stringsCanBeInNursery. (As a result, no post barriers are needed below.)
     masm.newGCString(output, temp3, &failure, stringsCanBeInNursery);
 
     // Store rope length and flags. temp1 still holds the result of AND'ing the
     // lhs and rhs flags, so we just have to clear the other flags and set
     // NON_ATOM_BIT to get our rope flags (Latin1 if both lhs and rhs are
     // Latin1).
     static_assert(JSString::INIT_ROPE_FLAGS == JSString::NON_ATOM_BIT,
                   "Rope type flags must be NON_ATOM_BIT only");
