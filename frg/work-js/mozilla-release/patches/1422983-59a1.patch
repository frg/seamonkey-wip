# HG changeset patch
# User Ben Kelly <ben@wanderview.com>
# Date 1512443613 18000
# Node ID 46463dab3129e5c56a96d34f1c275d14149d32bd
# Parent  af9c1a9a51da51a3034c5166a6ffa86afd16ba02
Bug 1422983 Cycle collect the ClientSource object when owned by an nsGlobalWindowInner or nsDocShell object. r=mccr8

diff --git a/docshell/base/nsDocShell.cpp b/docshell/base/nsDocShell.cpp
--- a/docshell/base/nsDocShell.cpp
+++ b/docshell/base/nsDocShell.cpp
@@ -948,16 +948,17 @@ nsDocShell::DestroyChildren()
 
   nsDocLoader::DestroyChildren();
 }
 
 NS_IMPL_CYCLE_COLLECTION_INHERITED(nsDocShell,
                                    nsDocLoader,
                                    mSessionStorageManager,
                                    mScriptGlobal,
+                                   mInitialClientSource,
                                    mChromeEventHandler)
 
 NS_IMPL_ADDREF_INHERITED(nsDocShell, nsDocLoader)
 NS_IMPL_RELEASE_INHERITED(nsDocShell, nsDocLoader)
 
 NS_INTERFACE_MAP_BEGIN_CYCLE_COLLECTION(nsDocShell)
   NS_INTERFACE_MAP_ENTRY(nsIDocShell)
   NS_INTERFACE_MAP_ENTRY(nsIDocShellTreeItem)
diff --git a/dom/base/nsGlobalWindow.cpp b/dom/base/nsGlobalWindow.cpp
--- a/dom/base/nsGlobalWindow.cpp
+++ b/dom/base/nsGlobalWindow.cpp
@@ -2290,16 +2290,18 @@ NS_IMPL_CYCLE_COLLECTION_TRAVERSE_BEGIN_
 
   NS_IMPL_CYCLE_COLLECTION_TRAVERSE(mIdleRequestExecutor)
   for (IdleRequest* request : tmp->mIdleRequestCallbacks) {
     cb.NoteNativeChild(request, NS_CYCLE_COLLECTION_PARTICIPANT(IdleRequest));
   }
 
   NS_IMPL_CYCLE_COLLECTION_TRAVERSE(mIdleObservers)
 
+  NS_IMPL_CYCLE_COLLECTION_TRAVERSE(mClientSource)
+
   NS_IMPL_CYCLE_COLLECTION_TRAVERSE(mGamepads)
 
   NS_IMPL_CYCLE_COLLECTION_TRAVERSE(mCacheStorage)
   NS_IMPL_CYCLE_COLLECTION_TRAVERSE(mVRDisplays)
 
   // Traverse stuff from nsPIDOMWindow
   NS_IMPL_CYCLE_COLLECTION_TRAVERSE(mChromeEventHandler)
   NS_IMPL_CYCLE_COLLECTION_TRAVERSE(mParentTarget)
@@ -2409,17 +2411,17 @@ NS_IMPL_CYCLE_COLLECTION_UNLINK_BEGIN(ns
   tmp->UnlinkHostObjectURIs();
 
   NS_IMPL_CYCLE_COLLECTION_UNLINK(mIdleRequestExecutor)
 
   // Here the IdleRequest list would've been unlinked, but we rely on
   // that IdleRequest objects have been traced and will remove
   // themselves while unlinking.
 
-  tmp->mClientSource.reset();
+  NS_IMPL_CYCLE_COLLECTION_UNLINK(mClientSource)
 
   NS_IMPL_CYCLE_COLLECTION_UNLINK(mPendingPromises)
 
   NS_IMPL_CYCLE_COLLECTION_UNLINK_PRESERVED_WRAPPER
 NS_IMPL_CYCLE_COLLECTION_UNLINK_END
 
 #ifdef DEBUG
 void
diff --git a/dom/clients/manager/ClientSource.cpp b/dom/clients/manager/ClientSource.cpp
--- a/dom/clients/manager/ClientSource.cpp
+++ b/dom/clients/manager/ClientSource.cpp
@@ -258,10 +258,26 @@ ClientSource::Info() const
 }
 
 nsISerialEventTarget*
 ClientSource::EventTarget() const
 {
   return mEventTarget;
 }
 
+void
+ClientSource::Traverse(nsCycleCollectionTraversalCallback& aCallback,
+                       const char* aName,
+                       uint32_t aFlags)
+{
+  if (mOwner.is<RefPtr<nsPIDOMWindowInner>>()) {
+    ImplCycleCollectionTraverse(aCallback,
+                                mOwner.as<RefPtr<nsPIDOMWindowInner>>(),
+                                aName, aFlags);
+  } else if (mOwner.is<nsCOMPtr<nsIDocShell>>()) {
+    ImplCycleCollectionTraverse(aCallback,
+                                mOwner.as<nsCOMPtr<nsIDocShell>>(),
+                                aName, aFlags);
+  }
+}
+
 } // namespace dom
 } // namespace mozilla
diff --git a/dom/clients/manager/ClientSource.h b/dom/clients/manager/ClientSource.h
--- a/dom/clients/manager/ClientSource.h
+++ b/dom/clients/manager/ClientSource.h
@@ -88,14 +88,36 @@ public:
   void
   Thaw();
 
   const ClientInfo&
   Info() const;
 
   nsISerialEventTarget*
   EventTarget() const;
+
+  void
+  Traverse(nsCycleCollectionTraversalCallback& aCallback,
+           const char* aName,
+           uint32_t aFlags);
 };
 
+inline void
+ImplCycleCollectionUnlink(UniquePtr<ClientSource>& aField)
+{
+  aField.reset();
+}
+
+inline void
+ImplCycleCollectionTraverse(nsCycleCollectionTraversalCallback& aCallback,
+                            UniquePtr<ClientSource>& aField,
+                            const char* aName,
+                            uint32_t aFlags)
+{
+  if (aField) {
+    aField->Traverse(aCallback, aName, aFlags);
+  }
+}
+
 } // namespace dom
 } // namespace mozilla
 
 #endif // _mozilla_dom_ClientSource_h
