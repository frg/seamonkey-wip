# HG changeset patch
# User Ted Campbell <tcampbell@mozilla.com>
# Date 1519346435 18000
# Node ID 2d8a49c03cad4956348e3dc91518159b50858c2e
# Parent  a396d427d831f749f80ae4c303e19810f9317d61
Bug 1438086 - Add TestMatchingHolder to CacheIR. r=jandem

This helper function abstracts away direct shape guards for typical
prototype walking cases.

MozReview-Commit-ID: 3FK9WfHzfNG

Fix previous checkin.

diff --git a/js/src/jit/CacheIR.cpp b/js/src/jit/CacheIR.cpp
--- a/js/src/jit/CacheIR.cpp
+++ b/js/src/jit/CacheIR.cpp
@@ -756,17 +756,16 @@ UncacheableProtoOnChain(JSObject* obj)
             return true;
 
         obj = obj->staticPrototype();
         if (!obj)
             return false;
     }
 }
 
-
 static void
 ShapeGuardProtoChain(CacheIRWriter& writer, JSObject* obj, ObjOperandId objId)
 {
     while (true) {
         // Guard on the proto if the shape does not imply the proto.
         bool guardProto = obj->hasUncacheableProto();
 
         obj = obj->staticPrototype();
@@ -828,17 +827,16 @@ EmitReadSlotGuard(CacheIRWriter& writer,
                 ShapeGuardProtoChainForCrossCompartmentHolder(writer, obj, objId, holder,
                                                               holderId);
             } else {
                 // Guard proto chain integrity.
                 GeneratePrototypeGuards(writer, obj, holder, objId);
 
                 // Guard on the holder's shape.
                 holderId->emplace(writer.loadObject(holder));
-                writer.guardShape(holderId->ref(), holder->as<NativeObject>().lastProperty());
                 TestMatchingHolder(writer, holder, holderId->ref());
             }
         } else {
             // The property does not exist. Guard on everything in the prototype
             // chain. This is guaranteed to see only Native objects because of
             // CanAttachNativeGetProp().
             ShapeGuardProtoChain(writer, obj, objId);
         }
@@ -2070,20 +2068,20 @@ GetPropIRGenerator::tryAttachTypedElemen
     // Don't attach typed object stubs if the underlying storage could be
     // detached, as the stub will always bail out.
     if (IsPrimitiveArrayTypedObject(obj) && cx_->compartment()->detachedTypedObjects)
         return false;
 
     TypedThingLayout layout = GetTypedThingLayout(obj->getClass());
 
     if (IsPrimitiveArrayTypedObject(obj)) {
-       writer.guardNoDetachedTypedObjects();
-       writer.guardGroupForLayout(objId, obj->group());
+        writer.guardNoDetachedTypedObjects();
+        writer.guardGroupForLayout(objId, obj->group());
     } else {
-         writer.guardShapeForClass(objId, obj->as<TypedArrayObject>().shape());
+        writer.guardShapeForClass(objId, obj->as<TypedArrayObject>().shape());
     }
 
     writer.loadTypedElementResult(objId, indexId, layout, TypedThingElementType(obj));
 
     // Reading from Uint32Array may produce an int32 now but a double value
     // later, so ensure we monitor the result.
     if (TypedThingElementType(obj) == Scalar::Type::Uint32)
         writer.typeMonitorResult();
