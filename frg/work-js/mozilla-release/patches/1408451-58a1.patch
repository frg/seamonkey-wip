# HG changeset patch
# User Christoph Kerschbaumer <ckerschb@christophkerschbaumer.com>
# Date 1508156332 -7200
# Node ID 8719c44ef3e2df2124137938d61d7ef83dfac0e9
# Parent  71a7a3bbcc4ac07004636febba2a3fe8b4848ceb
Bug 1408451: Log to web console when blocking toplevel data: URI navigations. r=bz

diff --git a/docshell/base/nsDocShell.cpp b/docshell/base/nsDocShell.cpp
--- a/docshell/base/nsDocShell.cpp
+++ b/docshell/base/nsDocShell.cpp
@@ -9826,20 +9826,23 @@ nsDocShell::InternalLoad(nsIURI* aURI,
       // an iframe since that's more common.
       contentType = nsIContentPolicy::TYPE_INTERNAL_IFRAME;
     }
   } else {
     contentType = nsIContentPolicy::TYPE_DOCUMENT;
     isTargetTopLevelDocShell = true;
   }
 
+  nsIDocument* doc = mContentViewer ? mContentViewer->GetDocument()
+                                    : nullptr;
   if (!nsContentSecurityManager::AllowTopLevelNavigationToDataURI(
         aURI,
         contentType,
         aTriggeringPrincipal,
+        doc,
         (aLoadType == LOAD_NORMAL_EXTERNAL),
         !aFileName.IsVoid())) {
     // logging to console happens within AllowTopLevelNavigationToDataURI
     return NS_OK;
   }
 
   // If there's no targetDocShell, that means we are about to create a new
   // window (or aWindowTarget is empty). Perform a content policy check before
diff --git a/docshell/base/nsDocShell.cpp.1408451.later b/docshell/base/nsDocShell.cpp.1408451.later
new file mode 100644
--- /dev/null
+++ b/docshell/base/nsDocShell.cpp.1408451.later
@@ -0,0 +1,21 @@
+--- nsDocShell.cpp
++++ nsDocShell.cpp
+@@ -10092,18 +10095,16 @@ nsDocShell::InternalLoad(nsIURI* aURI,
+ 
+         nsCOMPtr<nsIDocShellTreeItem> parent;
+         treeItem->GetSameTypeParent(getter_AddRefs(parent));
+         parent.swap(treeItem);
+       } while (treeItem);
+     }
+   }
+ 
+-  const nsIDocument* doc = mContentViewer ? mContentViewer->GetDocument()
+-                                          : nullptr;
+   const bool isDocumentAuxSandboxed = doc &&
+     (doc->GetSandboxFlags() & SANDBOXED_AUXILIARY_NAVIGATION);
+ 
+   if (aURI && mLoadURIDelegate &&
+       (!targetDocShell || targetDocShell == static_cast<nsIDocShell*>(this))) {
+     // Dispatch only load requests for the current or a new window to the
+     // delegate, e.g., to allow for GeckoView apps to handle the load event
+     // outside of Gecko.
diff --git a/dom/security/nsContentSecurityManager.cpp b/dom/security/nsContentSecurityManager.cpp
--- a/dom/security/nsContentSecurityManager.cpp
+++ b/dom/security/nsContentSecurityManager.cpp
@@ -25,16 +25,17 @@ NS_IMPL_ISUPPORTS(nsContentSecurityManag
                   nsIContentSecurityManager,
                   nsIChannelEventSink)
 
 /* static */ bool
 nsContentSecurityManager::AllowTopLevelNavigationToDataURI(
   nsIURI* aURI,
   nsContentPolicyType aContentPolicyType,
   nsIPrincipal* aTriggeringPrincipal,
+  nsIDocument* aDoc,
   bool aLoadFromExternal,
   bool aIsDownLoad)
 {
   // Let's block all toplevel document navigations to a data: URI.
   // In all cases where the toplevel document is navigated to a
   // data: URI the triggeringPrincipal is a codeBasePrincipal, or
   // a NullPrincipal. In other cases, e.g. typing a data: URL into
   // the URL-Bar, the triggeringPrincipal is a SystemPrincipal;
@@ -72,18 +73,17 @@ nsContentSecurityManager::AllowTopLevelN
   if (dataSpec.Length() > 50) {
     dataSpec.Truncate(50);
     dataSpec.AppendLiteral("...");
   }
   NS_ConvertUTF8toUTF16 specUTF16(NS_UnescapeURL(dataSpec));
   const char16_t* params[] = { specUTF16.get() };
   nsContentUtils::ReportToConsole(nsIScriptError::warningFlag,
                                   NS_LITERAL_CSTRING("DATA_URI_BLOCKED"),
-                                  // no doc available, log to browser console
-                                  nullptr,
+                                  aDoc,
                                   nsContentUtils::eSECURITY_PROPERTIES,
                                   "BlockTopLevelDataURINavigation",
                                   params, ArrayLength(params));
   return false;
 }
 
 static nsresult
 ValidateSecurityFlags(nsILoadInfo* aLoadInfo)
@@ -576,16 +576,17 @@ nsContentSecurityManager::AsyncOnChannel
     nsCOMPtr<nsIURI> uri;
     nsresult rv = NS_GetFinalChannelURI(aNewChannel, getter_AddRefs(uri));
     NS_ENSURE_SUCCESS(rv, rv);
     nsCOMPtr<nsIPrincipal> nullTriggeringPrincipal = NullPrincipal::Create();
     if (!nsContentSecurityManager::AllowTopLevelNavigationToDataURI(
           uri,
           newLoadInfo->GetExternalContentPolicyType(),
           nullTriggeringPrincipal,
+          nullptr, // no doc available, log to browser console
           false,
           false)) {
         // logging to console happens within AllowTopLevelNavigationToDataURI
       aOldChannel->Cancel(NS_ERROR_DOM_BAD_URI);
       return NS_ERROR_DOM_BAD_URI;
     }
   }
 
diff --git a/dom/security/nsContentSecurityManager.h b/dom/security/nsContentSecurityManager.h
--- a/dom/security/nsContentSecurityManager.h
+++ b/dom/security/nsContentSecurityManager.h
@@ -7,16 +7,17 @@
 #ifndef nsContentSecurityManager_h___
 #define nsContentSecurityManager_h___
 
 #include "nsIContentSecurityManager.h"
 #include "nsIChannel.h"
 #include "nsIChannelEventSink.h"
 
 class nsIStreamListener;
+class nsIDocument;
 
 #define NS_CONTENTSECURITYMANAGER_CONTRACTID "@mozilla.org/contentsecuritymanager;1"
 // cdcc1ab8-3cea-4e6c-a294-a651fa35227f
 #define NS_CONTENTSECURITYMANAGER_CID \
 { 0xcdcc1ab8, 0x3cea, 0x4e6c, \
   { 0xa2, 0x94, 0xa6, 0x51, 0xfa, 0x35, 0x22, 0x7f } }
 
 class nsContentSecurityManager : public nsIContentSecurityManager
@@ -30,16 +31,17 @@ public:
   nsContentSecurityManager() {}
 
   static nsresult doContentSecurityCheck(nsIChannel* aChannel,
                                          nsCOMPtr<nsIStreamListener>& aInAndOutListener);
 
   static bool AllowTopLevelNavigationToDataURI(nsIURI* aURI,
                                                nsContentPolicyType aContentPolicyType,
                                                nsIPrincipal* aTriggeringPrincipal,
+                                               nsIDocument* aDoc,
                                                bool aLoadFromExternal,
                                                bool aIsDownload);
 
 private:
   static nsresult CheckChannel(nsIChannel* aChannel);
 
   virtual ~nsContentSecurityManager() {}
 
