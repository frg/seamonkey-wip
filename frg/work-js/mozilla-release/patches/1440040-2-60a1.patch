# HG changeset patch
# User Andreas Pehrson <pehrsons@mozilla.com>
# Date 1519854075 -3600
#      Wed Feb 28 22:41:15 2018 +0100
# Node ID 20005cd06f7875950a02714c36327b9529b26f0d
# Parent  52312a6c6becf6c19d3f733a8c268086bd4a2587
Bug 1440040 - Properly apply microphone capture settings. r=padenot

MozReview-Commit-ID: 7TqYvHrltxm

diff --git a/dom/media/webrtc/MediaEngineWebRTC.h b/dom/media/webrtc/MediaEngineWebRTC.h
--- a/dom/media/webrtc/MediaEngineWebRTC.h
+++ b/dom/media/webrtc/MediaEngineWebRTC.h
@@ -522,17 +522,17 @@ private:
                               const nsString& aDeviceId,
                               const char** aOutBadConstraint);
 
 
   void UpdateAECSettingsIfNeeded(bool aEnable, webrtc::EcModes aMode);
   void UpdateAGCSettingsIfNeeded(bool aEnable, webrtc::AgcModes aMode);
   void UpdateNSSettingsIfNeeded(bool aEnable, webrtc::NsModes aMode);
 
-  void SetLastPrefs(const MediaEnginePrefs& aPrefs);
+  void ApplySettings(const MediaEnginePrefs& aPrefs);
 
   bool HasEnabledTrack() const;
 
   template<typename T>
   void InsertInGraph(const T* aBuffer,
                      size_t aFrames,
                      uint32_t aChannels);
 
diff --git a/dom/media/webrtc/MediaEngineWebRTCAudio.cpp b/dom/media/webrtc/MediaEngineWebRTCAudio.cpp
--- a/dom/media/webrtc/MediaEngineWebRTCAudio.cpp
+++ b/dom/media/webrtc/MediaEngineWebRTCAudio.cpp
@@ -509,37 +509,38 @@ MediaEngineWebRTCMicrophoneSource::Updat
     UpdateNSSettingsIfNeeded(prefs.mNoiseOn, static_cast<NsModes>(prefs.mNoise));
     UpdateAECSettingsIfNeeded(prefs.mAecOn, static_cast<EcModes>(prefs.mAec));
 
     webrtc::Config config;
     config.Set<webrtc::ExtendedFilter>(new webrtc::ExtendedFilter(mExtendedFilter));
     config.Set<webrtc::DelayAgnostic>(new webrtc::DelayAgnostic(mDelayAgnostic));
     mAudioProcessing->SetExtraOptions(config);
   }
-  SetLastPrefs(prefs);
+  mLastPrefs = prefs;
   return NS_OK;
 }
 
 #undef HANDLE_APM_ERROR
 
 void
-MediaEngineWebRTCMicrophoneSource::SetLastPrefs(const MediaEnginePrefs& aPrefs)
+MediaEngineWebRTCMicrophoneSource::ApplySettings(const MediaEnginePrefs& aPrefs)
 {
   AssertIsOnOwningThread();
 
   mLastPrefs = aPrefs;
 
   RefPtr<MediaEngineWebRTCMicrophoneSource> that = this;
   RefPtr<MediaStreamGraphImpl> graph;
   for (const Allocation& allocation : mAllocations) {
     if (allocation.mStream) {
       graph = allocation.mStream->GraphImpl();
       break;
     }
   }
+  MOZ_DIAGNOSTIC_ASSERT(graph);
   NS_DispatchToMainThread(media::NewRunnableFrom([that, graph, aPrefs]() mutable {
     that->mSettings->mEchoCancellation.Value() = aPrefs.mAecOn;
     that->mSettings->mAutoGainControl.Value() = aPrefs.mAgcOn;
     that->mSettings->mNoiseSuppression.Value() = aPrefs.mNoiseOn;
     that->mSettings->mChannelCount.Value() = aPrefs.mChannels;
 
     class Message : public ControlMessage {
     public:
@@ -706,16 +707,18 @@ MediaEngineWebRTCMicrophoneSource::Start
 
     // Must be *before* StartSend() so it will notice we selected external input (full_duplex)
     mAudioInput->StartRecording(allocation.mStream, mListener);
 
     MOZ_ASSERT(mState != kReleased);
     mState = kStarted;
   }
 
+  ApplySettings(mLastPrefs);
+
   return NS_OK;
 }
 
 nsresult
 MediaEngineWebRTCMicrophoneSource::Stop(const RefPtr<const AllocationHandle>& aHandle)
 {
   AssertIsOnOwningThread();
 
