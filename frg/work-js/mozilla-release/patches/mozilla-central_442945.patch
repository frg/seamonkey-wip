# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1540463759 0
#      Thu Oct 25 10:35:59 2018 +0000
# Node ID 04c752bcb18cbe9a4440d16dbb6cd84f1b21ea69
# Parent  113b4856d0f94b3e71bc24998971d67a00c75fcc
Bug 1501885 - Switch hfsplus toolchain to clang 7. r=ted

And remove the clang-6 toolchain, which is now unused.

Differential Revision: https://phabricator.services.mozilla.com/D9739

diff --git a/build/build-clang/clang-6-linux64.json b/build/build-clang/clang-6-linux64.json
deleted file mode 100644
--- a/build/build-clang/clang-6-linux64.json
+++ /dev/null
@@ -1,26 +0,0 @@
-{
-    "llvm_revision": "335538",
-    "stages": "3",
-    "build_libcxx": true,
-    "build_type": "Release",
-    "assertions": false,
-    "llvm_repo": "https://llvm.org/svn/llvm-project/llvm/tags/RELEASE_601/final",
-    "clang_repo": "https://llvm.org/svn/llvm-project/cfe/tags/RELEASE_601/final",
-    "lld_repo": "https://llvm.org/svn/llvm-project/lld/tags/RELEASE_601/final",
-    "compiler_repo": "https://llvm.org/svn/llvm-project/compiler-rt/tags/RELEASE_601/final",
-    "libcxx_repo": "https://llvm.org/svn/llvm-project/libcxx/tags/RELEASE_601/final",
-    "libcxxabi_repo": "https://llvm.org/svn/llvm-project/libcxxabi/tags/RELEASE_601/final",
-    "python_path": "/usr/bin/python2.7",
-    "gcc_dir": "/builds/worker/workspace/build/src/gcc",
-    "cc": "/builds/worker/workspace/build/src/gcc/bin/gcc",
-    "cxx": "/builds/worker/workspace/build/src/gcc/bin/g++",
-    "as": "/builds/worker/workspace/build/src/gcc/bin/gcc",
-    "patches": [
-      "static-llvm-symbolizer.patch",
-      "find_symbolizer_linux.patch",
-      "r322325.patch",
-      "r322401.patch",
-      "r325356.patch",
-      "r339636.patch"
-    ]
-}
diff --git a/build/unix/build-hfsplus/build-hfsplus.sh b/build/unix/build-hfsplus/build-hfsplus.sh
--- a/build/unix/build-hfsplus/build-hfsplus.sh
+++ b/build/unix/build-hfsplus/build-hfsplus.sh
@@ -1,10 +1,14 @@
 #!/bin/bash
 
+# hfsplus needs to be rebuilt when changing the clang version used to build it.
+# Until bug 1471905 is addressed, increase the following number
+# when that happens: 1
+
 set -e
 set -x
 
 hfplus_version=540.1.linux3
 md5sum=0435afc389b919027b69616ad1b05709
 filename=diskdev_cmds-${hfplus_version}.tar.gz
 make_flags="-j$(getconf _NPROCESSORS_ONLN)"
 
diff --git a/taskcluster/ci/toolchain/linux.yml b/taskcluster/ci/toolchain/linux.yml
--- a/taskcluster/ci/toolchain/linux.yml
+++ b/taskcluster/ci/toolchain/linux.yml
@@ -22,37 +22,16 @@ linux64-clang-3.9:
         resources:
             - 'build/build-clang/build-clang.py'
             - 'build/build-clang/clang-3.9-linux64.json'
             - 'taskcluster/scripts/misc/tooltool-download.sh'
         toolchain-artifact: public/build/clang.tar.xz
     toolchains:
         - linux64-gcc-4.9
 
-linux64-clang-6:
-    description: "Clang 6 toolchain build"
-    treeherder:
-        kind: build
-        platform: toolchains/opt
-        symbol: TL(clang6)
-        tier: 1
-    worker-type: aws-provisioner-v1/gecko-{level}-b-linux-xlarge
-    worker:
-        max-run-time: 7200
-    run:
-        using: toolchain-script
-        script: build-clang-6-linux.sh
-        resources:
-            - 'build/build-clang/build-clang.py'
-            - 'build/build-clang/clang-6-linux64.json'
-            - 'taskcluster/scripts/misc/tooltool-download.sh'
-        toolchain-artifact: public/build/clang.tar.xz
-    toolchains:
-        - linux64-gcc-4.9
-
 linux64-clang-7:
     description: "Clang 7 toolchain build"
     treeherder:
         kind: build
         platform: toolchains/opt
         symbol: TL(clang7)
         tier: 1
     worker-type: aws-provisioner-v1/gecko-{level}-b-linux-xlarge
@@ -369,17 +348,17 @@ linux64-hfsplus:
     run:
         using: toolchain-script
         script: build-hfsplus-linux.sh
         resources:
             - 'build/unix/build-hfsplus/build-hfsplus.sh'
             - 'taskcluster/scripts/misc/tooltool-download.sh'
         toolchain-artifact: public/build/hfsplus-tools.tar.xz
     toolchains:
-        - linux64-clang-6
+        - linux64-clang-7
 
 linux64-libdmg:
     description: "libdmg-hfsplus toolchain build"
     treeherder:
         kind: build
         platform: toolchains/opt
         symbol: TL(libdmg-hfs+)
         tier: 1
diff --git a/taskcluster/scripts/misc/build-clang-6-linux.sh b/taskcluster/scripts/misc/build-clang-6-linux.sh
deleted file mode 100755
--- a/taskcluster/scripts/misc/build-clang-6-linux.sh
+++ /dev/null
@@ -1,25 +0,0 @@
-#!/bin/bash
-set -x -e -v
-
-# This script is for building clang for Linux.
-
-WORKSPACE=$HOME/workspace
-HOME_DIR=$WORKSPACE/build
-UPLOAD_DIR=$HOME/artifacts
-
-cd $HOME_DIR/src
-
-. taskcluster/scripts/misc/tooltool-download.sh
-
-# gets a bit too verbose here
-set +x
-
-cd build/build-clang
-# |mach python| sets up a virtualenv for us!
-../../mach python ./build-clang.py -c clang-6-linux64.json
-
-set -x
-
-# Put a tarball in the artifacts dir
-mkdir -p $UPLOAD_DIR
-cp clang.tar.* $UPLOAD_DIR
