# HG changeset patch
# User Andre Bargull <andre.bargull@gmail.com>
# Date 1533570223 25200
# Node ID df05fdfe1af8936b52613cd05b81948846a9bad6
# Parent  b20289e446893be662123b91104e35646408a578
Bug 1481248: Use strict instead of sloppy equals for 'throw' method check in yield*. r=arai

diff --git a/js/src/frontend/BytecodeEmitter.cpp b/js/src/frontend/BytecodeEmitter.cpp
--- a/js/src/frontend/BytecodeEmitter.cpp
+++ b/js/src/frontend/BytecodeEmitter.cpp
@@ -6065,21 +6065,23 @@ BytecodeEmitter::emitAwaitInScope(Emitte
 }
 
 bool
 BytecodeEmitter::emitYieldStar(ParseNode* iter)
 {
     MOZ_ASSERT(sc->isFunctionBox());
     MOZ_ASSERT(sc->asFunctionBox()->isGenerator());
 
-    bool isAsyncGenerator = sc->asFunctionBox()->isAsync();
+    IteratorKind iterKind = sc->asFunctionBox()->isAsync()
+                            ? IteratorKind::Async
+                            : IteratorKind::Sync;
 
     if (!emitTree(iter))                                  // ITERABLE
         return false;
-    if (isAsyncGenerator) {
+    if (iterKind == IteratorKind::Async) {
         if (!emitAsyncIterator())                         // NEXT ITER
             return false;
     } else {
         if (!emitIterator())                              // NEXT ITER
             return false;
     }
 
     // Initial send value is undefined.
@@ -6097,77 +6099,62 @@ BytecodeEmitter::emitYieldStar(ParseNode
 
     JumpTarget tryStart{ offset() };
     if (!tryCatch.emitTry())                              // NEXT ITER RESULT
         return false;
 
     MOZ_ASSERT(this->stackDepth == startDepth);
 
     // 11.4.3.7 AsyncGeneratorYield step 5.
-    if (isAsyncGenerator) {
+    if (iterKind == IteratorKind::Async) {
         if (!emitAwaitInInnermostScope())                 // NEXT ITER RESULT
             return false;
     }
 
     // Load the generator object.
     if (!emitGetDotGeneratorInInnermostScope())           // NEXT ITER RESULT GENOBJ
         return false;
 
     // Yield RESULT as-is, without re-boxing.
     if (!emitYieldOp(JSOP_YIELD))                         // NEXT ITER RECEIVED
         return false;
 
     if (!tryCatch.emitCatch())                            // NEXT ITER RESULT
         return false;
 
-    stackDepth = startDepth;                              // NEXT ITER RESULT
+    MOZ_ASSERT(stackDepth == startDepth);
+
     if (!emit1(JSOP_EXCEPTION))                           // NEXT ITER RESULT EXCEPTION
         return false;
     if (!emitDupAt(2))                                    // NEXT ITER RESULT EXCEPTION ITER
         return false;
     if (!emit1(JSOP_DUP))                                 // NEXT ITER RESULT EXCEPTION ITER ITER
         return false;
     if (!emitAtomOp(cx->names().throw_, JSOP_CALLPROP))   // NEXT ITER RESULT EXCEPTION ITER THROW
         return false;
-    if (!emit1(JSOP_DUP))                                 // NEXT ITER RESULT EXCEPTION ITER THROW THROW
-        return false;
-    if (!emit1(JSOP_UNDEFINED))                           // NEXT ITER RESULT EXCEPTION ITER THROW THROW UNDEFINED
-        return false;
-    if (!emit1(JSOP_EQ))                                  // NEXT ITER RESULT EXCEPTION ITER THROW ?EQL
-        return false;
-
-    InternalIfEmitter ifThrowMethodIsNotDefined(this);
-    if (!ifThrowMethodIsNotDefined.emitThen())            // NEXT ITER RESULT EXCEPTION ITER THROW
-        return false;
+
     savedDepthTemp = stackDepth;
-    if (!emit1(JSOP_POP))                                 // NEXT ITER RESULT EXCEPTION ITER
-        return false;
-    // ES 14.4.13, YieldExpression : yield * AssignmentExpression, step 5.b.iii.2
-    //
-    // If the iterator does not have a "throw" method, it calls IteratorClose
-    // and then throws a TypeError.
-    IteratorKind iterKind = isAsyncGenerator ? IteratorKind::Async : IteratorKind::Sync;
-    if (!emitIteratorCloseInInnermostScope(iterKind))     // NEXT ITER RESULT EXCEPTION
-        return false;
-    if (!emitUint16Operand(JSOP_THROWMSG, JSMSG_ITERATOR_NO_THROW)) // throw
-        return false;
-    stackDepth = savedDepthTemp;
-    if (!ifThrowMethodIsNotDefined.emitEnd())             // NEXT ITER OLDRESULT EXCEPTION ITER THROW
-        return false;
+    InternalIfEmitter ifThrowMethodIsNotDefined(this);
+    if (!emitPushNotUndefinedOrNull())                    // NEXT ITER RESULT EXCEPTION ITER THROW NOT-UNDEF-OR-NULL
+        return false;
+
+    if (!ifThrowMethodIsNotDefined.emitThenElse())        // NEXT ITER RESULT EXCEPTION ITER THROW
+        return false;
+
     // ES 14.4.13, YieldExpression : yield * AssignmentExpression, step 5.b.iii.4.
     // RESULT = ITER.throw(EXCEPTION)                     // NEXT ITER OLDRESULT EXCEPTION ITER THROW
     if (!emit1(JSOP_SWAP))                                // NEXT ITER OLDRESULT EXCEPTION THROW ITER
         return false;
     if (!emit2(JSOP_PICK, 2))                             // NEXT ITER OLDRESULT THROW ITER EXCEPTION
         return false;
     if (!emitCall(JSOP_CALL, 1, iter))                    // NEXT ITER OLDRESULT RESULT
         return false;
     checkTypeSet(JSOP_CALL);
 
-    if (isAsyncGenerator) {
+    if (iterKind == IteratorKind::Async) {
         if (!emitAwaitInInnermostScope())                 // NEXT ITER OLDRESULT RESULT
             return false;
     }
 
     if (!emitCheckIsObj(CheckIsObjectKind::IteratorThrow)) // NEXT ITER OLDRESULT RESULT
         return false;
     if (!emit1(JSOP_SWAP))                                // NEXT ITER RESULT OLDRESULT
         return false;
@@ -6177,16 +6164,36 @@ BytecodeEmitter::emitYieldStar(ParseNode
     JumpList checkResult;
     // ES 14.4.13, YieldExpression : yield * AssignmentExpression, step 5.b.ii.
     //
     // Note that there is no GOSUB to the finally block here. If the iterator has a
     // "throw" method, it does not perform IteratorClose.
     if (!emitJump(JSOP_GOTO, &checkResult))               // goto checkResult
         return false;
 
+    stackDepth = savedDepthTemp;
+    if (!ifThrowMethodIsNotDefined.emitElse())            // NEXT ITER RESULT EXCEPTION ITER THROW
+        return false;
+
+    if (!emit1(JSOP_POP))                                 // NEXT ITER RESULT EXCEPTION ITER
+        return false;
+    // ES 14.4.13, YieldExpression : yield * AssignmentExpression, step 5.b.iii.2
+    //
+    // If the iterator does not have a "throw" method, it calls IteratorClose
+    // and then throws a TypeError.
+    if (!emitIteratorCloseInInnermostScope(iterKind))     // NEXT ITER RESULT EXCEPTION
+        return false;
+    if (!emitUint16Operand(JSOP_THROWMSG, JSMSG_ITERATOR_NO_THROW)) // throw
+        return false;
+
+    stackDepth = savedDepthTemp;
+    if (!ifThrowMethodIsNotDefined.emitEnd())
+        return false;
+
+    stackDepth = startDepth;
     if (!tryCatch.emitFinally())
          return false;
 
     // ES 14.4.13, yield * AssignmentExpression, step 5.c
     //
     // Call iterator.return() for receiving a "forced return" completion from
     // the generator.
 
@@ -6298,17 +6305,17 @@ BytecodeEmitter::emitYieldStar(ParseNode
     if (!emit1(JSOP_DUP2))                                       // RECEIVED NEXT ITER NEXT ITER
         return false;
     if (!emit2(JSOP_PICK, 4))                                    // NEXT ITER NEXT ITER RECEIVED
         return false;
     if (!emitCall(JSOP_CALL, 1, iter))                           // NEXT ITER RESULT
         return false;
     checkTypeSet(JSOP_CALL);
 
-    if (isAsyncGenerator) {
+    if (iterKind == IteratorKind::Async) {
         if (!emitAwaitInInnermostScope())                        // NEXT ITER RESULT RESULT
             return false;
     }
 
     if (!emitCheckIsObj(CheckIsObjectKind::IteratorNext))        // NEXT ITER RESULT
         return false;
     MOZ_ASSERT(this->stackDepth == startDepth);
 
diff --git a/js/src/tests/non262/generators/yield-star-throw-htmldda.js b/js/src/tests/non262/generators/yield-star-throw-htmldda.js
new file mode 100644
--- /dev/null
+++ b/js/src/tests/non262/generators/yield-star-throw-htmldda.js
@@ -0,0 +1,28 @@
+function* g(iter) {
+    yield* iter;
+}
+
+var calledReturn = false;
+
+var it = g({
+    [Symbol.iterator]() {
+        return this;
+    },
+    next() {
+        return {done: false};
+    },
+    throw: createIsHTMLDDA(),
+    return() {
+        calledReturn = true;
+        return {done: false};
+    }
+});
+
+it.next();
+
+assertThrowsInstanceOf(() => it.throw(""), TypeError);
+
+assertEq(calledReturn, false);
+
+if (typeof reportCompare === "function")
+    reportCompare(0, 0);

