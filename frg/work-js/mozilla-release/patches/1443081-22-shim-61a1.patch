# HG changeset patch
# User J. Ryan Stinnett <jryans@gmail.com>
# Date 1520879078 18000
# Node ID 49cce2c9e2ab21606a9abcdc0ab0af991806957f
# Parent  9cd3ed50c7bbd2eb8efc722a2be519dc3cde069c
Bug 1443081 - Apply spacing via `eslint --fix` for DevTools - part 22. r=jdescottes

MozReview-Commit-ID: 2RVNt140Zte

diff --git a/devtools/shim/DevToolsShim.jsm b/devtools/shim/DevToolsShim.jsm
--- a/devtools/shim/DevToolsShim.jsm
+++ b/devtools/shim/DevToolsShim.jsm
@@ -48,65 +48,65 @@ this.DevToolsShim = {
   tools: [],
   themes: [],
 
   /**
    * Check if DevTools are currently installed (but not necessarily initialized).
    *
    * @return {Boolean} true if DevTools are installed.
    */
-  isInstalled: function () {
+  isInstalled: function() {
     return Services.io.getProtocolHandler("resource")
              .QueryInterface(Ci.nsIResProtocolHandler)
              .hasSubstitution("devtools");
   },
 
   /**
    * Returns true if DevTools are enabled for the current profile. If devtools are not
    * enabled, initializing DevTools will open the onboarding page. Some entry points
    * should no-op in this case.
    */
-  isEnabled: function () {
+  isEnabled: function() {
     let enabled = Services.prefs.getBoolPref(DEVTOOLS_ENABLED_PREF);
     return enabled && !this.isDisabledByPolicy();
   },
 
   /**
    * Returns true if the devtools are completely disabled and can not be enabled. All
    * entry points should return without throwing, initDevTools should never be called.
    */
-  isDisabledByPolicy: function () {
+  isDisabledByPolicy: function() {
     return Services.prefs.getBoolPref(DEVTOOLS_POLICY_DISABLED_PREF, false);
   },
 
   /**
    * Check if DevTools have already been initialized.
    *
    * @return {Boolean} true if DevTools are initialized.
    */
-  isInitialized: function () {
+  isInitialized: function() {
     return !!this._gDevTools;
   },
 
   /**
    * Register an instance of gDevTools. Should be called by DevTools during startup.
    *
    * @param {DevTools} a devtools instance (from client/framework/devtools)
    */
-  register: function (gDevTools) {
+  register: function(gDevTools) {
     this._gDevTools = gDevTools;
     this._onDevToolsRegistered();
     this._gDevTools.emit("devtools-registered");
   },
 
   /**
    * Unregister the current instance of gDevTools. Should be called by DevTools during
    * shutdown.
    */
-  unregister: function () {
+  unregister: function() {
     if (this.isInitialized()) {
       this._gDevTools.emit("devtools-unregistered");
       this._gDevTools = null;
     }
   },
 
   /**
    * The following methods can be called before DevTools are initialized:
@@ -121,103 +121,103 @@ this.DevToolsShim = {
    * appropriate method as soon as a gDevTools instance is registered.
    */
 
   /**
    * This method is used by browser/components/extensions/ext-devtools.js for the events:
    * - toolbox-created
    * - toolbox-destroyed
    */
-  on: function (event, listener) {
+  on: function(event, listener) {
     if (this.isInitialized()) {
       this._gDevTools.on(event, listener);
     } else {
       this.listeners.push([event, listener]);
     }
   },
 
   /**
    * This method is currently only used by devtools code, but is kept here for consistency
    * with on().
    */
-  off: function (event, listener) {
+  off: function(event, listener) {
     if (this.isInitialized()) {
       this._gDevTools.off(event, listener);
     } else {
       removeItem(this.listeners, ([e, l]) => e === event && l === listener);
     }
   },
 
   /**
    * This method is only used by the addon-sdk and should be removed when Firefox 56 is
    * no longer supported.
    */
-  registerTool: function (tool) {
+  registerTool: function(tool) {
     if (this.isInitialized()) {
       this._gDevTools.registerTool(tool);
     } else {
       this.tools.push(tool);
     }
   },
 
   /**
    * This method is only used by the addon-sdk and should be removed when Firefox 56 is
    * no longer supported.
    */
-  unregisterTool: function (tool) {
+  unregisterTool: function(tool) {
     if (this.isInitialized()) {
       this._gDevTools.unregisterTool(tool);
     } else {
       removeItem(this.tools, t => t === tool);
     }
   },
 
   /**
    * This method is only used by the addon-sdk and should be removed when Firefox 56 is
    * no longer supported.
    */
-  registerTheme: function (theme) {
+  registerTheme: function(theme) {
     if (this.isInitialized()) {
       this._gDevTools.registerTheme(theme);
     } else {
       this.themes.push(theme);
     }
   },
 
   /**
    * This method is only used by the addon-sdk and should be removed when Firefox 56 is
    * no longer supported.
    */
-  unregisterTheme: function (theme) {
+  unregisterTheme: function(theme) {
     if (this.isInitialized()) {
       this._gDevTools.unregisterTheme(theme);
     } else {
       removeItem(this.themes, t => t === theme);
     }
   },
 
   /**
    * Called from SessionStore.jsm in mozilla-central when saving the current state.
    *
    * @param {Object} state
    *                 A SessionStore state object that gets modified by reference
    */
-  saveDevToolsSession: function (state) {
+  saveDevToolsSession: function(state) {
     if (!this.isInitialized()) {
       return;
     }
 
     this._gDevTools.saveDevToolsSession(state);
   },
 
   /**
    * Called from SessionStore.jsm in mozilla-central when restoring a previous session.
    * Will always be called, even if the session does not contain DevTools related items.
    */
-  restoreDevToolsSession: function (session) {
+  restoreDevToolsSession: function(session) {
     if (!this.isEnabled()) {
       return;
     }
 
     let {scratchpads, browserConsole, browserToolbox} = session;
     let hasDevToolsData = browserConsole || browserToolbox ||
                           (scratchpads && scratchpads.length);
     if (!hasDevToolsData) {
@@ -237,30 +237,30 @@ this.DevToolsShim = {
    *        The browser tab on which inspect node was used.
    * @param {Array} selectors
    *        An array of CSS selectors to find the target node. Several selectors can be
    *        needed if the element is nested in frames and not directly in the root
    *        document.
    * @return {Promise} a promise that resolves when the node is selected in the inspector
    *         markup view or that resolves immediately if DevTools are not installed.
    */
-  inspectNode: function (tab, selectors) {
+  inspectNode: function(tab, selectors) {
     if (!this.isEnabled()) {
       if (!this.isDisabledByPolicy()) {
         DevtoolsStartup.openInstallPage("ContextMenu");
       }
       return Promise.resolve();
     }
 
     this.initDevTools("ContextMenu");
 
     return this._gDevTools.inspectNode(tab, selectors);
   },
 
-  _onDevToolsRegistered: function () {
+  _onDevToolsRegistered: function() {
     // Register all pending event listeners on the real gDevTools object.
     for (let [event, listener] of this.listeners) {
       this._gDevTools.on(event, listener);
     }
 
     for (let tool of this.tools) {
       this._gDevTools.registerTool(tool);
     }
@@ -278,17 +278,17 @@ this.DevToolsShim = {
    * Initialize DevTools via DevToolsStartup if needed. This method throws if DevTools are
    * not enabled.. If the entry point is supposed to trigger the onboarding, call it
    * explicitly via DevtoolsStartup.openInstallPage().
    *
    * @param {String} reason
    *        optional, if provided should be a valid entry point for DEVTOOLS_ENTRY_POINT
    *        in toolkit/components/telemetry/Histograms.json
    */
-  initDevTools: function (reason) {
+  initDevTools: function(reason) {
     if (!this.isEnabled()) {
       throw new Error("DevTools are not enabled and can not be initialized.");
     }
 
     if (!this.isInitialized()) {
       DevtoolsStartup.initDevTools(reason);
     }
   }
@@ -320,17 +320,17 @@ let webExtensionsMethods = [
   "createTargetForTab",
   "createWebExtensionInspectedWindowFront",
   "getTargetForTab",
   "getTheme",
   "openBrowserConsole",
 ];
 
 for (let method of [...addonSdkMethods, ...webExtensionsMethods]) {
-  this.DevToolsShim[method] = function () {
+  this.DevToolsShim[method] = function() {
     if (!this.isEnabled()) {
       throw new Error("Could not call a DevToolsShim webextension method ('" + method +
         "'): DevTools are not initialized.");
     }
 
     this.initDevTools();
     return this._gDevTools[method].apply(this._gDevTools, arguments);
   };
diff --git a/devtools/shim/aboutdebugging-registration.js b/devtools/shim/aboutdebugging-registration.js
--- a/devtools/shim/aboutdebugging-registration.js
+++ b/devtools/shim/aboutdebugging-registration.js
@@ -18,25 +18,25 @@ function AboutDebugging() {}
 AboutDebugging.prototype = {
   uri: Services.io.newURI("chrome://devtools/content/aboutdebugging/aboutdebugging.xhtml"),
   classDescription: "about:debugging",
   classID: Components.ID("1060afaf-dc9e-43da-8646-23a2faf48493"),
   contractID: "@mozilla.org/network/protocol/about;1?what=debugging",
 
   QueryInterface: XPCOMUtils.generateQI([nsIAboutModule]),
 
-  newChannel: function (uri, loadInfo) {
+  newChannel: function(uri, loadInfo) {
     let chan = Services.io.newChannelFromURIWithLoadInfo(
       this.uri,
       loadInfo
     );
     chan.owner = Services.scriptSecurityManager.getSystemPrincipal();
     return chan;
   },
 
-  getURIFlags: function (uri) {
+  getURIFlags: function(uri) {
     return nsIAboutModule.ALLOW_SCRIPT;
   }
 };
 
 this.NSGetFactory = XPCOMUtils.generateNSGetFactory([
   AboutDebugging
 ]);
diff --git a/devtools/shim/aboutdevtools/aboutdevtools-registration.js b/devtools/shim/aboutdevtools/aboutdevtools-registration.js
--- a/devtools/shim/aboutdevtools/aboutdevtools-registration.js
+++ b/devtools/shim/aboutdevtools/aboutdevtools-registration.js
@@ -16,25 +16,25 @@ function AboutDevtools() {}
 AboutDevtools.prototype = {
   uri: Services.io.newURI("chrome://devtools-shim/content/aboutdevtools/aboutdevtools.xhtml"),
   classDescription: "about:devtools",
   classID: Components.ID("3a16d383-92bd-4c24-ac10-0e2bd66883ab"),
   contractID: "@mozilla.org/network/protocol/about;1?what=devtools",
 
   QueryInterface: XPCOMUtils.generateQI([nsIAboutModule]),
 
-  newChannel: function (uri, loadInfo) {
+  newChannel: function(uri, loadInfo) {
     let chan = Services.io.newChannelFromURIWithLoadInfo(
       this.uri,
       loadInfo
     );
     chan.owner = Services.scriptSecurityManager.getSystemPrincipal();
     return chan;
   },
 
-  getURIFlags: function (uri) {
+  getURIFlags: function(uri) {
     return nsIAboutModule.ALLOW_SCRIPT;
   }
 };
 
 this.NSGetFactory = XPCOMUtils.generateNSGetFactory([
   AboutDevtools
 ]);
diff --git a/devtools/shim/aboutdevtools/aboutdevtools.js b/devtools/shim/aboutdevtools/aboutdevtools.js
--- a/devtools/shim/aboutdevtools/aboutdevtools.js
+++ b/devtools/shim/aboutdevtools/aboutdevtools.js
@@ -131,17 +131,17 @@ function createFeatureEl(feature) {
      <p class="feature-desc">
        ${desc}
        <a class="external feature-link" href="${link}" target="_blank">${learnMore}</a>
      </p>`;
 
   return li;
 }
 
-window.addEventListener("load", function () {
+window.addEventListener("load", function() {
   const inspectorShortcut = getToolboxShortcut();
   const welcomeMessage = document.getElementById("welcome-message");
 
   // Set the welcome message content with the correct keyboard sortcut for the current
   // platform.
   welcomeMessage.textContent = aboutDevtoolsBundle.formatStringFromName("welcome.message",
     [inspectorShortcut], 1);
 
@@ -168,17 +168,17 @@ window.addEventListener("load", function
   for (let feature of features) {
     featuresContainer.appendChild(createFeatureEl(feature));
   }
 
   // Update the current page based on the current value of DEVTOOLS_ENABLED_PREF.
   updatePage();
 }, { once: true });
 
-window.addEventListener("beforeunload", function () {
+window.addEventListener("beforeunload", function() {
   // Focus the tab that triggered the DevTools onboarding.
   if (document.visibilityState != "visible") {
     // Only try to focus the correct tab if the current tab is the about:devtools page.
     return;
   }
 
   // Retrieve the original tab if it is still available.
   let browserWindow = Services.wm.getMostRecentWindow("navigator:browser");
@@ -187,13 +187,13 @@ window.addEventListener("beforeunload", 
   let originalTab = gBrowser.getTabForBrowser(originalBrowser);
 
   if (originalTab) {
     // If the original tab was found, select it.
     gBrowser.selectedTab = originalTab;
   }
 }, {once: true});
 
-window.addEventListener("unload", function () {
+window.addEventListener("unload", function() {
   document.getElementById("install").removeEventListener("click", onInstallButtonClick);
   document.getElementById("close").removeEventListener("click", onCloseButtonClick);
   Services.prefs.removeObserver(DEVTOOLS_ENABLED_PREF, updatePage);
 }, {once: true});
diff --git a/devtools/shim/aboutdevtools/test/browser_aboutdevtools_closes_page.js b/devtools/shim/aboutdevtools/test/browser_aboutdevtools_closes_page.js
--- a/devtools/shim/aboutdevtools/test/browser_aboutdevtools_closes_page.js
+++ b/devtools/shim/aboutdevtools/test/browser_aboutdevtools_closes_page.js
@@ -1,17 +1,17 @@
 /* vim: set ts=2 et sw=2 tw=80: */
 /* Any copyright is dedicated to the Public Domain.
  http://creativecommons.org/publicdomain/zero/1.0/ */
 
 "use strict";
 
 /* eslint-env browser */
 
-add_task(async function () {
+add_task(async function() {
   pushPref("devtools.enabled", false);
 
   let {doc, win} = await openAboutDevTools();
 
   info("Check that the close button is available on the page");
   let closeButton = doc.getElementById("close");
   ok(closeButton, "close button is displayed");
 
diff --git a/devtools/shim/aboutdevtools/test/browser_aboutdevtools_enables_devtools.js b/devtools/shim/aboutdevtools/test/browser_aboutdevtools_enables_devtools.js
--- a/devtools/shim/aboutdevtools/test/browser_aboutdevtools_enables_devtools.js
+++ b/devtools/shim/aboutdevtools/test/browser_aboutdevtools_enables_devtools.js
@@ -1,17 +1,17 @@
 /* vim: set ts=2 et sw=2 tw=80: */
 /* Any copyright is dedicated to the Public Domain.
  http://creativecommons.org/publicdomain/zero/1.0/ */
 
 "use strict";
 
 /* eslint-env browser */
 
-add_task(async function () {
+add_task(async function() {
   pushPref("devtools.enabled", false);
 
   let {tab, doc, win} = await openAboutDevTools();
 
   let installPage = doc.getElementById("install-page");
   let welcomePage = doc.getElementById("welcome-page");
 
   info("Check that about:devtools is in the correct state with devtools.enabled=false");
diff --git a/devtools/shim/aboutdevtools/test/browser_aboutdevtools_focus_owner_tab.js b/devtools/shim/aboutdevtools/test/browser_aboutdevtools_focus_owner_tab.js
--- a/devtools/shim/aboutdevtools/test/browser_aboutdevtools_focus_owner_tab.js
+++ b/devtools/shim/aboutdevtools/test/browser_aboutdevtools_focus_owner_tab.js
@@ -5,17 +5,17 @@
 "use strict";
 
 /* eslint-env browser */
 
 /**
  * When closing about:devtools, test that the tab where the user triggered about:devtools
  * is selected again.
  */
-add_task(async function () {
+add_task(async function() {
   await pushPref("devtools.enabled", false);
 
   info("Add an about:blank tab");
   let tab1 = await addTab("data:text/html;charset=utf-8,tab1");
   let tab2 = await addTab("data:text/html;charset=utf-8,tab2");
   ok(tab1 === gBrowser.tabs[1], "tab1 is the second tab in the current browser window");
 
   info("Select the first tab");
@@ -41,17 +41,17 @@ add_task(async function () {
   await removeTab(tab1);
   await removeTab(tab2);
 });
 
 /**
  * When closing about:devtools, test that the current tab is not updated if
  * about:devtools was not the selectedTab.
  */
-add_task(async function () {
+add_task(async function() {
   await pushPref("devtools.enabled", false);
 
   info("Add an about:blank tab");
   let tab1 = await addTab("data:text/html;charset=utf-8,tab1");
   let tab2 = await addTab("data:text/html;charset=utf-8,tab2");
   ok(tab1 === gBrowser.tabs[1], "tab1 is the second tab in the current browser window");
 
   info("Select the first tab");
diff --git a/devtools/shim/aboutdevtools/test/browser_aboutdevtools_reuse_existing.js b/devtools/shim/aboutdevtools/test/browser_aboutdevtools_reuse_existing.js
--- a/devtools/shim/aboutdevtools/test/browser_aboutdevtools_reuse_existing.js
+++ b/devtools/shim/aboutdevtools/test/browser_aboutdevtools_reuse_existing.js
@@ -4,17 +4,17 @@
 
 "use strict";
 
 /* eslint-env browser */
 
 /**
  * Test that only one tab of about:devtools is used for a given window.
  */
-add_task(async function () {
+add_task(async function() {
   await pushPref("devtools.enabled", false);
 
   info("Add an about:blank tab");
   let tab = await addTab("about:blank");
 
   synthesizeToggleToolboxKey();
 
   info("Wait for the about:devtools tab to be selected");
diff --git a/devtools/shim/aboutdevtools/test/head.js b/devtools/shim/aboutdevtools/test/head.js
--- a/devtools/shim/aboutdevtools/test/head.js
+++ b/devtools/shim/aboutdevtools/test/head.js
@@ -13,31 +13,31 @@ waitForExplicitFinish();
 /**
  * Waits until a predicate returns true.
  *
  * @param function predicate
  *        Invoked once in a while until it returns true.
  * @param number interval [optional]
  *        How often the predicate is invoked, in milliseconds.
  */
-const waitUntil = function (predicate, interval = 100) {
+const waitUntil = function(predicate, interval = 100) {
   if (predicate()) {
     return Promise.resolve(true);
   }
   return new Promise(resolve => {
-    setTimeout(function () {
+    setTimeout(function() {
       waitUntil(predicate, interval).then(() => resolve(true));
     }, interval);
   });
 };
 
 /**
  * Open the provided url in a new tab.
  */
-const addTab = async function (url) {
+const addTab = async function(url) {
   info("Adding a new tab with URL: " + url);
 
   let { gBrowser } = window;
 
   let tab = BrowserTestUtils.addTab(gBrowser, url);
   gBrowser.selectedTab = tab;
 
   await BrowserTestUtils.browserLoaded(tab.linkedBrowser);
@@ -47,48 +47,48 @@ const addTab = async function (url) {
   return tab;
 };
 
 /**
  * Remove the given tab.
  * @param {Object} tab The tab to be removed.
  * @return Promise<undefined> resolved when the tab is successfully removed.
  */
-const removeTab = async function (tab) {
+const removeTab = async function(tab) {
   info("Removing tab.");
 
   let { gBrowser } = tab.ownerGlobal;
 
   await new Promise(resolve => {
     gBrowser.tabContainer.addEventListener("TabClose", resolve, {once: true});
     gBrowser.removeTab(tab);
   });
 
   info("Tab removed and finished closing");
 };
 
 /**
  * Open a new tab on about:devtools
  */
-const openAboutDevTools = async function () {
+const openAboutDevTools = async function() {
   info("Open about:devtools programmatically in a new tab");
   let tab = await addTab("about:devtools");
 
   let browser = tab.linkedBrowser;
   let doc = browser.contentDocument;
   let win = browser.contentWindow;
 
   return {tab, doc, win};
 };
 
 /**
  * Copied from devtools shared-head.js.
  * Set a temporary value for a preference, that will be cleaned up after the test.
  */
-const pushPref = function (preferenceName, value) {
+const pushPref = function(preferenceName, value) {
   return new Promise(resolve => {
     let options = {"set": [[preferenceName, value]]};
     SpecialPowers.pushPrefEnv(options, resolve);
   });
 };
 
 /**
  * Helper to call the toggle devtools shortcut.
diff --git a/devtools/shim/aboutdevtoolstoolbox-registration.js b/devtools/shim/aboutdevtoolstoolbox-registration.js
--- a/devtools/shim/aboutdevtoolstoolbox-registration.js
+++ b/devtools/shim/aboutdevtoolstoolbox-registration.js
@@ -20,23 +20,23 @@ function AboutDevtoolsToolbox() {}
 AboutDevtoolsToolbox.prototype = {
   uri: Services.io.newURI("chrome://devtools/content/framework/toolbox.xul"),
   classDescription: "about:devtools-toolbox",
   classID: Components.ID("11342911-3135-45a8-8d71-737a2b0ad469"),
   contractID: "@mozilla.org/network/protocol/about;1?what=devtools-toolbox",
 
   QueryInterface: XPCOMUtils.generateQI([nsIAboutModule]),
 
-  newChannel: function (uri, loadInfo) {
+  newChannel: function(uri, loadInfo) {
     let chan = Services.io.newChannelFromURIWithLoadInfo(this.uri, loadInfo);
     chan.owner = Services.scriptSecurityManager.getSystemPrincipal();
     return chan;
   },
 
-  getURIFlags: function (uri) {
+  getURIFlags: function(uri) {
     return nsIAboutModule.ALLOW_SCRIPT |
            nsIAboutModule.ENABLE_INDEXED_DB |
            nsIAboutModule.HIDE_FROM_ABOUTABOUT;
   }
 };
 
 this.NSGetFactory = XPCOMUtils.generateNSGetFactory([
   AboutDevtoolsToolbox
diff --git a/devtools/shim/devtools-startup.js b/devtools/shim/devtools-startup.js
--- a/devtools/shim/devtools-startup.js
+++ b/devtools/shim/devtools-startup.js
@@ -39,27 +39,27 @@ ChromeUtils.defineModuleGetter(this, "Se
                                "resource://gre/modules/Services.jsm");
 ChromeUtils.defineModuleGetter(this, "AppConstants",
                                "resource://gre/modules/AppConstants.jsm");
 ChromeUtils.defineModuleGetter(this, "CustomizableUI",
                                "resource:///modules/CustomizableUI.jsm");
 ChromeUtils.defineModuleGetter(this, "CustomizableWidgets",
                                "resource:///modules/CustomizableWidgets.jsm");
 
-XPCOMUtils.defineLazyGetter(this, "StartupBundle", function () {
+XPCOMUtils.defineLazyGetter(this, "StartupBundle", function() {
   const url = "chrome://devtools-shim/locale/startup.properties";
   return Services.strings.createBundle(url);
 });
 
-XPCOMUtils.defineLazyGetter(this, "KeyShortcutsBundle", function () {
+XPCOMUtils.defineLazyGetter(this, "KeyShortcutsBundle", function() {
   const url = "chrome://devtools-shim/locale/key-shortcuts.properties";
   return Services.strings.createBundle(url);
 });
 
-XPCOMUtils.defineLazyGetter(this, "KeyShortcuts", function () {
+XPCOMUtils.defineLazyGetter(this, "KeyShortcuts", function() {
   const isMac = AppConstants.platform == "macosx";
 
   // Common modifier shared by most key shortcuts
   const modifiers = isMac ? "accel,alt" : "accel,shift";
 
   // List of all key shortcuts triggering installation UI
   // `id` should match tool's id from client/definitions.js
   return [
@@ -189,21 +189,21 @@ DevToolsStartup.prototype = {
    */
   recorded: false,
 
   /**
    * Flag that indicates if the developer toggle was already added to customizableUI.
    */
   developerToggleCreated: false,
 
-  isDisabledByPolicy: function () {
+  isDisabledByPolicy: function() {
     return Services.prefs.getBoolPref(DEVTOOLS_POLICY_DISABLED_PREF, false);
   },
 
-  handle: function (cmdLine) {
+  handle: function(cmdLine) {
     let flags = this.readCommandLineFlags(cmdLine);
 
     // handle() can be called after browser startup (e.g. opening links from other apps).
     let isInitialLaunch = cmdLine.state == Ci.nsICommandLine.STATE_INITIAL_LAUNCH;
     if (isInitialLaunch) {
       // Execute only on first launch of this browser instance.
       let hasDevToolsFlag = flags.console || flags.devtools || flags.debugger;
       this.setupEnabledPref(hasDevToolsFlag);
@@ -561,17 +561,17 @@ DevToolsStartup.prototype = {
 
     // Bug 371900: command event is fired only if "oncommand" attribute is set.
     k.setAttribute("oncommand", ";");
     k.addEventListener("command", oncommand);
 
     return k;
   },
 
-  initDevTools: function (reason) {
+  initDevTools: function(reason) {
     // If an entry point is fired and tools are not enabled open the installation page
     if (!Services.prefs.getBoolPref(DEVTOOLS_ENABLED_PREF)) {
       this.openInstallPage(reason);
       return null;
     }
 
     if (reason && !this.recorded) {
       // Only save the first call for each firefox run as next call
@@ -599,17 +599,17 @@ DevToolsStartup.prototype = {
    *
    * @param {String} reason
    *        One of "KeyShortcut", "SystemMenu", "HamburgerMenu", "ContextMenu",
    *        "CommandLine".
    * @param {String} keyId
    *        Optional. If the onboarding flow was triggered by a keyboard shortcut, pass
    *        the shortcut key id (or toolId) to about:devtools.
    */
-  openInstallPage: function (reason, keyId) {
+  openInstallPage: function(reason, keyId) {
     // If DevTools are completely disabled, bail out here as this might be called directly
     // from other files.
     if (this.isDisabledByPolicy()) {
       return;
     }
 
     let { gBrowser } = Services.wm.getMostRecentWindow("navigator:browser");
 
@@ -645,17 +645,17 @@ DevToolsStartup.prototype = {
     if (params.length > 0) {
       url += "?" + params.join("&");
     }
 
     // Set relatedToCurrent: true to open the tab next to the current one.
     gBrowser.selectedTab = gBrowser.addTab(url, {relatedToCurrent: true});
   },
 
-  handleConsoleFlag: function (cmdLine) {
+  handleConsoleFlag: function(cmdLine) {
     let window = Services.wm.getMostRecentWindow("devtools:webconsole");
     if (!window) {
       let require = this.initDevTools("CommandLine");
       let { HUDService } = require("devtools/client/webconsole/hudservice");
       let { console } = ChromeUtils.import("resource://gre/modules/Console.jsm", {});
       HUDService.toggleBrowserConsole().catch(console.error);
     } else {
       // the Browser Console was already open
@@ -663,17 +663,17 @@ DevToolsStartup.prototype = {
     }
 
     if (cmdLine.state == Ci.nsICommandLine.STATE_REMOTE_AUTO) {
       cmdLine.preventDefault = true;
     }
   },
 
   // Open the toolbox on the selected tab once the browser starts up.
-  handleDevToolsFlag: function (window) {
+  handleDevToolsFlag: function(window) {
     const require = this.initDevTools("CommandLine");
     const {gDevTools} = require("devtools/client/framework/devtools");
     const {TargetFactory} = require("devtools/client/framework/target");
     let target = TargetFactory.forTab(window.gBrowser.selectedTab);
     gDevTools.showToolbox(target);
   },
 
   _isRemoteDebuggingEnabled() {
@@ -694,25 +694,25 @@ DevToolsStartup.prototype = {
       console.error(new Error(errorMsg));
       // Dump as well, as we're doing this from a commandline, make sure people
       // don't miss it:
       dump(errorMsg + "\n");
     }
     return remoteDebuggingEnabled;
   },
 
-  handleDebuggerFlag: function (cmdLine) {
+  handleDebuggerFlag: function(cmdLine) {
     if (!this._isRemoteDebuggingEnabled()) {
       return;
     }
 
     let devtoolsThreadResumed = false;
     let pauseOnStartup = cmdLine.handleFlag("wait-for-jsdebugger", false);
     if (pauseOnStartup) {
-      let observe = function (subject, topic, data) {
+      let observe = function(subject, topic, data) {
         devtoolsThreadResumed = true;
         Services.obs.removeObserver(observe, "devtools-thread-resumed");
       };
       Services.obs.addObserver(observe, "devtools-thread-resumed");
     }
 
     const { BrowserToolboxProcess } = ChromeUtils.import("resource://devtools/client/framework/ToolboxProcess.jsm", {});
     BrowserToolboxProcess.init();
@@ -744,17 +744,17 @@ DevToolsStartup.prototype = {
    *   Start the server on a Unix domain socket.
    *
    * --start-debugger-server ws:6789
    *   Start the WebSocket server on port 6789.
    *
    * --start-debugger-server ws:
    *   Start the WebSocket server on the default port (taken from d.d.remote-port)
    */
-  handleDebuggerServerFlag: function (cmdLine, portOrPath) {
+  handleDebuggerServerFlag: function(cmdLine, portOrPath) {
     if (!this._isRemoteDebuggingEnabled()) {
       return;
     }
 
     let webSocket = false;
     let defaultPort = Services.prefs.getIntPref("devtools.debugger.remote-port");
     if (portOrPath === true) {
       // Default to pref values if no values given on command line
@@ -825,17 +825,17 @@ DevToolsStartup.prototype = {
 
 /**
  * Singleton object that represents the JSON View in-content tool.
  * It has the same lifetime as the browser.
  */
 const JsonView = {
   initialized: false,
 
-  initialize: function () {
+  initialize: function() {
     // Prevent loading the frame script multiple times if we call this more than once.
     if (this.initialized) {
       return;
     }
     this.initialized = true;
 
     // Load JSON converter module. This converter is responsible
     // for handling 'application/json' documents and converting
@@ -853,17 +853,17 @@ const JsonView = {
   },
 
   // Message handlers for events from child processes
 
   /**
    * Save JSON to a file needs to be implemented here
    * in the parent process.
    */
-  onSave: function (message) {
+  onSave: function(message) {
     let chrome = Services.wm.getMostRecentWindow("navigator:browser");
     let browser = chrome.gBrowser.selectedBrowser;
     if (message.data === null) {
       // Save original contents
       chrome.saveBrowser(browser);
     } else {
       // The following code emulates saveBrowser, but:
       // - Uses the given blob URL containing the custom contents to save.
diff --git a/devtools/shim/tests/browser/browser_shim_disable_devtools.js b/devtools/shim/tests/browser/browser_shim_disable_devtools.js
--- a/devtools/shim/tests/browser/browser_shim_disable_devtools.js
+++ b/devtools/shim/tests/browser/browser_shim_disable_devtools.js
@@ -9,17 +9,17 @@
 const { require } = ChromeUtils.import("resource://devtools/shared/Loader.jsm", {});
 const { CustomizableUI } = ChromeUtils.import("resource:///modules/CustomizableUI.jsm", {});
 const { AppConstants } = require("resource://gre/modules/AppConstants.jsm");
 const { gDevTools } = require("devtools/client/framework/devtools");
 
 /**
  * Test that the preference devtools.policy.disabled disables entry points for devtools.
  */
-add_task(async function () {
+add_task(async function() {
   info("Disable DevTools entry points (does not apply to the already created window");
   await new Promise(resolve => {
     let options = {"set": [["devtools.policy.disabled", true]]};
     SpecialPowers.pushPrefEnv(options, resolve);
   });
 
   // In DEV_EDITION the browser starts with the developer-button in the toolbar. This
   // applies to all new windows and forces creating keyboard shortcuts. The preference
diff --git a/devtools/shim/tests/unit/test_devtools_shim.js b/devtools/shim/tests/unit/test_devtools_shim.js
--- a/devtools/shim/tests/unit/test_devtools_shim.js
+++ b/devtools/shim/tests/unit/test_devtools_shim.js
@@ -28,17 +28,17 @@ function createMockDevTools() {
   ];
 
   let mock = {
     callLog: {}
   };
 
   for (let method of methods) {
     // Create a stub for method, that only pushes its arguments in the inner callLog
-    mock[method] = function (...args) {
+    mock[method] = function(...args) {
       mock.callLog[method].push(args);
     };
     mock.callLog[method] = [];
   }
 
   return mock;
 }
 
