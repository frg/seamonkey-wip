# HG changeset patch
# User Anthony Ramine <n.oxyde@gmail.com>
# Date 1518090082 18000
# Node ID 9bfcc4cc8945287fe1cdfbdb15c5b3785b26e9d1
# Parent  26c620380026f892eaaa3e19fb37fd0a71f124e2
servo: Merge #19985 - Derive ComputeSquaredDistance for animated filters (from servo:derive-these-things-too); r=emilio

Source-Repo: https://github.com/servo/servo
Source-Revision: c22baf58d06d19a9347993698fd70f9a4513bcb2

diff --git a/servo/components/style/properties/helpers/animated_properties.mako.rs b/servo/components/style/properties/helpers/animated_properties.mako.rs
--- a/servo/components/style/properties/helpers/animated_properties.mako.rs
+++ b/servo/components/style/properties/helpers/animated_properties.mako.rs
@@ -2866,35 +2866,16 @@ impl ToAnimatedZero for AnimatedFilter {
             % if product == "gecko":
             Filter::DropShadow(ref this) => Ok(Filter::DropShadow(this.to_animated_zero()?)),
             % endif
             _ => Err(()),
         }
     }
 }
 
-// FIXME(nox): This should be derived.
-impl ComputeSquaredDistance for AnimatedFilter {
-    fn compute_squared_distance(&self, other: &Self) -> Result<SquaredDistance, ()> {
-        match (self, other) {
-            % for func in FILTER_FUNCTIONS:
-            (&Filter::${func}(ref this), &Filter::${func}(ref other)) => {
-                this.compute_squared_distance(other)
-            },
-            % endfor
-            % if product == "gecko":
-            (&Filter::DropShadow(ref this), &Filter::DropShadow(ref other)) => {
-                this.compute_squared_distance(other)
-            },
-            % endif
-            _ => Err(()),
-        }
-    }
-}
-
 impl Animate for AnimatedFilterList {
     #[inline]
     fn animate(
         &self,
         other: &Self,
         procedure: Procedure,
     ) -> Result<Self, ()> {
         if procedure == Procedure::Add {
diff --git a/servo/components/style/values/generics/effects.rs b/servo/components/style/values/generics/effects.rs
--- a/servo/components/style/values/generics/effects.rs
+++ b/servo/components/style/values/generics/effects.rs
@@ -19,17 +19,18 @@ pub struct BoxShadow<Color, SizeLength, 
     pub spread: ShapeLength,
     /// Whether this is an inset box shadow.
     #[animation(constant)]
     pub inset: bool,
 }
 
 /// A generic value for a single `filter`.
 #[cfg_attr(feature = "servo", derive(Deserialize, Serialize))]
-#[derive(Clone, Debug, MallocSizeOf, PartialEq, ToAnimatedValue, ToComputedValue, ToCss)]
+#[derive(Clone, ComputeSquaredDistance, Debug, MallocSizeOf, PartialEq)]
+#[derive(ToAnimatedValue, ToComputedValue, ToCss)]
 pub enum Filter<Angle, Factor, Length, DropShadow> {
     /// `blur(<length>)`
     #[css(function)]
     Blur(Length),
     /// `brightness(<factor>)`
     #[css(function)]
     Brightness(Factor),
     /// `contrast(<factor>)`
@@ -52,16 +53,17 @@ pub enum Filter<Angle, Factor, Length, D
     Saturate(Factor),
     /// `sepia(<factor>)`
     #[css(function)]
     Sepia(Factor),
     /// `drop-shadow(...)`
     #[css(function)]
     DropShadow(DropShadow),
     /// `<url>`
+    #[animation(error)]
     #[cfg(feature = "gecko")]
     Url(SpecifiedUrl),
 }
 
 /// A generic value for the `drop-shadow()` filter and the `text-shadow` property.
 ///
 /// Contrary to the canonical order from the spec, the color is serialised
 /// first, like in Gecko and Webkit.
diff --git a/servo/components/style/values/mod.rs b/servo/components/style/values/mod.rs
--- a/servo/components/style/values/mod.rs
+++ b/servo/components/style/values/mod.rs
@@ -11,16 +11,17 @@
 use Atom;
 pub use cssparser::{RGBA, Token, Parser, serialize_identifier, CowRcStr, SourceLocation};
 use parser::{Parse, ParserContext};
 use selectors::parser::SelectorParseErrorKind;
 #[allow(unused_imports)] use std::ascii::AsciiExt;
 use std::fmt::{self, Debug, Write};
 use std::hash;
 use style_traits::{CssWriter, ParseError, StyleParseErrorKind, ToCss};
+use values::distance::{ComputeSquaredDistance, SquaredDistance};
 
 pub mod animated;
 pub mod computed;
 pub mod distance;
 pub mod generics;
 pub mod specified;
 
 /// A CSS float value.
@@ -61,16 +62,28 @@ where
     dest.write_str("%")
 }
 
 /// Convenience void type to disable some properties and values through types.
 #[cfg_attr(feature = "servo", derive(Deserialize, MallocSizeOf, Serialize))]
 #[derive(Clone, Copy, Debug, PartialEq, ToComputedValue, ToCss)]
 pub enum Impossible {}
 
+// FIXME(nox): This should be derived but the derive code cannot cope
+// with uninhabited enums.
+impl ComputeSquaredDistance for Impossible {
+    #[inline]
+    fn compute_squared_distance(
+        &self,
+        _other: &Self,
+    ) -> Result<SquaredDistance, ()> {
+        match *self {}
+    }
+}
+
 impl Parse for Impossible {
     fn parse<'i, 't>(
         _context: &ParserContext,
         input: &mut Parser<'i, 't>)
     -> Result<Self, ParseError<'i>> {
         Err(input.new_custom_error(StyleParseErrorKind::UnspecifiedError))
     }
 }
