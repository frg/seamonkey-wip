# HG changeset patch
# User Ben Kelly <ben@wanderview.com>
# Date 1511889040 18000
# Node ID 2319e3c47580aadcecdb404cc490752f75da744c
# Parent  de34b5d2a9760c73a12615abf8a34991146bde7b
Bug 1420743 P1 Do a better job of clearing docshell's mInitialClientSource at the end of page load. r=baku

diff --git a/docshell/base/nsDocShell.cpp b/docshell/base/nsDocShell.cpp
--- a/docshell/base/nsDocShell.cpp
+++ b/docshell/base/nsDocShell.cpp
@@ -7734,16 +7734,21 @@ nsDocShell::OnSecurityChange(nsIWebProgr
 nsresult
 nsDocShell::EndPageLoad(nsIWebProgress* aProgress,
                         nsIChannel* aChannel, nsresult aStatus)
 {
   if (!aChannel) {
     return NS_ERROR_NULL_POINTER;
   }
 
+  // Make sure to discard the initial client if we never created the initial
+  // about:blank document.  Do this before possibly returning from the method
+  // due to an error.
+  mInitialClientSource.reset();
+
   nsCOMPtr<nsIConsoleReportCollector> reporter = do_QueryInterface(aChannel);
   if (reporter) {
     nsCOMPtr<nsILoadGroup> loadGroup;
     aChannel->GetLoadGroup(getter_AddRefs(loadGroup));
     if (loadGroup) {
       reporter->FlushConsoleReports(loadGroup);
     } else {
       reporter->FlushConsoleReports(GetDocument());
@@ -7769,20 +7774,16 @@ nsDocShell::EndPageLoad(nsIWebProgress* 
         internalLoadGroup->OnEndPageLoad(aChannel);
       }
     }
   }
 
   // Timing is picked up by the window, we don't need it anymore
   mTiming = nullptr;
 
-  // Make sure to discard the initial client if we never created the initial
-  // about:blank document.
-  mInitialClientSource.reset();
-
   // clean up reload state for meta charset
   if (eCharsetReloadRequested == mCharsetReloadState) {
     mCharsetReloadState = eCharsetReloadStopOrigional;
   } else {
     mCharsetReloadState = eCharsetReloadInit;
   }
 
   // Save a pointer to the currently-loading history entry.
@@ -11723,16 +11724,21 @@ nsDocShell::DoChannelLoad(nsIChannel* aC
   uint32_t openFlags = 0;
   if (mLoadType == LOAD_LINK) {
     openFlags |= nsIURILoader::IS_CONTENT_PREFERRED;
   }
   if (!mAllowContentRetargeting) {
     openFlags |= nsIURILoader::DONT_RETARGET;
   }
 
+  // If anything fails here, make sure to clear our initial ClientSource.
+  auto cleanupInitialClient = MakeScopeExit([&] {
+    mInitialClientSource.reset();
+  });
+
   nsCOMPtr<nsPIDOMWindowOuter> win = GetWindow();
   NS_ENSURE_TRUE(win, NS_ERROR_FAILURE);
 
   MaybeCreateInitialClientSource();
 
   // Since we are loading a document we need to make sure the proper reserved
   // and initial client data is stored on the nsILoadInfo.  The
   // ClientChannelHelper does this and ensures that it is propagated properly
@@ -11748,16 +11754,19 @@ nsDocShell::DoChannelLoad(nsIChannel* aC
   rv = aURILoader->OpenURI(aChannel, openFlags, this);
   NS_ENSURE_SUCCESS(rv, rv);
 
   // We're about to load a new page and it may take time before necko
   // gives back any data, so main thread might have a chance to process a
   // collector slice
   nsJSContext::MaybeRunNextCollectorSlice(this, JS::gcreason::DOCSHELL);
 
+  // Success.  Keep the initial ClientSource if it exists.
+  cleanupInitialClient.release();
+
   return NS_OK;
 }
 
 nsresult
 nsDocShell::ScrollToAnchor(bool aCurHasRef, bool aNewHasRef,
                            nsACString& aNewHash, uint32_t aLoadType)
 {
   if (!mCurrentURI) {
diff --git a/dom/base/nsGlobalWindow.cpp b/dom/base/nsGlobalWindow.cpp
--- a/dom/base/nsGlobalWindow.cpp
+++ b/dom/base/nsGlobalWindow.cpp
@@ -3445,16 +3445,24 @@ nsGlobalWindow::EnsureClientSource()
 {
   MOZ_DIAGNOSTIC_ASSERT(mDoc);
 
   bool newClientSource = false;
 
   nsCOMPtr<nsIChannel> channel = mDoc->GetChannel();
   nsCOMPtr<nsILoadInfo> loadInfo = channel ? channel->GetLoadInfo() : nullptr;
 
+  // Take the initial client source from the docshell immediately.  Even if we
+  // don't end up using it here we should consume it.
+  UniquePtr<ClientSource> initialClientSource;
+  nsIDocShell* docshell = GetDocShell();
+  if (docshell) {
+    initialClientSource = docshell->TakeInitialClientSource();
+  }
+
   // Try to get the reserved client from the LoadInfo.  A Client is
   // reserved at the start of the channel load if there is not an
   // initial about:blank document that will be reused.  It is also
   // created if the channel load encounters a cross-origin redirect.
   if (loadInfo) {
     UniquePtr<ClientSource> reservedClient = loadInfo->TakeReservedClientSource();
     if (reservedClient) {
       mClientSource.reset();
@@ -3465,22 +3473,19 @@ nsGlobalWindow::EnsureClientSource()
 
   // We don't have a LoadInfo reserved client, but maybe we should
   // be inheriting an initial one from the docshell.  This means
   // that the docshell started the channel load before creating the
   // initial about:blank document.  This is an optimization, though,
   // and it created an initial Client as a placeholder for the document.
   // In this case we want to inherit this placeholder Client here.
   if (!mClientSource) {
-    nsIDocShell* docshell = GetDocShell();
-    if (docshell) {
-      mClientSource = docshell->TakeInitialClientSource();
-      if (mClientSource) {
-        newClientSource = true;
-      }
+    mClientSource = Move(initialClientSource);
+    if (mClientSource) {
+      newClientSource = true;
     }
   }
  
   // If we don't have a reserved client or an initial client, then create
   // one now.  This can happen in certain cases where we avoid preallocating
   // the client in the docshell.  This mainly occurs in situations where
   // the principal is not clearly inherited from the parent; e.g. sandboxed
   // iframes, window.open(), etc.
