# HG changeset patch
# User Masayuki Nakano <masayuki@d-toybox.com>
# Date 1516892360 -32400
# Node ID 34abf97931631c9d4504f25ac0ce7f0e509aa62f
# Parent  c4c15d3c29dfdfcf738a9928523ff05d43050487
Bug 1433101 - part 2: Treat Enter and Shift+Enter as printable key r=smaug

Chromium dispatches a keypress event when pressing Enter or Shift+Enter.
Actually, when user press them in <pre> element of HTML editor, ﾂ･n is inserted.
It makes sense to treat the key combinations as inputting text.

MozReview-Commit-ID: Hvx87MZtZkn

diff --git a/editor/libeditor/HTMLEditor.cpp b/editor/libeditor/HTMLEditor.cpp
--- a/editor/libeditor/HTMLEditor.cpp
+++ b/editor/libeditor/HTMLEditor.cpp
@@ -662,18 +662,17 @@ HTMLEditor::HandleKeyPressEvent(WidgetKe
       }
       if (aKeyboardEvent->IsShift()) {
         return NS_OK; // don't type text for shift tabs
       }
       aKeyboardEvent->PreventDefault();
       return TypedText(NS_LITERAL_STRING("\t"), eTypedText);
     }
     case NS_VK_RETURN:
-      if (aKeyboardEvent->IsControl() || aKeyboardEvent->IsAlt() ||
-          aKeyboardEvent->IsMeta() || aKeyboardEvent->IsOS()) {
+      if (!aKeyboardEvent->IsInputtingLineBreak()) {
         return NS_OK;
       }
       aKeyboardEvent->PreventDefault(); // consumed
       if (aKeyboardEvent->IsShift()) {
         // only inserts a br node
         return TypedText(EmptyString(), eTypedBR);
       }
       // uses rules to figure out what to insert
diff --git a/editor/libeditor/TextEditor.cpp b/editor/libeditor/TextEditor.cpp
--- a/editor/libeditor/TextEditor.cpp
+++ b/editor/libeditor/TextEditor.cpp
@@ -368,19 +368,17 @@ TextEditor::HandleKeyPressEvent(WidgetKe
         return NS_OK;
       }
 
       // else we insert the tab straight through
       aKeyboardEvent->PreventDefault();
       return TypedText(NS_LITERAL_STRING("\t"), eTypedText);
     }
     case NS_VK_RETURN:
-      if (IsSingleLineEditor() || aKeyboardEvent->IsControl() ||
-          aKeyboardEvent->IsAlt() || aKeyboardEvent->IsMeta() ||
-          aKeyboardEvent->IsOS()) {
+      if (IsSingleLineEditor() || !aKeyboardEvent->IsInputtingLineBreak()) {
         return NS_OK;
       }
       aKeyboardEvent->PreventDefault();
       return TypedText(EmptyString(), eTypedBreak);
   }
 
   if (!aKeyboardEvent->IsInputtingText()) {
     // we don't PreventDefault() here or keybindings like control-x won't work
diff --git a/widget/TextEventDispatcher.cpp b/widget/TextEventDispatcher.cpp
--- a/widget/TextEventDispatcher.cpp
+++ b/widget/TextEventDispatcher.cpp
@@ -686,17 +686,17 @@ TextEventDispatcher::DispatchKeyboardEve
                    static_cast<WidgetKeyboardEvent&>(original).mKeyValue);
       MOZ_ASSERT(keyEvent.mCodeValue ==
                    static_cast<WidgetKeyboardEvent&>(original).mCodeValue);
     }
   }
 
   if (sDispatchKeyPressEventsOnlySystemGroupInContent &&
       keyEvent.mMessage == eKeyPress &&
-      !keyEvent.IsInputtingText()) {
+      !keyEvent.IsInputtingText() && !keyEvent.IsInputtingLineBreak()) {
     keyEvent.mFlags.mOnlySystemGroupDispatchInContent = true;
   }
 
   DispatchInputEvent(mWidget, keyEvent, aStatus);
   return true;
 }
 
 bool
diff --git a/widget/TextEvents.h b/widget/TextEvents.h
--- a/widget/TextEvents.h
+++ b/widget/TextEvents.h
@@ -196,16 +196,26 @@ public:
     return mMessage == eKeyPress &&
            mCharCode &&
            !(mModifiers & (MODIFIER_ALT |
                            MODIFIER_CONTROL |
                            MODIFIER_META |
                            MODIFIER_OS));
   }
 
+  bool IsInputtingLineBreak() const
+  {
+    return mMessage == eKeyPress &&
+           mKeyNameIndex == KEY_NAME_INDEX_Enter &&
+           !(mModifiers & (MODIFIER_ALT |
+                           MODIFIER_CONTROL |
+                           MODIFIER_META |
+                           MODIFIER_OS));
+  }
+
   virtual WidgetEvent* Duplicate() const override
   {
     MOZ_ASSERT(mClass == eKeyboardEventClass,
                "Duplicate() must be overridden by sub class");
     // Not copying widget, it is a weak reference.
     WidgetKeyboardEvent* result =
       new WidgetKeyboardEvent(false, mMessage, nullptr);
     result->AssignKeyEventData(*this, true);
