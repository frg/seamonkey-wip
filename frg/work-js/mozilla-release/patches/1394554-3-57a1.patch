# HG changeset patch
# User Christoph Kerschbaumer <ckerschb@christophkerschbaumer.com>
# Date 1504683299 -7200
# Node ID d80fa4d123620fd099db9b0e790e5d5daed24590
# Parent  d6c69f7a3471b84e296132d28727248af9e66167
Bug 1394554: Test block data: URI toplevel navigations after redirect. r=smaug

diff --git a/dom/security/test/general/browser.ini b/dom/security/test/general/browser.ini
--- a/dom/security/test/general/browser.ini
+++ b/dom/security/test/general/browser.ini
@@ -1,2 +1,5 @@
 [DEFAULT]
 [browser_test_toplevel_data_navigations.js]
+support-files =
+  file_toplevel_data_navigations.sjs
+  file_toplevel_data_meta_redirect.html
diff --git a/dom/security/test/general/browser_test_toplevel_data_navigations.js b/dom/security/test/general/browser_test_toplevel_data_navigations.js
--- a/dom/security/test/general/browser_test_toplevel_data_navigations.js
+++ b/dom/security/test/general/browser_test_toplevel_data_navigations.js
@@ -1,16 +1,54 @@
+/* eslint-disable mozilla/no-arbitrary-setTimeout */
+
 "use strict";
 
 const kDataBody = "toplevel navigation to data: URI allowed";
 const kDataURI = "data:text/html,<body>" + kDataBody + "</body>";
+const kTestPath = getRootDirectory(gTestPath)
+                  .replace("chrome://mochitests/content", "http://example.com")
+const kRedirectURI = kTestPath + "file_toplevel_data_navigations.sjs";
+const kMetaRedirectURI = kTestPath + "file_toplevel_data_meta_redirect.html";
 
-add_task(async function test_nav_data_uri_click() {
+add_task(async function test_nav_data_uri() {
   await SpecialPowers.pushPrefEnv({
     "set": [["security.data_uri.block_toplevel_data_uri_navigations", true]],
   });
   await BrowserTestUtils.withNewTab(kDataURI, async function(browser) {
     await ContentTask.spawn(gBrowser.selectedBrowser, {kDataBody}, async function({kDataBody}) { // eslint-disable-line
      is(content.document.body.innerHTML, kDataBody,
         "data: URI navigation from system should be allowed");
     });
   });
 });
+
+add_task(async function test_nav_data_uri_redirect() {
+  await SpecialPowers.pushPrefEnv({
+    "set": [["security.data_uri.block_toplevel_data_uri_navigations", true]],
+  });
+  let tab = BrowserTestUtils.addTab(gBrowser, kRedirectURI);
+  registerCleanupFunction(async function() {
+    await BrowserTestUtils.removeTab(tab);
+  });
+  // wait to make sure data: URI did not load before checking that it got blocked
+  await new Promise(resolve => setTimeout(resolve, 500));
+  await ContentTask.spawn(gBrowser.selectedBrowser, {}, async function() {
+    is(content.document.body.innerHTML, "",
+       "data: URI navigation after server redirect should be blocked");
+  });
+});
+
+add_task(async function test_nav_data_uri_meta_redirect() {
+  await SpecialPowers.pushPrefEnv({
+    "set": [["security.data_uri.block_toplevel_data_uri_navigations", true]],
+  });
+  let tab = BrowserTestUtils.addTab(gBrowser, kMetaRedirectURI);
+  registerCleanupFunction(async function() {
+    await BrowserTestUtils.removeTab(tab);
+  });
+  // wait to make sure data: URI did not load before checking that it got blocked
+  await new Promise(resolve => setTimeout(resolve, 500));
+  await ContentTask.spawn(gBrowser.selectedBrowser, {}, async function() {
+    is(content.document.body.innerHTML, "",
+       "data: URI navigation after meta redirect should be blocked");
+  });
+});
diff --git a/dom/security/test/general/file_toplevel_data_meta_redirect.html b/dom/security/test/general/file_toplevel_data_meta_redirect.html
new file mode 100644
--- /dev/null
+++ b/dom/security/test/general/file_toplevel_data_meta_redirect.html
@@ -0,0 +1,10 @@
+<html>
+<body>
+<head>
+  <meta http-equiv="refresh"
+        content="0; url='data:text/html,<body>toplevel meta redirect to data: URI should be blocked</body>'">
+</head>
+<body>
+Meta Redirect to data: URI
+</body>
+</html>
diff --git a/dom/security/test/general/file_toplevel_data_navigations.sjs b/dom/security/test/general/file_toplevel_data_navigations.sjs
new file mode 100644
--- /dev/null
+++ b/dom/security/test/general/file_toplevel_data_navigations.sjs
@@ -0,0 +1,14 @@
+// Custom *.sjs file specifically for the needs of Bug:
+// Bug 1394554 - Block toplevel data: URI navigations after redirect
+
+var DATA_URI =
+  "data:text/html,<body>toplevel data: URI navigations after redirect should be blocked</body>";
+
+function handleRequest(request, response)
+{
+  // avoid confusing cache behaviors
+  response.setHeader("Cache-Control", "no-cache", false);
+
+  response.setStatusLine("1.1", 302, "Found");
+  response.setHeader("Location", DATA_URI, false);
+}
