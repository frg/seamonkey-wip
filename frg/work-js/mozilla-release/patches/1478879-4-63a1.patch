# HG changeset patch
# User Nicholas Nethercote <nnethercote@mozilla.com>
# Date 1532657930 -36000
#      Fri Jul 27 12:18:50 2018 +1000
# Node ID 3bd2221b034d4a57fd97df85fc64a60c5ba3478b
# Parent  2c3b5f4eda625adf917db7402bc1d655d1c1e3bc
Bug 1478879 - Remove Range/Enum use in testHashTable.cpp. r=luke

diff --git a/js/src/jsapi-tests/testHashTable.cpp b/js/src/jsapi-tests/testHashTable.cpp
--- a/js/src/jsapi-tests/testHashTable.cpp
+++ b/js/src/jsapi-tests/testHashTable.cpp
@@ -59,49 +59,49 @@ struct LowToHighWithRemoval
 static bool
 MapsAreEqual(IntMap& am, IntMap& bm)
 {
     bool equal = true;
     if (am.count() != bm.count()) {
         equal = false;
         fprintf(stderr, "A.count() == %u and B.count() == %u\n", am.count(), bm.count());
     }
-    for (IntMap::Range r = am.all(); !r.empty(); r.popFront()) {
-        if (!bm.has(r.front().key())) {
+    for (auto iter = am.iter(); !iter.done(); iter.next()) {
+        if (!bm.has(iter.get().key())) {
             equal = false;
-            fprintf(stderr, "B does not have %x which is in A\n", r.front().key());
+            fprintf(stderr, "B does not have %x which is in A\n", iter.get().key());
         }
     }
-    for (IntMap::Range r = bm.all(); !r.empty(); r.popFront()) {
-        if (!am.has(r.front().key())) {
+    for (auto iter = bm.iter(); !iter.done(); iter.next()) {
+        if (!am.has(iter.get().key())) {
             equal = false;
-            fprintf(stderr, "A does not have %x which is in B\n", r.front().key());
+            fprintf(stderr, "A does not have %x which is in B\n", iter.get().key());
         }
     }
     return equal;
 }
 
 static bool
 SetsAreEqual(IntSet& am, IntSet& bm)
 {
     bool equal = true;
     if (am.count() != bm.count()) {
         equal = false;
         fprintf(stderr, "A.count() == %u and B.count() == %u\n", am.count(), bm.count());
     }
-    for (IntSet::Range r = am.all(); !r.empty(); r.popFront()) {
-        if (!bm.has(r.front())) {
+    for (auto iter = am.iter(); !iter.done(); iter.next()) {
+        if (!bm.has(iter.get())) {
             equal = false;
-            fprintf(stderr, "B does not have %x which is in A\n", r.front());
+            fprintf(stderr, "B does not have %x which is in A\n", iter.get());
         }
     }
-    for (IntSet::Range r = bm.all(); !r.empty(); r.popFront()) {
-        if (!am.has(r.front())) {
+    for (auto iter = bm.iter(); !iter.done(); iter.next()) {
+        if (!am.has(iter.get())) {
             equal = false;
-            fprintf(stderr, "A does not have %x which is in B\n", r.front());
+            fprintf(stderr, "A does not have %x which is in B\n", iter.get());
         }
     }
     return equal;
 }
 
 static bool
 AddLowKeys(IntMap* am, IntMap* bm, int seed)
 {
@@ -141,55 +141,55 @@ AddLowKeys(IntSet* as, IntSet* bs, int s
 
 template <class NewKeyFunction>
 static bool
 SlowRekey(IntMap* m) {
     IntMap tmp;
     if (!tmp.init())
         return false;
 
-    for (IntMap::Range r = m->all(); !r.empty(); r.popFront()) {
-        if (NewKeyFunction::shouldBeRemoved(r.front().key()))
+    for (auto iter = m->iter(); !iter.done(); iter.next()) {
+        if (NewKeyFunction::shouldBeRemoved(iter.get().key()))
             continue;
-        uint32_t hi = NewKeyFunction::rekey(r.front().key());
+        uint32_t hi = NewKeyFunction::rekey(iter.get().key());
         if (tmp.has(hi))
             return false;
-        if (!tmp.putNew(hi, r.front().value()))
+        if (!tmp.putNew(hi, iter.get().value()))
             return false;
     }
 
     m->clear();
-    for (IntMap::Range r = tmp.all(); !r.empty(); r.popFront()) {
-        if (!m->putNew(r.front().key(), r.front().value()))
+    for (auto iter = tmp.iter(); !iter.done(); iter.next()) {
+        if (!m->putNew(iter.get().key(), iter.get().value()))
             return false;
     }
 
     return true;
 }
 
 template <class NewKeyFunction>
 static bool
 SlowRekey(IntSet* s) {
     IntSet tmp;
     if (!tmp.init())
         return false;
 
-    for (IntSet::Range r = s->all(); !r.empty(); r.popFront()) {
-        if (NewKeyFunction::shouldBeRemoved(r.front()))
+    for (auto iter = s->iter(); !iter.done(); iter.next()) {
+        if (NewKeyFunction::shouldBeRemoved(iter.get()))
             continue;
-        uint32_t hi = NewKeyFunction::rekey(r.front());
+        uint32_t hi = NewKeyFunction::rekey(iter.get());
         if (tmp.has(hi))
             return false;
         if (!tmp.putNew(hi))
             return false;
     }
 
     s->clear();
-    for (IntSet::Range r = tmp.all(); !r.empty(); r.popFront()) {
-        if (!s->putNew(r.front()))
+    for (auto iter = tmp.iter(); !iter.done(); iter.next()) {
+        if (!s->putNew(iter.get()))
             return false;
     }
 
     return true;
 }
 
 BEGIN_TEST(testHashRekeyManual)
 {
@@ -198,20 +198,20 @@ BEGIN_TEST(testHashRekeyManual)
     CHECK(bm.init());
     for (size_t i = 0; i < TestIterations; ++i) {
 #ifdef FUZZ
         fprintf(stderr, "map1: %lu\n", i);
 #endif
         CHECK(AddLowKeys(&am, &bm, i));
         CHECK(MapsAreEqual(am, bm));
 
-        for (IntMap::Enum e(am); !e.empty(); e.popFront()) {
-            uint32_t tmp = LowToHigh::rekey(e.front().key());
-            if (tmp != e.front().key())
-                e.rekeyFront(tmp);
+        for (auto iter = am.modIter(); !iter.done(); iter.next()) {
+            uint32_t tmp = LowToHigh::rekey(iter.get().key());
+            if (tmp != iter.get().key())
+                iter.rekey(tmp);
         }
         CHECK(SlowRekey<LowToHigh>(&bm));
 
         CHECK(MapsAreEqual(am, bm));
         am.clear();
         bm.clear();
     }
 
@@ -220,20 +220,20 @@ BEGIN_TEST(testHashRekeyManual)
     CHECK(bs.init());
     for (size_t i = 0; i < TestIterations; ++i) {
 #ifdef FUZZ
         fprintf(stderr, "set1: %lu\n", i);
 #endif
         CHECK(AddLowKeys(&as, &bs, i));
         CHECK(SetsAreEqual(as, bs));
 
-        for (IntSet::Enum e(as); !e.empty(); e.popFront()) {
-            uint32_t tmp = LowToHigh::rekey(e.front());
-            if (tmp != e.front())
-                e.rekeyFront(tmp);
+        for (auto iter = as.modIter(); !iter.done(); iter.next()) {
+            uint32_t tmp = LowToHigh::rekey(iter.get());
+            if (tmp != iter.get())
+                iter.rekey(tmp);
         }
         CHECK(SlowRekey<LowToHigh>(&bs));
 
         CHECK(SetsAreEqual(as, bs));
         as.clear();
         bs.clear();
     }
 
@@ -248,23 +248,23 @@ BEGIN_TEST(testHashRekeyManualRemoval)
     CHECK(bm.init());
     for (size_t i = 0; i < TestIterations; ++i) {
 #ifdef FUZZ
         fprintf(stderr, "map2: %lu\n", i);
 #endif
         CHECK(AddLowKeys(&am, &bm, i));
         CHECK(MapsAreEqual(am, bm));
 
-        for (IntMap::Enum e(am); !e.empty(); e.popFront()) {
-            if (LowToHighWithRemoval::shouldBeRemoved(e.front().key())) {
-                e.removeFront();
+        for (auto iter = am.modIter(); !iter.done(); iter.next()) {
+            if (LowToHighWithRemoval::shouldBeRemoved(iter.get().key())) {
+                iter.remove();
             } else {
-                uint32_t tmp = LowToHighWithRemoval::rekey(e.front().key());
-                if (tmp != e.front().key())
-                    e.rekeyFront(tmp);
+                uint32_t tmp = LowToHighWithRemoval::rekey(iter.get().key());
+                if (tmp != iter.get().key())
+                    iter.rekey(tmp);
             }
         }
         CHECK(SlowRekey<LowToHighWithRemoval>(&bm));
 
         CHECK(MapsAreEqual(am, bm));
         am.clear();
         bm.clear();
     }
@@ -274,23 +274,23 @@ BEGIN_TEST(testHashRekeyManualRemoval)
     CHECK(bs.init());
     for (size_t i = 0; i < TestIterations; ++i) {
 #ifdef FUZZ
         fprintf(stderr, "set1: %lu\n", i);
 #endif
         CHECK(AddLowKeys(&as, &bs, i));
         CHECK(SetsAreEqual(as, bs));
 
-        for (IntSet::Enum e(as); !e.empty(); e.popFront()) {
-            if (LowToHighWithRemoval::shouldBeRemoved(e.front())) {
-                e.removeFront();
+        for (auto iter = as.modIter(); !iter.done(); iter.next()) {
+            if (LowToHighWithRemoval::shouldBeRemoved(iter.get())) {
+                iter.remove();
             } else {
-                uint32_t tmp = LowToHighWithRemoval::rekey(e.front());
-                if (tmp != e.front())
-                    e.rekeyFront(tmp);
+                uint32_t tmp = LowToHighWithRemoval::rekey(iter.get());
+                if (tmp != iter.get())
+                    iter.rekey(tmp);
             }
         }
         CHECK(SlowRekey<LowToHighWithRemoval>(&bs));
 
         CHECK(SetsAreEqual(as, bs));
         as.clear();
         bs.clear();
     }
@@ -387,48 +387,48 @@ BEGIN_TEST(testHashMapLookupWithDefaultO
 
     js::oom::ResetSimulatedOOM();
     return true;
 }
 
 END_TEST(testHashMapLookupWithDefaultOOM)
 #endif // defined(DEBUG)
 
-BEGIN_TEST(testHashTableMovableEnum)
+BEGIN_TEST(testHashTableMovableModIterator)
 {
     IntSet set;
     CHECK(set.init());
 
-    // Exercise returning a hash table Enum object from a function.
+    // Exercise returning a hash table ModIterator object from a function.
 
     CHECK(set.put(1));
-    for (auto e = enumerateSet(set); !e.empty(); e.popFront())
-        e.removeFront();
+    for (auto iter = setModIter(set); !iter.done(); iter.next())
+        iter.remove();
     CHECK(set.count() == 0);
 
-    // Test moving an Enum object explicitly.
+    // Test moving an ModIterator object explicitly.
 
     CHECK(set.put(1));
     CHECK(set.put(2));
     CHECK(set.put(3));
     CHECK(set.count() == 3);
     {
-        auto e1 = IntSet::Enum(set);
-        CHECK(!e1.empty());
-        e1.removeFront();
-        e1.popFront();
+        auto i1 = set.modIter();
+        CHECK(!i1.done());
+        i1.remove();
+        i1.next();
 
-        auto e2 = std::move(e1);
-        CHECK(!e2.empty());
-        e2.removeFront();
-        e2.popFront();
+        auto i2 = std::move(i1);
+        CHECK(!i2.done());
+        i2.remove();
+        i2.next();
     }
 
     CHECK(set.count() == 1);
     return true;
 }
 
-IntSet::Enum enumerateSet(IntSet& set)
+IntSet::ModIterator setModIter(IntSet& set)
 {
-    return IntSet::Enum(set);
+    return set.modIter();
 }
 
-END_TEST(testHashTableMovableEnum)
+END_TEST(testHashTableMovableModIterator)
