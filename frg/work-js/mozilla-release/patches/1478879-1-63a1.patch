# HG changeset patch
# User Nicholas Nethercote <nnethercote@mozilla.com>
# Date 1532996583 -36000
#      Tue Jul 31 10:23:03 2018 +1000
# Node ID 591d08c5c22b1286579a216f0dc2c462cb5679fc
# Parent  b50724cff788dfdea5f595c7ddc213d4491555c0
Bug 1478879 - Remove zero-arg constructor for Range. r=luke

It's only used by InlineTable::Range, and can be avoided by using
mozilla::Maybe.

This also means Range::mTable can be changed from a pointer to a reference,
like Enum::mTable.

diff --git a/js/src/ds/InlineTable.h b/js/src/ds/InlineTable.h
--- a/js/src/ds/InlineTable.h
+++ b/js/src/ds/InlineTable.h
@@ -2,16 +2,17 @@
  * vim: set ts=8 sts=4 et sw=4 tw=99:
  * This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef ds_InlineTable_h
 #define ds_InlineTable_h
 
+#include "mozilla/Maybe.h"
 #include "mozilla/Move.h"
 
 #include "js/AllocPolicy.h"
 #include "js/HashTable.h"
 
 namespace js {
 
 namespace detail {
@@ -338,32 +339,33 @@ class InlineTable : private AllocPolicy
             remove(p);
     }
 
     class Range
     {
         friend class InlineTable;
 
       protected:
-        TableRange   tableRange_;
+        mozilla::Maybe<TableRange> tableRange_; // `Nothing` if `isInline_==true`
         InlineEntry* cur_;
         InlineEntry* end_;
         bool         isInline_;
 
         explicit Range(TableRange r)
-          : cur_(nullptr),
+          : tableRange_(mozilla::Some(r)),
+            cur_(nullptr),
             end_(nullptr),
             isInline_(false)
         {
-            tableRange_ = r;
             MOZ_ASSERT(!isInlineRange());
         }
 
         Range(const InlineEntry* begin, const InlineEntry* end)
-          : cur_(const_cast<InlineEntry*>(begin)),
+          : tableRange_(mozilla::Nothing()),
+            cur_(const_cast<InlineEntry*>(begin)),
             end_(const_cast<InlineEntry*>(end)),
             isInline_(true)
         {
             advancePastNulls(cur_);
             MOZ_ASSERT(isInlineRange());
         }
 
         bool assertInlineRangeInvariants() const {
@@ -387,32 +389,32 @@ class InlineTable : private AllocPolicy
 
         void bumpCurPtr() {
             MOZ_ASSERT(isInlineRange());
             advancePastNulls(cur_ + 1);
         }
 
       public:
         bool empty() const {
-            return isInlineRange() ? cur_ == end_ : tableRange_.empty();
+            return isInlineRange() ? cur_ == end_ : tableRange_->empty();
         }
 
         Entry front() {
             MOZ_ASSERT(!empty());
             if (isInlineRange())
                 return Entry(cur_);
-            return Entry(&tableRange_.front());
+            return Entry(&tableRange_->front());
         }
 
         void popFront() {
             MOZ_ASSERT(!empty());
             if (isInlineRange())
                 bumpCurPtr();
             else
-                tableRange_.popFront();
+                tableRange_->popFront();
         }
     };
 
     Range all() const {
         return usingTable() ? Range(table_.all()) : Range(inlineStart(), inlineEnd());
     }
 };
 
diff --git a/mfbt/HashTable.h b/mfbt/HashTable.h
--- a/mfbt/HashTable.h
+++ b/mfbt/HashTable.h
@@ -1203,75 +1203,63 @@ public:
   {
   protected:
     friend class HashTable;
 
     Range(const HashTable& aTable, Entry* aCur, Entry* aEnd)
       : mCur(aCur)
       , mEnd(aEnd)
 #ifdef DEBUG
-      , mTable(&aTable)
+      , mTable(aTable)
       , mMutationCount(aTable.mMutationCount)
       , mGeneration(aTable.generation())
       , mValidEntry(true)
 #endif
     {
       while (mCur < mEnd && !mCur->isLive()) {
         ++mCur;
       }
     }
 
     Entry* mCur;
     Entry* mEnd;
 #ifdef DEBUG
-    const HashTable* mTable;
+    const HashTable& mTable;
     uint64_t mMutationCount;
     Generation mGeneration;
     bool mValidEntry;
 #endif
 
   public:
-    Range()
-      : mCur(nullptr)
-      , mEnd(nullptr)
-#ifdef DEBUG
-      , mTable(nullptr)
-      , mMutationCount(0)
-      , mGeneration(0)
-      , mValidEntry(false)
-#endif
-    {
-    }
-
     bool empty() const
     {
 #ifdef DEBUG
-      MOZ_ASSERT(mGeneration == mTable->generation());
-      MOZ_ASSERT(mMutationCount == mTable->mMutationCount);
+      MOZ_ASSERT(mGeneration == mTable.generation());
+      MOZ_ASSERT(mMutationCount == mTable.mMutationCount);
 #endif
       return mCur == mEnd;
     }
 
     T& front() const
     {
       MOZ_ASSERT(!empty());
 #ifdef DEBUG
       MOZ_ASSERT(mValidEntry);
-      MOZ_ASSERT(mGeneration == mTable->generation());
-      MOZ_ASSERT(mMutationCount == mTable->mMutationCount);
+      MOZ_ASSERT(mGeneration == mTable.generation());
+      MOZ_ASSERT(mMutationCount == mTable.mMutationCount);
 #endif
       return mCur->get();
     }
 
     void popFront()
     {
       MOZ_ASSERT(!empty());
 #ifdef DEBUG
-      MOZ_ASSERT(mGeneration == mTable->generation());
-      MOZ_ASSERT(mMutationCount == mTable->mMutationCount);
+      MOZ_ASSERT(mGeneration == mTable.generation());
+      MOZ_ASSERT(mMutationCount == mTable.mMutationCount);
 #endif
       while (++mCur < mEnd && !mCur->isLive()) {
         continue;
       }
 #ifdef DEBUG
       mValidEntry = true;
 #endif
     }
@@ -1333,18 +1321,18 @@ public:
 #endif
     }
 
     NonConstT& mutableFront()
     {
       MOZ_ASSERT(!this->empty());
 #ifdef DEBUG
       MOZ_ASSERT(this->mValidEntry);
-      MOZ_ASSERT(this->mGeneration == this->Range::mTable->generation());
-      MOZ_ASSERT(this->mMutationCount == this->Range::mTable->mMutationCount);
+      MOZ_ASSERT(this->mGeneration == this->Range::mTable.generation());
+      MOZ_ASSERT(this->mMutationCount == this->Range::mTable.mMutationCount);
 #endif
       return this->mCur->getMutable();
     }
 
     // Removes the |front()| element and re-inserts it into the table with
     // a new key at the new Lookup position.  |front()| is invalid after
     // this operation until the next call to |popFront()|.
     void rekeyFront(const Lookup& aLookup, const Key& aKey)
