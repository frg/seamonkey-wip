From ddb4da4feff385fc2ffac107c871045545d7b716 Mon Sep 17 00:00:00 2001
From: hawkeye116477 <hawkeye116477@gmail.com>
Date: Thu, 19 Aug 2021 13:45:44 +0200
Subject: [PATCH 1079/1328] Bug 1627838: Factor out RegExpShared::executeAtom

This code is the same for both the new and the old version.
We use `patternAtom` because the new version will add the ability to search for an atom that is not identical to the pattern source (for example, a pattern containing a unicode escape).
---
 js/src/vm/RegExpObject.cpp | 69 +++++++++++++++++++++++---------------
 js/src/vm/RegExpShared.h   |  9 +++++
 2 files changed, 51 insertions(+), 27 deletions(-)

diff --git a/js/src/vm/RegExpObject.cpp b/js/src/vm/RegExpObject.cpp
index 060b055b1fbf..b016da70e413 100644
--- a/js/src/vm/RegExpObject.cpp
+++ b/js/src/vm/RegExpObject.cpp
@@ -1151,33 +1151,7 @@ RegExpShared::execute(JSContext* cx,
     irregexp::RegExpStackScope stackScope(cx);
 
     if (re->canStringMatch) {
-        MOZ_ASSERT(re->pairCount() == 1);
-        size_t sourceLength = re->source->length();
-        if (re->sticky()) {
-            // First part checks size_t overflow.
-            if (sourceLength + start < sourceLength || sourceLength + start > length)
-                return RegExpRunStatus_Success_NotFound;
-            if (!HasSubstringAt(input, re->source, start))
-                return RegExpRunStatus_Success_NotFound;
-
-            (*matches)[0].start = start;
-            (*matches)[0].limit = start + sourceLength;
-
-            matches->checkAgainst(length);
-
-            return RegExpRunStatus_Success;
-        }
-
-        int res = StringFindPattern(input, re->source, start);
-        if (res == -1)
-            return RegExpRunStatus_Success_NotFound;
-
-        (*matches)[0].start = res;
-        (*matches)[0].limit = res + sourceLength;
-
-        matches->checkAgainst(length);
-
-        return RegExpRunStatus_Success;
+        return executeAtom(cx, re, input, start, matches);
     }
 
     do {
@@ -1244,6 +1218,47 @@ RegExpShared::execute(JSContext* cx,
 }
 #endif // JS_NEW_REGEXP
 
+/* static */
+RegExpRunStatus
+RegExpShared::executeAtom(JSContext* cx,
+                          MutableHandleRegExpShared re,
+                          HandleLinearString input,
+                          size_t start,
+                          MatchPairs* matches)
+{
+    MOZ_ASSERT(re->pairCount() == 1);
+
+    size_t length = input->length();
+    size_t searchLength = re->patternAtom()->length();
+
+    if (re->sticky()) {
+        // First part checks size_t overflow.
+        if (searchLength + start < searchLength || searchLength + start > length) {
+            return RegExpRunStatus_Success_NotFound;
+        }
+        if (!HasSubstringAt(input, re->patternAtom(), start)) {
+            return RegExpRunStatus_Success_NotFound;
+        }
+
+        (*matches)[0].start = start;
+        (*matches)[0].limit = start + searchLength;
+        matches->checkAgainst(length);
+
+        return RegExpRunStatus_Success;
+    }
+
+    int res = StringFindPattern(input, re->patternAtom(), start);
+    if (res == -1) {
+        return RegExpRunStatus_Success_NotFound;
+    }
+
+    (*matches)[0].start = res;
+    (*matches)[0].limit = res + searchLength;
+    matches->checkAgainst(length);
+
+    return RegExpRunStatus_Success;
+}
+
 size_t
 RegExpShared::sizeOfExcludingThis(mozilla::MallocSizeOf mallocSizeOf)
 {
diff --git a/js/src/vm/RegExpShared.h b/js/src/vm/RegExpShared.h
index f6073b4d1a62..5046e5740eaf 100644
--- a/js/src/vm/RegExpShared.h
+++ b/js/src/vm/RegExpShared.h
@@ -135,6 +135,12 @@ class RegExpShared : public gc::TenuredCell
   public:
     ~RegExpShared() = delete;
 
+    static RegExpRunStatus executeAtom(JSContext* cx,
+                                       MutableHandleRegExpShared re,
+                                       HandleLinearString input,
+                                       size_t start,
+                                       MatchPairs* matches);
+
     // Execute this RegExp on input starting from searchIndex, filling in matches.
     static RegExpRunStatus execute(JSContext* cx, MutableHandleRegExpShared res,
                                    HandleLinearString input, size_t searchIndex,
@@ -156,6 +162,9 @@ class RegExpShared : public gc::TenuredCell
     size_t pairCount() const            { return getParenCount() + 1; }
 
     JSAtom* getSource() const           { return source; }
+
+    JSAtom* patternAtom() const         { return getSource(); }
+
     JS::RegExpFlags getFlags() const    { return flags; }
 
     bool global() const                 { return flags.global(); }
-- 
2.33.0.windows.2

