# HG changeset patch
# User Julian Descottes <jdescottes@mozilla.com>
# Date 1508324732 -7200
# Node ID 9ac26a4888e412d0add1c5d9a43df3336e712751
# Parent  2023ff0e0dc875889dacab4bfbf6179ee710de51
Bug 1171482 - extract throttle to dedicated util;r=bgrins

MozReview-Commit-ID: L0CVtw2w03a

diff --git a/devtools/client/inspector/shared/utils.js b/devtools/client/inspector/shared/utils.js
--- a/devtools/client/inspector/shared/utils.js
+++ b/devtools/client/inspector/shared/utils.js
@@ -5,16 +5,17 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 "use strict";
 
 const {parseDeclarations} = require("devtools/shared/css/parsing-utils");
 const promise = require("promise");
 const {getCSSLexer} = require("devtools/shared/css/lexer");
 const {KeyCodes} = require("devtools/client/shared/keycodes");
+const {throttle} = require("devtools/shared/throttle");
 
 const HTML_NS = "http://www.w3.org/1999/xhtml";
 
 /**
  * Create a child element with a set of attributes.
  *
  * @param {Element} parent
  *        The parent node.
@@ -95,65 +96,16 @@ function advanceValidate(keyCode, value,
     }
   }
   return false;
 }
 
 exports.advanceValidate = advanceValidate;
 
 /**
- * From underscore's `_.throttle`
- * http://underscorejs.org
- * (c) 2009-2014 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
- * Underscore may be freely distributed under the MIT license.
- *
- * Returns a function, that, when invoked, will only be triggered at most once during a
- * given window of time. The throttled function will run as much as it can, without ever
- * going more than once per wait duration.
- *
- * @param  {Function} func
- *         The function to throttle
- * @param  {number} wait
- *         The wait period
- * @param  {Object} scope
- *         The scope to use for func
- * @return {Function} The throttled function
- */
-function throttle(func, wait, scope) {
-  let args, result;
-  let timeout = null;
-  let previous = 0;
-
-  let later = function () {
-    previous = Date.now();
-    timeout = null;
-    result = func.apply(scope, args);
-    args = null;
-  };
-
-  return function () {
-    let now = Date.now();
-    let remaining = wait - (now - previous);
-    args = arguments;
-    if (remaining <= 0) {
-      clearTimeout(timeout);
-      timeout = null;
-      previous = now;
-      result = func.apply(scope, args);
-      args = null;
-    } else if (!timeout) {
-      timeout = setTimeout(later, remaining);
-    }
-    return result;
-  };
-}
-
-exports.throttle = throttle;
-
-/**
  * Event handler that causes a blur on the target if the input has
  * multiple CSS properties as the value.
  */
 function blurOnMultipleProperties(cssProperties) {
   return (e) => {
     setTimeout(() => {
       let props = parseDeclarations(cssProperties.isKnown, e.target.value);
       if (props.length > 1) {
@@ -174,8 +126,9 @@ exports.blurOnMultipleProperties = blurO
  * @return {Promise} A rejected promise
  */
 function promiseWarn(error) {
   console.error(error);
   return promise.reject(error);
 }
 
 exports.promiseWarn = promiseWarn;
+exports.throttle = throttle;
diff --git a/devtools/shared/moz.build b/devtools/shared/moz.build
--- a/devtools/shared/moz.build
+++ b/devtools/shared/moz.build
@@ -69,13 +69,14 @@ DevToolsModules(
     'old-event-emitter.js',
     'Parser.jsm',
     'path.js',
     'plural-form.js',
     'protocol.js',
     'system.js',
     'task.js',
     'ThreadSafeDevToolsUtils.js',
+    'throttle.js',
     'wasm-source-map.js',
 )
 
 with Files('**'):
     BUG_COMPONENT = ('Firefox', 'Developer Tools')
diff --git a/devtools/shared/throttle.js b/devtools/shared/throttle.js
new file mode 100644
--- /dev/null
+++ b/devtools/shared/throttle.js
@@ -0,0 +1,54 @@
+/* This Source Code Form is subject to the terms of the Mozilla Public
+ * License, v. 2.0. If a copy of the MPL was not distributed with this
+ * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
+
+"use strict";
+
+/**
+ * From underscore's `_.throttle`
+ * http://underscorejs.org
+ * (c) 2009-2014 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
+ * Underscore may be freely distributed under the MIT license.
+ *
+ * Returns a function, that, when invoked, will only be triggered at most once during a
+ * given window of time. The throttled function will run as much as it can, without ever
+ * going more than once per wait duration.
+ *
+ * @param  {Function} func
+ *         The function to throttle
+ * @param  {number} wait
+ *         The wait period
+ * @param  {Object} scope
+ *         The scope to use for func
+ * @return {Function} The throttled function
+ */
+function throttle(func, wait, scope) {
+  let args, result;
+  let timeout = null;
+  let previous = 0;
+
+  let later = function () {
+    previous = Date.now();
+    timeout = null;
+    result = func.apply(scope, args);
+    args = null;
+  };
+
+  return function () {
+    let now = Date.now();
+    let remaining = wait - (now - previous);
+    args = arguments;
+    if (remaining <= 0) {
+      clearTimeout(timeout);
+      timeout = null;
+      previous = now;
+      result = func.apply(scope, args);
+      args = null;
+    } else if (!timeout) {
+      timeout = setTimeout(later, remaining);
+    }
+    return result;
+  };
+}
+
+exports.throttle = throttle;
