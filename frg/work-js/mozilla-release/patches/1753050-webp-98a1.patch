# HG changeset patch
# User june wilde <jewilde@mozilla.com>
# Date 1643984315 0
#      Fri Feb 04 14:18:35 2022 +0000
# Node ID c3bf485e36d48ea27db2acfd056c41711da79a33
# Parent  9d70daa2857fb6889a890bde2b8435b1fdea0fbe
Bug 1753050 - Add libwebp to libraries Updatebot can update; r=tjr,aosmond

Differential Revision: https://phabricator.services.mozilla.com/D137696

diff --git a/media/libpng/MOZCHANGES b/media/libpng/MOZCHANGES
--- a/media/libpng/MOZCHANGES
+++ b/media/libpng/MOZCHANGES
@@ -6,17 +6,17 @@ v. 2.0. If a copy of the MPL was not dis
 obtain one at http://mozilla.org/MPL/2.0/.
 
 This modified version of libpng code adds animated PNG support and is
 released under the same license as the upstream library. The modifications
 are Copyright (c) 2006-2007 Andrew Smith, Copyright (c) 2008-2017 Max Stepin,
 and are delimited by "#ifdef PNG_APNG_SUPPORTED / #endif" directives
 surrounding them in the modified libpng source files.
 
-2021/09/12  -- Enabled automatic updates via Updatebot (meta bug #1618282). For
+2022/02/01  -- Enabled automatic updates via Updatebot (meta bug #1618282). For
                further updates to the library please review the mercurial
                history for /media/libpng/moz.yaml (bug #1687541).
 
 2019/04/15  -- Synced with libpng-1.6.37 (bug #1513236)
 
 2018/09/14  -- Synced with libpng-1.6.35 (bug #1491467).
 
 2017/08/30  -- Synced with libpng-1.6.34 (bug #1402057).
diff --git a/media/libpng/moz.yaml b/media/libpng/moz.yaml
--- a/media/libpng/moz.yaml
+++ b/media/libpng/moz.yaml
@@ -25,17 +25,18 @@ updatebot:
       enabled: True
 
 vendoring:
   url: https://github.com/glennrp/libpng
   source-hosting: github
   tracking: tag
 
   exclude:
-    - "*"
+    - "**"
+    - ".*"
 
   include:
     - arm
     - contrib/arm-neon/linux.c
     - intel
     - mips
     - powerpc
     - ANNOUNCE
diff --git a/media/libwebp/MOZCHANGES b/media/libwebp/MOZCHANGES
--- a/media/libwebp/MOZCHANGES
+++ b/media/libwebp/MOZCHANGES
@@ -1,10 +1,14 @@
 Changes made to pristine libwebp source by mozilla.org developers.
 
+2022/02/03  -- Enabled automatic updates via Updatebot (meta bug #1618282). For
+               further updates to the library please review the mercurial
+               history for /media/libwebp/moz.yaml (bug #1753050).
+
 2022/01/25  -- Synced with libwebp-1.2.2 (bug 1752025).
 
 2021/09/08  -- Synced with libwebp-1.2.1 (bug 1729748).
 
 2021/02/07  -- Synced with libwebp-1.2.0 (bug 1691317).
 
 2020/02/26  -- Synced with libwebp-1.1.0 (bug 1618288).
 
diff --git a/media/libwebp/moz.yaml b/media/libwebp/moz.yaml
new file mode 100644
--- /dev/null
+++ b/media/libwebp/moz.yaml
@@ -0,0 +1,60 @@
+schema: 1
+
+bugzilla:
+  product: "Core"
+  component: "ImageLib"
+
+origin:
+  name: "libwebp"
+  description: "WebP codec library"
+
+  url: "https://chromium.googlesource.com/webm/libwebp"
+  license: BSD-3-Clause
+
+  release: commit b0a860891dcd4c0c2d7c6149e5cccb6eb881cc21 (2022-01-19T23:35:26.000Z).
+
+  revision: "v1.2.2"
+
+  license-file: COPYING
+
+updatebot:
+  maintainer-phab: aosmond
+  maintainer-bz: aosmond@mozilla.com
+  tasks:
+    - type: vendoring
+      enabled: True
+
+vendoring:
+  url: https://github.com/webmproject/libwebp
+  source-hosting: github
+  tracking: tag
+
+  exclude:
+    - "**"
+    - ".*"
+
+  include:
+    - AUTHORS
+    - COPYING
+    - NEWS
+    - PATENTS
+    - README
+    - README.mux
+    - src/webp/*.h
+    - src/dec/*.h
+    - src/dec/*.c
+    - src/demux/demux.c
+    - src/dsp/*.h
+    - src/dsp/*.c
+    - src/enc/*.h
+    - src/enc/*.c
+    - src/utils/*.h
+    - src/utils/*.c
+
+  keep:
+    - MOZCHANGES
+    - src/moz
+
+  update-actions:
+    - action: delete-path
+      path: src/dsp/cpu.c
diff --git a/media/libwebp/update.sh b/media/libwebp/update.sh
deleted file mode 100755
--- a/media/libwebp/update.sh
+++ /dev/null
@@ -1,40 +0,0 @@
-#!/bin/sh
-# This Source Code Form is subject to the terms of the Mozilla Public
-# License, v. 2.0. If a copy of the MPL was not distributed with this
-# file, You can obtain one at http://mozilla.org/MPL/2.0/.
-
-#
-# Usage: ./update.sh <libwebp_directory>
-#
-# Copies the needed files from a directory containing the original
-# libwebp source.
-
-cp $1/AUTHORS .
-cp $1/COPYING .
-cp $1/NEWS .
-cp $1/PATENTS .
-cp $1/README .
-cp $1/README.mux .
-
-mkdir -p src/webp
-cp $1/src/webp/*.h src/webp
-
-mkdir -p src/dec
-cp $1/src/dec/*.h src/dec
-cp $1/src/dec/*.c src/dec
-
-mkdir -p src/demux
-cp $1/src/demux/demux.c src/demux
-
-mkdir -p src/dsp
-cp $1/src/dsp/*.h src/dsp
-cp $1/src/dsp/*.c src/dsp
-rm src/dsp/cpu.c
-
-mkdir -p src/enc
-cp $1/src/enc/*.h src/enc
-cp $1/src/enc/*.c src/enc
-
-mkdir -p src/utils
-cp $1/src/utils/*.h src/utils
-cp $1/src/utils/*.c src/utils
