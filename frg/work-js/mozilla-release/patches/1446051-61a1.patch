# HG changeset patch
# User Andre Bargull <andre.bargull@gmail.com>
# Date 1521642444 25200
# Node ID 2356a4fada564d0e9d1fc1909051c3b068ec2f01
# Parent  e44b6441fb25df74bff576d4e240842086daa6f7
Bug 1446051 - Use CallICU helper for str_normalize. r=Waldo

diff --git a/js/src/builtin/String.cpp b/js/src/builtin/String.cpp
--- a/js/src/builtin/String.cpp
+++ b/js/src/builtin/String.cpp
@@ -1610,67 +1610,56 @@ js::str_normalize(JSContext* cx, unsigne
         MOZ_ASSERT(form == NFKD);
         normalizer = unorm2_getNFKDInstance(&status);
     }
     if (U_FAILURE(status)) {
         JS_ReportErrorNumberASCII(cx, GetErrorMessage, nullptr, JSMSG_INTERNAL_INTL_ERROR);
         return false;
     }
 
-    int32_t spanLength = unorm2_spanQuickCheckYes(normalizer,
-                                                  srcChars.begin().get(), srcChars.length(),
-                                                  &status);
+    int32_t spanLengthInt = unorm2_spanQuickCheckYes(normalizer,
+                                                     srcChars.begin().get(), srcChars.length(),
+                                                     &status);
     if (U_FAILURE(status)) {
         JS_ReportErrorNumberASCII(cx, GetErrorMessage, nullptr, JSMSG_INTERNAL_INTL_ERROR);
         return false;
     }
-    MOZ_ASSERT(0 <= spanLength && size_t(spanLength) <= srcChars.length());
+    MOZ_ASSERT(0 <= spanLengthInt && size_t(spanLengthInt) <= srcChars.length());
+    size_t spanLength = size_t(spanLengthInt);
 
     // Return if the input string is already normalized.
-    if (size_t(spanLength) == srcChars.length()) {
+    if (spanLength == srcChars.length()) {
         // Step 7.
         args.rval().setString(str);
         return true;
     }
 
     static const size_t INLINE_CAPACITY = 32;
 
     Vector<char16_t, INLINE_CAPACITY> chars(cx);
     if (!chars.resize(Max(INLINE_CAPACITY, srcChars.length())))
         return false;
 
     // Copy the already normalized prefix.
     if (spanLength > 0)
-        PodCopy(chars.begin(), srcChars.begin().get(), size_t(spanLength));
-
-    mozilla::RangedPtr<const char16_t> remainingStart = srcChars.begin() + spanLength;
-    size_t remainingLength = srcChars.length() - size_t(spanLength);
-
-    int32_t size = unorm2_normalizeSecondAndAppend(normalizer,
-                                                   chars.begin(), spanLength, chars.length(),
-                                                   remainingStart.get(), remainingLength, &status);
-    if (status == U_BUFFER_OVERFLOW_ERROR) {
-        MOZ_ASSERT(size >= 0);
-        if (!chars.resize(size))
-            return false;
-        status = U_ZERO_ERROR;
-#ifdef DEBUG
-        int32_t finalSize =
-#endif
-        unorm2_normalizeSecondAndAppend(normalizer,
-                                        chars.begin(), spanLength, chars.length(),
-                                        remainingStart.get(), remainingLength, &status);
-        MOZ_ASSERT_IF(!U_FAILURE(status), size == finalSize);
-    }
-    if (U_FAILURE(status)) {
-        JS_ReportErrorNumberASCII(cx, GetErrorMessage, nullptr, JSMSG_INTERNAL_INTL_ERROR);
+        PodCopy(chars.begin(), srcChars.begin().get(), spanLength);
+
+    int32_t size =
+        intl::CallICU(cx, [normalizer, &srcChars, spanLength](UChar* chars, uint32_t size,
+                                                              UErrorCode* status)
+        {
+            mozilla::RangedPtr<const char16_t> remainingStart = srcChars.begin() + spanLength;
+            size_t remainingLength = srcChars.length() - spanLength;
+
+            return unorm2_normalizeSecondAndAppend(normalizer, chars, spanLength, size,
+                                                   remainingStart.get(), remainingLength, status);
+        }, chars);
+    if (size < 0)
         return false;
-    }
-
-    MOZ_ASSERT(size >= 0);
+
     JSString* ns = NewStringCopyN<CanGC>(cx, chars.begin(), size);
     if (!ns)
         return false;
 
     // Step 7.
     args.rval().setString(ns);
     return true;
 }
diff --git a/js/src/builtin/intl/CommonFunctions.h b/js/src/builtin/intl/CommonFunctions.h
--- a/js/src/builtin/intl/CommonFunctions.h
+++ b/js/src/builtin/intl/CommonFunctions.h
@@ -80,21 +80,20 @@ static_assert(mozilla::IsSame<UChar, cha
 // our uses of ICU string functions, below and elsewhere, will try to fill the
 // buffer's entire inline capacity before growing it and heap-allocating.
 constexpr size_t INITIAL_CHAR_BUFFER_SIZE = 32;
 
 template <typename ICUStringFunction, size_t InlineCapacity>
 static int32_t
 CallICU(JSContext* cx, const ICUStringFunction& strFn, Vector<char16_t, InlineCapacity>& chars)
 {
-    MOZ_ASSERT(chars.length() == 0);
-    MOZ_ALWAYS_TRUE(chars.resize(InlineCapacity));
+    MOZ_ASSERT(chars.length() >= InlineCapacity);
 
     UErrorCode status = U_ZERO_ERROR;
-    int32_t size = strFn(chars.begin(), InlineCapacity, &status);
+    int32_t size = strFn(chars.begin(), chars.length(), &status);
     if (status == U_BUFFER_OVERFLOW_ERROR) {
         MOZ_ASSERT(size >= 0);
         if (!chars.resize(size_t(size)))
             return -1;
         status = U_ZERO_ERROR;
         strFn(chars.begin(), size, &status);
     }
     if (U_FAILURE(status)) {
@@ -106,16 +105,17 @@ CallICU(JSContext* cx, const ICUStringFu
     return size;
 }
 
 template <typename ICUStringFunction>
 static JSString*
 CallICU(JSContext* cx, const ICUStringFunction& strFn)
 {
     Vector<char16_t, INITIAL_CHAR_BUFFER_SIZE> chars(cx);
+    MOZ_ALWAYS_TRUE(chars.resize(INITIAL_CHAR_BUFFER_SIZE));
 
     int32_t size = CallICU(cx, strFn, chars);
     if (size < 0)
         return nullptr;
 
     return NewStringCopyN<CanGC>(cx, chars.begin(), size_t(size));
 }
 
diff --git a/js/src/builtin/intl/DateTimeFormat.cpp b/js/src/builtin/intl/DateTimeFormat.cpp
--- a/js/src/builtin/intl/DateTimeFormat.cpp
+++ b/js/src/builtin/intl/DateTimeFormat.cpp
@@ -476,16 +476,18 @@ js::intl_isDefaultTimeZone(JSContext* cx
     }
 
     // The current default might be stale, because JS::ResetTimeZone() doesn't
     // immediately update ICU's default time zone. So perform an update if
     // needed.
     js::ResyncICUDefaultTimeZone();
 
     Vector<char16_t, INITIAL_CHAR_BUFFER_SIZE> chars(cx);
+    MOZ_ALWAYS_TRUE(chars.resize(INITIAL_CHAR_BUFFER_SIZE));
+
     int32_t size = CallICU(cx, ucal_getDefaultTimeZone, chars);
     if (size < 0)
         return false;
 
     JSLinearString* str = args[0].toString()->ensureLinear(cx);
     if (!str)
         return false;
 
