# HG changeset patch
# User Peter Van der Beken <peterv@propagandism.org>
# Date 1659468469 0
# Node ID 7d69277b335be5a5076e21c0fe9b73e85677e7ff
# Parent  b503b79de4ed9799aba47ff3af147070bb11c298
Bug 1769155 - Deal with document replacement. r=smaug, a=RyanVM

Differential Revision: https://phabricator.services.mozilla.com/D153513

diff --git a/dom/xml/nsXMLContentSink.cpp b/dom/xml/nsXMLContentSink.cpp
--- a/dom/xml/nsXMLContentSink.cpp
+++ b/dom/xml/nsXMLContentSink.cpp
@@ -317,76 +317,84 @@ nsXMLContentSink::DidBuildModel(bool aTe
     mDocument->EndLoad();
 
     DropParserAndPerfHint();
   }
 
   return NS_OK;
 }
 
-NS_IMETHODIMP
-nsXMLContentSink::OnDocumentCreated(Document* aResultDocument) {
-  NS_ENSURE_ARG(aResultDocument);
-
+nsresult nsXMLContentSink::OnDocumentCreated(Document* aSourceDocument,
+                                             Document* aResultDocument) {
   aResultDocument->SetDocWriteDisabled(true);
 
   nsCOMPtr<nsIContentViewer> contentViewer;
   mDocShell->GetContentViewer(getter_AddRefs(contentViewer));
-  if (contentViewer) {
+  // Make sure that we haven't loaded a new document into the contentviewer
+  // after starting the XSLT transform.
+  if (contentViewer && contentViewer->GetDocument() == aSourceDocument) {
     return contentViewer->SetDocumentInternal(aResultDocument, true);
   }
   return NS_OK;
 }
 
-NS_IMETHODIMP
-nsXMLContentSink::OnTransformDone(nsresult aResult, Document* aResultDocument) {
+nsresult nsXMLContentSink::OnTransformDone(Document* aSourceDocument,
+                                           nsresult aResult,
+                                           Document* aResultDocument) {
   MOZ_ASSERT(aResultDocument,
              "Don't notify about transform end without a document.");
 
   mDocumentChildren.Clear();
 
   nsCOMPtr<nsIContentViewer> contentViewer;
   mDocShell->GetContentViewer(getter_AddRefs(contentViewer));
 
-  if (NS_FAILED(aResult) && contentViewer) {
-    // Transform failed.
-    aResultDocument->SetMayStartLayout(false);
-    // We have an error document.
-    contentViewer->SetDocument(aResultDocument);
-  }
-
   RefPtr<Document> originalDocument = mDocument;
   bool blockingOnload = mIsBlockingOnload;
-  if (!mRunsToCompletion) {
-    // This BlockOnload call corresponds to the UnblockOnload call in
-    // nsContentSink::DropParserAndPerfHint.
-    aResultDocument->BlockOnload();
-    mIsBlockingOnload = true;
-  }
-  // Transform succeeded, or it failed and we have an error document to display.
-  mDocument = aResultDocument;
-  aResultDocument->SetDocWriteDisabled(false);
+
+  // Make sure that we haven't loaded a new document into the contentviewer
+  // after starting the XSLT transform.
+  if (contentViewer && (contentViewer->GetDocument() == aSourceDocument ||
+                        contentViewer->GetDocument() == aResultDocument)) {
+    if (NS_FAILED(aResult)) {
+      // Transform failed.
+      aResultDocument->SetMayStartLayout(false);
+      // We have an error document.
+      contentViewer->SetDocument(aResultDocument);
+    }
 
-  // Notify document observers that all the content has been stuck
-  // into the document.
-  // XXX do we need to notify for things like PIs?  Or just the
-  // documentElement?
-  nsIContent* rootElement = mDocument->GetRootElement();
-  if (rootElement) {
-    NS_ASSERTION(mDocument->ComputeIndexOf(rootElement) != -1,
-                 "rootElement not in doc?");
-    mDocument->BeginUpdate();
-    MutationObservers::NotifyContentInserted(mDocument, rootElement);
-    mDocument->EndUpdate();
-  }
+    if (!mRunsToCompletion) {
+      // This BlockOnload call corresponds to the UnblockOnload call in
+      // nsContentSink::DropParserAndPerfHint.
+      aResultDocument->BlockOnload();
+      mIsBlockingOnload = true;
+    }
+    // Transform succeeded, or it failed and we have an error document to
+    // display.
+    mDocument = aResultDocument;
+    aResultDocument->SetDocWriteDisabled(false);
 
-  // Start the layout process
-  StartLayout(false);
+    // Notify document observers that all the content has been stuck
+    // into the document.
+    // XXX do we need to notify for things like PIs?  Or just the
+    // documentElement?
+    nsIContent* rootElement = mDocument->GetRootElement();
+    if (rootElement) {
+      NS_ASSERTION(mDocument->ComputeIndexOf(rootElement) != -1,
+                   "rootElement not in doc?");
+      mDocument->BeginUpdate();
+      MutationObservers::NotifyContentInserted(mDocument, rootElement);
+      mDocument->EndUpdate();
+    }
 
-  ScrollToRef();
+    // Start the layout process
+    StartLayout(false);
+
+    ScrollToRef();
+   }
 
   originalDocument->EndLoad();
   if (blockingOnload) {
     // This UnblockOnload call corresponds to the BlockOnload call in
     // nsContentSink::WillBuildModelImpl.
     originalDocument->UnblockOnload(true);
   }
 
diff --git a/dom/xml/nsXMLContentSink.h b/dom/xml/nsXMLContentSink.h
--- a/dom/xml/nsXMLContentSink.h
+++ b/dom/xml/nsXMLContentSink.h
@@ -73,20 +73,21 @@ class nsXMLContentSink : public nsConten
   virtual bool IsScriptExecuting() override;
   virtual void ContinueInterruptedParsingAsync() override;
   bool IsPrettyPrintXML() const override { return mPrettyPrintXML; }
   bool IsPrettyPrintHasSpecialRoot() const override {
     return mPrettyPrintHasSpecialRoot;
   }
 
   // nsITransformObserver
-  NS_IMETHOD OnDocumentCreated(
-      mozilla::dom::Document* aResultDocument) override;
-  NS_IMETHOD OnTransformDone(nsresult aResult,
+  nsresult OnDocumentCreated(mozilla::dom::Document* aSourceDocument,
                              mozilla::dom::Document* aResultDocument) override;
+  nsresult OnTransformDone(mozilla::dom::Document* aSourceDocument,
+                           nsresult aResult,
+                           mozilla::dom::Document* aResultDocument) override;
 
   // nsICSSLoaderObserver
   NS_IMETHOD StyleSheetLoaded(mozilla::StyleSheet* aSheet, bool aWasDeferred,
                               nsresult aStatus) override;
   static bool ParsePIData(const nsString& aData, nsString& aHref,
                           nsString& aTitle, nsString& aMedia,
                           bool& aIsAlternate);
 
diff --git a/dom/xslt/nsIDocumentTransformer.h b/dom/xslt/nsIDocumentTransformer.h
--- a/dom/xslt/nsIDocumentTransformer.h
+++ b/dom/xslt/nsIDocumentTransformer.h
@@ -28,20 +28,23 @@ class Document;
       0x9a, 0x67, 0xb7, 0x01, 0x19, 0x59, 0x7d, 0xe7 \
     }                                                \
   }
 
 class nsITransformObserver : public nsISupports {
  public:
   NS_DECLARE_STATIC_IID_ACCESSOR(NS_ITRANSFORMOBSERVER_IID)
 
-  NS_IMETHOD OnDocumentCreated(mozilla::dom::Document* aResultDocument) = 0;
+  virtual nsresult OnDocumentCreated(
+      mozilla::dom::Document* aSourceDocument,
+      mozilla::dom::Document* aResultDocument) = 0;
 
-  NS_IMETHOD OnTransformDone(nsresult aResult,
-                             mozilla::dom::Document* aResultDocument) = 0;
+  virtual nsresult OnTransformDone(mozilla::dom::Document* aSourceDocument,
+                                   nsresult aResult,
+                                   mozilla::dom::Document* aResultDocument) = 0;
 };
 
 NS_DEFINE_STATIC_IID_ACCESSOR(nsITransformObserver, NS_ITRANSFORMOBSERVER_IID)
 
 #define NS_IDOCUMENTTRANSFORMER_IID                  \
   {                                                  \
     0xf45e1ff8, 0x50f3, 0x4496, {                    \
       0xb3, 0xa2, 0x0e, 0x03, 0xe8, 0x4a, 0x57, 0x11 \
diff --git a/dom/xslt/xslt/txMozillaTextOutput.cpp b/dom/xslt/xslt/txMozillaTextOutput.cpp
--- a/dom/xslt/xslt/txMozillaTextOutput.cpp
+++ b/dom/xslt/xslt/txMozillaTextOutput.cpp
@@ -16,18 +16,21 @@
 #include "mozilla/Encoding.h"
 #include "nsTextNode.h"
 #include "nsNameSpaceManager.h"
 #include "mozilla/dom/DocumentFragment.h"
 
 using namespace mozilla;
 using namespace mozilla::dom;
 
-txMozillaTextOutput::txMozillaTextOutput(nsITransformObserver* aObserver)
-    : mObserver(do_GetWeakReference(aObserver)), mCreatedDocument(false) {
+txMozillaTextOutput::txMozillaTextOutput(Document* aSourceDocument,
+                                         nsITransformObserver* aObserver)
+    : mSourceDocument(aSourceDocument),
+      mObserver(do_GetWeakReference(aObserver)),
+      mCreatedDocument(false) {
   MOZ_COUNT_CTOR(txMozillaTextOutput);
 }
 
 txMozillaTextOutput::txMozillaTextOutput(DocumentFragment* aDest)
     : mTextParent(aDest),
       mDocument(mTextParent->OwnerDoc()),
       mCreatedDocument(false) {
   MOZ_COUNT_CTOR(txMozillaTextOutput);
@@ -81,34 +84,33 @@ nsresult txMozillaTextOutput::endDocumen
         mDocument->GetReadyStateEnum() == Document::READYSTATE_INTERACTIVE,
         "Bad readyState");
   }
   mDocument->SetReadyStateInternal(Document::READYSTATE_INTERACTIVE);
 
   if (NS_SUCCEEDED(aResult)) {
     nsCOMPtr<nsITransformObserver> observer = do_QueryReferent(mObserver);
     if (observer) {
-      observer->OnTransformDone(aResult, mDocument);
+      observer->OnTransformDone(mSourceDocument, aResult, mDocument);
     }
   }
 
   return NS_OK;
 }
 
 nsresult txMozillaTextOutput::endElement() { return NS_OK; }
 
 nsresult txMozillaTextOutput::processingInstruction(const nsString& aTarget,
                                                     const nsString& aData) {
   return NS_OK;
 }
 
 nsresult txMozillaTextOutput::startDocument() { return NS_OK; }
 
-nsresult txMozillaTextOutput::createResultDocument(Document* aSourceDocument,
-                                                   bool aLoadedAsData) {
+nsresult txMozillaTextOutput::createResultDocument(bool aLoadedAsData) {
   /*
    * Create an XHTML document to hold the text.
    *
    * <html>
    *   <head />
    *   <body>
    *     <pre id="transformiixResult"> * The text comes here * </pre>
    *   <body>
@@ -126,40 +128,40 @@ nsresult txMozillaTextOutput::createResu
   mCreatedDocument = true;
   // This should really be handled by Document::BeginLoad
   MOZ_ASSERT(
       mDocument->GetReadyStateEnum() == Document::READYSTATE_UNINITIALIZED,
       "Bad readyState");
   mDocument->SetReadyStateInternal(Document::READYSTATE_LOADING);
   bool hasHadScriptObject = false;
   nsIScriptGlobalObject* sgo =
-      aSourceDocument->GetScriptHandlingObject(hasHadScriptObject);
+      mSourceDocument->GetScriptHandlingObject(hasHadScriptObject);
   NS_ENSURE_STATE(sgo || !hasHadScriptObject);
 
   NS_ASSERTION(mDocument, "Need document");
 
   // Reset and set up document
-  URIUtils::ResetWithSource(mDocument, aSourceDocument);
+  URIUtils::ResetWithSource(mDocument, mSourceDocument);
   // Only do this after resetting the document to ensure we have the
   // correct principal.
   mDocument->SetScriptHandlingObject(sgo);
 
   // Set the charset
   if (!mOutputFormat.mEncoding.IsEmpty()) {
     const Encoding* encoding = Encoding::ForLabel(mOutputFormat.mEncoding);
     if (encoding) {
       mDocument->SetDocumentCharacterSetSource(kCharsetFromOtherComponent);
       mDocument->SetDocumentCharacterSet(WrapNotNull(encoding));
     }
   }
 
   // Notify the contentsink that the document is created
   nsCOMPtr<nsITransformObserver> observer = do_QueryReferent(mObserver);
   if (observer) {
-    rv = observer->OnDocumentCreated(mDocument);
+    rv = observer->OnDocumentCreated(mSourceDocument, mDocument);
     NS_ENSURE_SUCCESS(rv, rv);
   }
 
   // Create the content
 
   // When transforming into a non-displayed document (i.e. when there is no
   // observer) we only create a transformiix:result root element.
   if (!observer) {
diff --git a/dom/xslt/xslt/txMozillaTextOutput.h b/dom/xslt/xslt/txMozillaTextOutput.h
--- a/dom/xslt/xslt/txMozillaTextOutput.h
+++ b/dom/xslt/xslt/txMozillaTextOutput.h
@@ -19,29 +19,30 @@ namespace dom {
 class Document;
 class DocumentFragment;
 class Element;
 }  // namespace dom
 }  // namespace mozilla
 
 class txMozillaTextOutput : public txAOutputXMLEventHandler {
  public:
-  explicit txMozillaTextOutput(nsITransformObserver* aObserver);
+  explicit txMozillaTextOutput(mozilla::dom::Document* aSourceDocument,
+                               nsITransformObserver* aObserver);
   explicit txMozillaTextOutput(mozilla::dom::DocumentFragment* aDest);
   virtual ~txMozillaTextOutput();
 
   TX_DECL_TXAXMLEVENTHANDLER
   TX_DECL_TXAOUTPUTXMLEVENTHANDLER
 
-  nsresult createResultDocument(mozilla::dom::Document* aSourceDocument,
-                                bool aLoadedAsData);
+  nsresult createResultDocument(bool aLoadedAsData);
 
  private:
   nsresult createXHTMLElement(nsAtom* aName, mozilla::dom::Element** aResult);
 
+  nsCOMPtr<mozilla::dom::Document> mSourceDocument;
   nsCOMPtr<nsIContent> mTextParent;
   nsWeakPtr mObserver;
   RefPtr<mozilla::dom::Document> mDocument;
   txOutputFormat mOutputFormat;
   nsString mText;
   bool mCreatedDocument;
 };
 
diff --git a/dom/xslt/xslt/txMozillaXMLOutput.cpp b/dom/xslt/xslt/txMozillaXMLOutput.cpp
--- a/dom/xslt/xslt/txMozillaXMLOutput.cpp
+++ b/dom/xslt/xslt/txMozillaXMLOutput.cpp
@@ -42,28 +42,29 @@
 
 using namespace mozilla;
 using namespace mozilla::dom;
 
 #define TX_ENSURE_CURRENTNODE                            \
   NS_ASSERTION(mCurrentNode, "mCurrentNode is nullptr"); \
   if (!mCurrentNode) return NS_ERROR_UNEXPECTED
 
-txMozillaXMLOutput::txMozillaXMLOutput(txOutputFormat* aFormat,
+txMozillaXMLOutput::txMozillaXMLOutput(Document* aSourceDocument,
+                                       txOutputFormat* aFormat,
                                        nsITransformObserver* aObserver)
     : mTreeDepth(0),
       mBadChildLevel(0),
       mTableState(NORMAL),
       mCreatingNewDocument(true),
       mOpenedElementIsHTML(false),
       mRootContentCreated(false),
       mNoFixup(false) {
   MOZ_COUNT_CTOR(txMozillaXMLOutput);
   if (aObserver) {
-    mNotifier = new txTransformNotifier();
+    mNotifier = new txTransformNotifier(aSourceDocument);
     if (mNotifier) {
       mNotifier->Init(aObserver);
     }
   }
 
   mOutputFormat.merge(*aFormat);
   mOutputFormat.setFromDefaults();
 }
@@ -841,18 +842,20 @@ nsresult txMozillaXMLOutput::createHTMLE
   nsCOMPtr<Element> el;
   nsresult rv = NS_NewHTMLElement(
       getter_AddRefs(el), ni.forget(),
       mCreatingNewDocument ? FROM_PARSER_XSLT : FROM_PARSER_FRAGMENT);
   el.forget(aResult);
   return rv;
 }
 
-txTransformNotifier::txTransformNotifier()
-    : mPendingStylesheetCount(0), mInTransform(false) {}
+txTransformNotifier::txTransformNotifier(Document* aSourceDocument)
+    : mSourceDocument(aSourceDocument),
+      mPendingStylesheetCount(0),
+      mInTransform(false) {}
 
 txTransformNotifier::~txTransformNotifier() = default;
 
 NS_IMPL_ISUPPORTS(txTransformNotifier, nsIScriptLoaderObserver,
                   nsICSSLoaderObserver)
 
 NS_IMETHODIMP
 txTransformNotifier::ScriptAvailable(nsresult aResult,
@@ -913,17 +916,17 @@ void txTransformNotifier::OnTransformEnd
 }
 
 void txTransformNotifier::OnTransformStart() { mInTransform = true; }
 
 nsresult txTransformNotifier::SetOutputDocument(Document* aDocument) {
   mDocument = aDocument;
 
   // Notify the contentsink that the document is created
-  return mObserver->OnDocumentCreated(mDocument);
+  return mObserver->OnDocumentCreated(mSourceDocument, mDocument);
 }
 
 void txTransformNotifier::SignalTransformEnd(nsresult aResult) {
   if (mInTransform ||
       (NS_SUCCEEDED(aResult) &&
        (mScriptElements.Count() > 0 || mPendingStylesheetCount > 0))) {
     return;
   }
@@ -944,11 +947,11 @@ void txTransformNotifier::SignalTransfor
     // XXX Maybe we want to cancel script loads if NS_FAILED(rv)?
 
     if (NS_FAILED(aResult)) {
       mDocument->CSSLoader()->Stop();
     }
   }
 
   if (NS_SUCCEEDED(aResult)) {
-    mObserver->OnTransformDone(aResult, mDocument);
+    mObserver->OnTransformDone(mSourceDocument, aResult, mDocument);
   }
 }
diff --git a/dom/xslt/xslt/txMozillaXMLOutput.h b/dom/xslt/xslt/txMozillaXMLOutput.h
--- a/dom/xslt/xslt/txMozillaXMLOutput.h
+++ b/dom/xslt/xslt/txMozillaXMLOutput.h
@@ -27,17 +27,17 @@ class Document;
 class DocumentFragment;
 class Element;
 }  // namespace dom
 }  // namespace mozilla
 
 class txTransformNotifier final : public nsIScriptLoaderObserver,
                                   public nsICSSLoaderObserver {
  public:
-  txTransformNotifier();
+  explicit txTransformNotifier(mozilla::dom::Document* aSourceDocument);
 
   NS_DECL_ISUPPORTS
   NS_DECL_NSISCRIPTLOADEROBSERVER
 
   // nsICSSLoaderObserver
   NS_IMETHOD StyleSheetLoaded(mozilla::StyleSheet* aSheet, bool aWasDeferred,
                               nsresult aStatus) override;
 
@@ -47,26 +47,28 @@ class txTransformNotifier final : public
   void OnTransformEnd(nsresult aResult = NS_OK);
   void OnTransformStart();
   nsresult SetOutputDocument(mozilla::dom::Document* aDocument);
 
  private:
   ~txTransformNotifier();
   void SignalTransformEnd(nsresult aResult = NS_OK);
 
+  nsCOMPtr<mozilla::dom::Document> mSourceDocument;
   nsCOMPtr<mozilla::dom::Document> mDocument;
   nsCOMPtr<nsITransformObserver> mObserver;
   nsCOMArray<nsIScriptElement> mScriptElements;
   uint32_t mPendingStylesheetCount;
   bool mInTransform;
 };
 
 class txMozillaXMLOutput : public txAOutputXMLEventHandler {
  public:
-  txMozillaXMLOutput(txOutputFormat* aFormat, nsITransformObserver* aObserver);
+  txMozillaXMLOutput(mozilla::dom::Document* aSourceDocument,
+                     txOutputFormat* aFormat, nsITransformObserver* aObserver);
   txMozillaXMLOutput(txOutputFormat* aFormat,
                      mozilla::dom::DocumentFragment* aFragment, bool aNoFixup);
   ~txMozillaXMLOutput();
 
   TX_DECL_TXAXMLEVENTHANDLER
   TX_DECL_TXAOUTPUTXMLEVENTHANDLER
 
   nsresult closePrevious(bool aFlushText);
diff --git a/dom/xslt/xslt/txMozillaXSLTProcessor.cpp b/dom/xslt/xslt/txMozillaXSLTProcessor.cpp
--- a/dom/xslt/xslt/txMozillaXSLTProcessor.cpp
+++ b/dom/xslt/xslt/txMozillaXSLTProcessor.cpp
@@ -74,33 +74,32 @@ nsresult txToDocHandlerFactory::createHa
     case eMethodNotSet:
     case eXMLOutput: {
       *aHandler = new txUnknownHandler(mEs);
       return NS_OK;
     }
 
     case eHTMLOutput: {
       UniquePtr<txMozillaXMLOutput> handler(
-          new txMozillaXMLOutput(aFormat, mObserver));
+          new txMozillaXMLOutput(mSourceDocument, aFormat, mObserver));
 
       nsresult rv = handler->createResultDocument(
           u""_ns, kNameSpaceID_None, mSourceDocument, mDocumentIsData);
       if (NS_SUCCEEDED(rv)) {
         *aHandler = handler.release();
       }
 
       return rv;
     }
 
     case eTextOutput: {
       UniquePtr<txMozillaTextOutput> handler(
-          new txMozillaTextOutput(mObserver));
+          new txMozillaTextOutput(mSourceDocument, mObserver));
 
-      nsresult rv =
-          handler->createResultDocument(mSourceDocument, mDocumentIsData);
+      nsresult rv = handler->createResultDocument(mDocumentIsData);
       if (NS_SUCCEEDED(rv)) {
         *aHandler = handler.release();
       }
 
       return rv;
     }
   }
 
@@ -117,33 +116,32 @@ nsresult txToDocHandlerFactory::createHa
     case eMethodNotSet: {
       NS_ERROR("How can method not be known when root element is?");
       return NS_ERROR_UNEXPECTED;
     }
 
     case eXMLOutput:
     case eHTMLOutput: {
       UniquePtr<txMozillaXMLOutput> handler(
-          new txMozillaXMLOutput(aFormat, mObserver));
+          new txMozillaXMLOutput(mSourceDocument, aFormat, mObserver));
 
       nsresult rv = handler->createResultDocument(aName, aNsID, mSourceDocument,
                                                   mDocumentIsData);
       if (NS_SUCCEEDED(rv)) {
         *aHandler = handler.release();
       }
 
       return rv;
     }
 
     case eTextOutput: {
       UniquePtr<txMozillaTextOutput> handler(
-          new txMozillaTextOutput(mObserver));
+          new txMozillaTextOutput(mSourceDocument, mObserver));
 
-      nsresult rv =
-          handler->createResultDocument(mSourceDocument, mDocumentIsData);
+      nsresult rv = handler->createResultDocument(mDocumentIsData);
       if (NS_SUCCEEDED(rv)) {
         *aHandler = handler.release();
       }
 
       return rv;
     }
   }
 
@@ -1023,18 +1021,17 @@ void txMozillaXSLTProcessor::notifyError
     if (rv.Failed()) {
       return;
     }
   }
 
   MOZ_ASSERT(document->GetReadyStateEnum() == Document::READYSTATE_LOADING,
              "Bad readyState.");
   document->SetReadyStateInternal(Document::READYSTATE_INTERACTIVE);
-
-  mObserver->OnTransformDone(mTransformResult, document);
+  mObserver->OnTransformDone(mSource->OwnerDoc(), mTransformResult, document);
 }
 
 nsresult txMozillaXSLTProcessor::ensureStylesheet() {
   if (mStylesheet) {
     return NS_OK;
   }
 
   NS_ENSURE_TRUE(mStylesheetDocument, NS_ERROR_NOT_INITIALIZED);

