# HG changeset patch
# User Jon Coppeard <jcoppeard@mozilla.com>
# Date 1526482693 -3600
# Node ID f0433be07dc594b15ad488cb235bbf60b99c004f
# Parent  476835b97935e5dfd2f62c98e13bcc9d32172543
Bug 1444956 - Support BinAST decoding in the script loader r=baku

diff --git a/dom/script/ScriptLoadHandler.cpp b/dom/script/ScriptLoadHandler.cpp
--- a/dom/script/ScriptLoadHandler.cpp
+++ b/dom/script/ScriptLoadHandler.cpp
@@ -4,16 +4,19 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "ScriptLoadHandler.h"
 #include "ScriptLoader.h"
 #include "ScriptTrace.h"
 
 #include "nsContentUtils.h"
+#include "nsIEncodedChannel.h"
+#include "nsIStringEnumerator.h"
+#include "nsMimeTypes.h"
 
 #include "mozilla/Telemetry.h"
 
 namespace mozilla {
 namespace dom {
 
 #undef LOG
 #define LOG(args) \
@@ -54,17 +57,17 @@ ScriptLoadHandler::OnIncrementalData(nsI
   }
 
   nsresult rv = NS_OK;
   if (mRequest->IsUnknownDataType()) {
     rv = EnsureKnownDataType(aLoader);
     NS_ENSURE_SUCCESS(rv, rv);
   }
 
-  if (mRequest->IsSource()) {
+  if (mRequest->IsTextSource()) {
     if (!EnsureDecoder(aLoader, aData, aDataLength,
                        /* aEndOfStream = */ false)) {
       return NS_OK;
     }
 
     // Below we will/shall consume entire data chunk.
     *aConsumedLength = aDataLength;
 
@@ -99,41 +102,41 @@ ScriptLoadHandler::DecodeRawData(const u
                                  uint32_t aDataLength,
                                  bool aEndOfStream)
 {
   CheckedInt<size_t> needed = mDecoder->MaxUTF16BufferLength(aDataLength);
   if (!needed.isValid()) {
     return NS_ERROR_OUT_OF_MEMORY;
   }
 
-  uint32_t haveRead = mRequest->mScriptText.length();
+  uint32_t haveRead = mRequest->ScriptText().length();
 
   CheckedInt<uint32_t> capacity = haveRead;
   capacity += needed.value();
 
-  if (!capacity.isValid() || !mRequest->mScriptText.reserve(capacity.value())) {
+  if (!capacity.isValid() || !mRequest->ScriptText().reserve(capacity.value())) {
     return NS_ERROR_OUT_OF_MEMORY;
   }
 
   uint32_t result;
   size_t read;
   size_t written;
   bool hadErrors;
   Tie(result, read, written, hadErrors) = mDecoder->DecodeToUTF16(
     MakeSpan(aData, aDataLength),
-    MakeSpan(mRequest->mScriptText.begin() + haveRead, needed.value()),
+    MakeSpan(mRequest->ScriptText().begin() + haveRead, needed.value()),
     aEndOfStream);
   MOZ_ASSERT(result == kInputEmpty);
   MOZ_ASSERT(read == aDataLength);
   MOZ_ASSERT(written <= needed.value());
   Unused << hadErrors;
 
   haveRead += written;
   MOZ_ASSERT(haveRead <= capacity.value(), "mDecoder produced more data than expected");
-  MOZ_ALWAYS_TRUE(mRequest->mScriptText.resizeUninitialized(haveRead));
+  MOZ_ALWAYS_TRUE(mRequest->ScriptText().resizeUninitialized(haveRead));
 
   return NS_OK;
 }
 
 bool
 ScriptLoadHandler::EnsureDecoder(nsIIncrementalStreamLoader* aLoader,
                                  const uint8_t* aData,
                                  uint32_t aDataLength,
@@ -268,43 +271,45 @@ ScriptLoadHandler::MaybeDecodeSRI()
   return NS_OK;
 }
 
 nsresult
 ScriptLoadHandler::EnsureKnownDataType(nsIIncrementalStreamLoader* aLoader)
 {
   MOZ_ASSERT(mRequest->IsUnknownDataType());
   MOZ_ASSERT(mRequest->IsLoading());
-  if (mRequest->IsLoadingSource()) {
-    mRequest->mDataType = ScriptLoadRequest::DataType::eSource;
-    TRACE_FOR_TEST(mRequest->mElement, "scriptloader_load_source");
-    return NS_OK;
-  }
 
   nsCOMPtr<nsIRequest> req;
   nsresult rv = aLoader->GetRequest(getter_AddRefs(req));
   MOZ_ASSERT(req, "StreamLoader's request went away prematurely");
   NS_ENSURE_SUCCESS(rv, rv);
 
+  if (mRequest->IsLoadingSource()) {
+    mRequest->SetTextSource();
+    TRACE_FOR_TEST(mRequest->mElement, "scriptloader_load_source");
+    return NS_OK;
+  }
+
   nsCOMPtr<nsICacheInfoChannel> cic(do_QueryInterface(req));
   if (cic) {
     nsAutoCString altDataType;
     cic->GetAlternativeDataType(altDataType);
     if (altDataType.Equals(nsContentUtils::JSBytecodeMimeType())) {
-      mRequest->mDataType = ScriptLoadRequest::DataType::eBytecode;
+      mRequest->SetBytecode();
       TRACE_FOR_TEST(mRequest->mElement, "scriptloader_load_bytecode");
     } else {
       MOZ_ASSERT(altDataType.IsEmpty());
-      mRequest->mDataType = ScriptLoadRequest::DataType::eSource;
+      mRequest->SetTextSource();
       TRACE_FOR_TEST(mRequest->mElement, "scriptloader_load_source");
     }
   } else {
-    mRequest->mDataType = ScriptLoadRequest::DataType::eSource;
+    mRequest->SetTextSource();
     TRACE_FOR_TEST(mRequest->mElement, "scriptloader_load_source");
   }
+
   MOZ_ASSERT(!mRequest->IsUnknownDataType());
   MOZ_ASSERT(mRequest->IsLoading());
   return NS_OK;
 }
 
 NS_IMETHODIMP
 ScriptLoadHandler::OnStreamComplete(nsIIncrementalStreamLoader* aLoader,
                                     nsISupports* aContext,
@@ -324,25 +329,25 @@ ScriptLoadHandler::OnStreamComplete(nsII
   aLoader->GetRequest(getter_AddRefs(channelRequest));
 
   if (!mRequest->IsCanceled()) {
     if (mRequest->IsUnknownDataType()) {
       rv = EnsureKnownDataType(aLoader);
       NS_ENSURE_SUCCESS(rv, rv);
     }
 
-    if (mRequest->IsSource()) {
+    if (mRequest->IsTextSource()) {
       DebugOnly<bool> encoderSet =
         EnsureDecoder(aLoader, aData, aDataLength, /* aEndOfStream = */ true);
       MOZ_ASSERT(encoderSet);
       rv = DecodeRawData(aData, aDataLength, /* aEndOfStream = */ true);
       NS_ENSURE_SUCCESS(rv, rv);
 
       LOG(("ScriptLoadRequest (%p): Source length = %u",
-           mRequest.get(), unsigned(mRequest->mScriptText.length())));
+           mRequest.get(), unsigned(mRequest->ScriptText().length())));
 
       // If SRI is required for this load, appending new bytes to the hash.
       if (mSRIDataVerifier && NS_SUCCEEDED(mSRIStatus)) {
         mSRIStatus = mSRIDataVerifier->Update(aDataLength, aData);
       }
     } else {
       MOZ_ASSERT(mRequest->IsBytecode());
       if (!mRequest->mScriptBytecode.append(aData, aDataLength)) {
diff --git a/dom/script/ScriptLoadRequest.cpp b/dom/script/ScriptLoadRequest.cpp
--- a/dom/script/ScriptLoadRequest.cpp
+++ b/dom/script/ScriptLoadRequest.cpp
@@ -1,16 +1,19 @@
 /* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "ModuleLoadRequest.h"
+
 #include "mozilla/HoldDropJSObjects.h"
+#include "mozilla/Unused.h"
+
 #include "nsICacheInfoChannel.h"
 #include "ScriptLoadRequest.h"
 #include "ScriptSettings.h"
 
 namespace mozilla {
 namespace dom {
 
 //////////////////////////////////////////////////////////////
@@ -58,17 +61,16 @@ ScriptLoadRequest::ScriptLoadRequest(Scr
   , mInDeferList(false)
   , mInAsyncList(false)
   , mIsNonAsyncScriptInserted(false)
   , mIsXSLT(false)
   , mIsCanceled(false)
   , mWasCompiledOMT(false)
   , mIsTracking(false)
   , mOffThreadToken(nullptr)
-  , mScriptText()
   , mScriptBytecode()
   , mBytecodeOffset(0)
   , mURI(aURI)
   , mLineNo(1)
   , mCORSMode(aCORSMode)
   , mIntegrity(aIntegrity)
   , mReferrer(aReferrer)
   , mReferrerPolicy(aReferrerPolicy)
@@ -147,16 +149,46 @@ ScriptLoadRequest::SetScriptMode(bool aD
     mScriptMode = ScriptMode::eAsync;
   } else if (aDeferAttr || IsModuleRequest()) {
     mScriptMode = ScriptMode::eDeferred;
   } else {
     mScriptMode = ScriptMode::eBlocking;
   }
 }
 
+void
+ScriptLoadRequest::SetUnknownDataType()
+{
+  mDataType = DataType::eUnknown;
+  mScriptData.reset();
+}
+
+void
+ScriptLoadRequest::SetTextSource()
+{
+  MOZ_ASSERT(IsUnknownDataType());
+  mDataType = DataType::eTextSource;
+  mScriptData.emplace(VariantType<Vector<char16_t>>());
+}
+
+void
+ScriptLoadRequest::SetBytecode()
+{
+  MOZ_ASSERT(IsUnknownDataType());
+  mDataType = DataType::eBytecode;
+}
+
+void
+ScriptLoadRequest::ClearScriptSource()
+{
+  if (IsTextSource()) {
+    ScriptText().clearAndFree();
+  }
+}
+
 //////////////////////////////////////////////////////////////
 // ScriptLoadRequestList
 //////////////////////////////////////////////////////////////
 
 ScriptLoadRequestList::~ScriptLoadRequestList()
 {
   Clear();
 }
diff --git a/dom/script/ScriptLoadRequest.h b/dom/script/ScriptLoadRequest.h
--- a/dom/script/ScriptLoadRequest.h
+++ b/dom/script/ScriptLoadRequest.h
@@ -5,17 +5,19 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef mozilla_dom_ScriptLoadRequest_h
 #define mozilla_dom_ScriptLoadRequest_h
 
 #include "mozilla/CORSMode.h"
 #include "mozilla/dom/SRIMetadata.h"
 #include "mozilla/LinkedList.h"
+#include "mozilla/Maybe.h"
 #include "mozilla/net/ReferrerPolicy.h"
+#include "mozilla/Variant.h"
 #include "mozilla/Vector.h"
 #include "nsCOMPtr.h"
 #include "nsCycleCollectionParticipant.h"
 #include "nsIScriptElement.h"
 
 class nsICacheInfoChannel;
 
 namespace mozilla {
@@ -129,33 +131,50 @@ public:
   {
     return mProgress == Progress::eCompiling ||
            (IsReadyToRun() && mWasCompiledOMT);
   }
 
   // Type of data provided by the nsChannel.
   enum class DataType : uint8_t {
     eUnknown,
-    eSource,
+    eTextSource,
     eBytecode
   };
 
   bool IsUnknownDataType() const
   {
     return mDataType == DataType::eUnknown;
   }
+  bool IsTextSource() const
+  {
+    return mDataType == DataType::eTextSource;
+  }
   bool IsSource() const
   {
-    return mDataType == DataType::eSource;
+    return IsTextSource();
   }
   bool IsBytecode() const
   {
     return mDataType == DataType::eBytecode;
   }
 
+  void SetUnknownDataType();
+  void SetTextSource();
+  void SetBytecode();
+
+  const Vector<char16_t>& ScriptText() const {
+    MOZ_ASSERT(IsTextSource());
+    return mScriptData->as<Vector<char16_t>>();
+  }
+  Vector<char16_t>& ScriptText() {
+    MOZ_ASSERT(IsTextSource());
+    return mScriptData->as<Vector<char16_t>>();
+  }
+
   enum class ScriptMode : uint8_t {
     eBlocking,
     eDeferred,
     eAsync
   };
 
   void SetScriptMode(bool aDeferAttr, bool aAsyncAttr);
 
@@ -175,16 +194,17 @@ public:
   }
 
   virtual bool IsTopLevel() const
   {
     // Classic scripts are always top level.
     return true;
   }
 
+  void ClearScriptSource();
 
   void MaybeCancelOffThreadScript();
   void DropBytecodeCacheReferences();
 
   using super::getNext;
   using super::isInList;
 
   const ScriptKind mKind;
@@ -204,19 +224,20 @@ public:
   bool mIsTracking;       // True if the script comes from a source on our tracking protection list.
   JS::OffThreadToken* mOffThreadToken; // Off-thread parsing token.
   nsString mSourceMapURL; // Holds source map url for loaded scripts
 
   // Holds the top-level JSScript that corresponds to the current source, once
   // it is parsed, and planned to be saved in the bytecode cache.
   JS::Heap<JSScript*> mScript;
 
-  // Holds script text for non-inline scripts. Don't use nsString so we can give
-  // ownership to jsapi.
-  mozilla::Vector<char16_t> mScriptText;
+  // Holds script source data for non-inline scripts. Don't use nsString so we
+  // can give ownership to jsapi. Holds either char16_t source text characters
+  // or nothing depending on mSourceEncoding.
+  Maybe<Variant<Vector<char16_t>, Vector<uint8_t>>> mScriptData;
 
   // Holds the SRI serialized hash and the script bytecode for non-inline
   // scripts.
   mozilla::Vector<uint8_t> mScriptBytecode;
   uint32_t mBytecodeOffset; // Offset of the bytecode in mScriptBytecode
 
   const nsCOMPtr<nsIURI> mURI;
   nsCOMPtr<nsIPrincipal> mTriggeringPrincipal;
diff --git a/dom/script/ScriptLoader.cpp b/dom/script/ScriptLoader.cpp
--- a/dom/script/ScriptLoader.cpp
+++ b/dom/script/ScriptLoader.cpp
@@ -48,16 +48,17 @@
 #include "nsIContentSecurityPolicy.h"
 #include "mozilla/Logging.h"
 #include "nsCRT.h"
 #include "nsContentCreatorFunctions.h"
 #include "nsProxyRelease.h"
 #include "nsSandboxFlags.h"
 #include "nsContentTypeParser.h"
 #include "nsINetworkPredictor.h"
+#include "nsMimeTypes.h"
 #include "mozilla/ConsoleReportCollector.h"
 
 #include "mozilla/AsyncEventDispatcher.h"
 #include "mozilla/Attributes.h"
 #include "mozilla/Telemetry.h"
 #include "mozilla/TimeStamp.h"
 #include "mozilla/Unused.h"
 #include "nsIScriptError.h"
@@ -197,27 +198,26 @@ CollectScriptTelemetry(nsIIncrementalStr
 
   // Report the type of source, as well as the size of the source.
   if (aRequest->IsLoadingSource()) {
     if (aRequest->mIsInline) {
       AccumulateCategorical(LABELS_DOM_SCRIPT_LOADING_SOURCE::Inline);
       nsAutoString inlineData;
       aRequest->mElement->GetScriptText(inlineData);
       Accumulate(DOM_SCRIPT_INLINE_SIZE, inlineData.Length());
-    } else {
+    } else if (aRequest->IsTextSource()) {
       AccumulateCategorical(LABELS_DOM_SCRIPT_LOADING_SOURCE::SourceFallback);
-      Accumulate(DOM_SCRIPT_SOURCE_SIZE, aRequest->mScriptText.length());
+      Accumulate(DOM_SCRIPT_SOURCE_SIZE, aRequest->ScriptText().length());
     }
   } else {
     MOZ_ASSERT(aRequest->IsLoading());
-    if (aRequest->IsSource()) {
+    if (aRequest->IsTextSource()) {
       AccumulateCategorical(LABELS_DOM_SCRIPT_LOADING_SOURCE::Source);
-      Accumulate(DOM_SCRIPT_SOURCE_SIZE, aRequest->mScriptText.length());
-    } else {
-      MOZ_ASSERT(aRequest->IsBytecode());
+      Accumulate(DOM_SCRIPT_SOURCE_SIZE, aRequest->ScriptText().length());
+    } else if (aRequest->IsBytecode()) {
       AccumulateCategorical(LABELS_DOM_SCRIPT_LOADING_SOURCE::AltData);
       Accumulate(DOM_SCRIPT_BYTECODE_SIZE, aRequest->mScriptBytecode.length());
     }
   }
 
   // Skip if we do not have any cache information for the given script.
   if (!aLoader) {
     return;
@@ -428,17 +428,17 @@ void ScriptLoader::ClearModuleMap() {
 }
 
 nsresult ScriptLoader::ProcessFetchedModuleSource(ModuleLoadRequest* aRequest) {
   MOZ_ASSERT(!aRequest->mModuleScript);
 
   nsresult rv = CreateModuleScript(aRequest);
   MOZ_ASSERT(NS_FAILED(rv) == !aRequest->mModuleScript);
 
-  aRequest->mScriptText.clearAndFree();
+  aRequest->ClearScriptSource();
 
   if (NS_FAILED(rv)) {
     aRequest->LoadFailed();
     return rv;
   }
 
   if (!aRequest->mIsInline) {
     SetModuleFetchFinishedAndResumeWaitingRequests(aRequest, rv);
@@ -979,17 +979,17 @@ ScriptLoader::RestartLoad(ScriptLoadRequ
   return NS_BINDING_RETARGETED;
 }
 
 nsresult
 ScriptLoader::StartLoad(ScriptLoadRequest* aRequest)
 {
   MOZ_ASSERT(aRequest->IsLoading());
   NS_ENSURE_TRUE(mDocument, NS_ERROR_NULL_POINTER);
-  aRequest->mDataType = ScriptLoadRequest::DataType::eUnknown;
+  aRequest->SetUnknownDataType();
 
   // If this document is sandboxed without 'allow-scripts', abort.
   if (mDocument->HasScriptsBlockedBySandbox()) {
     return NS_OK;
   }
 
   if (LOG_ENABLED()) {
     nsAutoCString url;
@@ -1125,20 +1125,21 @@ ScriptLoader::StartLoad(ScriptLoadReques
         cos->AddClassFlags(nsIClassOfService::TailAllowed);
       }
     }
   }
 
   nsCOMPtr<nsIHttpChannel> httpChannel(do_QueryInterface(channel));
   if (httpChannel) {
     // HTTP content negotation has little value in this context.
+    nsAutoCString acceptTypes("*/*");
     rv = httpChannel->SetRequestHeader(NS_LITERAL_CSTRING("Accept"),
-                                       NS_LITERAL_CSTRING("*/*"),
-                                       false);
+                                       acceptTypes, false);
     MOZ_ASSERT(NS_SUCCEEDED(rv));
+
     rv = httpChannel->SetReferrerWithPolicy(aRequest->mReferrer,
                                             aRequest->mReferrerPolicy);
     MOZ_ASSERT(NS_SUCCEEDED(rv));
 
     nsCOMPtr<nsIHttpChannelInternal> internalChannel(do_QueryInterface(httpChannel));
     if (internalChannel) {
       rv = internalChannel->SetIntegrityMetadata(aRequest->mIntegrity.GetIntegrityString());
       MOZ_ASSERT(NS_SUCCEEDED(rv));
@@ -1512,17 +1513,17 @@ ScriptLoader::ProcessInlineScript(nsIScr
     CreateLoadRequest(aScriptKind, mDocument->GetDocumentURI(), aElement,
                       corsMode,
                       SRIMetadata(), // SRI doesn't apply
                       mDocument->GetReferrerPolicy());
   request->mIsInline = true;
   request->mTriggeringPrincipal = mDocument->NodePrincipal();
   request->mLineNo = aElement->GetScriptLineNumber();
   request->mProgress = ScriptLoadRequest::Progress::eLoading_Source;
-  request->mDataType = ScriptLoadRequest::DataType::eSource;
+  request->SetTextSource();
   TRACE_FOR_TEST_BOOL(request->mElement, "scriptloader_load_source");
   CollectScriptTelemetry(nullptr, request);
 
   // Only the 'async' attribute is heeded on an inline module script and
   // inline classic scripts ignore both these attributes.
   MOZ_ASSERT(!aElement->GetScriptDeferred());
   MOZ_ASSERT_IF(!request->IsModuleRequest(), !aElement->GetScriptAsync());
   request->SetScriptMode(false, aElement->GetScriptAsync());
@@ -1782,56 +1783,57 @@ ScriptLoader::AttemptAsyncScriptCompile(
   JS::Rooted<JSObject*> global(cx, globalObject->GetGlobalJSObject());
   JS::CompileOptions options(cx);
 
   nsresult rv = FillCompileOptionsForRequest(jsapi, aRequest, global, &options);
   if (NS_WARN_IF(NS_FAILED(rv))) {
     return rv;
   }
 
-  if (aRequest->IsSource()) {
-    if (!JS::CanCompileOffThread(cx, options, aRequest->mScriptText.length())) {
+  if (aRequest->IsTextSource()) {
+    if (!JS::CanCompileOffThread(cx, options, aRequest->ScriptText().length())) {
       return NS_ERROR_FAILURE;
     }
   } else {
+    MOZ_ASSERT(aRequest->IsBytecode());
     size_t length = aRequest->mScriptBytecode.length() - aRequest->mBytecodeOffset;
     if (!JS::CanDecodeOffThread(cx, options, length)) {
       return NS_ERROR_FAILURE;
     }
   }
 
   RefPtr<NotifyOffThreadScriptLoadCompletedRunnable> runnable =
     new NotifyOffThreadScriptLoadCompletedRunnable(aRequest, this);
 
   if (aRequest->IsModuleRequest()) {
-    MOZ_ASSERT(aRequest->IsSource());
+    MOZ_ASSERT(aRequest->IsTextSource());
     if (!JS::CompileOffThreadModule(cx, options,
-                                    aRequest->mScriptText.begin(),
-                                    aRequest->mScriptText.length(),
+                                    aRequest->ScriptText().begin(),
+                                    aRequest->ScriptText().length(),
                                     OffThreadScriptLoaderCallback,
                                     static_cast<void*>(runnable))) {
       return NS_ERROR_OUT_OF_MEMORY;
     }
-  } else if (aRequest->IsSource()) {
-    if (!JS::CompileOffThread(cx, options,
-                              aRequest->mScriptText.begin(),
-                              aRequest->mScriptText.length(),
-                              OffThreadScriptLoaderCallback,
-                              static_cast<void*>(runnable))) {
-      return NS_ERROR_OUT_OF_MEMORY;
-    }
-  } else {
-    MOZ_ASSERT(aRequest->IsBytecode());
+  } else if (aRequest->IsBytecode()) {
     if (!JS::DecodeOffThreadScript(cx, options,
                                    aRequest->mScriptBytecode,
                                    aRequest->mBytecodeOffset,
                                    OffThreadScriptLoaderCallback,
                                    static_cast<void*>(runnable))) {
       return NS_ERROR_OUT_OF_MEMORY;
     }
+  } else {
+    MOZ_ASSERT(aRequest->IsTextSource());
+    if (!JS::CompileOffThread(cx, options,
+                              aRequest->ScriptText().begin(),
+                              aRequest->ScriptText().length(),
+                              OffThreadScriptLoaderCallback,
+                              static_cast<void*>(runnable))) {
+      return NS_ERROR_OUT_OF_MEMORY;
+    }
   }
 
   mDocument->BlockOnload();
 
   // Once the compilation is finished, an event would be added to the event loop
   // to call ScriptLoader::ProcessOffThreadRequest with the same request.
   aRequest->mProgress = ScriptLoadRequest::Progress::eCompiling;
 
@@ -1868,18 +1870,18 @@ ScriptLoader::GetScriptSource(ScriptLoad
     // XXX This is inefficient - GetText makes multiple
     // copies.
     aRequest->mElement->GetScriptText(inlineData);
     return SourceBufferHolder(inlineData.get(),
                               inlineData.Length(),
                               SourceBufferHolder::NoOwnership);
   }
 
-  return SourceBufferHolder(aRequest->mScriptText.begin(),
-                            aRequest->mScriptText.length(),
+  return SourceBufferHolder(aRequest->ScriptText().begin(),
+                            aRequest->ScriptText().length(),
                             SourceBufferHolder::NoOwnership);
 }
 
 nsresult
 ScriptLoader::ProcessRequest(ScriptLoadRequest* aRequest)
 {
   LOG(("ScriptLoadRequest (%p): Process request", aRequest));
 
@@ -1977,17 +1979,17 @@ ScriptLoader::ProcessRequest(ScriptLoadR
     // (disappearing window, some other error, ...). Finish the
     // request to avoid leaks in the JS engine.
     MOZ_ASSERT(!aRequest->IsModuleRequest());
     aRequest->MaybeCancelOffThreadScript();
   }
 
   // Free any source data, but keep the bytecode content as we might have to
   // save it later.
-  aRequest->mScriptText.clearAndFree();
+  aRequest->ClearScriptSource();
   if (aRequest->IsBytecode()) {
     // We received bytecode as input, thus we were decoding, and we will not be
     // encoding the bytecode once more. We can safely clear the content of this
     // buffer.
     aRequest->mScriptBytecode.clearAndFree();
   }
 
   return rv;
@@ -2144,19 +2146,27 @@ ScriptLoader::ShouldCacheBytecode(Script
       fetchCountMin = 4;
       break;
     }
   }
 
   // If the script is too small/large, do not attempt at creating a bytecode
   // cache for this script, as the overhead of parsing it might not be worth the
   // effort.
-  if (hasSourceLengthMin && aRequest->mScriptText.length() < sourceLengthMin) {
-    LOG(("ScriptLoadRequest (%p): Bytecode-cache: Script is too small.", aRequest));
-    return false;
+  if (hasSourceLengthMin) {
+    size_t sourceLength;
+    size_t minLength;
+
+    sourceLength = aRequest->ScriptText().length();
+    minLength = sourceLengthMin;
+
+    if (sourceLength < minLength) {
+      LOG(("ScriptLoadRequest (%p): Bytecode-cache: Script is too small.", aRequest));
+      return false;
+    }
   }
 
   // Check that we loaded the cache entry a few times before attempting any
   // bytecode-cache optimization, such that we do not waste time on entry which
   // are going to be dropped soon.
   if (hasFetchCountMin) {
     int32_t fetchCount = 0;
     if (NS_FAILED(aRequest->mCacheInfo->GetCacheTokenFetchCount(&fetchCount))) {
@@ -2219,17 +2229,17 @@ ScriptLoader::EvaluateScript(ScriptLoadR
 
   bool oldProcessingScriptTag = context->GetProcessingScriptTag();
   context->SetProcessingScriptTag(true);
   nsresult rv;
   {
     if (aRequest->IsModuleRequest()) {
       // When a module is already loaded, it is not feched a second time and the
       // mDataType of the request might remain set to DataType::Unknown.
-      MOZ_ASSERT(!aRequest->IsBytecode());
+      MOZ_ASSERT(aRequest->IsTextSource() || aRequest->IsUnknownDataType());
       LOG(("ScriptLoadRequest (%p): Evaluate Module", aRequest));
 
       // currentScript is set to null for modules.
       AutoCurrentScriptUpdater scriptUpdater(this, nullptr);
 
       EnsureModuleResolveHook(cx);
 
       ModuleLoadRequest* request = aRequest->AsModuleRequest();
@@ -2290,44 +2300,42 @@ ScriptLoader::EvaluateScript(ScriptLoadR
           // bytecode.
           MOZ_ASSERT(!aRequest->mCacheInfo);
         } else {
           MOZ_ASSERT(aRequest->IsSource());
           JS::Rooted<JSScript*> script(cx);
           bool encodeBytecode = ShouldCacheBytecode(aRequest);
 
           TimeStamp start;
-          if (Telemetry::CanRecordExtended()) {
-            // Only record telemetry for scripts which are above the threshold.
-            if (aRequest->mCacheInfo && aRequest->mScriptText.length() >= 1024) {
-              start = TimeStamp::Now();
-            }
-          }
 
           {
             nsJSUtils::ExecutionContext exec(cx, global);
             exec.SetEncodeBytecode(encodeBytecode);
             TRACE_FOR_TEST(aRequest->mElement, "scriptloader_execute");
             if (aRequest->mOffThreadToken) {
               // Off-main-thread parsing.
               LOG(("ScriptLoadRequest (%p): Join (off-thread parsing) and Execute",
                    aRequest));
+              MOZ_ASSERT(aRequest->IsTextSource());
               rv = exec.JoinAndExec(&aRequest->mOffThreadToken, &script);
+
               if (start) {
                 AccumulateTimeDelta(encodeBytecode
                                     ? DOM_SCRIPT_OFF_THREAD_PARSE_ENCODE_EXEC_MS
                                     : DOM_SCRIPT_OFF_THREAD_PARSE_EXEC_MS,
                                     start);
               }
             } else {
               // Main thread parsing (inline and small scripts)
               LOG(("ScriptLoadRequest (%p): Compile And Exec", aRequest));
+              MOZ_ASSERT(aRequest->IsTextSource());
               nsAutoString inlineData;
               SourceBufferHolder srcBuf = GetScriptSource(aRequest, inlineData);
               rv = exec.CompileAndExec(options, srcBuf, &script);
+
               if (start) {
                 AccumulateTimeDelta(encodeBytecode
                                     ? DOM_SCRIPT_MAIN_THREAD_PARSE_ENCODE_EXEC_MS
                                     : DOM_SCRIPT_MAIN_THREAD_PARSE_EXEC_MS,
                                     start);
               }
             }
           }
