# HG changeset patch
# User Tooru Fujisawa <arai_a@mac.com>
# Date 1552982525 0
#      Tue Mar 19 08:02:05 2019 +0000
# Node ID 106a5731e7f4c9982443c97b1569b63b8a7e18cd
# Parent  6b7d5fe2fcda1bbaca9f7175c88505292108f3ae
Bug 1530544 - Use consistent question style for prompt inside bootstrap and mercurial-setup. r=glandium

Changed the following:
  - Use (Yn) instead of [Y/n] to align with mercurial-setup that is using
    mercurial's ui class
  - Use (Yn) prompt instead of int prompt where the options are Yes/No
  - Remove empty lines around options to align with the first question
    about Firefox {Desktop,Mobile} {,non-}Artifact
  - Move the question `Would you like to enable build system telemetry?`
    to the last line
  - Use `Your choice:` propmt for int prompt to align with the first question

Differential Revision: https://phabricator.services.mozilla.com/D23457

diff --git a/python/mozboot/mozboot/base.py b/python/mozboot/mozboot/base.py
--- a/python/mozboot/mozboot/base.py
+++ b/python/mozboot/mozboot/base.py
@@ -436,17 +436,17 @@ class BaseBootstrapper(object):
             return choice
         else:
             raise Exception("Error! Reached max attempts of entering option.")
 
     def prompt_yesno(self, prompt):
         ''' Prompts the user with prompt and requires a yes/no answer.'''
         valid = False
         while not valid:
-            choice = raw_input(prompt + ' [Y/n]: ').strip().lower()[:1]
+            choice = raw_input(prompt + ' (Yn): ').strip().lower()[:1]
             if choice == '':
                 choice = 'y'
             if choice not in ('y', 'n'):
                 print('ERROR! Please enter y or n!')
             else:
                 valid = True
 
         return choice == 'y'
diff --git a/python/mozboot/mozboot/bootstrap.py b/python/mozboot/mozboot/bootstrap.py
--- a/python/mozboot/mozboot/bootstrap.py
+++ b/python/mozboot/mozboot/bootstrap.py
@@ -73,22 +73,17 @@ in a common directory on the filesystem.
 is:
 
   {statedir}
 
 If you would like to use a different directory, hit CTRL+c and set the
 MOZBUILD_STATE_PATH environment variable to the directory you'd like to
 use and re-run the bootstrapper.
 
-Would you like to create this directory?
-
-  1. Yes
-  2. No
-
-Your choice: '''
+Would you like to create this directory? (Yn):'''
 
 STYLO_NODEJS_DIRECTORY_MESSAGE = '''
 Stylo and NodeJS packages require a directory to store shared, persistent
 state.  On this machine, that directory is:
 
   {statedir}
 
 Please restart bootstrap and create that directory when prompted.
@@ -119,52 +114,40 @@ Or, if you really prefer vanilla flavor 
     git clone https://github.com/mozilla/gecko-dev.git
 '''
 
 CONFIGURE_MERCURIAL = '''
 Mozilla recommends a number of changes to Mercurial to enhance your
 experience with it.
 
 Would you like to run a configuration wizard to ensure Mercurial is
-optimally configured?
-
-  1. Yes
-  2. No
-
-Please enter your reply: '''
+optimally configured?'''
 
 CONFIGURE_GIT = '''
 Mozilla recommends using git-cinnabar to work with mozilla-central.
 
 Would you like to run a few configuration steps to ensure Git is
-optimally configured?
-
-  1. Yes
-  2. No
-
-Please enter your reply: '''
+optimally configured?'''
 
 CLONE_VCS = '''
 If you would like to clone the {} {} repository, please
 enter the destination path below.
 '''
 
 CLONE_VCS_PROMPT = '''
 Destination directory for {} clone (leave empty to not clone): '''.lstrip()
 
-CLONE_VCS_NOT_EMPTY = '''
+CLONE_VCS_NOT_EMPTY = '''\
 Destination directory '{}' is not empty.
 
 Would you like to clone to '{}' instead?
-
   1. Yes
   2. No, let me enter another path
   3. No, stop cloning
-
-Please enter your reply: '''.lstrip()
+Your choice: '''
 
 CLONE_VCS_NOT_EMPTY_FALLBACK_FAILED = '''
 ERROR! Destination directory '{}' is not empty and '{}' exists.
 '''
 
 CLONE_VCS_NOT_DIR = '''
 ERROR! Destination '{}' exists but is not a directory.
 '''
@@ -261,17 +244,16 @@ class Bootstrapper(object):
                                       'for your OS.')
 
         self.instance = cls(**args)
 
     def input_clone_dest(self, with_hg=True):
         repo_name = 'mozilla-unified'
         vcs = 'Mercurial'
         if not with_hg:
-            repo_name = 'gecko'
             vcs = 'Git'
         print(CLONE_VCS.format(repo_name, vcs))
 
         while True:
             dest = raw_input(CLONE_VCS_PROMPT.format(vcs))
             dest = dest.strip()
             if not dest:
                 return ''
@@ -306,22 +288,18 @@ class Bootstrapper(object):
     # be available. We /could/ refactor parts of mach_bootstrap.py to be
     # part of this directory to avoid the code duplication.
     def try_to_create_state_dir(self):
         state_dir = get_state_dir()
 
         if not os.path.exists(state_dir):
             should_create_state_dir = True
             if not self.instance.no_interactive:
-                choice = self.instance.prompt_int(
-                    prompt=STATE_DIR_INFO.format(statedir=state_dir),
-                    low=1,
-                    high=2)
-
-                should_create_state_dir = choice == 1
+                should_create_state_dir = self.instance.prompt_yesno(
+                    prompt=STATE_DIR_INFO.format(statedir=state_dir))
 
             # This directory is by default in $HOME, or overridden via an env
             # var, so we probably shouldn't gate it on --no-system-changes.
             if should_create_state_dir:
                 print('Creating global state directory: %s' % state_dir)
                 os.makedirs(state_dir, mode=0o770)
 
         state_dir_available = os.path.exists(state_dir)
@@ -349,17 +327,17 @@ class Bootstrapper(object):
         self.instance.ensure_stylo_packages(state_dir, checkout_root)
         self.instance.ensure_clang_static_analysis_package(state_dir, checkout_root)
         self.instance.ensure_nasm_packages(state_dir, checkout_root)
 
     def bootstrap(self):
         if self.choice is None:
             # Like ['1. Firefox for Desktop', '2. Firefox for Android Artifact Mode', ...].
             labels = ['%s. %s' % (i + 1, name) for (i, (name, _)) in enumerate(APPLICATIONS_LIST)]
-            prompt = APPLICATION_CHOICE % '\n'.join(labels)
+            prompt = APPLICATION_CHOICE % '\n'.join('  {}'.format(label) for label in labels)
             prompt_choice = self.instance.prompt_int(prompt=prompt, low=1, high=len(APPLICATIONS))
             name, application = APPLICATIONS_LIST[prompt_choice-1]
         elif self.choice not in APPLICATIONS.keys():
             raise Exception('Please pick a valid application choice: (%s)' %
                             '/'.join(APPLICATIONS.keys()))
         else:
             name, application = APPLICATIONS[self.choice]
 
@@ -398,34 +376,28 @@ class Bootstrapper(object):
                                      hg=self.instance.which('hg'))
         (checkout_type, checkout_root) = r
 
         # Possibly configure Mercurial, but not if the current checkout or repo
         # type is Git.
         if hg_installed and state_dir_available and (checkout_type == 'hg' or self.vcs == 'hg'):
             configure_hg = False
             if not self.instance.no_interactive:
-                choice = self.instance.prompt_int(prompt=CONFIGURE_MERCURIAL,
-                                                  low=1, high=2)
-                if choice == 1:
-                    configure_hg = True
+                configure_hg = self.instance.prompt_yesno(prompt=CONFIGURE_MERCURIAL)
             else:
                 configure_hg = self.hg_configure
 
             if configure_hg:
                 configure_mercurial(self.instance.which('hg'), state_dir)
 
         # Offer to configure Git, if the current checkout or repo type is Git.
         elif self.instance.which('git') and (checkout_type == 'git' or self.vcs == 'git'):
             should_configure_git = False
             if not self.instance.no_interactive:
-                choice = self.instance.prompt_int(prompt=CONFIGURE_GIT,
-                                                  low=1, high=2)
-                if choice == 1:
-                    should_configure_git = True
+                should_configure_git = self.instance.prompt_yesno(prompt=CONFIGURE_GIT)
             else:
                 # Assuming default configuration setting applies to all VCS.
                 should_configure_git = self.hg_configure
 
             if should_configure_git:
                 configure_git(self.instance.which('git'), state_dir,
                               checkout_root)
 
diff --git a/python/mozboot/mozboot/debian.py b/python/mozboot/mozboot/debian.py
--- a/python/mozboot/mozboot/debian.py
+++ b/python/mozboot/mozboot/debian.py
@@ -14,23 +14,20 @@ may become out of date. This may cause i
 Mercurial extensions that rely on new Mercurial features. As a result,
 you may not have an optimal version control experience.
 
 To have the best Mercurial experience possible, we recommend installing
 Mercurial via the "pip" Python packaging utility. This will likely result
 in files being placed in /usr/local/bin and /usr/local/lib.
 
 How would you like to continue?
-
-1) Install a modern Mercurial via pip (recommended)
-2) Install a legacy Mercurial via apt
-3) Do not install Mercurial
-
-Choice:
-'''.strip()
+  1. Install a modern Mercurial via pip (recommended)
+  2. Install a legacy Mercurial via apt
+  3. Do not install Mercurial
+Your choice: '''
 
 
 class DebianBootstrapper(NasmInstall, NodeInstall, StyloInstall, ClangStaticAnalysisInstall,
                          BaseBootstrapper):
     # These are common packages for all Debian-derived distros (such as
     # Ubuntu).
     COMMON_PACKAGES = [
         'autoconf2.13',
diff --git a/python/mozboot/mozboot/osx.py b/python/mozboot/mozboot/osx.py
--- a/python/mozboot/mozboot/osx.py
+++ b/python/mozboot/mozboot/osx.py
@@ -105,20 +105,19 @@ output as packages are built.
 PACKAGE_MANAGER_OLD_CLANG = '''
 We require a newer compiler than what is provided by your version of Xcode.
 
 We will install a modern version of Clang through %s.
 '''
 
 PACKAGE_MANAGER_CHOICE = '''
 Please choose a package manager you'd like:
-1. Homebrew
-2. MacPorts (Does not yet support bootstrapping Firefox for Android.)
-Your choice:
-'''
+  1. Homebrew
+  2. MacPorts (Does not yet support bootstrapping Firefox for Android.)
+Your choice: '''
 
 NO_PACKAGE_MANAGER_WARNING = '''
 It seems you don't have any supported package manager installed.
 '''
 
 PACKAGE_MANAGER_EXISTS = '''
 Looks like you have %s installed. We will install all required packages via %s.
 '''
