# HG changeset patch
# User Dave Hunt <dhunt@mozilla.com>
# Date 1539610590 0
# Node ID 4ccf0f4b0ef23fc71f5593ca269e625c0cca5d56
# Parent  39430a2c5a41a86df3606868ada9c79586db3640
Bug 1490253 - Update documentation on vendoring Python packages based on switch to pip-tools; r=ahal

Differential Revision: https://phabricator.services.mozilla.com/D8205

diff --git a/python/docs/index.rst b/python/docs/index.rst
--- a/python/docs/index.rst
+++ b/python/docs/index.rst
@@ -32,34 +32,23 @@ Adding a Python package
 To vendor a Python package, run ``mach vendor python [PACKAGE]``, where
 ``[PACKAGE]`` is one or more package names along with a version number in the
 format ``pytest==3.5.1``. The package will be installed, transient dependencies
 will be determined, and a ``requirements.txt`` file will be generated with the
 full list of dependencies. The requirements file is then used with ``pip`` to
 download and extract the source distributions of all packages into the
 ``third_party/python`` directory.
 
-If you're familiar with ``Pipfile`` you can also directly modify this in the in
-the top source directory and then run ``mach vendor python`` for your changes
-to take effect. This allows advanced options such as specifying alternative
-package indexes (see below), and
-`PEP 508 specifiers <https://www.python.org/dev/peps/pep-0508/>`_.
-
-Note that the `specification <https://github.com/pypa/pipfile>`_ for
-``Pipfile`` and ``Pipfile.lock`` is still in active development. More
-information can be found in the
-`Pipenv documentation <https://docs.pipenv.org/>`_, which is the reference
-implementation we're using.
+Alternatively, you can also modify the direct dependencies in
+``third_party/python/requirements.in`` and then run ``mach vendor python`` for your
+changes to take effect.
 
 What if the package isn't on PyPI?
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 
-If the package is available on another Python package index, then you can add
-these details to ``Pipfile`` by
-`specifying package indexes <https://docs.pipenv.org/advanced/#specifying-package-indexes>`_.
 If the package isn't available on any Python package index, then you can
 manually copy the source distribution into the ``third_party/python`` directory.
 
 Using a Python package index
 ============================
 
 If the Python package is not used in the building of Firefox then it can be
 installed from a package index. Some tasks are not permitted to use external
