# HG changeset patch
# User Andrew Halberstadt <ahalberstadt@mozilla.com>
# Date 1563240115 0
# Node ID 0c04c1b1f297a4181161208af576b58f4f020164
# Parent  b2bca0b098a28f85e3c6d6421b1f4f21d8c6d1c7
Bug 1473498 - Fixing Py3 compatibility errors reachable from testing/mach_commands.py r=gbrown

Differential Revision: https://phabricator.services.mozilla.com/D37762

diff --git a/testing/mach_commands.py b/testing/mach_commands.py
--- a/testing/mach_commands.py
+++ b/testing/mach_commands.py
@@ -16,17 +16,16 @@ import shutil
 from mach.decorators import (
     CommandArgument,
     CommandProvider,
     Command,
     SettingsProvider,
 )
 
 from mozbuild.base import MachCommandBase, MachCommandConditions as conditions
-from moztest.resolve import TEST_SUITES
 from argparse import ArgumentParser
 
 UNKNOWN_TEST = '''
 I was unable to find tests from the given argument(s).
 
 You should specify a test directory, filename, test suite name, or
 abbreviation. If no arguments are given, there must be local file
 changes and corresponding IMPACTED_TESTS annotations in moz.build
@@ -42,19 +41,18 @@ UNKNOWN_FLAVOR = '''
 I know you are trying to run a %s%s test. Unfortunately, I can't run those
 tests yet. Sorry!
 '''.strip()
 
 TEST_HELP = '''
 Test or tests to run. Tests can be specified by filename, directory, suite
 name or suite alias.
 
-The following test suites and aliases are supported: %s
-''' % ', '.join(sorted(TEST_SUITES))
-TEST_HELP = TEST_HELP.strip()
+The following test suites and aliases are supported: {}
+'''.strip()
 
 
 @SettingsProvider
 class TestConfig(object):
 
     @classmethod
     def config_settings(cls):
         from mozlog.commandline import log_formatters
@@ -66,18 +64,20 @@ class TestConfig(object):
         return [
             ('test.format', 'string', format_desc, 'mach', {'choices': format_choices}),
             ('test.level', 'string', level_desc, 'info', {'choices': level_choices}),
         ]
 
 
 def get_test_parser():
     from mozlog.commandline import add_logging_group
+    from moztest.resolve import TEST_SUITES
     parser = argparse.ArgumentParser()
-    parser.add_argument('what', default=None, nargs='*', help=TEST_HELP)
+    parser.add_argument('what', default=None, nargs='+',
+                        help=TEST_HELP.format(', '.join(sorted(TEST_SUITES))))
     parser.add_argument('extra_args', default=None, nargs=argparse.REMAINDER,
                         help="Extra arguments to pass to the underlying test command(s). "
                              "If an underlying command doesn't recognize the argument, it "
                              "will fail.")
     add_logging_group(parser)
     return parser
 
 
diff --git a/testing/mach_commands.py.1473498.later b/testing/mach_commands.py.1473498.later
new file mode 100644
--- /dev/null
+++ b/testing/mach_commands.py.1473498.later
@@ -0,0 +1,20 @@
+--- mach_commands.py
++++ mach_commands.py
+@@ -134,16 +134,17 @@ def create_parser_addtest():
+ @CommandProvider
+ class AddTest(MachCommandBase):
+     @Command('addtest', category='testing',
+              description='Generate tests based on templates',
+              parser=create_parser_addtest)
+     def addtest(self, suite=None, test=None, doc=None, overwrite=False,
+                 editor=MISSING_ARG, **kwargs):
+         import addtest
++        from moztest.resolve import TEST_SUITES
+ 
+         if not suite and not test:
+             return create_parser_addtest().parse_args(["--help"])
+ 
+         if suite in SUITE_SYNONYMS:
+             suite = SUITE_SYNONYMS[suite]
+ 
+         if test:
diff --git a/testing/mozbase/manifestparser/manifestparser/manifestparser.py b/testing/mozbase/manifestparser/manifestparser/manifestparser.py
--- a/testing/mozbase/manifestparser/manifestparser/manifestparser.py
+++ b/testing/mozbase/manifestparser/manifestparser/manifestparser.py
@@ -1,34 +1,35 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this file,
 # You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import, print_function
 
-from StringIO import StringIO
+from io import BytesIO
 import json
 import fnmatch
 import os
 import shutil
 import sys
 import types
 
+from six import string_types
+
 from .ini import read_ini
 from .filters import (
     DEFAULT_FILTERS,
     enabled,
     exists as _exists,
     filterlist,
 )
 
 __all__ = ['ManifestParser', 'TestManifest', 'convert']
 
 relpath = os.path.relpath
-string = (basestring,)
 
 
 # path normalization
 
 def normalize_path(path):
     """normalize a relative path"""
     if sys.platform.startswith('win'):
         return path.replace('/', os.path.sep)
@@ -115,17 +116,17 @@ class ManifestParser(object):
                 if self.strict:
                     raise IOError(message)
                 else:
                     sys.stderr.write("%s\n" % message)
                     return
             return include_file
 
         # get directory of this file if not file-like object
-        if isinstance(filename, string):
+        if isinstance(filename, string_types):
             # If we're using mercurial as our filesystem via a finder
             # during manifest reading, the getcwd() calls that happen
             # with abspath calls will not be meaningful, so absolute
             # paths are required.
             if self.finder:
                 assert os.path.isabs(filename)
             filename = os.path.abspath(filename)
             self.source_files.add(filename)
@@ -246,30 +247,30 @@ class ManifestParser(object):
         read and add manifests from file paths or file-like objects
 
         filenames -- file paths or file-like objects to read as manifests
         defaults -- default variables
         """
 
         # ensure all files exist
         missing = [filename for filename in filenames
-                   if isinstance(filename, string) and not self.path_exists(filename)]
+                   if isinstance(filename, string_types) and not self.path_exists(filename)]
         if missing:
             raise IOError('Missing files: %s' % ', '.join(missing))
 
         # default variables
         _defaults = defaults.copy() or self._defaults.copy()
         _defaults.setdefault('here', None)
 
         # process each file
         for filename in filenames:
             # set the per file defaults
             defaults = _defaults.copy()
             here = None
-            if isinstance(filename, string):
+            if isinstance(filename, string_types):
                 here = os.path.dirname(os.path.abspath(filename))
                 defaults['here'] = here  # directory of master .ini file
 
             if self.rootdir is None:
                 # set the root directory
                 # == the directory of the first manifest given
                 self.rootdir = here
 
@@ -384,17 +385,17 @@ class ManifestParser(object):
     def verifyDirectory(self, directories, pattern=None, extensions=None):
         """
         checks what is on the filesystem vs what is in a manifest
         returns a 2-tuple of sets:
         (missing_from_filesystem, missing_from_manifest)
         """
 
         files = set([])
-        if isinstance(directories, basestring):
+        if isinstance(directories, string_types):
             directories = [directories]
 
         # get files in directories
         for directory in directories:
             for dirpath, dirnames, filenames in os.walk(directory, topdown=True):
 
                 # only add files that match a pattern
                 if pattern:
@@ -421,17 +422,17 @@ class ManifestParser(object):
         write a manifest given a query
         global and local options will be munged to do the query
         globals will be written to the top of the file
         locals (if given) will be written per test
         """
 
         # open file if `fp` given as string
         close = False
-        if isinstance(fp, string):
+        if isinstance(fp, string_types):
             fp = file(fp, 'w')
             close = True
 
         # root directory
         if rootdir is None:
             rootdir = self.rootdir
 
         # sanitize input
@@ -483,17 +484,17 @@ class ManifestParser(object):
                 print('%s = %s' % (key, test[key]), file=fp)
             print(file=fp)
 
         if close:
             # close the created file
             fp.close()
 
     def __str__(self):
-        fp = StringIO()
+        fp = BytesIO()
         self.write(fp=fp)
         value = fp.getvalue()
         return value
 
     def copy(self, directory, rootdir=None, *tags, **kwargs):
         """
         copy the manifests and associated tests
         - directory : directory to copy to
@@ -577,17 +578,17 @@ class ManifestParser(object):
     # directory importers
 
     @classmethod
     def _walk_directories(cls, directories, callback, pattern=None, ignore=()):
         """
         internal function to import directories
         """
 
-        if isinstance(pattern, basestring):
+        if isinstance(pattern, string_types):
             patterns = [pattern]
         else:
             patterns = pattern
         ignore = set(ignore)
 
         if not patterns:
             def accept_filename(filename):
                 return True
@@ -692,29 +693,29 @@ class ManifestParser(object):
     @classmethod
     def from_directories(cls, directories, pattern=None, ignore=(), write=None, relative_to=None):
         """
         convert directories to a simple manifest; returns ManifestParser instance
 
         pattern -- shell pattern (glob) or patterns of filenames to match
         ignore -- directory names to ignore
         write -- filename or file-like object of manifests to write;
-                 if `None` then a StringIO instance will be created
+                 if `None` then a BytesIO instance will be created
         relative_to -- write paths relative to this path;
                        if false then the paths are absolute
         """
 
         # determine output
         opened_manifest_file = None  # name of opened manifest file
         absolute = not relative_to  # whether to output absolute path names as names
-        if isinstance(write, string):
+        if isinstance(write, string_types):
             opened_manifest_file = write
             write = file(write, 'w')
         if write is None:
-            write = StringIO()
+            write = BytesIO()
 
         # walk the directories, generating manifests
         def callback(directory, dirpath, dirnames, filenames):
 
             # absolute paths
             filenames = [os.path.join(dirpath, filename)
                          for filename in filenames]
             # ensure new manifest isn't added
diff --git a/testing/mozbase/moztest/moztest/resolve.py b/testing/mozbase/moztest/moztest/resolve.py
--- a/testing/mozbase/moztest/moztest/resolve.py
+++ b/testing/mozbase/moztest/moztest/resolve.py
@@ -1,15 +1,15 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import, print_function, unicode_literals
 
-import cPickle as pickle
+import pickle
 import os
 import sys
 from collections import defaultdict
 
 import manifestparser
 import mozpack.path as mozpath
 from mozbuild.base import MozbuildObject
 from mozbuild.util import OrderedDefaultDict
diff --git a/testing/mozharness/mach_commands.py b/testing/mozharness/mach_commands.py
--- a/testing/mozharness/mach_commands.py
+++ b/testing/mozharness/mach_commands.py
@@ -4,20 +4,20 @@
 
 from __future__ import absolute_import, print_function, unicode_literals
 
 import argparse
 import os
 import re
 import subprocess
 import sys
-import urllib
-import urlparse
 
 import mozinfo
+from six.moves.urllib.parse import urljoin
+from six.moves.urllib.request import pathname2url
 
 from mach.decorators import (
     CommandArgument,
     CommandProvider,
     Command,
 )
 
 from mozbuild.base import MachCommandBase, MozbuildObject
@@ -127,17 +127,17 @@ class MozharnessRunner(MozbuildObject):
                 "script": "marionette.py",
                 "config": ["--config-file", self.config_path("marionette",
                                                              "test_config.py")]
             },
         }
 
 
     def path_to_url(self, path):
-        return urlparse.urljoin('file:', urllib.pathname2url(path))
+        return urljoin('file:', pathname2url(path))
 
     def _installer_url(self):
         package_re = {
             "linux": re.compile("^firefox-\d+\..+\.tar\.bz2$"),
             "win": re.compile("^firefox-\d+\..+\.installer\.exe$"),
             "mac": re.compile("^firefox-\d+\..+\.mac(?:64)?\.dmg$"),
         }[mozinfo.info['os']]
         dist_path = os.path.join(self.topobjdir, "dist")
