# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1536055538 -7200
#      Tue Sep 04 12:05:38 2018 +0200
# Node ID 78a325eb7c6a9922c064a12f5402f80c66cfed2f
# Parent  43a95f0f47256ac54ce6bb2044216de7a9406574
Bug 1466392 - Disallow setting JIT compiler options when there are worker threads, to avoid races. r=nbp

diff --git a/js/src/builtin/TestingFunctions.cpp b/js/src/builtin/TestingFunctions.cpp
--- a/js/src/builtin/TestingFunctions.cpp
+++ b/js/src/builtin/TestingFunctions.cpp
@@ -2588,85 +2588,16 @@ js::testingFunc_assertRecoveredOnBailout
     }
 
     // NOP when not in IonMonkey
     args.rval().setUndefined();
     return true;
 }
 
 static bool
-SetJitCompilerOption(JSContext* cx, unsigned argc, Value* vp)
-{
-    CallArgs args = CallArgsFromVp(argc, vp);
-    RootedObject callee(cx, &args.callee());
-
-    if (args.length() != 2) {
-        ReportUsageErrorASCII(cx, callee, "Wrong number of arguments.");
-        return false;
-    }
-
-    if (!args[0].isString()) {
-        ReportUsageErrorASCII(cx, callee, "First argument must be a String.");
-        return false;
-    }
-
-    if (!args[1].isInt32()) {
-        ReportUsageErrorASCII(cx, callee, "Second argument must be an Int32.");
-        return false;
-    }
-
-    JSFlatString* strArg = JS_FlattenString(cx, args[0].toString());
-    if (!strArg)
-        return false;
-
-#define JIT_COMPILER_MATCH(key, string)                 \
-    else if (JS_FlatStringEqualsAscii(strArg, string))  \
-        opt = JSJITCOMPILER_ ## key;
-
-    JSJitCompilerOption opt = JSJITCOMPILER_NOT_AN_OPTION;
-    if (false) {}
-    JIT_COMPILER_OPTIONS(JIT_COMPILER_MATCH);
-#undef JIT_COMPILER_MATCH
-
-    if (opt == JSJITCOMPILER_NOT_AN_OPTION) {
-        ReportUsageErrorASCII(cx, callee, "First argument does not name a valid option (see jsapi.h).");
-        return false;
-    }
-
-    int32_t number = args[1].toInt32();
-    if (number < 0)
-        number = -1;
-
-    // Throw if disabling the JITs and there's JIT code on the stack, to avoid
-    // assertion failures.
-    if ((opt == JSJITCOMPILER_BASELINE_ENABLE || opt == JSJITCOMPILER_ION_ENABLE) &&
-        number == 0)
-    {
-        js::jit::JitActivationIterator iter(cx);
-        if (!iter.done()) {
-            JS_ReportErrorASCII(cx, "Can't turn off JITs with JIT code on the stack.");
-            return false;
-        }
-    }
-
-    // JIT compiler options are process-wide, so we have to stop off-thread
-    // compilations for all runtimes to avoid races.
-    HelperThreadState().waitForAllThreads();
-
-    // Only release JIT code for the current runtime because there's no good
-    // way to discard code for other runtimes.
-    ReleaseAllJITCode(cx->runtime()->defaultFreeOp());
-
-    JS_SetGlobalJitCompilerOption(cx, opt, uint32_t(number));
-
-    args.rval().setUndefined();
-    return true;
-}
-
-static bool
 GetJitCompilerOptions(JSContext* cx, unsigned argc, Value* vp)
 {
     CallArgs args = CallArgsFromVp(argc, vp);
     RootedObject info(cx, JS_NewPlainObject(cx));
     if (!info)
         return false;
 
     uint32_t intValue = 0;
@@ -5606,20 +5537,16 @@ gc::ZealModeHelpText),
 "  This behaviour ensures that a falsy value means that we are not in ion, but expect a\n"
 "  compilation to occur in the future. Conversely, a truthy value means that we are either in\n"
 "  ion or that there is litle or no chance of ion ever compiling the current script."),
 
     JS_FN_HELP("assertJitStackInvariants", TestingFunc_assertJitStackInvariants, 0, 0,
 "assertJitStackInvariants()",
 "  Iterates the Jit stack and check that stack invariants hold."),
 
-    JS_FN_HELP("setJitCompilerOption", SetJitCompilerOption, 2, 0,
-"setCompilerOption(<option>, <number>)",
-"  Set a compiler option indexed in JSCompileOption enum to a number.\n"),
-
     JS_FN_HELP("setIonCheckGraphCoherency", SetIonCheckGraphCoherency, 1, 0,
 "setIonCheckGraphCoherency(bool)",
 "  Set whether Ion should perform graph consistency (DEBUG-only) assertions. These assertions\n"
 "  are valuable and should be generally enabled, however they can be very expensive for large\n"
 "  (wasm) programs."),
 
     JS_FN_HELP("serialize", Serialize, 1, 0,
 "serialize(data, [transferables, [policy]])",
diff --git a/js/src/jit-test/tests/basic/evalInWorker-jit-options.js b/js/src/jit-test/tests/basic/evalInWorker-jit-options.js
new file mode 100644
--- /dev/null
+++ b/js/src/jit-test/tests/basic/evalInWorker-jit-options.js
@@ -0,0 +1,12 @@
+if (helperThreadCount() === 0)
+    quit();
+var fun = function() {
+    var ex;
+    try {
+        setJitCompilerOption("baseline.warmup.trigger", 5);
+    } catch(e) {
+        ex = e;
+    }
+    assertEq(ex && ex.toString().includes("Can't set"), true);
+}
+evalInWorker("(" + fun.toString() + ")()");
diff --git a/js/src/shell/js.cpp b/js/src/shell/js.cpp
--- a/js/src/shell/js.cpp
+++ b/js/src/shell/js.cpp
@@ -4111,16 +4111,93 @@ SetInterruptCallback(JSContext* cx, unsi
     GetShellContext(cx)->interruptFunc = value;
     GetShellContext(cx)->haveInterruptFunc = true;
 
     args.rval().setUndefined();
     return true;
 }
 
 static bool
+SetJitCompilerOption(JSContext* cx, unsigned argc, Value* vp)
+{
+    CallArgs args = CallArgsFromVp(argc, vp);
+    RootedObject callee(cx, &args.callee());
+
+    if (args.length() != 2) {
+        ReportUsageErrorASCII(cx, callee, "Wrong number of arguments.");
+        return false;
+    }
+
+    if (!args[0].isString()) {
+        ReportUsageErrorASCII(cx, callee, "First argument must be a String.");
+        return false;
+    }
+
+    if (!args[1].isInt32()) {
+        ReportUsageErrorASCII(cx, callee, "Second argument must be an Int32.");
+        return false;
+    }
+
+    // Disallow setting JIT options when there are worker threads, to avoid
+    // races.
+    if (workerThreadsLock) {
+        ReportUsageErrorASCII(cx, callee,
+                              "Can't set JIT options when there are worker threads.");
+        return false;
+    }
+
+    JSFlatString* strArg = JS_FlattenString(cx, args[0].toString());
+    if (!strArg)
+        return false;
+
+#define JIT_COMPILER_MATCH(key, string)                 \
+    else if (JS_FlatStringEqualsAscii(strArg, string))  \
+        opt = JSJITCOMPILER_ ## key;
+
+    JSJitCompilerOption opt = JSJITCOMPILER_NOT_AN_OPTION;
+    if (false) {}
+    JIT_COMPILER_OPTIONS(JIT_COMPILER_MATCH);
+#undef JIT_COMPILER_MATCH
+
+    if (opt == JSJITCOMPILER_NOT_AN_OPTION) {
+        ReportUsageErrorASCII(cx, callee, "First argument does not name a valid option (see jsapi.h).");
+        return false;
+    }
+
+    int32_t number = args[1].toInt32();
+    if (number < 0)
+        number = -1;
+
+    // Throw if disabling the JITs and there's JIT code on the stack, to avoid
+    // assertion failures.
+    if ((opt == JSJITCOMPILER_BASELINE_ENABLE || opt == JSJITCOMPILER_ION_ENABLE) &&
+        number == 0)
+    {
+        js::jit::JitActivationIterator iter(cx);
+        if (!iter.done()) {
+            JS_ReportErrorASCII(cx, "Can't turn off JITs with JIT code on the stack.");
+            return false;
+        }
+    }
+
+    // JIT compiler options are process-wide, so we have to stop off-thread
+    // compilations for all runtimes to avoid races.
+    HelperThreadState().waitForAllThreads();
+
+    // Only release JIT code for the current runtime because there's no good
+    // way to discard code for other runtimes.
+    ReleaseAllJITCode(cx->runtime()->defaultFreeOp());
+
+    JS_SetGlobalJitCompilerOption(cx, opt, uint32_t(number));
+
+    args.rval().setUndefined();
+    return true;
+}
+
+static bool
 EnableLastWarning(JSContext* cx, unsigned argc, Value* vp)
 {
     ShellContext* sc = GetShellContext(cx);
     CallArgs args = CallArgsFromVp(argc, vp);
 
     sc->lastWarningEnabled = true;
     sc->lastWarning.setNull();
 
@@ -7575,16 +7652,20 @@ JS_FN_HELP("parseBin", BinParse, 1, 0,
 "  be called. Before returning, fun is called with the return value of the\n"
 "  interrupt handler."),
 
     JS_FN_HELP("setInterruptCallback", SetInterruptCallback, 1, 0,
 "setInterruptCallback(func)",
 "  Sets func as the interrupt callback function.\n"
 "  Calling this function will replace any callback set by |timeout|.\n"),
 
+    JS_FN_HELP("setJitCompilerOption", SetJitCompilerOption, 2, 0,
+"setJitCompilerOption(<option>, <number>)",
+"  Set a compiler option indexed in JSCompileOption enum to a number.\n"),
+
     JS_FN_HELP("enableLastWarning", EnableLastWarning, 0, 0,
 "enableLastWarning()",
 "  Enable storing the last warning."),
 
     JS_FN_HELP("disableLastWarning", DisableLastWarning, 0, 0,
 "disableLastWarning()",
 "  Disable storing the last warning."),
 
