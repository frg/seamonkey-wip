# HG changeset patch
# User Ben Kelly <ben@wanderview.com>
# Date 1513695888 18000
# Node ID 81fe3f741dc336b2ce1df1c5d9718ee5b179a24a
# Parent  1bf366f04bc32a49f72379eea9a49a61779817d5
Bug 1425316 P2 Improve nsGlobalWindowInner::CallOnChildren() to take variable arguments and allow iteration to be aborted. r=asuth

diff --git a/dom/base/nsGlobalWindow.cpp b/dom/base/nsGlobalWindow.cpp
--- a/dom/base/nsGlobalWindow.cpp
+++ b/dom/base/nsGlobalWindow.cpp
@@ -12593,27 +12593,29 @@ nsGlobalWindow::SyncStateFromParentWindo
 
   // Now apply only the number of Suspend() calls to reach the target
   // suspend count after applying the Freeze() calls.
   for (uint32_t i = 0; i < (parentSuspendDepth - parentFreezeDepth); ++i) {
     Suspend();
   }
 }
 
-template<typename Method>
-void
-nsGlobalWindow::CallOnChildren(Method aMethod)
+template<typename Method, typename... Args>
+nsGlobalWindow::CallState
+nsGlobalWindow::CallOnChildren(Method aMethod, Args& ...aArgs)
 {
   MOZ_ASSERT(NS_IsMainThread());
   MOZ_ASSERT(IsInnerWindow());
   MOZ_ASSERT(AsInner()->IsCurrentInnerWindow());
 
+  CallState state = CallState::Continue;
+
   nsCOMPtr<nsIDocShell> docShell = GetDocShell();
   if (!docShell) {
-    return;
+    return state;
   }
 
   int32_t childCount = 0;
   docShell->GetChildCount(&childCount);
 
   for (int32_t i = 0; i < childCount; ++i) {
     nsCOMPtr<nsIDocShellTreeItem> childShell;
     docShell->GetChildAt(i, getter_AddRefs(childShell));
@@ -12629,18 +12631,29 @@ nsGlobalWindow::CallOnChildren(Method aM
 
     // This is a bit hackish. Only freeze/suspend windows which are truly our
     // subwindows.
     nsCOMPtr<Element> frame = pWin->GetFrameElementInternal();
     if (!mDoc || !frame || mDoc != frame->OwnerDoc() || !inner) {
       continue;
     }
 
-    (inner->*aMethod)();
-  }
+    // Call the child method using our helper CallChild() template method.
+    // This allows us to handle both void returning methods and methods
+    // that return CallState explicitly.  For void returning methods we
+    // assume CallState::Continue.
+    typedef decltype((inner->*aMethod)(aArgs...)) returnType;
+    state = CallChild<returnType>(inner, aMethod, aArgs...);
+
+    if (state == CallState::Stop) {
+      return state;
+    }
+  }
+
+  return state;
 }
 
 Maybe<ClientInfo>
 nsGlobalWindow::GetClientInfo() const
 {
   MOZ_ASSERT(NS_IsMainThread());
   Maybe<ClientInfo> clientInfo;
   if (mClientSource) {
diff --git a/dom/base/nsGlobalWindow.h b/dom/base/nsGlobalWindow.h
--- a/dom/base/nsGlobalWindow.h
+++ b/dom/base/nsGlobalWindow.h
@@ -1566,18 +1566,52 @@ private:
                         bool aDoJSFixups,
                         bool aNavigate,
                         nsIArray *argv,
                         nsISupports *aExtraArgument,
                         nsIDocShellLoadInfo* aLoadInfo,
                         bool aForceNoOpener,
                         nsPIDOMWindowOuter **aReturn);
 
-  template<typename Method>
-  void CallOnChildren(Method aMethod);
+  // A type that methods called by CallOnChildren can return.  If Stop
+  // is returned then CallOnChildren will stop calling further children.
+  // If Continue is returned then CallOnChildren will keep calling further
+  // children.
+  enum class CallState
+  {
+    Continue,
+    Stop,
+  };
+
+  // Call the given method on the immediate children of this window.  The
+  // CallState returned by the last child method invocation is returned or
+  // CallState::Continue if the method returns void.
+  template<typename Method, typename... Args>
+  CallState CallOnChildren(Method aMethod, Args& ...aArgs);
+
+  // Helper to convert a void returning child method into an implicit
+  // CallState::Continue value.
+  template<typename Return, typename Method, typename... Args>
+  typename std::enable_if<std::is_void<Return>::value,
+                          nsGlobalWindow::CallState>::type
+  CallChild(nsGlobalWindow* aWindow, Method aMethod, Args& ...aArgs)
+  {
+    (aWindow->*aMethod)(aArgs...);
+    return nsGlobalWindow::CallState::Continue;
+  }
+
+  // Helper that passes through the CallState value from a child method.
+  template<typename Return, typename Method, typename... Args>
+  typename std::enable_if<std::is_same<Return,
+                                       nsGlobalWindow::CallState>::value,
+                          nsGlobalWindow::CallState>::type
+  CallChild(nsGlobalWindow* aWindow, Method aMethod, Args& ...aArgs)
+  {
+    return (aWindow->*aMethod)(aArgs...);
+  }
 
   void FreezeInternal();
   void ThawInternal();
 
 public:
   // Timeout Functions
   // |interval| is in milliseconds.
   int32_t SetTimeoutOrInterval(JSContext* aCx,
