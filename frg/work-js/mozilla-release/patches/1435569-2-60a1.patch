# HG changeset patch
# User Matthew Gaudet <mgaudet@mozilla.com>
# Date 1518619682 18000
# Node ID 287ae6668ed2a94ba5c04b84655a7fbc377cdef0
# Parent  78f3fdf0d56b7e91eeccae6599ae1d976e6e21e3
Bug 1435569: Give the "NotAttached" case a name r=evilpie

diff --git a/js/src/jit/BaselineIC.cpp b/js/src/jit/BaselineIC.cpp
--- a/js/src/jit/BaselineIC.cpp
+++ b/js/src/jit/BaselineIC.cpp
@@ -913,17 +913,17 @@ DoSetElemFallback(JSContext* cx, Baselin
                 else if (gen.shouldUnlinkPreliminaryObjectStubs())
                     StripPreliminaryObjectStubs(cx, stub);
 
                 JitSpew(JitSpew_BaselineIC, "  Attached CacheIR stub");
                 SetUpdateStubData(newStub->toCacheIR_Updated(), gen.typeCheckInfo());
                 return true;
             }
         } else {
-            gen.trackAttached(nullptr);
+            gen.trackAttached(IRGenerator::NotAttached);
         }
         if (!attached && !isTemporarilyUnoptimizable)
             stub->state().trackNotAttached();
     }
 
     return true;
 }
 
@@ -1579,17 +1579,17 @@ DoSetPropFallback(JSContext* cx, Baselin
                     newStub->toCacheIR_Updated()->notePreliminaryObject();
                 else if (gen.shouldUnlinkPreliminaryObjectStubs())
                     StripPreliminaryObjectStubs(cx, stub);
 
                 JitSpew(JitSpew_BaselineIC, "  Attached CacheIR stub");
                 SetUpdateStubData(newStub->toCacheIR_Updated(), gen.typeCheckInfo());
             }
         } else {
-            gen.trackAttached(nullptr);
+            gen.trackAttached(IRGenerator::NotAttached);
         }
         if (!attached && !isTemporarilyUnoptimizable)
             stub->state().trackNotAttached();
     }
 
     if (!attached && !isTemporarilyUnoptimizable)
         stub->noteUnoptimizableAccess();
 
diff --git a/js/src/jit/CacheIR.cpp b/js/src/jit/CacheIR.cpp
--- a/js/src/jit/CacheIR.cpp
+++ b/js/src/jit/CacheIR.cpp
@@ -211,17 +211,17 @@ GetPropIRGenerator::tryAttachStub()
                 return true;
             if (tryAttachXrayCrossCompartmentWrapper(obj, objId, id))
                 return true;
             if (tryAttachFunction(obj, objId, id))
                 return true;
             if (tryAttachProxy(obj, objId, id))
                 return true;
 
-            trackAttached(nullptr);
+            trackAttached(IRGenerator::NotAttached);
             return false;
         }
 
         MOZ_ASSERT(cacheKind_ == CacheKind::GetElem || cacheKind_ == CacheKind::GetElemSuper);
 
         if (tryAttachProxyElement(obj, objId))
             return true;
 
@@ -232,48 +232,48 @@ GetPropIRGenerator::tryAttachStub()
                 return true;
             if (tryAttachDenseElement(obj, objId, index, indexId))
                 return true;
             if (tryAttachDenseElementHole(obj, objId, index, indexId))
                 return true;
             if (tryAttachArgumentsObjectArg(obj, objId, index, indexId))
                 return true;
 
-            trackAttached(nullptr);
+            trackAttached(IRGenerator::NotAttached);
             return false;
         }
 
-        trackAttached(nullptr);
+        trackAttached(IRGenerator::NotAttached);
         return false;
     }
 
     if (nameOrSymbol) {
         if (tryAttachPrimitive(valId, id))
             return true;
         if (tryAttachStringLength(valId, id))
             return true;
         if (tryAttachMagicArgumentsName(valId, id))
             return true;
 
-        trackAttached(nullptr);
+        trackAttached(IRGenerator::NotAttached);
         return false;
     }
 
     if (idVal_.isInt32()) {
         ValOperandId indexId = getElemKeyValueId();
         if (tryAttachStringChar(valId, indexId))
             return true;
         if (tryAttachMagicArgument(valId, indexId))
             return true;
 
-        trackAttached(nullptr);
+        trackAttached(IRGenerator::NotAttached);
         return false;
     }
 
-    trackAttached(nullptr);
+    trackAttached(IRGenerator::NotAttached);
     return false;
 }
 
 bool
 GetPropIRGenerator::tryAttachIdempotentStub()
 {
     // For idempotent ICs, only attach stubs which we can be sure have no side
     // effects and produce a result which the MIR in the calling code is able
@@ -2187,17 +2187,17 @@ GetNameIRGenerator::tryAttachStub()
 
     if (tryAttachGlobalNameValue(envId, id))
         return true;
     if (tryAttachGlobalNameGetter(envId, id))
         return true;
     if (tryAttachEnvironmentName(envId, id))
         return true;
 
-    trackAttached(nullptr);
+    trackAttached(IRGenerator::NotAttached);
     return false;
 }
 
 bool
 CanAttachGlobalName(JSContext* cx, Handle<LexicalEnvironmentObject*> globalLexical, HandleId id,
                     MutableHandleNativeObject holder, MutableHandleShape shape)
 {
     // The property must be found, and it must be found as a normal data property.
@@ -2441,17 +2441,17 @@ BindNameIRGenerator::tryAttachStub()
     ObjOperandId envId(writer.setInputOperandId(0));
     RootedId id(cx_, NameToId(name_));
 
     if (tryAttachGlobalName(envId, id))
         return true;
     if (tryAttachEnvironmentName(envId, id))
         return true;
 
-    trackAttached(nullptr);
+    trackAttached(IRGenerator::NotAttached);
     return false;
 }
 
 bool
 BindNameIRGenerator::tryAttachGlobalName(ObjOperandId objId, HandleId id)
 {
     if (!IsGlobalOp(JSOp(*pc_)) || script_->hasNonSyntacticScope())
         return false;
@@ -2887,17 +2887,17 @@ HasPropIRGenerator::tryAttachStub()
 
     AutoAssertNoPendingException aanpe(cx_);
 
     // NOTE: Argument order is PROPERTY, OBJECT
     ValOperandId keyId(writer.setInputOperandId(0));
     ValOperandId valId(writer.setInputOperandId(1));
 
     if (!val_.isObject()) {
-        trackAttached(nullptr);
+        trackAttached(IRGenerator::NotAttached);
         return false;
     }
     RootedObject obj(cx_, &val_.toObject());
     ObjOperandId objId = writer.guardIsObject(valId);
 
     // Optimize Proxies
     if (tryAttachProxyElement(obj, objId, keyId))
         return true;
@@ -2910,37 +2910,37 @@ HasPropIRGenerator::tryAttachStub()
     }
 
     if (nameOrSymbol) {
         if (tryAttachNamedProp(obj, objId, id, keyId))
             return true;
         if (tryAttachDoesNotExist(obj, objId, id, keyId))
             return true;
 
-        trackAttached(nullptr);
+        trackAttached(IRGenerator::NotAttached);
         return false;
     }
 
     uint32_t index;
     Int32OperandId indexId;
     if (maybeGuardInt32Index(idVal_, keyId, &index, &indexId)) {
         if (tryAttachDense(obj, objId, index, indexId))
             return true;
         if (tryAttachDenseHole(obj, objId, index, indexId))
             return true;
         if (tryAttachTypedArray(obj, objId, index, indexId))
             return true;
         if (tryAttachSparse(obj, objId, index, indexId))
             return true;
 
-        trackAttached(nullptr);
+        trackAttached(IRGenerator::NotAttached);
         return false;
     }
 
-    trackAttached(nullptr);
+    trackAttached(IRGenerator::NotAttached);
     return false;
 }
 
 void
 HasPropIRGenerator::trackAttached(const char* name)
 {
 #ifdef JS_CACHEIR_SPEW
     if (const CacheIRSpewer::Guard& sp = CacheIRSpewer::Guard(*this, name)) {
@@ -4122,59 +4122,59 @@ InstanceOfIRGenerator::InstanceOfIRGener
 bool
 InstanceOfIRGenerator::tryAttachStub()
 {
     MOZ_ASSERT(cacheKind_ == CacheKind::InstanceOf);
     AutoAssertNoPendingException aanpe(cx_);
 
     // Ensure RHS is a function -- could be a Proxy, which the IC isn't prepared to handle.
     if (!rhsObj_->is<JSFunction>()) {
-        trackAttached(nullptr);
+        trackAttached(IRGenerator::NotAttached);
         return false;
     }
 
     HandleFunction fun = rhsObj_.as<JSFunction>();
 
     if (fun->isBoundFunction()) {
-        trackAttached(nullptr);
+        trackAttached(IRGenerator::NotAttached);
         return false;
     }
 
     // If the user has supplied their own @@hasInstance method we shouldn't
     // clobber it.
     if (!js::FunctionHasDefaultHasInstance(fun, cx_->wellKnownSymbols())) {
-        trackAttached(nullptr);
+        trackAttached(IRGenerator::NotAttached);
         return false;
     }
 
     // Refuse to optimize any function whose [[Prototype]] isn't
     // Function.prototype.
     if (!fun->hasStaticPrototype() || fun->hasUncacheableProto()) {
-        trackAttached(nullptr);
+        trackAttached(IRGenerator::NotAttached);
         return false;
     }
 
     Value funProto = cx_->global()->getPrototype(JSProto_Function);
     if (!funProto.isObject() || fun->staticPrototype() != &funProto.toObject()) {
-        trackAttached(nullptr);
+        trackAttached(IRGenerator::NotAttached);
         return false;
     }
 
     // Ensure that the function's prototype slot is the same.
     Shape* shape = fun->lookupPure(cx_->names().prototype);
     if (!shape || !shape->isDataProperty()) {
-        trackAttached(nullptr);
+        trackAttached(IRGenerator::NotAttached);
         return false;
     }
 
     uint32_t slot = shape->slot();
 
     MOZ_ASSERT(fun->numFixedSlots() == 0, "Stub code relies on this");
     if (!fun->getSlot(slot).isObject()) {
-        trackAttached(nullptr);
+        trackAttached(IRGenerator::NotAttached);
         return false;
     }
 
     JSObject* prototypeObject = &fun->getSlot(slot).toObject();
 
     // Abstract Objects
     ValOperandId lhs(writer.setInputOperandId(0));
     ValOperandId rhs(writer.setInputOperandId(1));
@@ -4712,21 +4712,21 @@ CompareIRGenerator::tryAttachStub()
     if (IsEqualityOp(op_)) {
         if (tryAttachString(lhsId, rhsId))
             return true;
         if (tryAttachObject(lhsId, rhsId))
             return true;
         if (tryAttachSymbol(lhsId, rhsId))
             return true;
 
-        trackAttached(nullptr);
+        trackAttached(IRGenerator::NotAttached);
         return false;
     }
 
-    trackAttached(nullptr);
+    trackAttached(IRGenerator::NotAttached);
     return false;
 }
 
 void
 CompareIRGenerator::trackAttached(const char* name)
 {
 #ifdef JS_CACHEIR_SPEW
     if (const CacheIRSpewer::Guard& sp = CacheIRSpewer::Guard(*this, name)) {
@@ -4765,17 +4765,17 @@ ToBoolIRGenerator::tryAttachStub()
         return true;
     if (tryAttachNullOrUndefined())
         return true;
     if (tryAttachObject())
         return true;
     if (tryAttachSymbol())
         return true;
 
-    trackAttached(nullptr);
+    trackAttached(IRGenerator::NotAttached);
     return false;
 }
 
 bool
 ToBoolIRGenerator::tryAttachInt32()
 {
     if (!val_.isInt32())
         return false;
diff --git a/js/src/jit/CacheIR.h b/js/src/jit/CacheIR.h
--- a/js/src/jit/CacheIR.h
+++ b/js/src/jit/CacheIR.h
@@ -1233,16 +1233,18 @@ class MOZ_RAII IRGenerator
     friend class CacheIRSpewer;
 
   public:
     explicit IRGenerator(JSContext* cx, HandleScript script, jsbytecode* pc, CacheKind cacheKind,
                          ICState::Mode mode);
 
     const CacheIRWriter& writerRef() const { return writer; }
     CacheKind cacheKind() const { return cacheKind_; }
+
+    static constexpr char* NotAttached = nullptr;
 };
 
 // Flags used to describe what values a GetProperty cache may produce.
 enum class GetPropertyResultFlags {
     None            = 0,
 
     // Values produced by this cache will go through a type barrier,
     // so the cache may produce any type of value that is compatible with its
