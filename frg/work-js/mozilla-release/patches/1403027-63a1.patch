# HG changeset patch
# User Thomas Wisniewski <wisniewskit@gmail.com>
# Date 1516845544 18000
# Node ID f839685ceb8eebc50fa69c9273141a4bdc8ce1e2
# Parent  0fb3fef69fe19c1a204309ef07050bef0a9d44e0
Bug 1403027 - Do not throw from PerformanceObserver.observe when none of the entryTypes are known (log a JS console warning instead); r=bz

MozReview-Commit-ID: Lx2cjWDX8sh

diff --git a/dom/locales/en-US/chrome/dom/dom.properties b/dom/locales/en-US/chrome/dom/dom.properties
--- a/dom/locales/en-US/chrome/dom/dom.properties
+++ b/dom/locales/en-US/chrome/dom/dom.properties
@@ -355,8 +355,9 @@ ScriptSourceMalformed=<script> source URI is malformed: “%S”.
 ModuleSourceMalformed=Module source URI is malformed: “%S”.
 # LOCALIZATION NOTE: Do not translate "<script>".
 ScriptSourceNotAllowed=<script> source URI is not allowed in this document: “%S”.
 ModuleSourceNotAllowed=Module source URI is not allowed in this document: “%S”.
 # LOCALIZATION NOTE: %1$S is the invalid property value and %2$S is the property name.
 InvalidKeyframePropertyValue=Keyframe property value “%1$S” is invalid according to the syntax for “%2$S”.
 # LOCALIZATION NOTE: Do not translate "ReadableStream".
 ReadableStreamReadingFailed=Failed to read data from the ReadableStream: “%S”.
+UnsupportedEntryTypesIgnored=Ignoring unsupported entryTypes: %S.
diff --git a/dom/performance/PerformanceObserver.cpp b/dom/performance/PerformanceObserver.cpp
--- a/dom/performance/PerformanceObserver.cpp
+++ b/dom/performance/PerformanceObserver.cpp
@@ -5,16 +5,17 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "PerformanceObserver.h"
 
 #include "mozilla/dom/Performance.h"
 #include "mozilla/dom/PerformanceBinding.h"
 #include "mozilla/dom/PerformanceEntryBinding.h"
 #include "mozilla/dom/PerformanceObserverBinding.h"
+#include "nsIScriptError.h"
 #include "nsPIDOMWindow.h"
 #include "nsQueryObject.h"
 #include "nsString.h"
 #include "PerformanceEntry.h"
 #include "PerformanceObserverEntryList.h"
 #include "WorkerPrivate.h"
 #include "WorkerScope.h"
 
@@ -139,36 +140,63 @@ PerformanceObserver::QueueEntry(Performa
 static const char16_t *const sValidTypeNames[4] = {
   u"mark",
   u"measure",
   u"resource",
   u"server"
 };
 
 void
-PerformanceObserver::Observe(const PerformanceObserverInit& aOptions,
-                             ErrorResult& aRv)
+PerformanceObserver::Observe(const PerformanceObserverInit& aOptions)
 {
   if (aOptions.mEntryTypes.IsEmpty()) {
-    aRv.Throw(NS_ERROR_DOM_TYPE_ERR);
     return;
   }
 
   nsTArray<nsString> validEntryTypes;
 
   for (const char16_t* name : sValidTypeNames) {
     nsDependentString validTypeName(name);
     if (aOptions.mEntryTypes.Contains<nsString>(validTypeName) &&
         !validEntryTypes.Contains<nsString>(validTypeName)) {
       validEntryTypes.AppendElement(validTypeName);
     }
   }
 
+  nsAutoString invalidTypesJoined;
+  bool addComma = false;
+  for (const auto& type : aOptions.mEntryTypes) {
+    if (!validEntryTypes.Contains<nsString>(type)) {
+      if (addComma) {
+        invalidTypesJoined.AppendLiteral(", ");
+      }
+      addComma = true;
+      invalidTypesJoined.Append(type);
+    }
+  }
+
+  if (!invalidTypesJoined.IsEmpty()) {
+    if (!NS_IsMainThread()) {
+      nsTArray<nsString> params;
+      params.AppendElement(invalidTypesJoined);
+      WorkerPrivate::ReportErrorToConsole("UnsupportedEntryTypesIgnored",
+                                          params);
+    } else {
+      nsCOMPtr<nsPIDOMWindowInner> ownerWindow =
+        do_QueryInterface(mOwner);
+      nsIDocument* document = ownerWindow->GetExtantDoc();
+      const char16_t* params[] = { invalidTypesJoined.get() };
+      nsContentUtils::ReportToConsole(nsIScriptError::warningFlag,
+                                      NS_LITERAL_CSTRING("DOM"), document,
+                                      nsContentUtils::eDOM_PROPERTIES,
+                                      "UnsupportedEntryTypesIgnored", params, 1);
+    }
+  }
+
   if (validEntryTypes.IsEmpty()) {
-    aRv.Throw(NS_ERROR_DOM_TYPE_ERR);
     return;
   }
 
   mEntryTypes.SwapElements(validEntryTypes);
 
   mPerformance->AddObserver(this);
 
   if (aOptions.mBuffered) {
diff --git a/dom/performance/PerformanceObserver.h b/dom/performance/PerformanceObserver.h
--- a/dom/performance/PerformanceObserver.h
+++ b/dom/performance/PerformanceObserver.h
@@ -49,18 +49,17 @@ public:
   PerformanceObserver(workers::WorkerPrivate* aWorkerPrivate,
                       PerformanceObserverCallback& aCb);
 
   virtual JSObject* WrapObject(JSContext* aCx,
                                JS::Handle<JSObject*> aGivenProto) override;
 
   nsISupports* GetParentObject() const { return mOwner; }
 
-  void Observe(const PerformanceObserverInit& aOptions,
-               mozilla::ErrorResult& aRv);
+  void Observe(const PerformanceObserverInit& aOptions);
 
   void Disconnect();
 
   void Notify();
   void QueueEntry(PerformanceEntry* aEntry);
 
 private:
   ~PerformanceObserver();
diff --git a/dom/performance/tests/test_performance_observer.html b/dom/performance/tests/test_performance_observer.html
--- a/dom/performance/tests/test_performance_observer.html
+++ b/dom/performance/tests/test_performance_observer.html
@@ -2,41 +2,122 @@
   Any copyright is dedicated to the Public Domain.
   http://creativecommons.org/publicdomain/zero/1.0/
 -->
 <!DOCTYPE html>
 <html>
 <head>
 <meta charset=utf-8>
 <title>Test for performance observer</title>
-<script src="/resources/testharness.js"></script>
-<script src="/resources/testharnessreport.js"></script>
+<script src="/tests/SimpleTest/SimpleTest.js"></script>
+<link rel="stylesheet" type="text/css" href="/tests/SimpleTest/test.css" />
 </head>
 <body>
 <div id="log"></div>
+<script>
+SimpleTest.requestFlakyTimeout("For testing when observer callbacks should not be called.");
+SimpleTest.waitForExplicitFinish();
+
+let _tests = [];
+
+let test = promise_test = fn => {
+  let cleanups = [];
+  _tests.push(async () => {
+    try {
+      await fn({
+        add_cleanup: f => { cleanups.push(f); },
+        step_timeout: function(f, timeout) {
+          var test_this = this;
+          var args = Array.prototype.slice.call(arguments, 2);
+          return setTimeout(() => {
+            return f.apply(test_this, args);
+          }, timeout);
+        }
+      });
+    } catch(e) {
+      ok(false, `got unexpected exception ${e}`);
+    }
+    try {
+      for (const fn of cleanups) {
+        fn();
+      }
+      runNextTest();
+    } catch (e) {
+      ok(false, `got unexpected exception during cleanup ${e}`);
+    }
+  });
+}
+
+function runNextTest() {
+  if (_tests.length == 0) {
+    SimpleTest.finish()
+    return;
+  }
+  _tests.shift()();
+}
+
+function assert_equals(actual, expected, description) {
+  ok(typeof actual == typeof expected,
+     `${description} expected (${typeof expected}) ${expected} but got (${typeof actual}) ${actual}`);
+  ok(Object.is(actual, expected),
+     `${description} expected ${expected} but got ${actual}`);
+}
+
+function assert_array_equals(actual, expected, description) {
+  ok(actual.length === expected.length,
+     `${description} lengths differ, expected ${expected.length} but got ${actual.length}`);
+  for (var i = 0; i < actual.length; i++) {
+    ok(actual.hasOwnProperty(i) === expected.hasOwnProperty(i),
+       `${description} property expected to be ${expected[i]} but got ${actual[i]}`);
+  }
+}
+
+function assert_throws(expected_exc, func, desc) {
+  try {
+    func.call(this);
+  } catch(e) {
+    var actual = e.name || e.type;
+    var expected = expected_exc.name || expected_exc.type;
+    ok(actual == expected,
+       `Expected '${expected}', got '${actual}'.`);
+    return;
+  }
+  ok(false, "Expected exception, but none was thrown");
+}
+
+function assert_unreached(description) {
+  ok(false, `${description} reached unreachable code`);
+}
+</script>
 <script src="test_performance_observer.js"></script>
 <script>
 function makeXHR(aUrl) {
   var xmlhttp = new XMLHttpRequest();
   xmlhttp.open("get", aUrl, true);
   xmlhttp.send();
 }
 
+let waitForConsole = new Promise(resolve => {
+  SimpleTest.monitorConsole(resolve, [{
+    message: /JavaScript Warning: "Ignoring unsupported entryTypes: invalid."/,
+  }]);
+});
+
 promise_test(t => {
   var promise = new Promise(resolve => {
     performance.clearResourceTimings();
 
     var observer = new PerformanceObserver(list => resolve(list));
     observer.observe({entryTypes: ['resource']});
     t.add_cleanup(() => observer.disconnect());
   });
 
   makeXHR("test-data.json");
 
-  return promise.then(list => {
+  return promise.then(async list => {
     assert_equals(list.getEntries().length, 1);
     assert_array_equals(list.getEntries(),
                         performance.getEntriesByType("resource"),
                         "Observed 'resource' entries should equal to entries obtained by getEntriesByType.");
 
     // getEntries filtering tests
     assert_array_equals(list.getEntries({name: "http://mochi.test:8888/tests/dom/base/test/test-data.json"}),
                         performance.getEntriesByName("http://mochi.test:8888/tests/dom/base/test/test-data.json"),
@@ -45,13 +126,17 @@ promise_test(t => {
                         performance.getEntriesByType("resource"),
                         "getEntries with entryType filter should return correct results.");
     assert_array_equals(list.getEntries({initiatorType: "xmlhttprequest"}),
                         performance.getEntriesByType("resource"),
                         "getEntries with initiatorType filter should return correct results.");
     assert_array_equals(list.getEntries({initiatorType: "link"}),
                         [],
                         "getEntries with non-existent initiatorType filter should return an empty array.");
+
+    SimpleTest.endMonitorConsole();
+    await waitForConsole;
   });
 }, "resource-timing test");
 
+runNextTest();
 </script>
 </body>
diff --git a/dom/performance/tests/test_performance_observer.js b/dom/performance/tests/test_performance_observer.js
--- a/dom/performance/tests/test_performance_observer.js
+++ b/dom/performance/tests/test_performance_observer.js
@@ -15,27 +15,25 @@ test(t => {
   assert_throws({name: "TypeError"}, function() {
     observer.observe();
   }, "observe() should throw TypeError exception if no option specified.");
 
   assert_throws({name: "TypeError"}, function() {
     observer.observe({ unsupportedAttribute: "unsupported" });
   }, "obsrve() should throw TypeError exception if the option has no 'entryTypes' attribute.");
 
-  assert_throws({name: "TypeError"}, function() {
-    observer.observe({ entryTypes: [] });
-  }, "obsrve() should throw TypeError exception if 'entryTypes' attribute is an empty sequence.");
+  assert_equals(undefined, observer.observe({ entryTypes: [] }),
+     "observe() should silently ignore empty 'entryTypes' sequence.");
 
   assert_throws({name: "TypeError"}, function() {
     observer.observe({ entryTypes: null });
   }, "obsrve() should throw TypeError exception if 'entryTypes' attribute is null.");
 
-  assert_throws({name: "TypeError"}, function() {
-    observer.observe({ entryTypes: ["invalid"]});
-  }, "obsrve() should throw TypeError exception if 'entryTypes' attribute value is invalid.");
+  assert_equals(undefined, observer.observe({ entryTypes: ["invalid"] }),
+     "observe() should silently ignore invalid 'entryTypes' values.");
 }, "Test that PerformanceObserver.observe throws exception");
 
 function promiseObserve(test, options) {
   return new Promise(resolve => {
     performance.clearMarks();
     performance.clearMeasures();
 
     var observer = new PerformanceObserver(list => resolve(list));
diff --git a/dom/performance/tests/test_worker_observer.html b/dom/performance/tests/test_worker_observer.html
--- a/dom/performance/tests/test_worker_observer.html
+++ b/dom/performance/tests/test_worker_observer.html
@@ -8,11 +8,34 @@
 <meta charset=utf-8>
 <title>Test for performance observer in worker</title>
 <script src="/resources/testharness.js"></script>
 <script src="/resources/testharnessreport.js"></script>
 </head>
 <body>
 <div id="log"></div>
 <script>
-fetch_tests_from_worker(new Worker("worker_performance_observer.js"));
+const worker = new Worker("worker_performance_observer.js");
+
+promise_test(t => {
+  let found = false;
+  return new Promise(resolve => {
+    SpecialPowers.registerConsoleListener(msg => {
+      if (msg.errorMessage === "Ignoring unsupported entryTypes: invalid.") {
+        found = true;
+        resolve();
+      }
+    });
+    worker.addEventListener("error", resolve);
+    worker.addEventListener("message", function(event) {
+      if (event.data.type === "complete") {
+        resolve();
+      }
+    });
+  }).then(() => {
+    SpecialPowers.postConsoleSentinel();
+    assert_true(found, "got the expected console warning");
+  });
+}, "Console warnings about invalid types should be logged during the tests");
+
+fetch_tests_from_worker(worker);
 </script>
 </body>
diff --git a/dom/webidl/PerformanceObserver.webidl b/dom/webidl/PerformanceObserver.webidl
--- a/dom/webidl/PerformanceObserver.webidl
+++ b/dom/webidl/PerformanceObserver.webidl
@@ -13,12 +13,11 @@ dictionary PerformanceObserverInit {
 };
 
 callback PerformanceObserverCallback = void (PerformanceObserverEntryList entries, PerformanceObserver observer);
 
 [Func="Performance::IsObserverEnabled",
  Constructor(PerformanceObserverCallback callback),
  Exposed=(Window,Worker)]
 interface PerformanceObserver {
-  [Throws]
   void observe(PerformanceObserverInit options);
   void disconnect();
 };
diff --git a/dom/workers/WorkerPrivate.cpp b/dom/workers/WorkerPrivate.cpp
--- a/dom/workers/WorkerPrivate.cpp
+++ b/dom/workers/WorkerPrivate.cpp
@@ -953,65 +953,74 @@ private:
   {
     return aWorkerPrivate->ThawInternal();
   }
 };
 
 class ReportErrorToConsoleRunnable final : public WorkerRunnable
 {
   const char* mMessage;
+  const nsTArray<nsString> mParams;
 
 public:
   // aWorkerPrivate is the worker thread we're on (or the main thread, if null)
   static void
-  Report(WorkerPrivate* aWorkerPrivate, const char* aMessage)
+  Report(WorkerPrivate* aWorkerPrivate, const char* aMessage,
+         const nsTArray<nsString>& aParams)
   {
     if (aWorkerPrivate) {
       aWorkerPrivate->AssertIsOnWorkerThread();
     } else {
       AssertIsOnMainThread();
     }
 
     // Now fire a runnable to do the same on the parent's thread if we can.
     if (aWorkerPrivate) {
       RefPtr<ReportErrorToConsoleRunnable> runnable =
-        new ReportErrorToConsoleRunnable(aWorkerPrivate, aMessage);
+        new ReportErrorToConsoleRunnable(aWorkerPrivate, aMessage, aParams);
       runnable->Dispatch();
       return;
     }
 
+    uint16_t paramCount = aParams.Length();
+    const char16_t** params = new const char16_t*[paramCount];
+    for (uint16_t i=0; i<paramCount; ++i) {
+      params[i] = aParams[i].get();
+    }
+
     // Log a warning to the console.
     nsContentUtils::ReportToConsole(nsIScriptError::warningFlag,
-                                    NS_LITERAL_CSTRING("DOM"),
-                                    nullptr,
-                                    nsContentUtils::eDOM_PROPERTIES,
-                                    aMessage);
+                                    NS_LITERAL_CSTRING("DOM"), nullptr,
+                                    nsContentUtils::eDOM_PROPERTIES, aMessage,
+                                    paramCount ? params : nullptr, paramCount);
+    delete[] params;
   }
 
 private:
-  ReportErrorToConsoleRunnable(WorkerPrivate* aWorkerPrivate, const char* aMessage)
+  ReportErrorToConsoleRunnable(WorkerPrivate* aWorkerPrivate, const char* aMessage,
+                               const nsTArray<nsString>& aParams)
   : WorkerRunnable(aWorkerPrivate, ParentThreadUnchangedBusyCount),
-    mMessage(aMessage)
+    mMessage(aMessage), mParams(aParams)
   { }
 
   virtual void
   PostDispatch(WorkerPrivate* aWorkerPrivate, bool aDispatchResult) override
   {
     aWorkerPrivate->AssertIsOnWorkerThread();
 
     // Dispatch may fail if the worker was canceled, no need to report that as
     // an error, so don't call base class PostDispatch.
   }
 
   virtual bool
   WorkerRun(JSContext* aCx, WorkerPrivate* aWorkerPrivate) override
   {
     WorkerPrivate* parent = aWorkerPrivate->GetParent();
     MOZ_ASSERT_IF(!parent, NS_IsMainThread());
-    Report(parent, mMessage);
+    Report(parent, mMessage, mParams);
     return true;
   }
 };
 
 class ReportErrorRunnable final : public WorkerRunnable
 {
   WorkerErrorReport mReport;
 
@@ -6414,22 +6423,31 @@ WorkerPrivate::ReportError(JSContext* aC
 
   mErrorHandlerRecursionCount--;
 }
 
 // static
 void
 WorkerPrivate::ReportErrorToConsole(const char* aMessage)
 {
+  nsTArray<nsString> emptyParams;
+  WorkerPrivate::ReportErrorToConsole(aMessage, emptyParams);
+}
+
+// static
+void
+WorkerPrivate::ReportErrorToConsole(const char* aMessage,
+                                    const nsTArray<nsString>& aParams)
+{
   WorkerPrivate* wp = nullptr;
   if (!NS_IsMainThread()) {
     wp = GetCurrentThreadWorkerPrivate();
   }
 
-  ReportErrorToConsoleRunnable::Report(wp, aMessage);
+  ReportErrorToConsoleRunnable::Report(wp, aMessage, aParams);
 }
 
 int32_t
 WorkerPrivate::SetTimeout(JSContext* aCx,
                           nsIScriptTimeoutHandler* aHandler,
                           int32_t aTimeout, bool aIsInterval,
                           ErrorResult& aRv)
 {
diff --git a/dom/workers/WorkerPrivate.h b/dom/workers/WorkerPrivate.h
--- a/dom/workers/WorkerPrivate.h
+++ b/dom/workers/WorkerPrivate.h
@@ -1237,16 +1237,19 @@ public:
 
   void
   ReportError(JSContext* aCx, JS::ConstUTF8CharsZ aToStringResult,
               JSErrorReport* aReport);
 
   static void
   ReportErrorToConsole(const char* aMessage);
 
+  static void
+  ReportErrorToConsole(const char* aMessage, const nsTArray<nsString>& aParams);
+
   int32_t
   SetTimeout(JSContext* aCx, nsIScriptTimeoutHandler* aHandler,
              int32_t aTimeout, bool aIsInterval,
              ErrorResult& aRv);
 
   void
   ClearTimeout(int32_t aId);
 
