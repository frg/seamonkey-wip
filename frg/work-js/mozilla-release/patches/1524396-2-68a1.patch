# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1553252718 0
# Node ID fc6f661f4113fa38c6e44663c404edc7e3ceadec
# Parent  427390948a1ed570b87f32d6e64cdf5bc924b887
Bug 1524396 - Unify how target/host linker/flags are passed to rust. r=chmanchester

The current setup uses different ways for different platforms, with
different workarounds, even using extra configuration items for Windows.

Now that there can't be a difference between the host per the build
system and the host per rust, we can get rid of those configuration
items, and use a more common infrastructure.

We cannot, however, avoid using wrapper scripts, because per-target rust
link-arg flags don't work up great.

The downside is that multiplies the number of wrappers, as we now have
to have a different one for host and target, and then we have .bat files
and shell scripts for, respectively, Windows hosts, and other hosts.

Depends on D24321

Differential Revision: https://phabricator.services.mozilla.com/D24322

diff --git a/.cargo/config.in b/.cargo/config.in
--- a/.cargo/config.in
+++ b/.cargo/config.in
@@ -9,10 +9,8 @@ replace-with = "vendored-sources"
 
 [source."https://github.com/hsivonen/packed_simd"]
 git = "https://github.com/hsivonen/packed_simd"
 replace-with = "vendored-sources"
 rev = "412f9a0aa556611de021bde89dee8fefe6e0fbbd"
 
 [source.vendored-sources]
 directory = '@top_srcdir@/third_party/rust'
-
-@WIN64_CARGO_LINKER_CONFIG@
diff --git a/build/cargo-host-linker b/build/cargo-host-linker
new file mode 100644
--- /dev/null
+++ b/build/cargo-host-linker
@@ -0,0 +1,3 @@
+#!/bin/sh
+# See comment in cargo-linker.
+eval ${MOZ_CARGO_WRAP_HOST_LD} ${MOZ_CARGO_WRAP_HOST_LDFLAGS} '"$@"'
diff --git a/build/cargo-host-linker.bat b/build/cargo-host-linker.bat
new file mode 100644
--- /dev/null
+++ b/build/cargo-host-linker.bat
@@ -0,0 +1,3 @@
+@echo off
+REM See comment in cargo-linker (without extension)
+%MOZ_CARGO_WRAP_HOST_LD% %MOZ_CARGO_WRAP_HOST_LDFLAGS% %*
diff --git a/build/cargo-linker b/build/cargo-linker
--- a/build/cargo-linker
+++ b/build/cargo-linker
@@ -9,14 +9,14 @@
 # Our solution to these problems is to use this wrapper script.  We pass
 # in the LD and the LDFLAGS to use via environment variables.  Note that
 # we do *not* quote either MOZ_CARGO_WRAP variable:
 #
 # * MOZ_CARGO_WRAP_LD is equivalent to CC on Unix-y platforms, and CC
 #   frequently has additional arguments in addition to the compiler
 #   itself.
 # * MOZ_CARGO_WRAP_LDFLAGS contains space-separated arguments to pass,
-#   and not quoting it ensures that either of those arguments is passed
+#   and not quoting it ensures that each of those arguments is passed
 #   as a separate argument to the actual LD.
 #
 # $@ is doubly quoted for the eval. See bug 1418598.
 
 eval ${MOZ_CARGO_WRAP_LD} ${MOZ_CARGO_WRAP_LDFLAGS} '"$@"'
diff --git a/build/cargo-linker.bat b/build/cargo-linker.bat
new file mode 100644
--- /dev/null
+++ b/build/cargo-linker.bat
@@ -0,0 +1,3 @@
+@echo off
+REM See comment in cargo-linker (without extension)
+%MOZ_CARGO_WRAP_LD% %MOZ_CARGO_WRAP_LDFLAGS% %*
diff --git a/build/moz.build b/build/moz.build
--- a/build/moz.build
+++ b/build/moz.build
@@ -7,18 +7,16 @@
 with Files('**'):
     BUG_COMPONENT = ('Firefox Build System', 'General')
 
 # This cannot be named "build" because of bug 922191.
 SPHINX_TREES['buildsystem'] = 'docs'
 
 if CONFIG['OS_ARCH'] == 'WINNT':
     DIRS += ['win32']
-    if CONFIG['WIN64_CARGO_LINKER']:
-        CONFIGURE_SUBST_FILES += ['win64/cargo-linker.bat']
 else:
     DIRS += ['unix']
 
 CRAMTEST_MANIFESTS += [
     'tests/cram/cram.ini',
 ]
 
 DEFINES['ACCEPTED_MAR_CHANNEL_IDS'] = CONFIG['ACCEPTED_MAR_CHANNEL_IDS']
diff --git a/build/moz.configure/rust.configure b/build/moz.configure/rust.configure
--- a/build/moz.configure/rust.configure
+++ b/build/moz.configure/rust.configure
@@ -352,39 +352,8 @@ def rust_tests(enable_rust_tests, rustdo
 
 set_config('MOZ_RUST_TESTS', rust_tests)
 
 js_option(env='WIN64_LINK', nargs=1, help='Path to link.exe that targets win64')
 js_option(env='WIN64_LIB', nargs=1, help='Paths to libraries for the win64 linker')
 
 set_config('WIN64_LINK', depends('WIN64_LINK')(lambda x: x))
 set_config('WIN64_LIB', depends('WIN64_LIB')(lambda x: x))
-
-
-@depends(target, rustc_info, c_compiler, 'WIN64_LINK', 'WIN64_LIB')
-def win64_cargo_linker(target, rustc_info, compiler_info, link, lib):
-    # When we're building a 32-bit Windows build with a 64-bit rustc, we
-    # need to configure the linker it will use for host binaries (build scripts)
-    # specially because the compiler configuration we use for the build is for
-    # MSVC targeting 32-bit binaries.
-    if target.kernel == 'WINNT' and \
-       target.cpu in ('x86', 'aarch64') and \
-       compiler_info.type in ('msvc', 'clang-cl') and \
-       rustc_info.host == 'x86_64-pc-windows-msvc' and link and lib:
-        return True
-
-
-set_config('WIN64_CARGO_LINKER', win64_cargo_linker)
-
-
-@depends(win64_cargo_linker, check_build_environment)
-@imports(_from='textwrap', _import='dedent')
-def win64_cargo_linker_config(linker, env):
-    if linker:
-        return dedent('''\
-        [target.x86_64-pc-windows-msvc]
-        linker = "{objdir}/build/win64/cargo-linker.bat"
-        '''.format(objdir=env.topobjdir))
-    # We want an empty string here so we don't leave the @ variable in the config file.
-    return ''
-
-
-set_config('WIN64_CARGO_LINKER_CONFIG', win64_cargo_linker_config)
diff --git a/build/win64/cargo-linker.bat.in b/build/win64/cargo-linker.bat.in
deleted file mode 100644
--- a/build/win64/cargo-linker.bat.in
+++ /dev/null
@@ -1,3 +0,0 @@
-set LIB=@WIN64_LIB@
-
-@WIN64_LINK@ %*
diff --git a/config/makefiles/rust.mk b/config/makefiles/rust.mk
--- a/config/makefiles/rust.mk
+++ b/config/makefiles/rust.mk
@@ -172,52 +172,72 @@ endef
 define CARGO_BUILD
 $(call RUN_CARGO,rustc)
 endef
 
 define CARGO_CHECK
 $(call RUN_CARGO,check)
 endef
 
+cargo_host_linker_env_var := CARGO_TARGET_$(call cargo_env,$(RUST_HOST_TARGET))_LINKER
 cargo_linker_env_var := CARGO_TARGET_$(call cargo_env,$(RUST_TARGET))_LINKER
 
-# Don't define a custom linker on Windows, as it's difficult to have a
-# non-binary file that will get executed correctly by Cargo.  We don't
-# have to worry about a cross-compiling (besides x86-64 -> x86, which
-# already works with the current setup) setup on Windows, and we don't
-# have to pass in any special linker options on Windows.
-ifneq (WINNT,$(OS_ARCH))
-
 # Defining all of this for ASan/TSan builds results in crashes while running
 # some crates's build scripts (!), so disable it for now.
 ifndef MOZ_ASAN
 ifndef MOZ_TSAN
 ifndef MOZ_UBSAN
 # Cargo needs the same linker flags as the C/C++ compiler,
 # but not the final libraries. Filter those out because they
 # cause problems on macOS 10.7; see bug 1365993 for details.
 export MOZ_CARGO_WRAP_LDFLAGS
 export MOZ_CARGO_WRAP_LD
+export MOZ_CARGO_WRAP_HOST_LDFLAGS
+export MOZ_CARGO_WRAP_HOST_LD
 # Exporting from make always exports a value. Setting a value per-recipe
 # would export an empty value for the host recipes. When not doing a
 # cross-compile, the --target for those is the same, and cargo will use
-# $(cargo_linker_env_var) for its linker, so we always pass the
-# cargo-linker wrapper, and fill MOZ_CARGO_WRAP_LD* more or less
+# CARGO_TARGET_*_LINKER for its linker, so we always pass the
+# cargo-linker wrapper, and fill MOZ_CARGO_WRAP_{HOST_,}LD* more or less
 # appropriately for all recipes.
+ifeq (WINNT,$(HOST_OS_ARCH))
+# Use .bat wrapping on Windows hosts, and shell wrapping on other hosts.
+# Like for CC/C*FLAGS, we want the target values to trump the host values when
+# both variables are the same.
+export $(cargo_host_linker_env_var):=$(topsrcdir)/build/cargo-host-linker.bat
+export $(cargo_linker_env_var):=$(topsrcdir)/build/cargo-linker.bat
+WRAP_HOST_LINKER_LIBPATHS:=$(HOST_LINKER_LIBPATHS_BAT)
+else
+export $(cargo_host_linker_env_var):=$(topsrcdir)/build/cargo-host-linker
 export $(cargo_linker_env_var):=$(topsrcdir)/build/cargo-linker
+WRAP_HOST_LINKER_LIBPATHS:=$(HOST_LINKER_LIBPATHS)
+endif
 $(TARGET_RECIPES): MOZ_CARGO_WRAP_LDFLAGS:=$(filter-out -fsanitize=cfi% -framework Cocoa -lobjc AudioToolbox ExceptionHandling -fprofile-%,$(LDFLAGS))
+
+$(HOST_RECIPES): MOZ_CARGO_WRAP_LDFLAGS:=$(HOST_LDFLAGS) $(WRAP_HOST_LINKER_LIBPATHS)
+$(TARGET_RECIPES) $(HOST_RECIPES): MOZ_CARGO_WRAP_HOST_LDFLAGS:=$(HOST_LDFLAGS) $(WRAP_HOST_LINKER_LIBPATHS)
+
+ifeq (,$(filter clang-cl,$(CC_TYPE)))
 $(TARGET_RECIPES): MOZ_CARGO_WRAP_LD:=$(CC)
-$(HOST_RECIPES): MOZ_CARGO_WRAP_LDFLAGS:=$(HOST_LDFLAGS)
+else
+$(TARGET_RECIPES): MOZ_CARGO_WRAP_LD:=$(LINKER)
+endif
+
+ifeq (,$(filter clang-cl,$(HOST_CC_TYPE)))
 $(HOST_RECIPES): MOZ_CARGO_WRAP_LD:=$(HOST_CC)
+$(TARGET_RECIPES) $(HOST_RECIPES): MOZ_CARGO_WRAP_HOST_LD:=$(HOST_CC)
+else
+$(HOST_RECIPES): MOZ_CARGO_WRAP_LD:=$(HOST_LINKER)
+$(TARGET_RECIPES) $(HOST_RECIPES): MOZ_CARGO_WRAP_HOST_LD:=$(HOST_LINKER)
+endif
+
 endif # MOZ_UBSAN
 endif # MOZ_TSAN
 endif # MOZ_ASAN
 
-endif # ifneq WINNT
-
 ifdef RUST_LIBRARY_FILE
 
 ifdef RUST_LIBRARY_FEATURES
 rust_features_flag := --features '$(RUST_LIBRARY_FEATURES)'
 endif
 
 # Assume any system libraries rustc links against are already in the target's LIBS.
 #
