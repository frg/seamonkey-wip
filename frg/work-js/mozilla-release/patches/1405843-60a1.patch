# HG changeset patch
# User Nicolas B. Pierron <nicolas.b.pierron@gmail.com>
# Date 1519838626 0
#      Wed Feb 28 17:23:46 2018 +0000
# Node ID 42237b8f42470906c1c89257ae7a20829b384983
# Parent  d3b20e603c5fe0c96c0d4a0b58a5b189dc7467b8
Bug 1405843 - JIT RValueAllocation::Mode: Add more assertions. r=jandem datareview=francois

Attempt to get more information for Bug 1405843:
  1. Assert/Crash if the payload check is incorrect, as this is the only code
     path which can give an Invalid RValueAllocation in case of a register
     allocator issue or a memory corruption.
  2. Assert that the RValueAllocation is never invalid.
  3. Dump whatever content manage to gothrough this set of impossibilities.

diff --git a/js/src/jit/Snapshots.cpp b/js/src/jit/Snapshots.cpp
--- a/js/src/jit/Snapshots.cpp
+++ b/js/src/jit/Snapshots.cpp
@@ -259,17 +259,17 @@ RValueAllocation::layoutFromMode(Mode mo
 
         if (mode >= TYPED_REG_MIN && mode <= TYPED_REG_MAX)
             return regLayout;
         if (mode >= TYPED_STACK_MIN && mode <= TYPED_STACK_MAX)
             return stackLayout;
       }
     }
 
-    MOZ_CRASH("Wrong mode type?");
+    MOZ_CRASH_UNSAFE_PRINTF("Unexpected mode: 0x%x", mode);
 }
 
 // Pad serialized RValueAllocations by a multiple of X bytes in the allocation
 // buffer.  By padding serialized value allocations, we are building an
 // indexable table of elements of X bytes, and thus we can safely divide any
 // offset within the buffer by X to obtain an index.
 //
 // By padding, we are loosing space within the allocation buffer, but we
diff --git a/js/src/jit/Snapshots.h b/js/src/jit/Snapshots.h
--- a/js/src/jit/Snapshots.h
+++ b/js/src/jit/Snapshots.h
@@ -298,16 +298,19 @@ class RValueAllocation
     }
 
     void writeHeader(CompactBufferWriter& writer, JSValueType type, uint32_t regCode) const;
   public:
     static RValueAllocation read(CompactBufferReader& reader);
     void write(CompactBufferWriter& writer) const;
 
   public:
+    bool valid() const {
+        return mode_ != INVALID;
+    }
     Mode mode() const {
         return Mode(mode_ & MODE_BITS_MASK);
     }
     bool needSideEffect() const {
         return mode_ & RECOVER_SIDE_EFFECT_MASK;
     }
 
     uint32_t index() const {
diff --git a/js/src/jit/shared/CodeGenerator-shared.cpp b/js/src/jit/shared/CodeGenerator-shared.cpp
--- a/js/src/jit/shared/CodeGenerator-shared.cpp
+++ b/js/src/jit/shared/CodeGenerator-shared.cpp
@@ -447,23 +447,25 @@ CodeGeneratorShared::encodeAllocation(LS
             masm.propagateOOM(graph.addConstantToPool(constant->toJSValue(), &index));
             alloc = RValueAllocation::ConstantPool(index);
             break;
         }
 
         JSValueType valueType =
             (type == MIRType::ObjectOrNull) ? JSVAL_TYPE_OBJECT : ValueTypeFromMIRType(type);
 
-        MOZ_ASSERT(payload->isMemory() || payload->isRegister());
+        MOZ_DIAGNOSTIC_ASSERT(payload->isMemory() || payload->isRegister());
         if (payload->isMemory())
             alloc = RValueAllocation::Typed(valueType, ToStackIndex(payload));
         else if (payload->isGeneralReg())
             alloc = RValueAllocation::Typed(valueType, ToRegister(payload));
         else if (payload->isFloatReg())
             alloc = RValueAllocation::Double(ToFloatRegister(payload));
+        else
+            MOZ_CRASH("Unexpected payload type.");
         break;
       }
       case MIRType::Float32:
       case MIRType::Int8x16:
       case MIRType::Int16x8:
       case MIRType::Int32x4:
       case MIRType::Float32x4:
       case MIRType::Bool8x16:
@@ -536,16 +538,17 @@ CodeGeneratorShared::encodeAllocation(LS
         if (payload->isRegister())
             alloc = RValueAllocation::Untyped(ToRegister(payload));
         else
             alloc = RValueAllocation::Untyped(ToStackIndex(payload));
 #endif
         break;
       }
     }
+    MOZ_DIAGNOSTIC_ASSERT(alloc.valid());
 
     // This set an extra bit as part of the RValueAllocation, such that we know
     // that recover instruction have to be executed without wrapping the
     // instruction in a no-op recover instruction.
     if (mir->isIncompleteObject())
         alloc.setNeedSideEffect();
 
     masm.propagateOOM(snapshots_.add(alloc));
