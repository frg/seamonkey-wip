# HG changeset patch
# User janczer <menshikov.ivn@gmail.com>
# Date 1518002436 18000
# Node ID fd7764a996d8af48a09bd4c2e587daaf64dc2085
# Parent  2e376efbbd473fefef547db1aa7f6c6340126568
servo: Merge #19970 - Change debug assertions to specific ones (from janczer:change_debug_assertions); r=emilio

<!-- Please describe your changes on the following line: -->

---
<!-- Thank you for contributing to Servo! Please replace each `[ ]` by `[X]` when the step is complete, and replace `__` with appropriate data: -->
- [X] `./mach build -d` does not report any errors
- [X] `./mach test-tidy` does not report any errors
- [X] These changes fix #19962 (github issue number if applicable).

<!-- Also, please make sure that "Allow edits from maintainers" checkbox is checked, so that we can help you if you get stuck somewhere along the way.-->

<!-- Pull requests that do not address these steps are welcome, but they will require additional verification as part of the review process. -->

Source-Repo: https://github.com/servo/servo
Source-Revision: 5d209a70ab11cd0ce6c7e086091112570c6e259d

diff --git a/servo/components/hashglobe/src/hash_map.rs b/servo/components/hashglobe/src/hash_map.rs
--- a/servo/components/hashglobe/src/hash_map.rs
+++ b/servo/components/hashglobe/src/hash_map.rs
@@ -494,17 +494,17 @@ fn robin_hood<'a, K: 'a, V: 'a>(bucket: 
         let (old_hash, old_key, old_val) = bucket.replace(hash, key, val);
         hash = old_hash;
         key = old_key;
         val = old_val;
 
         loop {
             displacement += 1;
             let probe = bucket.next();
-            debug_assert!(probe.index() != idx_end);
+            debug_assert_ne!(probe.index(), idx_end);
 
             let full_bucket = match probe.peek() {
                 Empty(bucket) => {
                     // Found a hole!
                     let bucket = bucket.put(hash, key, val);
                     // Now that it's stolen, just read the value's pointer
                     // right out of the table! Go back to the *starting point*.
                     //
@@ -573,17 +573,17 @@ impl<K, V, S> HashMap<K, V, S>
             buckets = match buckets.peek() {
                 Empty(empty) => {
                     empty.put(hash, k, v);
                     return;
                 }
                 Full(b) => b.into_bucket(),
             };
             buckets.next();
-            debug_assert!(buckets.index() != start_index);
+            debug_assert_ne!(buckets.index(), start_index);
         }
     }
 }
 
 impl<K, V, S> HashMap<K, V, S>
     where K: Eq + Hash,
           S: BuildHasher
 {
diff --git a/servo/components/servo_arc/lib.rs b/servo/components/servo_arc/lib.rs
--- a/servo/components/servo_arc/lib.rs
+++ b/servo/components/servo_arc/lib.rs
@@ -593,17 +593,17 @@ impl<H, T> Arc<HeaderSlice<H, [T]>> {
             let mut current: *mut T = &mut (*ptr).data.slice[0];
             for _ in 0..num_items {
                 ptr::write(current, items.next().expect("ExactSizeIterator over-reported length"));
                 current = current.offset(1);
             }
             assert!(items.next().is_none(), "ExactSizeIterator under-reported length");
 
             // We should have consumed the buffer exactly.
-            debug_assert!(current as *mut u8 == buffer.offset(size as isize));
+            debug_assert_eq!(current as *mut u8, buffer.offset(size as isize));
         }
 
         // Return the fat Arc.
         assert_eq!(size_of::<Self>(), size_of::<usize>() * 2, "The Arc will be fat");
         Arc { p: NonZeroPtrMut::new(ptr) }
     }
 
     #[inline]
diff --git a/servo/components/style/context.rs b/servo/components/style/context.rs
--- a/servo/components/style/context.rs
+++ b/servo/components/style/context.rs
@@ -452,17 +452,17 @@ pub enum SequentialTask<E: TElement> {
         tasks: PostAnimationTasks
     },
 }
 
 impl<E: TElement> SequentialTask<E> {
     /// Executes this task.
     pub fn execute(self) {
         use self::SequentialTask::*;
-        debug_assert!(thread_state::get() == ThreadState::LAYOUT);
+        debug_assert_eq!(thread_state::get(), ThreadState::LAYOUT);
         match self {
             Unused(_) => unreachable!(),
             #[cfg(feature = "gecko")]
             UpdateAnimations { el, before_change_style, tasks } => {
                 el.update_animations(before_change_style, tasks);
             }
             #[cfg(feature = "gecko")]
             PostAnimation { el, tasks } => {
@@ -540,17 +540,17 @@ impl<E: TElement> SelectorFlagsMap<E> {
         let f = self.map.entry(el).or_insert(ElementSelectorFlags::empty());
         *f |= flags;
 
         self.cache.insert((unsafe { SendElement::new(element) }, *f))
     }
 
     /// Applies the flags. Must be called on the main thread.
     fn apply_flags(&mut self) {
-        debug_assert!(thread_state::get() == ThreadState::LAYOUT);
+        debug_assert_eq!(thread_state::get(), ThreadState::LAYOUT);
         self.cache.evict_all();
         for (el, flags) in self.map.drain() {
             unsafe { el.set_selector_flags(flags); }
         }
     }
 }
 
 /// A list of SequentialTasks that get executed on Drop.
@@ -578,17 +578,17 @@ where
     }
 }
 
 impl<E> Drop for SequentialTaskList<E>
 where
     E: TElement,
 {
     fn drop(&mut self) {
-        debug_assert!(thread_state::get() == ThreadState::LAYOUT);
+        debug_assert_eq!(thread_state::get(), ThreadState::LAYOUT);
         for task in self.0.drain(..) {
             task.execute()
         }
     }
 }
 
 
 /// A helper type for stack limit checking.  This assumes that stacks grow
@@ -731,17 +731,17 @@ impl<E: TElement> ThreadLocalStyleContex
                 (STYLE_THREAD_STACK_SIZE_KB - STACK_SAFETY_MARGIN_KB) * 1024),
             nth_index_cache: NthIndexCache::default(),
         }
     }
 }
 
 impl<E: TElement> Drop for ThreadLocalStyleContext<E> {
     fn drop(&mut self) {
-        debug_assert!(thread_state::get() == ThreadState::LAYOUT);
+        debug_assert_eq!(thread_state::get(), ThreadState::LAYOUT);
 
         // Apply any slow selector flags that need to be set on parents.
         self.selector_flags.apply_flags();
     }
 }
 
 /// A `StyleContext` is just a simple container for a immutable reference to a
 /// shared style context, and a mutable reference to a local one.
diff --git a/servo/components/style/gecko/media_queries.rs b/servo/components/style/gecko/media_queries.rs
--- a/servo/components/style/gecko/media_queries.rs
+++ b/servo/components/style/gecko/media_queries.rs
@@ -365,45 +365,45 @@ impl MediaExpressionValue {
         // NB: If there's a null value, that means that we don't support the
         // feature.
         if css_value.mUnit == nsCSSUnit::eCSSUnit_Null {
             return None;
         }
 
         match for_expr.feature.mValueType {
             nsMediaFeature_ValueType::eLength => {
-                debug_assert!(css_value.mUnit == nsCSSUnit::eCSSUnit_Pixel);
+                debug_assert_eq!(css_value.mUnit, nsCSSUnit::eCSSUnit_Pixel);
                 let pixels = css_value.float_unchecked();
                 Some(MediaExpressionValue::Length(Length::from_px(pixels)))
             }
             nsMediaFeature_ValueType::eInteger => {
                 let i = css_value.integer_unchecked();
                 debug_assert!(i >= 0);
                 Some(MediaExpressionValue::Integer(i as u32))
             }
             nsMediaFeature_ValueType::eFloat => {
-                debug_assert!(css_value.mUnit == nsCSSUnit::eCSSUnit_Number);
+                debug_assert_eq!(css_value.mUnit, nsCSSUnit::eCSSUnit_Number);
                 Some(MediaExpressionValue::Float(css_value.float_unchecked()))
             }
             nsMediaFeature_ValueType::eBoolInteger => {
-                debug_assert!(css_value.mUnit == nsCSSUnit::eCSSUnit_Integer);
+                debug_assert_eq!(css_value.mUnit, nsCSSUnit::eCSSUnit_Integer);
                 let i = css_value.integer_unchecked();
                 debug_assert!(i == 0 || i == 1);
                 Some(MediaExpressionValue::BoolInteger(i == 1))
             }
             nsMediaFeature_ValueType::eResolution => {
-                debug_assert!(css_value.mUnit == nsCSSUnit::eCSSUnit_Pixel);
+                debug_assert_eq!(css_value.mUnit, nsCSSUnit::eCSSUnit_Pixel);
                 Some(MediaExpressionValue::Resolution(Resolution::Dppx(css_value.float_unchecked())))
             }
             nsMediaFeature_ValueType::eEnumerated => {
                 let value = css_value.integer_unchecked() as i16;
                 Some(MediaExpressionValue::Enumerated(value))
             }
             nsMediaFeature_ValueType::eIdent => {
-                debug_assert!(css_value.mUnit == nsCSSUnit::eCSSUnit_Ident);
+                debug_assert_eq!(css_value.mUnit, nsCSSUnit::eCSSUnit_Ident);
                 let string = unsafe {
                     let buffer = *css_value.mValue.mString.as_ref();
                     debug_assert!(!buffer.is_null());
                     string_from_chars_pointer(buffer.offset(1) as *const u16)
                 };
                 Some(MediaExpressionValue::Ident(string))
             }
             nsMediaFeature_ValueType::eIntRatio => {
@@ -770,21 +770,21 @@ impl Expression {
                     } else {
                         other.to_dpi()
                     }
                 };
 
                 one.to_dpi().partial_cmp(&actual_dpi).unwrap()
             }
             (&Ident(ref one), &Ident(ref other)) => {
-                debug_assert!(self.feature.mRangeType != nsMediaFeature_RangeType::eMinMaxAllowed);
+                debug_assert_ne!(self.feature.mRangeType, nsMediaFeature_RangeType::eMinMaxAllowed);
                 return one == other;
             }
             (&Enumerated(one), &Enumerated(other)) => {
-                debug_assert!(self.feature.mRangeType != nsMediaFeature_RangeType::eMinMaxAllowed);
+                debug_assert_ne!(self.feature.mRangeType, nsMediaFeature_RangeType::eMinMaxAllowed);
                 return one == other;
             }
             _ => unreachable!(),
         };
 
         cmp == Ordering::Equal || match self.range {
             Range::Min => cmp == Ordering::Less,
             Range::Equal => false,
diff --git a/servo/components/style/gecko_bindings/sugar/ns_style_coord.rs b/servo/components/style/gecko_bindings/sugar/ns_style_coord.rs
--- a/servo/components/style/gecko_bindings/sugar/ns_style_coord.rs
+++ b/servo/components/style/gecko_bindings/sugar/ns_style_coord.rs
@@ -373,17 +373,17 @@ pub unsafe trait CoordDataMut : CoordDat
             }
         }
     }
 
     #[inline]
     /// Gets the `Calc` value mutably, asserts in debug builds if the unit is
     /// not `Calc`.
     unsafe fn as_calc_mut(&mut self) -> &mut nsStyleCoord_Calc {
-        debug_assert!(self.unit() == nsStyleUnit::eStyleUnit_Calc);
+        debug_assert_eq!(self.unit(), nsStyleUnit::eStyleUnit_Calc);
         &mut *(*self.union().mPointer.as_mut() as *mut nsStyleCoord_Calc)
     }
 
     #[inline]
     /// Does what it promises, if the unit is `calc`, it bumps the reference
     /// count _of the calc expression_.
     fn addref_if_calc(&mut self) {
         unsafe {
@@ -446,20 +446,20 @@ pub unsafe trait CoordData {
                       || self.unit() == eStyleUnit_Enumerated);
         *self.union().mInt.as_ref()
     }
 
     #[inline]
     /// Pretend inner value is a calc; obtain it.
     /// Ensure that the unit is Calc before calling this.
     unsafe fn get_calc_value(&self) -> nsStyleCoord_CalcValue {
-        debug_assert!(self.unit() == nsStyleUnit::eStyleUnit_Calc);
+        debug_assert_eq!(self.unit(), nsStyleUnit::eStyleUnit_Calc);
         (*self.as_calc())._base
     }
 
 
     #[inline]
     /// Pretend the inner value is a calc expression, and obtain it.
     unsafe fn as_calc(&self) -> &nsStyleCoord_Calc {
-        debug_assert!(self.unit() == nsStyleUnit::eStyleUnit_Calc);
+        debug_assert_eq!(self.unit(), nsStyleUnit::eStyleUnit_Calc);
         &*(*self.union().mPointer.as_ref() as *const nsStyleCoord_Calc)
     }
 }
diff --git a/servo/components/style/properties/gecko.mako.rs b/servo/components/style/properties/gecko.mako.rs
--- a/servo/components/style/properties/gecko.mako.rs
+++ b/servo/components/style/properties/gecko.mako.rs
@@ -1208,17 +1208,17 @@ fn clone_single_transform_function(
     gecko_value: &structs::nsCSSValue
 ) -> values::computed::TransformOperation {
     use values::computed::{Length, Percentage, TransformOperation};
     use values::generics::transform::{Matrix, Matrix3D};
     use values::generics::transform::Transform;
 
     let convert_shared_list_to_operations = |value: &structs::nsCSSValue|
                                             -> Vec<TransformOperation> {
-        debug_assert!(value.mUnit == structs::nsCSSUnit::eCSSUnit_SharedList);
+        debug_assert_eq!(value.mUnit, structs::nsCSSUnit::eCSSUnit_SharedList);
         let value_list = unsafe {
             value.mValue.mSharedList.as_ref()
                     .as_mut().expect("List pointer should be non-null").mHead.as_ref()
         };
         debug_assert!(value_list.is_some(), "An empty shared list is not allowed");
         value_list.unwrap().into_iter()
                             .map(|item| clone_single_transform_function(item))
                             .collect()
@@ -1870,17 +1870,17 @@ fn static_assert() {
             Either::Second(Auto) => self.gecko.mZIndex.set_value(CoordDataValue::Auto),
         }
     }
 
     pub fn copy_z_index_from(&mut self, other: &Self) {
         use gecko_bindings::structs::nsStyleUnit;
         // z-index is never a calc(). If it were, we'd be leaking here, so
         // assert that it isn't.
-        debug_assert!(self.gecko.mZIndex.unit() != nsStyleUnit::eStyleUnit_Calc);
+        debug_assert_ne!(self.gecko.mZIndex.unit(), nsStyleUnit::eStyleUnit_Calc);
         unsafe {
             self.gecko.mZIndex.copy_from_unchecked(&other.gecko.mZIndex);
         }
     }
 
     pub fn reset_z_index(&mut self, other: &Self) {
         self.copy_z_index_from(other)
     }
@@ -1903,17 +1903,17 @@ fn static_assert() {
     ${impl_simple_type_with_conversion("align_items")}
 
     pub fn set_justify_items(&mut self, v: longhands::justify_items::computed_value::T) {
         self.gecko.mSpecifiedJustifyItems = v.specified.into();
         self.set_computed_justify_items(v.computed);
     }
 
     pub fn set_computed_justify_items(&mut self, v: values::specified::JustifyItems) {
-        debug_assert!(v.0 != ::values::specified::align::AlignFlags::AUTO);
+        debug_assert_ne!(v.0, ::values::specified::align::AlignFlags::AUTO);
         self.gecko.mJustifyItems = v.into();
     }
 
     pub fn reset_justify_items(&mut self, reset_style: &Self) {
         self.gecko.mJustifyItems = reset_style.gecko.mJustifyItems;
         self.gecko.mSpecifiedJustifyItems = reset_style.gecko.mSpecifiedJustifyItems;
     }
 
@@ -2951,17 +2951,17 @@ fn static_assert() {
 
 <%def name="impl_animation_or_transition_time_value(type, ident, gecko_ffi_name)">
     #[allow(non_snake_case)]
     pub fn set_${type}_${ident}<I>(&mut self, v: I)
         where I: IntoIterator<Item = longhands::${type}_${ident}::computed_value::single_value::T>,
               I::IntoIter: ExactSizeIterator + Clone
     {
         let v = v.into_iter();
-        debug_assert!(v.len() != 0);
+        debug_assert_ne!(v.len(), 0);
         let input_len = v.len();
         self.gecko.m${type.capitalize()}s.ensure_len(input_len);
 
         self.gecko.m${type.capitalize()}${gecko_ffi_name}Count = input_len as u32;
         for (gecko, servo) in self.gecko.m${type.capitalize()}s.iter_mut().take(input_len as usize).zip(v) {
             gecko.m${gecko_ffi_name} = servo.seconds() * 1000.;
         }
     }
@@ -2976,17 +2976,17 @@ fn static_assert() {
 </%def>
 
 <%def name="impl_animation_or_transition_timing_function(type)">
     pub fn set_${type}_timing_function<I>(&mut self, v: I)
         where I: IntoIterator<Item = longhands::${type}_timing_function::computed_value::single_value::T>,
               I::IntoIter: ExactSizeIterator + Clone
     {
         let v = v.into_iter();
-        debug_assert!(v.len() != 0);
+        debug_assert_ne!(v.len(), 0);
         let input_len = v.len();
         self.gecko.m${type.capitalize()}s.ensure_len(input_len);
 
         self.gecko.m${type.capitalize()}TimingFunctionCount = input_len as u32;
         for (gecko, servo) in self.gecko.m${type.capitalize()}s.iter_mut().take(input_len as usize).zip(v) {
             gecko.mTimingFunction = servo.into();
         }
     }
@@ -3032,17 +3032,17 @@ fn static_assert() {
         where I: IntoIterator<Item = longhands::animation_${ident}::computed_value::single_value::T>,
               I::IntoIter: ExactSizeIterator + Clone
     {
         use properties::longhands::animation_${ident}::single_value::computed_value::T as Keyword;
         use gecko_bindings::structs;
 
         let v = v.into_iter();
 
-        debug_assert!(v.len() != 0);
+        debug_assert_ne!(v.len(), 0);
         let input_len = v.len();
         self.gecko.mAnimations.ensure_len(input_len);
 
         self.gecko.mAnimation${gecko_ffi_name}Count = input_len as u32;
 
         for (gecko, servo) in self.gecko.mAnimations.iter_mut().take(input_len as usize).zip(v) {
             let result = match servo {
                 % for value in keyword.gecko_values():
@@ -3432,17 +3432,17 @@ fn static_assert() {
             && unsafe { bindings::Gecko_StyleAnimationsEquals(&self.gecko.mAnimations, &other.gecko.mAnimations) }
     }
 
     pub fn set_animation_name<I>(&mut self, v: I)
         where I: IntoIterator<Item = longhands::animation_name::computed_value::single_value::T>,
               I::IntoIter: ExactSizeIterator
     {
         let v = v.into_iter();
-        debug_assert!(v.len() != 0);
+        debug_assert_ne!(v.len(), 0);
         self.gecko.mAnimations.ensure_len(v.len());
 
         self.gecko.mAnimationNameCount = v.len() as u32;
         for (servo, gecko) in v.zip(self.gecko.mAnimations.iter_mut()) {
             let atom = match servo.0 {
                 None => atom!(""),
                 Some(ref name) => name.as_atom().clone(),
             };
@@ -4025,29 +4025,29 @@ fn static_assert() {
         use gecko_bindings::structs::nsStyleImageLayers_Size_DimensionType as DimensionType;
         use values::computed::LengthOrPercentageOrAuto;
         use values::generics::background::BackgroundSize;
 
         fn to_servo(value: CalcValue, ty: u8) -> LengthOrPercentageOrAuto {
             if ty == DimensionType::eAuto as u8 {
                 LengthOrPercentageOrAuto::Auto
             } else {
-                debug_assert!(ty == DimensionType::eLengthPercentage as u8);
+                debug_assert_eq!(ty, DimensionType::eLengthPercentage as u8);
                 value.into()
             }
         }
 
         longhands::background_size::computed_value::T(
             self.gecko.${image_layers_field}.mLayers.iter().map(|ref layer| {
                 if DimensionType::eCover as u8 == layer.mSize.mWidthType {
-                    debug_assert!(layer.mSize.mHeightType == DimensionType::eCover as u8);
+                    debug_assert_eq!(layer.mSize.mHeightType, DimensionType::eCover as u8);
                     return BackgroundSize::Cover
                 }
                 if DimensionType::eContain as u8 == layer.mSize.mWidthType {
-                    debug_assert!(layer.mSize.mHeightType == DimensionType::eContain as u8);
+                    debug_assert_eq!(layer.mSize.mHeightType, DimensionType::eContain as u8);
                     return BackgroundSize::Contain
                 }
                 BackgroundSize::Explicit {
                     width: to_servo(layer.mSize.mWidth._base, layer.mSize.mWidthType),
                     height: to_servo(layer.mSize.mHeight._base, layer.mSize.mHeightType),
                 }
             }).collect()
         )
@@ -4436,38 +4436,38 @@ fn static_assert() {
         use gecko_bindings::structs::NS_STYLE_CLIP_TOP_AUTO;
         use values::computed::{ClipRect, ClipRectOrAuto};
         use values::Either;
 
         if self.gecko.mClipFlags == NS_STYLE_CLIP_AUTO as u8 {
             ClipRectOrAuto::auto()
         } else {
             let left = if self.gecko.mClipFlags & NS_STYLE_CLIP_LEFT_AUTO as u8 != 0 {
-                debug_assert!(self.gecko.mClip.x == 0);
+                debug_assert_eq!(self.gecko.mClip.x, 0);
                 None
             } else {
                 Some(Au(self.gecko.mClip.x).into())
             };
 
             let top = if self.gecko.mClipFlags & NS_STYLE_CLIP_TOP_AUTO as u8 != 0 {
-                debug_assert!(self.gecko.mClip.y == 0);
+                debug_assert_eq!(self.gecko.mClip.y, 0);
                 None
             } else {
                 Some(Au(self.gecko.mClip.y).into())
             };
 
             let bottom = if self.gecko.mClipFlags & NS_STYLE_CLIP_BOTTOM_AUTO as u8 != 0 {
-                debug_assert!(self.gecko.mClip.height == 1 << 30); // NS_MAXSIZE
+                debug_assert_eq!(self.gecko.mClip.height, 1 << 30); // NS_MAXSIZE
                 None
             } else {
                 Some(Au(self.gecko.mClip.y + self.gecko.mClip.height).into())
             };
 
             let right = if self.gecko.mClipFlags & NS_STYLE_CLIP_RIGHT_AUTO as u8 != 0 {
-                debug_assert!(self.gecko.mClip.width == 1 << 30); // NS_MAXSIZE
+                debug_assert_eq!(self.gecko.mClip.width, 1 << 30); // NS_MAXSIZE
                 None
             } else {
                 Some(Au(self.gecko.mClip.x + self.gecko.mClip.width).into())
             };
 
             Either::First(ClipRect { top: top, right: right, bottom: bottom, left: left, })
         }
     }
diff --git a/servo/components/style/properties/helpers.mako.rs b/servo/components/style/properties/helpers.mako.rs
--- a/servo/components/style/properties/helpers.mako.rs
+++ b/servo/components/style/properties/helpers.mako.rs
@@ -298,17 +298,17 @@
             declaration: &PropertyDeclaration,
             context: &mut computed::Context,
         ) {
             let value = match *declaration {
                 PropertyDeclaration::${property.camel_case}(ref value) => {
                     DeclaredValue::Value(value)
                 },
                 PropertyDeclaration::CSSWideKeyword(ref declaration) => {
-                    debug_assert!(declaration.id == LonghandId::${property.camel_case});
+                    debug_assert_eq!(declaration.id, LonghandId::${property.camel_case});
                     DeclaredValue::CSSWideKeyword(declaration.keyword)
                 },
                 PropertyDeclaration::WithVariables(..) => {
                     panic!("variables should already have been substituted")
                 }
                 _ => panic!("entered the wrong cascade_property() implementation"),
             };
 
diff --git a/servo/components/style/rule_tree/mod.rs b/servo/components/style/rule_tree/mod.rs
--- a/servo/components/style/rule_tree/mod.rs
+++ b/servo/components/style/rule_tree/mod.rs
@@ -50,17 +50,17 @@ pub struct RuleTree {
 }
 
 impl Drop for RuleTree {
     fn drop(&mut self) {
         // GC the rule tree.
         unsafe { self.gc(); }
 
         // After the GC, the free list should be empty.
-        debug_assert!(self.root.get().next_free.load(Ordering::Relaxed) == FREE_LIST_SENTINEL);
+        debug_assert_eq!(self.root.get().next_free.load(Ordering::Relaxed), FREE_LIST_SENTINEL);
 
         // Remove the sentinel. This indicates that GCs will no longer occur.
         // Any further drops of StrongRuleNodes must occur on the main thread,
         // and will trigger synchronous dropping of the Rule nodes.
         self.root.get().next_free.store(ptr::null_mut(), Ordering::Relaxed);
     }
 }
 
@@ -841,17 +841,17 @@ pub struct StrongRuleNode {
     p: NonZeroPtrMut<RuleNode>,
 }
 
 #[cfg(feature = "servo")]
 malloc_size_of_is_0!(StrongRuleNode);
 
 impl StrongRuleNode {
     fn new(n: Box<RuleNode>) -> Self {
-        debug_assert!(n.parent.is_none() == !n.source.is_some());
+        debug_assert_eq!(n.parent.is_none(), !n.source.is_some());
 
         let ptr = Box::into_raw(n);
         log_new(ptr);
 
         debug!("Creating rule node: {:p}", ptr);
 
         StrongRuleNode::from_ptr(ptr)
     }
@@ -1069,17 +1069,17 @@ impl StrongRuleNode {
             debug!("GC'ing {:?}", weak.ptr());
             node.remove_from_child_list();
             log_drop(weak.ptr());
             let _ = Box::from_raw(weak.ptr());
         }
 
         me.free_count().store(0, Ordering::Relaxed);
 
-        debug_assert!(me.next_free.load(Ordering::Relaxed) == FREE_LIST_SENTINEL);
+        debug_assert_eq!(me.next_free.load(Ordering::Relaxed), FREE_LIST_SENTINEL);
     }
 
     unsafe fn maybe_gc(&self) {
         debug_assert!(self.get().is_root(), "Can't call GC on a non-root node!");
         if self.get().free_count().load(Ordering::Relaxed) > RULE_TREE_GC_INTERVAL {
             self.gc();
         }
     }
diff --git a/servo/components/style/stylist.rs b/servo/components/style/stylist.rs
--- a/servo/components/style/stylist.rs
+++ b/servo/components/style/stylist.rs
@@ -993,17 +993,17 @@ impl Stylist {
             // No need to bother setting the selector flags when we're computing
             // default styles.
             if rule_inclusion == RuleInclusion::DefaultOnly {
                 return;
             }
 
             // Gecko calls this from sequential mode, so we can directly apply
             // the flags.
-            debug_assert!(thread_state::get() == ThreadState::LAYOUT);
+            debug_assert_eq!(thread_state::get(), ThreadState::LAYOUT);
             let self_flags = flags.for_self();
             if !self_flags.is_empty() {
                 unsafe { element.set_selector_flags(self_flags); }
             }
             let parent_flags = flags.for_parent();
             if !parent_flags.is_empty() {
                 if let Some(p) = element.parent_element() {
                     unsafe { p.set_selector_flags(parent_flags); }
diff --git a/servo/components/style/values/computed/text.rs b/servo/components/style/values/computed/text.rs
--- a/servo/components/style/values/computed/text.rs
+++ b/servo/components/style/values/computed/text.rs
@@ -66,17 +66,17 @@ impl TextOverflow {
 }
 
 impl ToCss for TextOverflow {
     fn to_css<W>(&self, dest: &mut CssWriter<W>) -> fmt::Result
     where
         W: Write,
     {
         if self.sides_are_logical {
-            debug_assert!(self.first == TextOverflowSide::Clip);
+            debug_assert_eq!(self.first, TextOverflowSide::Clip);
             self.second.to_css(dest)?;
         } else {
             self.first.to_css(dest)?;
             dest.write_str(" ")?;
             self.second.to_css(dest)?;
         }
         Ok(())
     }
diff --git a/servo/components/style/values/specified/color.rs b/servo/components/style/values/specified/color.rs
--- a/servo/components/style/values/specified/color.rs
+++ b/servo/components/style/values/specified/color.rs
@@ -310,17 +310,17 @@ impl Color {
         }
         let mut serialization = [b'0'; 6];
         let space_padding = 6 - total;
         let mut written = space_padding;
         written += itoa::write(&mut serialization[written..], value).unwrap();
         if let Some(unit) = unit {
             written += (&mut serialization[written..]).write(unit.as_bytes()).unwrap();
         }
-        debug_assert!(written == 6);
+        debug_assert_eq!(written, 6);
         parse_hash_color(&serialization).map_err(|()| {
             location.new_custom_error(StyleParseErrorKind::UnspecifiedError)
         })
     }
 
     /// Returns false if the color is completely transparent, and
     /// true otherwise.
     pub fn is_non_transparent(&self) -> bool {
diff --git a/servo/ports/geckolib/glue.rs b/servo/ports/geckolib/glue.rs
--- a/servo/ports/geckolib/glue.rs
+++ b/servo/ports/geckolib/glue.rs
@@ -4143,17 +4143,17 @@ pub extern "C" fn Servo_StyleSet_GetKeyf
 }
 
 #[no_mangle]
 pub extern "C" fn Servo_StyleSet_GetFontFaceRules(
     raw_data: RawServoStyleSetBorrowed,
     rules: RawGeckoFontFaceRuleListBorrowedMut,
 ) {
     let data = PerDocumentStyleData::from_ffi(raw_data).borrow();
-    debug_assert!(rules.len() == 0);
+    debug_assert_eq!(rules.len(), 0);
 
     let global_style_data = &*GLOBAL_STYLE_DATA;
     let guard = global_style_data.shared_lock.read();
 
     let len: u32 = data
         .stylist
         .iter_extra_data_origins()
         .map(|(d, _)| d.font_faces.len() as u32)
diff --git a/servo/support/gecko/nsstring/src/lib.rs b/servo/support/gecko/nsstring/src/lib.rs
--- a/servo/support/gecko/nsstring/src/lib.rs
+++ b/servo/support/gecko/nsstring/src/lib.rs
@@ -382,17 +382,17 @@ macro_rules! define_string_types {
             fn deref(&self) -> &[$char_t] {
                 unsafe {
                     // All $AString values point to a struct prefix which is
                     // identical to $StringRepr, this we can transmute `self`
                     // into $StringRepr to get the reference to the underlying
                     // data.
                     let this: &$StringRepr = mem::transmute(self);
                     if this.data.is_null() {
-                        debug_assert!(this.length == 0);
+                        debug_assert_eq!(this.length, 0);
                         // Use an arbitrary non-null value as the pointer
                         slice::from_raw_parts(0x1 as *const $char_t, 0)
                     } else {
                         slice::from_raw_parts(this.data, this.length as usize)
                     }
                 }
             }
         }
