# HG changeset patch
# User Jason Orendorff <jorendorff@mozilla.com>
# Date 1519326286 21600
# Node ID 51d604ea785bbc46db7f88235faef597f57e671f
# Parent  97a8d0d0c73aa111de9efc9ba928ff19f4e49242
Bug 1440431 - Part 3: Add baselineCompile() testing function. r=nbp.

diff --git a/js/src/builtin/TestingFunctions.cpp b/js/src/builtin/TestingFunctions.cpp
--- a/js/src/builtin/TestingFunctions.cpp
+++ b/js/src/builtin/TestingFunctions.cpp
@@ -22,16 +22,17 @@
 #include "builtin/Promise.h"
 #include "builtin/SelfHostingDefines.h"
 #ifdef DEBUG
 #include "frontend/TokenStream.h"
 #include "irregexp/RegExpAST.h"
 #include "irregexp/RegExpEngine.h"
 #include "irregexp/RegExpParser.h"
 #endif
+#include "jit/BaselineJIT.h"
 #include "jit/InlinableNatives.h"
 #include "js/Debug.h"
 #include "js/HashTable.h"
 #include "js/StructuredClone.h"
 #include "js/UbiNode.h"
 #include "js/UbiNodeBreadthFirst.h"
 #include "js/UbiNodeShortestPaths.h"
 #include "js/UniquePtr.h"
@@ -4996,16 +4997,82 @@ js::TestingFunctionArgumentToScript(JSCo
         return nullptr;
 
     if (funp)
         *funp = fun;
 
     return script;
 }
 
+static bool
+BaselineCompile(JSContext* cx, unsigned argc, Value* vp)
+{
+    CallArgs args = CallArgsFromVp(argc, vp);
+    RootedObject callee(cx, &args.callee());
+
+    RootedScript script(cx);
+    if (args.length() == 0) {
+        NonBuiltinScriptFrameIter iter(cx);
+        if (iter.done()) {
+            ReportUsageErrorASCII(cx, callee, "no script argument and no script caller");
+            return false;
+        }
+        script = iter.script();
+    } else {
+        script = TestingFunctionArgumentToScript(cx, args[0]);
+        if (!script)
+            return false;
+    }
+
+    bool forceDebug = false;
+    if (args.length() > 1) {
+        if (args.length() > 2) {
+            ReportUsageErrorASCII(cx, callee, "too many arguments");
+            return false;
+        }
+        if (!args[1].isBoolean() && !args[1].isUndefined()) {
+            ReportUsageErrorASCII(cx, callee, "forceDebugInstrumentation argument should be boolean");
+            return false;
+        }
+        forceDebug = ToBoolean(args[1]);
+    }
+
+    if (script->hasBaselineScript()) {
+        if (forceDebug && !script->baselineScript()->hasDebugInstrumentation()) {
+            // There isn't an easy way to do this for a script that might be on
+            // stack right now. See js::jit::RecompileOnStackBaselineScriptsForDebugMode.
+            ReportUsageErrorASCII(cx, callee,
+                                  "unsupported case: recompiling script for debug mode");
+            return false;
+        }
+
+        args.rval().setUndefined();
+        return true;
+    }
+
+    if (!jit::IsBaselineEnabled(cx))
+        return ReturnStringCopy(cx, args, "baseline disabled");
+    if (!script->canBaselineCompile())
+        return ReturnStringCopy(cx, args, "can't compile");
+
+    jit::MethodStatus status = jit::BaselineCompile(cx, script, forceDebug);
+    switch (status) {
+      case jit::Method_Error:
+        return false;
+      case jit::Method_CantCompile:
+        return ReturnStringCopy(cx, args, "can't compile");
+      case jit::Method_Skipped:
+        return ReturnStringCopy(cx, args, "skipped");
+      case jit::Method_Compiled:
+        args.rval().setUndefined();
+    }
+
+    return true;
+}
+
 static const JSFunctionSpecWithHelp TestingFunctions[] = {
     JS_FN_HELP("gc", ::GC, 0, 0,
 "gc([obj] | 'zone' [, 'shrinking'])",
 "  Run the garbage collector. When obj is given, GC only its zone.\n"
 "  If 'zone' is given, GC any zones that were scheduled for\n"
 "  GC via schedulegc.\n"
 "  If 'shrinking' is passed as the optional second argument, perform a\n"
 "  shrinking GC rather than a normal GC."),
@@ -5619,16 +5686,25 @@ gc::ZealModeHelpText),
     JS_FN_HELP("isLegacyIterator", IsLegacyIterator, 1, 0,
 "isLegacyIterator(value)",
 "  Returns whether the value is considered is a legacy iterator.\n"),
 
     JS_FN_HELP("getTimeZone", GetTimeZone, 0, 0,
 "getTimeZone()",
 "  Get the current time zone.\n"),
 
+    JS_FN_HELP("baselineCompile", BaselineCompile, 2, 0,
+"baselineCompile([fun/code], forceDebugInstrumentation=false)",
+"  Baseline-compiles the given JS function or script.\n"
+"  Without arguments, baseline-compiles the caller's script; but note\n"
+"  that extra boilerplate is needed afterwards to cause the VM to start\n"
+"  running the jitcode rather than staying in the interpreter:\n"
+"    baselineCompile();  for (var i=0; i<1; i++) {} ...\n"
+"  The interpreter will enter the new jitcode at the loop header.\n"),
+
     JS_FS_HELP_END
 };
 
 static const JSFunctionSpecWithHelp FuzzingUnsafeTestingFunctions[] = {
 #ifdef DEBUG
     JS_FN_HELP("parseRegExp", ParseRegExp, 3, 0,
 "parseRegExp(pattern[, flags[, match_only])",
 "  Parses a RegExp pattern and returns a tree, potentially throwing."),
diff --git a/js/src/jit-test/tests/self-test/baselineCompile.js b/js/src/jit-test/tests/self-test/baselineCompile.js
new file mode 100644
--- /dev/null
+++ b/js/src/jit-test/tests/self-test/baselineCompile.js
@@ -0,0 +1,20 @@
+// Check that the help text for baselineCompile() is accurate.
+
+if (typeof inJit == "function" && typeof baselineCompile == "function") {
+    if (!inJit()) {
+
+        baselineCompile();  // compile the current script
+
+        assertEq(inJit(), false,
+                 "We have compiled this script to baseline jitcode, but shouldn't " +
+                 "be running it yet, according to the help text for baselineCompile() " +
+                 "in TestingFunctions.cpp. If you fail this assertion, nice work, and " +
+                 "please update the help text!");
+
+        for (var i=0; i<1; i++) {}  // exact boilerplate suggested by the help text
+
+        assertEq(inJit(), true,
+                 "help text in TestingFunctions.cpp claims the above loop causes " +
+                 "the interpreter to start running the new baseline jitcode");
+    }
+}
