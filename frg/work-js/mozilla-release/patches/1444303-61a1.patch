# HG changeset patch
# User Dragan Mladjenovic <dragan.mladjenovic@rt-rk.com>
# Date 1520578723 -3600
# Node ID 7de4ca7b082ad493430e026e650c842faf3e41c5
# Parent  25ade148226d93650b653c62e2f4b15cbc51caef
Bug 1444303 : [MIPS] Fix build failures after Bug 1425580 part 17; r=jandem

diff --git a/js/src/jit/mips-shared/LIR-mips-shared.h b/js/src/jit/mips-shared/LIR-mips-shared.h
--- a/js/src/jit/mips-shared/LIR-mips-shared.h
+++ b/js/src/jit/mips-shared/LIR-mips-shared.h
@@ -11,40 +11,45 @@ namespace js {
 namespace jit {
 
 // Convert a 32-bit unsigned integer to a double.
 class LWasmUint32ToDouble : public LInstructionHelper<1, 1, 0>
 {
   public:
     LIR_HEADER(WasmUint32ToDouble)
 
-    LWasmUint32ToDouble(const LAllocation& input) {
+    LWasmUint32ToDouble(const LAllocation& input)
+      : LInstructionHelper(classOpcode)
+    {
         setOperand(0, input);
     }
 };
 
 // Convert a 32-bit unsigned integer to a float32.
 class LWasmUint32ToFloat32 : public LInstructionHelper<1, 1, 0>
 {
   public:
     LIR_HEADER(WasmUint32ToFloat32)
 
-    LWasmUint32ToFloat32(const LAllocation& input) {
+    LWasmUint32ToFloat32(const LAllocation& input)
+      : LInstructionHelper(classOpcode)
+    {
         setOperand(0, input);
     }
 };
 
 
 class LDivI : public LBinaryMath<1>
 {
   public:
     LIR_HEADER(DivI);
 
-    LDivI(const LAllocation& lhs, const LAllocation& rhs,
-          const LDefinition& temp) {
+    LDivI(const LAllocation& lhs, const LAllocation& rhs, const LDefinition& temp)
+      : LBinaryMath(classOpcode)
+    {
         setOperand(0, lhs);
         setOperand(1, rhs);
         setTemp(0, temp);
     }
 
     MDiv* mir() const {
         return mir_->toDiv();
     }
@@ -53,122 +58,121 @@ class LDivI : public LBinaryMath<1>
 class LDivPowTwoI : public LInstructionHelper<1, 1, 1>
 {
     const int32_t shift_;
 
   public:
     LIR_HEADER(DivPowTwoI)
 
     LDivPowTwoI(const LAllocation& lhs, int32_t shift, const LDefinition& temp)
-      : shift_(shift)
+      : LInstructionHelper(classOpcode),
+        shift_(shift)
     {
         setOperand(0, lhs);
         setTemp(0, temp);
     }
 
     const LAllocation* numerator() {
         return getOperand(0);
     }
-
-    int32_t shift() {
+    int32_t shift() const {
         return shift_;
     }
-
     MDiv* mir() const {
         return mir_->toDiv();
     }
 };
 
 class LModI : public LBinaryMath<1>
 {
   public:
     LIR_HEADER(ModI);
 
-    LModI(const LAllocation& lhs, const LAllocation& rhs,
-          const LDefinition& callTemp)
+    LModI(const LAllocation& lhs, const LAllocation& rhs, const LDefinition& callTemp)
+      : LBinaryMath(classOpcode)
     {
         setOperand(0, lhs);
         setOperand(1, rhs);
         setTemp(0, callTemp);
     }
 
     const LDefinition* callTemp() {
         return getTemp(0);
     }
-
     MMod* mir() const {
         return mir_->toMod();
     }
 };
 
 class LModPowTwoI : public LInstructionHelper<1, 1, 0>
 {
     const int32_t shift_;
 
   public:
     LIR_HEADER(ModPowTwoI);
-    int32_t shift()
-    {
-        return shift_;
-    }
 
     LModPowTwoI(const LAllocation& lhs, int32_t shift)
-      : shift_(shift)
+      : LInstructionHelper(classOpcode),
+        shift_(shift)
     {
         setOperand(0, lhs);
     }
 
+    int32_t shift() const {
+        return shift_;
+    }
     MMod* mir() const {
         return mir_->toMod();
     }
 };
 
 class LModMaskI : public LInstructionHelper<1, 1, 2>
 {
     const int32_t shift_;
 
   public:
     LIR_HEADER(ModMaskI);
 
     LModMaskI(const LAllocation& lhs, const LDefinition& temp0, const LDefinition& temp1,
               int32_t shift)
-      : shift_(shift)
+      : LInstructionHelper(classOpcode),
+        shift_(shift)
     {
         setOperand(0, lhs);
         setTemp(0, temp0);
         setTemp(1, temp1);
     }
 
     int32_t shift() const {
         return shift_;
     }
-
     MMod* mir() const {
         return mir_->toMod();
     }
 };
 
 // Takes a tableswitch with an integer to decide
 class LTableSwitch : public LInstructionHelper<0, 1, 2>
 {
   public:
     LIR_HEADER(TableSwitch);
 
     LTableSwitch(const LAllocation& in, const LDefinition& inputCopy,
-                 const LDefinition& jumpTablePointer, MTableSwitch* ins) {
+                 const LDefinition& jumpTablePointer, MTableSwitch* ins)
+      : LInstructionHelper(classOpcode)
+    {
         setOperand(0, in);
         setTemp(0, inputCopy);
         setTemp(1, jumpTablePointer);
         setMir(ins);
     }
 
     MTableSwitch* mir() const {
         return mir_->toTableSwitch();
     }
-
     const LAllocation* index() {
         return getOperand(0);
     }
     const LDefinition* tempInt() {
         return getTemp(0);
     }
     // This is added to share the same CodeGenerator prefixes.
     const LDefinition* tempPointer() {
@@ -180,16 +184,17 @@ class LTableSwitch : public LInstruction
 class LTableSwitchV : public LInstructionHelper<0, BOX_PIECES, 3>
 {
   public:
     LIR_HEADER(TableSwitchV);
 
     LTableSwitchV(const LBoxAllocation& input, const LDefinition& inputCopy,
                   const LDefinition& floatCopy, const LDefinition& jumpTablePointer,
                   MTableSwitch* ins)
+      : LInstructionHelper(classOpcode)
     {
         setBoxOperand(InputValue, input);
         setTemp(0, inputCopy);
         setTemp(1, floatCopy);
         setTemp(2, jumpTablePointer);
         setMir(ins);
     }
 
@@ -210,26 +215,34 @@ class LTableSwitchV : public LInstructio
     }
 };
 
 class LMulI : public LBinaryMath<0>
 {
   public:
     LIR_HEADER(MulI);
 
+    LMulI()
+      : LBinaryMath(classOpcode)
+    {}
+
     MMul* mir() {
         return mir_->toMul();
     }
 };
 
 class LUDivOrMod : public LBinaryMath<0>
 {
   public:
     LIR_HEADER(UDivOrMod);
 
+    LUDivOrMod()
+      : LBinaryMath(classOpcode)
+    {}
+
     MBinaryArithInstruction* mir() const {
         MOZ_ASSERT(mir_->isDiv() || mir_->isMod());
         return static_cast<MBinaryArithInstruction*>(mir_);
     }
 
     bool canBeDivideByZero() const {
         if (mir_->isMod())
             return mir_->toMod()->canBeDivideByZero();
@@ -254,68 +267,75 @@ namespace details {
 
 // Base class for the int64 and non-int64 variants.
 template<size_t NumDefs>
 class LWasmUnalignedLoadBase : public details::LWasmLoadBase<NumDefs, 2>
 {
   public:
     typedef LWasmLoadBase<NumDefs, 2> Base;
 
-    explicit LWasmUnalignedLoadBase(const LAllocation& ptr, const LDefinition& valueHelper)
-      : Base(ptr, LAllocation())
+    explicit LWasmUnalignedLoadBase(LNode::Opcode opcode, const LAllocation& ptr,
+                                    const LDefinition& valueHelper)
+      : Base(opcode, ptr, LAllocation())
     {
         Base::setTemp(0, LDefinition::BogusTemp());
         Base::setTemp(1, valueHelper);
     }
+
     const LAllocation* ptr() {
         return Base::getOperand(0);
     }
     const LDefinition* ptrCopy() {
         return Base::getTemp(0);
     }
 };
 
 } // namespace details
 
 class LWasmUnalignedLoad : public details::LWasmUnalignedLoadBase<1>
 {
   public:
+    LIR_HEADER(WasmUnalignedLoad);
+
     explicit LWasmUnalignedLoad(const LAllocation& ptr, const LDefinition& valueHelper)
-      : LWasmUnalignedLoadBase(ptr, valueHelper)
+      : LWasmUnalignedLoadBase(classOpcode, ptr, valueHelper)
     {}
-    LIR_HEADER(WasmUnalignedLoad);
 };
 
 class LWasmUnalignedLoadI64 : public details::LWasmUnalignedLoadBase<INT64_PIECES>
 {
   public:
+    LIR_HEADER(WasmUnalignedLoadI64);
+
     explicit LWasmUnalignedLoadI64(const LAllocation& ptr, const LDefinition& valueHelper)
-      : LWasmUnalignedLoadBase(ptr, valueHelper)
+      : LWasmUnalignedLoadBase(classOpcode, ptr, valueHelper)
     {}
-    LIR_HEADER(WasmUnalignedLoadI64);
 };
 
 namespace details {
 
 // Base class for the int64 and non-int64 variants.
 template<size_t NumOps>
 class LWasmUnalignedStoreBase : public LInstructionHelper<0, NumOps, 2>
 {
   public:
     typedef LInstructionHelper<0, NumOps, 2> Base;
 
     static const size_t PtrIndex = 0;
     static const size_t ValueIndex = 1;
 
-    LWasmUnalignedStoreBase(const LAllocation& ptr, const LDefinition& valueHelper)
+    LWasmUnalignedStoreBase(LNode::Opcode opcode, const LAllocation& ptr,
+                            const LDefinition& valueHelper)
+      : Base(opcode)
     {
         Base::setOperand(0, ptr);
         Base::setTemp(0, LDefinition::BogusTemp());
         Base::setTemp(1, valueHelper);
     }
+
     MWasmStore* mir() const {
         return Base::mir_->toWasmStore();
     }
     const LAllocation* ptr() {
         return Base::getOperand(PtrIndex);
     }
     const LDefinition* ptrCopy() {
         return Base::getTemp(0);
@@ -323,48 +343,53 @@ class LWasmUnalignedStoreBase : public L
 };
 
 } // namespace details
 
 class LWasmUnalignedStore : public details::LWasmUnalignedStoreBase<2>
 {
   public:
     LIR_HEADER(WasmUnalignedStore);
+
     LWasmUnalignedStore(const LAllocation& ptr, const LAllocation& value,
                         const LDefinition& valueHelper)
-      : LWasmUnalignedStoreBase(ptr, valueHelper)
+      : LWasmUnalignedStoreBase(classOpcode, ptr, valueHelper)
     {
         setOperand(1, value);
     }
+
     const LAllocation* value() {
         return Base::getOperand(ValueIndex);
     }
 };
 
 class LWasmUnalignedStoreI64 : public details::LWasmUnalignedStoreBase<1 + INT64_PIECES>
 {
   public:
     LIR_HEADER(WasmUnalignedStoreI64);
     LWasmUnalignedStoreI64(const LAllocation& ptr, const LInt64Allocation& value,
                            const LDefinition& valueHelper)
-      : LWasmUnalignedStoreBase(ptr, valueHelper)
+      : LWasmUnalignedStoreBase(classOpcode, ptr, valueHelper)
     {
         setInt64Operand(1, value);
     }
+
     const LInt64Allocation value() {
         return getInt64Operand(ValueIndex);
     }
 };
 
 class LWasmCompareExchangeI64 : public LInstructionHelper<INT64_PIECES, 1 + INT64_PIECES + INT64_PIECES, 0>
 {
   public:
     LIR_HEADER(WasmCompareExchangeI64);
 
-    LWasmCompareExchangeI64(const LAllocation& ptr, const LInt64Allocation& oldValue, const LInt64Allocation& newValue)
+    LWasmCompareExchangeI64(const LAllocation& ptr, const LInt64Allocation& oldValue,
+                            const LInt64Allocation& newValue)
+      : LInstructionHelper(classOpcode)
     {
         setOperand(0, ptr);
         setInt64Operand(1, oldValue);
         setInt64Operand(1 + INT64_PIECES, newValue);
     }
 
     const LAllocation* ptr() {
         return getOperand(0);
@@ -381,16 +406,17 @@ class LWasmCompareExchangeI64 : public L
 };
 
 class LWasmAtomicExchangeI64 : public LInstructionHelper<INT64_PIECES, 1 + INT64_PIECES, 0>
 {
   public:
     LIR_HEADER(WasmAtomicExchangeI64);
 
     LWasmAtomicExchangeI64(const LAllocation& ptr, const LInt64Allocation& value)
+      : LInstructionHelper(classOpcode)
     {
         setOperand(0, ptr);
         setInt64Operand(1, value);
     }
 
     const LAllocation* ptr() {
         return getOperand(0);
     }
@@ -403,28 +429,28 @@ class LWasmAtomicExchangeI64 : public LI
 };
 
 class LWasmAtomicBinopI64 : public LInstructionHelper<INT64_PIECES, 1 + INT64_PIECES, 2>
 {
   public:
     LIR_HEADER(WasmAtomicBinopI64);
 
     LWasmAtomicBinopI64(const LAllocation& ptr, const LInt64Allocation& value)
+      : LInstructionHelper(classOpcode)
     {
         setOperand(0, ptr);
         setInt64Operand(1, value);
     }
 
     const LAllocation* ptr() {
         return getOperand(0);
     }
     const LInt64Allocation value() {
         return getInt64Operand(1);
     }
-
     const MWasmAtomicBinopHeap* mir() const {
         return mir_->toWasmAtomicBinopHeap();
     }
 };
 
 
 } // namespace jit
 } // namespace js
diff --git a/js/src/jit/mips32/LIR-mips32.h b/js/src/jit/mips32/LIR-mips32.h
--- a/js/src/jit/mips32/LIR-mips32.h
+++ b/js/src/jit/mips32/LIR-mips32.h
@@ -13,17 +13,18 @@ namespace jit {
 class LBoxFloatingPoint : public LInstructionHelper<2, 1, 1>
 {
     MIRType type_;
 
   public:
     LIR_HEADER(BoxFloatingPoint);
 
     LBoxFloatingPoint(const LAllocation& in, const LDefinition& temp, MIRType type)
-      : type_(type)
+      : LInstructionHelper(classOpcode),
+        type_(type)
     {
         setOperand(0, in);
         setTemp(0, temp);
     }
 
     MIRType type() const {
         return type_;
     }
@@ -32,16 +33,20 @@ class LBoxFloatingPoint : public LInstru
     }
 };
 
 class LUnbox : public LInstructionHelper<1, 2, 0>
 {
   public:
     LIR_HEADER(Unbox);
 
+    LUnbox()
+      : LInstructionHelper(classOpcode)
+    {}
+
     MUnbox* mir() const {
         return mir_->toUnbox();
     }
     const LAllocation* payload() {
         return getOperand(0);
     }
     const LAllocation* type() {
         return getOperand(1);
@@ -56,25 +61,25 @@ class LUnboxFloatingPoint : public LInst
     MIRType type_;
 
   public:
     LIR_HEADER(UnboxFloatingPoint);
 
     static const size_t Input = 0;
 
     LUnboxFloatingPoint(const LBoxAllocation& input, MIRType type)
-      : type_(type)
+      : LInstructionHelper(classOpcode),
+        type_(type)
     {
         setBoxOperand(Input, input);
     }
 
     MUnbox* mir() const {
         return mir_->toUnbox();
     }
-
     MIRType type() const {
         return type_;
     }
     const char* extraName() const {
         return StringFromMIRType(type_);
     }
 };
 
@@ -82,24 +87,27 @@ class LDivOrModI64 : public LCallInstruc
 {
   public:
     LIR_HEADER(DivOrModI64)
 
     static const size_t Lhs = 0;
     static const size_t Rhs = INT64_PIECES;
 
     LDivOrModI64(const LInt64Allocation& lhs, const LInt64Allocation& rhs)
+      : LCallInstructionHelper(classOpcode)
     {
         setInt64Operand(Lhs, lhs);
         setInt64Operand(Rhs, rhs);
     }
+
     MBinaryArithInstruction* mir() const {
         MOZ_ASSERT(mir_->isDiv() || mir_->isMod());
         return static_cast<MBinaryArithInstruction*>(mir_);
     }
+
     bool canBeDivideByZero() const {
         if (mir_->isMod())
             return mir_->toMod()->canBeDivideByZero();
         return mir_->toDiv()->canBeDivideByZero();
     }
     bool canBeNegativeOverflow() const {
         if (mir_->isMod())
             return mir_->toMod()->canBeNegativeDividend();
@@ -117,24 +125,26 @@ class LUDivOrModI64 : public LCallInstru
 {
   public:
     LIR_HEADER(UDivOrModI64)
 
     static const size_t Lhs = 0;
     static const size_t Rhs = INT64_PIECES;
 
     LUDivOrModI64(const LInt64Allocation& lhs, const LInt64Allocation& rhs)
+      : LCallInstructionHelper(classOpcode)
     {
         setInt64Operand(Lhs, lhs);
         setInt64Operand(Rhs, rhs);
     }
     MBinaryArithInstruction* mir() const {
         MOZ_ASSERT(mir_->isDiv() || mir_->isMod());
         return static_cast<MBinaryArithInstruction*>(mir_);
     }
+
     bool canBeDivideByZero() const {
         if (mir_->isMod())
             return mir_->toMod()->canBeDivideByZero();
         return mir_->toDiv()->canBeDivideByZero();
     }
     bool canBeNegativeOverflow() const {
         if (mir_->isMod())
             return mir_->toMod()->canBeNegativeDividend();
@@ -149,45 +159,49 @@ class LUDivOrModI64 : public LCallInstru
 };
 
 class LWasmTruncateToInt64 : public LCallInstructionHelper<INT64_PIECES, 1, 0>
 {
   public:
     LIR_HEADER(WasmTruncateToInt64);
 
     explicit LWasmTruncateToInt64(const LAllocation& in)
+      : LCallInstructionHelper(classOpcode)
     {
         setOperand(0, in);
     }
 
     MWasmTruncateToInt64* mir() const {
         return mir_->toWasmTruncateToInt64();
     }
 };
 
 class LInt64ToFloatingPoint : public LCallInstructionHelper<1, INT64_PIECES, 0>
 {
   public:
     LIR_HEADER(Int64ToFloatingPoint);
 
-    explicit LInt64ToFloatingPoint(const LInt64Allocation& in) {
+    explicit LInt64ToFloatingPoint(const LInt64Allocation& in)
+      : LCallInstructionHelper(classOpcode)
+    {
         setInt64Operand(0, in);
     }
 
     MInt64ToFloatingPoint* mir() const {
         return mir_->toInt64ToFloatingPoint();
     }
 };
 
 class LWasmAtomicLoadI64 : public LInstructionHelper<INT64_PIECES, 1, 0>
 {
   public:
     LIR_HEADER(WasmAtomicLoadI64);
 
     LWasmAtomicLoadI64(const LAllocation& ptr)
+      : LInstructionHelper(classOpcode)
     {
         setOperand(0, ptr);
     }
 
     const LAllocation* ptr() {
         return getOperand(0);
     }
     const MWasmLoad* mir() const {
@@ -195,17 +209,19 @@ class LWasmAtomicLoadI64 : public LInstr
     }
 };
 
 class LWasmAtomicStoreI64 : public LInstructionHelper<0, 1 + INT64_PIECES, 1>
 {
   public:
     LIR_HEADER(WasmAtomicStoreI64);
 
-    LWasmAtomicStoreI64(const LAllocation& ptr, const LInt64Allocation& value, const LDefinition& tmp)
+    LWasmAtomicStoreI64(const LAllocation& ptr, const LInt64Allocation& value,
+                        const LDefinition& tmp)
+      : LInstructionHelper(classOpcode)
     {
         setOperand(0, ptr);
         setInt64Operand(1, value);
         setTemp(0, tmp);
     }
 
     const LAllocation* ptr() {
         return getOperand(0);
diff --git a/js/src/jit/mips64/LIR-mips64.h b/js/src/jit/mips64/LIR-mips64.h
--- a/js/src/jit/mips64/LIR-mips64.h
+++ b/js/src/jit/mips64/LIR-mips64.h
@@ -7,20 +7,29 @@
 #ifndef jit_mips64_LIR_mips64_h
 #define jit_mips64_LIR_mips64_h
 
 namespace js {
 namespace jit {
 
 class LUnbox : public LInstructionHelper<1, 1, 0>
 {
+  protected:
+    LUnbox(LNode::Opcode opcode, const LAllocation& input)
+      : LInstructionHelper(opcode)
+    {
+        setOperand(0, input);
+    }
+
   public:
     LIR_HEADER(Unbox);
 
-    explicit LUnbox(const LAllocation& input) {
+    explicit LUnbox(const LAllocation& input)
+      : LInstructionHelper(classOpcode)
+    {
         setOperand(0, input);
     }
 
     static const size_t Input = 0;
 
     MUnbox* mir() const {
         return mir_->toUnbox();
     }
@@ -32,44 +41,46 @@ class LUnbox : public LInstructionHelper
 class LUnboxFloatingPoint : public LUnbox
 {
     MIRType type_;
 
   public:
     LIR_HEADER(UnboxFloatingPoint);
 
     LUnboxFloatingPoint(const LAllocation& input, MIRType type)
-      : LUnbox(input),
+      : LUnbox(classOpcode, input),
         type_(type)
     { }
 
     MIRType type() const {
         return type_;
     }
 };
 
 class LDivOrModI64 : public LBinaryMath<1>
 {
   public:
     LIR_HEADER(DivOrModI64)
 
-    LDivOrModI64(const LAllocation& lhs, const LAllocation& rhs, const LDefinition& temp) {
+    LDivOrModI64(const LAllocation& lhs, const LAllocation& rhs, const LDefinition& temp)
+      : LBinaryMath(classOpcode)
+    {
         setOperand(0, lhs);
         setOperand(1, rhs);
         setTemp(0, temp);
     }
 
     const LDefinition* remainder() {
         return getTemp(0);
     }
-
     MBinaryArithInstruction* mir() const {
         MOZ_ASSERT(mir_->isDiv() || mir_->isMod());
         return static_cast<MBinaryArithInstruction*>(mir_);
     }
+
     bool canBeDivideByZero() const {
         if (mir_->isMod())
             return mir_->toMod()->canBeDivideByZero();
         return mir_->toDiv()->canBeDivideByZero();
     }
     bool canBeNegativeOverflow() const {
         if (mir_->isMod())
             return mir_->toMod()->canBeNegativeDividend();
@@ -83,35 +94,35 @@ class LDivOrModI64 : public LBinaryMath<
     }
 };
 
 class LUDivOrModI64 : public LBinaryMath<1>
 {
   public:
     LIR_HEADER(UDivOrModI64);
 
-    LUDivOrModI64(const LAllocation& lhs, const LAllocation& rhs, const LDefinition& temp) {
+    LUDivOrModI64(const LAllocation& lhs, const LAllocation& rhs, const LDefinition& temp)
+      : LBinaryMath(classOpcode)
+    {
         setOperand(0, lhs);
         setOperand(1, rhs);
         setTemp(0, temp);
     }
 
     const LDefinition* remainder() {
         return getTemp(0);
     }
-
     const char* extraName() const {
         return mir()->isTruncated() ? "Truncated" : nullptr;
     }
 
     MBinaryArithInstruction* mir() const {
         MOZ_ASSERT(mir_->isDiv() || mir_->isMod());
         return static_cast<MBinaryArithInstruction*>(mir_);
     }
-
     bool canBeDivideByZero() const {
         if (mir_->isMod())
             return mir_->toMod()->canBeDivideByZero();
         return mir_->toDiv()->canBeDivideByZero();
     }
     wasm::BytecodeOffset bytecodeOffset() const {
         MOZ_ASSERT(mir_->isDiv() || mir_->isMod());
         if (mir_->isMod())
@@ -120,31 +131,35 @@ class LUDivOrModI64 : public LBinaryMath
     }
 };
 
 class LWasmTruncateToInt64 : public LInstructionHelper<1, 1, 0>
 {
   public:
     LIR_HEADER(WasmTruncateToInt64);
 
-    explicit LWasmTruncateToInt64(const LAllocation& in) {
+    explicit LWasmTruncateToInt64(const LAllocation& in)
+      : LInstructionHelper(classOpcode)
+    {
         setOperand(0, in);
     }
 
     MWasmTruncateToInt64* mir() const {
         return mir_->toWasmTruncateToInt64();
     }
 };
 
 class LInt64ToFloatingPoint : public LInstructionHelper<1, 1, 0>
 {
   public:
     LIR_HEADER(Int64ToFloatingPoint);
 
-    explicit LInt64ToFloatingPoint(const LInt64Allocation& in) {
+    explicit LInt64ToFloatingPoint(const LInt64Allocation& in)
+      : LInstructionHelper(classOpcode)
+    {
         setInt64Operand(0, in);
     }
 
     MInt64ToFloatingPoint* mir() const {
         return mir_->toInt64ToFloatingPoint();
     }
 };
 

