# HG changeset patch
# User Cosmin Sabou <csabou@mozilla.com>
# Date 1538587621 -10800
#      Wed Oct 03 20:27:01 2018 +0300
# Node ID 7a00217bd2fac77aaad3d3a98730594b362ede5f
# Parent  3530790e23d18b6f8f73471e367a942f201dd452
Backed out changeset 6d25c44c39ea (bug 1305743) on request from jgraham for causing issues with mozinfo.json. a=backout

diff --git a/python/mozbuild/mozbuild/frontend/emitter.py b/python/mozbuild/mozbuild/frontend/emitter.py
--- a/python/mozbuild/mozbuild/frontend/emitter.py
+++ b/python/mozbuild/mozbuild/frontend/emitter.py
@@ -105,16 +105,28 @@ class TreeMetadataEmitter(LoggingMixin):
     reader.BuildReader and converts it into the classes defined in the data
     module.
     """
 
     def __init__(self, config):
         self.populate_logger()
 
         self.config = config
+
+        mozinfo.find_and_update_from_json(config.topobjdir)
+
+        # Python 2.6 doesn't allow unicode keys to be used for keyword
+        # arguments. This gross hack works around the problem until we
+        # rid ourselves of 2.6.
+        self.info = {}
+        for k, v in mozinfo.info.items():
+            if isinstance(k, unicode):
+                k = k.encode('ascii')
+            self.info[k] = v
+
         self._libs = OrderedDefaultDict(list)
         self._binaries = OrderedDict()
         self._compile_dirs = set()
         self._host_compile_dirs = set()
         self._asm_compile_dirs = set()
         self._compile_flags = dict()
         self._compile_as_flags = dict()
         self._linkage = []
diff --git a/testing/mozbase/mozinfo/mozinfo/mozinfo.py b/testing/mozbase/mozinfo/mozinfo/mozinfo.py
--- a/testing/mozbase/mozinfo/mozinfo/mozinfo.py
+++ b/testing/mozbase/mozinfo/mozinfo/mozinfo.py
@@ -178,24 +178,23 @@ def sanitize(info):
         else:
             info["processor"] = "x86"
             info["bits"] = 32
 
 # method for updating information
 
 
 def update(new_info):
-    """Adds information from json file to the global symbol table.
-
-    Given the parameter new_info, this method will look for the file.
-    If found, the file is read into a buffer and assumed to be json.
+    """
+    Update the info.
 
     :param new_info: Either a dict containing the new info or a path/url
                      to a json file containing the new info.
     """
+
     PY3 = sys.version_info[0] == 3
     if PY3:
         string_types = str,
     else:
         string_types = basestring,
     if isinstance(new_info, string_types):
         # lazy import
         import mozfile
@@ -211,67 +210,49 @@ def update(new_info):
     # convenience data for os access
     for os_name in choices['os']:
         globals()['is' + os_name.title()] = info['os'] == os_name
     # unix is special
     if isLinux or isBsd:  # noqa
         globals()['isUnix'] = True
 
 
-def find_and_update_from_json(*dirs, **kwargs):
-    """Find a mozinfo.json file, load it, and update global symbol table.
-
-    This method will first check the relevant objdir directory for the
-    necessary mozinfo.json file, if the current script is being run from a
-    Mozilla objdir.
-
-    If the objdir directory did not supply the necessary data, this method
-    will then look for the required mozinfo.json file from the provided
-    tuple of directories.
+def find_and_update_from_json(*dirs):
+    """
+    Find a mozinfo.json file, load it, and update the info with the
+    contents.
 
-    If file is found, the global symbols table is updated via a helper method.
-
-    If no valid files are found, an exception is raised.
+    :param dirs: Directories in which to look for the file. They will be
+                 searched after first looking in the root of the objdir
+                 if the current script is being run from a Mozilla objdir.
 
-    :param tuple dirs: Directories in which to look for the file.
-    :param dict kwargs: optional values:
-                        raise_exception: if this value is provided, the default
-                                         behavior of raising an exception is
-                                         overridden.
-    :returns: EnvironmentError: default behavior.
-              None: if exception raising is suppressed.
-              json_path: string representation of path.
+    Returns the full path to mozinfo.json if it was found, or None otherwise.
     """
     # First, see if we're in an objdir
     try:
         from mozbuild.base import MozbuildObject, BuildEnvironmentNotFoundException
         from mozbuild.mozconfig import MozconfigFindException
         build = MozbuildObject.from_environment()
         json_path = _os.path.join(build.topobjdir, "mozinfo.json")
         if _os.path.isfile(json_path):
             update(json_path)
             return json_path
     except ImportError:
         pass
     except (BuildEnvironmentNotFoundException, MozconfigFindException):
         pass
 
-    raise_exception = kwargs.get('raise_exception', True)
-
     for d in dirs:
         d = _os.path.abspath(d)
         json_path = _os.path.join(d, "mozinfo.json")
         if _os.path.isfile(json_path):
             update(json_path)
             return json_path
 
-    if raise_exception:
-        raise EnvironmentError('{}: could not find any mozinfo.json.'.format(__name__))
-    else:
-        return None
+    return None
 
 
 def output_to_file(path):
     import json
     with open(path, 'w') as f:
         f.write(json.dumps(info))
 
 
diff --git a/testing/mozbase/mozinfo/tests/test.py b/testing/mozbase/mozinfo/tests/test.py
--- a/testing/mozbase/mozinfo/tests/test.py
+++ b/testing/mozbase/mozinfo/tests/test.py
@@ -70,67 +70,24 @@ class TestMozinfo(unittest.TestCase):
         """Test that mozinfo.find_and_update_from_json can
         find mozinfo.json in a directory passed to it."""
         j = os.path.join(self.tempdir, "mozinfo.json")
         with open(j, "w") as f:
             f.write(json.dumps({"foo": "abcdefg"}))
         self.assertEqual(mozinfo.find_and_update_from_json(self.tempdir), j)
         self.assertEqual(mozinfo.info["foo"], "abcdefg")
 
-    def test_find_and_update_file_no_argument(self):
-        """Test that mozinfo.find_and_update_from_json can
-        handle not being given any arguments.
-        """
-        with self.assertRaises(EnvironmentError):
-            self.assertEqual(mozinfo.find_and_update_from_json())
-
     def test_find_and_update_file_invalid_json(self):
         """Test that mozinfo.find_and_update_from_json can
         handle invalid JSON"""
         j = os.path.join(self.tempdir, "mozinfo.json")
         with open(j, 'w') as f:
             f.write('invalid{"json":')
         self.assertRaises(ValueError, mozinfo.find_and_update_from_json, self.tempdir)
 
-    def test_find_and_update_file_raise_exception(self):
-        """Test that mozinfo.find_and_update_from_json raises
-        an IOError exception if a True boolean value is
-        provided as the only argument.
-        """
-        with self.assertRaises(EnvironmentError):
-            mozinfo.find_and_update_from_json(raise_exception=True)
-
-    def test_find_and_update_file_raise_exception_multiple_arguments(self):
-        """Test that mozinfo.find_and_update_from_json raises
-        an IOError exception if a True boolean value is
-        provided as last positional argument.
-        """
-        with self.assertRaises(EnvironmentError):
-            mozinfo.find_and_update_from_json(self.tempdir, raise_exception=True)
-
-    def test_find_and_update_file_suppress_exception(self):
-        """Test that mozinfo.find_and_update_from_json suppresses
-        an IOError exception if a False boolean value is
-        provided as the only argument.
-        """
-        self.assertEqual(mozinfo.find_and_update_from_json(
-            raise_exception=False), None)
-
-    def test_find_and_update_file_suppress_exception_multiple_arguments(self):
-        """Test that mozinfo.find_and_update_from_json suppresses
-        an IOError exception if a False boolean value is
-        provided as last positional argument.
-        """
-        j = os.path.join(self.tempdir, "mozinfo.json")
-        with open(j, "w") as f:
-            f.write(json.dumps({"foo": "abcdefg"}))
-        self.assertEqual(mozinfo.find_and_update_from_json(
-            self.tempdir, raise_exception=False), j)
-        self.assertEqual(mozinfo.info["foo"], "abcdefg")
-
     def test_find_and_update_file_mozbuild(self):
         """Test that mozinfo.find_and_update_from_json can
         find mozinfo.json using the mozbuild module."""
         j = os.path.join(self.tempdir, "mozinfo.json")
         with open(j, "w") as f:
             f.write(json.dumps({"foo": "123456"}))
         m = mock.MagicMock()
         # Mock the value of MozbuildObject.from_environment().topobjdir.
