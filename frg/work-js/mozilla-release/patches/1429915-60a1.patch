# HG changeset patch
# User Daniel Holbert <dholbert@cs.stanford.edu>
# Date 1517348842 18000
# Node ID 775a2dbb53f6f92cd35bc4528783119caae43289
# Parent  0c35fe504bfc506746c0de416c4894ada56f153e
Bug 1429915: Use RefPtr instead of NS_ADDREF to set refcounted outparams in SVGTextFrame. r=jwatt

MozReview-Commit-ID: 6eBacKkAitR

diff --git a/layout/svg/SVGTextFrame.cpp b/layout/svg/SVGTextFrame.cpp
--- a/layout/svg/SVGTextFrame.cpp
+++ b/layout/svg/SVGTextFrame.cpp
@@ -4338,18 +4338,19 @@ SVGTextFrame::GetStartPositionOfChar(nsI
   if (!it.AdvanceToSubtree() ||
       !it.Next(aCharNum)) {
     return NS_ERROR_DOM_INDEX_SIZE_ERR;
   }
 
   // We need to return the start position of the whole glyph.
   uint32_t startIndex = it.GlyphStartTextElementCharIndex();
 
-  NS_ADDREF(*aResult =
-    new DOMSVGPoint(ToPoint(mPositions[startIndex].mPosition)));
+  RefPtr<DOMSVGPoint> point =
+    new DOMSVGPoint(ToPoint(mPositions[startIndex].mPosition));
+  point.forget(aResult);
   return NS_OK;
 }
 
 /**
  * Implements the SVG DOM GetEndPositionOfChar method for the specified
  * text content element.
  */
 nsresult
@@ -4383,17 +4384,18 @@ SVGTextFrame::GetEndPositionOfChar(nsICo
 
   // The end position is the start position plus the advance in the direction
   // of the glyph's rotation.
   Matrix m =
     Matrix::Rotation(mPositions[startIndex].mAngle) *
     Matrix::Translation(ToPoint(mPositions[startIndex].mPosition));
   Point p = m.TransformPoint(Point(advance / mFontSizeScaleFactor, 0));
 
-  NS_ADDREF(*aResult = new DOMSVGPoint(p));
+  RefPtr<DOMSVGPoint> point = new DOMSVGPoint(p);
+  point.forget(aResult);
   return NS_OK;
 }
 
 /**
  * Implements the SVG DOM GetExtentOfChar method for the specified
  * text content element.
  */
 nsresult
@@ -4450,17 +4452,18 @@ SVGTextFrame::GetExtentOfChar(nsIContent
       gfxRect(x, -presContext->AppUnitsToGfxUnits(ascent) * cssPxPerDevPx,
               advance,
               presContext->AppUnitsToGfxUnits(ascent + descent) * cssPxPerDevPx);
   }
 
   // Transform the glyph's rect into user space.
   gfxRect r = m.TransformBounds(glyphRect);
 
-  NS_ADDREF(*aResult = new dom::SVGRect(aContent, r.x, r.y, r.width, r.height));
+  RefPtr<dom::SVGRect> rect = new dom::SVGRect(aContent, r.x, r.y, r.width, r.height);
+  rect.forget(aResult);
   return NS_OK;
 }
 
 /**
  * Implements the SVG DOM GetRotationOfChar method for the specified
  * text content element.
  */
 nsresult
