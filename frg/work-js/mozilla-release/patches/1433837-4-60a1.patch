# HG changeset patch
# User Ted Campbell <tcampbell@mozilla.com>
# Date 1517206800 -7200
#      Mon Jan 29 08:20:00 2018 +0200
# Node ID b1c8ba93a6c2f5e9179c8ff252ba7e049558e408
# Parent  02d1cc075ba1f31d70255bbc939c43d47fb5a20d
Bug 1433837 - Cleanup JSObject slots_ initialization r=jandem
js::Allocate<JSObject> now only sets slots_ if nDynamicSlots is
non-zero. This avoids spurious writes for other types and is now
consistent with JIT code.

MozReview-Commit-ID: 3spPMFj7Fxz

diff --git a/js/src/gc/Allocator.cpp b/js/src/gc/Allocator.cpp
--- a/js/src/gc/Allocator.cpp
+++ b/js/src/gc/Allocator.cpp
@@ -114,20 +114,22 @@ GCRuntime::tryNewTenuredObject(JSContext
                 ReportOutOfMemory(cx);
             return nullptr;
         }
         Debug_SetSlotRangeToCrashOnTouch(slots, nDynamicSlots);
     }
 
     JSObject* obj = tryNewTenuredThing<JSObject, allowGC>(cx, kind, thingSize);
 
-    if (obj)
-        obj->setInitialSlotsMaybeNonNative(slots);
-    else
+    if (obj) {
+        if (nDynamicSlots)
+            static_cast<NativeObject*>(obj)->initSlots(slots);
+    } else {
         js_free(slots);
+    }
 
     return obj;
 }
 
 template <typename T, AllowGC allowGC /* = CanGC */>
 T*
 js::Allocate(JSContext* cx)
 {
diff --git a/js/src/gc/Allocator.h b/js/src/gc/Allocator.h
--- a/js/src/gc/Allocator.h
+++ b/js/src/gc/Allocator.h
@@ -16,17 +16,18 @@ struct Class;
 
 // Allocate a new GC thing. After a successful allocation the caller must
 // fully initialize the thing before calling any function that can potentially
 // trigger GC. This will ensure that GC tracing never sees junk values stored
 // in the partially initialized thing.
 //
 // Note that JSObject allocation must use the longer signature below that
 // includes slot, heap, and finalizer information in support of various
-// object-specific optimizations.
+// object-specific optimizations. If dynamic slots are requested they will be
+// allocated and the pointer stored directly in |NativeObject::slots_|.
 template <typename T, AllowGC allowGC = CanGC>
 T*
 Allocate(JSContext* cx);
 
 template <typename, AllowGC allowGC = CanGC>
 JSObject*
 Allocate(JSContext* cx, gc::AllocKind kind, size_t nDynamicSlots, gc::InitialHeap heap,
          const Class* clasp);
diff --git a/js/src/gc/Nursery.cpp b/js/src/gc/Nursery.cpp
--- a/js/src/gc/Nursery.cpp
+++ b/js/src/gc/Nursery.cpp
@@ -290,18 +290,21 @@ js::Nursery::allocateObject(JSContext* c
             /*
              * It is safe to leave the allocated object uninitialized, since we
              * do not visit unallocated things in the nursery.
              */
             return nullptr;
         }
     }
 
-    /* Always initialize the slots field to match the JIT behavior. */
-    obj->setInitialSlotsMaybeNonNative(slots);
+    /* Store slots pointer directly in new object. If no dynamic slots were
+     * requested, caller must initialize slots_ field itself as needed. We
+     * don't know if the caller was a native object or not. */
+    if (nDynamicSlots)
+        static_cast<NativeObject*>(obj)->initSlots(slots);
 
     TraceNurseryAlloc(obj, size);
     return obj;
 }
 
 void*
 js::Nursery::allocate(size_t size)
 {
diff --git a/js/src/jit/MacroAssembler.cpp b/js/src/jit/MacroAssembler.cpp
--- a/js/src/jit/MacroAssembler.cpp
+++ b/js/src/jit/MacroAssembler.cpp
@@ -990,16 +990,18 @@ MacroAssembler::allocateObject(Register 
     checkAllocatorState(fail);
 
     if (shouldNurseryAllocate(allocKind, initialHeap))
         return nurseryAllocate(result, temp, allocKind, nDynamicSlots, initialHeap, fail);
 
     if (!nDynamicSlots)
         return freeListAllocate(result, temp, allocKind, fail);
 
+    // Only NativeObject can have nDynamicSlots > 0 and reach here.
+
     callMallocStub(nDynamicSlots * sizeof(GCPtrValue), temp, fail);
 
     Label failAlloc;
     Label success;
 
     push(temp);
     freeListAllocate(result, temp, allocKind, &failAlloc);
 
diff --git a/js/src/jsobj.h b/js/src/jsobj.h
--- a/js/src/jsobj.h
+++ b/js/src/jsobj.h
@@ -149,22 +149,16 @@ class JSObject : public js::gc::Cell
     }
 
     JSCompartment* compartment() const { return group_->compartment(); }
     JSCompartment* maybeCompartment() const { return compartment(); }
 
     inline js::Shape* maybeShape() const;
     inline js::Shape* ensureShape(JSContext* cx);
 
-    // Set the initial slots and elements of an object. These pointers are only
-    // valid for native objects, but during initialization are set for all
-    // objects. For non-native objects, these must not be dynamically allocated
-    // pointers which leak when the non-native object finishes initialization.
-    inline void setInitialSlotsMaybeNonNative(js::HeapSlot* slots);
-
     enum GenerateShape {
         GENERATE_NONE,
         GENERATE_SHAPE
     };
 
     static bool setFlags(JSContext* cx, JS::HandleObject obj, js::BaseShape::Flag flags,
                          GenerateShape generateShape = GENERATE_NONE);
     inline bool hasAllFlags(js::BaseShape::Flag flags) const;
diff --git a/js/src/jsobjinlines.h b/js/src/jsobjinlines.h
--- a/js/src/jsobjinlines.h
+++ b/js/src/jsobjinlines.h
@@ -392,22 +392,16 @@ SetNewObjectMetadata(JSContext* cx, T* o
         }
     }
 
     return obj;
 }
 
 } // namespace js
 
-inline void
-JSObject::setInitialSlotsMaybeNonNative(js::HeapSlot* slots)
-{
-    static_cast<js::NativeObject*>(this)->slots_ = slots;
-}
-
 inline js::GlobalObject&
 JSObject::global() const
 {
     /*
      * The global is read-barriered so that it is kept live by access through
      * the JSCompartment. When accessed through a JSObject, however, the global
      * will be already be kept live by the black JSObject's parent pointer, so
      * does not need to be read-barriered.
diff --git a/js/src/vm/ArrayObject-inl.h b/js/src/vm/ArrayObject-inl.h
--- a/js/src/vm/ArrayObject-inl.h
+++ b/js/src/vm/ArrayObject-inl.h
@@ -52,17 +52,19 @@ ArrayObject::createArrayInternal(JSConte
     size_t nDynamicSlots = dynamicSlotsCount(0, shape->slotSpan(), clasp);
     JSObject* obj = js::Allocate<JSObject>(cx, kind, nDynamicSlots, heap, clasp);
     if (!obj)
         return nullptr;
 
     ArrayObject* aobj = static_cast<ArrayObject*>(obj);
     aobj->initGroup(group);
     aobj->initShape(shape);
-    // NOTE: Slots are created and assigned internally by Allocate<JSObject>.
+    // NOTE: Dynamic slots are created internally by Allocate<JSObject>.
+    if (!nDynamicSlots)
+        aobj->initSlots(nullptr);
 
     MOZ_ASSERT(clasp->shouldDelayMetadataBuilder());
     cx->compartment()->setObjectPendingMetadata(cx, aobj);
 
     return aobj;
 }
 
 /* static */ inline ArrayObject*
diff --git a/js/src/vm/NativeObject-inl.h b/js/src/vm/NativeObject-inl.h
--- a/js/src/vm/NativeObject-inl.h
+++ b/js/src/vm/NativeObject-inl.h
@@ -531,17 +531,19 @@ NativeObject::create(JSContext* cx, js::
 
     JSObject* obj = js::Allocate<JSObject>(cx, kind, nDynamicSlots, heap, clasp);
     if (!obj)
         return cx->alreadyReportedOOM();
 
     NativeObject* nobj = static_cast<NativeObject*>(obj);
     nobj->initGroup(group);
     nobj->initShape(shape);
-    // NOTE: Slots are created and assigned internally by Allocate<JSObject>.
+    // NOTE: Dynamic slots are created internally by Allocate<JSObject>.
+    if (!nDynamicSlots)
+        nobj->initSlots(nullptr);
     nobj->setEmptyElements();
 
     if (clasp->hasPrivate())
         nobj->initPrivate(nullptr);
 
     if (size_t span = shape->slotSpan())
         nobj->initializeSlotRange(0, span);
 
diff --git a/js/src/vm/NativeObject.h b/js/src/vm/NativeObject.h
--- a/js/src/vm/NativeObject.h
+++ b/js/src/vm/NativeObject.h
@@ -675,16 +675,22 @@ class NativeObject : public ShapedObject
      */
     static const uint32_t SLOT_CAPACITY_MIN = 8;
 
     HeapSlot* fixedSlots() const {
         return reinterpret_cast<HeapSlot*>(uintptr_t(this) + sizeof(NativeObject));
     }
 
   public:
+
+    /* Object allocation may directly initialize slots so this is public. */
+    void initSlots(HeapSlot* slots) {
+        slots_ = slots;
+    }
+
     static MOZ_MUST_USE bool generateOwnShape(JSContext* cx, HandleNativeObject obj,
                                               Shape* newShape = nullptr)
     {
         return replaceWithNewEquivalentShape(cx, obj, obj->lastProperty(), newShape);
     }
 
     static MOZ_MUST_USE bool reshapeForShadowedProp(JSContext* cx, HandleNativeObject obj);
     static MOZ_MUST_USE bool reshapeForProtoMutation(JSContext* cx, HandleNativeObject obj);
