# HG changeset patch
# User Iain Ireland <iireland@mozilla.com>
# Date 1589381598 0
# Node ID 5dbad591adfa265fa56d56cd7c6388ff6eaacd51
# Parent  ccaf9a97401087c8aa0abfc4732d3baf29fca823
Bug 1636495: Add JS::CheckRegExpSyntax r=evilpie

To make sure that `<input>` elements with `pattern` attributes update their validation state (`:invalid`) properly, nsContentUtils::IsPatternMatching needs to be able to distinguish between parsing errors caused by an invalid pattern, vs parsing errors caused by OOM/overrecursion.

This patch also fixes up the places inside the new regexp engine where we can throw over-recursed to make sure that we set the right flag on the context, then fixes regexp/huge-01.js (and the binast variants) to accept a different error message.

Differential Revision: https://phabricator.services.mozilla.com/D74499

diff --git a/js/public/RegExp.h b/js/public/RegExp.h
--- a/js/public/RegExp.h
+++ b/js/public/RegExp.h
@@ -85,11 +85,22 @@ extern JS_PUBLIC_API(RegExpFlags)
   GetRegExpFlags(JSContext* cx, Handle<JSObject*> obj);
 
 /*
  * Return the source text for a RegExp object (or a wrapper around one), or null on failure.
  */
 extern JS_PUBLIC_API(JSString*)
   GetRegExpSource(JSContext* cx, Handle<JSObject*> obj);
 
+/**
+ * Check whether the given source is a valid regexp. If the regexp parses
+ * successfully, returns true and sets |error| to undefined. If the regexp
+ * has a syntax error, returns true, sets |error| to that error object, and
+ * clears the exception. Returns false on OOM or over-recursion.
+ */
+extern JS_PUBLIC_API(bool) CheckRegExpSyntax(JSContext* cx,
+                                             const char16_t* chars,
+                                             size_t length, RegExpFlags flags,
+                                             MutableHandle<Value> error);
+
 } // namespace JS
 
 #endif // js_RegExp_h
diff --git a/js/src/jit-test/tests/regexp/huge-01.js b/js/src/jit-test/tests/regexp/huge-01.js
--- a/js/src/jit-test/tests/regexp/huge-01.js
+++ b/js/src/jit-test/tests/regexp/huge-01.js
@@ -5,15 +5,17 @@ function g(N, p) {
 
     try {
         var re = new RegExp(str);
         re.exec(prefix + "A");
     } catch(e) {
         // 1. Regexp too big is raised by the lack of the 64k virtual registers
         // reserved for the regexp evaluation.
         // 2. The stack overflow can occur during the analysis of the regexp
-        assertEq(e.message.includes("regexp too big") || e.message.includes("Stack overflow"), true);
+        assertEq(e.message.includes("regexp too big") ||
+                 e.message.includes("Stack overflow") ||
+                 e.message.includes("too much recursion"), true);
     }
 }
 
 var prefix = "/(?=k)ok/";
 for (var i = 0; i < 18; i++)
     g(Math.pow(2, i), prefix)
diff --git a/js/src/new-regexp/RegExpAPI.cpp b/js/src/new-regexp/RegExpAPI.cpp
--- a/js/src/new-regexp/RegExpAPI.cpp
+++ b/js/src/new-regexp/RegExpAPI.cpp
@@ -144,16 +144,21 @@ static size_t ComputeColumn(const char16
 // We never call it with additional arguments.
 template <typename CharT>
 static void ReportSyntaxError(TokenStream& ts,
                               RegExpCompileData& result, CharT* start,
                               size_t length, ...) {
   gc::AutoSuppressGC suppressGC(ts.context());
   uint32_t errorNumber = ErrorNumber(result.error);
 
+  if (errorNumber == JSMSG_OVER_RECURSED) {
+    ReportOverRecursed(ts.context());
+    return;
+  }
+
   uint32_t offset = std::max(result.error_pos, 0);
   MOZ_ASSERT(offset <= length);
 
   ErrorMetadata err;
 
   // Ordinarily this indicates whether line-of-context information can be
   // added, but we entirely ignore that here because we create a
   // a line of context based on the expression source.
@@ -423,17 +428,17 @@ bool CompilePattern(JSContext* cx, Mutab
 
   bool isLatin1 = input->hasLatin1Chars();
 
   SampleCharacters(input, compiler);
   data.node = WrapBody(re, compiler, data, &zone, isLatin1);
   data.error = AnalyzeRegExp(cx->isolate, isLatin1, data.node);
   if (data.error != RegExpError::kNone) {
     MOZ_ASSERT(data.error == RegExpError::kAnalysisStackOverflow);
-    JS_ReportErrorASCII(cx, "Stack overflow");
+    ReportOverRecursed(cx);
     return false;
   }
 
   bool useNativeCode = re->markedForTierUp(cx);
 
   MOZ_ASSERT_IF(useNativeCode, IsNativeRegExpEnabled(cx));
 
   Maybe<jit::JitContext> jctx;
diff --git a/js/src/vm/RegExpObject.cpp b/js/src/vm/RegExpObject.cpp
--- a/js/src/vm/RegExpObject.cpp
+++ b/js/src/vm/RegExpObject.cpp
@@ -1827,10 +1827,38 @@ JS::GetRegExpSource(JSContext* cx, Handl
     CHECK_THREAD(cx);
 
     RegExpShared* shared = RegExpToShared(cx, obj);
     if (!shared)
         return nullptr;
     return shared->getSource();
 }
 
-/************************************************************************/
+JS_PUBLIC_API(bool) JS::CheckRegExpSyntax(JSContext* cx, const char16_t* chars,
+                                         size_t length, RegExpFlags flags,
+                                         MutableHandleValue error) {
+  AssertHeapIsIdle();
+  CHECK_THREAD(cx);
+
+  CompileOptions options(cx);
+  frontend::TokenStream dummyTokenStream(cx, options, nullptr, 0, nullptr);
 
+  mozilla::Range<const char16_t> source(chars, length);
+#ifdef JS_NEW_REGEXP
+  bool success =
+      irregexp::CheckPatternSyntax(cx, dummyTokenStream, source, flags);
+#else
+  bool success = irregexp::ParsePatternSyntax(
+      dummyTokenStream, cx->tempLifoAlloc(), source, flags.unicode(), flags.dotAll());
+#endif
+  error.set(UndefinedValue());
+  if (!success) {
+    // We can fail because of OOM or over-recursion even if the syntax is valid.
+    if (cx->isThrowingOutOfMemory() || cx->isThrowingOverRecursed()) {
+      return false;
+    }
+    if (!cx->getPendingException(error)) {
+      return false;
+    }
+    cx->clearPendingException();
+  }
+  return true;
+}
