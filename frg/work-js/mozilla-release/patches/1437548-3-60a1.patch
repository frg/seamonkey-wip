# HG changeset patch
# User Patrick Brosset <pbrosset@mozilla.com>
# Date 1518514324 -3600
# Node ID 0ad9403b418ebf2a09e4aebc7412efc14d480da4
# Parent  721d5042b1ea3cc24404cbf58b34bd1f8efcb407
Bug 1437548 - 3 - Add a section that displays other fonts used in the page; r=gl

New accordion section in the fonts panel, that lists all fonts used elsewhere
than the currently selected node.
Collapsed by default.

MozReview-Commit-ID: IvgKbt9fH9w

diff --git a/devtools/client/inspector/fonts/actions/fonts.js b/devtools/client/inspector/fonts/actions/fonts.js
--- a/devtools/client/inspector/fonts/actions/fonts.js
+++ b/devtools/client/inspector/fonts/actions/fonts.js
@@ -8,16 +8,17 @@ const {
   UPDATE_FONTS,
 } = require("./index");
 
 module.exports = {
 
   /**
    * Update the list of fonts in the font inspector
    */
-  updateFonts(fonts) {
+  updateFonts(fonts, otherFonts) {
     return {
       type: UPDATE_FONTS,
       fonts,
+      otherFonts,
     };
   },
 
 };
diff --git a/devtools/client/inspector/fonts/components/FontList.js b/devtools/client/inspector/fonts/components/FontList.js
--- a/devtools/client/inspector/fonts/components/FontList.js
+++ b/devtools/client/inspector/fonts/components/FontList.js
@@ -23,28 +23,23 @@ class FontList extends PureComponent {
 
   render() {
     let {
       fonts,
       fontOptions,
       onPreviewFonts
     } = this.props;
 
-    return dom.div(
+    return dom.ul(
       {
-        id: "font-container"
+        className: "fonts-list"
       },
-      dom.ul(
-        {
-          id: "all-fonts"
-        },
-        fonts.map((font, i) => Font({
-          key: i,
-          font,
-          fontOptions,
-          onPreviewFonts,
-        }))
-      )
+      fonts.map((font, i) => Font({
+        key: i,
+        font,
+        fontOptions,
+        onPreviewFonts,
+      }))
     );
   }
 }
 
 module.exports = FontList;
diff --git a/devtools/client/inspector/fonts/components/FontsApp.js b/devtools/client/inspector/fonts/components/FontsApp.js
--- a/devtools/client/inspector/fonts/components/FontsApp.js
+++ b/devtools/client/inspector/fonts/components/FontsApp.js
@@ -4,52 +4,92 @@
 
 "use strict";
 
 const { createFactory, PureComponent } = require("devtools/client/shared/vendor/react");
 const dom = require("devtools/client/shared/vendor/react-dom-factories");
 const PropTypes = require("devtools/client/shared/vendor/react-prop-types");
 const { connect } = require("devtools/client/shared/vendor/react-redux");
 
+const Accordion = createFactory(require("devtools/client/inspector/layout/components/Accordion"));
 const FontList = createFactory(require("./FontList"));
 
 const { getStr } = require("../utils/l10n");
 const Types = require("../types");
 
 class FontsApp extends PureComponent {
   static get propTypes() {
     return {
-      fonts: PropTypes.arrayOf(PropTypes.shape(Types.font)).isRequired,
+      fontData: PropTypes.shape(Types.fontData).isRequired,
       fontOptions: PropTypes.shape(Types.fontOptions).isRequired,
       onPreviewFonts: PropTypes.func.isRequired,
     };
   }
 
-  render() {
+  renderElementFonts() {
     let {
-      fonts,
+      fontData,
       fontOptions,
       onPreviewFonts,
     } = this.props;
+    let { fonts } = fontData;
 
+    return fonts.length ?
+      FontList({
+        fonts,
+        fontOptions,
+        onPreviewFonts
+      })
+      :
+      dom.div(
+        {
+          className: "devtools-sidepanel-no-result"
+        },
+        getStr("fontinspector.noFontsOnSelectedElement")
+      );
+  }
+
+  renderOtherFonts() {
+    let {
+      fontData,
+      onPreviewFonts,
+      fontOptions,
+    } = this.props;
+    let { otherFonts } = fontData;
+
+    if (!otherFonts.length) {
+      return null;
+    }
+
+    return Accordion({
+      items: [
+        {
+          header: getStr("fontinspector.otherFontsInPageHeader"),
+          component: FontList,
+          componentProps: {
+            fontOptions,
+            fonts: otherFonts,
+            onPreviewFonts
+          },
+          opened: false
+        }
+      ]
+    });
+  }
+
+  render() {
     return dom.div(
       {
         className: "theme-sidebar inspector-tabpanel",
         id: "sidebar-panel-fontinspector"
       },
-      fonts.length ?
-        FontList({
-          fonts,
-          fontOptions,
-          onPreviewFonts
-        })
-        :
-        dom.div(
-          {
-            className: "devtools-sidepanel-no-result"
-          },
-          getStr("fontinspector.noFontsOnSelectedElement")
-        )
+      dom.div(
+        {
+          id: "font-container"
+        },
+        this.renderElementFonts(),
+        this.renderOtherFonts()
+      )
     );
   }
 }
 
 module.exports = connect(state => state)(FontsApp);
diff --git a/devtools/client/inspector/fonts/fonts.js b/devtools/client/inspector/fonts/fonts.js
--- a/devtools/client/inspector/fonts/fonts.js
+++ b/devtools/client/inspector/fonts/fonts.js
@@ -63,30 +63,75 @@ class FontInspector {
     // Listen for theme changes as the color of the previews depend on the theme
     gDevTools.on("theme-switched", this.onThemeChanged);
 
     this.store.dispatch(updatePreviewText(""));
     this.update(false, "");
   }
 
   /**
+   * Given all fonts on the page, and given the fonts used in given node, return all fonts
+   * not from the page not used in this node.
+   *
+   * @param  {Array} allFonts
+   *         All fonts used on the entire page
+   * @param  {Array} nodeFonts
+   *         Fonts used only in one of the nodes
+   * @return {Array}
+   *         All fonts, except the ones used in the current node
+   */
+  excludeNodeFonts(allFonts, nodeFonts) {
+    return allFonts.filter(font => {
+      return !nodeFonts.some(nodeFont => nodeFont.name === font.name);
+    });
+  }
+
+  /**
    * Destruction function called when the inspector is destroyed. Removes event listeners
    * and cleans up references.
    */
   destroy() {
     this.inspector.selection.off("new-node-front", this.onNewNode);
     this.inspector.sidebar.off("fontinspector-selected", this.onNewNode);
     gDevTools.off("theme-switched", this.onThemeChanged);
 
     this.document = null;
     this.inspector = null;
     this.pageStyle = null;
     this.store = null;
   }
 
+  async getFontsForNode(node, options) {
+    // In case we've been destroyed in the meantime
+    if (!this.document) {
+      return [];
+    }
+
+    let fonts = await this.pageStyle.getUsedFontFaces(node, options).catch(console.error);
+    if (!fonts) {
+      return [];
+    }
+
+    return fonts;
+  }
+
+  async getFontsNotInNode(nodeFonts, options) {
+    // In case we've been destroyed in the meantime
+    if (!this.document) {
+      return [];
+    }
+
+    let allFonts = await this.pageStyle.getAllUsedFontFaces(options).catch(console.error);
+    if (!allFonts) {
+      allFonts = [];
+    }
+
+    return this.excludeNodeFonts(allFonts, nodeFonts);
+  }
+
   /**
    * Returns true if the font inspector panel is visible, and false otherwise.
    */
   isPanelVisible() {
     return this.inspector.sidebar &&
            this.inspector.sidebar.getCurrentTabID() === "fontinspector";
   }
 
@@ -118,56 +163,60 @@ class FontInspector {
 
   async update() {
     // Stop refreshing if the inspector or store is already destroyed.
     if (!this.inspector || !this.store) {
       return;
     }
 
     let node = this.inspector.selection.nodeFront;
+
     let fonts = [];
+    let otherFonts = [];
+
     let { fontOptions } = this.store.getState();
     let { previewText } = fontOptions;
 
     // Clear the list of fonts if the currently selected node is not connected or a text
     // or element node unless all fonts are supposed to be shown.
     let isElementOrTextNode = this.inspector.selection.isElementNode() ||
                               this.inspector.selection.isTextNode();
     if (!node ||
         !this.isPanelVisible() ||
         !this.inspector.selection.isConnected() ||
         !isElementOrTextNode) {
-      this.store.dispatch(updateFonts(fonts));
+      this.store.dispatch(updateFonts(fonts, otherFonts));
       return;
     }
 
     let options = {
       includePreviews: true,
       previewText,
       previewFillStyle: getColor("body-color")
     };
 
-    fonts = await this.pageStyle.getUsedFontFaces(node, options).catch(console.error);
+    fonts = await this.getFontsForNode(node, options);
+    otherFonts = await this.getFontsNotInNode(fonts, options);
 
-    if (!fonts || !fonts.length) {
+    if (!fonts.length && !otherFonts.length) {
       // No fonts to display. Clear the previously shown fonts.
       if (this.store) {
-        this.store.dispatch(updateFonts(fonts));
+        this.store.dispatch(updateFonts(fonts, otherFonts));
       }
       return;
     }
 
-    for (let font of fonts) {
+    for (let font of [...fonts, ...otherFonts]) {
       font.previewUrl = await font.preview.data.string();
     }
 
     // in case we've been destroyed in the meantime
     if (!this.document) {
       return;
     }
 
-    this.store.dispatch(updateFonts(fonts));
+    this.store.dispatch(updateFonts(fonts, otherFonts));
 
     this.inspector.emit("fontinspector-updated");
   }
 }
 
 module.exports = FontInspector;
diff --git a/devtools/client/inspector/fonts/reducers/fonts.js b/devtools/client/inspector/fonts/reducers/fonts.js
--- a/devtools/client/inspector/fonts/reducers/fonts.js
+++ b/devtools/client/inspector/fonts/reducers/fonts.js
@@ -3,25 +3,28 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 "use strict";
 
 const {
   UPDATE_FONTS,
 } = require("../actions/index");
 
-const INITIAL_FONTS = [];
+const INITIAL_FONT_DATA = {
+  fonts: [],
+  otherFonts: []
+};
 
 let reducers = {
 
-  [UPDATE_FONTS](_, { fonts }) {
-    return fonts;
+  [UPDATE_FONTS](_, { fonts, otherFonts }) {
+    return { fonts, otherFonts };
   },
 
 };
 
-module.exports = function (fonts = INITIAL_FONTS, action) {
+module.exports = function (fontData = INITIAL_FONT_DATA, action) {
   let reducer = reducers[action.type];
   if (!reducer) {
-    return fonts;
+    return fontData;
   }
-  return reducer(fonts, action);
+  return reducer(fontData, action);
 };
diff --git a/devtools/client/inspector/fonts/test/browser.ini b/devtools/client/inspector/fonts/test/browser.ini
--- a/devtools/client/inspector/fonts/test/browser.ini
+++ b/devtools/client/inspector/fonts/test/browser.ini
@@ -13,10 +13,11 @@ support-files =
   !/devtools/client/inspector/test/shared-head.js
   !/devtools/client/shared/test/test-actor.js
   !/devtools/client/shared/test/test-actor-registry.js
 
 [browser_fontinspector.js]
 [browser_fontinspector_edit-previews.js]
 [browser_fontinspector_expand-css-code.js]
 [browser_fontinspector_expand-details.js]
+[browser_fontinspector_other-fonts.js]
 [browser_fontinspector_text-node.js]
 [browser_fontinspector_theme-change.js]
diff --git a/devtools/client/inspector/fonts/test/browser_fontinspector.js b/devtools/client/inspector/fonts/test/browser_fontinspector.js
--- a/devtools/client/inspector/fonts/test/browser_fontinspector.js
+++ b/devtools/client/inspector/fonts/test/browser_fontinspector.js
@@ -41,37 +41,33 @@ add_task(function* () {
   yield testBodyFonts(inspector, viewDoc);
   yield testDivFonts(inspector, viewDoc);
 });
 
 function isRemote(fontLi) {
   return fontLi.querySelectorAll(".font-format-url a").length === 1;
 }
 
-function getName(fontLi) {
-  return fontLi.querySelector(".font-name").textContent;
-}
-
 function getFormat(fontLi) {
   let link = fontLi.querySelector(".font-format-url a");
   if (!link) {
     return null;
   }
 
   return link.textContent;
 }
 
 function getCSSName(fontLi) {
   let text = fontLi.querySelector(".font-css-name").textContent;
 
   return text.substring(text.indexOf('"') + 1, text.lastIndexOf('"'));
 }
 
 function* testBodyFonts(inspector, viewDoc) {
-  let lis = viewDoc.querySelectorAll("#all-fonts > li");
+  let lis = getUsedFontsEls(viewDoc);
   is(lis.length, 5, "Found 5 fonts");
 
   for (let i = 0; i < FONTS.length; i++) {
     let li = lis[i];
     let font = FONTS[i];
 
     is(getName(li), font.name, "font " + i + " right font name");
     is(isRemote(li), font.remote, "font " + i + " remote value correct");
@@ -98,12 +94,12 @@ function* testBodyFonts(inspector, viewD
      "Arial", "local font has right css name");
 }
 
 function* testDivFonts(inspector, viewDoc) {
   let updated = inspector.once("fontinspector-updated");
   yield selectNode("div", inspector);
   yield updated;
 
-  let lis = viewDoc.querySelectorAll("#all-fonts > li");
+  let lis = getUsedFontsEls(viewDoc);
   is(lis.length, 1, "Found 1 font on DIV");
   is(getName(lis[0]), "Ostrich Sans Medium", "The DIV font has the right name");
 }
diff --git a/devtools/client/inspector/fonts/test/browser_fontinspector_edit-previews.js b/devtools/client/inspector/fonts/test/browser_fontinspector_edit-previews.js
--- a/devtools/client/inspector/fonts/test/browser_fontinspector_edit-previews.js
+++ b/devtools/client/inspector/fonts/test/browser_fontinspector_edit-previews.js
@@ -8,17 +8,17 @@
 // between systems, platforms and software versions.
 
 const TEST_URI = URL_ROOT + "browser_fontinspector.html";
 
 add_task(function* () {
   let {view} = yield openFontInspectorForURL(TEST_URI);
   let viewDoc = view.document;
 
-  let previews = viewDoc.querySelectorAll("#all-fonts .font-preview");
+  let previews = viewDoc.querySelectorAll("#font-container .font-preview");
   let initialPreviews = [...previews].map(p => p.src);
 
   info("Typing 'Abc' to check that the reference previews are correct.");
   yield updatePreviewText(view, "Abc");
   checkPreviewImages(viewDoc, initialPreviews, true);
 
   info("Typing something else to the preview box.");
   yield updatePreviewText(view, "The quick brown");
@@ -37,17 +37,17 @@ add_task(function* () {
  * @param {Array[String]} originalURIs
  *        An array of URIs to compare with the current URIs.
  * @param {Boolean} assertIdentical
  *        If true, this method asserts that the previous and current URIs are
  *        identical. If false, this method asserts that the previous and current
  *        URI's are different.
  */
 function checkPreviewImages(viewDoc, originalURIs, assertIdentical) {
-  let previews = viewDoc.querySelectorAll("#all-fonts .font-preview");
+  let previews = viewDoc.querySelectorAll("#font-container .font-preview");
   let newURIs = [...previews].map(p => p.src);
 
   is(newURIs.length, originalURIs.length,
     "The number of previews has not changed.");
 
   for (let i = 0; i < newURIs.length; ++i) {
     if (assertIdentical) {
       is(newURIs[i], originalURIs[i],
diff --git a/devtools/client/inspector/fonts/test/browser_fontinspector_expand-css-code.js b/devtools/client/inspector/fonts/test/browser_fontinspector_expand-css-code.js
--- a/devtools/client/inspector/fonts/test/browser_fontinspector_expand-css-code.js
+++ b/devtools/client/inspector/fonts/test/browser_fontinspector_expand-css-code.js
@@ -8,17 +8,17 @@
 
 const TEST_URI = URL_ROOT + "browser_fontinspector.html";
 
 add_task(function* () {
   let { view } = yield openFontInspectorForURL(TEST_URI);
   let viewDoc = view.document;
 
   info("Expanding the details section of the first font");
-  let fontEl = viewDoc.querySelector("#all-fonts > li");
+  let fontEl = getUsedFontsEls(viewDoc)[0];
   yield expandFontDetails(fontEl);
 
   info("Checking that the css font-face rule is collapsed by default");
   let codeEl = fontEl.querySelector(".font-css-code");
   is(codeEl.textContent, "@font-face {}", "The font-face rule is collapsed");
 
   info("Expanding the rule by clicking on the expander icon");
   let onExpanded = BrowserTestUtils.waitForCondition(() => {
diff --git a/devtools/client/inspector/fonts/test/browser_fontinspector_expand-details.js b/devtools/client/inspector/fonts/test/browser_fontinspector_expand-details.js
--- a/devtools/client/inspector/fonts/test/browser_fontinspector_expand-details.js
+++ b/devtools/client/inspector/fonts/test/browser_fontinspector_expand-details.js
@@ -8,28 +8,28 @@
 
 const TEST_URI = URL_ROOT + "browser_fontinspector.html";
 
 add_task(function* () {
   let { inspector, view } = yield openFontInspectorForURL(TEST_URI);
   let viewDoc = view.document;
 
   info("Checking all font are collapsed by default");
-  let fonts = viewDoc.querySelectorAll("#all-fonts > li");
+  let fonts = getUsedFontsEls(viewDoc);
   checkAllFontsCollapsed(fonts);
 
   info("Clicking on the first one to expand the font details");
   yield expandFontDetails(fonts[0]);
 
   ok(fonts[0].querySelector(".theme-twisty").hasAttribute("open"), `Twisty is open`);
   ok(isFontDetailsVisible(fonts[0]), `Font details is shown`);
 
   info("Selecting a node with different fonts and checking that all fonts are collapsed");
   yield selectNode(".black-text", inspector);
-  fonts = viewDoc.querySelectorAll("#all-fonts > li");
+  fonts = getUsedFontsEls(viewDoc);
   checkAllFontsCollapsed(fonts);
 });
 
 function checkAllFontsCollapsed(fonts) {
   fonts.forEach((el, i) => {
     let twisty = el.querySelector(".theme-twisty");
     ok(twisty, `Twisty ${i} exists`);
     ok(!twisty.hasAttribute("open"), `Twisty ${i} is closed`);
diff --git a/devtools/client/inspector/fonts/test/browser_fontinspector_other-fonts.js b/devtools/client/inspector/fonts/test/browser_fontinspector_other-fonts.js
new file mode 100644
--- /dev/null
+++ b/devtools/client/inspector/fonts/test/browser_fontinspector_other-fonts.js
@@ -0,0 +1,39 @@
+/* vim: set ts=2 et sw=2 tw=80: */
+/* Any copyright is dedicated to the Public Domain.
+   http://creativecommons.org/publicdomain/zero/1.0/ */
+"use strict";
+
+// Check that the font inspector has a section for "other fonts" and that section contains
+// the fonts *not* used in the currently selected element.
+// Check that it's collapsed by default, but can be expanded. That it does not contain
+// the fonts listed in the previous section.
+
+const TEST_URI = URL_ROOT + "browser_fontinspector.html";
+
+add_task(function* () {
+  const { inspector, view } = yield openFontInspectorForURL(TEST_URI);
+  const viewDoc = view.document;
+
+  let otherFontsAccordion = viewDoc.querySelector("#font-container .accordion");
+  ok(otherFontsAccordion, "There's an accordion in the panel");
+  is(otherFontsAccordion.textContent, "Other fonts in page", "It has the right title");
+
+  yield expandOtherFontsAccordion(viewDoc);
+  let otherFontsEls = getOtherFontsEls(viewDoc);
+
+  is(otherFontsEls.length, 1, "There is one font listed in the other fonts section");
+  // On Linux Times New Roman does not exist, Liberation Serif is used instead
+  let name = getName(otherFontsEls[0]);
+  ok(name === "Times New Roman" || name === "Liberation Serif",
+     "The other font listed is the right one");
+
+  info("Checking that fonts of the current element aren't listed");
+  yield selectNode(".bold-text", inspector);
+  yield expandOtherFontsAccordion(viewDoc);
+  otherFontsEls = getOtherFontsEls(viewDoc);
+
+  for (let otherFontEl of otherFontsEls) {
+    ok(![...getUsedFontsEls(viewDoc)].some(el => getName(el) === getName(otherFontEl)),
+       "Other font isn't listed in the main fonts section");
+  }
+});
diff --git a/devtools/client/inspector/fonts/test/browser_fontinspector_text-node.js b/devtools/client/inspector/fonts/test/browser_fontinspector_text-node.js
--- a/devtools/client/inspector/fonts/test/browser_fontinspector_text-node.js
+++ b/devtools/client/inspector/fonts/test/browser_fontinspector_text-node.js
@@ -12,11 +12,11 @@ add_task(function* () {
   let { inspector, view } = yield openFontInspectorForURL(TEST_URI);
   let viewDoc = view.document;
 
   info("Selecting the first text-node first-child of <body>");
   let bodyNode = yield getNodeFront("body", inspector);
   let { nodes } = yield inspector.walker.children(bodyNode);
   yield selectNode(nodes[0], inspector);
 
-  let sections = viewDoc.querySelectorAll("#all-fonts > li");
-  is(sections.length, 1, "Font inspector shows 1 font");
+  let lis = getUsedFontsEls(viewDoc);
+  is(lis.length, 1, "Font inspector shows 1 font");
 });
diff --git a/devtools/client/inspector/fonts/test/browser_fontinspector_theme-change.js b/devtools/client/inspector/fonts/test/browser_fontinspector_theme-change.js
--- a/devtools/client/inspector/fonts/test/browser_fontinspector_theme-change.js
+++ b/devtools/client/inspector/fonts/test/browser_fontinspector_theme-change.js
@@ -19,27 +19,27 @@ registerCleanupFunction(() => {
 
 add_task(function* () {
   let { inspector, view } = yield openFontInspectorForURL(TEST_URI);
   let { document: doc } = view;
 
   yield selectNode(".normal-text", inspector);
 
   // Store the original preview URI for later comparison.
-  let originalURI = doc.querySelector("#all-fonts .font-preview").src;
+  let originalURI = doc.querySelector("#font-container .font-preview").src;
   let newTheme = originalTheme === "light" ? "dark" : "light";
 
   info(`Original theme was '${originalTheme}'.`);
 
   yield setThemeAndWaitForUpdate(newTheme, inspector);
-  isnot(doc.querySelector("#all-fonts .font-preview").src, originalURI,
+  isnot(doc.querySelector("#font-container .font-preview").src, originalURI,
     "The preview image changed with the theme.");
 
   yield setThemeAndWaitForUpdate(originalTheme, inspector);
-  is(doc.querySelector("#all-fonts .font-preview").src, originalURI,
+  is(doc.querySelector("#font-container .font-preview").src, originalURI,
     "The preview image is correct after the original theme was restored.");
 });
 
 /**
  * Sets the current theme and waits for fontinspector-updated event.
  *
  * @param {String} theme - the new theme
  * @param {Object} inspector - the inspector panel
diff --git a/devtools/client/inspector/fonts/test/head.js b/devtools/client/inspector/fonts/test/head.js
--- a/devtools/client/inspector/fonts/test/head.js
+++ b/devtools/client/inspector/fonts/test/head.js
@@ -97,8 +97,59 @@ async function expandFontDetails(fontEl)
   twisty.click();
   await onExpanded;
 }
 
 function isFontDetailsVisible(fontEl) {
   return [...fontEl.querySelectorAll(".font-css-name, .font-css-code, .font-format-url")]
          .every(el => el.getBoxQuads().length);
 }
+
+/**
+ * Get all of the <li> elements for the fonts used on the currently selected element.
+ *
+ * @param  {document} viewDoc
+ * @return {Array}
+ */
+function getUsedFontsEls(viewDoc) {
+  return viewDoc.querySelectorAll("#font-container > .fonts-list > li");
+}
+
+/**
+ * Expand the other fonts accordion.
+ */
+async function expandOtherFontsAccordion(viewDoc) {
+  info("Expanding the other fonts section");
+
+  let accordion = viewDoc.querySelector("#font-container .accordion");
+  let isExpanded = () => accordion.querySelector(".fonts-list");
+
+  if (isExpanded()) {
+    return;
+  }
+
+  let onExpanded = BrowserTestUtils.waitForCondition(isExpanded,
+                                                     "Waiting for other fonts section");
+  accordion.querySelector(".theme-twisty").click();
+  await onExpanded;
+}
+
+/**
+ * Get all of the <li> elements for the fonts used elsewhere in the document.
+ *
+ * @param  {document} viewDoc
+ * @return {Array}
+ */
+function getOtherFontsEls(viewDoc) {
+  return viewDoc.querySelectorAll("#font-container .accordion .fonts-list > li");
+}
+
+/**
+ * Given a font element, return its name.
+ *
+ * @param  {DOMNode} fontEl
+ *         The font element.
+ * @return {String}
+ *         The name of the font as shown in the UI.
+ */
+function getName(fontEl) {
+  return fontEl.querySelector(".font-name").textContent;
+}
diff --git a/devtools/client/inspector/fonts/types.js b/devtools/client/inspector/fonts/types.js
--- a/devtools/client/inspector/fonts/types.js
+++ b/devtools/client/inspector/fonts/types.js
@@ -4,17 +4,17 @@
 
 "use strict";
 
 const PropTypes = require("devtools/client/shared/vendor/react-prop-types");
 
 /**
  * A single font.
  */
-exports.font = {
+const font = exports.font = {
   // The name of the font family
   CSSFamilyName: PropTypes.string,
 
   // The format of the font
   format: PropTypes.string,
 
   // The name of the font
   name: PropTypes.string,
@@ -31,8 +31,19 @@ exports.font = {
   // The URI of the font file
   URI: PropTypes.string,
 };
 
 exports.fontOptions = {
   // The current preview text
   previewText: PropTypes.string,
 };
+
+/**
+ * Font data
+ */
+exports.fontData = {
+  // The fonts used in the current element.
+  fonts: PropTypes.arrayOf(PropTypes.shape(font)),
+
+  // Fonts used elsewhere.
+  otherFonts: PropTypes.arrayOf(PropTypes.shape(font)),
+};
diff --git a/devtools/client/inspector/reducers.js b/devtools/client/inspector/reducers.js
--- a/devtools/client/inspector/reducers.js
+++ b/devtools/client/inspector/reducers.js
@@ -9,11 +9,11 @@
 
 exports.animations = require("devtools/client/inspector/animation/reducers/animations");
 exports.boxModel = require("devtools/client/inspector/boxmodel/reducers/box-model");
 exports.changes = require("devtools/client/inspector/changes/reducers/changes");
 exports.events = require("devtools/client/inspector/events/reducers/events");
 exports.extensionsSidebar = require("devtools/client/inspector/extensions/reducers/sidebar");
 exports.flexbox = require("devtools/client/inspector/flexbox/reducers/flexbox");
 exports.fontOptions = require("devtools/client/inspector/fonts/reducers/font-options");
-exports.fonts = require("devtools/client/inspector/fonts/reducers/fonts");
+exports.fontData = require("devtools/client/inspector/fonts/reducers/fonts");
 exports.grids = require("devtools/client/inspector/grids/reducers/grids");
 exports.highlighterSettings = require("devtools/client/inspector/grids/reducers/highlighter-settings");
diff --git a/devtools/client/locales/en-US/font-inspector.properties b/devtools/client/locales/en-US/font-inspector.properties
--- a/devtools/client/locales/en-US/font-inspector.properties
+++ b/devtools/client/locales/en-US/font-inspector.properties
@@ -15,8 +15,12 @@ fontinspector.system=system
 
 # LOCALIZATION NOTE (fontinspector.remote) This label indicates that the font is a remote
 # font.
 fontinspector.remote=remote
 
 # LOCALIZATION NOTE (fontinspector.noFontsOnSelectedElement): This label is shown when
 # no fonts found on the selected element.
 fontinspector.noFontsOnSelectedElement=No fonts were found for the current element.
+
+# LOCALIZATION NOTE (fontinspector.otherFontsInPageHeader): This is the text for the
+# header of a collapsible section containing other fonts used in the page.
+fontinspector.otherFontsInPageHeader=Other fonts in page
\ No newline at end of file
diff --git a/devtools/client/themes/fonts.css b/devtools/client/themes/fonts.css
--- a/devtools/client/themes/fonts.css
+++ b/devtools/client/themes/fonts.css
@@ -10,30 +10,30 @@
   height: 100%;
 }
 
 #font-container {
   overflow: auto;
   flex: auto;
 }
 
-#all-fonts {
+.fonts-list {
   padding: 0;
   margin: 0;
   list-style: none;
 }
 
 .font {
   border: 1px solid var(--theme-splitter-color);
   border-width: 0 1px 1px 0;
   display: grid;
   grid-template-columns: 14px auto 1fr;
   grid-template-rows: 50px;
   grid-column-gap: 10px;
-  padding: 0 10px;
+  padding: 0 10px 0 5px;
 }
 
 #font-container .theme-twisty {
   display: inline-block;
   cursor: pointer;
   place-self: center;
   vertical-align: text-top;
 }
