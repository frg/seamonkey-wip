# HG changeset patch
# User Jan Odvarko <odvarko@gmail.com>
# Date 1506350050 -7200
# Node ID 367484c62750ede3f737b9b72a53dc31d55242a9
# Parent  2f3e92f740f51b9bd8f56e72c3661dd627e40d93
Bug 1304328 - Stop using XUL in webconsole/jsterm.js; r=nchevobbe

MozReview-Commit-ID: EPLUaothwH5

diff --git a/devtools/client/themes/webconsole.css b/devtools/client/themes/webconsole.css
--- a/devtools/client/themes/webconsole.css
+++ b/devtools/client/themes/webconsole.css
@@ -401,66 +401,104 @@ html #webconsole-notificationbox {
   width: 100vw;
 }
 
 .jsterm-input-container {
   background-color: var(--theme-tab-toolbar-background);
   border-top: 1px solid var(--theme-splitter-color);
 }
 
+.jsterm-input-node {
+  /* Always allow scrolling on input - it auto expands in js by setting height,
+     but don't want it to get bigger than the window. 24px = toolbar height. */
+  max-height: calc(90vh - 24px);
+  background-image: var(--theme-command-line-image);
+  background-repeat: no-repeat;
+  background-size: 16px 16px;
+  background-position: 4px 50%;
+  color: var(--theme-content-color1);
+}
+
+.jsterm-complete-node {
+  color: var(--theme-comment);
+}
+
 .theme-light .jsterm-input-container {
   /* For light theme use a white background for the input - it looks better
      than off-white */
   background-color: #fff;
   border-top-color: #e0e0e0;
 }
 
 .theme-firebug .jsterm-input-container {
   border-top: 1px solid #ccc;
 }
 
-.jsterm-input-node,
-.jsterm-complete-node {
+/*  styles for the new HTML frontend */
+
+html .jsterm-stack-node {
+  position: relative;
+}
+
+textarea.jsterm-input-node,
+textarea.jsterm-complete-node {
+  width: 100%;
+  border: none;
+  margin: 0;
+  background-color: transparent;
+  resize: none;
+  font-size: var(--theme-toolbar-font-size);
+  line-height: 16px;
+  overflow-x: hidden;
+  /* Set padding for console input on textarea to make sure it is included in
+     scrollHeight that is used when resizing JSTerminal's input. */
+  padding: 4px 0;
+  padding-inline-start: 20px;
+}
+
+textarea.jsterm-complete-node {
+  position: absolute;
+  top: 0;
+  left: 0;
+  height: 100%;
+  pointer-events: none;
+}
+
+textarea.jsterm-input-node:focus {
+  background-image: var(--theme-command-line-image-focus);
+  box-shadow: none;
+}
+
+
+/*  styles for the old frontend, which can be removed in Bug 1381834 */
+
+textbox.jsterm-input-node,
+textbox.jsterm-complete-node {
   border: none;
   padding: 0;
   padding-inline-start: 20px;
   margin: 0;
   -moz-appearance: none;
   background-color: transparent;
 }
 
-.jsterm-input-node[focused="true"] {
-  background-image: var(--theme-command-line-image-focus);
-  box-shadow: none;
-}
-
-.jsterm-complete-node {
-  color: var(--theme-comment);
-}
-
-.jsterm-input-node {
-  /* Always allow scrolling on input - it auto expands in js by setting height,
-     but don't want it to get bigger than the window. 24px = toolbar height. */
-  max-height: calc(90vh - 24px);
-  background-image: var(--theme-command-line-image);
-  background-repeat: no-repeat;
-  background-size: 16px 16px;
-  background-position: 4px 50%;
-  color: var(--theme-content-color1);
-}
-
-:-moz-any(.jsterm-input-node,
-          .jsterm-complete-node) > .textbox-input-box > .textbox-textarea {
+:-moz-any(textbox.jsterm-input-node,
+          textbox.jsterm-complete-node) > .textbox-input-box > .textbox-textarea {
   overflow-x: hidden;
-  /* Set padding for console input on textbox to make sure it is inlcuded in
+  /* Set padding for console input on textbox to make sure it is included in
      scrollHeight that is used when resizing JSTerminal's input. Note: textbox
      default style has important already */
   padding: 4px 0 !important;
 }
 
+textbox.jsterm-input-node[focused="true"] {
+  background-image: var(--theme-command-line-image-focus);
+  box-shadow: none;
+}
+
 .inlined-variables-view .message-body {
   display: flex;
   flex-direction: column;
   resize: vertical;
   overflow: auto;
   min-height: 200px;
 }
 .inlined-variables-view iframe {
diff --git a/devtools/client/webconsole/jsterm.js b/devtools/client/webconsole/jsterm.js
--- a/devtools/client/webconsole/jsterm.js
+++ b/devtools/client/webconsole/jsterm.js
@@ -1001,17 +1001,21 @@ JSTerm.prototype = {
   resizeInput: function () {
     let inputNode = this.inputNode;
 
     // Reset the height so that scrollHeight will reflect the natural height of
     // the contents of the input field.
     inputNode.style.height = "auto";
 
     // Now resize the input field to fit its contents.
-    let scrollHeight = inputNode.inputField.scrollHeight;
+    // TODO: remove `inputNode.inputField.scrollHeight` when the old
+    // console UI is removed. See bug 1381834
+    let scrollHeight = inputNode.inputField ?
+      inputNode.inputField.scrollHeight : inputNode.scrollHeight;
+
     if (scrollHeight > 0) {
       inputNode.style.height = scrollHeight + "px";
     }
   },
 
   /**
    * Sets the value of the input field (command line), and resizes the field to
    * fit its contents. This method is preferred over setting "inputNode.value"
diff --git a/devtools/client/webconsole/new-console-output/new-console-output-wrapper.js b/devtools/client/webconsole/new-console-output/new-console-output-wrapper.js
--- a/devtools/client/webconsole/new-console-output/new-console-output-wrapper.js
+++ b/devtools/client/webconsole/new-console-output/new-console-output-wrapper.js
@@ -55,17 +55,18 @@ NewConsoleOutputWrapper.prototype = {
       }
 
       // Do not focus if an input field was clicked
       if (target.closest("input")) {
         return;
       }
 
       // Do not focus if something other than the output region was clicked
-      if (!target.closest(".webconsole-output")) {
+      // (including e.g. the clear messages button in toolbar)
+      if (!target.closest(".webconsole-output-wrapper")) {
         return;
       }
 
       // Do not focus if something is selected
       let selection = this.document.defaultView.getSelection();
       if (selection && !selection.isCollapsed) {
         return;
       }
diff --git a/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_input_focus.js b/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_input_focus.js
--- a/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_input_focus.js
+++ b/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_input_focus.js
@@ -13,48 +13,43 @@ const TEST_URI =
     console.log("console message 1");
   </script>`;
 
 add_task(function* () {
   let hud = yield openNewTabAndConsole(TEST_URI);
 
   hud.jsterm.clearOutput();
   let inputNode = hud.jsterm.inputNode;
-  ok(inputNode.getAttribute("focused"), "input node is focused after output is cleared");
+  ok(hasFocus(inputNode), "input node is focused after output is cleared");
 
   info("Focus during message logging");
   ContentTask.spawn(gBrowser.selectedBrowser, {}, function* () {
     content.wrappedJSObject.console.log("console message 2");
   });
   let msg = yield waitFor(() => findMessage(hud, "console message 2"));
-  ok(inputNode.getAttribute("focused"), "input node is focused, first time");
+  ok(hasFocus(inputNode, "input node is focused, first time"));
 
   info("Focus after clicking in the output area");
   yield waitForBlurredInput(hud);
   EventUtils.sendMouseEvent({type: "click"}, msg);
-  ok(inputNode.getAttribute("focused"), "input node is focused, second time");
+  ok(hasFocus(inputNode), "input node is focused, second time");
 
   info("Setting a text selection and making sure a click does not re-focus");
   yield waitForBlurredInput(hud);
   let selection = hud.iframeWindow.getSelection();
   selection.selectAllChildren(msg.querySelector(".message-body"));
   EventUtils.sendMouseEvent({type: "click"}, msg);
-  ok(!inputNode.getAttribute("focused"),
-    "input node not focused after text is selected");
+  ok(!hasFocus(inputNode), "input node not focused after text is selected");
 });
 
 function waitForBlurredInput(hud) {
   let inputNode = hud.jsterm.inputNode;
   return new Promise(resolve => {
     let lostFocus = () => {
-      ok(!inputNode.getAttribute("focused"), "input node is not focused");
+      ok(!hasFocus(inputNode), "input node is not focused");
       resolve();
     };
     inputNode.addEventListener("blur", lostFocus, { once: true });
 
-    // Clicking on a DOM Node outside of the webconsole document. The 'blur' event fires
-    // if we click on something in this document (like the filter box), but the 'focus'
-    // event won't re-fire on the textbox XBL binding when it's clicked on again.
-    // Bug 1304328 is tracking removal of XUL for jsterm, we should be able to click on
-    // the filter textbox instead of the url bar after that.
-    document.getElementById("urlbar").click();
+    // The 'blur' event fires if we focus e.g. the filter box.
+    inputNode.ownerDocument.querySelector("input.text-filter").focus();
   });
 }
diff --git a/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_keyboard_accessibility.js b/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_keyboard_accessibility.js
--- a/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_keyboard_accessibility.js
+++ b/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_keyboard_accessibility.js
@@ -49,17 +49,17 @@ add_task(function* () {
   let clearShortcut;
   if (Services.appinfo.OS === "Darwin") {
     clearShortcut = WCUL10n.getStr("webconsole.clear.keyOSX");
   } else {
     clearShortcut = WCUL10n.getStr("webconsole.clear.key");
   }
   synthesizeKeyShortcut(clearShortcut);
   yield waitFor(() => findMessages(hud, "").length == 0);
-  is(hud.jsterm.inputNode.getAttribute("focused"), "true", "jsterm input is focused");
+  ok(hasFocus(hud.jsterm.inputNode), "jsterm input is focused");
 
   // Focus filter
   info("try ctrl-f to focus filter");
   synthesizeKeyShortcut(WCUL10n.getStr("webconsole.find.key"));
-  ok(!hud.jsterm.inputNode.getAttribute("focused"), "jsterm input is not focused");
+  ok(!hasFocus(hud.jsterm.inputNode), "jsterm input is not focused");
   is(hud.ui.filterBox, outputScroller.ownerDocument.activeElement,
     "filter input is focused");
 });
diff --git a/devtools/client/webconsole/new-console-output/test/mochitest/head.js b/devtools/client/webconsole/new-console-output/test/mochitest/head.js
--- a/devtools/client/webconsole/new-console-output/test/mochitest/head.js
+++ b/devtools/client/webconsole/new-console-output/test/mochitest/head.js
@@ -1,15 +1,15 @@
 /* -*- indent-tabs-mode: nil; js-indent-level: 2 -*- */
 /* vim: set ft=javascript ts=2 et sw=2 tw=80: */
 /* Any copyright is dedicated to the Public Domain.
  * http://creativecommons.org/publicdomain/zero/1.0/ */
 /* import-globals-from ../../../../framework/test/shared-head.js */
 /* exported WCUL10n, openNewTabAndConsole, waitForMessages, waitFor, findMessage,
-   openContextMenu, hideContextMenu, loadDocument,
+   openContextMenu, hideContextMenu, loadDocument, hasFocus,
    waitForNodeMutation, testOpenInDebugger, checkClickOnNode */
 
 "use strict";
 
 // shared-head.js handles imports, constants, and utility functions
 // Load the shared-head file first.
 Services.scriptloader.loadSubScript(
   "chrome://mochitests/content/browser/devtools/client/framework/test/shared-head.js",
@@ -248,8 +248,15 @@ function* checkClickOnNode(hud, toolbox,
 
   let dbg = toolbox.getPanel("jsdebugger");
   is(
     dbg._selectors.getSelectedSource(dbg._getState()).get("url"),
     url,
     "expected source url"
   );
 }
+
+/**
+ * Returns true if the give node is currently focused.
+ */
+function hasFocus(node) {
+  return node.ownerDocument.activeElement == node;
+}
diff --git a/devtools/client/webconsole/webconsole.xhtml b/devtools/client/webconsole/webconsole.xhtml
--- a/devtools/client/webconsole/webconsole.xhtml
+++ b/devtools/client/webconsole/webconsole.xhtml
@@ -1,14 +1,14 @@
 <?xml version="1.0" encoding="utf-8"?>
 <!-- This Source Code Form is subject to the terms of the Mozilla Public
    - License, v. 2.0. If a copy of the MPL was not distributed with this
    - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->
 <!DOCTYPE html>
-<html xmlns="http://www.w3.org/1999/xhtml" xmlns:xul="http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul" dir="">
+<html xmlns="http://www.w3.org/1999/xhtml" dir="">
 <head>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
     <link rel="stylesheet" href="chrome://devtools/skin/widgets.css"/>
     <link rel="stylesheet" href="resource://devtools/client/themes/light-theme.css"/>
     <link rel="stylesheet" href="chrome://devtools/skin/webconsole.css"/>
     <link rel="stylesheet" href="chrome://devtools/skin/components-frame.css"/>
     <link rel="stylesheet" href="resource://devtools/client/shared/components/reps/reps.css"/>
     <link rel="stylesheet" href="resource://devtools/client/shared/components/tabs/tabs.css"/>
@@ -18,23 +18,23 @@
     <script src="chrome://devtools/content/shared/theme-switching.js"></script>
     <script type="application/javascript"
             src="resource://devtools/client/webconsole/new-console-output/main.js"></script>
   </head>
   <body class="theme-sidebar" role="application">
     <div id="app-wrapper" class="theme-body">
       <div id="output-container" role="document" aria-live="polite"/>
       <div id="jsterm-wrapper">
-        <xul:notificationbox id="webconsole-notificationbox">
+        <div id="webconsole-notificationbox">
           <div class="jsterm-input-container" style="direction:ltr">
-            <xul:stack class="jsterm-stack-node" flex="1">
-              <xul:textbox class="jsterm-complete-node devtools-monospace"
-                       multiline="true" rows="1" tabindex="-1"/>
-              <xul:textbox class="jsterm-input-node devtools-monospace"
-                       multiline="true" rows="1" tabindex="0"
+            <div class="jsterm-stack-node">
+              <textarea class="jsterm-complete-node devtools-monospace"
+                       tabindex="-1"/>
+              <textarea class="jsterm-input-node devtools-monospace"
+                       rows="1" tabindex="0"
                        aria-autocomplete="list"/>
-            </xul:stack>
+            </div>
           </div>
-        </xul:notificationbox>
+        </div>
       </div>
     </div>
   </body>
 </html>
