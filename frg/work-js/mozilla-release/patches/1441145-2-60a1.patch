# HG changeset patch
# User Andreas Pehrson <pehrsons@mozilla.com>
# Date 1519656177 -3600
#      Mon Feb 26 15:42:57 2018 +0100
# Node ID c672cca7633a7e4a1571a668cf3808b1834a075b
# Parent  844dff31a28622a1e3416bbc5332ba2ca37a64f1
Bug 1441145 - Swap width and height in target capability if the frame is rotated. r=jib

MozReview-Commit-ID: 3QO3W0J3b6G

diff --git a/dom/media/webrtc/MediaEngineRemoteVideoSource.cpp b/dom/media/webrtc/MediaEngineRemoteVideoSource.cpp
--- a/dom/media/webrtc/MediaEngineRemoteVideoSource.cpp
+++ b/dom/media/webrtc/MediaEngineRemoteVideoSource.cpp
@@ -478,16 +478,23 @@ MediaEngineRemoteVideoSource::DeliverFra
     MutexAutoLock lock(mMutex);
     MOZ_ASSERT(mState == kStarted);
     req_max_width = mCapability.width & 0xffff;
     req_max_height = mCapability.height & 0xffff;
     req_ideal_width = (mCapability.width >> 16) & 0xffff;
     req_ideal_height = (mCapability.height >> 16) & 0xffff;
   }
 
+  if (aProps.rotation() == 90 || aProps.rotation() == 270) {
+    // This frame is rotated, so what was negotiated as width is now height,
+    // and vice versa.
+    std::swap(req_max_width, req_max_height);
+    std::swap(req_ideal_width, req_ideal_height);
+  }
+
   int32_t dst_max_width = std::min(req_max_width, aProps.width());
   int32_t dst_max_height = std::min(req_max_height, aProps.height());
   // This logic works for both camera and screen sharing case.
   // for camera case, req_ideal_width and req_ideal_height is 0.
   // The following snippet will set dst_width to dst_max_width and dst_height to dst_max_height
   int32_t dst_width = std::min(req_ideal_width > 0 ? req_ideal_width : aProps.width(), dst_max_width);
   int32_t dst_height = std::min(req_ideal_height > 0 ? req_ideal_height : aProps.height(), dst_max_height);
 
@@ -538,19 +545,21 @@ MediaEngineRemoteVideoSource::DeliverFra
   if (!image->CopyData(data)) {
     MOZ_ASSERT_UNREACHABLE("We might fail to allocate a buffer, but with this "
                            "being a recycling container that shouldn't happen");
     return 0;
   }
 
 #ifdef DEBUG
   static uint32_t frame_num = 0;
-  LOGFRAME(("frame %d (%dx%d)->(%dx%d); timeStamp %u, ntpTimeMs %" PRIu64 ", renderTimeMs %" PRIu64,
+  LOGFRAME(("frame %d (%dx%d)->(%dx%d); rotation %d, timeStamp %u, "
+            "ntpTimeMs %" PRIu64 ", renderTimeMs %" PRIu64,
             frame_num++, aProps.width(), aProps.height(), dst_width, dst_height,
-            aProps.timeStamp(), aProps.ntpTimeMs(), aProps.renderTimeMs()));
+            aProps.rotation(), aProps.timeStamp(), aProps.ntpTimeMs(),
+            aProps.renderTimeMs()));
 #endif
 
   bool sizeChanged = false;
   {
     MutexAutoLock lock(mMutex);
     // implicitly releases last image
     sizeChanged = (!mImage && image) ||
                   (mImage && image && mImage->GetSize() != image->GetSize());
