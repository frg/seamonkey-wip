# HG changeset patch
# User Steve Fink <sfink@mozilla.com>
# Date 1524683751 25200
# Node ID 6529717260a226e15e02da023bfced594a763204
# Parent  9e2501f811e5e63e861cc6defaf0966aeefd3644
Bug 1456858 - structured clone comment fixups, r=jorendorff

diff --git a/js/public/StructuredClone.h b/js/public/StructuredClone.h
--- a/js/public/StructuredClone.h
+++ b/js/public/StructuredClone.h
@@ -327,18 +327,36 @@ struct JSStructuredCloneCallbacks {
     StructuredCloneErrorOp reportError;
     ReadTransferStructuredCloneOp readTransfer;
     TransferStructuredCloneOp writeTransfer;
     FreeTransferStructuredCloneOp freeTransfer;
     CanTransferStructuredCloneOp canTransfer;
 };
 
 enum OwnTransferablePolicy {
+    /**
+     * The buffer owns any Transferables that it might contain, and should
+     * properly release them upon destruction.
+     */
     OwnsTransferablesIfAny,
+
+    /**
+     * Do not free any Transferables within this buffer when deleting it. This
+     * is used to mark as clone buffer as containing data from another process,
+     * and so it can't legitimately contain pointers. If the buffer claims to
+     * have transferables, it's a bug or an attack. This is also used for
+     * abandon(), where a buffer still contains raw data but the ownership has
+     * been given over to some other entity.
+     */
     IgnoreTransferablesIfAny,
+
+    /**
+     * A buffer that cannot contain Transferables at all. This usually means
+     * the buffer is empty (not yet filled in, or having been cleared).
+     */
     NoTransferables
 };
 
 namespace js
 {
     class SharedArrayRawBuffer;
 
     class SharedArrayRawBufferRefs
diff --git a/js/src/builtin/TestingFunctions.cpp b/js/src/builtin/TestingFunctions.cpp
--- a/js/src/builtin/TestingFunctions.cpp
+++ b/js/src/builtin/TestingFunctions.cpp
@@ -3085,18 +3085,18 @@ Deserialize(JSContext* cx, unsigned argc
             if (!str)
                 return false;
             auto maybeScope = ParseCloneScope(cx, str);
             if (!maybeScope) {
                 JS_ReportErrorASCII(cx, "Invalid structured clone scope");
                 return false;
             }
 
-            if (fuzzingSafe && *maybeScope < scope) {
-                JS_ReportErrorASCII(cx, "Fuzzing builds must not set less restrictive scope "
+            if (*maybeScope < scope) {
+                JS_ReportErrorASCII(cx, "Cannot use less restrictive scope "
                                     "than the deserialized clone buffer's scope");
                 return false;
             }
 
             scope = *maybeScope;
         }
     }
 
