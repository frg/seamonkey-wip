# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1531302282 -7200
#      Wed Jul 11 11:44:42 2018 +0200
# Node ID e378e29a5ebbbff1605efee6739cbf3d4581101f
# Parent  b3468b999b15d5b91c8d55a9c3745e4d4f17591e
Bug 1468536 - Remove Debugger.Object.prototype.global. r=jimb

With same-compartment realms, cross-compartment wrappers don't have a meaningful global associated with them because CCWs are shared by all realms in the compartment.

diff --git a/devtools/server/actors/webconsole.js b/devtools/server/actors/webconsole.js
--- a/devtools/server/actors/webconsole.js
+++ b/devtools/server/actors/webconsole.js
@@ -1455,22 +1455,21 @@ WebConsoleActor.prototype =
     // Ready to evaluate the string.
     helpers.evalInput = string;
 
     let evalOptions;
     if (typeof options.url == "string") {
       evalOptions = { url: options.url };
     }
 
-    // If the debugger object is changed from the last evaluation,
-    // adopt this._lastConsoleInputEvaluation value in the new debugger,
-    // to prevents "Debugger.Object belongs to a different Debugger" exceptions
-    // related to the $_ bindings.
-    if (this._lastConsoleInputEvaluation &&
-        this._lastConsoleInputEvaluation.global !== dbgWindow) {
+    // Adopt this._lastConsoleInputEvaluation value in the new debugger,
+    // to prevent "Debugger.Object belongs to a different Debugger" exceptions
+    // related to the $_ bindings if the debugger object is changed from the
+    // last evaluation.
+    if (this._lastConsoleInputEvaluation) {
       this._lastConsoleInputEvaluation = dbg.adoptDebuggeeValue(
         this._lastConsoleInputEvaluation
       );
     }
 
     let result;
 
     if (frame) {
diff --git a/js/src/doc/Debugger/Debugger.Object.md b/js/src/doc/Debugger/Debugger.Object.md
--- a/js/src/doc/Debugger/Debugger.Object.md
+++ b/js/src/doc/Debugger/Debugger.Object.md
@@ -306,23 +306,16 @@ from its prototype:
     referent is not a [`Promise`][promise], throw a `TypeError` exception.
 
 `promiseTimeToResolution`
 :   If the referent is a [`Promise`][promise], this is the number of
     milliseconds elapsed between when the [`Promise`][promise] was created and
     when it was resolved. If the referent hasn't been resolved or is not a
     [`Promise`][promise], throw a `TypeError` exception.
 
-`global`
-:   A `Debugger.Object` instance referring to the global object in whose
-    scope the referent was allocated. This does not unwrap cross-compartment
-    wrappers: if the referent is a wrapper, the result refers to the
-    wrapper's global, not the wrapped object's global. The result refers to
-    the global directly, not via a wrapper.
-
 <code id="allocationsite">allocationSite</code>
 :   If [object allocation site tracking][tracking-allocs] was enabled when this
     `Debugger.Object`'s referent was allocated, return the
     [JavaScript execution stack][saved-frame] captured at the time of the
     allocation. Otherwise, return `null`.
 
 
 ## Function Properties of the Debugger.Object prototype
diff --git a/js/src/jit-test/tests/debug/Debugger-debuggees-18.js b/js/src/jit-test/tests/debug/Debugger-debuggees-18.js
--- a/js/src/jit-test/tests/debug/Debugger-debuggees-18.js
+++ b/js/src/jit-test/tests/debug/Debugger-debuggees-18.js
@@ -29,31 +29,28 @@ assertDebuggees();
 //
 // There's no direct "G", since globals are always in their own
 // compartments, never the debugger's; if we ever viewed them directly,
 // that would be a compartment violation.
 
 // "dg1" means "Debugger.Object referring (directly) to g1".
 var dg1 = dbg.addDebuggee(g1);
 dg1.toSource = function() { return "[Debugger.Object for global g1]"; };
-assertEq(dg1.global, dg1);
 assertEq(dg1.unwrap(), dg1);
 assertDebuggees(dg1);
 
 // We need to add g2 as a debuggee; that's the only way to get a D.O referring
 // to it without a wrapper.
 var dg2 = dbg.addDebuggee(g2);
 dg2.toSource = function() { return "[Debugger.Object for global g2]"; };
-assertEq(dg2.global, dg2);
 assertEq(dg2.unwrap(), dg2);
 assertDebuggees(dg1, dg2);
 
 // "dwg1" means "Debugger.Object referring to CCW of g1".
 var dwg1 = dg2.makeDebuggeeValue(g1);
-assertEq(dwg1.global, dg2);
 assertEq(dwg1.unwrap(), dg1);
 dwg1.toSource = function() { return "[Debugger.Object for CCW of global g1]"; };
 
 assertDebuggees(dg1, dg2);
 assertEq(dbg.removeDebuggee(g1), undefined);
 assertEq(dbg.removeDebuggee(g2), undefined);
 assertDebuggees();
 
diff --git a/js/src/jit-test/tests/debug/Object-executeInGlobal-05.js b/js/src/jit-test/tests/debug/Object-executeInGlobal-05.js
--- a/js/src/jit-test/tests/debug/Object-executeInGlobal-05.js
+++ b/js/src/jit-test/tests/debug/Object-executeInGlobal-05.js
@@ -7,16 +7,15 @@ var dbg = new Debugger();
 var g1 = newGlobal();
 var dg1 = dbg.addDebuggee(g1);
 
 var g2 = newGlobal();
 var dg2 = dbg.addDebuggee(g2);
 
 // Generate a Debugger.Object viewing g2 from g1's compartment.
 var dg1wg2 = dg1.makeDebuggeeValue(g2);
-assertEq(dg1wg2.global, dg1);
 assertEq(dg1wg2.unwrap(), dg2);
 assertThrowsInstanceOf(function () { dg1wg2.executeInGlobal('1'); }, TypeError);
 assertThrowsInstanceOf(function () { dg1wg2.executeInGlobalWithBindings('x', { x: 1 }); }, TypeError);
 
 // These, however, should not throw.
 assertEq(dg1wg2.unwrap().executeInGlobal('1729').return, 1729);
 assertEq(dg1wg2.unwrap().executeInGlobalWithBindings('x', { x: 1729 }).return, 1729);
diff --git a/js/src/jit-test/tests/debug/Object-global-01.js b/js/src/jit-test/tests/debug/Object-global-01.js
deleted file mode 100644
--- a/js/src/jit-test/tests/debug/Object-global-01.js
+++ /dev/null
@@ -1,26 +0,0 @@
-// Debugger.Object.prototype.global accessor surfaces.
-
-load(libdir + 'asserts.js');
-
-var dbg = new Debugger;
-var g = newGlobal();
-var gw = dbg.addDebuggee(g);
-
-assertEq(Object.getOwnPropertyDescriptor(gw, 'global'), undefined);
-var d = Object.getOwnPropertyDescriptor(Object.getPrototypeOf(gw), 'global');
-assertEq(d.enumerable, false);
-assertEq(d.configurable, true);
-assertEq(typeof d.get, "function");
-assertEq(d.get.length, 0);
-assertEq(d.set, undefined);
-
-// This should not throw.
-gw.global = '';
-
-// This should throw.
-assertThrowsInstanceOf(function () { "use strict"; gw.global = {}; }, TypeError);
-assertEq(gw.global, gw);
-
-// You shouldn't be able to apply the accessor to the prototype.
-assertThrowsInstanceOf(function () { return Debugger.Object.prototype.global; },
-                       TypeError);
diff --git a/js/src/jit-test/tests/debug/Object-global-02.js b/js/src/jit-test/tests/debug/Object-global-02.js
deleted file mode 100644
--- a/js/src/jit-test/tests/debug/Object-global-02.js
+++ /dev/null
@@ -1,25 +0,0 @@
-// Debugger.Object.prototype.global retrieves the correct global.
-
-var dbg = new Debugger;
-var g1 = newGlobal();
-var g1w = dbg.addDebuggee(g1);
-var g2 = newGlobal();
-var g2w = dbg.addDebuggee(g2);
-
-assertEq(g1w === g2w, false);
-assertEq(g1w.global, g1w);
-assertEq(g2w.global, g2w);
-
-var g1ow = g1w.makeDebuggeeValue(g1.Object());
-var g2ow = g2w.makeDebuggeeValue(g2.Object());
-assertEq(g1ow.global, g1w);
-assertEq(g2ow.global, g2w);
-
-// mild paranoia
-assertEq(g1ow.global === g1ow, false);
-assertEq(g2ow.global === g2ow, false);
-
-// The .global accessor doesn't unwrap.
-assertEq(g1w.makeDebuggeeValue(g2.Object()).global, g1w);
-assertEq(g2w.makeDebuggeeValue(g1.Object()).global, g2w);
-
diff --git a/js/src/jit-test/tests/debug/Object-script-lazy.js b/js/src/jit-test/tests/debug/Object-script-lazy.js
--- a/js/src/jit-test/tests/debug/Object-script-lazy.js
+++ b/js/src/jit-test/tests/debug/Object-script-lazy.js
@@ -23,30 +23,26 @@ g2.f = g1.f;
 g2.g = g1.g;
 g2.h = g1.h;
 
 // At this point, g1.f should still be a lazy function. Unwrapping a D.O
 // referring to g2's CCW of f should yield a D.O referring to f directly.
 // Asking for that second D.O's script should yield null, because it's not
 // a debuggee.
 var fDO = g2w.getOwnPropertyDescriptor('f').value;
-assertEq(fDO.global, g2w);
-assertEq(fDO.unwrap().global === g2w, false);
+assertEq(fDO.unwrap().class, "Function");
 assertEq(fDO.unwrap().script, null);
 
 // Similarly for g1.g, and asking for its parameter names.
 var gDO = g2w.getOwnPropertyDescriptor('g').value;
-assertEq(gDO.global, g2w);
-assertEq(gDO.unwrap().global === g2w, false);
 assertEq(gDO.unwrap().parameterNames, undefined);
 
 // Similarly for g1.h, and asking for its bound function properties.
 var hDO = g2w.getOwnPropertyDescriptor('h').value;
-assertEq(hDO.global, g2w);
-assertEq(hDO.unwrap().global === g2w, false);
+assertEq(hDO.unwrap().class, "Function");
 assertEq(hDO.unwrap().isBoundFunction, undefined);
 assertEq(hDO.unwrap().isArrowFunction, undefined);
 assertEq(hDO.unwrap().boundTargetFunction, undefined);
 assertEq(hDO.unwrap().boundThis, undefined);
 assertEq(hDO.unwrap().boundArguments, undefined);
 
 // Add g1 as a debuggee, and verify that we can get everything.
 dbg.addDebuggee(g1);
diff --git a/js/src/jit-test/tests/debug/Object-unwrap-02.js b/js/src/jit-test/tests/debug/Object-unwrap-02.js
--- a/js/src/jit-test/tests/debug/Object-unwrap-02.js
+++ b/js/src/jit-test/tests/debug/Object-unwrap-02.js
@@ -6,18 +6,16 @@ var dbg = new Debugger();
 var g1 = newGlobal();
 var dg1 = dbg.addDebuggee(g1);
 assertEq(dg1.unwrap(), dg1);
 
 var g2 = newGlobal();
 var dg2 = dbg.addDebuggee(g2);
 
 var dg1g2 = dg1.makeDebuggeeValue(g2);
-assertEq(dg1g2.global, dg1);
 assertEq(dg1g2.unwrap(), dg2);
 
 // Try an ordinary object, not a global.
 var g2o = g2.Object();
 var dg2o = dg2.makeDebuggeeValue(g2o);
 var dg1g2o = dg1.makeDebuggeeValue(g2o);
-assertEq(dg1g2o.global, dg1);
 assertEq(dg1g2o.unwrap(), dg2o);
 assertEq(dg1g2o.unwrap().unwrap(), dg2o);
diff --git a/js/src/jit-test/tests/debug/Script-global-01.js b/js/src/jit-test/tests/debug/Script-global-01.js
--- a/js/src/jit-test/tests/debug/Script-global-01.js
+++ b/js/src/jit-test/tests/debug/Script-global-01.js
@@ -11,10 +11,8 @@ dbg.onDebuggerStatement = function (fram
 }
 
 g.eval('debugger;');
 assertEq(log, 'd');
 
 g.eval('function f() { debugger; }');
 g.f();
 assertEq(log, 'dd');
-
-assertEq(gw.getOwnPropertyDescriptor('f').value.global, gw);
diff --git a/js/src/jit-test/tests/debug/Script-global-02.js b/js/src/jit-test/tests/debug/Script-global-02.js
--- a/js/src/jit-test/tests/debug/Script-global-02.js
+++ b/js/src/jit-test/tests/debug/Script-global-02.js
@@ -29,12 +29,8 @@ g2.eval('function g() { g1.f(); }');
 g3.g2 = g2;
 g3.eval('function h() { g2.g(); }');
 
 g1.g3 = g3;
 g1.eval('function i() { g3.h(); }');
 
 g1.i();
 assertEq(log, 'd');
-
-assertEq(g1w.getOwnPropertyDescriptor('f').value.global, g1w);
-assertEq(g2w.getOwnPropertyDescriptor('g').value.global, g2w);
-assertEq(g3w.getOwnPropertyDescriptor('h').value.global, g3w);
diff --git a/js/src/vm/Debugger.cpp b/js/src/vm/Debugger.cpp
--- a/js/src/vm/Debugger.cpp
+++ b/js/src/vm/Debugger.cpp
@@ -8933,29 +8933,16 @@ DebuggerObject::boundArgumentsGetter(JSC
     if (!obj)
         return false;
 
     args.rval().setObject(*obj);
     return true;
 }
 
 /* static */ bool
-DebuggerObject::globalGetter(JSContext* cx, unsigned argc, Value* vp)
-{
-    THIS_DEBUGOBJECT(cx, argc, vp, "get global", args, object)
-
-    RootedDebuggerObject result(cx);
-    if (!DebuggerObject::getGlobal(cx, object, &result))
-        return false;
-
-    args.rval().setObject(*result);
-    return true;
-}
-
-/* static */ bool
 DebuggerObject::allocationSiteGetter(JSContext* cx, unsigned argc, Value* vp)
 {
     THIS_DEBUGOBJECT(cx, argc, vp, "get allocationSite", args, object)
 
     RootedObject result(cx);
     if (!DebuggerObject::getAllocationSite(cx, object, &result))
         return false;
 
@@ -9641,17 +9628,16 @@ const JSPropertySpec DebuggerObject::pro
     JS_PSG("name", DebuggerObject::nameGetter, 0),
     JS_PSG("displayName", DebuggerObject::displayNameGetter, 0),
     JS_PSG("parameterNames", DebuggerObject::parameterNamesGetter, 0),
     JS_PSG("script", DebuggerObject::scriptGetter, 0),
     JS_PSG("environment", DebuggerObject::environmentGetter, 0),
     JS_PSG("boundTargetFunction", DebuggerObject::boundTargetFunctionGetter, 0),
     JS_PSG("boundThis", DebuggerObject::boundThisGetter, 0),
     JS_PSG("boundArguments", DebuggerObject::boundArgumentsGetter, 0),
-    JS_PSG("global", DebuggerObject::globalGetter, 0),
     JS_PSG("allocationSite", DebuggerObject::allocationSiteGetter, 0),
     JS_PSG("errorMessageName", DebuggerObject::errorMessageNameGetter, 0),
     JS_PSG("errorNotes", DebuggerObject::errorNotesGetter, 0),
     JS_PSG("errorLineNumber", DebuggerObject::errorLineNumberGetter, 0),
     JS_PSG("errorColumnNumber", DebuggerObject::errorColumnNumberGetter, 0),
     JS_PSG("isProxy", DebuggerObject::isProxyGetter, 0),
     JS_PSG("proxyTarget", DebuggerObject::proxyTargetGetter, 0),
     JS_PSG("proxyHandler", DebuggerObject::proxyHandlerGetter, 0),
@@ -9823,27 +9809,16 @@ DebuggerObject::getClassName(JSContext* 
     JSAtom* str = Atomize(cx, className, strlen(className));
     if (!str)
         return false;
 
     result.set(str);
     return true;
 }
 
-/* static */ bool
-DebuggerObject::getGlobal(JSContext* cx, HandleDebuggerObject object,
-                          MutableHandleDebuggerObject result)
-{
-    RootedObject referent(cx, object->referent());
-    Debugger* dbg = object->owner();
-
-    RootedObject global(cx, &referent->deprecatedGlobal());
-    return dbg->wrapDebuggeeObject(cx, global, result);
-}
-
 JSAtom*
 DebuggerObject::name(JSContext* cx) const
 {
     MOZ_ASSERT(isFunction());
 
     JSAtom* atom = referent()->as<JSFunction>().explicitName();
     if (atom)
         cx->markAtom(atom);
diff --git a/js/src/vm/Debugger.h b/js/src/vm/Debugger.h
--- a/js/src/vm/Debugger.h
+++ b/js/src/vm/Debugger.h
@@ -1415,18 +1415,16 @@ class DebuggerObject : public NativeObje
     static NativeObject* initClass(JSContext* cx, Handle<GlobalObject*> global,
                                    HandleObject debugCtor);
     static DebuggerObject* create(JSContext* cx, HandleObject proto, HandleObject obj,
                                   HandleNativeObject debugger);
 
     // Properties
     static MOZ_MUST_USE bool getClassName(JSContext* cx, HandleDebuggerObject object,
                                           MutableHandleString result);
-    static MOZ_MUST_USE bool getGlobal(JSContext* cx, HandleDebuggerObject object,
-                                       MutableHandleDebuggerObject result);
     static MOZ_MUST_USE bool getParameterNames(JSContext* cx, HandleDebuggerObject object,
                                                MutableHandle<StringVector> result);
     static MOZ_MUST_USE bool getBoundTargetFunction(JSContext* cx, HandleDebuggerObject object,
                                                  MutableHandleDebuggerObject result);
     static MOZ_MUST_USE bool getBoundThis(JSContext* cx, HandleDebuggerObject object,
                                           MutableHandleValue result);
     static MOZ_MUST_USE bool getBoundArguments(JSContext* cx, HandleDebuggerObject object,
                                                MutableHandle<ValueVector> result);
@@ -1543,17 +1541,16 @@ class DebuggerObject : public NativeObje
     static MOZ_MUST_USE bool nameGetter(JSContext* cx, unsigned argc, Value* vp);
     static MOZ_MUST_USE bool displayNameGetter(JSContext* cx, unsigned argc, Value* vp);
     static MOZ_MUST_USE bool parameterNamesGetter(JSContext* cx, unsigned argc, Value* vp);
     static MOZ_MUST_USE bool scriptGetter(JSContext* cx, unsigned argc, Value* vp);
     static MOZ_MUST_USE bool environmentGetter(JSContext* cx, unsigned argc, Value* vp);
     static MOZ_MUST_USE bool boundTargetFunctionGetter(JSContext* cx, unsigned argc, Value* vp);
     static MOZ_MUST_USE bool boundThisGetter(JSContext* cx, unsigned argc, Value* vp);
     static MOZ_MUST_USE bool boundArgumentsGetter(JSContext* cx, unsigned argc, Value* vp);
-    static MOZ_MUST_USE bool globalGetter(JSContext* cx, unsigned argc, Value* vp);
     static MOZ_MUST_USE bool allocationSiteGetter(JSContext* cx, unsigned argc, Value* vp);
     static MOZ_MUST_USE bool errorMessageNameGetter(JSContext* cx, unsigned argc, Value* vp);
     static MOZ_MUST_USE bool errorNotesGetter(JSContext* cx, unsigned argc, Value* vp);
     static MOZ_MUST_USE bool errorLineNumberGetter(JSContext* cx, unsigned argc, Value* vp);
     static MOZ_MUST_USE bool errorColumnNumberGetter(JSContext* cx, unsigned argc, Value* vp);
     static MOZ_MUST_USE bool isProxyGetter(JSContext* cx, unsigned argc, Value* vp);
     static MOZ_MUST_USE bool proxyTargetGetter(JSContext* cx, unsigned argc, Value* vp);
     static MOZ_MUST_USE bool proxyHandlerGetter(JSContext* cx, unsigned argc, Value* vp);
