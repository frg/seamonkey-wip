# HG changeset patch
# User Mats Palmgren <mats@mozilla.com>
# Date 1521150085 -3600
#      Thu Mar 15 22:41:25 2018 +0100
# Node ID 5b212971662e4a7fa4d5aa5300994e949527d489
# Parent  bcc15578356141883ac267f12a224522302c3ce8
Bug 1425599 part 14 - [css-grid] Use iterators instead of an array + start/end index for the item data (idempotent change).  r=dholbert

diff --git a/layout/generic/nsGridContainerFrame.cpp b/layout/generic/nsGridContainerFrame.cpp
--- a/layout/generic/nsGridContainerFrame.cpp
+++ b/layout/generic/nsGridContainerFrame.cpp
@@ -1180,23 +1180,22 @@ struct nsGridContainerFrame::Tracks
     }
   };
 
   using FitContentClamper =
     std::function<bool(uint32_t aTrack, nscoord aMinSize, nscoord* aSize)>;
 
   // Helper method for ResolveIntrinsicSize.
   template<TrackSizingPhase phase>
-  bool GrowSizeForSpanningItems(const nsTArray<Step2ItemData>& aItemData,
+  bool GrowSizeForSpanningItems(nsTArray<Step2ItemData>::iterator aIter,
+                                const nsTArray<Step2ItemData>::iterator aEnd,
                                 nsTArray<uint32_t>& aTracks,
                                 nsTArray<TrackSize>& aPlan,
                                 nsTArray<TrackSize>& aItemPlan,
                                 TrackSize::StateBits aSelector,
-                                uint32_t aStartIndex,
-                                uint32_t aEndIndex,
                                 const FitContentClamper& aClamper = nullptr,
                                 bool aNeedInfinitelyGrowableFlag = false);
   /**
    * Resolve Intrinsic Track Sizes.
    * http://dev.w3.org/csswg/css-grid/#algo-content
    */
   void ResolveIntrinsicSize(GridReflowInput&            aState,
                             nsTArray<GridItemInfo>&     aGridItems,
@@ -4243,33 +4242,32 @@ nsGridContainerFrame::Tracks::AlignBasel
     default:
       MOZ_ASSERT_UNREACHABLE("unexpected baseline subtree alignment");
   }
 }
 
 template<nsGridContainerFrame::Tracks::TrackSizingPhase phase>
 bool
 nsGridContainerFrame::Tracks::GrowSizeForSpanningItems(
-  const nsTArray<Step2ItemData>& aItemData,
+  nsTArray<Step2ItemData>::iterator aIter,
+  const nsTArray<Step2ItemData>::iterator aIterEnd,
   nsTArray<uint32_t>& aTracks,
   nsTArray<TrackSize>& aPlan,
   nsTArray<TrackSize>& aItemPlan,
   TrackSize::StateBits aSelector,
-  uint32_t aStartIndex,
-  uint32_t aEndIndex,
   const FitContentClamper& aFitContentClamper,
   bool aNeedInfinitelyGrowableFlag)
 {
   constexpr bool isMaxSizingPhase =
     phase == TrackSizingPhase::eIntrinsicMaximums ||
     phase == TrackSizingPhase::eMaxContentMaximums;
   bool needToUpdateSizes = false;
   InitializePlan<phase>(aPlan);
-  for (uint32_t i = aStartIndex; i < aEndIndex; ++i) {
-    const Step2ItemData& item = aItemData[i];
+  for (; aIter != aIterEnd; ++aIter) {
+    const Step2ItemData& item = *aIter;
     if (!(item.mState & aSelector)) {
       continue;
     }
     if (isMaxSizingPhase) {
       for (auto j = item.mLineRange.mStart, end = item.mLineRange.mEnd; j < end; ++j) {
         aPlan[j].mState |= TrackSize::eModified;
       }
     }
@@ -4430,85 +4428,80 @@ nsGridContainerFrame::Tracks::ResolveInt
     std::stable_sort(step2Items.begin(), step2Items.end(),
                      Step2ItemData::IsSpanLessThan);
 
     nsTArray<uint32_t> tracks(maxSpan);
     nsTArray<TrackSize> plan(mSizes.Length());
     plan.SetLength(mSizes.Length());
     nsTArray<TrackSize> itemPlan(mSizes.Length());
     itemPlan.SetLength(mSizes.Length());
-    for (uint32_t i = 0, len = step2Items.Length(); i < len; ) {
-      // Start / end index for items of the same span length:
-      const uint32_t spanGroupStartIndex = i;
-      uint32_t spanGroupEndIndex = len;
-      const uint32_t span = step2Items[i].mSpan;
-      for (++i; i < len; ++i) {
-        if (step2Items[i].mSpan != span) {
-          spanGroupEndIndex = i;
-          break;
-        }
+    // Start / end iterator for items of the same span length:
+    auto spanGroupStart = step2Items.begin();
+    auto spanGroupEnd = spanGroupStart;
+    const auto end = step2Items.end();
+    for (; spanGroupStart != end; spanGroupStart = spanGroupEnd) {
+      while (spanGroupEnd != end &&
+             !Step2ItemData::IsSpanLessThan(*spanGroupStart, *spanGroupEnd)) {
+        ++spanGroupEnd;
       }
 
+      const uint32_t span = spanGroupStart->mSpan;
       bool updatedBase = false; // Did we update any mBase in step 2.1 - 2.3?
       TrackSize::StateBits selector(TrackSize::eIntrinsicMinSizing);
       if (stateBitsPerSpan[span] & selector) {
         // Step 2.1 MinSize to intrinsic min-sizing.
         updatedBase =
           GrowSizeForSpanningItems<TrackSizingPhase::eIntrinsicMinimums>(
-            step2Items, tracks, plan, itemPlan, selector,
-            spanGroupStartIndex, spanGroupEndIndex);
+            spanGroupStart, spanGroupEnd, tracks, plan, itemPlan, selector);
       }
 
       selector = contentBasedMinSelector;
       if (stateBitsPerSpan[span] & selector) {
         // Step 2.2 MinContentContribution to min-/max-content (and 'auto' when
         // sizing under a min-content constraint) min-sizing.
         updatedBase |=
           GrowSizeForSpanningItems<TrackSizingPhase::eContentBasedMinimums>(
-            step2Items, tracks, plan, itemPlan, selector,
-            spanGroupStartIndex, spanGroupEndIndex);
+            spanGroupStart, spanGroupEnd, tracks, plan, itemPlan, selector);
       }
 
       selector = maxContentMinSelector;
       if (stateBitsPerSpan[span] & selector) {
         // Step 2.3 MaxContentContribution to max-content (and 'auto' when
         // sizing under a max-content constraint) min-sizing.
         updatedBase |=
           GrowSizeForSpanningItems<TrackSizingPhase::eMaxContentMinimums>(
-            step2Items, tracks, plan, itemPlan, selector,
-            spanGroupStartIndex, spanGroupEndIndex);
+            spanGroupStart, spanGroupEnd, tracks, plan, itemPlan, selector);
       }
 
       if (updatedBase) {
         // Step 2.4
         for (TrackSize& sz : mSizes) {
           if (sz.mBase > sz.mLimit) {
             sz.mLimit = sz.mBase;
           }
         }
       }
 
-      if (stateBitsPerSpan[span] & TrackSize::eIntrinsicMaxSizing) {
+      selector = TrackSize::eIntrinsicMaxSizing;
+      if (stateBitsPerSpan[span] & selector) {
         const bool willRunStep2_6 =
           stateBitsPerSpan[span] & TrackSize::eAutoOrMaxContentMaxSizing;
         // Step 2.5 MinSize to intrinsic max-sizing.
         GrowSizeForSpanningItems<TrackSizingPhase::eIntrinsicMaximums>(
-          step2Items, tracks, plan, itemPlan, TrackSize::eIntrinsicMaxSizing,
-          spanGroupStartIndex, spanGroupEndIndex, fitContentClamper,
-          willRunStep2_6);
+          spanGroupStart, spanGroupEnd, tracks, plan, itemPlan, selector,
+          fitContentClamper, willRunStep2_6);
 
         if (willRunStep2_6) {
           // Step 2.6 MaxContentContribution to max-content max-sizing.
+          selector = TrackSize::eAutoOrMaxContentMaxSizing;
           GrowSizeForSpanningItems<TrackSizingPhase::eMaxContentMaximums>(
-            step2Items, tracks, plan, itemPlan, TrackSize::eAutoOrMaxContentMaxSizing,
-            spanGroupStartIndex, spanGroupEndIndex, fitContentClamper);
+            spanGroupStart, spanGroupEnd, tracks, plan, itemPlan, selector,
+            fitContentClamper);
         }
       }
-
-      i = spanGroupEndIndex;
     }
   }
 
   // Step 3.
   for (TrackSize& sz : mSizes) {
     if (sz.mLimit == NS_UNCONSTRAINEDSIZE) {
       sz.mLimit = sz.mBase;
     }
