# HG changeset patch
# User Maeris Fogels <mars@mozilla.com>
# Date 1574349217 0
# Node ID a668eeee983067979ed361b58df91b1a7ad5a93c
# Parent  8fc18309cb5151ca000ea6d46cb54733cd5a5754
Bug 1593105 - [mozbuild] Port mozbuild.util.FileAvoidWrite to Python 3 r=firefox-build-system-reviewers,mshal

The FileAvoidWrite class does a bunch of stuff with bytes and strings
that doesn't work in Python 3.  Make sure the object is handling only
bytes or only strings under the hood so Python 3 is happy.

The FileAvoidWrite unit tests written with MockedOpen don't work in
Python 3 either.  Swap them out for vanilla pytest tests without
the MockedOpen dependency.

Differential Revision: https://phabricator.services.mozilla.com/D51341

diff --git a/python/mozbuild/mozbuild/test/python.ini b/python/mozbuild/mozbuild/test/python.ini
--- a/python/mozbuild/mozbuild/test/python.ini
+++ b/python/mozbuild/mozbuild/test/python.ini
@@ -1,6 +1,7 @@
 [DEFAULT]
 subsuite = mozbuild
 
 [test_expression.py]
 [test_licenses.py]
 [test_pythonutil.py]
+[test_util_fileavoidwrite.py]
diff --git a/python/mozbuild/mozbuild/test/test_util.py b/python/mozbuild/mozbuild/test/test_util.py
--- a/python/mozbuild/mozbuild/test/test_util.py
+++ b/python/mozbuild/mozbuild/test/test_util.py
@@ -4,31 +4,25 @@
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import, print_function, unicode_literals
 
 import itertools
 import hashlib
 import os
 import unittest
-import shutil
 import string
 import sys
-import tempfile
 import textwrap
 
 from mozfile.mozfile import NamedTemporaryFile
-from mozunit import (
-    main,
-    MockedOpen,
-)
+from mozunit import main
 
 from mozbuild.util import (
     expand_variables,
-    FileAvoidWrite,
     group_unified_files,
     hash_file,
     indented_repr,
     memoize,
     memoized_property,
     pair,
     resolve_target_to_make,
     MozbuildDeletionError,
@@ -78,96 +72,16 @@ class TestHashing(unittest.TestCase):
         temp.write(data)
         temp.flush()
 
         actual = hash_file(temp.name)
 
         self.assertEqual(actual, expected)
 
 
-class TestFileAvoidWrite(unittest.TestCase):
-    def test_file_avoid_write(self):
-        with MockedOpen({'file': 'content'}):
-            # Overwriting an existing file replaces its content
-            faw = FileAvoidWrite('file')
-            faw.write('bazqux')
-            self.assertEqual(faw.close(), (True, True))
-            self.assertEqual(open('file', 'r').read(), 'bazqux')
-
-            # Creating a new file (obviously) stores its content
-            faw = FileAvoidWrite('file2')
-            faw.write('content')
-            self.assertEqual(faw.close(), (False, True))
-            self.assertEqual(open('file2').read(), 'content')
-
-        with MockedOpen({'file': 'content'}):
-            with FileAvoidWrite('file') as file:
-                file.write('foobar')
-
-            self.assertEqual(open('file', 'r').read(), 'foobar')
-
-        class MyMockedOpen(MockedOpen):
-            '''MockedOpen extension to raise an exception if something
-            attempts to write in an opened file.
-            '''
-
-            def __call__(self, name, mode):
-                if 'w' in mode:
-                    raise Exception('Unexpected open with write mode')
-                return MockedOpen.__call__(self, name, mode)
-
-        with MyMockedOpen({'file': 'content'}):
-            # Validate that MyMockedOpen works as intended
-            file = FileAvoidWrite('file')
-            file.write('foobar')
-            self.assertRaises(Exception, file.close)
-
-            # Check that no write actually happens when writing the
-            # same content as what already is in the file
-            faw = FileAvoidWrite('file')
-            faw.write('content')
-            self.assertEqual(faw.close(), (True, False))
-
-    def test_diff_not_default(self):
-        """Diffs are not produced by default."""
-
-        with MockedOpen({'file': 'old'}):
-            faw = FileAvoidWrite('file')
-            faw.write('dummy')
-            faw.close()
-            self.assertIsNone(faw.diff)
-
-    def test_diff_update(self):
-        """Diffs are produced on file update."""
-
-        with MockedOpen({'file': 'old'}):
-            faw = FileAvoidWrite('file', capture_diff=True)
-            faw.write('new')
-            faw.close()
-
-            diff = '\n'.join(faw.diff)
-            self.assertIn('-old', diff)
-            self.assertIn('+new', diff)
-
-    def test_diff_create(self):
-        """Diffs are produced when files are created."""
-
-        tmpdir = tempfile.mkdtemp()
-        try:
-            path = os.path.join(tmpdir, 'file')
-            faw = FileAvoidWrite(path, capture_diff=True)
-            faw.write('new')
-            faw.close()
-
-            diff = '\n'.join(faw.diff)
-            self.assertIn('+new', diff)
-        finally:
-            shutil.rmtree(tmpdir)
-
-
 class TestResolveTargetToMake(unittest.TestCase):
     def setUp(self):
         self.topobjdir = data_path
 
     def assertResolve(self, path, expected):
         # Handle Windows path separators.
         (reldir, target) = resolve_target_to_make(self.topobjdir, path)
         if reldir is not None:
diff --git a/python/mozbuild/mozbuild/test/test_util_fileavoidwrite.py b/python/mozbuild/mozbuild/test/test_util_fileavoidwrite.py
new file mode 100644
--- /dev/null
+++ b/python/mozbuild/mozbuild/test/test_util_fileavoidwrite.py
@@ -0,0 +1,109 @@
+# This Source Code Form is subject to the terms of the Mozilla Public
+# License, v. 2.0. If a copy of the MPL was not distributed with this
+# file, You can obtain one at http://mozilla.org/MPL/2.0/.
+from __future__ import absolute_import, print_function, unicode_literals
+"""Tests for the FileAvoidWrite object."""
+
+import locale
+import pytest
+import pathlib2
+from mozbuild.util import FileAvoidWrite
+from mozunit import main
+
+
+@pytest.fixture
+def tmp_path(tmpdir):
+    """Backport of the tmp_path fixture from pytest 3.9.1."""
+    return pathlib2.Path(str(tmpdir))
+
+
+def test_overwrite_contents(tmp_path):
+    file = tmp_path / "file.txt"
+    file.write_text("abc")
+
+    faw = FileAvoidWrite(str(file))
+    faw.write("bazqux")
+
+    assert faw.close() == (True, True)
+    assert file.read_text() == "bazqux"
+
+
+def test_store_new_contents(tmp_path):
+    file = tmp_path / "file.txt"
+
+    faw = FileAvoidWrite(str(file))
+    faw.write("content")
+
+    assert faw.close() == (False, True)
+    assert file.read_text() == "content"
+
+
+def test_change_binary_file_contents(tmp_path):
+    file = tmp_path / "file.dat"
+    file.write_bytes(b"\0")
+
+    faw = FileAvoidWrite(str(file), readmode="rb")
+    faw.write(b"\0\0\0")
+
+    assert faw.close() == (True, True)
+    assert file.read_bytes() == b"\0\0\0"
+
+
+def test_obj_as_context_manager(tmp_path):
+    file = tmp_path / "file.txt"
+
+    with FileAvoidWrite(str(file)) as fh:
+        fh.write("foobar")
+
+    assert file.read_text() == "foobar"
+
+
+def test_no_write_happens_if_file_contents_same(tmp_path):
+    file = tmp_path / "file.txt"
+    file.write_text("content")
+    original_write_time = file.stat().st_mtime
+
+    faw = FileAvoidWrite(str(file))
+    faw.write("content")
+
+    assert faw.close() == (True, False)
+    assert file.stat().st_mtime == original_write_time
+
+
+def test_diff_not_created_by_default(tmp_path):
+    file = tmp_path / "file.txt"
+    faw = FileAvoidWrite(str(file))
+    faw.write("dummy")
+    faw.close()
+    assert faw.diff is None
+
+
+def test_diff_update(tmp_path):
+    file = tmp_path / "diffable.txt"
+    file.write_text("old")
+
+    faw = FileAvoidWrite(str(file), capture_diff=True)
+    faw.write("new")
+    faw.close()
+
+    diff = "\n".join(faw.diff)
+    assert "-old" in diff
+    assert "+new" in diff
+
+
+@pytest.mark.skipif(
+    locale.getdefaultlocale()[1] == "cp1252",
+    reason="Fails on win32 terminals with cp1252 encoding",
+)
+def test_write_unicode(tmp_path):
+    # Unicode grinning face :D
+    binary_emoji = b"\xf0\x9f\x98\x80"
+
+    file = tmp_path / "file.dat"
+    faw = FileAvoidWrite(str(file))
+    faw.write(binary_emoji)
+    faw.close()
+
+
+if __name__ == "__main__":
+    main()
diff --git a/python/mozbuild/mozbuild/util.py b/python/mozbuild/mozbuild/util.py
--- a/python/mozbuild/mozbuild/util.py
+++ b/python/mozbuild/mozbuild/util.py
@@ -1,40 +1,36 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this file,
 # You can obtain one at http://mozilla.org/MPL/2.0/.
 
 # This file contains miscellaneous utility functions that don't belong anywhere
 # in particular.
 
-from __future__ import absolute_import, unicode_literals, print_function
+from __future__ import absolute_import, print_function, unicode_literals
 
 import argparse
 import collections
 import ctypes
 import difflib
 import errno
 import functools
 import hashlib
 import itertools
 import os
 import re
-import six
 import stat
 import sys
 import time
-
 from collections import (
     OrderedDict,
 )
-from io import (
-    StringIO,
-    BytesIO,
-)
+from io import (BytesIO, StringIO)
 
+import six
 
 if sys.platform == 'win32':
     _kernel32 = ctypes.windll.kernel32
     _FILE_ATTRIBUTE_NOT_CONTENT_INDEXED = 0x2000
     system_encoding = 'mbcs'
 else:
     system_encoding = 'utf-8'
 
@@ -221,16 +217,17 @@ class FileAvoidWrite(BytesIO):
         self.name = filename
         assert type(capture_diff) == bool
         assert type(dry_run) == bool
         assert 'r' in readmode
         self._capture_diff = capture_diff
         self._write_to_file = not dry_run
         self.diff = None
         self.mode = readmode
+        self._binary_mode = 'b' in readmode
 
     def write(self, buf):
         if isinstance(buf, six.text_type):
             buf = buf.encode('utf-8')
         BytesIO.write(self, buf)
 
     def avoid_writing_to_file(self):
         self._write_to_file = False
@@ -241,17 +238,27 @@ class FileAvoidWrite(BytesIO):
         Returns a tuple of bools indicating what action was performed:
 
             (file existed, file updated)
 
         If ``capture_diff`` was specified at construction time and the
         underlying file was changed, ``.diff`` will be populated with the diff
         of the result.
         """
-        buf = self.getvalue()
+        if self._binary_mode or six.PY2:
+            # Use binary data under Python 2 because it can be written to files
+            # opened with either open(mode='w') or open(mode='wb') without raising
+            # unicode errors. Also use binary data if the caller explicitly asked for
+            # it.
+            buf = self.getvalue()
+        else:
+            # Use strings in Python 3 unless the caller explicitly asked for binary
+            # data.
+            buf = self.getvalue().decode('utf-8')
+
         BytesIO.close(self)
         existed = False
         old_content = None
 
         try:
             existing = open(self.name, self.mode)
             existed = True
         except IOError:
@@ -266,39 +273,66 @@ class FileAvoidWrite(BytesIO):
             finally:
                 existing.close()
 
         if self._write_to_file:
             ensureParentDir(self.name)
             # Maintain 'b' if specified.  'U' only applies to modes starting with
             # 'r', so it is dropped.
             writemode = 'w'
-            if 'b' in self.mode:
+            if self._binary_mode:
                 writemode += 'b'
             with open(self.name, writemode) as file:
                 file.write(buf)
 
-        if self._capture_diff:
-            try:
-                old_lines = old_content.splitlines() if existed else None
-                new_lines = buf.splitlines()
-
-                self.diff = simple_diff(self.name, old_lines, new_lines)
-            # FileAvoidWrite isn't unicode/bytes safe. So, files with non-ascii
-            # content or opened and written in different modes may involve
-            # implicit conversion and this will make Python unhappy. Since
-            # diffing isn't a critical feature, we just ignore the failure.
-            # This can go away once FileAvoidWrite uses io.BytesIO and
-            # io.StringIO. But that will require a lot of work.
-            except (UnicodeDecodeError, UnicodeEncodeError):
-                self.diff = ['Binary or non-ascii file changed: %s' %
-                             self.name]
+        self._generate_diff(buf, old_content)
 
         return existed, True
 
+    def _generate_diff(self, new_content, old_content):
+        """Generate a diff for the changed contents if `capture_diff` is True.
+
+        If the changed contents could not be decoded as utf-8 then generate a
+        placeholder message instead of a diff.
+
+        Args:
+            new_content: Str or bytes holding the new file contents.
+            old_content: Str or bytes holding the original file contents. Should be
+                None if no old content is being overwritten.
+        """
+        if not self._capture_diff:
+            return
+
+        try:
+            if old_content is None:
+                old_lines = None
+            else:
+                if self._binary_mode:
+                    # difflib doesn't work with bytes.
+                    old_content = old_content.decode('utf-8')
+
+                old_lines = old_content.splitlines()
+
+            if self._binary_mode:
+                # difflib doesn't work with bytes.
+                new_content = new_content.decode('utf-8')
+
+            new_lines = new_content.splitlines()
+
+            self.diff = simple_diff(self.name, old_lines, new_lines)
+        # FileAvoidWrite isn't unicode/bytes safe. So, files with non-ascii
+        # content or opened and written in different modes may involve
+        # implicit conversion and this will make Python unhappy. Since
+        # diffing isn't a critical feature, we just ignore the failure.
+        # This can go away once FileAvoidWrite uses io.BytesIO and
+        # io.StringIO. But that will require a lot of work.
+        except (UnicodeDecodeError, UnicodeEncodeError):
+            self.diff = ['Binary or non-ascii file changed: %s' %
+                         self.name]
+
     def __enter__(self):
         return self
 
     def __exit__(self, type, value, traceback):
         if not self.closed:
             self.close()
 
 
