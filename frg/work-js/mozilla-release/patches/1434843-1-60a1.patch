# HG changeset patch
# User Dragan Mladjenovic <dragan.mladjenovic>
# Date 1517474986 -3600
#      Thu Feb 01 09:49:46 2018 +0100
# Node ID ef2229b002f9a71c1a9fb6bee9eb7e212da13cf2
# Parent  5c10e3f4e008003c4d5767c1084642be92c45ff2
Bug 1434843 : [MIPS] Remove SecondScratchDoubleReg; r=yuyin

diff --git a/js/src/jit/mips32/Architecture-mips32.h b/js/src/jit/mips32/Architecture-mips32.h
--- a/js/src/jit/mips32/Architecture-mips32.h
+++ b/js/src/jit/mips32/Architecture-mips32.h
@@ -64,17 +64,17 @@ class FloatRegisters : public FloatRegis
     }
 
     static Encoding FromName(const char* name);
 
     static const uint32_t Total = 32;
     static const uint32_t TotalDouble = 16;
     static const uint32_t TotalSingle = 16;
 
-    static const uint32_t Allocatable = 28;
+    static const uint32_t Allocatable = 30;
     static const SetType AllSingleMask = (1ULL << TotalSingle) - 1;
 
     static const SetType AllDoubleMask = ((1ULL << TotalDouble) - 1) << TotalSingle;
     static const SetType AllMask = AllDoubleMask | AllSingleMask;
 
     // When saving all registers we only need to do is save double registers.
     static const uint32_t TotalPhys = 16;
     static const uint32_t RegisterIdLimit = 32;
@@ -90,18 +90,17 @@ class FloatRegisters : public FloatRegis
          (SetType(1) << (FloatRegisters::f28 >> 1)) |
          (SetType(1) << (FloatRegisters::f30 >> 1))) * ((1 << TotalSingle) + 1);
 
     static const SetType VolatileMask = AllMask & ~NonVolatileMask;
 
     static const SetType WrapperMask = VolatileMask;
 
     static const SetType NonAllocatableMask =
-        ((SetType(1) << (FloatRegisters::f16 >> 1)) |
-         (SetType(1) << (FloatRegisters::f18 >> 1))) * ((1 << TotalSingle) + 1);
+        (SetType(1) << (FloatRegisters::f18 >> 1)) * ((1 << TotalSingle) + 1);
 
     static const SetType AllocatableMask = AllMask & ~NonAllocatableMask;
 };
 
 class FloatRegister : public FloatRegisterMIPSShared
 {
   public:
     enum RegType {
diff --git a/js/src/jit/mips32/Assembler-mips32.h b/js/src/jit/mips32/Assembler-mips32.h
--- a/js/src/jit/mips32/Assembler-mips32.h
+++ b/js/src/jit/mips32/Assembler-mips32.h
@@ -78,18 +78,16 @@ static constexpr Register WasmTableCallI
 
 static constexpr Register JSReturnReg_Type = a3;
 static constexpr Register JSReturnReg_Data = a2;
 static constexpr Register64 ReturnReg64(v1, v0);
 static constexpr FloatRegister ReturnFloat32Reg = { FloatRegisters::f0, FloatRegister::Single };
 static constexpr FloatRegister ReturnDoubleReg = { FloatRegisters::f0, FloatRegister::Double };
 static constexpr FloatRegister ScratchFloat32Reg = { FloatRegisters::f18, FloatRegister::Single };
 static constexpr FloatRegister ScratchDoubleReg = { FloatRegisters::f18, FloatRegister::Double };
-static constexpr FloatRegister SecondScratchFloat32Reg = { FloatRegisters::f16, FloatRegister::Single };
-static constexpr FloatRegister SecondScratchDoubleReg = { FloatRegisters::f16, FloatRegister::Double };
 
 struct ScratchFloat32Scope : public AutoFloatRegisterScope
 {
     explicit ScratchFloat32Scope(MacroAssembler& masm)
       : AutoFloatRegisterScope(masm, ScratchFloat32Reg)
     { }
 };
 
diff --git a/js/src/jit/mips32/MacroAssembler-mips32.cpp b/js/src/jit/mips32/MacroAssembler-mips32.cpp
--- a/js/src/jit/mips32/MacroAssembler-mips32.cpp
+++ b/js/src/jit/mips32/MacroAssembler-mips32.cpp
@@ -57,42 +57,57 @@ MacroAssemblerMIPSCompat::convertInt32To
 {
     computeScaledAddress(src, ScratchRegister);
     convertInt32ToDouble(Address(ScratchRegister, src.offset), dest);
 }
 
 void
 MacroAssemblerMIPSCompat::convertUInt32ToDouble(Register src, FloatRegister dest)
 {
-    // We use SecondScratchDoubleReg because MacroAssembler::loadFromTypedArray
-    // calls with ScratchDoubleReg as dest.
-    MOZ_ASSERT(dest != SecondScratchDoubleReg);
-
-    // Subtract INT32_MIN to get a positive number
-    ma_subu(ScratchRegister, src, Imm32(INT32_MIN));
-
-    // Convert value
-    as_mtc1(ScratchRegister, dest);
-    as_cvtdw(dest, dest);
-
-    // Add unsigned value of INT32_MIN
-    ma_lid(SecondScratchDoubleReg, 2147483648.0);
-    as_addd(dest, dest, SecondScratchDoubleReg);
+    Label positive, done;
+    ma_b(src, src, &positive, NotSigned, ShortJump);
+
+    const uint32_t kExponentShift = mozilla::FloatingPoint<double>::kExponentShift - 32;
+    const uint32_t kExponent = (31 + mozilla::FloatingPoint<double>::kExponentBias);
+
+    ma_ext(SecondScratchReg, src, 31 - kExponentShift, kExponentShift);
+    ma_li(ScratchRegister, Imm32(kExponent << kExponentShift));
+    ma_or(SecondScratchReg, ScratchRegister);
+    ma_sll(ScratchRegister, src, Imm32(kExponentShift + 1));
+    moveToDoubleHi(SecondScratchReg, dest);
+    moveToDoubleLo(ScratchRegister, dest);
+
+    ma_b(&done, ShortJump);
+
+    bind(&positive);
+    convertInt32ToDouble(src, dest);
+
+    bind(&done);
+
 }
 
 void
 MacroAssemblerMIPSCompat::convertUInt32ToFloat32(Register src, FloatRegister dest)
 {
     Label positive, done;
     ma_b(src, src, &positive, NotSigned, ShortJump);
 
-    // We cannot do the same as convertUInt32ToDouble because float32 doesn't
-    // have enough precision.
-    convertUInt32ToDouble(src, dest);
-    convertDoubleToFloat32(dest, dest);
+    const uint32_t kExponentShift = mozilla::FloatingPoint<double>::kExponentShift - 32;
+    const uint32_t kExponent = (31 + mozilla::FloatingPoint<double>::kExponentBias);
+
+    ma_ext(SecondScratchReg, src, 31 - kExponentShift, kExponentShift);
+    ma_li(ScratchRegister, Imm32(kExponent << kExponentShift));
+    ma_or(SecondScratchReg, ScratchRegister);
+    ma_sll(ScratchRegister, src, Imm32(kExponentShift + 1));
+    FloatRegister destDouble = dest.asDouble();
+    moveToDoubleHi(SecondScratchReg, destDouble);
+    moveToDoubleLo(ScratchRegister, destDouble);
+
+    convertDoubleToFloat32(destDouble, dest);
+
     ma_b(&done, ShortJump);
 
     bind(&positive);
     convertInt32ToFloat32(src, dest);
 
     bind(&done);
 }
 
diff --git a/js/src/jit/mips64/Architecture-mips64.h b/js/src/jit/mips64/Architecture-mips64.h
--- a/js/src/jit/mips64/Architecture-mips64.h
+++ b/js/src/jit/mips64/Architecture-mips64.h
@@ -36,17 +36,17 @@ class FloatRegisters : public FloatRegis
     static const char* GetName(uint32_t i) {
         MOZ_ASSERT(i < TotalPhys);
         return FloatRegistersMIPSShared::GetName(Encoding(i));
     }
 
     static Encoding FromName(const char* name);
 
     static const uint32_t Total = 32 * NumTypes;
-    static const uint32_t Allocatable = 60;
+    static const uint32_t Allocatable = 62;
     // When saving all registers we only need to do is save double registers.
     static const uint32_t TotalPhys = 32;
 
     static_assert(sizeof(SetType) * 8 >= Total,
                   "SetType should be large enough to enumerate all registers.");
 
     // Magic values which are used to duplicate a mask of physical register for
     // a specific type of register. A multiplication is used to copy and shift
@@ -74,20 +74,17 @@ class FloatRegisters : public FloatRegis
         ) * SpreadScalar
         | AllPhysMask * SpreadVector;
 
     static const SetType VolatileMask = AllMask & ~NonVolatileMask;
 
     static const SetType WrapperMask = VolatileMask;
 
     static const SetType NonAllocatableMask =
-        ( // f21 and f23 are MIPS scratch float registers.
-          (1U << FloatRegisters::f21) |
-          (1U << FloatRegisters::f23)
-        ) * Spread;
+        (1U << FloatRegisters::f23) * Spread;
 
     static const SetType AllocatableMask = AllMask & ~NonAllocatableMask;
 };
 
 template <typename T>
 class TypedRegisterSet;
 
 class FloatRegister : public FloatRegisterMIPSShared
diff --git a/js/src/jit/mips64/Assembler-mips64.h b/js/src/jit/mips64/Assembler-mips64.h
--- a/js/src/jit/mips64/Assembler-mips64.h
+++ b/js/src/jit/mips64/Assembler-mips64.h
@@ -72,18 +72,16 @@ static constexpr Register WasmTableCallI
 static constexpr Register JSReturnReg = v1;
 static constexpr Register JSReturnReg_Type = JSReturnReg;
 static constexpr Register JSReturnReg_Data = JSReturnReg;
 static constexpr Register64 ReturnReg64(ReturnReg);
 static constexpr FloatRegister ReturnFloat32Reg = { FloatRegisters::f0, FloatRegisters::Single };
 static constexpr FloatRegister ReturnDoubleReg = { FloatRegisters::f0, FloatRegisters::Double };
 static constexpr FloatRegister ScratchFloat32Reg = { FloatRegisters::f23, FloatRegisters::Single };
 static constexpr FloatRegister ScratchDoubleReg = { FloatRegisters::f23, FloatRegisters::Double };
-static constexpr FloatRegister SecondScratchFloat32Reg = { FloatRegisters::f21, FloatRegisters::Single };
-static constexpr FloatRegister SecondScratchDoubleReg = { FloatRegisters::f21, FloatRegisters::Double };
 
 struct ScratchFloat32Scope : public AutoFloatRegisterScope
 {
     explicit ScratchFloat32Scope(MacroAssembler& masm)
       : AutoFloatRegisterScope(masm, ScratchFloat32Reg)
     { }
 };
 
diff --git a/js/src/jit/mips64/MacroAssembler-mips64.cpp b/js/src/jit/mips64/MacroAssembler-mips64.cpp
--- a/js/src/jit/mips64/MacroAssembler-mips64.cpp
+++ b/js/src/jit/mips64/MacroAssembler-mips64.cpp
@@ -53,30 +53,18 @@ MacroAssemblerMIPS64Compat::convertInt32
 {
     computeScaledAddress(src, ScratchRegister);
     convertInt32ToDouble(Address(ScratchRegister, src.offset), dest);
 }
 
 void
 MacroAssemblerMIPS64Compat::convertUInt32ToDouble(Register src, FloatRegister dest)
 {
-    // We use SecondScratchDoubleReg because MacroAssembler::loadFromTypedArray
-    // calls with ScratchDoubleReg as dest.
-    MOZ_ASSERT(dest != SecondScratchDoubleReg);
-
-    // Subtract INT32_MIN to get a positive number
-    ma_subu(ScratchRegister, src, Imm32(INT32_MIN));
-
-    // Convert value
-    as_mtc1(ScratchRegister, dest);
-    as_cvtdw(dest, dest);
-
-    // Add unsigned value of INT32_MIN
-    ma_lid(SecondScratchDoubleReg, 2147483648.0);
-    as_addd(dest, dest, SecondScratchDoubleReg);
+    ma_dext(ScratchRegister, src, Imm32(0), Imm32(32));
+    asMasm().convertInt64ToDouble(Register64(ScratchRegister), dest);
 }
 
 void
 MacroAssemblerMIPS64Compat::convertUInt64ToDouble(Register src, FloatRegister dest)
 {
     Label positive, done;
     ma_b(src, src, &positive, NotSigned, ShortJump);
 
@@ -96,29 +84,18 @@ MacroAssemblerMIPS64Compat::convertUInt6
     as_cvtdl(dest, dest);
 
     bind(&done);
 }
 
 void
 MacroAssemblerMIPS64Compat::convertUInt32ToFloat32(Register src, FloatRegister dest)
 {
-    Label positive, done;
-    ma_b(src, src, &positive, NotSigned, ShortJump);
-
-    // We cannot do the same as convertUInt32ToDouble because float32 doesn't
-    // have enough precision.
-    convertUInt32ToDouble(src, dest);
-    convertDoubleToFloat32(dest, dest);
-    ma_b(&done, ShortJump);
-
-    bind(&positive);
-    convertInt32ToFloat32(src, dest);
-
-    bind(&done);
+    ma_dext(ScratchRegister, src, Imm32(0), Imm32(32));
+    asMasm().convertInt64ToFloat32(Register64(ScratchRegister), dest);
 }
 
 void
 MacroAssemblerMIPS64Compat::convertDoubleToFloat32(FloatRegister src, FloatRegister dest)
 {
     as_cvtsd(dest, src);
 }
 
