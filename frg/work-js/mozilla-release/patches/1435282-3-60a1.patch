# HG changeset patch
# User Kartikaya Gupta <kgupta@mozilla.com>
# Date 1517586505 18000
# Node ID cf517d879754ca691d8d73273a05d50d5b464b7a
# Parent  e1e692b7fa195c98265cf11826ec7b0e09f09ba6
Bug 1435282 - Don't force the render transaction to happen immediately if we're doing an async flush. r=sotaro

This splits the FlushRendering function into sync and async versions
just for a bit more clarity. In the async version we don't want to set
the mForceRendering flag at all because we don't need to force a
rendering - if there is one already pending then that's good enough. And
anyway in practice the async version seems to only ever be invoked by
CompositorBridgeParent::SetTestSampleTime which I'll be changing in the
next patch.

MozReview-Commit-ID: 4cdU0U5B1pp

diff --git a/gfx/layers/ipc/CompositorBridgeParent.cpp b/gfx/layers/ipc/CompositorBridgeParent.cpp
--- a/gfx/layers/ipc/CompositorBridgeParent.cpp
+++ b/gfx/layers/ipc/CompositorBridgeParent.cpp
@@ -520,32 +520,32 @@ CompositorBridgeParent::RecvWaitOnTransa
 {
   return IPC_OK();
 }
 
 mozilla::ipc::IPCResult
 CompositorBridgeParent::RecvFlushRendering()
 {
   if (mOptions.UseWebRender()) {
-    mWrBridge->FlushRendering(/* aIsSync */ true);
+    mWrBridge->FlushRendering();
     return IPC_OK();
   }
 
   if (mCompositorScheduler->NeedsComposite()) {
     CancelCurrentCompositeTask();
     ForceComposeToTarget(nullptr);
   }
   return IPC_OK();
 }
 
 mozilla::ipc::IPCResult
 CompositorBridgeParent::RecvFlushRenderingAsync()
 {
   if (mOptions.UseWebRender()) {
-    mWrBridge->FlushRendering(/* aIsSync */ false);
+    mWrBridge->FlushRenderingAsync();
     return IPC_OK();
   }
 
   return RecvFlushRendering();
 }
 
 mozilla::ipc::IPCResult
 CompositorBridgeParent::RecvForcePresent()
@@ -1160,17 +1160,17 @@ CompositorBridgeParent::SetTestSampleTim
   if (aTime.IsNull()) {
     return false;
   }
 
   mIsTesting = true;
   mTestTime = aTime;
 
   if (mWrBridge) {
-    mWrBridge->FlushRendering(/*aIsSync*/ false);
+    mWrBridge->FlushRenderingAsync();
     return true;
   }
 
   bool testComposite = mCompositionManager &&
                        mCompositorScheduler->NeedsComposite();
 
   // Update but only if we were already scheduled to animate
   if (testComposite) {
diff --git a/gfx/layers/wr/WebRenderBridgeParent.cpp b/gfx/layers/wr/WebRenderBridgeParent.cpp
--- a/gfx/layers/wr/WebRenderBridgeParent.cpp
+++ b/gfx/layers/wr/WebRenderBridgeParent.cpp
@@ -1329,32 +1329,40 @@ WebRenderBridgeParent::ScheduleGenerateF
 {
   if (mCompositorScheduler) {
     mAsyncImageManager->SetWillGenerateFrame();
     mCompositorScheduler->ScheduleComposition();
   }
 }
 
 void
-WebRenderBridgeParent::FlushRendering(bool aIsSync)
+WebRenderBridgeParent::FlushRendering()
 {
   if (mDestroyed) {
     return;
   }
 
   mForceRendering = true;
   if (mCompositorScheduler->FlushPendingComposite()) {
-    if (aIsSync) {
-      mApi->WaitFlushed();
-    }
+    mApi->WaitFlushed();
   }
   mForceRendering = false;
 }
 
 void
+WebRenderBridgeParent::FlushRenderingAsync()
+{
+  if (mDestroyed) {
+    return;
+  }
+
+  mCompositorScheduler->FlushPendingComposite();
+}
+
+void
 WebRenderBridgeParent::Pause()
 {
   MOZ_ASSERT(mWidget);
 #ifdef MOZ_WIDGET_ANDROID
   if (!mWidget || mDestroyed) {
     return;
   }
   mApi->Pause();
diff --git a/gfx/layers/wr/WebRenderBridgeParent.h b/gfx/layers/wr/WebRenderBridgeParent.h
--- a/gfx/layers/wr/WebRenderBridgeParent.h
+++ b/gfx/layers/wr/WebRenderBridgeParent.h
@@ -167,17 +167,18 @@ public:
   wr::IdNamespace GetIdNamespace()
   {
     return mIdNamespace;
   }
 
   void UpdateAPZ(bool aUpdateHitTestingTree);
   const WebRenderScrollData& GetScrollData() const;
 
-  void FlushRendering(bool aIsSync);
+  void FlushRendering();
+  void FlushRenderingAsync();
 
   /**
    * Schedule generating WebRender frame definitely at next composite timing.
    *
    * WebRenderBridgeParent uses composite timing to check if there is an update
    * to AsyncImagePipelines. If there is no update, WebRenderBridgeParent skips
    * to generate frame. If we need to generate new frame at next composite timing,
    * call this method.
