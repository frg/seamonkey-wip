# HG changeset patch
# User Andre Bargull <andre.bargull@gmail.com>
# Date 1533312724 25200
#      Fri Aug 03 09:12:04 2018 -0700
# Node ID 215a50752a4966c2d8ef8dc2b51f73a25351c6b0
# Parent  34165b194cf9df25e92febd28948d93554280bf6
Bug 1480819 - Part 4: Don't save non-volatile registers. r=mgaudet

diff --git a/js/src/jit/CodeGenerator.cpp b/js/src/jit/CodeGenerator.cpp
--- a/js/src/jit/CodeGenerator.cpp
+++ b/js/src/jit/CodeGenerator.cpp
@@ -1715,17 +1715,17 @@ public:
                   CompileRuntime* runtime,
                   bool latin1, Register string,
                   Register base, Register temp1, Register temp2,
                   BaseIndex startIndexAddress, BaseIndex limitIndexAddress,
                   bool stringsCanBeInNursery,
                   Label* failure);
 
     // Generate fallback path for creating DependentString.
-    void generateFallback(MacroAssembler& masm, LiveRegisterSet regsToSave);
+    void generateFallback(MacroAssembler& masm);
 };
 
 void
 CreateDependentString::generate(MacroAssembler& masm, const JSAtomState& names,
                                 CompileRuntime* runtime,
                                 bool latin1, Register string,
                                 Register base, Register temp1, Register temp2,
                                 BaseIndex startIndexAddress, BaseIndex limitIndexAddress,
@@ -1856,17 +1856,16 @@ CreateDependentString::generate(MacroAss
         // Post-barrier the base store, whether it was the direct or indirect
         // base (both will end up in temp1 here).
         masm.branchPtrInNurseryChunk(Assembler::Equal, string, temp2, &done);
         masm.branchPtrInNurseryChunk(Assembler::NotEqual, temp1, temp2, &done);
 
         LiveRegisterSet regsToSave(RegisterSet::Volatile());
         regsToSave.takeUnchecked(temp1);
         regsToSave.takeUnchecked(temp2);
-        regsToSave.addUnchecked(string);
 
         masm.PushRegsInMask(regsToSave);
 
         masm.mov(ImmPtr(runtime), temp1);
 
         masm.setupUnalignedABICall(temp2);
         masm.passABIArg(temp1);
         masm.passABIArg(string);
@@ -1888,22 +1887,24 @@ AllocateString(JSContext* cx)
 static void*
 AllocateFatInlineString(JSContext* cx)
 {
     AutoUnsafeCallWithABI unsafe;
     return js::Allocate<JSFatInlineString, NoGC>(cx, js::gc::TenuredHeap);
 }
 
 void
-CreateDependentString::generateFallback(MacroAssembler& masm, LiveRegisterSet regsToSave)
+CreateDependentString::generateFallback(MacroAssembler& masm)
 {
     JitSpew(JitSpew_Codegen, "# Emitting CreateDependentString fallback");
 
-    regsToSave.take(string_);
-    regsToSave.take(temp_);
+    LiveRegisterSet regsToSave(RegisterSet::Volatile());
+    regsToSave.takeUnchecked(string_);
+    regsToSave.takeUnchecked(temp_);
+
     for (FallbackKind kind : mozilla::MakeEnumeratedRange(FallbackKind::Count)) {
         masm.bind(&fallbacks_[kind]);
 
         masm.PushRegsInMask(regsToSave);
 
         masm.setupUnalignedABICall(string_);
         masm.loadJSContext(string_);
         masm.passABIArg(string_);
@@ -1924,27 +1925,28 @@ static void*
 CreateMatchResultFallbackFunc(JSContext* cx, gc::AllocKind kind, size_t nDynamicSlots)
 {
     AutoUnsafeCallWithABI unsafe;
     return js::Allocate<JSObject, NoGC>(cx, kind, nDynamicSlots, gc::DefaultHeap,
                                         &ArrayObject::class_);
 }
 
 static void
-CreateMatchResultFallback(MacroAssembler& masm, LiveRegisterSet regsToSave,
-                          Register object, Register temp1, Register temp2,
+CreateMatchResultFallback(MacroAssembler& masm, Register object, Register temp1, Register temp2,
                           ArrayObject* templateObj, Label* fail)
 {
     JitSpew(JitSpew_Codegen, "# Emitting CreateMatchResult fallback");
 
     MOZ_ASSERT(templateObj->group()->clasp() == &ArrayObject::class_);
 
-    regsToSave.take(object);
-    regsToSave.take(temp1);
-    regsToSave.take(temp2);
+    LiveRegisterSet regsToSave(RegisterSet::Volatile());
+    regsToSave.takeUnchecked(object);
+    regsToSave.takeUnchecked(temp1);
+    regsToSave.takeUnchecked(temp2);
+
     masm.PushRegsInMask(regsToSave);
 
     masm.setupUnalignedABICall(object);
 
     masm.loadJSContext(object);
     masm.passABIArg(object);
     masm.move32(Imm32(int32_t(templateObj->asTenured().getAllocKind())), temp1);
     masm.passABIArg(temp1);
@@ -2113,34 +2115,23 @@ JitRealm::generateRegExpMatcherStub(JSCo
     // All done!
     masm.tagValue(JSVAL_TYPE_OBJECT, object, result);
     masm.ret();
 
     masm.bind(&notFound);
     masm.moveValue(NullValue(), result);
     masm.ret();
 
-    // Fallback paths for CreateDependentString and createGCObject.
-    // Need to save all registers in use when they were called.
-    LiveRegisterSet regsToSave(RegisterSet::Volatile());
-    regsToSave.addUnchecked(regexp);
-    regsToSave.addUnchecked(input);
-    regsToSave.addUnchecked(lastIndex);
-    regsToSave.addUnchecked(temp1);
-    regsToSave.addUnchecked(temp2);
-    regsToSave.addUnchecked(temp3);
-    regsToSave.addUnchecked(temp4);
-    if (maybeTemp5 != InvalidReg)
-        regsToSave.addUnchecked(maybeTemp5);
-
+    // Fallback paths for CreateDependentString.
     for (int isLatin = 0; isLatin <= 1; isLatin++)
-        depStr[isLatin].generateFallback(masm, regsToSave);
-
+        depStr[isLatin].generateFallback(masm);
+
+    // Fallback path for createGCObject.
     masm.bind(&matchResultFallback);
-    CreateMatchResultFallback(masm, regsToSave, object, temp2, temp3, templateObject, &oolEntry);
+    CreateMatchResultFallback(masm, object, temp2, temp3, templateObject, &oolEntry);
     masm.jump(&matchResultJoin);
 
     // Use an undefined value to signal to the caller that the OOL stub needs to be called.
     masm.bind(&oolEntry);
     masm.moveValue(UndefinedValue(), result);
     masm.ret();
 
     Linker linker(masm);
