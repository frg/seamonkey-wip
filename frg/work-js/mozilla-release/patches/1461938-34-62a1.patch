# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1527156174 -7200
#      Thu May 24 12:02:54 2018 +0200
# Node ID dff489ff6e4c7f32fbbf6f2ffc8e695cf13fc7cb
# Parent  6a363dbae27396fd891c955e0dbe42957b89d10a
Bug 1461938 part 34 - Move IteratorCache from JSCompartment to ObjectRealm. r=jonco

diff --git a/js/src/vm/Iteration.cpp b/js/src/vm/Iteration.cpp
--- a/js/src/vm/Iteration.cpp
+++ b/js/src/vm/Iteration.cpp
@@ -818,17 +818,17 @@ LookupInIteratorCache(JSContext* cx, JSO
 
         pobj = pobj->staticPrototype();
     } while (pobj);
 
     MOZ_ASSERT(!guards.empty());
     *numGuards = guards.length();
 
     IteratorHashPolicy::Lookup lookup(guards.begin(), guards.length(), key);
-    auto p = cx->compartment()->iteratorCache.lookup(lookup);
+    auto p = ObjectRealm::get(obj).iteratorCache.lookup(lookup);
     if (!p)
         return nullptr;
 
     PropertyIteratorObject* iterobj = *p;
     MOZ_ASSERT(iterobj->compartment() == cx->compartment());
 
     NativeIterator* ni = iterobj->getNativeIterator();
     if (ni->flags & (JSITER_ACTIVE|JSITER_UNREUSABLE))
@@ -868,17 +868,17 @@ StoreInIteratorCache(JSContext* cx, JSOb
 
     NativeIterator* ni = iterobj->getNativeIterator();
     MOZ_ASSERT(ni->guardCount() > 0);
 
     IteratorHashPolicy::Lookup lookup(reinterpret_cast<ReceiverGuard*>(ni->guardsBegin()),
                                       ni->guardCount(),
                                       ni->guard_key);
 
-    JSCompartment::IteratorCache& cache = cx->compartment()->iteratorCache;
+    ObjectRealm::IteratorCache& cache = ObjectRealm::get(obj).iteratorCache;
     bool ok;
     auto p = cache.lookupForAdd(lookup);
     if (MOZ_LIKELY(!p)) {
         ok = cache.add(p, iterobj);
     } else {
         // If we weren't able to use an existing cached iterator, just
         // replace it.
         cache.remove(p);
diff --git a/js/src/vm/JSCompartment.cpp b/js/src/vm/JSCompartment.cpp
--- a/js/src/vm/JSCompartment.cpp
+++ b/js/src/vm/JSCompartment.cpp
@@ -104,16 +104,22 @@ JSCompartment::init(JSContext* maybecx)
     }
 
     return true;
 }
 
 bool
 ObjectRealm::init(JSContext* maybecx)
 {
+    if (!iteratorCache.init()) {
+        if (maybecx)
+            ReportOutOfMemory(maybecx);
+        return false;
+    }
+
     NativeIteratorSentinel sentinel(NativeIterator::allocateSentinel(maybecx));
     if (!sentinel)
         return false;
 
     iteratorSentinel_ = Move(sentinel);
     enumerators = iteratorSentinel_.get();
     return true;
 }
@@ -136,18 +142,17 @@ Realm::init(JSContext* maybecx)
      * also create tons of iframes, which seems unlikely).
      */
     JS::ResetTimeZone();
 
     if (!objects_.init(maybecx))
         return false;
 
     if (!savedStacks_.init() ||
-        !varNames_.init() ||
-        !iteratorCache.init())
+        !varNames_.init())
     {
         if (maybecx)
             ReportOutOfMemory(maybecx);
         return false;
     }
 
     return true;
 }
@@ -1008,17 +1013,17 @@ Realm::checkScriptMapsAfterMovingGC()
 #endif
 
 void
 Realm::purge()
 {
     dtoaCache.purge();
     newProxyCache.purge();
     objectGroups.purge();
-    iteratorCache.clearAndShrink();
+    objects_.iteratorCache.clearAndShrink();
     arraySpeciesLookup.purge();
 }
 
 void
 Realm::clearTables()
 {
     global_.set(nullptr);
 
diff --git a/js/src/vm/JSCompartment.h b/js/src/vm/JSCompartment.h
--- a/js/src/vm/JSCompartment.h
+++ b/js/src/vm/JSCompartment.h
@@ -587,21 +587,16 @@ struct JSCompartment
   public:
     void assertNoCrossCompartmentWrappers() {
         MOZ_ASSERT(crossCompartmentWrappers.empty());
     }
 
   public:
     js::RegExpCompartment        regExps;
 
-    using IteratorCache = js::HashSet<js::PropertyIteratorObject*,
-                                      js::IteratorHashPolicy,
-                                      js::SystemAllocPolicy>;
-    IteratorCache iteratorCache;
-
     // Recompute the probability with which this compartment should record
     // profiling data (stack traces, allocations log, etc.) about each
     // allocation. We consult the probabilities requested by the Debugger
     // instances observing us, if any.
     void chooseAllocationSamplingProbability() { savedStacks_.chooseSamplingProbability(this); }
 
   protected:
     void addSizeOfExcludingThis(mozilla::MallocSizeOf mallocSizeOf,
@@ -747,16 +742,21 @@ class ObjectRealm
     // but can have that buffer created lazily if it is accessed later. This
     // table manages references from such typed objects to their buffers.
     js::UniquePtr<js::ObjectWeakMap> lazyArrayBuffers;
 
     // Keep track of the metadata objects which can be associated with each JS
     // object. Both keys and values are in this realm.
     js::UniquePtr<js::ObjectWeakMap> objectMetadataTable;
 
+    using IteratorCache = js::HashSet<js::PropertyIteratorObject*,
+                                      js::IteratorHashPolicy,
+                                      js::SystemAllocPolicy>;
+    IteratorCache iteratorCache;
+
     static inline ObjectRealm& get(const JSObject* obj);
 
     explicit ObjectRealm(JS::Zone* zone);
     ~ObjectRealm();
 
     MOZ_MUST_USE bool init(JSContext* maybecx);
 
     void finishRoots();
