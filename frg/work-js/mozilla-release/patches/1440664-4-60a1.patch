# HG changeset patch
# User Kartikaya Gupta <kgupta@mozilla.com>
# Date 1519941156 18000
# Node ID 6098d2c015d704bb5c91b31ed03c0a3dec521ce3
# Parent  707680d656def1e2777824650e9178f5225c6856
Bug 1440664 - Re-generate FFI header. r=jrmuizel

MozReview-Commit-ID: 8oJcG2CPR2U

diff --git a/gfx/webrender_bindings/webrender_ffi_generated.h b/gfx/webrender_bindings/webrender_ffi_generated.h
--- a/gfx/webrender_bindings/webrender_ffi_generated.h
+++ b/gfx/webrender_bindings/webrender_ffi_generated.h
@@ -337,21 +337,24 @@ using LayoutSize = LayerSize;
 // items.
 struct BuiltDisplayListDescriptor {
   // The first IPC time stamp: before any work has been done
   uint64_t builder_start_time;
   // The second IPC time stamp: after serialization
   uint64_t builder_finish_time;
   // The third IPC time stamp: just before sending
   uint64_t send_start_time;
+  // The amount of clips ids assigned while building this display list.
+  size_t total_clip_ids;
 
   bool operator==(const BuiltDisplayListDescriptor& aOther) const {
     return builder_start_time == aOther.builder_start_time &&
            builder_finish_time == aOther.builder_finish_time &&
-           send_start_time == aOther.send_start_time;
+           send_start_time == aOther.send_start_time &&
+           total_clip_ids == aOther.total_clip_ids;
   }
 };
 
 struct WrVecU8 {
   uint8_t *data;
   size_t length;
   size_t capacity;
 
@@ -618,18 +621,19 @@ struct WrAnimationProperty {
 //
 // For now layout pixels are equivalent to layer pixels, but it may change.
 using LayoutPixel = LayerPixel;
 
 // A 3d transform stored as a 4 by 4 matrix in row-major order in memory.
 //
 // Transforms can be parametrized over the source and destination units, to describe a
 // transformation from a space to another.
-// For example, `TypedTransform3D<f32, WordSpace, ScreenSpace>::transform_point3d`
-// takes a `TypedPoint3D<f32, WordSpace>` and returns a `TypedPoint3D<f32, ScreenSpace>`.
+// For example, `TypedTransform3D<f32, WorldSpace, ScreenSpace>::transform_point3d`
+// takes a `TypedPoint3D<f32, WorldSpace>` and returns a `TypedPoint3D<f32, ScreenSpace>`.
+//
 //
 // Transforms expose a set of convenience methods for pre- and post-transformations.
 // A pre-transformation corresponds to adding an operation that is applied before
 // the rest of the transformation, while a post-transformation adds an operation
 // that is applied after.
 template<typename T, typename Src, typename Dst>
 struct TypedTransform3D {
   T m11;
@@ -1044,44 +1048,44 @@ WR_INLINE
 void wr_dec_ref_arc(const VecU8 *aArc)
 WR_DESTRUCTOR_SAFE_FUNC;
 
 WR_INLINE
 void wr_dp_clear_save(WrState *aState)
 WR_FUNC;
 
 WR_INLINE
-uint64_t wr_dp_define_clip(WrState *aState,
-                           const uint64_t *aAncestorScrollId,
-                           const uint64_t *aAncestorClipId,
-                           LayoutRect aClipRect,
-                           const ComplexClipRegion *aComplex,
-                           size_t aComplexCount,
-                           const WrImageMask *aMask)
+size_t wr_dp_define_clip(WrState *aState,
+                         const size_t *aAncestorScrollId,
+                         const size_t *aAncestorClipId,
+                         LayoutRect aClipRect,
+                         const ComplexClipRegion *aComplex,
+                         size_t aComplexCount,
+                         const WrImageMask *aMask)
 WR_FUNC;
 
 WR_INLINE
-uint64_t wr_dp_define_scroll_layer(WrState *aState,
-                                   uint64_t aScrollId,
-                                   const uint64_t *aAncestorScrollId,
-                                   const uint64_t *aAncestorClipId,
-                                   LayoutRect aContentRect,
-                                   LayoutRect aClipRect)
+size_t wr_dp_define_scroll_layer(WrState *aState,
+                                 uint64_t aScrollId,
+                                 const size_t *aAncestorScrollId,
+                                 const size_t *aAncestorClipId,
+                                 LayoutRect aContentRect,
+                                 LayoutRect aClipRect)
 WR_FUNC;
 
 WR_INLINE
-uint64_t wr_dp_define_sticky_frame(WrState *aState,
-                                   LayoutRect aContentRect,
-                                   const float *aTopMargin,
-                                   const float *aRightMargin,
-                                   const float *aBottomMargin,
-                                   const float *aLeftMargin,
-                                   StickyOffsetBounds aVerticalBounds,
-                                   StickyOffsetBounds aHorizontalBounds,
-                                   LayoutVector2D aAppliedOffset)
+size_t wr_dp_define_sticky_frame(WrState *aState,
+                                 LayoutRect aContentRect,
+                                 const float *aTopMargin,
+                                 const float *aRightMargin,
+                                 const float *aBottomMargin,
+                                 const float *aLeftMargin,
+                                 StickyOffsetBounds aVerticalBounds,
+                                 StickyOffsetBounds aHorizontalBounds,
+                                 LayoutVector2D aAppliedOffset)
 WR_FUNC;
 
 WR_INLINE
 void wr_dp_pop_all_shadows(WrState *aState)
 WR_FUNC;
 
 WR_INLINE
 void wr_dp_pop_clip(WrState *aState)
@@ -1169,23 +1173,23 @@ WR_FUNC;
 
 WR_INLINE
 void wr_dp_push_clear_rect(WrState *aState,
                            LayoutRect aRect)
 WR_FUNC;
 
 WR_INLINE
 void wr_dp_push_clip(WrState *aState,
-                     uint64_t aClipId)
+                     size_t aClipId)
 WR_FUNC;
 
 WR_INLINE
 void wr_dp_push_clip_and_scroll_info(WrState *aState,
-                                     uint64_t aScrollId,
-                                     const uint64_t *aClipId)
+                                     size_t aScrollId,
+                                     const size_t *aClipId)
 WR_FUNC;
 
 WR_INLINE
 void wr_dp_push_iframe(WrState *aState,
                        LayoutRect aRect,
                        bool aIsBackfaceVisible,
                        WrPipelineId aPipelineId)
 WR_FUNC;
@@ -1246,17 +1250,17 @@ void wr_dp_push_rect(WrState *aState,
                      LayoutRect aRect,
                      LayoutRect aClip,
                      bool aIsBackfaceVisible,
                      ColorF aColor)
 WR_FUNC;
 
 WR_INLINE
 void wr_dp_push_scroll_layer(WrState *aState,
-                             uint64_t aScrollId)
+                             size_t aScrollId)
 WR_FUNC;
 
 WR_INLINE
 void wr_dp_push_shadow(WrState *aState,
                        LayoutRect aBounds,
                        LayoutRect aClip,
                        bool aIsBackfaceVisible,
                        Shadow aShadow)
