# HG changeset patch
# User Dave Townsend <dtownsend@oxymoronical.com>
# Date 1530227169 25200
# Node ID 40ed437da7ae4407ece2ad47cd8fe3c7943f9eb9
# Parent  adb70002d5ce6846d694c4c71be6aa6df2a5d2bd
Bug 1453751: Load favicons in the content process. r=mak, r=gijs, r=aswan, r=mixedpuppy

Summary:
This moves the load of favicons into the content process. We use the same logic
for finding favicons (based on waiting until none have shown up for a short
time) but then load the favicon and convert it to a data uri which we then
dispatch to the parent process. Along the way this fixes asssociating the load
with the tab for WebExtension and devtools, fixes CSP usage for the load, fixes
expiry detection of the favicon and stops us from loading the same resource
twice.

This change also merges the prefs browser.chrome.site_icons and
browser.chrome.favicons leaving just the former controlling favicon loading. It
adds the pref browser.chrome.guess_favicon to allow disabling guessing where
a favicon might be located for a site (at <hostname>/favicon.ico). This is
mainly to allow disabling this in tests where those additional yet automatic
requests are uninteresting for the test.

There are multiple clean-ups that can follow this but this is a first step along
that path.

MozReview-Commit-ID: E0Cs59UnxaF

Reviewers: mak

Tags: #secure-revision

Bug #: 1453751

Differential Revision: https://phabricator.services.mozilla.com/D1672
Differential Revision: https://phabricator.services.mozilla.com/D1673
Differential Revision: https://phabricator.services.mozilla.com/D1674
Differential Revision: https://phabricator.services.mozilla.com/D1850
Differential Revision: https://phabricator.services.mozilla.com/D1869

diff --git a/extensions/cookie/test/browser.ini b/extensions/cookie/test/browser.ini
--- a/extensions/cookie/test/browser.ini
+++ b/extensions/cookie/test/browser.ini
@@ -1,13 +1,14 @@
 [DEFAULT]
 
 [browser_test_favicon.js]
 support-files =
   damonbowling.jpg
   damonbowling.jpg^headers^
+  file_favicon.html
 [browser_permmgr_sync.js]
 # The browser_permmgr_sync test tests e10s specific behavior, and runs code
 # paths which would hit the debug only assertion in
 # nsPermissionManager::PermissionKey::CreateFromPrincipal. Because of this, it
 # is only run in e10s opt builds.
 skip-if = debug || !e10s
 [browser_permmgr_viewsrc.js]
diff --git a/extensions/cookie/test/browser_test_favicon.js b/extensions/cookie/test/browser_test_favicon.js
--- a/extensions/cookie/test/browser_test_favicon.js
+++ b/extensions/cookie/test/browser_test_favicon.js
@@ -1,23 +1,22 @@
-// tests third party cookie blocking using a favicon load directly from chrome.
-// in this case, the docshell of the channel is chrome, not content; thus
-// the cookie should be considered third party.
+// Tests third party cookie blocking using a favicon loaded from a different
+// domain. The cookie should be considered third party.
 
 add_task(async function() {
   const iconUrl = "http://example.org/browser/extensions/cookie/test/damonbowling.jpg";
+  const pageUrl = "http://example.com/browser/extensions/cookie/test/file_favicon.html";
 	await SpecialPowers.pushPrefEnv({"set": [["network.cookie.cookieBehavior", 1]]});
 
   let promise = TestUtils.topicObserved("cookie-rejected", subject => {
     let uri = subject.QueryInterface(Ci.nsIURI);
     return uri.spec == iconUrl;
   });
 
-  let tab = await BrowserTestUtils.openNewForegroundTab(gBrowser, "http://example.com/");
+  // Kick off a page load that will load the favicon.
+  let tab = await BrowserTestUtils.openNewForegroundTab(gBrowser, pageUrl);
   registerCleanupFunction(async function() {
     BrowserTestUtils.removeTab(tab);
   });
 
-  // Kick off a favicon load.
-  gBrowser.setIcon(tab, iconUrl);
   await promise;
   ok(true, "foreign favicon cookie was blocked");
 });
diff --git a/extensions/cookie/test/file_favicon.html b/extensions/cookie/test/file_favicon.html
new file mode 100644
--- /dev/null
+++ b/extensions/cookie/test/file_favicon.html
@@ -0,0 +1,7 @@
+<!DOCTYPE html>
+
+<html>
+  <head>
+    <link rel="shortcut icon" href="http://example.org/browser/extensions/cookie/test/damonbowling.jpg">
+  </head>
+</html>
