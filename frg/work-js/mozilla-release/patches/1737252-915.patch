# HG changeset patch
# User Hubert Boma Manilla <hmanilla@mozilla.com>
# Date 1637877255 0
#      Thu Nov 25 21:54:15 2021 +0000
# Node ID b5857a3da213cb5401c61de40e60564b68d66b09
# Parent  61d94fad86da1d447eb905a93be3d07a369c42a8
Bug 1737252 - [devtools] Escaping back tick signs. r=nchevobbe, a=RyanVM

This patch escapes the back ticks properly. There may need to be further
thoughts on having seperate context menu items for powershell versus cmd.exe
enabling us to efficiently handles those differently.

Differential Revision: https://phabricator.services.mozilla.com/D131033

diff --git a/devtools/client/netmonitor/test/browser_net_curl-utils.js b/devtools/client/netmonitor/test/browser_net_curl-utils.js
--- a/devtools/client/netmonitor/test/browser_net_curl-utils.js
+++ b/devtools/client/netmonitor/test/browser_net_curl-utils.js
@@ -258,16 +258,30 @@ function testEscapeStringWin() {
   let backslashes = "\\A simple string\\";
   is(CurlUtils.escapeStringWin(backslashes), '"\\\\A simple string\\\\"',
     "Backslashes should be escaped.");
 
   let newLines = "line1\r\nline2\r\nline3";
   is(CurlUtils.escapeStringWin(newLines),
     '"line1"^\u000d\u000A\u000d\u000A"line2"^\u000d\u000A\u000d\u000A"line3"',
     "Newlines should be escaped.");
+
+  const dollarSignCommand = "$(calc.exe)";
+  is(
+    CurlUtils.escapeStringWin(dollarSignCommand),
+    '"`$(calc.exe)"',
+    "Dollar sign should be escaped."
+  );
+
+  const tickSignCommand = "`$(calc.exe)";
+  is(
+    CurlUtils.escapeStringWin(tickSignCommand),
+    '"```$(calc.exe)"',
+    "Both the tick and dollar signs should be escaped."
+  );
 }
 
 async function createCurlData(selected, getLongString, requestData) {
   let { id, url, method, httpVersion } = selected;
 
   // Create a sanitized object for the Curl command generator.
   let data = {
     url,
diff --git a/devtools/client/shared/curl.js b/devtools/client/shared/curl.js
--- a/devtools/client/shared/curl.js
+++ b/devtools/client/shared/curl.js
@@ -396,19 +396,26 @@ const CurlUtils = {
   },
 
   /**
    * Escape util function for Windows systems.
    * Credit: Google DevTools
    */
   escapeStringWin: function(str) {
     /*
-       Replace dollar sign because of commands (e.g $(cmd.exe)) in
-       powershell when using double quotes.
-       Useful details http://www.rlmueller.net/PowerShellEscape.htm
+       Replace the backtick character ` with `` in order to escape it.
+       The backtick character is an escape character in PowerShell and
+       can, among other things, be used to disable the effect of some
+       of the other escapes created below.
+       Also see http://www.rlmueller.net/PowerShellEscape.htm for
+       useful details.
+
+       Replace dollar sign because of commands in powershell when using
+       double quotes. e.g $(calc.exe) Also see
+       http://www.rlmueller.net/PowerShellEscape.htm for details.
 
        Replace quote by double quote (but not by \") because it is
        recognized by both cmd.exe and MS Crt arguments parser.
 
        Replace % by "%" because it could be expanded to an environment
        variable value. So %% becomes "%""%". Even if an env variable ""
        (2 doublequotes) is declared, the cmd.exe will not
        substitute it with its value.
@@ -422,19 +429,20 @@ const CurlUtils = {
        So for quote: `"Text-start\r\ntext-continue"`,
        we get: `"Text-start"^\r\n\r\n"text-continue"`,
        where `^\r\n` is just breaking the command, the `\r\n` right
        after is actual escaped newline.
     */
     return (
       '"' +
       str
-        .replace(/\$/g, "`$")
-        .replace(/"/g, '""')
-        .replace(/%/g, '"%"')
+        .replaceAll("`", "``")
+        .replaceAll("$", "`$")
+        .replaceAll('"', '""')
+        .replaceAll("%", '"%"')
         .replace(/\\/g, "\\\\")
         .replace(/[\r\n]{1,2}/g, '"^$&$&"') +
       '"'
     );
   }
 };
 
 exports.CurlUtils = CurlUtils;
