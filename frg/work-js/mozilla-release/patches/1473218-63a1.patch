# HG changeset patch
# User Andrea Marchesini <amarchesini@mozilla.com>
# Date 1530856909 -7200
# Node ID 90f470be3f1d206410d73b8862b64dbe7391d85c
# Parent  a90c692bc45d03ae5f5d2708e831462f86d46b9d
Bug 1473218 - Implement report-sample support for CSP directives, r=ckerschb

diff --git a/dom/security/nsCSPContext.cpp b/dom/security/nsCSPContext.cpp
--- a/dom/security/nsCSPContext.cpp
+++ b/dom/security/nsCSPContext.cpp
@@ -558,20 +558,23 @@ nsCSPContext::GetAllowsInline(nsContentP
 
     if (!allowed) {
       // policy is violoated: deny the load unless policy is report only and
       // report the violation.
       if (!mPolicies[i]->getReportOnlyFlag()) {
         *outAllowsInline = false;
       }
       nsAutoString violatedDirective;
-      mPolicies[i]->getDirectiveStringForContentType(aContentType, violatedDirective);
+      bool reportSample = false;
+      mPolicies[i]->getDirectiveStringAndReportSampleForContentType(aContentType,
+                                                                    violatedDirective,
+                                                                    &reportSample);
       reportInlineViolation(aContentType,
                             aNonce,
-                            content,
+                            reportSample ? content : EmptyString(),
                             violatedDirective,
                             i,
                             aLineNumber,
                             aColumnNumber);
     }
   }
   return NS_OK;
 }
@@ -608,23 +611,26 @@ nsCSPContext::GetAllowsInline(nsContentP
 #define CASE_CHECK_AND_REPORT(violationType, contentPolicyType, nonceOrHash,   \
                               keyword, observerTopic)                          \
   case nsIContentSecurityPolicy::VIOLATION_TYPE_ ## violationType :            \
     PR_BEGIN_MACRO                                                             \
     if (!mPolicies[p]->allows(nsIContentPolicy::TYPE_ ## contentPolicyType,    \
                               keyword, nonceOrHash, false))                    \
     {                                                                          \
       nsAutoString violatedDirective;                                          \
-      mPolicies[p]->getDirectiveStringForContentType(                          \
+      bool reportSample = false;                                               \
+      mPolicies[p]->getDirectiveStringAndReportSampleForContentType(           \
                         nsIContentPolicy::TYPE_ ## contentPolicyType,          \
-                        violatedDirective);                                    \
+                        violatedDirective, &reportSample);                     \
       this->AsyncReportViolation(selfISupports, nullptr, violatedDirective, p, \
                                  NS_LITERAL_STRING(observerTopic),             \
-                                 aSourceFile, aScriptSample, aLineNum,         \
-                                 aColumnNum);                                  \
+                                 aSourceFile,                                  \
+                                 reportSample                                  \
+                                   ? aScriptSample : EmptyString(),            \
+                                 aLineNum, aColumnNum);                        \
     }                                                                          \
     PR_END_MACRO;                                                              \
     break
 
 /**
  * For each policy, log any violation on the Error Console and send a report
  * if a report-uri is present in the policy
  *
@@ -1272,17 +1278,17 @@ class CSPReportSenderRunnable final : pu
 
       if (blockedURI) {
         blockedURI->GetSpec(blockedDataStr);
         if (blockedDataStr.Length() > 40) {
           bool isData = false;
           rv = blockedURI->SchemeIs("data", &isData);
           if (NS_SUCCEEDED(rv) && isData) {
             blockedDataStr.Truncate(40);
-            blockedDataStr.AppendASCII("...");
+            blockedDataStr.AppendASCII("…");
           }
         }
       } else if (blockedString) {
         blockedString->GetData(blockedDataStr);
       }
 
       if (blockedDataStr.Length() > 0) {
         nsString blockedDataChar16 = NS_ConvertUTF8toUTF16(blockedDataStr);
diff --git a/dom/security/nsCSPParser.cpp b/dom/security/nsCSPParser.cpp
--- a/dom/security/nsCSPParser.cpp
+++ b/dom/security/nsCSPParser.cpp
@@ -535,16 +535,20 @@ nsCSPParser::keywordSource()
                NS_ConvertUTF16toUTF8(mCurValue).get()));
 
   // Special case handling for 'self' which is not stored internally as a keyword,
   // but rather creates a nsCSPHostSrc using the selfURI
   if (CSP_IsKeyword(mCurToken, CSP_SELF)) {
     return CSP_CreateHostSrcFromSelfURI(mSelfURI);
   }
 
+  if (CSP_IsKeyword(mCurToken, CSP_REPORT_SAMPLE)) {
+    return new nsCSPKeywordSrc(CSP_UTF16KeywordToEnum(mCurToken));
+  }
+
   if (CSP_IsKeyword(mCurToken, CSP_STRICT_DYNAMIC)) {
     // make sure strict dynamic is enabled
     if (!sStrictDynamicEnabled) {
       return nullptr;
     }
     if (!CSP_IsDirective(mCurDir[0], nsIContentSecurityPolicy::SCRIPT_SRC_DIRECTIVE)) {
       // Todo: Enforce 'strict-dynamic' within default-src; see Bug 1313937
       const char16_t* params[] = { u"strict-dynamic" };
@@ -853,19 +857,20 @@ nsCSPParser::sourceList(nsTArray<nsCSPBa
     if (src) {
       outSrcs.AppendElement(src);
     }
   }
 
   // Check if the directive contains a 'none'
   if (isNone) {
     // If the directive contains no other srcs, then we set the 'none'
-    if (outSrcs.Length() == 0) {
+    if (outSrcs.IsEmpty() ||
+        (outSrcs.Length() == 1 && outSrcs[0]->isReportSample())) {
       nsCSPKeywordSrc *keyword = new nsCSPKeywordSrc(CSP_NONE);
-      outSrcs.AppendElement(keyword);
+      outSrcs.InsertElementAt(0, keyword);
     }
     // Otherwise, we ignore 'none' and report a warning
     else {
       const char16_t* params[] = { CSP_EnumToUTF16Keyword(CSP_NONE) };
       logWarningErrorToConsole(nsIScriptError::warningFlag, "ignoringUnknownOption",
                                params, ArrayLength(params));
     }
   }
@@ -1199,19 +1204,20 @@ nsCSPParser::directive()
     CSP_IsDirective(mCurDir[0], nsIContentSecurityPolicy::FRAME_ANCESTORS_DIRECTIVE);
 
   // Try to parse all the srcs by handing the array off to directiveValue
   nsTArray<nsCSPBaseSrc*> srcs;
   directiveValue(srcs);
 
   // If we can not parse any srcs; we let the source expression be the empty set ('none')
   // see, http://www.w3.org/TR/CSP11/#source-list-parsing
-  if (srcs.Length() == 0) {
+  if (srcs.IsEmpty() ||
+      (srcs.Length() == 1 && srcs[0]->isReportSample())) {
     nsCSPKeywordSrc *keyword = new nsCSPKeywordSrc(CSP_NONE);
-    srcs.AppendElement(keyword);
+    srcs.InsertElementAt(0, keyword);
   }
 
   // If policy contains 'strict-dynamic' invalidate all srcs within script-src.
   if (mStrictDynamic) {
     MOZ_ASSERT(cspDir->equals(nsIContentSecurityPolicy::SCRIPT_SRC_DIRECTIVE),
                "strict-dynamic only allowed within script-src");
     for (uint32_t i = 0; i < srcs.Length(); i++) {
       // Please note that nsCSPNonceSrc as well as nsCSPHashSrc overwrite invalidate(),
diff --git a/dom/security/nsCSPUtils.cpp b/dom/security/nsCSPUtils.cpp
--- a/dom/security/nsCSPUtils.cpp
+++ b/dom/security/nsCSPUtils.cpp
@@ -1249,16 +1249,28 @@ bool nsCSPDirective::equals(CSPDirective
 }
 
 void
 nsCSPDirective::getDirName(nsAString& outStr) const
 {
   outStr.AppendASCII(CSP_CSPDirectiveToString(mDirective));
 }
 
+bool
+nsCSPDirective::hasReportSampleKeyword() const
+{
+  for (nsCSPBaseSrc* src : mSrcs) {
+    if (src->isReportSample()) {
+      return true;
+    }
+  }
+
+  return false;
+}
+
 /* =============== nsCSPChildSrcDirective ============= */
 
 nsCSPChildSrcDirective::nsCSPChildSrcDirective(CSPDirective aDirective)
   : nsCSPDirective(aDirective)
   , mRestrictFrames(false)
   , mRestrictWorkers(false)
 {
 }
@@ -1599,33 +1611,39 @@ nsCSPPolicy::hasDirective(CSPDirective a
 
 /*
  * Use this function only after ::allows() returned 'false'. Most and
  * foremost it's used to get the violated directive before sending reports.
  * The parameter outDirective is the equivalent of 'outViolatedDirective'
  * for the ::permits() function family.
  */
 void
-nsCSPPolicy::getDirectiveStringForContentType(nsContentPolicyType aContentType,
-                                              nsAString& outDirective) const
+nsCSPPolicy::getDirectiveStringAndReportSampleForContentType(nsContentPolicyType aContentType,
+                                                             nsAString& outDirective,
+                                                             bool* aReportSample) const
 {
+  MOZ_ASSERT(aReportSample);
+  *aReportSample = false;
+
   nsCSPDirective* defaultDir = nullptr;
   for (uint32_t i = 0; i < mDirectives.Length(); i++) {
     if (mDirectives[i]->restrictsContentType(aContentType)) {
       mDirectives[i]->getDirName(outDirective);
+      *aReportSample = mDirectives[i]->hasReportSampleKeyword();
       return;
     }
     if (mDirectives[i]->isDefaultDirective()) {
       defaultDir = mDirectives[i];
     }
   }
   // if we haven't found a matching directive yet,
   // the contentType must be restricted by the default directive
   if (defaultDir) {
     defaultDir->getDirName(outDirective);
+    *aReportSample = defaultDir->hasReportSampleKeyword();
     return;
   }
   NS_ASSERTION(false, "Can not query directive string for contentType!");
   outDirective.AppendASCII("couldNotQueryViolatedDirective");
 }
 
 void
 nsCSPPolicy::getDirectiveAsString(CSPDirective aDir, nsAString& outDirective) const
diff --git a/dom/security/nsCSPUtils.h b/dom/security/nsCSPUtils.h
--- a/dom/security/nsCSPUtils.h
+++ b/dom/security/nsCSPUtils.h
@@ -121,16 +121,17 @@ inline CSPDirective CSP_StringToCSPDirec
 
 #define FOR_EACH_CSP_KEYWORD(macro) \
   macro(CSP_SELF,            "'self'") \
   macro(CSP_UNSAFE_INLINE,   "'unsafe-inline'") \
   macro(CSP_UNSAFE_EVAL,     "'unsafe-eval'") \
   macro(CSP_NONE,            "'none'") \
   macro(CSP_NONCE,           "'nonce-") \
   macro(CSP_REQUIRE_SRI_FOR, "require-sri-for") \
+  macro(CSP_REPORT_SAMPLE,   "'report-sample'") \
   macro(CSP_STRICT_DYNAMIC,  "'strict-dynamic'")
 
 enum CSPKeyword {
   #define KEYWORD_ENUM(id_, string_) id_,
   FOR_EACH_CSP_KEYWORD(KEYWORD_ENUM)
   #undef KEYWORD_ENUM
 
   // CSP_LAST_KEYWORD_VALUE always needs to be the last element in the enum
@@ -232,16 +233,19 @@ class nsCSPBaseSrc {
     virtual bool allows(enum CSPKeyword aKeyword, const nsAString& aHashOrNonce,
                         bool aParserCreated) const;
     virtual bool visit(nsCSPSrcVisitor* aVisitor) const = 0;
     virtual void toString(nsAString& outStr) const = 0;
 
     virtual void invalidate() const
       { mInvalidated = true; }
 
+    virtual bool isReportSample() const
+      { return false; }
+
   protected:
     // invalidate srcs if 'script-dynamic' is present or also invalidate
     // unsafe-inline' if nonce- or hash-source specified
     mutable bool mInvalidated;
 
 };
 
 /* =============== nsCSPSchemeSrc ============ */
@@ -330,16 +334,18 @@ class nsCSPKeywordSrc : public nsCSPBase
     inline void invalidate() const override
     {
       // keywords that need to invalidated
       if (mKeyword == CSP_SELF || mKeyword == CSP_UNSAFE_INLINE) {
         mInvalidated = true;
       }
     }
 
+    bool isReportSample() const override
+      { return mKeyword == CSP_REPORT_SAMPLE; }
   private:
     CSPKeyword mKeyword;
 };
 
 /* =============== nsCSPNonceSource =========== */
 
 class nsCSPNonceSrc : public nsCSPBaseSrc {
   public:
@@ -469,16 +475,18 @@ class nsCSPDirective {
     virtual bool equals(CSPDirective aDirective) const;
 
     void getReportURIs(nsTArray<nsString> &outReportURIs) const;
 
     bool visitSrcs(nsCSPSrcVisitor* aVisitor) const;
 
     virtual void getDirName(nsAString& outStr) const;
 
+    bool hasReportSampleKeyword() const;
+
   protected:
     CSPDirective            mDirective;
     nsTArray<nsCSPBaseSrc*> mSrcs;
 };
 
 /* =============== nsCSPChildSrcDirective ============= */
 
 /*
@@ -672,18 +680,19 @@ class nsCSPPolicy {
     inline void setReportOnlyFlag(bool aFlag)
       { mReportOnly = aFlag; }
 
     inline bool getReportOnlyFlag() const
       { return mReportOnly; }
 
     void getReportURIs(nsTArray<nsString> &outReportURIs) const;
 
-    void getDirectiveStringForContentType(nsContentPolicyType aContentType,
-                                          nsAString& outDirective) const;
+    void getDirectiveStringAndReportSampleForContentType(nsContentPolicyType aContentType,
+                                                         nsAString& outDirective,
+                                                         bool* aReportSample) const;
 
     void getDirectiveAsString(CSPDirective aDir, nsAString& outDirective) const;
 
     uint32_t getSandboxFlags() const;
 
     bool requireSRIForType(nsContentPolicyType aContentType);
 
     inline uint32_t getNumDirectives() const
diff --git a/dom/security/test/csp/file_data-uri_blocked.html^headers^ b/dom/security/test/csp/file_data-uri_blocked.html^headers^
--- a/dom/security/test/csp/file_data-uri_blocked.html^headers^
+++ b/dom/security/test/csp/file_data-uri_blocked.html^headers^
@@ -1,1 +1,1 @@
-Content-Security-Policy: default-src 'self'; img-src 'none'
+Content-Security-Policy: default-src 'self' 'report-sample'; img-src 'none' 'report-sample'
diff --git a/dom/security/test/csp/test_bug1242019.html b/dom/security/test/csp/test_bug1242019.html
--- a/dom/security/test/csp/test_bug1242019.html
+++ b/dom/security/test/csp/test_bug1242019.html
@@ -27,17 +27,17 @@ var expectedURI = "data:image/png;base64
 
 SpecialPowers.registerConsoleListener(function ConsoleMsgListener(aMsg) {
   // look for the message with data uri and see the data uri is truncated to 40 chars
   data_start = aMsg.message.indexOf(expectedURI) 
   if (data_start > -1) {
     data_uri = "";
     data_uri = aMsg.message.substr(data_start);
     // this will either match the elipsis after the URI or the . at the end of the message
-    data_uri = data_uri.substr(0, data_uri.indexOf("."));
+    data_uri = data_uri.substr(0, data_uri.indexOf("…"));
     if (data_uri == "") {
       return;
     }
 
     ok(data_uri.length == 40, "Data URI only shows 40 characters in the console");
     SimpleTest.executeSoon(cleanup);
   }
 });
diff --git a/dom/security/test/csp/test_report.html b/dom/security/test/csp/test_report.html
--- a/dom/security/test/csp/test_report.html
+++ b/dom/security/test/csp/test_report.html
@@ -24,20 +24,20 @@ https://bugzilla.mozilla.org/show_bug.cg
  * of the JSON is not formatted properly (e.g. not properly escaped)
  * then JSON.parse will fail, which allows to pinpoint such errors
  * in the catch block, and the test will fail. Since we use an
  * observer, we can set the actual report-uri to a foo value.
  */
 
 const testfile = "tests/dom/security/test/csp/file_report.html";
 const reportURI = "http://mochi.test:8888/foo.sjs";
-const policy = "default-src 'none'; report-uri " + reportURI;
+const policy = "default-src 'none' 'report-sample'; report-uri " + reportURI;
 const docUri = "http://mochi.test:8888/tests/dom/security/test/csp/file_testserver.sjs" +
                "?file=tests/dom/security/test/csp/file_report.html" +
-               "&csp=default-src%20%27none%27%3B%20report-uri%20http%3A//mochi.test%3A8888/foo.sjs";
+               "&csp=default-src%20%27none%27%20%27report-sample%27%3B%20report-uri%20http%3A//mochi.test%3A8888/foo.sjs";
 
 window.checkResults = function(reportObj) {
   var cspReport = reportObj["csp-report"];
 
   // The following uris' fragments should be stripped before reporting:
   //    * document-uri
   //    * blocked-uri
   //    * source-file
@@ -47,17 +47,17 @@ window.checkResults = function(reportObj
   // we can not test for the whole referrer since it includes platform specific information
   ok(cspReport["referrer"].startsWith("http://mochi.test:8888/tests/dom/security/test/csp/test_report.html"),
      "Incorrect referrer");
 
   is(cspReport["blocked-uri"], "self", "Incorrect blocked-uri");
 
   is(cspReport["violated-directive"], "default-src", "Incorrect violated-directive");
 
-  is(cspReport["original-policy"], "default-src 'none'; report-uri http://mochi.test:8888/foo.sjs",
+  is(cspReport["original-policy"], "default-src 'none' 'report-sample'; report-uri http://mochi.test:8888/foo.sjs",
      "Incorrect original-policy");
 
   is(cspReport["source-file"], docUri, "Incorrect source-file");
 
   is(cspReport["script-sample"], "\n    var foo = \"propEscFoo\";\n    var bar...",
      "Incorrect script-sample");
 
   is(cspReport["line-number"], 7, "Incorrect line-number");
diff --git a/dom/security/test/unit/test_csp_reports.js b/dom/security/test/unit/test_csp_reports.js
--- a/dom/security/test/unit/test_csp_reports.js
+++ b/dom/security/test/unit/test_csp_reports.js
@@ -65,17 +65,17 @@ function makeReportHandler(testpath, mes
  */
 function makeTest(id, expectedJSON, useReportOnlyPolicy, callback) {
   testsToFinish++;
   do_test_pending();
 
   // set up a new CSP instance for each test.
   var csp = Cc["@mozilla.org/cspcontext;1"]
               .createInstance(Ci.nsIContentSecurityPolicy);
-  var policy = "default-src 'none'; " +
+  var policy = "default-src 'none' 'report-sample'; " +
                "report-uri " + REPORT_SERVER_URI +
                                ":" + REPORT_SERVER_PORT +
                                "/test" + id;
   var selfuri = NetUtil.newURI(REPORT_SERVER_URI +
                                ":" + REPORT_SERVER_PORT +
                                "/foo/self");
 
   dump("Created test " + id + " : " + policy + "\n\n");
diff --git a/toolkit/components/extensions/test/xpcshell/test_ext_contentscript_triggeringPrincipal.js b/toolkit/components/extensions/test/xpcshell/test_ext_contentscript_triggeringPrincipal.js
--- a/toolkit/components/extensions/test/xpcshell/test_ext_contentscript_triggeringPrincipal.js
+++ b/toolkit/components/extensions/test/xpcshell/test_ext_contentscript_triggeringPrincipal.js
@@ -1066,17 +1066,17 @@ add_task(async function test_contentscri
  * content page.
  */
 add_task(async function test_contentscript_csp() {
   // We currently don't get the full set of CSP reports when running in network
   // scheduling chaos mode (bug 1408193). It's not entirely clear why.
   let chaosMode = parseInt(env.get("MOZ_CHAOSMODE"), 16);
   let checkCSPReports = !(chaosMode === 0 || chaosMode & 0x02);
 
-  gContentSecurityPolicy = `default-src 'none'; script-src 'nonce-deadbeef' 'unsafe-eval'; report-uri ${CSP_REPORT_PATH};`;
+  gContentSecurityPolicy = `default-src 'none' 'report-sample'; script-src 'nonce-deadbeef' 'unsafe-eval' 'report-sample'; report-uri ${CSP_REPORT_PATH};`;
 
   let extension = ExtensionTestUtils.loadExtension(EXTENSION_DATA);
   await extension.startup();
 
   let urlsPromise = extension.awaitMessage("css-sources").then(msg => {
     return mergeSources(
       computeExpectedForbiddenURLs(msg, true),
       computeBaseURLs(TESTS, EXTENSION_SOURCES, PAGE_SOURCES));
