# HG changeset patch
# User Nicolas Chevobbe <nchevobbe@mozilla.com>
# Date 1520241324 -3600
# Node ID 8b4c82584d6b33a563abd453c743fff5f5a5b191
# Parent  34d7c26c0373b98443bd1207659c0aa8fe55c15a
Bug 1307925 - Localize and explicitely test Navigation message; r=Honza.

The message is now localized, with the URL as a parameter.
browser_webconsole_persist was modified to explicitely wait for
the Navigation message to be logged.

MozReview-Commit-ID: L0hsrkYsl16

diff --git a/devtools/client/locales/en-US/webconsole.properties b/devtools/client/locales/en-US/webconsole.properties
--- a/devtools/client/locales/en-US/webconsole.properties
+++ b/devtools/client/locales/en-US/webconsole.properties
@@ -322,8 +322,13 @@ webconsole.filteredMessages.label=#1 ite
 # Label used as the text of the "Reset filters" button in the "filtered messages" bar.
 # It resets the default filters of the console to their original values.
 webconsole.resetFiltersButton.label=Reset filters
 
 # LOCALIZATION NOTE (webconsole.enablePersistentLogs.label)
 webconsole.enablePersistentLogs.label=Persist Logs
 # LOCALIZATION NOTE (webconsole.enablePersistentLogs.tooltip)
 webconsole.enablePersistentLogs.tooltip=If you enable this option the output will not be cleared each time you navigate to a new page
+
+# LOCALIZATION NOTE (webconsole.navigated): this string is used in the console when the
+# current inspected page is navigated to a new location.
+# Parameters: %S is the new URL.
+webconsole.navigated=Navigated to %S
diff --git a/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_persist.js b/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_persist.js
--- a/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_persist.js
+++ b/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_persist.js
@@ -15,20 +15,21 @@ const TEST_URI = "http://example.com/bro
 registerCleanupFunction(() => {
   Services.prefs.clearUserPref("devtools.webconsole.persistlog");
 });
 
 add_task(async function () {
   info("Testing that messages disappear on a refresh if logs aren't persisted");
   let hud = await openNewTabAndConsole(TEST_URI);
 
-  await ContentTask.spawn(gBrowser.selectedBrowser, {}, () => {
-    content.wrappedJSObject.doLogs(5);
+  const INITIAL_LOGS_NUMBER = 5;
+  await ContentTask.spawn(gBrowser.selectedBrowser, INITIAL_LOGS_NUMBER, (count) => {
+    content.wrappedJSObject.doLogs(count);
   });
-  await waitFor(() => findMessages(hud, "").length === 5);
+  await waitFor(() => findMessages(hud, "").length === INITIAL_LOGS_NUMBER);
   ok(true, "Messages showed up initially");
 
   await refreshTab();
   await waitFor(() => findMessages(hud, "").length === 0);
   ok(true, "Messages disappeared");
 
   await closeToolbox();
 });
@@ -36,19 +37,23 @@ add_task(async function () {
 add_task(async function () {
   info("Testing that messages persist on a refresh if logs are persisted");
 
   let hud = await openNewTabAndConsole(TEST_URI);
 
   hud.ui.outputNode.querySelector(".webconsole-filterbar-primary .filter-checkbox")
     .click();
 
-  await ContentTask.spawn(gBrowser.selectedBrowser, {}, () => {
-    content.wrappedJSObject.doLogs(5);
+  const INITIAL_LOGS_NUMBER = 5;
+  await ContentTask.spawn(gBrowser.selectedBrowser, INITIAL_LOGS_NUMBER, (count) => {
+    content.wrappedJSObject.doLogs(count);
   });
-  await waitFor(() => findMessages(hud, "").length === 5);
+  await waitFor(() => findMessages(hud, "").length === INITIAL_LOGS_NUMBER);
   ok(true, "Messages showed up initially");
 
-  await refreshTab();
-  await waitFor(() => findMessages(hud, "").length === 6);
+  const onNavigatedMessage = waitForMessage(hud, "Navigated to");
+  refreshTab();
+  await onNavigatedMessage;
 
-  ok(findMessage(hud, "Navigated"), "Navigated message appeared");
+  ok(true, "Navigation message appeared as expected");
+  is(findMessages(hud, "").length, INITIAL_LOGS_NUMBER + 1,
+    "Messages logged before navigation are still visible");
 });
diff --git a/devtools/client/webconsole/new-console-output/utils/messages.js b/devtools/client/webconsole/new-console-output/utils/messages.js
--- a/devtools/client/webconsole/new-console-output/utils/messages.js
+++ b/devtools/client/webconsole/new-console-output/utils/messages.js
@@ -171,17 +171,17 @@ function transformConsoleAPICallPacket(p
 }
 
 function transformNavigationMessagePacket(packet) {
   let { message } = packet;
   return new ConsoleMessage({
     source: MESSAGE_SOURCE.CONSOLE_API,
     type: MESSAGE_TYPE.LOG,
     level: MESSAGE_LEVEL.LOG,
-    messageText: "Navigated to " + message.url,
+    messageText: l10n.getFormatStr("webconsole.navigated", [message.url]),
     timeStamp: message.timeStamp
   });
 }
 
 function transformLogMessagePacket(packet) {
   let {
     message,
     timeStamp,
