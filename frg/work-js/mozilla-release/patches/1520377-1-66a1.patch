# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1547680709 0
# Node ID e822bc175cee7ba13449081a4864bbec22cf3fc9
# Parent  c2b78a2b53124208733a299a50cf2d0fc1bf87a6
Bug 1520377 - Inline subconfigure.run in js/sub.configure. r=nalexander

Differential Revision: https://phabricator.services.mozilla.com/D16643

diff --git a/build/subconfigure.py b/build/subconfigure.py
--- a/build/subconfigure.py
+++ b/build/subconfigure.py
@@ -69,63 +69,9 @@ def execute_and_prefix(*args, **kwargs):
     proc = subprocess.Popen(*args, stdout=subprocess.PIPE,
                             stderr=subprocess.STDOUT, **kwargs)
     while True:
         line = proc.stdout.readline()
         if not line:
             break
         print(prefix_lines(line.rstrip(), prefix))
         sys.stdout.flush()
-    return proc.wait()
-
-
-def run(data):
-    objdir = data['objdir']
-    relobjdir = data['relobjdir'] = os.path.relpath(objdir, os.getcwd())
-
-    cache_file = data['cache-file']
-
-    # Only run configure if one of the following is true:
-    # - config.status doesn't exist
-    # - config.status is older than configure
-    # - the configure arguments changed
-    configure = mozpath.join(data['srcdir'], 'old-configure')
-    config_status_path = mozpath.join(objdir, 'config.status')
-    skip_configure = True
-    if not os.path.exists(config_status_path):
-        skip_configure = False
-    else:
-        if os.path.getmtime(config_status_path) < os.path.getmtime(configure) or \
-                data.get('previous-args', data['args']) != data['args']:
-            skip_configure = False
-
-    if not skip_configure:
-        # Because configure is a shell script calling a python script
-        # calling a shell script, on Windows, with msys screwing the
-        # environment, we lose the benefits from our own efforts in this
-        # script to get past the msys problems. So manually call the python
-        # script instead, so that we don't do a native->msys transition
-        # here. Then the python configure will still have the right
-        # environment when calling the shell configure.
-        command = [
-            sys.executable,
-            os.path.join(os.path.dirname(__file__), '..', 'configure.py'),
-            '--enable-project=js',
-        ]
-        data['env']['OLD_CONFIGURE'] = os.path.join(
-            os.path.dirname(configure), 'old-configure')
-        command += data['args']
-        command += ['--cache-file=%s' % cache_file]
-
-        print(prefix_lines('configuring', relobjdir))
-        print(prefix_lines('running %s' % ' '.join(command[:-1]), relobjdir))
-        sys.stdout.flush()
-        returncode = execute_and_prefix(command, cwd=objdir, env=data['env'],
-                                        prefix=relobjdir)
-        if returncode:
-            return returncode
-
-        # Leave config.status with a new timestamp if configure is newer than
-        # its original mtime.
-        if config_status_path and os.path.getmtime(configure) <= os.path.getmtime(config_status_path):
-            os.utime(config_status_path, None)
-
-    return 0
\ No newline at end of file
+    return proc.wait()
\ No newline at end of file
diff --git a/js/sub.configure b/js/sub.configure
--- a/js/sub.configure
+++ b/js/sub.configure
@@ -3,16 +3,17 @@
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 @depends(check_build_environment, prepare_configure_options, prepare_mozconfig,
          old_configure, old_configure_assignments, '--cache-file')
 @imports('itertools')
 @imports('os')
 @imports('subconfigure')
 @imports('sys')
+@imports(_from='__builtin__', _import='open')
 @imports(_from='mozbuild.util', _import='encode')
 def js_subconfigure(build_env, prepare_configure_options, mozconfig,
                     old_configure, old_configure_assignments, cache_file):
     substs = dict(old_configure['substs'])
     assignments = dict(old_configure_assignments)
     environ = dict(os.environ)
     if prepare_configure_options.extra_env:
         environ.update(prepare_configure_options.extra_env)
@@ -80,14 +81,61 @@ def js_subconfigure(build_env, prepare_c
 
     options += [
         'JS_STANDALONE=',
         '--cache-file=%s' % (cache_file or './config.cache'),
     ]
     srcdir = os.path.join(build_env.topsrcdir, 'js', 'src')
     objdir = os.path.join(build_env.topobjdir, 'js', 'src')
     data = subconfigure.prepare(srcdir, objdir, options)
-    data['env'] = encode(environ)
-    ret = subconfigure.run(data)
+
+    # subconfigure.prepare does some normalization so use the value it returned.
+    cache_file = data['cache-file']
+
+    # Only run configure if one of the following is true:
+    # - config.status doesn't exist
+    # - config.status is older than an input to configure
+    # - the configure arguments changed
+    configure = os.path.join(srcdir, 'old-configure')
+    config_status_path = os.path.join(objdir, 'config.status')
+    skip_configure = True
+    if not os.path.exists(config_status_path):
+        skip_configure = False
+    else:
+        config_status_deps = os.path.join(objdir, 'config_status_deps.in')
+        if not os.path.exists(config_status_deps):
+            skip_configure = False
+        else:
+            with open(config_status_deps, 'r') as fh:
+                dep_files = fh.read().splitlines() + [configure]
+            if (any(not os.path.exists(f) or
+                    (os.path.getmtime(config_status_path) < os.path.getmtime(f))
+                    for f in dep_files) or
+                data.get('previous-args', data['args']) != data['args']):
+                skip_configure = False
 
-    if ret:
-        log.error('subconfigure failed')
-        sys.exit(ret)
+    if not skip_configure:
+        # Because configure is a shell script calling a python script
+        # calling a shell script, on Windows, with msys screwing the
+        # environment, we lose the benefits from our own efforts in this
+        # script to get past the msys problems. So manually call the python
+        # script instead, so that we don't do a native->msys transition
+        # here. Then the python configure will still have the right
+        # environment when calling the shell configure.
+        command = [
+            sys.executable,
+            os.path.join(build_env.topsrcdir, 'configure.py'),
+            '--enable-project=js',
+        ]
+        environ['OLD_CONFIGURE'] = os.path.join(
+            os.path.dirname(configure), 'old-configure')
+        command += data['args']
+        command += ['--cache-file=%s' % cache_file]
+
+        log.info(subconfigure.prefix_lines('configuring', 'js/src'))
+        log.info(subconfigure.prefix_lines('running %s' % ' '.join(command[:-1]), 'js/src'))
+        ret = subconfigure.execute_and_prefix(command, cwd=objdir, env=encode(environ),
+                                              prefix='js/src')
+        if ret:
+            log.error('subconfigure failed')
+            sys.exit(ret)
+
+    return 0
