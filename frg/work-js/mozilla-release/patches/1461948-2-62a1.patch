# HG changeset patch
# User Lars T Hansen <lhansen@mozilla.com>
# Date 1526463751 -7200
#      Wed May 16 11:42:31 2018 +0200
# Node ID d9de6329f9adb0fe2af88e40b69e22f1d88b86b7
# Parent  7bfcf62f68078e4854bfe41575521207f436a7aa
Bug 1461948 - Abstract the inline/outline TypedObject decision. r=till

diff --git a/js/src/builtin/TypedObject.cpp b/js/src/builtin/TypedObject.cpp
--- a/js/src/builtin/TypedObject.cpp
+++ b/js/src/builtin/TypedObject.cpp
@@ -1519,17 +1519,17 @@ OutlineTypedObject::createDerived(JSCont
     obj->attach(cx, *typedObj, offset);
     return obj;
 }
 
 /*static*/ TypedObject*
 TypedObject::createZeroed(JSContext* cx, HandleTypeDescr descr, gc::InitialHeap heap)
 {
     // If possible, create an object with inline data.
-    if (descr->size() <= InlineTypedObject::MaximumSize) {
+    if (InlineTypedObject::canAccommodateType(descr)) {
         AutoSetNewObjectMetadata metadata(cx);
 
         InlineTypedObject* obj = InlineTypedObject::create(cx, descr, heap);
         if (!obj)
             return nullptr;
         JS::AutoCheckCannotGC nogc(cx);
         descr->initInstances(cx->runtime(), obj->inlineTypedMem(nogc), 1);
         return obj;
@@ -2888,17 +2888,17 @@ TraceListVisitor::fillList(Vector<int32_
 
 static bool
 CreateTraceList(JSContext* cx, HandleTypeDescr descr)
 {
     // Trace lists are only used for inline typed objects. We don't use them
     // for larger objects, both to limit the size of the trace lists and
     // because tracing outline typed objects is considerably more complicated
     // than inline ones.
-    if (descr->size() > InlineTypedObject::MaximumSize || descr->transparent())
+    if (!InlineTypedObject::canAccommodateType(descr) || descr->transparent())
         return true;
 
     TraceListVisitor visitor;
     visitReferences(*descr, nullptr, visitor);
 
     Vector<int32_t> entries(cx);
     if (!visitor.fillList(entries))
         return false;
diff --git a/js/src/builtin/TypedObject.h b/js/src/builtin/TypedObject.h
--- a/js/src/builtin/TypedObject.h
+++ b/js/src/builtin/TypedObject.h
@@ -695,25 +695,33 @@ class OutlineOpaqueTypedObject : public 
 // Class for a typed object whose data is allocated inline.
 class InlineTypedObject : public TypedObject
 {
     friend class TypedObject;
 
     // Start of the inline data, which immediately follows the shape and type.
     uint8_t data_[1];
 
+    static const size_t MaximumSize = JSObject::MAX_BYTE_SIZE - sizeof(TypedObject);
+
   protected:
     uint8_t* inlineTypedMem() const {
         return (uint8_t*) &data_;
     }
 
   public:
-    static const size_t MaximumSize = JSObject::MAX_BYTE_SIZE - sizeof(TypedObject);
+    static inline gc::AllocKind allocKindForTypeDescriptor(TypeDescr* descr);
 
-    static inline gc::AllocKind allocKindForTypeDescriptor(TypeDescr* descr);
+    static bool canAccommodateSize(size_t size) {
+        return size <= MaximumSize;
+    }
+
+    static bool canAccommodateType(TypeDescr* type) {
+        return type->size() <= MaximumSize;
+    }
 
     uint8_t* inlineTypedMem(const JS::AutoRequireNoGC&) const {
         return inlineTypedMem();
     }
 
     uint8_t* inlineTypedMemForGC() const {
         return inlineTypedMem();
     }
diff --git a/js/src/jit/MCallOptimize.cpp b/js/src/jit/MCallOptimize.cpp
--- a/js/src/jit/MCallOptimize.cpp
+++ b/js/src/jit/MCallOptimize.cpp
@@ -3765,17 +3765,17 @@ IonBuilder::InliningResult
 IonBuilder::inlineConstructTypedObject(CallInfo& callInfo, TypeDescr* descr)
 {
     // Only inline default constructors for now.
     if (callInfo.argc() != 0) {
         trackOptimizationOutcome(TrackedOutcome::CantInlineNativeBadForm);
         return InliningStatus_NotInlined;
     }
 
-    if (size_t(descr->size()) > InlineTypedObject::MaximumSize)
+    if (!InlineTypedObject::canAccommodateType(descr))
         return InliningStatus_NotInlined;
 
     JSObject* obj = inspector->getTemplateObjectForClassHook(pc, descr->getClass());
     if (!obj || !obj->is<InlineTypedObject>())
         return InliningStatus_NotInlined;
 
     InlineTypedObject* templateObject = &obj->as<InlineTypedObject>();
     if (&templateObject->typeDescr() != descr)
@@ -3991,17 +3991,17 @@ IonBuilder::inlineConstructSimdObject(Ca
     MIRType simdType;
     if (!MaybeSimdTypeToMIRType(descr->type(), &simdType)) {
         trackOptimizationOutcome(TrackedOutcome::SimdTypeNotOptimized);
         return InliningStatus_NotInlined;
     }
 
     // Take the templateObject out of Baseline ICs, such that we can box
     // SIMD value type in the same kind of objects.
-    MOZ_ASSERT(size_t(descr->size(descr->type())) < InlineTypedObject::MaximumSize);
+    MOZ_ASSERT(InlineTypedObject::canAccommodateType(descr));
     MOZ_ASSERT(descr->getClass() == &SimdTypeDescr::class_,
                "getTemplateObjectForSimdCtor needs an update");
 
     JSObject* templateObject = inspector->getTemplateObjectForSimdCtor(pc, descr->type());
     if (!templateObject)
         return InliningStatus_NotInlined;
 
     // The previous assertion ensures this will never fail if we were able to
