# HG changeset patch
# User Ryan VanderMeulen <ryanvm@gmail.com>
# Date 1563225180 0
#      Mon Jul 15 21:13:00 2019 +0000
# Node ID b097a2426f3d2611d3931240978acf3c269dd5a2
# Parent  944e89d3e2ef2c7c650e25f21b828650e5121b9a
Bug 1566032 - Update jsoncpp to version 1.9.1. r=gsvelto

Differential Revision: https://phabricator.services.mozilla.com/D38060

diff --git a/toolkit/components/jsoncpp/GIT-INFO b/toolkit/components/jsoncpp/GIT-INFO
--- a/toolkit/components/jsoncpp/GIT-INFO
+++ b/toolkit/components/jsoncpp/GIT-INFO
@@ -1,2 +1,2 @@
-3c32dca89214c03b107cc9d1c468000cff3f8127
+645250b6690785be60ab6780ce4b58698d884d11
 
diff --git a/toolkit/components/jsoncpp/include/json/config.h b/toolkit/components/jsoncpp/include/json/config.h
--- a/toolkit/components/jsoncpp/include/json/config.h
+++ b/toolkit/components/jsoncpp/include/json/config.h
@@ -25,16 +25,21 @@
 //#  define JSON_USE_CPPTL_SMALLMAP 1
 
 // If non-zero, the library uses exceptions to report bad input instead of C
 // assertion macros. The default is to use exceptions.
 #ifndef JSON_USE_EXCEPTION
 #define JSON_USE_EXCEPTION 1
 #endif
 
+// Temporary, tracked for removal with issue #982.
+#ifndef JSON_USE_NULLREF
+#define JSON_USE_NULLREF 1
+#endif
+
 /// If defined, indicates that the source file is amalgamated
 /// to prevent private header inclusion.
 /// Remarks: it is automatically defined in the generated amalgamated header.
 // #define JSON_IS_AMALGAMATION
 
 #ifdef JSON_IN_CPPTL
 #include <cpptl/config.h>
 #ifndef JSON_USE_CPPTL
diff --git a/toolkit/components/jsoncpp/include/json/reader.h b/toolkit/components/jsoncpp/include/json/reader.h
--- a/toolkit/components/jsoncpp/include/json/reader.h
+++ b/toolkit/components/jsoncpp/include/json/reader.h
@@ -373,17 +373,17 @@ public:
 
 /** Consume entire stream and use its begin/end.
  * Someday we might have a real StreamReader, but for now this
  * is convenient.
  */
 bool JSON_API parseFromStream(CharReader::Factory const&,
                               IStream&,
                               Value* root,
-                              std::string* errs);
+                              String* errs);
 
 /** \brief Read from 'sin' into 'root'.
 
  Always keep comments from the input JSON.
 
  This can be used to read a file into a particular sub-object.
  For example:
  \code
diff --git a/toolkit/components/jsoncpp/include/json/value.h b/toolkit/components/jsoncpp/include/json/value.h
--- a/toolkit/components/jsoncpp/include/json/value.h
+++ b/toolkit/components/jsoncpp/include/json/value.h
@@ -19,29 +19,16 @@
 #include <map>
 #else
 #include <cpptl/smallmap.h>
 #endif
 #ifdef JSON_USE_CPPTL
 #include <cpptl/forwards.h>
 #endif
 
-// Conditional NORETURN attribute on the throw functions would:
-// a) suppress false positives from static code analysis
-// b) possibly improve optimization opportunities.
-#if !defined(JSONCPP_NORETURN)
-#if defined(_MSC_VER)
-#define JSONCPP_NORETURN __declspec(noreturn)
-#elif defined(__GNUC__)
-#define JSONCPP_NORETURN __attribute__((__noreturn__))
-#else
-#define JSONCPP_NORETURN
-#endif
-#endif
-
 // Disable warning C4251: <data member>: <type> needs to have dll-interface to
 // be used by...
 #if defined(JSONCPP_DISABLE_DLL_INTERFACE_WARNING)
 #pragma warning(push)
 #pragma warning(disable : 4251)
 #endif // if defined(JSONCPP_DISABLE_DLL_INTERFACE_WARNING)
 
 #pragma pack(push, 8)
@@ -84,19 +71,19 @@ public:
  */
 class JSON_API LogicError : public Exception {
 public:
   LogicError(String const& msg);
 };
 #endif
 
 /// used internally
-JSONCPP_NORETURN void throwRuntimeError(String const& msg);
+[[noreturn]] void throwRuntimeError(String const& msg);
 /// used internally
-JSONCPP_NORETURN void throwLogicError(String const& msg);
+[[noreturn]] void throwLogicError(String const& msg);
 
 /** \brief Type of the value held by a Value object.
  */
 enum ValueType {
   nullValue = 0, ///< 'null' value
   intValue,      ///< signed integer value
   uintValue,     ///< unsigned integer value
   realValue,     ///< double value
@@ -201,21 +188,24 @@ public:
 #endif // defined(JSON_HAS_INT64)
   typedef Json::LargestInt LargestInt;
   typedef Json::LargestUInt LargestUInt;
   typedef Json::ArrayIndex ArrayIndex;
 
   // Required for boost integration, e. g. BOOST_TEST
   typedef std::string value_type;
 
-  static const Value& null; ///< We regret this reference to a global instance;
-                            ///< prefer the simpler Value().
-  static const Value& nullRef; ///< just a kludge for binary-compatibility; same
-                               ///< as null
-  static Value const& nullSingleton(); ///< Prefer this to null or nullRef.
+#if JSON_USE_NULLREF
+  // Binary compatibility kludges, do not use.
+  static const Value& null;
+  static const Value& nullRef;
+#endif
+
+  // null and nullRef are deprecated, use this instead.
+  static Value const& nullSingleton();
 
   /// Minimum signed integer value that can be stored in a Json::Value.
   static const LargestInt minLargestInt;
   /// Maximum signed integer value that can be stored in a Json::Value.
   static const LargestInt maxLargestInt;
   /// Maximum unsigned integer value that can be stored in a Json::Value.
   static const LargestUInt maxLargestUInt;
 
diff --git a/toolkit/components/jsoncpp/include/json/version.h b/toolkit/components/jsoncpp/include/json/version.h
--- a/toolkit/components/jsoncpp/include/json/version.h
+++ b/toolkit/components/jsoncpp/include/json/version.h
@@ -1,17 +1,17 @@
 // DO NOT EDIT. This file (and "version") is a template used by the build system
 // (either CMake or Meson) to generate a "version.h" header file.
 #ifndef JSON_VERSION_H_INCLUDED
 #define JSON_VERSION_H_INCLUDED
 
-#define JSONCPP_VERSION_STRING "1.9.0"
+#define JSONCPP_VERSION_STRING "1.9.1"
 #define JSONCPP_VERSION_MAJOR 1
 #define JSONCPP_VERSION_MINOR 9
-#define JSONCPP_VERSION_PATCH 0
+#define JSONCPP_VERSION_PATCH 1
 #define JSONCPP_VERSION_QUALIFIER
 #define JSONCPP_VERSION_HEXA ((JSONCPP_VERSION_MAJOR << 24) \
                             | (JSONCPP_VERSION_MINOR << 16) \
                             | (JSONCPP_VERSION_PATCH << 8))
 
 #ifdef JSONCPP_USING_SECURE_MEMORY
 #undef JSONCPP_USING_SECURE_MEMORY
 #endif
diff --git a/toolkit/components/jsoncpp/src/lib_json/json_reader.cpp b/toolkit/components/jsoncpp/src/lib_json/json_reader.cpp
--- a/toolkit/components/jsoncpp/src/lib_json/json_reader.cpp
+++ b/toolkit/components/jsoncpp/src/lib_json/json_reader.cpp
@@ -1043,16 +1043,17 @@ bool OurReader::parse(const char* beginD
   lastValue_ = nullptr;
   commentsBefore_.clear();
   errors_.clear();
   while (!nodes_.empty())
     nodes_.pop();
   nodes_.push(&root);
 
   bool successful = readValue();
+  nodes_.pop();
   Token token;
   skipCommentTokens(token);
   if (features_.failIfExtra_) {
     if ((features_.strictRoot_ || token.type_ != tokenError) &&
         token.type_ != tokenEndOfStream) {
       addError("Extra non-whitespace after JSON value.", token);
       return false;
     }
diff --git a/toolkit/components/jsoncpp/src/lib_json/json_value.cpp b/toolkit/components/jsoncpp/src/lib_json/json_value.cpp
--- a/toolkit/components/jsoncpp/src/lib_json/json_value.cpp
+++ b/toolkit/components/jsoncpp/src/lib_json/json_value.cpp
@@ -49,17 +49,16 @@ int JSON_API msvc_pre1900_c99_snprintf(c
 // Disable warning C4702 : unreachable code
 #if defined(_MSC_VER)
 #pragma warning(disable : 4702)
 #endif
 
 #define JSON_ASSERT_UNREACHABLE assert(false)
 
 namespace Json {
-
 template <typename T>
 static std::unique_ptr<T> cloneUnique(const std::unique_ptr<T>& p) {
   std::unique_ptr<T> r;
   if (p) {
     r = std::unique_ptr<T>(new T(*p));
   }
   return r;
 }
@@ -67,31 +66,32 @@ static std::unique_ptr<T> cloneUnique(co
 // This is a walkaround to avoid the static initialization of Value::null.
 // kNull must be word-aligned to avoid crashing on ARM.  We use an alignment of
 // 8 (instead of 4) as a bit of future-proofing.
 #if defined(__ARMEL__)
 #define ALIGNAS(byte_alignment) __attribute__((aligned(byte_alignment)))
 #else
 #define ALIGNAS(byte_alignment)
 #endif
-// static const unsigned char ALIGNAS(8) kNull[sizeof(Value)] = { 0 };
-// const unsigned char& kNullRef = kNull[0];
-// const Value& Value::null = reinterpret_cast<const Value&>(kNullRef);
-// const Value& Value::nullRef = null;
 
 // static
 Value const& Value::nullSingleton() {
   static Value const nullStatic;
   return nullStatic;
 }
 
+#if JSON_USE_NULLREF
 // for backwards compatibility, we'll leave these global references around, but
 // DO NOT use them in JSONCPP library code any more!
+// static
 Value const& Value::null = Value::nullSingleton();
+
+// static
 Value const& Value::nullRef = Value::nullSingleton();
+#endif
 
 const Int Value::minInt = Int(~(UInt(-1) / 2));
 const Int Value::maxInt = Int(UInt(-1) / 2);
 const UInt Value::maxUInt = UInt(-1);
 #if defined(JSON_HAS_INT64)
 const Int64 Value::minInt64 = Int64(~(UInt64(-1) / 2));
 const Int64 Value::maxInt64 = Int64(UInt64(-1) / 2);
 const UInt64 Value::maxUInt64 = UInt64(-1);
@@ -227,25 +227,25 @@ static inline void releaseStringValue(ch
 namespace Json {
 
 #if JSON_USE_EXCEPTION
 Exception::Exception(String msg) : msg_(std::move(msg)) {}
 Exception::~Exception() JSONCPP_NOEXCEPT {}
 char const* Exception::what() const JSONCPP_NOEXCEPT { return msg_.c_str(); }
 RuntimeError::RuntimeError(String const& msg) : Exception(msg) {}
 LogicError::LogicError(String const& msg) : Exception(msg) {}
-JSONCPP_NORETURN void throwRuntimeError(String const& msg) {
+[[noreturn]] void throwRuntimeError(String const& msg) {
   throw RuntimeError(msg);
 }
-JSONCPP_NORETURN void throwLogicError(String const& msg) {
+[[noreturn]] void throwLogicError(String const& msg) {
   throw LogicError(msg);
 }
 #else // !JSON_USE_EXCEPTION
-JSONCPP_NORETURN void throwRuntimeError(String const& msg) { abort(); }
-JSONCPP_NORETURN void throwLogicError(String const& msg) { abort(); }
+[[noreturn]] void throwRuntimeError(String const& msg) { abort(); }
+[[noreturn]] void throwLogicError(String const& msg) { abort(); }
 #endif
 
 // //////////////////////////////////////////////////////////////////
 // //////////////////////////////////////////////////////////////////
 // //////////////////////////////////////////////////////////////////
 // class Value::CZString
 // //////////////////////////////////////////////////////////////////
 // //////////////////////////////////////////////////////////////////
@@ -1643,30 +1643,30 @@ void Path::invalidPath(const String& /*p
   // Error: invalid path.
 }
 
 const Value& Path::resolve(const Value& root) const {
   const Value* node = &root;
   for (const auto& arg : args_) {
     if (arg.kind_ == PathArgument::kindIndex) {
       if (!node->isArray() || !node->isValidIndex(arg.index_)) {
-        // Error: unable to resolve path (array value expected at position...
-        return Value::null;
+        // Error: unable to resolve path (array value expected at position... )
+        return Value::nullSingleton();
       }
       node = &((*node)[arg.index_]);
     } else if (arg.kind_ == PathArgument::kindKey) {
       if (!node->isObject()) {
         // Error: unable to resolve path (object value expected at position...)
-        return Value::null;
+        return Value::nullSingleton();
       }
       node = &((*node)[arg.key_]);
       if (node == &Value::nullSingleton()) {
         // Error: unable to resolve path (object has no member named '' at
         // position...)
-        return Value::null;
+        return Value::nullSingleton();
       }
     }
   }
   return *node;
 }
 
 Value Path::resolve(const Value& root, const Value& defaultValue) const {
   const Value* node = &root;
diff --git a/toolkit/components/jsoncpp/version.txt b/toolkit/components/jsoncpp/version.txt
--- a/toolkit/components/jsoncpp/version.txt
+++ b/toolkit/components/jsoncpp/version.txt
@@ -1,1 +1,1 @@
-1.9.0
+1.9.1
