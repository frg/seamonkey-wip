# HG changeset patch
# User Olli Pettay <Olli.Pettay@helsinki.fi>
# Date 1521828361 -7200
# Node ID 342633799d0850f79cf27db8c826cdc4149a2cd9
# Parent  5887a8bcd1308dc8d80ea421b525c12795113deb
Bug 1445207, populate input type=date's .value when changing the value in the UI, r=mconley

diff --git a/dom/html/test/forms/test_input_date_key_events.html b/dom/html/test/forms/test_input_date_key_events.html
--- a/dom/html/test/forms/test_input_date_key_events.html
+++ b/dom/html/test/forms/test_input_date_key_events.html
@@ -190,16 +190,22 @@ var testData = [
     expectedVal: "2016-01-01"
   },
   {
     // End key should have no effect on year field.
     keys: ["KEY_Tab", "KEY_Tab", "KEY_End"],
     initialVal: "2016-01-01",
     expectedVal: "2016-01-01"
   },
+  {
+    // Incomplete value maps to empty .value.
+    keys: ["1111"],
+    initialVal: "",
+    expectedVal: ""
+  },
 ];
 
 function sendKeys(aKeys) {
   for (let i = 0; i < aKeys.length; i++) {
     let key = aKeys[i];
     if (key.startsWith("KEY_")) {
       synthesizeKey(key);
     } else {
@@ -210,20 +216,20 @@ function sendKeys(aKeys) {
 
 function test() {
   var elem = document.getElementById("input");
 
   for (let { keys, initialVal, expectedVal } of testData) {
     elem.focus();
     elem.value = initialVal;
     sendKeys(keys);
-    elem.blur();
     is(elem.value, expectedVal,
        "Test with " + keys + ", result should be " + expectedVal);
     elem.value = "";
+    elem.blur();
   }
 
   function chromeListener(e) {
     ok(false, "Picker should not be opened when dispatching untrusted click.");
   }
   SpecialPowers.addChromeEventListener("MozOpenDateTimePicker",
     chromeListener);
   input.click();
diff --git a/dom/html/test/forms/test_input_datetime_input_change_events.html b/dom/html/test/forms/test_input_datetime_input_change_events.html
--- a/dom/html/test/forms/test_input_datetime_input_change_events.html
+++ b/dom/html/test/forms/test_input_datetime_input_change_events.html
@@ -65,28 +65,29 @@ function test() {
     synthesizeKey("KEY_ArrowDown");
     is(input.value, expectedValues[i][0], "Check that value was set correctly (1).");
     is(changeEvents[i], 1, "Change event should be dispatched (1).");
     is(inputEvents[i], 1, "Input event should ne dispatched (1).");
 
     // Test that change and input events are fired when changing the value with
     // the keyboard.
     sendString("01");
+    // We get event per character.
     is(input.value, expectedValues[i][1], "Check that value was set correctly (2).");
-    is(changeEvents[i], 2, "Change event should be dispatched (2).");
-    is(inputEvents[i], 2, "Input event should be dispatched (2).");
+    is(changeEvents[i], 3, "Change event should be dispatched (2).");
+    is(inputEvents[i], 3, "Input event should be dispatched (2).");
 
     // Reset button is desktop only.
     if (isDesktop) {
       // Test that change and input events are fired when clearing the value using
       // the reset button.
       synthesizeMouse(input, resetButton_X, resetButton_Y, {});
       is(input.value, "", "Check that value was set correctly (3).");
-      is(changeEvents[i], 3, "Change event should be dispatched (3).");
-      is(inputEvents[i], 3, "Input event should be dispatched (3).");
+      is(changeEvents[i], 4, "Change event should be dispatched (3).");
+      is(inputEvents[i], 4, "Input event should be dispatched (3).");
     }
   }
 }
 
 </script>
 </pre>
 </body>
 </html>
diff --git a/dom/html/test/forms/test_input_time_key_events.html b/dom/html/test/forms/test_input_time_key_events.html
--- a/dom/html/test/forms/test_input_time_key_events.html
+++ b/dom/html/test/forms/test_input_time_key_events.html
@@ -159,16 +159,22 @@ var testData = [
     expectedVal: "16:00:00"
   },
   {
     // End key on second field sets it to the minimum second, which is 59.
     keys: ["KEY_Tab", "KEY_Tab", "KEY_End"],
     initialVal: "16:00:30",
     expectedVal: "16:00:59"
   },
+  {
+    // Incomplete value maps to empty .value.
+    keys: ["1"],
+    initialVal: "",
+    expectedVal: ""
+  },
 ];
 
 function sendKeys(aKeys) {
   for (let i = 0; i < aKeys.length; i++) {
     let key = aKeys[i];
     if (key.startsWith("KEY_")) {
       synthesizeKey(key);
     } else {
@@ -179,19 +185,19 @@ function sendKeys(aKeys) {
 
 function test() {
   var elem = document.getElementById("input");
 
   for (let { keys, initialVal, expectedVal } of testData) {
     elem.focus();
     elem.value = initialVal;
     sendKeys(keys);
-    elem.blur();
     is(elem.value, expectedVal,
        "Test with " + keys + ", result should be " + expectedVal);
     elem.value = "";
+    elem.blur();
   }
 }
 
 </script>
 </pre>
 </body>
 </html>
diff --git a/toolkit/content/widgets/datetimebox.xml b/toolkit/content/widgets/datetimebox.xml
--- a/toolkit/content/widgets/datetimebox.xml
+++ b/toolkit/content/widgets/datetimebox.xml
@@ -248,16 +248,17 @@
             let n = Number(buffer);
             let max = targetField.getAttribute("max");
             let maxLength = targetField.getAttribute("maxlength");
             if (buffer.length >= maxLength || n * 10 > max) {
               buffer = "";
               this.advanceToNextField();
             }
             targetField.setAttribute("typeBuffer", buffer);
+            this.setInputValueFromFields();
           }
         ]]>
         </body>
       </method>
 
       <method name="incrementFieldValue">
         <parameter name="aTargetField"/>
         <parameter name="aTimes"/>
@@ -1024,16 +1025,17 @@
 
           if (this.hasDayPeriodField() &&
               targetField == this.mDayPeriodField) {
             if (key == "a" || key == "A") {
               this.setDayPeriodValue(this.mAMIndicator);
             } else if (key == "p" || key == "P") {
               this.setDayPeriodValue(this.mPMIndicator);
             }
+            this.setInputValueFromFields();
             return;
           }
 
           if (targetField.classList.contains("numeric") && key.match(/[0-9]/)) {
             let buffer = targetField.getAttribute("typeBuffer") || "";
 
             buffer = buffer.concat(key);
             this.setFieldValue(targetField, buffer);
@@ -1041,16 +1043,17 @@
             let n = Number(buffer);
             let max = targetField.getAttribute("max");
             let maxLength = targetField.getAttribute("maxlength");
             if (buffer.length >= maxLength || n * 10 > max) {
               buffer = "";
               this.advanceToNextField();
             }
             targetField.setAttribute("typeBuffer", buffer);
+            this.setInputValueFromFields();
           }
         ]]>
         </body>
       </method>
 
       <method name="setFieldValue">
        <parameter name="aField"/>
        <parameter name="aValue"/>
