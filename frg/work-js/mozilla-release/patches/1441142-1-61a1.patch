# HG changeset patch
# User Lars T Hansen <lhansen@mozilla.com>
# Date 1516615994 -3600
# Node ID 62541e869aa2ce268b6a3ad78d6ac7094435c2c7
# Parent  32fdbeda9305b05dab9695bb0415b0fbaeab084f
Bug 1441142 - Adapt wasm frame code for ARM64.  r=bbouvier

Since the SP behaves unusually on ARM64 we get architecture-specific
code paths for the prologue, the epilogue, and the unwinding, and the
Frame must also be 16-byte aligned.

diff --git a/js/src/wasm/WasmFrameIter.cpp b/js/src/wasm/WasmFrameIter.cpp
--- a/js/src/wasm/WasmFrameIter.cpp
+++ b/js/src/wasm/WasmFrameIter.cpp
@@ -284,23 +284,31 @@ static const unsigned PoppedTLSReg = 0;
 static const unsigned BeforePushRetAddr = 0;
 static const unsigned PushedRetAddr = 4;
 static const unsigned PushedTLS = 8;
 static const unsigned PushedFP = 12;
 static const unsigned SetFP = 16;
 static const unsigned PoppedFP = 4;
 static const unsigned PoppedTLSReg = 0;
 #elif defined(JS_CODEGEN_ARM64)
+// On ARM64 we do not use push or pop; the prologues and epilogues are
+// structured differently due to restrictions on SP alignment.  Even so,
+// PushedRetAddr, PushedTLS, and PushedFP are used in some restricted contexts
+// and must be superficially meaningful.
 static const unsigned BeforePushRetAddr = 0;
-static const unsigned PushedRetAddr = 0;
-static const unsigned PushedTLS = 1;
-static const unsigned PushedFP = 1;
-static const unsigned SetFP = 0;
-static const unsigned PoppedFP = 0;
-static const unsigned PoppedTLSReg = 0;
+static const unsigned PushedRetAddr = 8;
+static const unsigned PushedTLS = 12;
+static const unsigned PushedFP = 16;
+static const unsigned SetFP = 20;
+static const unsigned PoppedFP = 8;
+static const unsigned PoppedTLSReg = 4;
+static_assert(BeforePushRetAddr == 0, "Required by StartUnwinding");
+static_assert(PushedFP > PushedRetAddr, "Required by StartUnwinding");
+static_assert(PushedFP > PushedTLS, "Required by StartUnwinding");
+static_assert(PoppedFP > PoppedTLSReg, "Required by StartUnwinding");
 #elif defined(JS_CODEGEN_MIPS32) || defined(JS_CODEGEN_MIPS64)
 static const unsigned PushedRetAddr = 8;
 static const unsigned PushedTLS = 12;
 static const unsigned PushedFP = 16;
 static const unsigned SetFP = 20;
 static const unsigned PoppedFP = 8;
 static const unsigned PoppedTLSReg = 4;
 #elif defined(JS_CODEGEN_NONE)
@@ -354,27 +362,48 @@ GenerateCallablePrologue(MacroAssembler&
     masm.setFramePushed(0);
 
     // ProfilingFrameIterator needs to know the offsets of several key
     // instructions from entry. To save space, we make these offsets static
     // constants and assert that they match the actual codegen below. On ARM,
     // this requires AutoForbidPools to prevent a constant pool from being
     // randomly inserted between two instructions.
 #if defined(JS_CODEGEN_MIPS32) || defined(JS_CODEGEN_MIPS64)
+    {
         *entry = masm.currentOffset();
 
         masm.subFromStackPtr(Imm32(sizeof(Frame)));
         masm.storePtr(ra, Address(StackPointer, offsetof(Frame, returnAddress)));
         MOZ_ASSERT_IF(!masm.oom(), PushedRetAddr == masm.currentOffset() - *entry);
         masm.storePtr(WasmTlsReg, Address(StackPointer, offsetof(Frame, tls)));
         MOZ_ASSERT_IF(!masm.oom(), PushedTLS == masm.currentOffset() - *entry);
         masm.storePtr(FramePointer, Address(StackPointer, offsetof(Frame, callerFP)));
         MOZ_ASSERT_IF(!masm.oom(), PushedFP == masm.currentOffset() - *entry);
         masm.moveStackPtrTo(FramePointer);
         MOZ_ASSERT_IF(!masm.oom(), SetFP == masm.currentOffset() - *entry);
+    }
+#elif defined(JS_CODEGEN_ARM64)
+    {
+        // We do not use the PseudoStackPointer.
+        MOZ_ASSERT(masm.GetStackPointer64().code() == sp.code());
+
+        AutoForbidPools afp(&masm, /* number of instructions in scope = */ 5);
+
+        *entry = masm.currentOffset();
+
+        masm.Sub(sp, sp, sizeof(Frame));
+        masm.Str(ARMRegister(lr, 64), MemOperand(sp, offsetof(Frame, returnAddress)));
+        MOZ_ASSERT_IF(!masm.oom(), PushedRetAddr == masm.currentOffset() - *entry);
+        masm.Str(ARMRegister(WasmTlsReg, 64), MemOperand(sp, offsetof(Frame, tls)));
+        MOZ_ASSERT_IF(!masm.oom(), PushedTLS == masm.currentOffset() - *entry);
+        masm.Str(ARMRegister(FramePointer, 64), MemOperand(sp, offsetof(Frame, callerFP)));
+        MOZ_ASSERT_IF(!masm.oom(), PushedFP == masm.currentOffset() - *entry);
+        masm.Mov(ARMRegister(FramePointer, 64), sp);
+        MOZ_ASSERT_IF(!masm.oom(), SetFP == masm.currentOffset() - *entry);
+    }
 #else
     {
 # if defined(JS_CODEGEN_ARM)
         AutoForbidPools afp(&masm, /* number of instructions in scope = */ 7);
 
         *entry = masm.currentOffset();
 
         MOZ_ASSERT(BeforePushRetAddr == 0);
@@ -415,16 +444,35 @@ GenerateCallableEpilogue(MacroAssembler&
     masm.loadPtr(Address(StackPointer, offsetof(Frame, tls)), WasmTlsReg);
     poppedTlsReg = masm.currentOffset();
     masm.loadPtr(Address(StackPointer, offsetof(Frame, returnAddress)), ra);
 
     *ret = masm.currentOffset();
     masm.as_jr(ra);
     masm.addToStackPtr(Imm32(sizeof(Frame)));
 
+#elif defined(JS_CODEGEN_ARM64)
+
+    // We do not use the PseudoStackPointer.
+    MOZ_ASSERT(masm.GetStackPointer64().code() == sp.code());
+
+    AutoForbidPools afp(&masm, /* number of instructions in scope = */ 5);
+
+    masm.Ldr(ARMRegister(FramePointer, 64), MemOperand(sp, offsetof(Frame, callerFP)));
+    poppedFP = masm.currentOffset();
+
+    masm.Ldr(ARMRegister(WasmTlsReg, 64), MemOperand(sp, offsetof(Frame, tls)));
+    poppedTlsReg = masm.currentOffset();
+
+    masm.Ldr(ARMRegister(lr, 64), MemOperand(sp, offsetof(Frame, returnAddress)));
+    *ret = masm.currentOffset();
+
+    masm.Add(sp, sp, sizeof(Frame));
+    masm.Ret(ARMRegister(lr, 64));
+
 #else
     // Forbid pools for the same reason as described in GenerateCallablePrologue.
 # if defined(JS_CODEGEN_ARM)
     AutoForbidPools afp(&masm, /* number of instructions in scope = */ 7);
 # endif
 
     // There is an important ordering constraint here: fp must be repointed to
     // the caller's frame before any field of the frame currently pointed to by
@@ -632,16 +680,24 @@ wasm::GenerateJitEntryPrologue(MacroAsse
 #if defined(JS_CODEGEN_ARM)
         AutoForbidPools afp(&masm, /* number of instructions in scope = */ 2);
         offsets->begin = masm.currentOffset();
         MOZ_ASSERT(BeforePushRetAddr == 0);
         masm.push(lr);
 #elif defined(JS_CODEGEN_MIPS32) || defined(JS_CODEGEN_MIPS64)
         offsets->begin = masm.currentOffset();
         masm.push(ra);
+#elif defined(JS_CODEGEN_ARM64)
+        AutoForbidPools afp(&masm, /* number of instructions in scope = */ 3);
+        offsets->begin = masm.currentOffset();
+        MOZ_ASSERT(BeforePushRetAddr == 0);
+        // Subtract from SP first as SP must be aligned before offsetting.
+        masm.Sub(sp, sp, 8);
+        masm.storePtr(lr, Address(masm.getStackPointer(), 0));
+        masm.adjustFrame(8);
 #else
         // The x86/x64 call instruction pushes the return address.
         offsets->begin = masm.currentOffset();
 #endif
         MOZ_ASSERT_IF(!masm.oom(), PushedRetAddr == masm.currentOffset() - offsets->begin);
 
         // Save jit frame pointer, so unwinding from wasm to jit frames is trivial.
         masm.moveStackPtrTo(FramePointer);
@@ -836,25 +892,38 @@ js::wasm::StartUnwinding(const RegisterS
       case CodeRange::FarJumpIsland:
       case CodeRange::ImportJitExit:
       case CodeRange::ImportInterpExit:
       case CodeRange::BuiltinThunk:
       case CodeRange::OldTrapExit:
       case CodeRange::DebugTrap:
 #if defined(JS_CODEGEN_MIPS32) || defined(JS_CODEGEN_MIPS64)
         if (offsetFromEntry < PushedFP || codeRange->isThunk()) {
-            // On MIPS we relay on register state instead of state saved on
+            // On MIPS we rely on register state instead of state saved on
             // stack until the wasm::Frame is completely built.
             // On entry the return address is in ra (registers.lr) and
             // fp holds the caller's fp.
             fixedPC = (uint8_t*) registers.lr;
             fixedFP = fp;
             AssertMatchesCallSite(fixedPC, fixedFP);
         } else
-#elif defined(JS_CODEGEN_ARM) || defined(JS_CODEGEN_ARM64)
+#elif defined(JS_CODEGEN_ARM64)
+        if (offsetFromEntry < PushedFP || codeRange->isThunk()) {
+            // Constraints above ensure that this covers BeforePushRetAddr,
+            // PushedRetAddr, and PushedTLS.
+            //
+            // On ARM64 we subtract the size of the Frame from SP and then store
+            // values into the stack.  Execution can be interrupted at various
+            // places in that sequence.  We rely on the register state for our
+            // values.
+            fixedPC = (uint8_t*) registers.lr;
+            fixedFP = fp;
+            AssertMatchesCallSite(fixedPC, fixedFP);
+        } else
+#elif defined(JS_CODEGEN_ARM)
         if (offsetFromEntry == BeforePushRetAddr || codeRange->isThunk()) {
             // The return address is still in lr and fp holds the caller's fp.
             fixedPC = (uint8_t*) registers.lr;
             fixedFP = fp;
             AssertMatchesCallSite(fixedPC, fixedFP);
         } else
 #endif
         if (offsetFromEntry == PushedRetAddr || codeRange->isThunk()) {
@@ -882,16 +951,25 @@ js::wasm::StartUnwinding(const RegisterS
             (void)PoppedTLSReg;
             // The fixedFP field of the Frame has been loaded into fp.
             // The ra and TLS might also be loaded, but the Frame structure is
             // still on stack, so we can acess the ra form there.
             MOZ_ASSERT(fp == reinterpret_cast<Frame*>(sp)->callerFP);
             fixedPC = reinterpret_cast<Frame*>(sp)->returnAddress;
             fixedFP = fp;
             AssertMatchesCallSite(fixedPC, fixedFP);
+#elif defined(JS_CODEGEN_ARM64)
+        // The stack pointer does not move until all values have
+        // been restored so several cases can be coalesced here.
+        } else if (offsetInCode >= codeRange->ret() - PoppedFP &&
+                   offsetInCode <= codeRange->ret())
+        {
+            fixedPC = reinterpret_cast<Frame*>(sp)->returnAddress;
+            fixedFP = fp;
+            AssertMatchesCallSite(fixedPC, fixedFP);
 #else
         } else if (offsetInCode >= codeRange->ret() - PoppedFP &&
                    offsetInCode < codeRange->ret() - PoppedTLSReg)
         {
             // The fixedFP field of the Frame has been popped into fp.
             fixedPC = sp[1];
             fixedFP = fp;
             AssertMatchesCallSite(fixedPC, fixedFP);
@@ -935,17 +1013,18 @@ js::wasm::StartUnwinding(const RegisterS
         // The entry trampoline is the final frame in an wasm JitActivation. The
         // entry trampoline also doesn't GeneratePrologue/Epilogue so we can't
         // use the general unwinding logic above.
         break;
       case CodeRange::JitEntry:
         // There's a jit frame above the current one; we don't care about pc
         // since the Jit entry frame is a jit frame which can be considered as
         // an exit frame.
-#if defined(JS_CODEGEN_ARM) || defined(JS_CODEGEN_MIPS32) || defined(JS_CODEGEN_MIPS64)
+#if defined(JS_CODEGEN_ARM) || defined(JS_CODEGEN_ARM64) || \
+    defined(JS_CODEGEN_MIPS32) || defined(JS_CODEGEN_MIPS64)
         if (offsetFromEntry < PushedRetAddr) {
             // We haven't pushed the jit return address yet, thus the jit
             // frame is incomplete. During profiling frame iteration, it means
             // that the jit profiling frame iterator won't be able to unwind
             // this frame; drop it.
             return false;
         }
 #endif
diff --git a/js/src/wasm/WasmTypes.cpp b/js/src/wasm/WasmTypes.cpp
--- a/js/src/wasm/WasmTypes.cpp
+++ b/js/src/wasm/WasmTypes.cpp
@@ -587,16 +587,22 @@ DebugFrame::alignmentStaticAsserts()
     // VS2017 doesn't consider offsetOfFrame() to be a constexpr, so we have
     // to use offsetof directly. These asserts can't be at class-level
     // because the type is incomplete.
 
     static_assert(WasmStackAlignment >= Alignment,
                   "Aligned by ABI before pushing DebugFrame");
     static_assert((offsetof(DebugFrame, frame_) + sizeof(Frame)) % Alignment == 0,
                   "Aligned after pushing DebugFrame");
+#ifdef JS_CODEGEN_ARM64
+    // This constraint may or may not be necessary.  If you hit this because
+    // you've changed the frame size then feel free to remove it, but be extra
+    // aware of possible problems.
+    static_assert(sizeof(DebugFrame) % 16 == 0, "ARM64 SP alignment");
+#endif
 }
 
 GlobalObject*
 DebugFrame::global() const
 {
     return &instance()->object()->global();
 }
 
diff --git a/js/src/wasm/WasmTypes.h b/js/src/wasm/WasmTypes.h
--- a/js/src/wasm/WasmTypes.h
+++ b/js/src/wasm/WasmTypes.h
@@ -1880,30 +1880,38 @@ struct Frame
     // The caller's Frame*. See GenerateCallableEpilogue for why this must be
     // the first field of wasm::Frame (in a downward-growing stack).
     Frame* callerFP;
 
     // The saved value of WasmTlsReg on entry to the function. This is
     // effectively the callee's instance.
     TlsData* tls;
 
-#if defined(JS_CODEGEN_MIPS32)
-    // Double word aligned frame ensures correct alignment for wasm locals
-    // on architectures that require the stack alignment to be more than word size.
+#if defined(JS_CODEGEN_MIPS32) || defined(JS_CODEGEN_ARM64)
+    // Double word aligned frame ensures:
+    // - correct alignment for wasm locals on architectures that require the
+    //   stack alignment to be more than word size.
+    // - correct stack alignment on architectures that require the SP alignment
+    //   to be more than word size.
     uintptr_t padding_;
 #endif
+
     // The return address pushed by the call (in the case of ARM/MIPS the return
     // address is pushed by the first instruction of the prologue).
     void* returnAddress;
 
     // Helper functions:
 
     Instance* instance() const { return tls->instance; }
 };
 
+#if defined(JS_CODEGEN_ARM64)
+static_assert(sizeof(Frame) % 16 == 0, "frame size");
+#endif
+
 // A DebugFrame is a Frame with additional fields that are added after the
 // normal function prologue by the baseline compiler. If a Module is compiled
 // with debugging enabled, then all its code creates DebugFrames on the stack
 // instead of just Frames. These extra fields are used by the Debugger API.
 
 class DebugFrame
 {
     // The results field left uninitialized and only used during the baseline

