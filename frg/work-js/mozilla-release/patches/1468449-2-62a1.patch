# HG changeset patch
# User Jeff Walden <jwalden@mit.edu>
# Date 1528866762 25200
#      Tue Jun 12 22:12:42 2018 -0700
# Node ID bace341bdc89502a4d643be9321b6e00bc4ae7d5
# Parent  9b3683e49b733bf9302e01e308709b4d272381e2
Bug 1468449 - Use std::uninitialized_copy_n to copy over BindingNames in a parser-lifetime Data to a long-lived Scope.  r=jonco

diff --git a/js/src/frontend/Parser.cpp b/js/src/frontend/Parser.cpp
--- a/js/src/frontend/Parser.cpp
+++ b/js/src/frontend/Parser.cpp
@@ -1762,17 +1762,17 @@ Parser<FullParseHandler, CharT>::checkSt
     return true;
 }
 
 template <typename Scope>
 typename Scope::Data*
 NewEmptyBindingData(JSContext* cx, LifoAlloc& alloc, uint32_t numBindings)
 {
     using Data = typename Scope::Data;
-    size_t allocSize = Scope::sizeOfData(numBindings);
+    size_t allocSize = SizeOfData<typename Scope::Data>(numBindings);
     auto* bindings = alloc.allocInSize<Data>(allocSize, numBindings);
     if (!bindings)
         ReportOutOfMemory(cx);
     return bindings;
 }
 
 /**
  * Copy-construct |BindingName|s from |bindings| into |cursor|, then return
diff --git a/js/src/vm/Scope.cpp b/js/src/vm/Scope.cpp
--- a/js/src/vm/Scope.cpp
+++ b/js/src/vm/Scope.cpp
@@ -1,16 +1,19 @@
 /* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
  * vim: set ts=8 sts=4 et sw=4 tw=99:
  * This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "vm/Scope.h"
 
+#include <memory>
+#include <new>
+
 #include "builtin/ModuleObject.h"
 #include "gc/Allocator.h"
 #include "gc/FreeOp.h"
 #include "util/StringBuffer.h"
 #include "vm/EnvironmentObject.h"
 #include "vm/JSScript.h"
 #include "wasm/WasmInstance.h"
 
@@ -138,42 +141,34 @@ CreateEnvironmentShape(JSContext* cx, Bi
 }
 
 template <typename ConcreteScope>
 static UniquePtr<typename ConcreteScope::Data>
 CopyScopeData(JSContext* cx, Handle<typename ConcreteScope::Data*> data)
 {
     // Make sure the binding names are marked in the context's zone, if we are
     // copying data from another zone.
-    BindingName* names = nullptr;
-    uint32_t length = 0;
-    ConcreteScope::getDataNamesAndLength(data, &names, &length);
+    BindingName* names = data->trailingNames.start();
+    uint32_t length = data->length;
     for (size_t i = 0; i < length; i++) {
         if (JSAtom* name = names[i].name())
             cx->markAtom(name);
     }
 
-    size_t dataSize = ConcreteScope::sizeOfData(data->length);
-    size_t headerSize = sizeof(typename ConcreteScope::Data);
-    MOZ_ASSERT(dataSize >= headerSize);
-    size_t extraSize = dataSize - headerSize;
-
-    uint8_t* copyBytes = cx->zone()->pod_malloc<uint8_t>(dataSize);
-    if (!copyBytes) {
+    size_t size = SizeOfData<typename ConcreteScope::Data>(data->length);
+    void* bytes = cx->zone()->pod_malloc<char>(size);
+    if (!bytes) {
         ReportOutOfMemory(cx);
         return nullptr;
     }
 
-    auto dataCopy = reinterpret_cast<typename ConcreteScope::Data*>(copyBytes);
-    new (dataCopy) typename ConcreteScope::Data(*data);
+    auto* dataCopy = new (bytes) typename ConcreteScope::Data(*data);
 
-    uint8_t* extra = reinterpret_cast<uint8_t*>(data.get()) + headerSize;
-    uint8_t* extraCopy = copyBytes + headerSize;
+    std::uninitialized_copy_n(names, length, dataCopy->trailingNames.start());
 
-    mozilla::PodCopy<uint8_t>(extraCopy, extra, extraSize);
     return UniquePtr<typename ConcreteScope::Data>(dataCopy);
 }
 
 template <typename ConcreteScope>
 static bool
 PrepareScopeData(JSContext* cx, BindingIter& bi, Handle<UniquePtr<typename ConcreteScope::Data>> data,
                  const Class* cls, uint32_t baseShapeFlags, MutableHandleShape envShape)
 {
@@ -198,17 +193,18 @@ PrepareScopeData(JSContext* cx, BindingI
 
     return true;
 }
 
 template <typename ConcreteScope>
 static UniquePtr<typename ConcreteScope::Data>
 NewEmptyScopeData(JSContext* cx, uint32_t length = 0)
 {
-    uint8_t* bytes = cx->zone()->pod_malloc<uint8_t>(ConcreteScope::sizeOfData(length));
+    size_t dataSize = SizeOfData<typename ConcreteScope::Data>(length);
+    uint8_t* bytes = cx->zone()->pod_malloc<uint8_t>(dataSize);
     if (!bytes)
         ReportOutOfMemory(cx);
     auto data = reinterpret_cast<typename ConcreteScope::Data*>(bytes);
     if (data)
         new (data) typename ConcreteScope::Data(length);
     return UniquePtr<typename ConcreteScope::Data>(data);
 }
 
diff --git a/js/src/vm/Scope.h b/js/src/vm/Scope.h
--- a/js/src/vm/Scope.h
+++ b/js/src/vm/Scope.h
@@ -3,18 +3,21 @@
  * This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef vm_Scope_h
 #define vm_Scope_h
 
 #include "mozilla/Maybe.h"
+#include "mozilla/TypeTraits.h"
 #include "mozilla/Variant.h"
 
+#include <stddef.h>
+
 #include "jsutil.h"
 
 #include "gc/DeletePolicy.h"
 #include "gc/Heap.h"
 #include "gc/Policy.h"
 #include "js/UbiNode.h"
 #include "js/UniquePtr.h"
 #include "vm/BytecodeUtil.h"
@@ -399,16 +402,30 @@ class Scope : public js::gc::TenuredCell
     void traceChildren(JSTracer* trc);
     void finalize(FreeOp* fop);
 
     size_t sizeOfExcludingThis(mozilla::MallocSizeOf mallocSizeOf) const;
 
     void dump();
 };
 
+/** Empty base class for scope Data classes to inherit from. */
+class BaseScopeData
+{};
+
+template<class Data>
+inline size_t
+SizeOfData(uint32_t numBindings)
+{
+    static_assert(mozilla::IsBaseOf<BaseScopeData, Data>::value,
+                  "Data must be the correct sort of data, i.e. it must "
+                  "inherit from BaseScopeData");
+    return sizeof(Data) + (numBindings ? numBindings - 1 : 0) * sizeof(BindingName);
+}
+
 //
 // A lexical scope that holds let and const bindings. There are 4 kinds of
 // LexicalScopes.
 //
 // Lexical
 //   A plain lexical scope.
 //
 // SimpleCatch
@@ -428,17 +445,17 @@ class Scope : public js::gc::TenuredCell
 class LexicalScope : public Scope
 {
     friend class Scope;
     friend class BindingIter;
 
   public:
     // Data is public because it is created by the frontend. See
     // Parser<FullParseHandler>::newLexicalScopeData.
-    struct Data
+    struct Data : BaseScopeData
     {
         // Bindings are sorted by kind in both frames and environments.
         //
         //   lets - [0, constStart)
         // consts - [constStart, length)
         uint32_t constStart = 0;
         uint32_t length = 0;
 
@@ -451,25 +468,16 @@ class LexicalScope : public Scope
         TrailingNamesArray trailingNames;
 
         explicit Data(size_t nameCount) : trailingNames(nameCount) {}
         Data() = delete;
 
         void trace(JSTracer* trc);
     };
 
-    static size_t sizeOfData(uint32_t length) {
-        return sizeof(Data) + (length ? length - 1 : 0) * sizeof(BindingName);
-    }
-
-    static void getDataNamesAndLength(Data* data, BindingName** names, uint32_t* length) {
-        *names = data->trailingNames.start();
-        *length = data->length;
-    }
-
     static LexicalScope* create(JSContext* cx, ScopeKind kind, Handle<Data*> data,
                                 uint32_t firstFrameSlot, HandleScope enclosing);
 
     template <XDRMode mode>
     static XDRResult XDR(XDRState<mode>* xdr, ScopeKind kind, HandleScope enclosing,
                     MutableHandleScope scope);
 
   private:
@@ -534,17 +542,17 @@ class FunctionScope : public Scope
     friend class BindingIter;
     friend class PositionalFormalParameterIter;
     friend class Scope;
     static const ScopeKind classScopeKind_ = ScopeKind::Function;
 
   public:
     // Data is public because it is created by the
     // frontend. See Parser<FullParseHandler>::newFunctionScopeData.
-    struct Data
+    struct Data : BaseScopeData
     {
         // The canonical function of the scope, as during a scope walk we
         // often query properties of the JSFunction (e.g., is the function an
         // arrow).
         GCPtrFunction canonicalFunction = {};
 
         // If parameter expressions are present, parameters act like lexical
         // bindings.
@@ -577,25 +585,16 @@ class FunctionScope : public Scope
 
         explicit Data(size_t nameCount) : trailingNames(nameCount) {}
         Data() = delete;
 
         void trace(JSTracer* trc);
         Zone* zone() const;
     };
 
-    static size_t sizeOfData(uint32_t length) {
-        return sizeof(Data) + (length ? length - 1 : 0) * sizeof(BindingName);
-    }
-
-    static void getDataNamesAndLength(Data* data, BindingName** names, uint32_t* length) {
-        *names = data->trailingNames.start();
-        *length = data->length;
-    }
-
     static FunctionScope* create(JSContext* cx, Handle<Data*> data,
                                  bool hasParameterExprs, bool needsEnvironment,
                                  HandleFunction fun, HandleScope enclosing);
 
     static FunctionScope* clone(JSContext* cx, Handle<FunctionScope*> scope, HandleFunction fun,
                                 HandleScope enclosing);
 
     template <XDRMode mode>
@@ -662,17 +661,17 @@ class VarScope : public Scope
 {
     friend class GCMarker;
     friend class BindingIter;
     friend class Scope;
 
   public:
     // Data is public because it is created by the
     // frontend. See Parser<FullParseHandler>::newVarScopeData.
-    struct Data
+    struct Data : BaseScopeData
     {
         // All bindings are vars.
         uint32_t length = 0;
 
         // Frame slots [firstFrameSlot(), nextFrameSlot) are live when this is
         // the innermost scope.
         uint32_t nextFrameSlot = 0;
 
@@ -681,25 +680,16 @@ class VarScope : public Scope
         TrailingNamesArray trailingNames;
 
         explicit Data(size_t nameCount) : trailingNames(nameCount) {}
         Data() = delete;
 
         void trace(JSTracer* trc);
     };
 
-    static size_t sizeOfData(uint32_t length) {
-        return sizeof(Data) + (length ? length - 1 : 0) * sizeof(BindingName);
-    }
-
-    static void getDataNamesAndLength(Data* data, BindingName** names, uint32_t* length) {
-        *names = data->trailingNames.start();
-        *length = data->length;
-    }
-
     static VarScope* create(JSContext* cx, ScopeKind kind, Handle<Data*> data,
                             uint32_t firstFrameSlot, bool needsEnvironment,
                             HandleScope enclosing);
 
     template <XDRMode mode>
     static XDRResult XDR(XDRState<mode>* xdr, ScopeKind kind, HandleScope enclosing,
                     MutableHandleScope scope);
 
@@ -755,17 +745,17 @@ Scope::is<VarScope>() const
 class GlobalScope : public Scope
 {
     friend class Scope;
     friend class BindingIter;
 
   public:
     // Data is public because it is created by the frontend. See
     // Parser<FullParseHandler>::newGlobalScopeData.
-    struct Data
+    struct Data : BaseScopeData
     {
         // Bindings are sorted by kind.
         // `vars` includes top-level functions which is distinguished by a bit
         // on the BindingName.
         //
         //            vars - [0, letStart)
         //            lets - [letStart, constStart)
         //          consts - [constStart, length)
@@ -778,25 +768,16 @@ class GlobalScope : public Scope
         TrailingNamesArray trailingNames;
 
         explicit Data(size_t nameCount) : trailingNames(nameCount) {}
         Data() = delete;
 
         void trace(JSTracer* trc);
     };
 
-    static size_t sizeOfData(uint32_t length) {
-        return sizeof(Data) + (length ? length - 1 : 0) * sizeof(BindingName);
-    }
-
-    static void getDataNamesAndLength(Data* data, BindingName** names, uint32_t* length) {
-        *names = data->trailingNames.start();
-        *length = data->length;
-    }
-
     static GlobalScope* create(JSContext* cx, ScopeKind kind, Handle<Data*> data);
 
     static GlobalScope* createEmpty(JSContext* cx, ScopeKind kind) {
         return create(cx, kind, nullptr);
     }
 
     static GlobalScope* clone(JSContext* cx, Handle<GlobalScope*> scope, ScopeKind kind);
 
@@ -860,17 +841,17 @@ class WithScope : public Scope
 class EvalScope : public Scope
 {
     friend class Scope;
     friend class BindingIter;
 
   public:
     // Data is public because it is created by the frontend. See
     // Parser<FullParseHandler>::newEvalScopeData.
-    struct Data
+    struct Data : BaseScopeData
     {
         // All bindings in an eval script are 'var' bindings. The implicit
         // lexical scope around the eval is present regardless of strictness
         // and is its own LexicalScope.
         // `vars` includes top-level functions which is distinguished by a bit
         // on the BindingName.
         //
         //            vars - [0, length)
@@ -886,25 +867,16 @@ class EvalScope : public Scope
         TrailingNamesArray trailingNames;
 
         explicit Data(size_t nameCount) : trailingNames(nameCount) {}
         Data() = delete;
 
         void trace(JSTracer* trc);
     };
 
-    static size_t sizeOfData(uint32_t length) {
-        return sizeof(Data) + (length ? length - 1 : 0) * sizeof(BindingName);
-    }
-
-    static void getDataNamesAndLength(Data* data, BindingName** names, uint32_t* length) {
-        *names = data->trailingNames.start();
-        *length = data->length;
-    }
-
     static EvalScope* create(JSContext* cx, ScopeKind kind, Handle<Data*> data,
                              HandleScope enclosing);
 
     template <XDRMode mode>
     static XDRResult XDR(XDRState<mode>* xdr, ScopeKind kind, HandleScope enclosing,
                     MutableHandleScope scope);
 
   private:
@@ -965,17 +937,17 @@ class ModuleScope : public Scope
     friend class GCMarker;
     friend class BindingIter;
     friend class Scope;
     static const ScopeKind classScopeKind_ = ScopeKind::Module;
 
   public:
     // Data is public because it is created by the frontend. See
     // Parser<FullParseHandler>::newModuleScopeData.
-    struct Data
+    struct Data : BaseScopeData
     {
         // The module of the scope.
         GCPtr<ModuleObject*> module = {};
 
         // Bindings are sorted by kind.
         //
         // imports - [0, varStart)
         //    vars - [varStart, letStart)
@@ -996,25 +968,16 @@ class ModuleScope : public Scope
 
         explicit Data(size_t nameCount) : trailingNames(nameCount) {}
         Data() = delete;
 
         void trace(JSTracer* trc);
         Zone* zone() const;
     };
 
-    static size_t sizeOfData(uint32_t length) {
-        return sizeof(Data) + (length ? length - 1 : 0) * sizeof(BindingName);
-    }
-
-    static void getDataNamesAndLength(Data* data, BindingName** names, uint32_t* length) {
-        *names = data->trailingNames.start();
-        *length = data->length;
-    }
-
     static ModuleScope* create(JSContext* cx, Handle<Data*> data,
                                Handle<ModuleObject*> module, HandleScope enclosing);
 
   private:
     static ModuleScope* createWithData(JSContext* cx, MutableHandle<UniquePtr<Data>> data,
                                        Handle<ModuleObject*> module, HandleScope enclosing);
 
     Data& data() {
@@ -1041,17 +1004,17 @@ class ModuleScope : public Scope
 
 class WasmInstanceScope : public Scope
 {
     friend class BindingIter;
     friend class Scope;
     static const ScopeKind classScopeKind_ = ScopeKind::WasmInstance;
 
   public:
-    struct Data
+    struct Data : BaseScopeData
     {
         uint32_t memoriesStart = 0;
         uint32_t globalsStart = 0;
         uint32_t length = 0;
         uint32_t nextFrameSlot = 0;
 
         // The wasm instance of the scope.
         GCPtr<WasmInstanceObject*> instance = {};
@@ -1061,20 +1024,16 @@ class WasmInstanceScope : public Scope
         explicit Data(size_t nameCount) : trailingNames(nameCount) {}
         Data() = delete;
 
         void trace(JSTracer* trc);
     };
 
     static WasmInstanceScope* create(JSContext* cx, WasmInstanceObject* instance);
 
-    static size_t sizeOfData(uint32_t length) {
-        return sizeof(Data) + (length ? length - 1 : 0) * sizeof(BindingName);
-    }
-
   private:
     Data& data() {
         return *reinterpret_cast<Data*>(data_);
     }
 
     const Data& data() const {
         return *reinterpret_cast<Data*>(data_);
     }
@@ -1104,36 +1063,32 @@ class WasmInstanceScope : public Scope
 //
 class WasmFunctionScope : public Scope
 {
     friend class BindingIter;
     friend class Scope;
     static const ScopeKind classScopeKind_ = ScopeKind::WasmFunction;
 
   public:
-    struct Data
+    struct Data : BaseScopeData
     {
         uint32_t length = 0;
         uint32_t nextFrameSlot = 0;
         uint32_t funcIndex = 0;
 
         TrailingNamesArray trailingNames;
 
         explicit Data(size_t nameCount) : trailingNames(nameCount) {}
         Data() = delete;
 
         void trace(JSTracer* trc);
     };
 
     static WasmFunctionScope* create(JSContext* cx, HandleScope enclosing, uint32_t funcIndex);
 
-    static size_t sizeOfData(uint32_t length) {
-        return sizeof(Data) + (length ? length - 1 : 0) * sizeof(BindingName);
-    }
-
   private:
     Data& data() {
         return *reinterpret_cast<Data*>(data_);
     }
 
     const Data& data() const {
         return *reinterpret_cast<Data*>(data_);
     }
