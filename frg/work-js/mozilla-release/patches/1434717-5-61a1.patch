# HG changeset patch
# User Matthew Gaudet <mgaudet@mozilla.com>
# Date 1517585923 18000
#      Fri Feb 02 10:38:43 2018 -0500
# Node ID ea25dec22fd06218d1e084980f269a01096959b0
# Parent  1a83c0ee76b4401bc02eb2dbef340f0e4468ed73
Bug 1434717: Part 5: Connect UnaryArith IC to IonMonkey r=tcampbell

diff --git a/js/src/jit-test/tests/cacheir/unaryarith.js b/js/src/jit-test/tests/cacheir/unaryarith.js
--- a/js/src/jit-test/tests/cacheir/unaryarith.js
+++ b/js/src/jit-test/tests/cacheir/unaryarith.js
@@ -1,23 +1,29 @@
-function warmup(fun, input, output) {
-    for (var i = 0; i < 30; i++) {
-        var y = fun(input);
-        assertEq(y, output)
+setJitCompilerOption('ion.forceinlineCaches', 1);
+
+function warmup(fun, input_array, output_array) {
+    assertEq(output_array.length, input_array.length);
+    for (var index = 0; index < input_array.length; index++) {
+        input = input_array[index];
+        output = output_array[index];
+        for (var i = 0; i < 30; i++) {
+            var y = fun(input);
+            assertEq(y, output)
+        }
     }
 }
 
 var fun1 = (x) => { return -x; }
 var fun2 = (x) => { return -x; }
 
 var fun3 = (x) => { return ~x; }
 var fun4 = (x) => { return ~x; }
 
-warmup(fun1, 1, -1);
-warmup(fun1, 0, -0);
+warmup(fun1, [1, 2], [-1, -2]);
+warmup(fun2, [0], [-0]);
 
-warmup(fun2, 3, -3);
-warmup(fun2, 1.2, -1.2);
+warmup(fun2, [3, 4], [-3, -4]);
+warmup(fun1, [1.2, 1.4], [-1.2, -1.4]);
 
-warmup(fun3, -1, 0);
-warmup(fun4, 1.2, -2);
-warmup(fun4, 3, -4)
+warmup(fun3, [-1, 0], [0, -1]);
+warmup(fun4, [-1.0, 0.0, 1.2, 3], [0, -1, -2, -4]);
 
diff --git a/js/src/jit/CodeGenerator.cpp b/js/src/jit/CodeGenerator.cpp
--- a/js/src/jit/CodeGenerator.cpp
+++ b/js/src/jit/CodeGenerator.cpp
@@ -170,22 +170,26 @@ static const VMFunction IonBindNameICInf
 typedef JSObject* (*IonGetIteratorICFn)(JSContext*, HandleScript, IonGetIteratorIC*, HandleValue);
 static const VMFunction IonGetIteratorICInfo =
     FunctionInfo<IonGetIteratorICFn>(IonGetIteratorIC::update, "IonGetIteratorIC::update");
 
 typedef bool (*IonInICFn)(JSContext*, HandleScript, IonInIC*, HandleValue, HandleObject, bool*);
 static const VMFunction IonInICInfo =
     FunctionInfo<IonInICFn>(IonInIC::update, "IonInIC::update");
 
-
 typedef bool (*IonInstanceOfICFn)(JSContext*, HandleScript, IonInstanceOfIC*,
                          HandleValue lhs, HandleObject rhs, bool* res);
 static const VMFunction IonInstanceOfInfo =
     FunctionInfo<IonInstanceOfICFn>(IonInstanceOfIC::update, "IonInstanceOfIC::update");
 
+typedef bool (*IonUnaryArithICFn)(JSContext* cx, HandleScript outerScript, IonUnaryArithIC* stub,
+                                    HandleValue val, MutableHandleValue res);
+static const VMFunction IonUnaryArithICInfo =
+    FunctionInfo<IonUnaryArithICFn>(IonUnaryArithIC::update, "IonUnaryArithIC::update");
+
 void
 CodeGenerator::visitOutOfLineICFallback(OutOfLineICFallback* ool)
 {
     LInstruction* lir = ool->lir();
     size_t cacheIndex = ool->cacheIndex();
     size_t cacheInfoIndex = ool->cacheInfoIndex();
 
     DataPtr<IonIC> ic(this, cacheIndex);
@@ -352,17 +356,32 @@ CodeGenerator::visitOutOfLineICFallback(
         callVM(IonInstanceOfInfo, lir);
 
         StoreRegisterTo(hasInstanceOfIC->output()).generate(this);
         restoreLiveIgnore(lir, StoreRegisterTo(hasInstanceOfIC->output()).clobbered());
 
         masm.jump(ool->rejoin());
         return;
       }
-      case CacheKind::UnaryArith:
+      case CacheKind::UnaryArith: {
+        IonUnaryArithIC* unaryArithIC = ic->asUnaryArithIC();
+
+        saveLive(lir);
+
+        pushArg(unaryArithIC->input());
+        icInfo_[cacheInfoIndex].icOffsetForPush = pushArgWithPatch(ImmWord(-1));
+        pushArg(ImmGCPtr(gen->info().script()));
+        callVM(IonUnaryArithICInfo, lir);
+
+        StoreValueTo(unaryArithIC->output()).generate(this);
+        restoreLiveIgnore(lir, StoreValueTo(unaryArithIC->output()).clobbered());
+
+        masm.jump(ool->rejoin());
+        return;
+      }
       case CacheKind::Call:
       case CacheKind::Compare:
       case CacheKind::TypeOf:
       case CacheKind::ToBool:
       case CacheKind::GetIntrinsic:
         MOZ_CRASH("Unsupported IC");
     }
     MOZ_CRASH();
@@ -2748,16 +2767,26 @@ CodeGenerator::visitBinarySharedStub(LBi
       case JSOP_STRICTNE:
         emitSharedStub(ICStub::Kind::Compare_Fallback, lir);
         break;
       default:
         MOZ_CRASH("Unsupported jsop in shared stubs.");
     }
 }
 
+void
+CodeGenerator::visitUnaryCache(LUnaryCache* lir)
+{
+    LiveRegisterSet liveRegs = lir->safepoint()->liveRegs();
+    TypedOrValueRegister input = TypedOrValueRegister(ToValue(lir, LUnaryCache::Input));
+    ValueOperand output = ToOutValue(lir);
+
+    IonUnaryArithIC ic(liveRegs, input, output);
+    addIC(lir, allocateIC(ic));
+}
 
 void
 CodeGenerator::visitNullarySharedStub(LNullarySharedStub* lir)
 {
     jsbytecode* pc = lir->mir()->resumePoint()->pc();
     JSOp jsop = JSOp(*pc);
     switch (jsop) {
       case JSOP_NEWARRAY: {
diff --git a/js/src/jit/IonBuilder.cpp b/js/src/jit/IonBuilder.cpp
--- a/js/src/jit/IonBuilder.cpp
+++ b/js/src/jit/IonBuilder.cpp
@@ -3266,16 +3266,20 @@ IonBuilder::jsop_bitnot()
     MDefinition* input = current->pop();
 
     if (!forceInlineCaches()) {
         MOZ_TRY(bitnotTrySpecialized(&emitted, input));
         if(emitted)
             return Ok();
     }
 
+    MOZ_TRY(arithTrySharedStub(&emitted, JSOP_BITNOT, nullptr, input));
+    if (emitted)
+        return Ok();
+
     // Not possible to optimize. Do a slow vm call.
     MBitNot* ins = MBitNot::New(alloc(), input);
 
     current->add(ins);
     current->push(ins);
     MOZ_ASSERT(ins->isEffectful());
     return resumeAfter(ins);
 }
@@ -3528,22 +3532,26 @@ IonBuilder::arithTrySharedStub(bool* emi
 
     if (JitOptions.disableSharedStubs)
         return Ok();
 
     // The actual jsop 'jsop_pos' is not supported yet.
     if (actualOp == JSOP_POS)
         return Ok();
 
-    // JSOP_NEG and JSOP_BITNOT can't be handled here right now.
-    if (actualOp == JSOP_NEG || actualOp == JSOP_BITNOT)
-        return Ok();
 
     MInstruction* stub = nullptr;
     switch (actualOp) {
+      case JSOP_NEG:
+      case JSOP_BITNOT:
+        MOZ_ASSERT_IF(op == JSOP_MUL,
+                      left->maybeConstantValue() && left->maybeConstantValue()->toInt32() == -1);
+        MOZ_ASSERT_IF(op != JSOP_MUL, !left);
+        stub = MUnaryCache::New(alloc(), right);
+        break;
       case JSOP_ADD:
       case JSOP_SUB:
       case JSOP_MUL:
       case JSOP_DIV:
       case JSOP_MOD:
       case JSOP_POW:
         stub = MBinarySharedStub::New(alloc(), left, right);
         break;
diff --git a/js/src/jit/IonCacheIRCompiler.cpp b/js/src/jit/IonCacheIRCompiler.cpp
--- a/js/src/jit/IonCacheIRCompiler.cpp
+++ b/js/src/jit/IonCacheIRCompiler.cpp
@@ -540,22 +540,34 @@ IonCacheIRCompiler::init()
         outputUnchecked_.emplace(TypedOrValueRegister(MIRType::Boolean, AnyRegister(output)));
 
         MOZ_ASSERT(numInputs == 2);
         allocator.initInputLocation(0, ic->lhs());
         allocator.initInputLocation(1, TypedOrValueRegister(MIRType::Object,
                                                             AnyRegister(ic->rhs())));
         break;
       }
+      case CacheKind::UnaryArith: {
+        IonUnaryArithIC *ic = ic_->asUnaryArithIC();
+        ValueOperand output = ic->output();
+
+        available.add(output);
+
+        liveRegs_.emplace(ic->liveRegs());
+        outputUnchecked_.emplace(TypedOrValueRegister(output));
+
+        MOZ_ASSERT(numInputs == 1);
+        allocator.initInputLocation(0, ic->input());
+        break;
+      }
       case CacheKind::Call:
       case CacheKind::Compare:
       case CacheKind::TypeOf:
       case CacheKind::ToBool:
       case CacheKind::GetIntrinsic:
-      case CacheKind::UnaryArith:
         MOZ_CRASH("Unsupported IC");
     }
 
     if (liveRegs_)
         liveFloatRegs_ = LiveFloatRegisterSet(liveRegs_->fpus());
 
     allocator.initAvailableRegs(available);
     allocator.initAvailableRegsAfterSpill();
diff --git a/js/src/jit/IonIC.cpp b/js/src/jit/IonIC.cpp
--- a/js/src/jit/IonIC.cpp
+++ b/js/src/jit/IonIC.cpp
@@ -51,22 +51,23 @@ IonIC::scratchRegisterForEntryJump()
       case CacheKind::In:
         return asInIC()->temp();
       case CacheKind::HasOwn:
         return asHasOwnIC()->output();
       case CacheKind::GetIterator:
         return asGetIteratorIC()->temp1();
       case CacheKind::InstanceOf:
         return asInstanceOfIC()->output();
+      case CacheKind::UnaryArith:
+        return asUnaryArithIC()->output().scratchReg();
       case CacheKind::Call:
       case CacheKind::Compare:
       case CacheKind::TypeOf:
       case CacheKind::ToBool:
       case CacheKind::GetIntrinsic:
-      case CacheKind::UnaryArith:
         MOZ_CRASH("Unsupported IC");
     }
 
     MOZ_CRASH("Invalid kind");
 }
 
 void
 IonIC::discardStubs(Zone* zone)
@@ -506,16 +507,58 @@ IonInstanceOfIC::update(JSContext* cx, H
 
         if (!attached)
             ic->state().trackNotAttached();
     }
 
     return HasInstance(cx, rhs, lhs, res);
 }
 
+/*  static */  bool
+IonUnaryArithIC::update(JSContext* cx, HandleScript outerScript, IonUnaryArithIC* ic,
+                        HandleValue val, MutableHandleValue res)
+{
+    IonScript* ionScript = outerScript->ionScript();
+    RootedScript script(cx, ic->script());
+    jsbytecode* pc = ic->pc();
+    JSOp op = JSOp(*pc);
+
+    switch (op) {
+      case JSOP_BITNOT: {
+        int32_t result;
+        if (!BitNot(cx, val, &result))
+            return false;
+        res.setInt32(result);
+        break;
+      }
+      case JSOP_NEG:
+        if (!NegOperation(cx, val, res))
+            return false;
+        break;
+      default:
+        MOZ_CRASH("Unexpected op");
+    }
+
+    if (ic->state().maybeTransition())
+        ic->discardStubs(cx->zone());
+
+    if (ic->state().canAttachStub()) {
+        bool attached = false;
+        UnaryArithIRGenerator gen(cx, script, pc, ic->state().mode(), op, val, res);
+
+        if (gen.tryAttachStub())
+             ic->attachCacheIRStub(cx, gen.writerRef(), gen.cacheKind(), ionScript, &attached);
+
+        if (!attached)
+            ic->state().trackNotAttached();
+    }
+
+    return true;
+}
+
 uint8_t*
 IonICStub::stubDataStart()
 {
     return reinterpret_cast<uint8_t*>(this) + stubInfo_->stubDataOffset();
 }
 
 void
 IonIC::attachStub(IonICStub* newStub, JitCode* code)
diff --git a/js/src/jit/IonIC.h b/js/src/jit/IonIC.h
--- a/js/src/jit/IonIC.h
+++ b/js/src/jit/IonIC.h
@@ -60,16 +60,17 @@ class IonGetPropertyIC;
 class IonSetPropertyIC;
 class IonGetPropSuperIC;
 class IonGetNameIC;
 class IonBindNameIC;
 class IonGetIteratorIC;
 class IonHasOwnIC;
 class IonInIC;
 class IonInstanceOfIC;
+class IonUnaryArithIC;
 
 class IonIC
 {
     // This either points at the OOL path for the fallback path, or the code for
     // the first stub.
     uint8_t* codeRaw_;
 
     // The first optimized stub, or nullptr.
@@ -167,16 +168,20 @@ class IonIC
     IonInIC* asInIC() {
         MOZ_ASSERT(kind_ == CacheKind::In);
         return (IonInIC*)this;
     }
     IonInstanceOfIC* asInstanceOfIC() {
         MOZ_ASSERT(kind_ == CacheKind::InstanceOf);
         return (IonInstanceOfIC*)this;
     }
+    IonUnaryArithIC* asUnaryArithIC() {
+        MOZ_ASSERT(kind_ == CacheKind::UnaryArith);
+        return (IonUnaryArithIC*)this;
+    }
 
     void updateBaseAddress(JitCode* code);
 
     // Returns the Register to use as scratch when entering IC stubs. This
     // should either be an output register or a temp.
     Register scratchRegisterForEntryJump();
 
     void trace(JSTracer* trc);
@@ -470,12 +475,36 @@ class IonInstanceOfIC : public IonIC
     Register rhs() const { return rhs_; }
     Register output() const { return output_; }
 
     // This signature mimics that of TryAttachInstanceOfStub in baseline
     static MOZ_MUST_USE bool update(JSContext* cx, HandleScript outerScript, IonInstanceOfIC* ic,
                                     HandleValue lhs, HandleObject rhs, bool* attached);
 };
 
+class IonUnaryArithIC : public IonIC
+{
+    LiveRegisterSet liveRegs_;
+
+    TypedOrValueRegister input_;
+    ValueOperand output_;
+
+    public:
+
+    IonUnaryArithIC(LiveRegisterSet liveRegs, TypedOrValueRegister input,  ValueOperand output)
+      : IonIC(CacheKind::UnaryArith),
+        liveRegs_(liveRegs),
+        input_(input),
+        output_(output)
+    { }
+
+    LiveRegisterSet liveRegs() const { return liveRegs_; }
+    TypedOrValueRegister input() const { return input_; }
+    ValueOperand output() const { return output_; }
+
+    static MOZ_MUST_USE bool update(JSContext* cx, HandleScript outerScript, IonUnaryArithIC* stub,
+                                    HandleValue val, MutableHandleValue res);
+};
+
 } // namespace jit
 } // namespace js
 
 #endif /* jit_IonIC_h */
diff --git a/js/src/jit/Lowering.cpp b/js/src/jit/Lowering.cpp
--- a/js/src/jit/Lowering.cpp
+++ b/js/src/jit/Lowering.cpp
@@ -2528,16 +2528,27 @@ LIRGenerator::visitBinarySharedStub(MBin
 
     LBinarySharedStub* lir = new(alloc()) LBinarySharedStub(useBoxFixedAtStart(lhs, R0),
                                                             useBoxFixedAtStart(rhs, R1));
     defineSharedStubReturn(lir, ins);
     assignSafepoint(lir, ins);
 }
 
 void
+LIRGenerator::visitUnaryCache(MUnaryCache* ins)
+{
+    MDefinition* input = ins->getOperand(0);
+    MOZ_ASSERT(ins->type() == MIRType::Value);
+
+    LUnaryCache* lir = new(alloc()) LUnaryCache(useBox(input));
+    defineBox(lir, ins);
+    assignSafepoint(lir, ins);
+}
+
+void
 LIRGenerator::visitNullarySharedStub(MNullarySharedStub* ins)
 {
     MOZ_ASSERT(ins->type() == MIRType::Value);
 
     LNullarySharedStub* lir = new(alloc()) LNullarySharedStub();
 
     defineSharedStubReturn(lir, ins);
     assignSafepoint(lir, ins);
diff --git a/js/src/jit/MIR.h b/js/src/jit/MIR.h
--- a/js/src/jit/MIR.h
+++ b/js/src/jit/MIR.h
@@ -8283,16 +8283,31 @@ class MBinarySharedStub
         setResultType(MIRType::Value);
     }
 
   public:
     INSTRUCTION_HEADER(BinarySharedStub)
     TRIVIAL_NEW_WRAPPERS
 };
 
+class MUnaryCache
+  : public MUnaryInstruction,
+    public BoxPolicy<0>::Data
+{
+    explicit MUnaryCache(MDefinition* input)
+      : MUnaryInstruction(classOpcode, input)
+    {
+        setResultType(MIRType::Value);
+    }
+
+  public:
+    INSTRUCTION_HEADER(UnaryCache)
+    TRIVIAL_NEW_WRAPPERS
+};
+
 class MNullarySharedStub
   : public MNullaryInstruction
 {
     explicit MNullarySharedStub()
       : MNullaryInstruction(classOpcode)
     {
         setResultType(MIRType::Value);
     }
diff --git a/js/src/jit/shared/LIR-shared.h b/js/src/jit/shared/LIR-shared.h
--- a/js/src/jit/shared/LIR-shared.h
+++ b/js/src/jit/shared/LIR-shared.h
@@ -5536,29 +5536,33 @@ class LBinarySharedStub : public LCallIn
     const MBinarySharedStub* mir() const {
         return mir_->toBinarySharedStub();
     }
 
     static const size_t LhsInput = 0;
     static const size_t RhsInput = BOX_PIECES;
 };
 
-class LUnarySharedStub : public LCallInstructionHelper<BOX_PIECES, BOX_PIECES, 0>
-{
-  public:
-    LIR_HEADER(UnarySharedStub)
-
-    explicit LUnarySharedStub(const LBoxAllocation& input)
-      : LCallInstructionHelper(classOpcode)
+class LUnaryCache : public LInstructionHelper<BOX_PIECES, BOX_PIECES, 0>
+{
+  public:
+    LIR_HEADER(UnaryCache)
+
+    explicit LUnaryCache(const LBoxAllocation& input)
+      : LInstructionHelper(classOpcode)
     {
         setBoxOperand(Input, input);
     }
 
-    const MUnarySharedStub* mir() const {
-        return mir_->toUnarySharedStub();
+    const MUnaryCache* mir() const {
+        return mir_->toUnaryCache();
+    }
+
+    const LAllocation* input() {
+        return getOperand(Input);
     }
 
     static const size_t Input = 0;
 };
 
 class LNullarySharedStub : public LCallInstructionHelper<BOX_PIECES, 0, 0>
 {
   public:
