# HG changeset patch
# User Brad Werth <bwerth@mozilla.com>
# Date 1662590617 0
# Node ID 087f6d6d5f511367db49351f945d5a4ca428fdba
# Parent  eb42af2839b3f0808008765acca9f32fdd91e684
Bug 1776653 Part 2: Prevent concurrent font creation between Font Loader and main thread on macOS. r=jrmuizel

This is a very broad lock on all the activity of LoadFontFamilyData. If we
wanted to narrow it for performance reasons, we may only need to protect
the calls to CTFontCreateWithFontDescriptor, but those happen within a
loop so the  overhead of holding and releasing the lock repeatedly could
also be a performance hit.

Differential Revision: https://phabricator.services.mozilla.com/D155107

diff --git a/gfx/thebes/gfxMacPlatformFontList.mm b/gfx/thebes/gfxMacPlatformFontList.mm
--- a/gfx/thebes/gfxMacPlatformFontList.mm
+++ b/gfx/thebes/gfxMacPlatformFontList.mm
@@ -1781,36 +1781,41 @@ gfxMacPlatformFontList::LookupSystemFont
     // convert size from css pixels to device pixels
     aFontStyle.size = [font pointSize] * aDevPixPerCSSPixel;
     aFontStyle.systemFont = true;
 }
 
 // used to load system-wide font info on off-main thread
 class MacFontInfo : public FontInfoData {
 public:
-    MacFontInfo(bool aLoadOtherNames,
-                bool aLoadFaceNames,
-                bool aLoadCmaps) :
-        FontInfoData(aLoadOtherNames, aLoadFaceNames, aLoadCmaps)
-    {}
+    MacFontInfo(bool aLoadOtherNames, bool aLoadFaceNames, bool aLoadCmaps, RecursiveMutex& aLock)
+        : FontInfoData(aLoadOtherNames, aLoadFaceNames, aLoadCmaps), mLock(aLock) {}
 
     virtual ~MacFontInfo() {}
 
     virtual void Load() {
         nsAutoreleasePool localPool;
         FontInfoData::Load();
     }
 
     // loads font data for all members of a given family
     virtual void LoadFontFamilyData(const nsAString& aFamilyName);
+
+    RecursiveMutex& mLock;
 };
 
 void
 MacFontInfo::LoadFontFamilyData(const nsAString& aFamilyName)
 {
+    // Prevent this from running concurrently with CGFont operations on the main thread,
+    // because the macOS font cache is fragile with concurrent access. This appears to be
+    // a vulnerability within CoreText in versions of macOS before macOS 13. In time, we
+    // can remove this lock.
+    RecursiveMutexAutoLock lock(mLock);
+
     // family name ==> CTFontDescriptor
     NSString *famName = GetNSStringForString(aFamilyName);
     CFStringRef family = CFStringRef(famName);
 
     CFMutableDictionaryRef attr =
         CFDictionaryCreateMutable(NULL, 0, &kCFTypeDictionaryKeyCallBacks,
                                   &kCFTypeDictionaryValueCallBacks);
     CFDictionaryAddValue(attr, kCTFontFamilyNameAttribute, family);
@@ -1911,18 +1916,17 @@ MacFontInfo::LoadFontFamilyData(const ns
 }
 
 already_AddRefed<FontInfoData>
 gfxMacPlatformFontList::CreateFontInfoData()
 {
     bool loadCmaps = !UsesSystemFallback() ||
         gfxPlatform::GetPlatform()->UseCmapsDuringSystemFallback();
 
-    RefPtr<MacFontInfo> fi =
-        new MacFontInfo(true, NeedFullnamePostscriptNames(), loadCmaps);
+    RefPtr<MacFontInfo> fi = new MacFontInfo(true, NeedFullnamePostscriptNames(), loadCmaps, mLock);
     return fi.forget();
 }
 
 gfxFontFamily*
 gfxMacPlatformFontList::CreateFontFamily(const nsAString& aName) const
 {
     return new gfxMacFontFamily(aName, 0.0);
 }
