# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1534846312 -7200
#      Tue Aug 21 12:11:52 2018 +0200
# Node ID 224b09c2e661110abc9d29c7cca417cb5de5d3b4
# Parent  5cb4cd7c449ecef9226496dc736d7c7325d37473
Bug 1466118 part 3 - Replace assertSameCompartmentDebugOnly with JSContext::debugOnlyCheck. r=luke

diff --git a/js/src/jit/CacheIR.cpp b/js/src/jit/CacheIR.cpp
--- a/js/src/jit/CacheIR.cpp
+++ b/js/src/jit/CacheIR.cpp
@@ -28,18 +28,19 @@ using mozilla::Maybe;
 
 const char* const js::jit::CacheKindNames[] = {
 #define DEFINE_KIND(kind) #kind,
     CACHE_IR_KINDS(DEFINE_KIND)
 #undef DEFINE_KIND
 };
 
 void
-CacheIRWriter::assertSameCompartment(JSObject* obj) {
-    assertSameCompartmentDebugOnly(cx_, obj);
+CacheIRWriter::assertSameCompartment(JSObject* obj)
+{
+    cx_->debugOnlyCheck(obj);
 }
 
 StubField
 CacheIRWriter::readStubFieldForIon(uint32_t offset, StubField::Type type) const
 {
     size_t index = 0;
     size_t currentOffset = 0;
 
@@ -5716,9 +5717,9 @@ NewObjectIRGenerator::tryAttachStub()
 
     writer.guardNoAllocationMetadataBuilder();
     writer.guardObjectGroupNotPretenured(templateObject_->group());
     writer.loadNewObjectFromTemplateResult(templateObject_);
     writer.returnFromIC();
 
     trackAttached("NewObjectWithTemplate");
     return true;
-}
\ No newline at end of file
+}
diff --git a/js/src/vm/Interpreter-inl.h b/js/src/vm/Interpreter-inl.h
--- a/js/src/vm/Interpreter-inl.h
+++ b/js/src/vm/Interpreter-inl.h
@@ -522,17 +522,17 @@ GetObjectElementOperation(JSContext* cx,
 
         RootedId id(cx);
         if (!ToPropertyKey(cx, key, &id))
             return false;
         if (!GetProperty(cx, obj, receiver, id, res))
             return false;
     } while (false);
 
-    assertSameCompartmentDebugOnly(cx, res);
+    cx->debugOnlyCheck(res);
     return true;
 }
 
 static MOZ_ALWAYS_INLINE bool
 GetPrimitiveElementOperation(JSContext* cx, JSOp op, JS::HandleValue receiver,
                              HandleValue key, MutableHandleValue res)
 {
     MOZ_ASSERT(op == JSOP_GETELEM || op == JSOP_CALLELEM);
@@ -569,17 +569,17 @@ GetPrimitiveElementOperation(JSContext* 
 
         RootedId id(cx);
         if (!ToPropertyKey(cx, key, &id))
             return false;
         if (!GetProperty(cx, boxed, receiver, id, res))
             return false;
     } while (false);
 
-    assertSameCompartmentDebugOnly(cx, res);
+    cx->debugOnlyCheck(res);
     return true;
 }
 
 static MOZ_ALWAYS_INLINE bool
 GetElemOptimizedArguments(JSContext* cx, AbstractFramePtr frame, MutableHandleValue lref,
                           HandleValue rref, MutableHandleValue res, bool* done)
 {
     MOZ_ASSERT(!*done);
diff --git a/js/src/vm/Interpreter.cpp b/js/src/vm/Interpreter.cpp
--- a/js/src/vm/Interpreter.cpp
+++ b/js/src/vm/Interpreter.cpp
@@ -1498,27 +1498,27 @@ HandleError(JSContext* cx, InterpreterRe
     // After this point, we will pop the frame regardless. Settle the frame on
     // the end of the script.
     regs.setToEndOfScript();
 
     return ok ? SuccessfulReturnContinuation : ErrorReturnContinuation;
 }
 
 #define REGS                     (activation.regs())
-#define PUSH_COPY(v)             do { *REGS.sp++ = (v); assertSameCompartmentDebugOnly(cx, REGS.sp[-1]); } while (0)
+#define PUSH_COPY(v)             do { *REGS.sp++ = (v); cx->debugOnlyCheck(REGS.sp[-1]); } while (0)
 #define PUSH_COPY_SKIP_CHECK(v)  *REGS.sp++ = (v)
 #define PUSH_NULL()              REGS.sp++->setNull()
 #define PUSH_UNDEFINED()         REGS.sp++->setUndefined()
 #define PUSH_BOOLEAN(b)          REGS.sp++->setBoolean(b)
 #define PUSH_DOUBLE(d)           REGS.sp++->setDouble(d)
 #define PUSH_INT32(i)            REGS.sp++->setInt32(i)
 #define PUSH_SYMBOL(s)           REGS.sp++->setSymbol(s)
-#define PUSH_STRING(s)           do { REGS.sp++->setString(s); assertSameCompartmentDebugOnly(cx, REGS.sp[-1]); } while (0)
-#define PUSH_OBJECT(obj)         do { REGS.sp++->setObject(obj); assertSameCompartmentDebugOnly(cx, REGS.sp[-1]); } while (0)
-#define PUSH_OBJECT_OR_NULL(obj) do { REGS.sp++->setObjectOrNull(obj); assertSameCompartmentDebugOnly(cx, REGS.sp[-1]); } while (0)
+#define PUSH_STRING(s)           do { REGS.sp++->setString(s); cx->debugOnlyCheck(REGS.sp[-1]); } while (0)
+#define PUSH_OBJECT(obj)         do { REGS.sp++->setObject(obj); cx->debugOnlyCheck(REGS.sp[-1]); } while (0)
+#define PUSH_OBJECT_OR_NULL(obj) do { REGS.sp++->setObjectOrNull(obj); cx->debugOnlyCheck(REGS.sp[-1]); } while (0)
 #define PUSH_MAGIC(magic)        REGS.sp++->setMagic(magic)
 #define POP_COPY_TO(v)           (v) = *--REGS.sp
 #define POP_RETURN_VALUE()       REGS.fp()->setReturnValue(*--REGS.sp)
 
 #define FETCH_OBJECT(cx, n, obj)                                              \
     JS_BEGIN_MACRO                                                            \
         HandleValue val = REGS.stackHandleAt(n);                              \
         obj = ToObjectFromStack((cx), (val));                                 \
@@ -2956,46 +2956,46 @@ CASE(JSOP_GETPROP)
 CASE(JSOP_LENGTH)
 CASE(JSOP_CALLPROP)
 {
     MutableHandleValue lval = REGS.stackHandleAt(-1);
     if (!GetPropertyOperation(cx, REGS.fp(), script, REGS.pc, lval, lval))
         goto error;
 
     TypeScript::Monitor(cx, script, REGS.pc, lval);
-    assertSameCompartmentDebugOnly(cx, lval);
+    cx->debugOnlyCheck(lval);
 }
 END_CASE(JSOP_GETPROP)
 
 CASE(JSOP_GETPROP_SUPER)
 {
     ReservedRooted<Value> receiver(&rootValue0, REGS.sp[-2]);
     ReservedRooted<JSObject*> obj(&rootObject1, &REGS.sp[-1].toObject());
     MutableHandleValue rref = REGS.stackHandleAt(-2);
 
     if (!GetProperty(cx, obj, receiver, script->getName(REGS.pc), rref))
         goto error;
 
     TypeScript::Monitor(cx, script, REGS.pc, rref);
-    assertSameCompartmentDebugOnly(cx, rref);
+    cx->debugOnlyCheck(rref);
 
     REGS.sp--;
 }
 END_CASE(JSOP_GETPROP_SUPER)
 
 CASE(JSOP_GETBOUNDNAME)
 {
     ReservedRooted<JSObject*> env(&rootObject0, &REGS.sp[-1].toObject());
     ReservedRooted<jsid> id(&rootId0, NameToId(script->getName(REGS.pc)));
     MutableHandleValue rval = REGS.stackHandleAt(-1);
     if (!GetNameBoundInEnvironment(cx, env, id, rval))
         goto error;
 
     TypeScript::Monitor(cx, script, REGS.pc, rval);
-    assertSameCompartmentDebugOnly(cx, rval);
+    cx->debugOnlyCheck(rval);
 }
 END_CASE(JSOP_GETBOUNDNAME)
 
 CASE(JSOP_SETINTRINSIC)
 {
     HandleValue value = REGS.stackHandleAt(-1);
 
     if (!SetIntrinsicOperation(cx, script, REGS.pc, value))
@@ -3701,17 +3701,17 @@ CASE(JSOP_GETLOCAL)
 
     /*
      * Skip the same-compartment assertion if the local will be immediately
      * popped. We do not guarantee sync for dead locals when coming in from the
      * method JIT, and a GETLOCAL followed by POP is not considered to be
      * a use of the variable.
      */
     if (REGS.pc[JSOP_GETLOCAL_LENGTH] != JSOP_POP)
-        assertSameCompartmentDebugOnly(cx, REGS.sp[-1]);
+        cx->debugOnlyCheck(REGS.sp[-1]);
 }
 END_CASE(JSOP_GETLOCAL)
 
 CASE(JSOP_SETLOCAL)
 {
     uint32_t i = GET_LOCALNO(REGS.pc);
 
     MOZ_ASSERT(!IsUninitializedLexical(REGS.fp()->unaliasedLocal(i)));
diff --git a/js/src/vm/JSContext-inl.h b/js/src/vm/JSContext-inl.h
--- a/js/src/vm/JSContext-inl.h
+++ b/js/src/vm/JSContext-inl.h
@@ -202,26 +202,26 @@ assertSameCompartment(JSContext* cx, con
 } // namespace js
 
 template <class... Args> inline void
 JSContext::releaseCheck(const Args&... args)
 {
     assertSameCompartmentImpl(this, 0, args...);
 }
 
-namespace js {
-
-template <class... Args> inline void
-assertSameCompartmentDebugOnly(JSContext* cx, const Args&... args)
+template <class... Args> MOZ_ALWAYS_INLINE void
+JSContext::debugOnlyCheck(const Args&... args)
 {
 #if defined(DEBUG) && defined(JS_CRASH_DIAGNOSTICS)
-    assertSameCompartmentImpl(cx, 1, args...);
+    assertSameCompartmentImpl(this, 0, args...);
 #endif
 }
 
+namespace js {
+
 STATIC_PRECONDITION_ASSUME(ubound(args.argv_) >= argc)
 MOZ_ALWAYS_INLINE bool
 CallNativeImpl(JSContext* cx, NativeImpl impl, const CallArgs& args)
 {
 #ifdef DEBUG
     bool alreadyThrowing = cx->isExceptionPending();
 #endif
     assertSameCompartment(cx, args);
diff --git a/js/src/vm/JSContext.h b/js/src/vm/JSContext.h
--- a/js/src/vm/JSContext.h
+++ b/js/src/vm/JSContext.h
@@ -951,16 +951,17 @@ struct JSContext : public JS::RootingCon
 
     JSObject* getIncumbentGlobal(JSContext* cx);
     bool enqueuePromiseJob(JSContext* cx, js::HandleFunction job, js::HandleObject promise,
                            js::HandleObject incumbentGlobal);
     void addUnhandledRejectedPromise(JSContext* cx, js::HandleObject promise);
     void removeUnhandledRejectedPromise(JSContext* cx, js::HandleObject promise);
 
     template <class... Args> inline void releaseCheck(const Args&... args);
+    template <class... Args> MOZ_ALWAYS_INLINE void debugOnlyCheck(const Args&... args);
 }; /* struct JSContext */
 
 inline JS::Result<>
 JSContext::boolToResult(bool ok)
 {
     if (MOZ_LIKELY(ok)) {
         MOZ_ASSERT(!isExceptionPending());
         MOZ_ASSERT(!isPropagatingForcedReturn());
