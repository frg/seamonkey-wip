# HG changeset patch
# User Xidorn Quan <me@upsuper.org>
# Date 1520317241 -39600
# Node ID 89eb7b95d909b82010c4c1c9b62b93766c7b9e17
# Parent  114844360add33d053b51b1bc78608ff440928a9
Bug 1443397 - Modernize several rect and region related functions in Windows widget to use typed types. r=jimm

Mostly just convert nsInt{Rect,Region} to LayoutDeviceInt{Rect,Region}.

One exception is to change the parameter of nsWindow::OnResize from
nsIntRect to LayoutDeviceIntSize, because it really only needs that.

MozReview-Commit-ID: 963Mzd5Wed6

diff --git a/widget/windows/WinUtils.cpp b/widget/windows/WinUtils.cpp
--- a/widget/windows/WinUtils.cpp
+++ b/widget/windows/WinUtils.cpp
@@ -1712,22 +1712,22 @@ WinUtils::GetShellItemPath(IShellItem* a
   if (FAILED(aItem->GetDisplayName(SIGDN_FILESYSPATH, &str)))
     return false;
   aResultString.Assign(str);
   CoTaskMemFree(str);
   return !aResultString.IsEmpty();
 }
 
 /* static */
-nsIntRegion
+LayoutDeviceIntRegion
 WinUtils::ConvertHRGNToRegion(HRGN aRgn)
 {
   NS_ASSERTION(aRgn, "Don't pass NULL region here");
 
-  nsIntRegion rgn;
+  LayoutDeviceIntRegion rgn;
 
   DWORD size = ::GetRegionData(aRgn, 0, nullptr);
   AutoTArray<uint8_t,100> buffer;
   buffer.SetLength(size);
 
   RGNDATA* data = reinterpret_cast<RGNDATA*>(buffer.Elements());
   if (!::GetRegionData(aRgn, size, data))
     return rgn;
@@ -1741,22 +1741,22 @@ WinUtils::ConvertHRGNToRegion(HRGN aRgn)
   for (uint32_t i = 0; i < data->rdh.nCount; ++i) {
     RECT* r = rects + i;
     rgn.Or(rgn, ToIntRect(*r));
   }
 
   return rgn;
 }
 
-nsIntRect
+LayoutDeviceIntRect
 WinUtils::ToIntRect(const RECT& aRect)
 {
-  return nsIntRect(aRect.left, aRect.top,
-                   aRect.right - aRect.left,
-                   aRect.bottom - aRect.top);
+  return LayoutDeviceIntRect(aRect.left, aRect.top,
+                             aRect.right - aRect.left,
+                             aRect.bottom - aRect.top);
 }
 
 /* static */
 bool WinUtils::IsIMEEnabled(const InputContext& aInputContext) {
   return IsIMEEnabled(aInputContext.mIMEState.mEnabled);
 }
 
 /* static */
diff --git a/widget/windows/WinUtils.h b/widget/windows/WinUtils.h
--- a/widget/windows/WinUtils.h
+++ b/widget/windows/WinUtils.h
@@ -415,30 +415,30 @@ public:
    * aItem  the shell item containing the path.
    * aResultString  the resulting string path.
    * returns  true if a path was retreived.
    */
   static bool GetShellItemPath(IShellItem* aItem,
                                nsString& aResultString);
 
   /**
-   * ConvertHRGNToRegion converts a Windows HRGN to an nsIntRegion.
+   * ConvertHRGNToRegion converts a Windows HRGN to an LayoutDeviceIntRegion.
    *
    * aRgn the HRGN to convert.
-   * returns the nsIntRegion.
+   * returns the LayoutDeviceIntRegion.
    */
-  static nsIntRegion ConvertHRGNToRegion(HRGN aRgn);
+  static LayoutDeviceIntRegion ConvertHRGNToRegion(HRGN aRgn);
 
   /**
-   * ToIntRect converts a Windows RECT to a nsIntRect.
+   * ToIntRect converts a Windows RECT to a LayoutDeviceIntRect.
    *
    * aRect the RECT to convert.
-   * returns the nsIntRect.
+   * returns the LayoutDeviceIntRect.
    */
-  static nsIntRect ToIntRect(const RECT& aRect);
+  static LayoutDeviceIntRect ToIntRect(const RECT& aRect);
 
   /**
    * Helper used in invalidating flash plugin windows owned
    * by low rights flash containers.
    */
   static void InvalidatePluginAsWorkaround(nsIWidget* aWidget,
                                            const LayoutDeviceIntRect& aRect);
 
diff --git a/widget/windows/nsWindow.cpp b/widget/windows/nsWindow.cpp
--- a/widget/windows/nsWindow.cpp
+++ b/widget/windows/nsWindow.cpp
@@ -2449,18 +2449,17 @@ nsWindow::ResetLayout()
 
   // If hidden, just send the frame changed event for now.
   if (!mIsVisible)
     return;
 
   // Send a gecko size event to trigger reflow.
   RECT clientRc = {0};
   GetClientRect(mWnd, &clientRc);
-  nsIntRect evRect(WinUtils::ToIntRect(clientRc));
-  OnResize(evRect);
+  OnResize(WinUtils::ToIntRect(clientRc).Size());
 
   // Invalidate and update
   Invalidate();
 }
 
 // Internally track the caption status via a window property. Required
 // due to our internal handling of WM_NCACTIVATE when custom client
 // margins are set.
@@ -6588,17 +6587,16 @@ void nsWindow::OnWindowPosChanged(WINDOW
   if (!(wp->flags & SWP_NOSIZE)) {
     RECT r;
     int32_t newWidth, newHeight;
 
     ::GetWindowRect(mWnd, &r);
 
     newWidth  = r.right - r.left;
     newHeight = r.bottom - r.top;
-    nsIntRect rect(wp->x, wp->y, newWidth, newHeight);
 
     if (newWidth > mLastSize.width)
     {
       RECT drect;
 
       // getting wider
       drect.left   = wp->x + mLastSize.width;
       drect.top    = wp->y;
@@ -6644,22 +6642,22 @@ void nsWindow::OnWindowPosChanged(WINDOW
     if (mSizeMode == nsSizeMode_Maximized) {
       if (UpdateNonClientMargins(nsSizeMode_Maximized, true)) {
         // gecko resize event already sent by UpdateNonClientMargins.
         return;
       }
     }
 
     // Recalculate the width and height based on the client area for gecko events.
+    LayoutDeviceIntSize clientSize(newWidth, newHeight);
     if (::GetClientRect(mWnd, &r)) {
-      rect.SizeTo(r.right - r.left, r.bottom - r.top);
-    }
-    
+      clientSize = WinUtils::ToIntRect(r).Size();
+    }
     // Send a gecko resize event
-    OnResize(rect);
+    OnResize(clientSize);
   }
 }
 
 void nsWindow::OnWindowPosChanging(LPWINDOWPOS& info)
 {
   // Update non-client margins if the frame size is changing, and let the
   // browser know we are changing size modes, so alternative css can kick in.
   // If we're going into fullscreen mode, ignore this, since it'll reset
@@ -7106,24 +7104,29 @@ void nsWindow::OnDestroy()
   // Finalize panning feedback to possibly restore window displacement
   mGesture.PanFeedbackFinalize(mWnd, true);
 
   // Clear the main HWND.
   mWnd = nullptr;
 }
 
 // Send a resize message to the listener
-bool nsWindow::OnResize(nsIntRect &aWindowRect)
-{
-  bool result = mWidgetListener ?
-    mWidgetListener->WindowResized(this, aWindowRect.Width(), aWindowRect.Height()) : false;
+bool
+nsWindow::OnResize(const LayoutDeviceIntSize& aSize)
+{
+  bool result = false;
+  if (mWidgetListener) {
+    result = mWidgetListener->
+      WindowResized(this, aSize.width, aSize.height);
+  }
 
   // If there is an attached view, inform it as well as the normal widget listener.
   if (mAttachedWidgetListener) {
-    return mAttachedWidgetListener->WindowResized(this, aWindowRect.Width(), aWindowRect.Height());
+    return mAttachedWidgetListener->
+      WindowResized(this, aSize.width, aSize.height);
   }
 
   return result;
 }
 
 bool nsWindow::OnHotKey(WPARAM wParam, LPARAM lParam)
 {
   return true;
diff --git a/widget/windows/nsWindow.h b/widget/windows/nsWindow.h
--- a/widget/windows/nsWindow.h
+++ b/widget/windows/nsWindow.h
@@ -412,17 +412,17 @@ protected:
   static void             UpdateFirstEventTime(DWORD aEventTime);
   void                    FinishLiveResizing(ResizeState aNewState);
   nsIntPoint              GetTouchCoordinates(WPARAM wParam, LPARAM lParam);
 
   /**
    * Event handlers
    */
   virtual void            OnDestroy() override;
-  virtual bool            OnResize(nsIntRect &aWindowRect);
+  bool                    OnResize(const LayoutDeviceIntSize& aSize);
   bool                    OnGesture(WPARAM wParam, LPARAM lParam);
   bool                    OnTouch(WPARAM wParam, LPARAM lParam);
   bool                    OnHotKey(WPARAM wParam, LPARAM lParam);
   bool                    OnPaint(HDC aDC, uint32_t aNestingLevel);
   void                    OnWindowPosChanged(WINDOWPOS* wp);
   void                    OnWindowPosChanging(LPWINDOWPOS& info);
   void                    OnSysColorChanged();
   void                    OnDPIChanged(int32_t x, int32_t y,
@@ -480,17 +480,17 @@ protected:
 
   /**
    * Misc.
    */
   void                    StopFlashing();
   static bool             IsTopLevelMouseExit(HWND aWnd);
   virtual nsresult        SetWindowClipRegion(const nsTArray<LayoutDeviceIntRect>& aRects,
                                               bool aIntersectWithExisting) override;
-  nsIntRegion             GetRegionToPaint(bool aForceFullRepaint,
+  LayoutDeviceIntRegion   GetRegionToPaint(bool aForceFullRepaint,
                                            PAINTSTRUCT ps, HDC aDC);
   void                    ClearCachedResources();
   nsIWidgetListener*      GetPaintListener();
 
   virtual void AddWindowOverlayWebRenderCommands(mozilla::layers::WebRenderBridgeChild* aWrBridge,
                                                  mozilla::wr::DisplayListBuilder& aBuilder,
                                                  mozilla::wr::IpcResourceUpdateQueue& aResourceUpdates) override;
 
diff --git a/widget/windows/nsWindowGfx.cpp b/widget/windows/nsWindowGfx.cpp
--- a/widget/windows/nsWindowGfx.cpp
+++ b/widget/windows/nsWindowGfx.cpp
@@ -93,38 +93,38 @@ static IconMetrics sIconMetrics[] = {
  ** BLOCK: nsWindow impl.
  **
  ** Paint related nsWindow methods.
  **
  **************************************************************
  **************************************************************/
 
 // GetRegionToPaint returns the invalidated region that needs to be painted
-nsIntRegion nsWindow::GetRegionToPaint(bool aForceFullRepaint,
-                                       PAINTSTRUCT ps, HDC aDC)
+LayoutDeviceIntRegion
+nsWindow::GetRegionToPaint(bool aForceFullRepaint, PAINTSTRUCT ps, HDC aDC)
 {
   if (aForceFullRepaint) {
     RECT paintRect;
     ::GetClientRect(mWnd, &paintRect);
-    return nsIntRegion(WinUtils::ToIntRect(paintRect));
+    return LayoutDeviceIntRegion(WinUtils::ToIntRect(paintRect));
   }
 
   HRGN paintRgn = ::CreateRectRgn(0, 0, 0, 0);
   if (paintRgn != nullptr) {
     int result = GetRandomRgn(aDC, paintRgn, SYSRGN);
     if (result == 1) {
       POINT pt = {0,0};
       ::MapWindowPoints(nullptr, mWnd, &pt, 1);
       ::OffsetRgn(paintRgn, pt.x, pt.y);
     }
-    nsIntRegion rgn(WinUtils::ConvertHRGNToRegion(paintRgn));
+    LayoutDeviceIntRegion rgn(WinUtils::ConvertHRGNToRegion(paintRgn));
     ::DeleteObject(paintRgn);
     return rgn;
   }
-  return nsIntRegion(WinUtils::ToIntRect(ps.rcPaint));
+  return LayoutDeviceIntRegion(WinUtils::ToIntRect(ps.rcPaint));
 }
 
 #define WORDSSIZE(x) ((x).width * (x).height)
 static bool
 EnsureSharedSurfaceSize(IntSize size)
 {
   IntSize screenSize;
   screenSize.height = GetSystemMetrics(SM_CYSCREEN);
@@ -216,23 +216,23 @@ bool nsWindow::OnPaint(HDC aDC, uint32_t
   HDC hDC = aDC ? aDC : (::BeginPaint(mWnd, &ps));
   mPaintDC = hDC;
 
 #ifdef MOZ_XUL
   bool forceRepaint = aDC || (eTransparencyTransparent == mTransparencyMode);
 #else
   bool forceRepaint = nullptr != aDC;
 #endif
-  nsIntRegion region = GetRegionToPaint(forceRepaint, ps, hDC);
+  LayoutDeviceIntRegion region = GetRegionToPaint(forceRepaint, ps, hDC);
 
   if (GetLayerManager()->AsKnowsCompositor()) {
     // We need to paint to the screen even if nothing changed, since if we
     // don't have a compositing window manager, our pixels could be stale.
     GetLayerManager()->SetNeedsComposite(true);
-    GetLayerManager()->SendInvalidRegion(region);
+    GetLayerManager()->SendInvalidRegion(region.ToUnknownRegion());
   }
 
   RefPtr<nsWindow> strongThis(this);
 
   nsIWidgetListener* listener = GetPaintListener();
   if (listener) {
     listener->WillPaintWindow(this);
   }
@@ -251,17 +251,17 @@ bool nsWindow::OnPaint(HDC aDC, uint32_t
   if (!region.IsEmpty() && listener)
   {
     // Should probably pass in a real region here, using GetRandomRgn
     // http://msdn.microsoft.com/library/default.asp?url=/library/en-us/gdi/clipping_4q0e.asp
 
 #ifdef WIDGET_DEBUG_OUTPUT
     debug_DumpPaintEvent(stdout,
                          this,
-                         region,
+                         region.ToUnknownRegion(),
                          "noname",
                          (int32_t) mWnd);
 #endif // WIDGET_DEBUG_OUTPUT
 
     switch (GetLayerManager()->GetBackendType()) {
       case LayersBackend::LAYERS_BASIC:
         {
           RefPtr<gfxASurface> targetSurface;
@@ -322,35 +322,33 @@ bool nsWindow::OnPaint(HDC aDC, uint32_t
 #endif
 
           RefPtr<gfxContext> thebesContext = gfxContext::CreateOrNull(dt);
           MOZ_ASSERT(thebesContext); // already checked draw target above
 
           {
             AutoLayerManagerSetup
               setupLayerManager(this, thebesContext, doubleBuffering);
-            result = listener->PaintWindow(
-              this, LayoutDeviceIntRegion::FromUnknownRegion(region));
+            result = listener->PaintWindow(this, region);
           }
 
 #ifdef MOZ_XUL
           if (eTransparencyTransparent == mTransparencyMode) {
             // Data from offscreen drawing surface was copied to memory bitmap of transparent
             // bitmap. Now it can be read from memory bitmap to apply alpha channel and after
             // that displayed on the screen.
             mBasicLayersSurface->RedrawTransparentWindow();
           }
 #endif
         }
         break;
       case LayersBackend::LAYERS_CLIENT:
       case LayersBackend::LAYERS_WR:
         {
-          result = listener->PaintWindow(
-            this, LayoutDeviceIntRegion::FromUnknownRegion(region));
+          result = listener->PaintWindow(this, region);
           if (!gfxEnv::DisableForcePresent() && gfxWindowsPlatform::GetPlatform()->DwmCompositionEnabled()) {
             nsCOMPtr<nsIRunnable> event =
               NewRunnableMethod("nsWindow::ForcePresent", this, &nsWindow::ForcePresent);
             NS_DispatchToMainThread(event);
           }
         }
         break;
       default:
