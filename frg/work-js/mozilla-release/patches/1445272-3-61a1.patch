# HG changeset patch
# User Benjamin Bouvier <benj@benj.me>
# Date 1521639968 -3600
#      Wed Mar 21 14:46:08 2018 +0100
# Node ID f8b79c586af6e9ee3cdd5b56fd8f84f8ce2d9900
# Parent  40baddc5f0b6206f9a90bd694c596b0d8f8a9837
Bug 1445272: Add Anyref to the wasm type system; r=luke

diff --git a/js/src/jit/IonTypes.h b/js/src/jit/IonTypes.h
--- a/js/src/jit/IonTypes.h
+++ b/js/src/jit/IonTypes.h
@@ -563,16 +563,18 @@ MIRTypeToSize(MIRType type)
       case MIRType::Int32:
         return 4;
       case MIRType::Int64:
         return 8;
       case MIRType::Float32:
         return 4;
       case MIRType::Double:
         return 8;
+      case MIRType::Pointer:
+        return sizeof(uintptr_t);
       default:
         MOZ_CRASH("MIRTypeToSize - unhandled case");
     }
 }
 
 static inline const char*
 StringFromMIRType(MIRType type)
 {
diff --git a/js/src/wasm/AsmJS.cpp b/js/src/wasm/AsmJS.cpp
--- a/js/src/wasm/AsmJS.cpp
+++ b/js/src/wasm/AsmJS.cpp
@@ -7719,16 +7719,19 @@ ValidateGlobalVariable(JSContext* cx, co
           case ValType::B32x4: {
             SimdConstant simdConstant;
             if (!ToSimdConstant<Bool32x4>(cx, v, &simdConstant))
                 return false;
             // Bool32x4 uses the same data layout as Int32x4.
             *val = Val(simdConstant.asInt32x4());
             return true;
           }
+          case ValType::AnyRef: {
+            MOZ_CRASH("not available in asm.js");
+          }
         }
       }
     }
 
     MOZ_CRASH("unreachable");
 }
 
 static bool
diff --git a/js/src/wasm/WasmBaselineCompile.cpp b/js/src/wasm/WasmBaselineCompile.cpp
--- a/js/src/wasm/WasmBaselineCompile.cpp
+++ b/js/src/wasm/WasmBaselineCompile.cpp
@@ -9804,17 +9804,17 @@ js::wasm::BaselineCompileFunctions(const
     for (const FuncCompileInput& func : inputs) {
         Decoder d(func.begin, func.end, func.lineOrBytecode, error);
 
         // Build the local types vector.
 
         ValTypeVector locals;
         if (!locals.appendAll(env.funcSigs[func.index]->args()))
             return false;
-        if (!DecodeLocalEntries(d, env.kind, &locals))
+        if (!DecodeLocalEntries(d, env.kind, env.gcTypesEnabled, &locals))
             return false;
 
         // One-pass baseline compilation.
 
         BaseCompiler f(env, func, locals, d, &alloc, &masm);
         if (!f.init())
             return false;
         if (!f.emitFunction())
diff --git a/js/src/wasm/WasmBinaryConstants.h b/js/src/wasm/WasmBinaryConstants.h
--- a/js/src/wasm/WasmBinaryConstants.h
+++ b/js/src/wasm/WasmBinaryConstants.h
@@ -57,32 +57,37 @@ enum class TypeCode
     F32x4                                = 0x78,
     B8x16                                = 0x77,
     B16x8                                = 0x76,
     B32x4                                = 0x75,
 
     // A function pointer with any signature
     AnyFunc                              = 0x70,  // SLEB128(-0x10)
 
+    // A reference to any type.
+    AnyRef                               = 0x6f,
+
     // Type constructor for function types
     Func                                 = 0x60,  // SLEB128(-0x20)
 
     // Special code representing the block signature ()->()
     BlockVoid                            = 0x40,  // SLEB128(-0x40)
 
     Limit                                = 0x80
 };
 
 enum class ValType
 {
     I32                                  = uint8_t(TypeCode::I32),
     I64                                  = uint8_t(TypeCode::I64),
     F32                                  = uint8_t(TypeCode::F32),
     F64                                  = uint8_t(TypeCode::F64),
 
+    AnyRef                               = uint8_t(TypeCode::AnyRef),
+
     // ------------------------------------------------------------------------
     // The rest of these types are currently only emitted internally when
     // compiling asm.js and are rejected by wasm validation.
 
     I8x16                                = uint8_t(TypeCode::I8x16),
     I16x8                                = uint8_t(TypeCode::I16x8),
     I32x4                                = uint8_t(TypeCode::I32x4),
     F32x4                                = uint8_t(TypeCode::F32x4),
@@ -318,16 +323,20 @@ enum class Op
     // Sign extension
     I32Extend8S                          = 0xc0,
     I32Extend16S                         = 0xc1,
     I64Extend8S                          = 0xc2,
     I64Extend16S                         = 0xc3,
     I64Extend32S                         = 0xc4,
 #endif
 
+    // GC ops
+    RefNull                              = 0xd0,
+    RefIsNull                            = 0xd1,
+
     FirstPrefix                          = 0xfc,
     NumericPrefix                        = 0xfc,
     ThreadPrefix                         = 0xfe,
     MozPrefix                            = 0xff,
 
     Limit                                = 0x100
 };
 
diff --git a/js/src/wasm/WasmBinaryIterator.cpp b/js/src/wasm/WasmBinaryIterator.cpp
--- a/js/src/wasm/WasmBinaryIterator.cpp
+++ b/js/src/wasm/WasmBinaryIterator.cpp
@@ -172,16 +172,17 @@ wasm::Classify(OpBytes op)
       case Op::F32ConvertUI64:
       case Op::F32DemoteF64:
       case Op::F64ConvertSI32:
       case Op::F64ConvertUI32:
       case Op::F64ConvertSI64:
       case Op::F64ConvertUI64:
       case Op::F64ReinterpretI64:
       case Op::F64PromoteF32:
+      case Op::RefIsNull:
 #ifdef ENABLE_WASM_SIGNEXTEND_OPS
       case Op::I32Extend8S:
       case Op::I32Extend16S:
       case Op::I64Extend8S:
       case Op::I64Extend16S:
       case Op::I64Extend32S:
 #endif
         return OpKind::Conversion;
@@ -235,16 +236,18 @@ wasm::Classify(OpBytes op)
       case Op::Else:
         return OpKind::Else;
       case Op::End:
         return OpKind::End;
       case Op::CurrentMemory:
         return OpKind::CurrentMemory;
       case Op::GrowMemory:
         return OpKind::GrowMemory;
+      case Op::RefNull:
+        return OpKind::RefNull;
       case Op::NumericPrefix: {
 #ifdef ENABLE_WASM_SATURATING_TRUNC_OPS
           switch (NumericOp(op.b1)) {
             case NumericOp::I32TruncSSatF32:
             case NumericOp::I32TruncUSatF32:
             case NumericOp::I32TruncSSatF64:
             case NumericOp::I32TruncUSatF64:
             case NumericOp::I64TruncSSatF32:
diff --git a/js/src/wasm/WasmBinaryIterator.h b/js/src/wasm/WasmBinaryIterator.h
--- a/js/src/wasm/WasmBinaryIterator.h
+++ b/js/src/wasm/WasmBinaryIterator.h
@@ -36,63 +36,123 @@ enum class LabelKind : uint8_t
     Then,
     Else
 };
 
 // The type of values on the operand stack during validation. The Any type
 // represents the type of a value produced by an unconditional branch.
 enum class StackType
 {
-    I32   = uint8_t(ValType::I32),
-    I64   = uint8_t(ValType::I64),
-    F32   = uint8_t(ValType::F32),
-    F64   = uint8_t(ValType::F64),
+    I32    = uint8_t(ValType::I32),
+    I64    = uint8_t(ValType::I64),
+    F32    = uint8_t(ValType::F32),
+    F64    = uint8_t(ValType::F64),
 
-    I8x16 = uint8_t(ValType::I8x16),
-    I16x8 = uint8_t(ValType::I16x8),
-    I32x4 = uint8_t(ValType::I32x4),
-    F32x4 = uint8_t(ValType::F32x4),
-    B8x16 = uint8_t(ValType::B8x16),
-    B16x8 = uint8_t(ValType::B16x8),
-    B32x4 = uint8_t(ValType::B32x4),
+    I8x16  = uint8_t(ValType::I8x16),
+    I16x8  = uint8_t(ValType::I16x8),
+    I32x4  = uint8_t(ValType::I32x4),
+    F32x4  = uint8_t(ValType::F32x4),
+    B8x16  = uint8_t(ValType::B8x16),
+    B16x8  = uint8_t(ValType::B16x8),
+    B32x4  = uint8_t(ValType::B32x4),
 
-    Any   = uint8_t(TypeCode::Limit)
+    AnyRef = uint8_t(ValType::AnyRef),
+
+    Any    = uint8_t(TypeCode::Limit),
 };
 
 static inline StackType
 ToStackType(ValType type)
 {
     return StackType(type);
 }
 
 static inline ValType
 NonAnyToValType(StackType type)
 {
     MOZ_ASSERT(type != StackType::Any);
     return ValType(type);
 }
 
 static inline bool
-Unify(StackType one, StackType two, StackType* result)
+IsRefType(StackType st)
+{
+    return IsRefType(NonAnyToValType(st));
+}
+
+static inline bool
+IsSubtypeOf(StackType one, StackType two)
+{
+    MOZ_ASSERT(IsRefType(one));
+    MOZ_ASSERT(IsRefType(two));
+    return one == two || two == StackType::AnyRef;
+}
+
+static inline bool
+Unify(HasGcTypes gcTypesEnabled, StackType observed, StackType expected, StackType* result)
+{
+    if (MOZ_LIKELY(observed == expected)) {
+        *result = observed;
+        return true;
+    }
+
+    if (observed == StackType::Any) {
+        *result = expected;
+        return true;
+    }
+
+    if (expected == StackType::Any) {
+        *result = observed;
+        return true;
+    }
+
+    if (gcTypesEnabled == HasGcTypes::True && IsRefType(observed) && IsRefType(expected) &&
+        IsSubtypeOf(observed, expected))
+    {
+        *result = expected;
+        return true;
+    }
+
+    return false;
+}
+
+static inline bool
+Join(HasGcTypes gcTypesEnabled, StackType one, StackType two, StackType* result)
 {
     if (MOZ_LIKELY(one == two)) {
         *result = one;
         return true;
     }
 
     if (one == StackType::Any) {
         *result = two;
         return true;
     }
 
     if (two == StackType::Any) {
         *result = one;
         return true;
     }
 
+    if (gcTypesEnabled == HasGcTypes::True && IsRefType(one) && IsRefType(two)) {
+        if (IsSubtypeOf(two, one)) {
+            *result = one;
+            return true;
+        }
+
+        if (IsSubtypeOf(one, two)) {
+            *result = two;
+            return true;
+        }
+
+        // No subtyping relations between the two types.
+        *result = StackType::AnyRef;
+        return true;
+    }
+
     return false;
 }
 
 #ifdef DEBUG
 // Families of opcodes that share a signature and validation logic.
 enum class OpKind {
     Block,
     Loop,
@@ -108,17 +168,16 @@ enum class OpKind {
     F32x4,
     B8x16,
     B16x8,
     B32x4,
     Br,
     BrIf,
     BrTable,
     Nop,
-    Nullary,
     Unary,
     Binary,
     Comparison,
     Conversion,
     Load,
     Store,
     TeeStore,
     CurrentMemory,
@@ -154,16 +213,17 @@ enum class OpKind {
     Swizzle,
     Shuffle,
     Splat,
     SimdSelect,
     SimdCtor,
     SimdBooleanReduction,
     SimdShiftByScalar,
     SimdComparison,
+    RefNull,
 };
 
 // Return the OpKind for a given Op. This is used for sanity-checking that
 // API users use the correct read function for a given Op.
 OpKind
 Classify(OpBytes op);
 #endif
 
@@ -529,16 +589,17 @@ class MOZ_STACK_CLASS OpIter : private P
     MOZ_MUST_USE bool readF64Const(double* f64);
     MOZ_MUST_USE bool readI8x16Const(I8x16* i8x16);
     MOZ_MUST_USE bool readI16x8Const(I16x8* i16x8);
     MOZ_MUST_USE bool readI32x4Const(I32x4* i32x4);
     MOZ_MUST_USE bool readF32x4Const(F32x4* f32x4);
     MOZ_MUST_USE bool readB8x16Const(I8x16* i8x16);
     MOZ_MUST_USE bool readB16x8Const(I16x8* i16x8);
     MOZ_MUST_USE bool readB32x4Const(I32x4* i32x4);
+    MOZ_MUST_USE bool readRefNull();
     MOZ_MUST_USE bool readCall(uint32_t* calleeIndex, ValueVector* argValues);
     MOZ_MUST_USE bool readCallIndirect(uint32_t* sigIndex, Value* callee, ValueVector* argValues);
     MOZ_MUST_USE bool readOldCallDirect(uint32_t numFuncImports, uint32_t* funcIndex,
                                         ValueVector* argValues);
     MOZ_MUST_USE bool readOldCallIndirect(uint32_t* sigIndex, Value* callee, ValueVector* argValues);
     MOZ_MUST_USE bool readWake(LinearMemoryAddress<Value>* addr, Value* count);
     MOZ_MUST_USE bool readWait(LinearMemoryAddress<Value>* addr,
                                ValType resultType,
@@ -726,17 +787,17 @@ OpIter<Policy>::popWithType(StackType ex
         if (valueStack_.empty())
             return fail("popping value from empty stack");
         return fail("popping value from outside block");
     }
 
     TypeAndValue<Value> tv = valueStack_.popCopy();
 
     StackType _;
-    if (MOZ_UNLIKELY(!Unify(tv.type(), expectedType, &_)))
+    if (MOZ_UNLIKELY(!Unify(env_.gcTypesEnabled, tv.type(), expectedType, &_)))
         return typeMismatch(tv.type(), expectedType);
 
     *value = tv.value();
     return true;
 }
 
 // This function pops as many types from the stack as determined by the given
 // signature. Currently, all signatures are limited to 0 or 1 types, with
@@ -778,18 +839,21 @@ OpIter<Policy>::topWithType(ValType expe
 
         if (valueStack_.empty())
             return fail("reading value from empty stack");
         return fail("reading value from outside block");
     }
 
     TypeAndValue<Value>& tv = valueStack_.back();
 
-    if (MOZ_UNLIKELY(!Unify(tv.type(), ToStackType(expectedType), &tv.typeRef())))
+    if (MOZ_UNLIKELY(!Unify(env_.gcTypesEnabled, tv.type(), ToStackType(expectedType),
+                            &tv.typeRef())))
+    {
         return typeMismatch(tv.type(), ToStackType(expectedType));
+    }
 
     *value = tv.value();
     return true;
 }
 
 template <typename Policy>
 inline bool
 OpIter<Policy>::topWithType(ExprType expectedType, Value* value)
@@ -841,33 +905,41 @@ OpIter<Policy>::getControl(uint32_t rela
 template <typename Policy>
 inline bool
 OpIter<Policy>::readBlockType(ExprType* type)
 {
     uint8_t unchecked;
     if (!d_.readBlockType(&unchecked))
         return fail("unable to read block signature");
 
+    bool known = false;
     switch (unchecked) {
       case uint8_t(ExprType::Void):
       case uint8_t(ExprType::I32):
       case uint8_t(ExprType::I64):
       case uint8_t(ExprType::F32):
       case uint8_t(ExprType::F64):
       case uint8_t(ExprType::I8x16):
       case uint8_t(ExprType::I16x8):
       case uint8_t(ExprType::I32x4):
       case uint8_t(ExprType::F32x4):
       case uint8_t(ExprType::B8x16):
       case uint8_t(ExprType::B16x8):
       case uint8_t(ExprType::B32x4):
+        known = true;
         break;
-      default:
+      case uint8_t(ExprType::AnyRef):
+        known = env_.gcTypesEnabled == HasGcTypes::True;
+        break;
+      case uint8_t(ExprType::Limit):
+        break;
+    }
+
+    if (!known)
         return fail("invalid inline block type");
-    }
 
     *type = ExprType(unchecked);
     return true;
 }
 
 template <typename Policy>
 inline bool
 OpIter<Policy>::readOp(OpBytes* op)
@@ -1377,17 +1449,17 @@ OpIter<Policy>::readSelect(StackType* ty
     StackType falseType;
     if (!popAnyType(&falseType, falseValue))
         return false;
 
     StackType trueType;
     if (!popAnyType(&trueType, trueValue))
         return false;
 
-    if (!Unify(falseType, trueType, type))
+    if (!Join(env_.gcTypesEnabled, falseType, trueType, type))
         return fail("select operand types must match");
 
     infalliblePush(*type);
     return true;
 }
 
 template <typename Policy>
 inline bool
@@ -1592,16 +1664,27 @@ OpIter<Policy>::readB32x4Const(I32x4* i3
     MOZ_ASSERT(Classify(op_) == OpKind::B32x4);
 
     return readFixedI32x4(i32x4) &&
            push(ValType::B32x4);
 }
 
 template <typename Policy>
 inline bool
+OpIter<Policy>::readRefNull()
+{
+    MOZ_ASSERT(Classify(op_) == OpKind::RefNull);
+    uint8_t valType;
+    if (!d_.readValType(&valType) || ValType(valType) != ValType::AnyRef)
+        return fail("unknown nullref type");
+    return push(StackType::AnyRef);
+}
+
+template <typename Policy>
+inline bool
 OpIter<Policy>::popCallArgs(const ValTypeVector& expectedTypes, ValueVector* values)
 {
     // Iterate through the argument types backward so that pops occur in the
     // right order.
 
     if (!values->resize(expectedTypes.length()))
         return false;
 
diff --git a/js/src/wasm/WasmBinaryToAST.cpp b/js/src/wasm/WasmBinaryToAST.cpp
--- a/js/src/wasm/WasmBinaryToAST.cpp
+++ b/js/src/wasm/WasmBinaryToAST.cpp
@@ -1819,17 +1819,17 @@ AstDecodeFunctionBody(AstDecodeContext &
     const uint8_t* bodyEnd = bodyBegin + bodySize;
 
     const SigWithId* sig = c.env().funcSigs[funcIndex];
 
     ValTypeVector locals;
     if (!locals.appendAll(sig->args()))
         return false;
 
-    if (!DecodeLocalEntries(c.d, ModuleKind::Wasm, &locals))
+    if (!DecodeLocalEntries(c.d, ModuleKind::Wasm, c.env().gcTypesEnabled, &locals))
         return false;
 
     AstDecodeOpIter iter(c.env(), c.d);
     c.startFunction(&iter, &locals, sig->ret());
 
     AstName funcName;
     if (!GenerateName(c, AstName(u"func"), funcIndex, &funcName))
         return false;
diff --git a/js/src/wasm/WasmDebug.cpp b/js/src/wasm/WasmDebug.cpp
--- a/js/src/wasm/WasmDebug.cpp
+++ b/js/src/wasm/WasmDebug.cpp
@@ -537,17 +537,17 @@ DebugState::debugGetLocalTypes(uint32_t 
 
     // Decode local var types from wasm binary function body.
     const CodeRange& range = codeRanges(Tier::Debug)[debugFuncToCodeRangeIndex(funcIndex)];
     // In wasm, the Code points to the function start via funcLineOrBytecode.
     MOZ_ASSERT(!metadata().isAsmJS() && maybeBytecode_);
     size_t offsetInModule = range.funcLineOrBytecode();
     Decoder d(maybeBytecode_->begin() + offsetInModule,  maybeBytecode_->end(),
               offsetInModule, /* error = */ nullptr);
-    return DecodeLocalEntries(d, metadata().kind, locals);
+    return DecodeLocalEntries(d, metadata().kind, metadata().temporaryHasGcTypes, locals);
 }
 
 ExprType
 DebugState::debugGetResultType(uint32_t funcIndex)
 {
     MOZ_ASSERT(debugEnabled());
     return metadata().debugFuncReturnTypes[funcIndex];
 }
diff --git a/js/src/wasm/WasmIonCompile.cpp b/js/src/wasm/WasmIonCompile.cpp
--- a/js/src/wasm/WasmIonCompile.cpp
+++ b/js/src/wasm/WasmIonCompile.cpp
@@ -214,16 +214,19 @@ class FunctionCompiler
                 ins = MConstant::NewInt64(alloc(), 0);
                 break;
               case ValType::F32:
                 ins = MConstant::New(alloc(), Float32Value(0.f), MIRType::Float32);
                 break;
               case ValType::F64:
                 ins = MConstant::New(alloc(), DoubleValue(0.0), MIRType::Double);
                 break;
+              case ValType::AnyRef:
+                MOZ_CRASH("ion support for anyref locale default value NYI");
+                break;
               case ValType::I8x16:
                 ins = MSimdConstant::New(alloc(), SimdConstant::SplatX16(0), MIRType::Int8x16);
                 break;
               case ValType::I16x8:
                 ins = MSimdConstant::New(alloc(), SimdConstant::SplatX8(0), MIRType::Int16x8);
                 break;
               case ValType::I32x4:
                 ins = MSimdConstant::New(alloc(), SimdConstant::SplatX4(0), MIRType::Int32x4);
@@ -2975,16 +2978,17 @@ SimdToLaneType(ValType type)
       case ValType::F32x4:  return ValType::F32;
       case ValType::B8x16:
       case ValType::B16x8:
       case ValType::B32x4:  return ValType::I32; // Boolean lanes are Int32 in asm.
       case ValType::I32:
       case ValType::I64:
       case ValType::F32:
       case ValType::F64:
+      case ValType::AnyRef:
         break;
     }
     MOZ_CRASH("bad simd type");
 }
 
 static bool
 EmitExtractLane(FunctionCompiler& f, ValType operandType, SimdSign sign)
 {
@@ -3255,16 +3259,17 @@ EmitSimdCtor(FunctionCompiler& f, ValTyp
         f.iter().setResult(f.constructSimd<MSimdValueX4>(args[0], args[1], args[2], args[3],
                            MIRType::Bool32x4));
         return true;
       }
       case ValType::I32:
       case ValType::I64:
       case ValType::F32:
       case ValType::F64:
+      case ValType::AnyRef:
         break;
     }
     MOZ_CRASH("unexpected SIMD type");
 }
 
 static bool
 EmitSimdOp(FunctionCompiler& f, ValType type, SimdOperation op, SimdSign sign)
 {
@@ -3968,16 +3973,21 @@ EmitBodyExprs(FunctionCompiler& f)
             CHECK(EmitReinterpret(f, ValType::I32, ValType::F32, MIRType::Int32));
           case uint16_t(Op::I64ReinterpretF64):
             CHECK(EmitReinterpret(f, ValType::I64, ValType::F64, MIRType::Int64));
           case uint16_t(Op::F32ReinterpretI32):
             CHECK(EmitReinterpret(f, ValType::F32, ValType::I32, MIRType::Float32));
           case uint16_t(Op::F64ReinterpretI64):
             CHECK(EmitReinterpret(f, ValType::F64, ValType::I64, MIRType::Double));
 
+          // GC types are NYI in Ion.
+          case uint16_t(Op::RefNull):
+          case uint16_t(Op::RefIsNull):
+            return f.iter().unrecognizedOpcode(&op);
+
           // Sign extensions
 #ifdef ENABLE_WASM_SIGNEXTEND_OPS
           case uint16_t(Op::I32Extend8S):
             CHECK(EmitSignExtend(f, 1, 4));
           case uint16_t(Op::I32Extend16S):
             CHECK(EmitSignExtend(f, 2, 4));
           case uint16_t(Op::I64Extend8S):
             CHECK(EmitSignExtend(f, 1, 8));
@@ -4381,17 +4391,17 @@ wasm::IonCompileFunctions(const ModuleEn
     for (const FuncCompileInput& func : inputs) {
         Decoder d(func.begin, func.end, func.lineOrBytecode, error);
 
         // Build the local types vector.
 
         ValTypeVector locals;
         if (!locals.appendAll(env.funcSigs[func.index]->args()))
             return false;
-        if (!DecodeLocalEntries(d, env.kind, &locals))
+        if (!DecodeLocalEntries(d, env.kind, env.gcTypesEnabled, &locals))
             return false;
 
         // Set up for Ion compilation.
 
         const JitCompileOptions options;
         MIRGraph graph(&alloc);
         CompileInfo compileInfo(locals.length());
         MIRGenerator mir(nullptr, options, &alloc, &graph, &compileInfo,
diff --git a/js/src/wasm/WasmTypes.cpp b/js/src/wasm/WasmTypes.cpp
--- a/js/src/wasm/WasmTypes.cpp
+++ b/js/src/wasm/WasmTypes.cpp
@@ -67,16 +67,19 @@ Val::writePayload(uint8_t* dst) const
       case ValType::I16x8:
       case ValType::I32x4:
       case ValType::F32x4:
       case ValType::B8x16:
       case ValType::B16x8:
       case ValType::B32x4:
         memcpy(dst, &u, jit::Simd128DataSize);
         return;
+      case ValType::AnyRef:
+        // TODO
+        MOZ_CRASH("writing imported value of AnyRef in global NYI");
     }
 }
 
 bool
 wasm::IsRoundingFunction(SymbolicAddress callee, jit::RoundingMode* mode)
 {
     switch (callee) {
       case SymbolicAddress::FloorD:
@@ -167,53 +170,56 @@ Sig::sizeOfExcludingThis(MallocSizeOf ma
     return args_.sizeOfExcludingThis(mallocSizeOf);
 }
 
 typedef uint32_t ImmediateType;  // for 32/64 consistency
 static const unsigned sTotalBits = sizeof(ImmediateType) * 8;
 static const unsigned sTagBits = 1;
 static const unsigned sReturnBit = 1;
 static const unsigned sLengthBits = 4;
-static const unsigned sTypeBits = 2;
+static const unsigned sTypeBits = 3;
 static const unsigned sMaxTypes = (sTotalBits - sTagBits - sReturnBit - sLengthBits) / sTypeBits;
 
 static bool
 IsImmediateType(ValType vt)
 {
     switch (vt) {
       case ValType::I32:
       case ValType::I64:
       case ValType::F32:
       case ValType::F64:
+      case ValType::AnyRef:
         return true;
       case ValType::I8x16:
       case ValType::I16x8:
       case ValType::I32x4:
       case ValType::F32x4:
       case ValType::B8x16:
       case ValType::B16x8:
       case ValType::B32x4:
         return false;
     }
     MOZ_CRASH("bad ValType");
 }
 
 static unsigned
 EncodeImmediateType(ValType vt)
 {
-    static_assert(3 < (1 << sTypeBits), "fits");
+    static_assert(4 < (1 << sTypeBits), "fits");
     switch (vt) {
       case ValType::I32:
         return 0;
       case ValType::I64:
         return 1;
       case ValType::F32:
         return 2;
       case ValType::F64:
         return 3;
+      case ValType::AnyRef:
+        return 4;
       case ValType::I8x16:
       case ValType::I16x8:
       case ValType::I32x4:
       case ValType::F32x4:
       case ValType::B8x16:
       case ValType::B16x8:
       case ValType::B32x4:
         break;
diff --git a/js/src/wasm/WasmTypes.h b/js/src/wasm/WasmTypes.h
--- a/js/src/wasm/WasmTypes.h
+++ b/js/src/wasm/WasmTypes.h
@@ -178,19 +178,20 @@ SizeOf(ValType vt)
       case ValType::I8x16:
       case ValType::I16x8:
       case ValType::I32x4:
       case ValType::F32x4:
       case ValType::B8x16:
       case ValType::B16x8:
       case ValType::B32x4:
         return 16;
-      default:
-        MOZ_CRASH("Invalid ValType");
+      case ValType::AnyRef:
+        MOZ_CRASH("unexpected anyref");
     }
+    MOZ_CRASH("Invalid ValType");
 }
 
 static inline bool
 IsSimdType(ValType vt)
 {
     switch (vt) {
       case ValType::I8x16:
       case ValType::I16x8:
@@ -270,54 +271,68 @@ IsSimdBoolType(ValType vt)
 {
     return vt == ValType::B8x16 || vt == ValType::B16x8 || vt == ValType::B32x4;
 }
 
 static inline jit::MIRType
 ToMIRType(ValType vt)
 {
     switch (vt) {
-      case ValType::I32: return jit::MIRType::Int32;
-      case ValType::I64: return jit::MIRType::Int64;
-      case ValType::F32: return jit::MIRType::Float32;
-      case ValType::F64: return jit::MIRType::Double;
-      case ValType::I8x16: return jit::MIRType::Int8x16;
-      case ValType::I16x8: return jit::MIRType::Int16x8;
-      case ValType::I32x4: return jit::MIRType::Int32x4;
-      case ValType::F32x4: return jit::MIRType::Float32x4;
-      case ValType::B8x16: return jit::MIRType::Bool8x16;
-      case ValType::B16x8: return jit::MIRType::Bool16x8;
-      case ValType::B32x4: return jit::MIRType::Bool32x4;
+      case ValType::I32:    return jit::MIRType::Int32;
+      case ValType::I64:    return jit::MIRType::Int64;
+      case ValType::F32:    return jit::MIRType::Float32;
+      case ValType::F64:    return jit::MIRType::Double;
+      case ValType::AnyRef: return jit::MIRType::Pointer;
+      case ValType::I8x16:  return jit::MIRType::Int8x16;
+      case ValType::I16x8:  return jit::MIRType::Int16x8;
+      case ValType::I32x4:  return jit::MIRType::Int32x4;
+      case ValType::F32x4:  return jit::MIRType::Float32x4;
+      case ValType::B8x16:  return jit::MIRType::Bool8x16;
+      case ValType::B16x8:  return jit::MIRType::Bool16x8;
+      case ValType::B32x4:  return jit::MIRType::Bool32x4;
     }
     MOZ_MAKE_COMPILER_ASSUME_IS_UNREACHABLE("bad type");
 }
 
+static inline bool
+IsRefType(ValType vt)
+{
+    return vt == ValType::AnyRef;
+}
+
+static inline bool
+IsNumberType(ValType vt)
+{
+    return !IsRefType(vt);
+}
+
 // The ExprType enum represents the type of a WebAssembly expression or return
 // value and may either be a value type or void. Soon, expression types will be
 // generalized to a list of ValType and this enum will go away, replaced,
 // wherever it is used, by a varU32 + list of ValType.
 
 enum class ExprType
 {
-    Void  = uint8_t(TypeCode::BlockVoid),
+    Void   = uint8_t(TypeCode::BlockVoid),
 
-    I32   = uint8_t(TypeCode::I32),
-    I64   = uint8_t(TypeCode::I64),
-    F32   = uint8_t(TypeCode::F32),
-    F64   = uint8_t(TypeCode::F64),
+    I32    = uint8_t(TypeCode::I32),
+    I64    = uint8_t(TypeCode::I64),
+    F32    = uint8_t(TypeCode::F32),
+    F64    = uint8_t(TypeCode::F64),
+    AnyRef = uint8_t(TypeCode::AnyRef),
 
-    I8x16 = uint8_t(TypeCode::I8x16),
-    I16x8 = uint8_t(TypeCode::I16x8),
-    I32x4 = uint8_t(TypeCode::I32x4),
-    F32x4 = uint8_t(TypeCode::F32x4),
-    B8x16 = uint8_t(TypeCode::B8x16),
-    B16x8 = uint8_t(TypeCode::B16x8),
-    B32x4 = uint8_t(TypeCode::B32x4),
+    I8x16  = uint8_t(TypeCode::I8x16),
+    I16x8  = uint8_t(TypeCode::I16x8),
+    I32x4  = uint8_t(TypeCode::I32x4),
+    F32x4  = uint8_t(TypeCode::F32x4),
+    B8x16  = uint8_t(TypeCode::B8x16),
+    B16x8  = uint8_t(TypeCode::B16x8),
+    B32x4  = uint8_t(TypeCode::B32x4),
 
-    Limit = uint8_t(TypeCode::Limit)
+    Limit  = uint8_t(TypeCode::Limit)
 };
 
 static inline bool
 IsVoid(ExprType et)
 {
     return et == ExprType::Void;
 }
 
@@ -345,28 +360,29 @@ ToMIRType(ExprType et)
 {
     return IsVoid(et) ? jit::MIRType::None : ToMIRType(ValType(et));
 }
 
 static inline const char*
 ToCString(ExprType type)
 {
     switch (type) {
-      case ExprType::Void:  return "void";
-      case ExprType::I32:   return "i32";
-      case ExprType::I64:   return "i64";
-      case ExprType::F32:   return "f32";
-      case ExprType::F64:   return "f64";
-      case ExprType::I8x16: return "i8x16";
-      case ExprType::I16x8: return "i16x8";
-      case ExprType::I32x4: return "i32x4";
-      case ExprType::F32x4: return "f32x4";
-      case ExprType::B8x16: return "b8x16";
-      case ExprType::B16x8: return "b16x8";
-      case ExprType::B32x4: return "b32x4";
+      case ExprType::Void:    return "void";
+      case ExprType::I32:     return "i32";
+      case ExprType::I64:     return "i64";
+      case ExprType::F32:     return "f32";
+      case ExprType::F64:     return "f64";
+      case ExprType::AnyRef:  return "anyref";
+      case ExprType::I8x16:   return "i8x16";
+      case ExprType::I16x8:   return "i16x8";
+      case ExprType::I32x4:   return "i32x4";
+      case ExprType::F32x4:   return "f32x4";
+      case ExprType::B8x16:   return "b8x16";
+      case ExprType::B16x8:   return "b16x8";
+      case ExprType::B32x4:   return "b32x4";
       case ExprType::Limit:;
     }
     MOZ_CRASH("bad expression type");
 }
 
 static inline const char*
 ToCString(ValType type)
 {
diff --git a/js/src/wasm/WasmValidate.cpp b/js/src/wasm/WasmValidate.cpp
--- a/js/src/wasm/WasmValidate.cpp
+++ b/js/src/wasm/WasmValidate.cpp
@@ -297,29 +297,34 @@ wasm::EncodeLocalEntries(Encoder& e, con
         if (!e.writeValType(prev))
             return false;
     }
 
     return true;
 }
 
 static bool
-DecodeValType(Decoder& d, ModuleKind kind, ValType* type)
+DecodeValType(Decoder& d, ModuleKind kind, HasGcTypes gcTypesEnabled, ValType* type)
 {
     uint8_t unchecked;
     if (!d.readValType(&unchecked))
         return false;
 
     switch (unchecked) {
       case uint8_t(ValType::I32):
       case uint8_t(ValType::F32):
       case uint8_t(ValType::F64):
       case uint8_t(ValType::I64):
         *type = ValType(unchecked);
         return true;
+      case uint8_t(ValType::AnyRef):
+        if (gcTypesEnabled == HasGcTypes::False)
+            break;
+        *type = ValType(unchecked);
+        return true;
       case uint8_t(ValType::I8x16):
       case uint8_t(ValType::I16x8):
       case uint8_t(ValType::I32x4):
       case uint8_t(ValType::F32x4):
       case uint8_t(ValType::B8x16):
       case uint8_t(ValType::B16x8):
       case uint8_t(ValType::B32x4):
         if (kind != ModuleKind::AsmJS)
@@ -328,32 +333,33 @@ DecodeValType(Decoder& d, ModuleKind kin
         return true;
       default:
         break;
     }
     return d.fail("bad type");
 }
 
 bool
-wasm::DecodeLocalEntries(Decoder& d, ModuleKind kind, ValTypeVector* locals)
+wasm::DecodeLocalEntries(Decoder& d, ModuleKind kind, HasGcTypes gcTypesEnabled,
+                         ValTypeVector* locals)
 {
     uint32_t numLocalEntries;
     if (!d.readVarU32(&numLocalEntries))
         return d.fail("failed to read number of local entries");
 
     for (uint32_t i = 0; i < numLocalEntries; i++) {
         uint32_t count;
         if (!d.readVarU32(&count))
             return d.fail("failed to read local entry count");
 
         if (MaxLocals - locals->length() < count)
             return d.fail("too many locals");
 
         ValType type;
-        if (!DecodeValType(d, kind, &type))
+        if (!DecodeValType(d, kind, gcTypesEnabled, &type))
             return false;
 
         if (!locals->appendN(type, count))
             return false;
     }
 
     return true;
 }
@@ -737,16 +743,30 @@ DecodeFunctionBodyExprs(const ModuleEnvi
               default:
                 return iter.unrecognizedOpcode(&op);
             }
             break;
 #else
             return iter.unrecognizedOpcode(&op);
 #endif
           }
+#ifdef ENABLE_WASM_GC
+          case uint16_t(Op::RefNull): {
+            if (env.gcTypesEnabled == HasGcTypes::False)
+                return iter.unrecognizedOpcode(&op);
+            CHECK(iter.readRefNull());
+            break;
+          }
+          case uint16_t(Op::RefIsNull): {
+            if (env.gcTypesEnabled == HasGcTypes::False)
+                return iter.unrecognizedOpcode(&op);
+            CHECK(iter.readConversion(ValType::AnyRef, ValType::I32, &nothing));
+            break;
+          }
+#endif
           case uint16_t(Op::ThreadPrefix): {
 #ifdef ENABLE_WASM_THREAD_OPS
             switch (op.b1) {
               case uint16_t(ThreadOp::Wake): {
                 LinearMemoryAddress<Nothing> addr;
                 CHECK(iter.readWake(&addr, &nothing));
               }
               case uint16_t(ThreadOp::I32Wait): {
@@ -931,17 +951,17 @@ wasm::ValidateFunctionBody(const ModuleE
     const Sig& sig = *env.funcSigs[funcIndex];
 
     ValTypeVector locals;
     if (!locals.appendAll(sig.args()))
         return false;
 
     const uint8_t* bodyBegin = d.currentPosition();
 
-    if (!DecodeLocalEntries(d, ModuleKind::Wasm, &locals))
+    if (!DecodeLocalEntries(d, ModuleKind::Wasm, env.gcTypesEnabled, &locals))
         return false;
 
     if (!DecodeFunctionBodyExprs(env, sig, locals, bodyBegin + bodySize, &d))
         return false;
 
     return true;
 }
 
@@ -996,32 +1016,32 @@ DecodeTypeSection(Decoder& d, ModuleEnvi
         if (numArgs > MaxParams)
             return d.fail("too many arguments in signature");
 
         ValTypeVector args;
         if (!args.resize(numArgs))
             return false;
 
         for (uint32_t i = 0; i < numArgs; i++) {
-            if (!DecodeValType(d, ModuleKind::Wasm, &args[i]))
+            if (!DecodeValType(d, ModuleKind::Wasm, env->gcTypesEnabled, &args[i]))
                 return false;
         }
 
         uint32_t numRets;
         if (!d.readVarU32(&numRets))
             return d.fail("bad number of function returns");
 
         if (numRets > 1)
             return d.fail("too many returns in signature");
 
         ExprType result = ExprType::Void;
 
         if (numRets == 1) {
             ValType type;
-            if (!DecodeValType(d, ModuleKind::Wasm, &type))
+            if (!DecodeValType(d, ModuleKind::Wasm, env->gcTypesEnabled, &type))
                 return false;
 
             result = ToExprType(type);
         }
 
         env->sigs[sigIndex] = Sig(Move(args), result);
     }
 
@@ -1156,17 +1176,18 @@ GlobalIsJSCompatible(Decoder& d, ValType
 #endif
 
     return true;
 }
 
 static bool
 DecodeGlobalType(Decoder& d, ValType* type, bool* isMutable)
 {
-    if (!DecodeValType(d, ModuleKind::Wasm, type))
+    // No gc types in globals at the moment.
+    if (!DecodeValType(d, ModuleKind::Wasm, HasGcTypes::False, type))
         return false;
 
     uint8_t flags;
     if (!d.readFixedU8(&flags))
         return d.fail("expected global flags");
 
     if (flags & ~uint8_t(GlobalTypeImmediate::AllowedMask))
         return d.fail("unexpected bits set in global flags");
diff --git a/js/src/wasm/WasmValidate.h b/js/src/wasm/WasmValidate.h
--- a/js/src/wasm/WasmValidate.h
+++ b/js/src/wasm/WasmValidate.h
@@ -694,17 +694,17 @@ class Decoder
 
 // The local entries are part of function bodies and thus serialized by both
 // wasm and asm.js and decoded as part of both validation and compilation.
 
 MOZ_MUST_USE bool
 EncodeLocalEntries(Encoder& d, const ValTypeVector& locals);
 
 MOZ_MUST_USE bool
-DecodeLocalEntries(Decoder& d, ModuleKind kind, ValTypeVector* locals);
+DecodeLocalEntries(Decoder& d, ModuleKind kind, HasGcTypes gcTypesEnabled, ValTypeVector* locals);
 
 // Returns whether the given [begin, end) prefix of a module's bytecode starts a
 // code section and, if so, returns the SectionRange of that code section.
 // Note that, even if this function returns 'false', [begin, end) may actually
 // be a valid module in the special case when there are no function defs and the
 // code section is not present. Such modules can be valid so the caller must
 // handle this special case.
 
