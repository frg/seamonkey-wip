# HG changeset patch
# User Jan Keromnes <janx@linux.com>
# Date 1529384760 -10800
# Node ID e276f2617d4e8494c06e78660f71af722b534ac4
# Parent  0eadd6a3d21c9d408e2a75dab75076c2b6ae3fb8
Bug 1468811 - Fail static-analysis autotest when a clang-tidy check doesn't exist. r=sylvestre

diff --git a/python/mozbuild/mozbuild/mach_commands.py b/python/mozbuild/mozbuild/mach_commands.py
--- a/python/mozbuild/mozbuild/mach_commands.py
+++ b/python/mozbuild/mozbuild/mach_commands.py
@@ -1727,16 +1727,17 @@ class StaticAnalysis(MachCommandBase):
         # Function return codes
         self.TOOLS_SUCCESS = 0
         self.TOOLS_FAILED_DOWNLOAD = 1
         self.TOOLS_UNSUPORTED_PLATFORM = 2
         self.TOOLS_CHECKER_NO_TEST_FILE = 3
         self.TOOLS_CHECKER_RETURNED_NO_ISSUES = 4
         self.TOOLS_CHECKER_RESULT_FILE_NOT_FOUND = 5
         self.TOOLS_CHECKER_DIFF_FAILED = 6
+        self.TOOLS_CHECKER_NOT_FOUND = 7
 
         # Configure the tree or download clang-tidy package, depending on the option that we choose
         if intree_tool:
             _, config, _ = self._get_config_environment()
             clang_tools_path = self.topsrcdir
             self._clang_tidy_path = mozpath.join(
                 clang_tools_path, "clang", "bin",
                 "clang-tidy" + config.substs.get('BIN_SUFFIX', ''))
@@ -1776,16 +1777,23 @@ class StaticAnalysis(MachCommandBase):
         import multiprocessing
 
         max_workers = multiprocessing.cpu_count()
 
         self.log(logging.INFO, 'static-analysis', {},
                  "RUNNING: clang-tidy autotest for platform {0} with {1} workers.".format(
                      platform, max_workers))
 
+        # List all available checkers
+        cmd = [self._clang_tidy_path, '-list-checks', '-checks=*']
+        clang_output = subprocess.check_output(
+            cmd, stderr=subprocess.STDOUT).decode('utf-8')
+        available_checks = clang_output.split('\n')[1:]
+        self._clang_tidy_checks = [c.strip() for c in available_checks if c]
+
         with concurrent.futures.ThreadPoolExecutor(max_workers=max_workers) as executor:
             futures = []
             for item in config['clang_checkers']:
                 # Do not test mozilla specific checks nor the default '-*'
                 if not (item['publish'] and ('restricted-platforms' in item
                                              and platform not in item['restricted-platforms']
                                              or 'restricted-platforms' not in item)
                         and item['name'] not in ['mozilla-*', '-*'] and
@@ -1866,17 +1874,22 @@ class StaticAnalysis(MachCommandBase):
     def _verify_checker(self, item):
         check = item['name']
         test_file_path = mozpath.join(self._clang_tidy_base_path, "test", check)
         test_file_path_cpp = test_file_path + '.cpp'
         test_file_path_json = test_file_path + '.json'
 
         self.log(logging.INFO, 'static-analysis', {},"RUNNING: clang-tidy checker {}.".format(check))
 
-        # Verify is test file exists for checker
+        # Verify if this checker actually exists
+        if not check in self._clang_tidy_checks:
+            self.log(logging.ERROR, 'static-analysis', {}, "ERROR: clang-tidy checker {} doesn't exist in this clang-tidy version.".format(check))
+            return self.TOOLS_CHECKER_NOT_FOUND
+
+        # Verify if the test file exists for this checker
         if not os.path.exists(test_file_path_cpp):
             self.log(logging.ERROR, 'static-analysis', {}, "ERROR: clang-tidy checker {} doesn't have a test file.".format(check))
             return self.TOOLS_CHECKER_NO_TEST_FILE
 
         cmd = [self._clang_tidy_path, '-checks=-*, ' + check, test_file_path_cpp]
 
         clang_output = subprocess.check_output(
             cmd, stderr=subprocess.STDOUT).decode('utf-8')
