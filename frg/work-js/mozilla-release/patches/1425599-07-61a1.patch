# HG changeset patch
# User Mats Palmgren <mats@mozilla.com>
# Date 1521150084 -3600
#      Thu Mar 15 22:41:24 2018 +0100
# Node ID 6a271f8b8d435ad420f4538755097890faa4a118
# Parent  d94a08198cc99a918faac31eeb261ecf6dd5ab04
Bug 1425599 part 7 - [css-grid] Remove the 'limits' copy of track sizes since they are no longer needed (idempotent change).  r=dholbert

diff --git a/layout/generic/nsGridContainerFrame.cpp b/layout/generic/nsGridContainerFrame.cpp
--- a/layout/generic/nsGridContainerFrame.cpp
+++ b/layout/generic/nsGridContainerFrame.cpp
@@ -1187,17 +1187,16 @@ struct nsGridContainerFrame::Tracks
                                 nsTArray<TrackSize>& aPlan,
                                 nsTArray<TrackSize>& aItemPlan,
                                 TrackSize::StateBits aSelector,
                                 uint32_t aStartIndex,
                                 uint32_t aEndIndex);
   template<TrackSizingPhase phase>
   bool GrowLimitForSpanningItems(const nsTArray<Step2ItemData>& aItemData,
                                  nsTArray<uint32_t>& aTracks,
-                                 const nsTArray<TrackSize>& aSizes,
                                  nsTArray<TrackSize>& aPlan,
                                  nsTArray<TrackSize>& aItemPlan,
                                  TrackSize::StateBits aSelector,
                                  const TrackSizingFunctions& aFunctions,
                                  nscoord aPercentageBasis,
                                  uint32_t aStartIndex,
                                  uint32_t aEndIndex);
   /**
@@ -1501,17 +1500,16 @@ struct nsGridContainerFrame::Tracks
     }
   }
 
   /**
    * Distribute aAvailableSpace to the planned limits for aGrowableTracks.
    */
   template<TrackSizingPhase phase>
   void DistributeToTrackLimits(nscoord              aAvailableSpace,
-                               const nsTArray<TrackSize>& aSizes,
                                nsTArray<TrackSize>& aPlan,
                                nsTArray<TrackSize>& aItemPlan,
                                nsTArray<uint32_t>&  aGrowableTracks,
                                const TrackSizingFunctions& aFunctions,
                                nscoord                     aPercentageBasis)
   {
     auto fitContentClamper = [&aFunctions, aPercentageBasis] (uint32_t aTrack,
                                                               nscoord aMinSize,
@@ -4286,17 +4284,16 @@ nsGridContainerFrame::Tracks::GrowBaseFo
   return updatedBase;
 }
 
 template<nsGridContainerFrame::Tracks::TrackSizingPhase phase>
 bool
 nsGridContainerFrame::Tracks::GrowLimitForSpanningItems(
   const nsTArray<Step2ItemData>& aItemData,
   nsTArray<uint32_t>& aTracks,
-  const nsTArray<TrackSize>& aSizes,
   nsTArray<TrackSize>& aPlan,
   nsTArray<TrackSize>& aItemPlan,
   TrackSize::StateBits aSelector,
   const TrackSizingFunctions& aFunctions,
   nscoord aPercentageBasis,
   uint32_t aStartIndex,
   uint32_t aEndIndex)
 {
@@ -4310,17 +4307,17 @@ nsGridContainerFrame::Tracks::GrowLimitF
       aPlan[j].mState |= TrackSize::eModified;
     }
     nscoord space = item.SizeContributionForPhase(phase);
     if (space > 0) {
       aTracks.ClearAndRetainStorage();
       space = CollectGrowable<phase>(space, item.mLineRange, aSelector,
                                      aTracks);
       if (space > 0) {
-        DistributeToTrackLimits<phase>(space, aSizes, aPlan, aItemPlan, aTracks,
+        DistributeToTrackLimits<phase>(space, aPlan, aItemPlan, aTracks,
                                        aFunctions, aPercentageBasis);
       }
     }
   }
   return true;
 }
 
 void
@@ -4497,50 +4494,37 @@ nsGridContainerFrame::Tracks::ResolveInt
         for (TrackSize& sz : mSizes) {
           if (sz.mBase > sz.mLimit) {
             sz.mLimit = sz.mBase;
           }
         }
       }
 
       if (stateBitsPerSpan[span] & TrackSize::eIntrinsicMaxSizing) {
-        nsTArray<TrackSize> limits(mSizes);
-        for (TrackSize& sz : limits) {
-          if (sz.mLimit == NS_UNCONSTRAINEDSIZE) {
-            // use mBase as the planned limit
-          } else {
-            sz.mBase = sz.mLimit;
-          }
-        }
-
         // Step 2.5 MinSize to intrinsic max-sizing.
         GrowLimitForSpanningItems<TrackSizingPhase::eIntrinsicMaximums>(
-          step2Items, tracks, limits, plan, itemPlan, TrackSize::eIntrinsicMaxSizing,
+          step2Items, tracks, plan, itemPlan, TrackSize::eIntrinsicMaxSizing,
           aFunctions, aPercentageBasis, spanGroupStartIndex, spanGroupEndIndex);
 
         for (size_t j = 0, len = mSizes.Length(); j < len; ++j) {
           TrackSize& sz = itemPlan[j];
           sz.mState &= ~(TrackSize::eFrozen | TrackSize::eSkipGrowUnlimited);
           if (plan[j].mState & TrackSize::eModified) {
             if (mSizes[j].mLimit == NS_UNCONSTRAINEDSIZE) {
               mSizes[j].mState |= TrackSize::eInfinitelyGrowable;
             }
             mSizes[j].mLimit = plan[j].mBase;
-            limits[j].mBase = plan[j].mBase;
-            if (limits[j].mLimit != NS_UNCONSTRAINEDSIZE) {
-              limits[j].mLimit = limits[j].mBase;
-            }
           }
           plan[j].mState &= ~(TrackSize::eModified);
         }
 
         if (stateBitsPerSpan[span] & TrackSize::eAutoOrMaxContentMaxSizing) {
           // Step 2.6 MaxContentContribution to max-content max-sizing.
           GrowLimitForSpanningItems<TrackSizingPhase::eMaxContentMaximums>(
-            step2Items, tracks, limits, plan, itemPlan, TrackSize::eAutoOrMaxContentMaxSizing,
+            step2Items, tracks, plan, itemPlan, TrackSize::eAutoOrMaxContentMaxSizing,
             aFunctions, aPercentageBasis, spanGroupStartIndex, spanGroupEndIndex);
           
         }
         CopyPlanToLimit(plan);
       }
 
       i = spanGroupEndIndex;
     }
