# HG changeset patch
# User Andrea Marchesini <amarchesini@mozilla.com>
# Date 1515416705 -3600
# Node ID ab06b612becfad4d44692aa78c7c20c6badb3e71
# Parent  5dfdcabe1cd591cf75e00b032f38106457c334e4
Bug 1419771 - Introduce DOMPrefs, a thread-safe access to preferences for DOM - part 13 - Streams API enabled, r=asuth

diff --git a/dom/base/DOMPrefs.cpp b/dom/base/DOMPrefs.cpp
--- a/dom/base/DOMPrefs.cpp
+++ b/dom/base/DOMPrefs.cpp
@@ -41,16 +41,17 @@ PREF(PerformanceLoggingEnabled, "dom.per
 PREF(NotificationEnabled, "dom.webnotifications.enabled")
 PREF(NotificationEnabledInServiceWorkers, "dom.webnotifications.serviceworker.enabled")
 PREF(NotificationRIEnabled, "dom.webnotifications.requireinteraction.enabled")
 PREF(ServiceWorkersEnabled, "dom.serviceWorkers.enabled")
 PREF(ServiceWorkersTestingEnabled, "dom.serviceWorkers.testing.enabled")
 PREF(StorageManagerEnabled, "dom.storageManager.enabled")
 PREF(PromiseRejectionEventsEnabled, "dom.promise_rejection_events.enabled")
 PREF(PushEnabled, "dom.push.enabled")
+PREF(StreamsEnabled, "dom.streams.enabled")
 
 #undef PREF
 
 #define PREF_WEBIDL(name)                        \
   /* static */ bool                              \
   DOMPrefs::name(JSContext* aCx, JSObject* aObj) \
   {                                              \
     return DOMPrefs::name();                     \
@@ -59,13 +60,14 @@ PREF(PushEnabled, "dom.push.enabled")
 PREF_WEBIDL(ImageBitmapExtensionsEnabled)
 PREF_WEBIDL(DOMCachesEnabled)
 PREF_WEBIDL(NotificationEnabledInServiceWorkers)
 PREF_WEBIDL(NotificationRIEnabled)
 PREF_WEBIDL(ServiceWorkersEnabled)
 PREF_WEBIDL(StorageManagerEnabled)
 PREF_WEBIDL(PromiseRejectionEventsEnabled)
 PREF_WEBIDL(PushEnabled)
+PREF_WEBIDL(StreamsEnabled)
 
 #undef PREF_WEBIDL
 
 } // dom namespace
 } // mozilla namespace
diff --git a/dom/base/DOMPrefs.h b/dom/base/DOMPrefs.h
--- a/dom/base/DOMPrefs.h
+++ b/dom/base/DOMPrefs.h
@@ -58,14 +58,18 @@ public:
 
   // Returns true if the dom.promise_rejection_events.enabled pref is set.
   static bool PromiseRejectionEventsEnabled();
   static bool PromiseRejectionEventsEnabled(JSContext* aCx, JSObject* aObj);
 
   // Returns true if the dom.push.enabled pref is set.
   static bool PushEnabled();
   static bool PushEnabled(JSContext* aCx, JSObject* aObj);
+
+  // Returns true if the dom.streams.enabled pref is set.
+  static bool StreamsEnabled();
+  static bool StreamsEnabled(JSContext* aCx, JSObject* aObj);
 };
 
 } // dom namespace
 } // mozilla namespace
 
 #endif // mozilla_dom_DOMPrefs_h
diff --git a/dom/base/nsContentUtils.cpp b/dom/base/nsContentUtils.cpp
--- a/dom/base/nsContentUtils.cpp
+++ b/dom/base/nsContentUtils.cpp
@@ -9071,35 +9071,16 @@ nsContentUtils::GetReferrerPolicyFromHea
       referrerPolicy = policy;
     }
   }
   return referrerPolicy;
 }
 
 // static
 bool
-nsContentUtils::StreamsEnabled(JSContext* aCx, JSObject* aObj)
-{
-  if (NS_IsMainThread()) {
-    return Preferences::GetBool("dom.streams.enabled", false);
-  }
-
-  using namespace workers;
-
-  // Otherwise, check the pref via the WorkerPrivate
-  WorkerPrivate* workerPrivate = GetWorkerPrivateFromContext(aCx);
-  if (!workerPrivate) {
-    return false;
-  }
-
-  return workerPrivate->StreamsEnabled();
-}
-
-// static
-bool
 nsContentUtils::IsNonSubresourceRequest(nsIChannel* aChannel)
 {
   nsLoadFlags loadFlags = 0;
   aChannel->GetLoadFlags(&loadFlags);
   if (loadFlags & nsIChannel::LOAD_DOCUMENT_URI) {
     return true;
   }
 
diff --git a/dom/base/nsContentUtils.h b/dom/base/nsContentUtils.h
--- a/dom/base/nsContentUtils.h
+++ b/dom/base/nsContentUtils.h
@@ -2952,18 +2952,16 @@ public:
    * Parse a referrer policy from a Referrer-Policy header
    * https://www.w3.org/TR/referrer-policy/#parse-referrer-policy-from-header
    *
    * @param aHeader the response's Referrer-Policy header to parse
    * @return referrer policy from the response header.
    */
   static mozilla::net::ReferrerPolicy GetReferrerPolicyFromHeader(const nsAString& aHeader);
 
-  static bool StreamsEnabled(JSContext* aCx, JSObject* aObj);
-
   static bool IsNonSubresourceRequest(nsIChannel* aChannel);
 
   static uint32_t CookiesBehavior()
   {
     return sCookiesBehavior;
   }
 
   static uint32_t CookiesLifetimePolicy()
diff --git a/dom/webidl/Response.webidl b/dom/webidl/Response.webidl
--- a/dom/webidl/Response.webidl
+++ b/dom/webidl/Response.webidl
@@ -30,17 +30,17 @@ interface Response {
 
   [ChromeOnly, NewObject, Throws] Response cloneUnfiltered();
 };
 Response implements Body;
 
 // This should be part of Body but we don't want to expose body to request yet.
 // See bug 1387483.
 partial interface Response {
-  [GetterThrows, Func="nsContentUtils::StreamsEnabled"]
+  [GetterThrows, Func="mozilla::dom::DOMPrefs::StreamsEnabled"]
   readonly attribute ReadableStream? body;
 };
 
 dictionary ResponseInit {
   unsigned short status = 200;
   ByteString statusText = "OK";
   HeadersInit headers;
 };
diff --git a/dom/workers/WorkerPrefs.h b/dom/workers/WorkerPrefs.h
--- a/dom/workers/WorkerPrefs.h
+++ b/dom/workers/WorkerPrefs.h
@@ -16,17 +16,16 @@
 //     macro in Workers.h.
 //   * The name of the function that updates the new value of a pref.
 //
 //   WORKER_PREF("foo.bar", UpdaterFunction)
 //
 //   * First argument is the name of the pref.
 //   * The name of the function that updates the new value of a pref.
 
-WORKER_SIMPLE_PREF("dom.streams.enabled", StreamsEnabled, STREAMS_ENABLED)
 WORKER_SIMPLE_PREF("dom.requestcontext.enabled", RequestContextEnabled, REQUESTCONTEXT_ENABLED)
 WORKER_SIMPLE_PREF("gfx.offscreencanvas.enabled", OffscreenCanvasEnabled, OFFSCREENCANVAS_ENABLED)
 WORKER_SIMPLE_PREF("dom.webkitBlink.dirPicker.enabled", WebkitBlinkDirectoryPickerEnabled, DOM_WEBKITBLINK_DIRPICKER_WEBKITBLINK)
 WORKER_SIMPLE_PREF("dom.netinfo.enabled", NetworkInformationEnabled, NETWORKINFORMATION_ENABLED)
 WORKER_SIMPLE_PREF("dom.fetchObserver.enabled", FetchObserverEnabled, FETCHOBSERVER_ENABLED)
 WORKER_SIMPLE_PREF("privacy.resistFingerprinting", ResistFingerprintingEnabled, RESISTFINGERPRINTING_ENABLED)
 WORKER_SIMPLE_PREF("devtools.enabled", DevToolsEnabled, DEVTOOLS_ENABLED)
 WORKER_PREF("intl.accept_languages", PrefLanguagesChanged)
