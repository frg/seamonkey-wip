# HG changeset patch
# User Brad Werth <bwerth@mozilla.com>
# Date 1517432881 18000
# Node ID e187bbe58b54f821d0c29645d6a5154a897d8960
# Parent  8fbd85d84f96d16df06d81c2648277469a547060
Bug 1418930 Part 5: Update nsStyleStruct::FinishStyle and nsRuleNode::ComputeDisplayData to set CORS mode for shape-outside images. r=emilio

MozReview-Commit-ID: 7MXJHE2vidS

diff --git a/layout/style/nsRuleNode.cpp b/layout/style/nsRuleNode.cpp
--- a/layout/style/nsRuleNode.cpp
+++ b/layout/style/nsRuleNode.cpp
@@ -6456,16 +6456,20 @@ nsRuleNode::ComputeDisplayData(void* aSt
       display->mShapeOutside = parentDisplay->mShapeOutside;
       break;
     case eCSSUnit_Image:
     case eCSSUnit_Function:
     case eCSSUnit_Gradient:
     case eCSSUnit_Element: {
       auto shapeImage = MakeUnique<nsStyleImage>();
       SetStyleImage(aContext, *shapeOutsideValue, *shapeImage, conditions);
+      if (shapeImage->GetType() == eStyleImageType_Image) {
+        shapeImage->GetImageRequest()->GetImageValue()->SetCORSMode(
+          CORSMode::CORS_ANONYMOUS);
+      }
       display->mShapeOutside = StyleShapeSource();
       display->mShapeOutside.SetShapeImage(Move(shapeImage));
       break;
     }
     case eCSSUnit_Array: {
       display->mShapeOutside = StyleShapeSource();
       SetStyleShapeSourceToCSSValue(&display->mShapeOutside, shapeOutsideValue,
                                     aContext, mPresContext, conditions);
diff --git a/layout/style/nsStyleStruct.cpp b/layout/style/nsStyleStruct.cpp
--- a/layout/style/nsStyleStruct.cpp
+++ b/layout/style/nsStyleStruct.cpp
@@ -30,16 +30,17 @@
 
 #include "imgIRequest.h"
 #include "imgIContainer.h"
 #include "CounterStyleManager.h"
 
 #include "mozilla/dom/AnimationEffectReadOnlyBinding.h" // for PlaybackDirection
 #include "mozilla/dom/DocGroup.h"
 #include "mozilla/dom/ImageTracker.h"
+#include "mozilla/CORSMode.h"
 #include "mozilla/ClearOnShutdown.h"
 #include "mozilla/Likely.h"
 #include "nsIURI.h"
 #include "nsIDocument.h"
 #include <algorithm>
 #include "ImageLoader.h"
 
 using namespace mozilla;
@@ -3741,16 +3742,23 @@ void
 nsStyleDisplay::FinishStyle(nsPresContext* aPresContext)
 {
   MOZ_ASSERT(NS_IsMainThread());
   MOZ_ASSERT(aPresContext->StyleSet()->IsServo());
 
   if (mShapeOutside.GetType() == StyleShapeSourceType::Image) {
     const UniquePtr<nsStyleImage>& shapeImage = mShapeOutside.GetShapeImage();
     if (shapeImage) {
+      // Bug 1434963: The CORS mode should instead be set when the
+      // ImageValue is created, in both Gecko and Stylo. That will
+      // avoid doing a mutation here.
+      if (shapeImage->GetType() == eStyleImageType_Image) {
+        shapeImage->GetImageRequest()->GetImageValue()->SetCORSMode(
+          CORSMode::CORS_ANONYMOUS);
+      }
       shapeImage->ResolveImage(aPresContext);
     }
   }
 
   GenerateCombinedTransform();
 }
 
 static inline nsChangeHint
