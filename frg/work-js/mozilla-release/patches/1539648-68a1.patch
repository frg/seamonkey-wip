# HG changeset patch
# User Jeff Gilbert <jgilbert@mozilla.com>
# Date 1553722976 0
# Node ID 10d43b6ca5e9b36cf239835918248ab10204b996
# Parent  132a633dcb54fd3f0c8f041ecdc40d91c2f595dc
Bug 1539648 - Use ANGLE's CHROMIUM_color_buffer_float_rgba for WEBGL_color_buffer_float. r=lsalzman

Differential Revision: https://phabricator.services.mozilla.com/D25139

diff --git a/dom/canvas/WebGLExtensionColorBufferFloat.cpp.1539648.later b/dom/canvas/WebGLExtensionColorBufferFloat.cpp.1539648.later
new file mode 100644
--- /dev/null
+++ b/dom/canvas/WebGLExtensionColorBufferFloat.cpp.1539648.later
@@ -0,0 +1,27 @@
+--- WebGLExtensionColorBufferFloat.cpp
++++ WebGLExtensionColorBufferFloat.cpp
+@@ -34,24 +34,16 @@ WebGLExtensionColorBufferFloat::WebGLExt
+ }
+ 
+ WebGLExtensionColorBufferFloat::~WebGLExtensionColorBufferFloat() {}
+ 
+ bool WebGLExtensionColorBufferFloat::IsSupported(const WebGLContext* webgl) {
+   if (webgl->IsWebGL2()) return false;
+ 
+   const auto& gl = webgl->gl;
+-  if (gl->IsANGLE()) {
+-    // ANGLE supports this, but doesn't have a way to advertize its support,
+-    // since it's compliant with WEBGL_color_buffer_float's clamping, but not
+-    // EXT_color_buffer_float.
+-    // TODO: This probably isn't necessary anymore.
+-    return true;
+-  }
+-
+   return gl->IsSupported(gl::GLFeature::renderbuffer_color_float) &&
+          gl->IsSupported(gl::GLFeature::frag_color_float);
+ }
+ 
+ IMPL_WEBGL_EXTENSION_GOOP(WebGLExtensionColorBufferFloat,
+                           WEBGL_color_buffer_float)
+ 
+ }  // namespace mozilla
diff --git a/dom/canvas/WebGLExtensionTextureFloat.cpp.1539648.later b/dom/canvas/WebGLExtensionTextureFloat.cpp.1539648.later
new file mode 100644
--- /dev/null
+++ b/dom/canvas/WebGLExtensionTextureFloat.cpp.1539648.later
@@ -0,0 +1,22 @@
+--- WebGLExtensionTextureFloat.cpp
++++ WebGLExtensionTextureFloat.cpp
+@@ -36,17 +36,18 @@ WebGLExtensionTextureFloat::WebGLExtensi
+ 
+   const bool needsSizedFormat = !gl->IsGLES();
+ 
+   ////////////////
+ 
+   pi = {LOCAL_GL_RGBA, LOCAL_GL_FLOAT};
+   dui = {pi.format, pi.format, pi.type};
+   swizzle = nullptr;
+-  if (needsSizedFormat) {
++  if (needsSizedFormat ||
++      gl->IsExtensionSupported(gl::GLContext::CHROMIUM_color_buffer_float_rgba)) {
+     dui.internalFormat = LOCAL_GL_RGBA32F;
+   }
+   fnAdd(webgl::EffectiveFormat::RGBA32F);
+ 
+   //////
+ 
+   pi = {LOCAL_GL_RGB, LOCAL_GL_FLOAT};
+   dui = {pi.format, pi.format, pi.type};
diff --git a/dom/canvas/test/webgl-mochitest/test_webgl_color_buffer_float.html b/dom/canvas/test/webgl-mochitest/test_webgl_color_buffer_float.html
--- a/dom/canvas/test/webgl-mochitest/test_webgl_color_buffer_float.html
+++ b/dom/canvas/test/webgl-mochitest/test_webgl_color_buffer_float.html
@@ -144,17 +144,17 @@ function TestScreenColor(gl, isFBFloat, 
 
     default:
       throw 'Bad `readType`.';
   }
 
   var testText = RGBAToString(floatArr);
   var refText = RGBAToString([r, g, b, a]);
 
-  var eps = 1.0 / 255.0;
+  var eps = 1.0 / 16.0; // Must work with RGBA4!
   var isSame = (Math.abs(floatArr[0] - r) < eps &&
                 Math.abs(floatArr[1] - g) < eps &&
                 Math.abs(floatArr[2] - b) < eps &&
                 Math.abs(floatArr[3] - a) < eps);
 
   ok(isSame, 'Should be ' + refText + ', was ' + testText + ',');
 }
 
@@ -373,25 +373,31 @@ function TestType(gl, prog, isTex, sized
     TestScreenColor(gl, isFormatFloat, 0, 0.5, 1.0, 1);
 
   ////////
 
   ok(true, 'Blending:');
 
   gl.enable(gl.BLEND);
   gl.blendFunc(gl.CONSTANT_COLOR, gl.ZERO);
-  gl.blendColor(0, 10, 0.1, 1);
+  gl.blendColor(0, 6, 0.1, 1);
 
   gl.uniform4f(prog.uFragColor, 0, 0.5, 15.0, 1);
   gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
 
-  if (isFormatFloat)
-    TestScreenColor(gl, isFormatFloat, 0, 5.0, 1.5, 1);
-  else
-    TestScreenColor(gl, isFormatFloat, 0, 0.5, 0.1, 1);
+  let correctError = 0;
+  if (sizedFormat == RGBA32F && !gl.getExtension('EXT_float_blend')) {
+    correctError = gl.INVALID_OPERATION;
+  }
+  if (!TestError(gl, correctError, 'Is blending an error?')) {
+    if (isFormatFloat)
+      TestScreenColor(gl, isFormatFloat, 0, 3.0, 1.5, 1);
+    else
+      TestScreenColor(gl, isFormatFloat, 0, 0.5, 0.1, 1);
+  }
 
   gl.disable(gl.BLEND);
 
   //////////////////////////////////////
 }
 
 // Give ourselves a scope to return early from:
 (function() {
diff --git a/gfx/gl/GLContext.cpp b/gfx/gl/GLContext.cpp
--- a/gfx/gl/GLContext.cpp
+++ b/gfx/gl/GLContext.cpp
@@ -119,16 +119,18 @@ static const char* const sExtensionNames
     "GL_ARB_texture_rectangle",
     "GL_ARB_texture_rg",
     "GL_ARB_texture_storage",
     "GL_ARB_texture_swizzle",
     "GL_ARB_timer_query",
     "GL_ARB_transform_feedback2",
     "GL_ARB_uniform_buffer_object",
     "GL_ARB_vertex_array_object",
+    "GL_CHROMIUM_color_buffer_float_rgb",
+    "GL_CHROMIUM_color_buffer_float_rgba",
     "GL_EXT_bgra",
     "GL_EXT_blend_minmax",
     "GL_EXT_color_buffer_float",
     "GL_EXT_color_buffer_half_float",
     "GL_EXT_copy_texture",
     "GL_EXT_disjoint_timer_query",
     "GL_EXT_draw_buffers",
     "GL_EXT_draw_buffers2",
diff --git a/gfx/gl/GLContext.h b/gfx/gl/GLContext.h
--- a/gfx/gl/GLContext.h
+++ b/gfx/gl/GLContext.h
@@ -385,16 +385,18 @@ public:
         ARB_texture_rectangle,
         ARB_texture_rg,
         ARB_texture_storage,
         ARB_texture_swizzle,
         ARB_timer_query,
         ARB_transform_feedback2,
         ARB_uniform_buffer_object,
         ARB_vertex_array_object,
+        CHROMIUM_color_buffer_float_rgb,
+        CHROMIUM_color_buffer_float_rgba,
         EXT_bgra,
         EXT_blend_minmax,
         EXT_color_buffer_float,
         EXT_color_buffer_half_float,
         EXT_copy_texture,
         EXT_disjoint_timer_query,
         EXT_draw_buffers,
         EXT_draw_buffers2,
diff --git a/gfx/gl/GLContextFeatures.cpp b/gfx/gl/GLContextFeatures.cpp
--- a/gfx/gl/GLContextFeatures.cpp
+++ b/gfx/gl/GLContextFeatures.cpp
@@ -198,16 +198,17 @@ static const FeatureInfo sFeatureInfoArr
         "frag_color_float",
         GLVersion::GL3,
         GLESVersion::ES3,
         GLContext::Extension_None,
         {
             GLContext::ARB_color_buffer_float,
             GLContext::EXT_color_buffer_float,
             GLContext::EXT_color_buffer_half_float,
+            GLContext::CHROMIUM_color_buffer_float_rgba,
             GLContext::Extensions_End
         }
     },
     {
         "frag_depth",
         GLVersion::GL2,
         GLESVersion::ES3,
         GLContext::Extension_None,
@@ -502,16 +503,17 @@ static const FeatureInfo sFeatureInfoArr
     {
         "renderbuffer_color_float",
         GLVersion::GL3,
         GLESVersion::ES3_2,
         GLContext::Extension_None,
         {
             GLContext::ARB_texture_float,
             GLContext::EXT_color_buffer_float,
+            GLContext::CHROMIUM_color_buffer_float_rgba,
             GLContext::Extensions_End
         }
     },
     {
         "renderbuffer_color_half_float",
         GLVersion::GL3,
         GLESVersion::ES3_2,
         GLContext::Extension_None,
