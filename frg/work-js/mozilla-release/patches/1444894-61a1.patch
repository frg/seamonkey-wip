# HG changeset patch
# User Jason Orendorff <jorendorff@mozilla.com>
# Date 1520902446 18000
#      Mon Mar 12 19:54:06 2018 -0500
# Node ID 92d4018c82ba97188917a26f78010dccc103dbd9
# Parent  35c7481b2c2eac39e0a00a2d00398145b38813de
Bug 1444894 - Add a compartment assertion to js::BaselineCompile. r=nbp,jorendorff

diff --git a/js/src/builtin/TestingFunctions.cpp b/js/src/builtin/TestingFunctions.cpp
--- a/js/src/builtin/TestingFunctions.cpp
+++ b/js/src/builtin/TestingFunctions.cpp
@@ -5051,47 +5051,60 @@ BaselineCompile(JSContext* cx, unsigned 
         }
         if (!args[1].isBoolean() && !args[1].isUndefined()) {
             ReportUsageErrorASCII(cx, callee, "forceDebugInstrumentation argument should be boolean");
             return false;
         }
         forceDebug = ToBoolean(args[1]);
     }
 
-    if (script->hasBaselineScript()) {
-        if (forceDebug && !script->baselineScript()->hasDebugInstrumentation()) {
-            // There isn't an easy way to do this for a script that might be on
-            // stack right now. See js::jit::RecompileOnStackBaselineScriptsForDebugMode.
-            ReportUsageErrorASCII(cx, callee,
-                                  "unsupported case: recompiling script for debug mode");
-            return false;
+    const char* returnedStr = nullptr;
+    do {
+        AutoCompartment ac(cx, script);
+        if (script->hasBaselineScript()) {
+            if (forceDebug && !script->baselineScript()->hasDebugInstrumentation()) {
+                // There isn't an easy way to do this for a script that might be on
+                // stack right now. See js::jit::RecompileOnStackBaselineScriptsForDebugMode.
+                ReportUsageErrorASCII(cx, callee,
+                                      "unsupported case: recompiling script for debug mode");
+                return false;
+            }
+
+            args.rval().setUndefined();
+            return true;
+        }
+
+        if (!jit::IsBaselineEnabled(cx)) {
+            returnedStr = "baseline disabled";
+            break;
         }
-
-        args.rval().setUndefined();
-        return true;
-    }
-
-    if (!jit::IsBaselineEnabled(cx))
-        return ReturnStringCopy(cx, args, "baseline disabled");
-    if (!script->canBaselineCompile())
-        return ReturnStringCopy(cx, args, "can't compile");
-    if (!cx->compartment()->ensureJitCompartmentExists(cx))
-        return false;
-
-    jit::MethodStatus status = jit::BaselineCompile(cx, script, forceDebug);
-    switch (status) {
-      case jit::Method_Error:
-        return false;
-      case jit::Method_CantCompile:
-        return ReturnStringCopy(cx, args, "can't compile");
-      case jit::Method_Skipped:
-        return ReturnStringCopy(cx, args, "skipped");
-      case jit::Method_Compiled:
-        args.rval().setUndefined();
-    }
+        if (!script->canBaselineCompile()) {
+            returnedStr = "can't compile";
+            break;
+        }
+        if (!cx->compartment()->ensureJitCompartmentExists(cx))
+            return false;
+
+        jit::MethodStatus status = jit::BaselineCompile(cx, script, forceDebug);
+        switch (status) {
+          case jit::Method_Error:
+            return false;
+          case jit::Method_CantCompile:
+            returnedStr = "can't compile";
+            break;
+          case jit::Method_Skipped:
+            returnedStr = "skipped";
+            break;
+          case jit::Method_Compiled:
+            args.rval().setUndefined();
+        }
+    } while(false);
+
+    if (returnedStr)
+        return ReturnStringCopy(cx, args, returnedStr);
 
     return true;
 }
 
 static const JSFunctionSpecWithHelp TestingFunctions[] = {
     JS_FN_HELP("gc", ::GC, 0, 0,
 "gc([obj] | 'zone' [, 'shrinking'])",
 "  Run the garbage collector. When obj is given, GC only its zone.\n"
diff --git a/js/src/jit-test/tests/self-test/baselineCompile-Bug1444894.js b/js/src/jit-test/tests/self-test/baselineCompile-Bug1444894.js
new file mode 100644
--- /dev/null
+++ b/js/src/jit-test/tests/self-test/baselineCompile-Bug1444894.js
@@ -0,0 +1,5 @@
+
+if (typeof baselineCompile == "function") {
+    gc();
+    newGlobal().baselineCompile();
+}
diff --git a/js/src/jit/BaselineJIT.cpp b/js/src/jit/BaselineJIT.cpp
--- a/js/src/jit/BaselineJIT.cpp
+++ b/js/src/jit/BaselineJIT.cpp
@@ -229,16 +229,17 @@ jit::EnterBaselineAtBranch(JSContext* cx
 
     fp->setReturnValue(data.result);
     return JitExec_Ok;
 }
 
 MethodStatus
 jit::BaselineCompile(JSContext* cx, JSScript* script, bool forceDebugInstrumentation)
 {
+    assertSameCompartment(cx, script);
     MOZ_ASSERT(!script->hasBaselineScript());
     MOZ_ASSERT(script->canBaselineCompile());
     MOZ_ASSERT(IsBaselineEnabled(cx));
 
     script->ensureNonLazyCanonicalFunction();
 
     TempAllocator temp(&cx->tempLifoAlloc());
     JitContext jctx(cx, nullptr);
