# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1528919260 25200
#      Wed Jun 13 12:47:40 2018 -0700
# Node ID db248a2fb2024b0da563724cdb0abd85857d8919
# Parent  f5516888a7013a4b90e54f1065bae3842cebf974
Bug 1468137 - Remove JS_Get*Prototype APIs; use JS::GetRealm*Prototype instead. r=evilpie

diff --git a/dom/base/nsDOMClassInfo.cpp b/dom/base/nsDOMClassInfo.cpp
--- a/dom/base/nsDOMClassInfo.cpp
+++ b/dom/base/nsDOMClassInfo.cpp
@@ -1432,17 +1432,17 @@ ResolvePrototype(nsIXPConnect *aXPConnec
         if (!JS_WrapObject(cx, &proto) ||
             !JS_SetPrototype(cx, dot_prototype, proto)) {
           return NS_ERROR_UNEXPECTED;
         }
       }
     } else {
       JSAutoRealm ar(cx, winobj);
       if (!proto) {
-        proto = JS_GetObjectPrototype(cx, winobj);
+        proto = JS::GetRealmObjectPrototype(cx);
       }
       dot_prototype = ::JS_NewObjectWithUniqueType(cx,
                                                    &sDOMConstructorProtoClass,
                                                    proto);
       NS_ENSURE_TRUE(dot_prototype, NS_ERROR_OUT_OF_MEMORY);
     }
   }
 
diff --git a/js/public/Realm.h b/js/public/Realm.h
--- a/js/public/Realm.h
+++ b/js/public/Realm.h
@@ -1,20 +1,14 @@
 /* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
  * vim: set ts=8 sts=4 et sw=4 tw=99:
  * This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
-/*
- * Ways to get various per-Realm objects. All the getters declared in this
- * header operate on the Realm corresponding to the current compartment on the
- * JSContext.
- */
-
 #ifndef js_Realm_h
 #define js_Realm_h
 
 #include "jspubtd.h"
 #include "js/GCPolicyAPI.h"
 #include "js/TypeDecls.h"  // forward-declaration of JS::Realm
 
 namespace js {
@@ -109,16 +103,21 @@ typedef void
 extern JS_PUBLIC_API(void)
 SetRealmNameCallback(JSContext* cx, RealmNameCallback callback);
 
 // Get the global object for the given realm. This only returns nullptr during
 // GC, between collecting the global object and destroying the Realm.
 extern JS_PUBLIC_API(JSObject*)
 GetRealmGlobalOrNull(Handle<Realm*> realm);
 
+/*
+ * Ways to get various per-Realm objects. All the getters declared below operate
+ * on the JSContext's current Realm.
+ */
+
 extern JS_PUBLIC_API(JSObject*)
 GetRealmObjectPrototype(JSContext* cx);
 
 extern JS_PUBLIC_API(JSObject*)
 GetRealmFunctionPrototype(JSContext* cx);
 
 extern JS_PUBLIC_API(JSObject*)
 GetRealmArrayPrototype(JSContext* cx);
diff --git a/js/rust/build.rs b/js/rust/build.rs
--- a/js/rust/build.rs
+++ b/js/rust/build.rs
@@ -280,23 +280,19 @@ const WHITELIST_FUNCTIONS: &'static [&'s
     "JS_DeletePropertyById",
     "js::detail::IsWindowSlow",
     "JS::Evaluate",
     "JS_ForwardGetPropertyTo",
     "JS_ForwardSetPropertyTo",
     "JS::GCTraceKindToAscii",
     "js::GetArrayBufferLengthAndData",
     "js::GetArrayBufferViewLengthAndData",
-    "JS_GetErrorPrototype",
     "js::GetFunctionNativeReserved",
-    "JS_GetFunctionPrototype",
     "js::GetGlobalForObjectCrossCompartment",
-    "JS_GetIteratorPrototype",
     "js::GetObjectProto",
-    "JS_GetObjectPrototype",
     "JS_GetObjectRuntime",
     "JS_GetOwnPropertyDescriptorById",
     "JS::GetPromiseResult",
     "JS::GetPromiseState",
     "JS_GetPropertyDescriptorById",
     "js::GetPropertyKeys",
     "JS_GetPrototype",
     "JS_GetRuntime",
@@ -343,16 +339,20 @@ const WHITELIST_FUNCTIONS: &'static [&'s
     "JS_GetLatin1StringCharsAndLength",
     "JS_GetParentRuntime",
     "JS_GetPendingException",
     "JS_GetProperty",
     "JS_GetPropertyById",
     "js::GetPropertyKeys",
     "JS_GetPrototype",
     "JS_GetReservedSlot",
+    "JS::GetRealmErrorPrototype",
+    "JS::GetRealmFunctionPrototype",
+    "JS::GetRealmIteratorPrototype",
+    "JS::GetRealmObjectPrototype",
     "JS::GetScriptedCallerGlobal",
     "JS_GetTwoByteStringCharsAndLength",
     "JS_GetUint16ArrayData",
     "JS_GetUint32ArrayData",
     "JS_GetUint8ArrayData",
     "JS_GetUint8ClampedArrayData",
     "JS::GetWellKnownSymbol",
     "JS_GlobalObjectTraceHook",
diff --git a/js/rust/tests/rooting.rs b/js/rust/tests/rooting.rs
--- a/js/rust/tests/rooting.rs
+++ b/js/rust/tests/rooting.rs
@@ -25,17 +25,17 @@ fn rooting() {
         let c_option = JS::RealmOptions::default();
 
         rooted!(in(cx) let global = JS_NewGlobalObject(cx,
                                                        &SIMPLE_GLOBAL_CLASS,
                                                        ptr::null_mut(),
                                                        h_option,
                                                        &c_option));
         let _ac = js::ac::AutoCompartment::with_obj(cx, global.get());
-        rooted!(in(cx) let prototype_proto = JS_GetObjectPrototype(cx, global.handle()));
+        rooted!(in(cx) let prototype_proto = JS::GetRealmObjectPrototype(cx));
         rooted!(in(cx) let proto = JS_NewObjectWithUniqueType(cx,
                                                               &CLASS as *const _,
                                                               prototype_proto.handle()));
         define_methods(cx, proto.handle(), &METHODS[..]).unwrap();
     }
 }
 
 unsafe extern "C" fn generic_method(_: *mut JSContext, _: u32, _: *mut JS::Value) -> bool {
diff --git a/js/src/jsapi-tests/testGetPropertyDescriptor.cpp b/js/src/jsapi-tests/testGetPropertyDescriptor.cpp
--- a/js/src/jsapi-tests/testGetPropertyDescriptor.cpp
+++ b/js/src/jsapi-tests/testGetPropertyDescriptor.cpp
@@ -34,17 +34,17 @@ BEGIN_TEST(test_GetPropertyDescriptor)
   CHECK(value.isTrue());
   CHECK(JS_GetProperty(cx, descObj, "enumerable", &value));
   CHECK(value.isTrue());
 
   CHECK(JS_GetPropertyDescriptor(cx, obj, "not-here", &desc));
   CHECK_EQUAL(desc.object(), nullptr);
 
   CHECK(JS_GetPropertyDescriptor(cx, obj, "toString", &desc));
-  JS::RootedObject objectProto(cx, JS_GetObjectPrototype(cx, obj));
+  JS::RootedObject objectProto(cx, JS::GetRealmObjectPrototype(cx));
   CHECK(objectProto);
   CHECK_EQUAL(desc.object(), objectProto);
   CHECK(desc.value().isObject());
   CHECK(JS::IsCallable(&desc.value().toObject()));
 
   return true;
 }
 END_TEST(test_GetPropertyDescriptor)
diff --git a/js/src/jsapi.cpp b/js/src/jsapi.cpp
--- a/js/src/jsapi.cpp
+++ b/js/src/jsapi.cpp
@@ -1208,59 +1208,16 @@ JS_IdToProtoKey(JSContext* cx, HandleId 
     if (GlobalObject::skipDeselectedConstructor(cx, stdnm->key))
         return JSProto_Null;
 
     MOZ_ASSERT(MOZ_ARRAY_LENGTH(standard_class_names) == JSProto_LIMIT + 1);
     return static_cast<JSProtoKey>(stdnm - standard_class_names);
 }
 
 JS_PUBLIC_API(JSObject*)
-JS_GetObjectPrototype(JSContext* cx, HandleObject forObj)
-{
-    CHECK_REQUEST(cx);
-    assertSameCompartment(cx, forObj);
-    Rooted<GlobalObject*> global(cx, &forObj->global());
-    return GlobalObject::getOrCreateObjectPrototype(cx, global);
-}
-
-JS_PUBLIC_API(JSObject*)
-JS_GetFunctionPrototype(JSContext* cx, HandleObject forObj)
-{
-    CHECK_REQUEST(cx);
-    assertSameCompartment(cx, forObj);
-    Rooted<GlobalObject*> global(cx, &forObj->global());
-    return GlobalObject::getOrCreateFunctionPrototype(cx, global);
-}
-
-JS_PUBLIC_API(JSObject*)
-JS_GetArrayPrototype(JSContext* cx, HandleObject forObj)
-{
-    CHECK_REQUEST(cx);
-    assertSameCompartment(cx, forObj);
-    Rooted<GlobalObject*> global(cx, &forObj->global());
-    return GlobalObject::getOrCreateArrayPrototype(cx, global);
-}
-
-JS_PUBLIC_API(JSObject*)
-JS_GetErrorPrototype(JSContext* cx)
-{
-    CHECK_REQUEST(cx);
-    Rooted<GlobalObject*> global(cx, cx->global());
-    return GlobalObject::getOrCreateCustomErrorPrototype(cx, global, JSEXN_ERR);
-}
-
-JS_PUBLIC_API(JSObject*)
-JS_GetIteratorPrototype(JSContext* cx)
-{
-    CHECK_REQUEST(cx);
-    Rooted<GlobalObject*> global(cx, cx->global());
-    return GlobalObject::getOrCreateIteratorPrototype(cx, global);
-}
-
-JS_PUBLIC_API(JSObject*)
 JS_GetGlobalForObject(JSContext* cx, JSObject* obj)
 {
     AssertHeapIsIdle();
     assertSameCompartment(cx, obj);
     return &obj->global();
 }
 
 extern JS_PUBLIC_API(bool)
diff --git a/js/src/jsapi.h b/js/src/jsapi.h
--- a/js/src/jsapi.h
+++ b/js/src/jsapi.h
@@ -1154,51 +1154,16 @@ IdentifyStandardConstructor(JSObject* ob
 extern JS_PUBLIC_API(void)
 ProtoKeyToId(JSContext* cx, JSProtoKey key, JS::MutableHandleId idp);
 
 } /* namespace JS */
 
 extern JS_PUBLIC_API(JSProtoKey)
 JS_IdToProtoKey(JSContext* cx, JS::HandleId id);
 
-/**
- * Returns the original value of |Function.prototype| from the global object in
- * which |forObj| was created.
- */
-extern JS_PUBLIC_API(JSObject*)
-JS_GetFunctionPrototype(JSContext* cx, JS::HandleObject forObj);
-
-/**
- * Returns the original value of |Object.prototype| from the global object in
- * which |forObj| was created.
- */
-extern JS_PUBLIC_API(JSObject*)
-JS_GetObjectPrototype(JSContext* cx, JS::HandleObject forObj);
-
-/**
- * Returns the original value of |Array.prototype| from the global object in
- * which |forObj| was created.
- */
-extern JS_PUBLIC_API(JSObject*)
-JS_GetArrayPrototype(JSContext* cx, JS::HandleObject forObj);
-
-/**
- * Returns the original value of |Error.prototype| from the global
- * object of the current compartment of cx.
- */
-extern JS_PUBLIC_API(JSObject*)
-JS_GetErrorPrototype(JSContext* cx);
-
-/**
- * Returns the %IteratorPrototype% object that all built-in iterator prototype
- * chains go through for the global object of the current compartment of cx.
- */
-extern JS_PUBLIC_API(JSObject*)
-JS_GetIteratorPrototype(JSContext* cx);
-
 extern JS_PUBLIC_API(JSObject*)
 JS_GetGlobalForObject(JSContext* cx, JSObject* obj);
 
 extern JS_PUBLIC_API(bool)
 JS_IsGlobalObject(JSObject* obj);
 
 extern JS_PUBLIC_API(JSObject*)
 JS_GlobalLexicalEnvironment(JSObject* obj);
diff --git a/js/xpconnect/src/Sandbox.cpp b/js/xpconnect/src/Sandbox.cpp
--- a/js/xpconnect/src/Sandbox.cpp
+++ b/js/xpconnect/src/Sandbox.cpp
@@ -1177,17 +1177,17 @@ xpc::CreateSandboxObject(JSContext* cx, 
 
             // Ensure |Object.prototype| is instantiated before prototype-
             // splicing below.  For write-to-global-prototype behavior, extend
             // this to all builtin properties.
             if (options.writeToGlobalPrototype) {
                 if (!JS_EnumerateStandardClasses(cx, sandbox))
                     return NS_ERROR_XPC_UNEXPECTED;
             } else {
-                if (!JS_GetObjectPrototype(cx, sandbox))
+                if (!JS::GetRealmObjectPrototype(cx))
                     return NS_ERROR_XPC_UNEXPECTED;
             }
         }
 
         if (options.proto) {
             bool ok = JS_WrapObject(cx, &options.proto);
             if (!ok)
                 return NS_ERROR_XPC_UNEXPECTED;
diff --git a/js/xpconnect/src/XPCWrappedNative.cpp b/js/xpconnect/src/XPCWrappedNative.cpp
--- a/js/xpconnect/src/XPCWrappedNative.cpp
+++ b/js/xpconnect/src/XPCWrappedNative.cpp
@@ -703,23 +703,19 @@ XPCWrappedNative::Init(nsIXPCScriptable*
                                !!(jsclazz->flags & JSCLASS_IS_GLOBAL));
 
     MOZ_ASSERT(jsclazz &&
                jsclazz->name &&
                jsclazz->flags &&
                jsclazz->getResolve() &&
                jsclazz->hasFinalize(), "bad class");
 
-    // XXXbz JS_GetObjectPrototype wants an object, even though it then asserts
-    // that this object is same-compartment with cx, which means it could just
-    // use the cx global...
-    RootedObject global(cx, CurrentGlobalOrNull(cx));
     RootedObject protoJSObject(cx, HasProto() ?
                                    GetProto()->GetJSProtoObject() :
-                                   JS_GetObjectPrototype(cx, global));
+                                   JS::GetRealmObjectPrototype(cx));
     if (!protoJSObject) {
         return false;
     }
 
     mFlatJSObject = JS_NewObjectWithGivenProto(cx, jsclazz, protoJSObject);
     if (!mFlatJSObject) {
         mFlatJSObject.unsetFlags(FLAT_JS_OBJECT_VALID);
         return false;
diff --git a/js/xpconnect/src/XPCWrappedNativeProto.cpp b/js/xpconnect/src/XPCWrappedNativeProto.cpp
--- a/js/xpconnect/src/XPCWrappedNativeProto.cpp
+++ b/js/xpconnect/src/XPCWrappedNativeProto.cpp
@@ -56,18 +56,17 @@ XPCWrappedNativeProto::Init(nsIXPCScript
     AutoJSContext cx;
     mScriptable = scriptable;
 
     const js::Class* jsclazz =
         (mScriptable && mScriptable->AllowPropModsToPrototype())
         ? &XPC_WN_ModsAllowed_Proto_JSClass
         : &XPC_WN_NoMods_Proto_JSClass;
 
-    JS::RootedObject global(cx, mScope->GetGlobalJSObject());
-    JS::RootedObject proto(cx, JS_GetObjectPrototype(cx, global));
+    JS::RootedObject proto(cx, JS::GetRealmObjectPrototype(cx));
     mJSProtoObject = JS_NewObjectWithUniqueType(cx, js::Jsvalify(jsclazz),
                                                 proto);
 
     bool success = !!mJSProtoObject;
     if (success) {
         JS_SetPrivate(mJSProtoObject, this);
         if (callPostCreatePrototype)
             success = CallPostCreatePrototype();
