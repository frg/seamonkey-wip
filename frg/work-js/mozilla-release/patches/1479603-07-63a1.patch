# HG changeset patch
# User Matthew Gaudet <mgaudet@mozilla.com>
# Date 1534776002 14400
#      Mon Aug 20 10:40:02 2018 -0400
# Node ID 652d157c6c62c1b24ffd3db5f08d04c3393e2b4c
# Parent  4bdec5f4de8812ff5b5bb2df9473345744744a40
Bug 1479603 - [Part 7] Collapse IonICEntry and BaselineICEntry into ICEntry r=jandem

diff --git a/js/src/jit/BaselineBailouts.cpp b/js/src/jit/BaselineBailouts.cpp
--- a/js/src/jit/BaselineBailouts.cpp
+++ b/js/src/jit/BaselineBailouts.cpp
@@ -1060,32 +1060,32 @@ InitFromBailout(JSContext* cx, size_t fr
         // If the bailout was a resumeAfter, and the opcode is monitored,
         // then the bailed out state should be in a position to enter
         // into the ICTypeMonitor chain for the op.
         bool enterMonitorChain = false;
         if (resumeAfter && (CodeSpec[op].format & JOF_TYPESET)) {
             // Not every monitored op has a monitored fallback stub, e.g.
             // JSOP_NEWOBJECT, which always returns the same type for a
             // particular script/pc location.
-            BaselineICEntry& icEntry = baselineScript->icEntryFromPCOffset(pcOff);
+            ICEntry& icEntry = baselineScript->icEntryFromPCOffset(pcOff);
             ICFallbackStub* fallbackStub = icEntry.firstStub()->getChainFallback();
             if (fallbackStub->isMonitoredFallback())
                 enterMonitorChain = true;
         }
 
         uint32_t numUses = js::StackUses(pc);
 
         if (resumeAfter && !enterMonitorChain)
             pc = GetNextPc(pc);
 
         builder.setResumePC(pc);
         builder.setResumeFramePtr(prevFramePtr);
 
         if (enterMonitorChain) {
-            BaselineICEntry& icEntry = baselineScript->icEntryFromPCOffset(pcOff);
+            ICEntry& icEntry = baselineScript->icEntryFromPCOffset(pcOff);
             ICFallbackStub* fallbackStub = icEntry.firstStub()->getChainFallback();
             MOZ_ASSERT(fallbackStub->isMonitoredFallback());
             JitSpew(JitSpew_BaselineBailouts, "      [TYPE-MONITOR CHAIN]");
 
             ICTypeMonitor_Fallback* typeMonitorFallback =
                 fallbackStub->toMonitoredFallbackStub()->getFallbackMonitorStub(cx, script);
             if (!typeMonitorFallback)
                 return false;
@@ -1242,17 +1242,17 @@ InitFromBailout(JSContext* cx, size_t fr
     size_t baselineFrameDescr = MakeFrameDescriptor((uint32_t) builder.framePushed(),
                                                     JitFrame_BaselineJS,
                                                     BaselineStubFrameLayout::Size());
     if (!builder.writeWord(baselineFrameDescr, "Descriptor"))
         return false;
 
     // Calculate and write out return address.
     // The icEntry in question MUST have an inlinable fallback stub.
-    BaselineICEntry& icEntry = baselineScript->icEntryFromPCOffset(pcOff);
+    ICEntry& icEntry = baselineScript->icEntryFromPCOffset(pcOff);
     MOZ_ASSERT(IsInlinableFallback(icEntry.firstStub()->getChainFallback()));
     if (!builder.writePtr(baselineScript->returnAddressForIC(icEntry), "ReturnAddr"))
         return false;
 
     // Build baseline stub frame:
     // +===============+
     // |    StubPtr    |
     // +---------------+
diff --git a/js/src/jit/BaselineCompiler.cpp b/js/src/jit/BaselineCompiler.cpp
--- a/js/src/jit/BaselineCompiler.cpp
+++ b/js/src/jit/BaselineCompiler.cpp
@@ -242,17 +242,17 @@ BaselineCompiler::compile()
     // If profiler instrumentation is enabled, toggle instrumentation on.
     if (cx->runtime()->jitRuntime()->isProfilerInstrumentationEnabled(cx->runtime()))
         baselineScript->toggleProfilerInstrumentation(true);
 
     // Patch IC loads using IC entries.
     for (size_t i = 0; i < icLoadLabels_.length(); i++) {
         CodeOffset label = icLoadLabels_[i].label;
         size_t icEntry = icLoadLabels_[i].icEntry;
-        BaselineICEntry* entryAddr = &(baselineScript->icEntry(icEntry));
+        ICEntry* entryAddr = &(baselineScript->icEntry(icEntry));
         Assembler::PatchDataWithValueCheck(CodeLocationLabel(code, label),
                                            ImmPtr(entryAddr),
                                            ImmPtr((void*)-1));
     }
 
     if (modifiesArguments_)
         baselineScript->setModifiesArguments();
     if (analysis_.usesEnvironmentChain())
@@ -507,17 +507,17 @@ BaselineCompiler::emitOutOfLinePostBarri
     masm.popValue(R0);
     masm.ret();
     return true;
 }
 
 bool
 BaselineCompiler::emitIC(ICStub* stub, ICEntry::Kind kind)
 {
-    BaselineICEntry* entry = allocateICEntry(stub, kind);
+    ICEntry* entry = allocateICEntry(stub, kind);
     if (!entry)
         return false;
 
     CodeOffset patchOffset;
     EmitCallIC(&patchOffset, masm);
     entry->setReturnOffset(CodeOffset(masm.currentOffset()));
     if (!addICLoadLabel(patchOffset))
         return false;
diff --git a/js/src/jit/BaselineDebugModeOSR.cpp b/js/src/jit/BaselineDebugModeOSR.cpp
--- a/js/src/jit/BaselineDebugModeOSR.cpp
+++ b/js/src/jit/BaselineDebugModeOSR.cpp
@@ -213,17 +213,17 @@ CollectJitStackScripts(JSContext* cx, co
                 // We are in the middle of handling an exception and the frame
                 // must have an override pc.
                 uint32_t offset = script->pcToOffset(baselineFrame->overridePc());
                 if (!entries.append(DebugModeOSREntry(script, offset)))
                     return false;
             } else {
                 // The frame must be settled on a pc with an ICEntry.
                 uint8_t* retAddr = frame.returnAddressToFp();
-                BaselineICEntry& icEntry = script->baselineScript()->icEntryFromReturnAddress(retAddr);
+                ICEntry& icEntry = script->baselineScript()->icEntryFromReturnAddress(retAddr);
                 if (!entries.append(DebugModeOSREntry(script, icEntry)))
                     return false;
             }
 
             if (entries.back().needsRecompileInfo()) {
                 if (!entries.back().allocateRecompileInfo(cx))
                     return false;
 
@@ -493,43 +493,43 @@ PatchBaselineFramesForDebugMode(JSContex
                 //
                 // Patching returns from a VM call. After fixing up the the
                 // continuation for unsynced values (the frame register is
                 // popped by the callVM trampoline), we resume at the
                 // return-from-callVM address. The assumption here is that all
                 // callVMs which can trigger debug mode OSR are the *only*
                 // callVMs generated for their respective pc locations in the
                 // baseline JIT code.
-                BaselineICEntry& callVMEntry = bl->callVMEntryFromPCOffset(pcOffset);
+                ICEntry& callVMEntry = bl->callVMEntryFromPCOffset(pcOffset);
                 recompInfo->resumeAddr = bl->returnAddressForIC(callVMEntry);
                 popFrameReg = false;
                 break;
               }
 
               case ICEntry::Kind_WarmupCounter: {
                 // Case J above.
                 //
                 // Patching mechanism is identical to a CallVM. This is
                 // handled especially only because the warmup counter VM call is
                 // part of the prologue, and not tied an opcode.
-                BaselineICEntry& warmupCountEntry = bl->warmupCountICEntry();
+                ICEntry& warmupCountEntry = bl->warmupCountICEntry();
                 recompInfo->resumeAddr = bl->returnAddressForIC(warmupCountEntry);
                 popFrameReg = false;
                 break;
               }
 
               case ICEntry::Kind_StackCheck:
               case ICEntry::Kind_EarlyStackCheck: {
                 // Case I above.
                 //
                 // Patching mechanism is identical to a CallVM. This is
                 // handled especially only because the stack check VM call is
                 // part of the prologue, and not tied an opcode.
                 bool earlyCheck = kind == ICEntry::Kind_EarlyStackCheck;
-                BaselineICEntry& stackCheckEntry = bl->stackCheckICEntry(earlyCheck);
+                ICEntry& stackCheckEntry = bl->stackCheckICEntry(earlyCheck);
                 recompInfo->resumeAddr = bl->returnAddressForIC(stackCheckEntry);
                 popFrameReg = false;
                 break;
               }
 
               case ICEntry::Kind_DebugTrap:
                 // Case C above.
                 //
diff --git a/js/src/jit/BaselineInspector.h b/js/src/jit/BaselineInspector.h
--- a/js/src/jit/BaselineInspector.h
+++ b/js/src/jit/BaselineInspector.h
@@ -38,17 +38,17 @@ class SetElemICInspector : public ICInsp
     bool sawOOBDenseWrite() const;
     bool sawOOBTypedArrayWrite() const;
 };
 
 class BaselineInspector
 {
   private:
     JSScript* script;
-    BaselineICEntry* prevLookedUpEntry;
+    ICEntry* prevLookedUpEntry;
 
   public:
     explicit BaselineInspector(JSScript* script)
       : script(script), prevLookedUpEntry(nullptr)
     {
         MOZ_ASSERT(script);
     }
 
@@ -62,41 +62,41 @@ class BaselineInspector
 
   private:
 #ifdef DEBUG
     bool isValidPC(jsbytecode* pc) {
         return script->containsPC(pc);
     }
 #endif
 
-    BaselineICEntry& icEntryFromPC(jsbytecode* pc) {
+    ICEntry& icEntryFromPC(jsbytecode* pc) {
         MOZ_ASSERT(hasBaselineScript());
         MOZ_ASSERT(isValidPC(pc));
-        BaselineICEntry& ent =
+        ICEntry& ent =
             baselineScript()->icEntryFromPCOffset(script->pcToOffset(pc), prevLookedUpEntry);
         MOZ_ASSERT(ent.isForOp());
         prevLookedUpEntry = &ent;
         return ent;
     }
 
-    BaselineICEntry* maybeICEntryFromPC(jsbytecode* pc) {
+    ICEntry* maybeICEntryFromPC(jsbytecode* pc) {
         MOZ_ASSERT(hasBaselineScript());
         MOZ_ASSERT(isValidPC(pc));
-        BaselineICEntry* ent =
+        ICEntry* ent =
             baselineScript()->maybeICEntryFromPCOffset(script->pcToOffset(pc), prevLookedUpEntry);
         if (!ent)
             return nullptr;
         MOZ_ASSERT(ent->isForOp());
         prevLookedUpEntry = ent;
         return ent;
     }
 
     template <typename ICInspectorType>
     ICInspectorType makeICInspector(jsbytecode* pc, ICStub::Kind expectedFallbackKind) {
-        BaselineICEntry* ent = nullptr;
+        ICEntry* ent = nullptr;
         if (hasBaselineScript()) {
             ent = &icEntryFromPC(pc);
             MOZ_ASSERT(ent->fallbackStub()->kind() == expectedFallbackKind);
         }
         return ICInspectorType(this, pc, ent);
     }
 
     ICStub* monomorphicStub(jsbytecode* pc);
diff --git a/js/src/jit/BaselineJIT.cpp b/js/src/jit/BaselineJIT.cpp
--- a/js/src/jit/BaselineJIT.cpp
+++ b/js/src/jit/BaselineJIT.cpp
@@ -372,17 +372,17 @@ BaselineScript::New(JSScript* jsscript,
                     size_t icEntries,
                     size_t pcMappingIndexEntries, size_t pcMappingSize,
                     size_t bytecodeTypeMapEntries,
                     size_t yieldEntries,
                     size_t traceLoggerToggleOffsetEntries)
 {
     static const unsigned DataAlignment = sizeof(uintptr_t);
 
-    size_t icEntriesSize = icEntries * sizeof(BaselineICEntry);
+    size_t icEntriesSize = icEntries * sizeof(ICEntry);
     size_t pcMappingIndexEntriesSize = pcMappingIndexEntries * sizeof(PCMappingIndexEntry);
     size_t bytecodeTypeMapSize = bytecodeTypeMapEntries * sizeof(uint32_t);
     size_t yieldEntriesSize = yieldEntries * sizeof(uintptr_t);
     size_t tlEntriesSize = traceLoggerToggleOffsetEntries * sizeof(uint32_t);
 
     size_t paddedICEntriesSize = AlignBytes(icEntriesSize, DataAlignment);
     size_t paddedPCMappingIndexEntriesSize = AlignBytes(pcMappingIndexEntriesSize, DataAlignment);
     size_t paddedPCMappingSize = AlignBytes(pcMappingSize, DataAlignment);
@@ -436,17 +436,17 @@ BaselineScript::New(JSScript* jsscript,
 void
 BaselineScript::trace(JSTracer* trc)
 {
     TraceEdge(trc, &method_, "baseline-method");
     TraceNullableEdge(trc, &templateEnv_, "baseline-template-environment");
 
     // Mark all IC stub codes hanging off the IC stub entries.
     for (size_t i = 0; i < numICEntries(); i++) {
-        BaselineICEntry& ent = icEntry(i);
+        ICEntry& ent = icEntry(i);
         ent.trace(trc);
     }
 }
 
 /* static */
 void
 BaselineScript::writeBarrierPre(Zone* zone, BaselineScript* script)
 {
@@ -531,17 +531,17 @@ BaselineScript::removeDependentWasmImpor
     for (DependentWasmImport& dep : *dependentWasmImports_) {
         if (dep.instance == &instance && dep.importIndex == idx) {
             dependentWasmImports_->erase(&dep);
             break;
         }
     }
 }
 
-BaselineICEntry&
+ICEntry&
 BaselineScript::icEntry(size_t index)
 {
     MOZ_ASSERT(index < numICEntries());
     return icEntryList()[index];
 }
 
 PCMappingIndexEntry&
 BaselineScript::pcMappingIndexEntry(size_t index)
@@ -564,30 +564,30 @@ BaselineScript::pcMappingReader(size_t i
 }
 
 struct ICEntries
 {
     BaselineScript* const baseline_;
 
     explicit ICEntries(BaselineScript* baseline) : baseline_(baseline) {}
 
-    BaselineICEntry& operator[](size_t index) const {
+    ICEntry& operator[](size_t index) const {
         return baseline_->icEntry(index);
     }
 };
 
-BaselineICEntry&
+ICEntry&
 BaselineScript::icEntryFromReturnOffset(CodeOffset returnOffset)
 {
     size_t loc;
 #ifdef DEBUG
     bool found =
 #endif
         BinarySearchIf(ICEntries(this), 0, numICEntries(),
-                       [&returnOffset](BaselineICEntry& entry) {
+                       [&returnOffset](ICEntry& entry) {
                            size_t roffset = returnOffset.offset();
                            size_t entryRoffset = entry.returnOffset().offset();
                            if (roffset < entryRoffset)
                                return -1;
                            if (entryRoffset < roffset)
                                return 1;
                            return 0;
                        },
@@ -598,34 +598,34 @@ BaselineScript::icEntryFromReturnOffset(
     MOZ_ASSERT(icEntry(loc).returnOffset().offset() == returnOffset.offset());
     return icEntry(loc);
 }
 
 static inline bool
 ComputeBinarySearchMid(BaselineScript* baseline, uint32_t pcOffset, size_t* loc)
 {
     return BinarySearchIf(ICEntries(baseline), 0, baseline->numICEntries(),
-                          [pcOffset](BaselineICEntry& entry) {
+                          [pcOffset](ICEntry& entry) {
                               uint32_t entryOffset = entry.pcOffset();
                               if (pcOffset < entryOffset)
                                   return -1;
                               if (entryOffset < pcOffset)
                                   return 1;
                               return 0;
                           },
                           loc);
 }
 
 uint8_t*
-BaselineScript::returnAddressForIC(const BaselineICEntry& ent)
+BaselineScript::returnAddressForIC(const ICEntry& ent)
 {
     return method()->raw() + ent.returnOffset().offset();
 }
 
-BaselineICEntry*
+ICEntry*
 BaselineScript::maybeICEntryFromPCOffset(uint32_t pcOffset)
 {
     // Multiple IC entries can have the same PC offset, but this method only looks for
     // those which have isForOp() set.
     size_t mid;
     if (!ComputeBinarySearchMid(this, pcOffset, &mid))
         return nullptr;
 
@@ -642,55 +642,55 @@ BaselineScript::maybeICEntryFromPCOffset
     }
     for (size_t i = mid+1; i < numICEntries() && icEntry(i).pcOffset() == pcOffset; i++) {
         if (icEntry(i).isForOp())
             return &icEntry(i);
     }
     return nullptr;
 }
 
-BaselineICEntry&
+ICEntry&
 BaselineScript::icEntryFromPCOffset(uint32_t pcOffset)
 {
-    BaselineICEntry* entry = maybeICEntryFromPCOffset(pcOffset);
+    ICEntry* entry = maybeICEntryFromPCOffset(pcOffset);
     MOZ_RELEASE_ASSERT(entry);
     return *entry;
 }
 
-BaselineICEntry*
-BaselineScript::maybeICEntryFromPCOffset(uint32_t pcOffset, BaselineICEntry* prevLookedUpEntry)
+ICEntry*
+BaselineScript::maybeICEntryFromPCOffset(uint32_t pcOffset, ICEntry* prevLookedUpEntry)
 {
     // Do a linear forward search from the last queried PC offset, or fallback to a
     // binary search if the last offset is too far away.
     if (prevLookedUpEntry && pcOffset >= prevLookedUpEntry->pcOffset() &&
         (pcOffset - prevLookedUpEntry->pcOffset()) <= 10)
     {
-        BaselineICEntry* firstEntry = &icEntry(0);
-        BaselineICEntry* lastEntry = &icEntry(numICEntries() - 1);
-        BaselineICEntry* curEntry = prevLookedUpEntry;
+        ICEntry* firstEntry = &icEntry(0);
+        ICEntry* lastEntry = &icEntry(numICEntries() - 1);
+        ICEntry* curEntry = prevLookedUpEntry;
         while (curEntry >= firstEntry && curEntry <= lastEntry) {
             if (curEntry->pcOffset() == pcOffset && curEntry->isForOp())
                 return curEntry;
             curEntry++;
         }
         return nullptr;
     }
 
     return maybeICEntryFromPCOffset(pcOffset);
 }
 
-BaselineICEntry&
-BaselineScript::icEntryFromPCOffset(uint32_t pcOffset, BaselineICEntry* prevLookedUpEntry)
+ICEntry&
+BaselineScript::icEntryFromPCOffset(uint32_t pcOffset, ICEntry* prevLookedUpEntry)
 {
-    BaselineICEntry* entry = maybeICEntryFromPCOffset(pcOffset, prevLookedUpEntry);
+    ICEntry* entry = maybeICEntryFromPCOffset(pcOffset, prevLookedUpEntry);
     MOZ_RELEASE_ASSERT(entry);
     return *entry;
 }
 
-BaselineICEntry&
+ICEntry&
 BaselineScript::callVMEntryFromPCOffset(uint32_t pcOffset)
 {
     // Like icEntryFromPCOffset, but only looks for the fake ICEntries
     // inserted by VM calls.
     size_t mid;
     MOZ_ALWAYS_TRUE(ComputeBinarySearchMid(this, pcOffset, &mid));
     MOZ_ASSERT(mid < numICEntries());
 
@@ -702,44 +702,44 @@ BaselineScript::callVMEntryFromPCOffset(
     }
     for (size_t i = mid+1; i < numICEntries() && icEntry(i).pcOffset() == pcOffset; i++) {
         if (icEntry(i).kind() == ICEntry::Kind_CallVM)
             return icEntry(i);
     }
     MOZ_CRASH("Invalid PC offset for callVM entry.");
 }
 
-BaselineICEntry&
+ICEntry&
 BaselineScript::stackCheckICEntry(bool earlyCheck)
 {
     // The stack check will always be at offset 0, so just do a linear search
     // from the beginning. This is only needed for debug mode OSR, when
     // patching a frame that has invoked a Debugger hook via the interrupt
     // handler via the stack check, which is part of the prologue.
     ICEntry::Kind kind = earlyCheck ? ICEntry::Kind_EarlyStackCheck : ICEntry::Kind_StackCheck;
     for (size_t i = 0; i < numICEntries() && icEntry(i).pcOffset() == 0; i++) {
         if (icEntry(i).kind() == kind)
             return icEntry(i);
     }
     MOZ_CRASH("No stack check ICEntry found.");
 }
 
-BaselineICEntry&
+ICEntry&
 BaselineScript::warmupCountICEntry()
 {
     // The stack check will be at a very low offset, so just do a linear search
     // from the beginning.
     for (size_t i = 0; i < numICEntries() && icEntry(i).pcOffset() == 0; i++) {
         if (icEntry(i).kind() == ICEntry::Kind_WarmupCounter)
             return icEntry(i);
     }
     MOZ_CRASH("No warmup count ICEntry found.");
 }
 
-BaselineICEntry&
+ICEntry&
 BaselineScript::icEntryFromReturnAddress(uint8_t* returnAddr)
 {
     MOZ_ASSERT(returnAddr > method_->raw());
     MOZ_ASSERT(returnAddr < method_->raw() + method_->instructionsSize());
     CodeOffset offset(returnAddr - method_->raw());
     return icEntryFromReturnOffset(offset);
 }
 
@@ -750,22 +750,22 @@ BaselineScript::copyYieldAndAwaitEntries
 
     for (size_t i = 0; i < yieldAndAwaitOffsets.length(); i++) {
         uint32_t offset = yieldAndAwaitOffsets[i];
         entries[i] = nativeCodeForPC(script, script->offsetToPC(offset));
     }
 }
 
 void
-BaselineScript::copyICEntries(JSScript* script, const BaselineICEntry* entries)
+BaselineScript::copyICEntries(JSScript* script, const ICEntry* entries)
 {
     // Fix up the return offset in the IC entries and copy them in.
     // Also write out the IC entry ptrs in any fallback stubs that were added.
     for (uint32_t i = 0; i < numICEntries(); i++) {
-        BaselineICEntry& realEntry = icEntry(i);
+        ICEntry& realEntry = icEntry(i);
         realEntry = entries[i];
 
         if (!realEntry.hasStub()) {
             // VM call without any stubs.
             continue;
         }
 
         // If the attached stub is a fallback stub, then fix it up with
@@ -1051,17 +1051,17 @@ BaselineScript::toggleProfilerInstrument
 }
 
 void
 BaselineScript::purgeOptimizedStubs(Zone* zone)
 {
     JitSpew(JitSpew_BaselineIC, "Purging optimized stubs");
 
     for (size_t i = 0; i < numICEntries(); i++) {
-        BaselineICEntry& entry = icEntry(i);
+        ICEntry& entry = icEntry(i);
         if (!entry.hasStub())
             continue;
 
         ICStub* lastStub = entry.firstStub();
         while (lastStub->next())
             lastStub = lastStub->next();
 
         if (lastStub->isFallback()) {
@@ -1093,17 +1093,17 @@ BaselineScript::purgeOptimizedStubs(Zone
         } else {
             MOZ_ASSERT(lastStub->isTableSwitch());
         }
     }
 
 #ifdef DEBUG
     // All remaining stubs must be allocated in the fallback space.
     for (size_t i = 0; i < numICEntries(); i++) {
-        BaselineICEntry& entry = icEntry(i);
+        ICEntry& entry = icEntry(i);
         if (!entry.hasStub())
             continue;
 
         ICStub* stub = entry.firstStub();
         while (stub->next()) {
             MOZ_ASSERT(stub->allocatedInFallbackSpace());
             stub = stub->next();
         }
diff --git a/js/src/jit/BaselineJIT.h b/js/src/jit/BaselineJIT.h
--- a/js/src/jit/BaselineJIT.h
+++ b/js/src/jit/BaselineJIT.h
@@ -17,17 +17,17 @@
 #include "vm/JSContext.h"
 #include "vm/Realm.h"
 #include "vm/TraceLogging.h"
 
 namespace js {
 namespace jit {
 
 class StackValue;
-class BaselineICEntry;
+class ICEntry;
 class ICStub;
 class ControlFlowGraph;
 
 class PCMappingSlotInfo
 {
     uint8_t slotInfo_;
 
   public:
@@ -350,18 +350,18 @@ struct BaselineScript
 
     uint32_t postDebugPrologueOffset() const {
         return postDebugPrologueOffset_;
     }
     uint8_t* postDebugPrologueAddr() const {
         return method_->raw() + postDebugPrologueOffset_;
     }
 
-    BaselineICEntry* icEntryList() {
-        return (BaselineICEntry*)(reinterpret_cast<uint8_t*>(this) + icEntriesOffset_);
+    ICEntry* icEntryList() {
+        return (ICEntry*)(reinterpret_cast<uint8_t*>(this) + icEntriesOffset_);
     }
     uint8_t** yieldEntryList() {
         return (uint8_t**)(reinterpret_cast<uint8_t*>(this) + yieldEntriesOffset_);
     }
     PCMappingIndexEntry* pcMappingIndexEntryList() {
         return (PCMappingIndexEntry*)(reinterpret_cast<uint8_t*>(this) + pcMappingIndexOffset_);
     }
     uint8_t* pcMappingData() {
@@ -386,35 +386,35 @@ struct BaselineScript
         MOZ_ASSERT(!templateEnv_);
         templateEnv_ = templateEnv;
     }
 
     bool containsCodeAddress(uint8_t* addr) const {
         return method()->raw() <= addr && addr <= method()->raw() + method()->instructionsSize();
     }
 
-    BaselineICEntry* maybeICEntryFromPCOffset(uint32_t pcOffset);
-    BaselineICEntry* maybeICEntryFromPCOffset(uint32_t pcOffset,
-                                              BaselineICEntry* prevLookedUpEntry);
+    ICEntry* maybeICEntryFromPCOffset(uint32_t pcOffset);
+    ICEntry* maybeICEntryFromPCOffset(uint32_t pcOffset,
+                                              ICEntry* prevLookedUpEntry);
 
-    BaselineICEntry& icEntry(size_t index);
-    BaselineICEntry& icEntryFromReturnOffset(CodeOffset returnOffset);
-    BaselineICEntry& icEntryFromPCOffset(uint32_t pcOffset);
-    BaselineICEntry& icEntryFromPCOffset(uint32_t pcOffset, BaselineICEntry* prevLookedUpEntry);
-    BaselineICEntry& callVMEntryFromPCOffset(uint32_t pcOffset);
-    BaselineICEntry& stackCheckICEntry(bool earlyCheck);
-    BaselineICEntry& warmupCountICEntry();
-    BaselineICEntry& icEntryFromReturnAddress(uint8_t* returnAddr);
-    uint8_t* returnAddressForIC(const BaselineICEntry& ent);
+    ICEntry& icEntry(size_t index);
+    ICEntry& icEntryFromReturnOffset(CodeOffset returnOffset);
+    ICEntry& icEntryFromPCOffset(uint32_t pcOffset);
+    ICEntry& icEntryFromPCOffset(uint32_t pcOffset, ICEntry* prevLookedUpEntry);
+    ICEntry& callVMEntryFromPCOffset(uint32_t pcOffset);
+    ICEntry& stackCheckICEntry(bool earlyCheck);
+    ICEntry& warmupCountICEntry();
+    ICEntry& icEntryFromReturnAddress(uint8_t* returnAddr);
+    uint8_t* returnAddressForIC(const ICEntry& ent);
 
     size_t numICEntries() const {
         return icEntries_;
     }
 
-    void copyICEntries(JSScript* script, const BaselineICEntry* entries);
+    void copyICEntries(JSScript* script, const ICEntry* entries);
     void adoptFallbackStubs(FallbackICStubSpace* stubSpace);
 
     void copyYieldAndAwaitEntries(JSScript* script, Vector<uint32_t>& yieldAndAwaitOffsets);
 
     PCMappingIndexEntry& pcMappingIndexEntry(size_t index);
     CompactBufferReader pcMappingReader(size_t indexEntry);
 
     size_t numPCMappingIndexEntries() const {
diff --git a/js/src/jit/IonCode.h b/js/src/jit/IonCode.h
--- a/js/src/jit/IonCode.h
+++ b/js/src/jit/IonCode.h
@@ -21,17 +21,16 @@
 #include "vm/TraceLogging.h"
 #include "vm/TypeInference.h"
 
 namespace js {
 namespace jit {
 
 class MacroAssembler;
 class IonBuilder;
-class IonICEntry;
 class JitCode;
 
 typedef Vector<JSObject*, 4, JitAllocPolicy> ObjectVector;
 typedef Vector<TraceLoggerEvent, 0, SystemAllocPolicy> TraceLoggerEventVector;
 
 // Header at start of raw code buffer
 struct JitCodeHeader
 {
diff --git a/js/src/jit/SharedIC.cpp b/js/src/jit/SharedIC.cpp
--- a/js/src/jit/SharedIC.cpp
+++ b/js/src/jit/SharedIC.cpp
@@ -90,30 +90,17 @@ TypeFallbackICSpew(JSContext* cx, ICType
 
 ICFallbackStub*
 ICEntry::fallbackStub() const
 {
     return firstStub()->getChainFallback();
 }
 
 void
-IonICEntry::trace(JSTracer* trc)
-{
-    TraceManuallyBarrieredEdge(trc, &script_, "IonICEntry::script_");
-    traceEntry(trc);
-}
-
-void
-BaselineICEntry::trace(JSTracer* trc)
-{
-    traceEntry(trc);
-}
-
-void
-ICEntry::traceEntry(JSTracer* trc)
+ICEntry::trace(JSTracer* trc)
 {
     if (!hasStub())
         return;
     for (ICStub* stub = firstStub(); stub; stub = stub->next())
         stub->trace(trc);
 }
 
 ICStubConstIterator&
diff --git a/js/src/jit/SharedIC.h b/js/src/jit/SharedIC.h
--- a/js/src/jit/SharedIC.h
+++ b/js/src/jit/SharedIC.h
@@ -333,44 +333,16 @@ class ICEntry
     static inline size_t offsetOfFirstStub() {
         return offsetof(ICEntry, firstStub_);
     }
 
     inline ICStub** addressOfFirstStub() {
         return &firstStub_;
     }
 
-  protected:
-    void traceEntry(JSTracer* trc);
-};
-
-class BaselineICEntry : public ICEntry
-{
-  public:
-    BaselineICEntry(uint32_t pcOffset, Kind kind)
-      : ICEntry(pcOffset, kind)
-    { }
-
-    void trace(JSTracer* trc);
-};
-
-class IonICEntry : public ICEntry
-{
-    JSScript* script_;
-
-  public:
-    IonICEntry(uint32_t pcOffset, Kind kind, JSScript* script)
-      : ICEntry(pcOffset, kind),
-        script_(script)
-    { }
-
-    JSScript* script() {
-        return script_;
-    }
-
     void trace(JSTracer* trc);
 };
 
 class ICMonitoredStub;
 class ICMonitoredFallbackStub;
 class ICUpdatedStub;
 
 // Constant iterator that traverses arbitrary chains of ICStubs.
diff --git a/js/src/jit/shared/BaselineCompiler-shared.h b/js/src/jit/shared/BaselineCompiler-shared.h
--- a/js/src/jit/shared/BaselineCompiler-shared.h
+++ b/js/src/jit/shared/BaselineCompiler-shared.h
@@ -25,17 +25,17 @@ class BaselineCompilerShared
     bool ionCompileable_;
     bool compileDebugInstrumentation_;
 
     TempAllocator& alloc_;
     BytecodeAnalysis analysis_;
     FrameInfo frame;
 
     FallbackICStubSpace stubSpace_;
-    js::Vector<BaselineICEntry, 16, SystemAllocPolicy> icEntries_;
+    js::Vector<ICEntry, 16, SystemAllocPolicy> icEntries_;
 
     // Stores the native code offset for a bytecode pc.
     struct PCMappingEntry
     {
         uint32_t pcOffset;
         uint32_t nativeOffset;
         PCMappingSlotInfo slotInfo;
 
@@ -65,37 +65,37 @@ class BaselineCompilerShared
     CodeOffset profilerEnterFrameToggleOffset_;
     CodeOffset profilerExitFrameToggleOffset_;
 
     Vector<CodeOffset> traceLoggerToggleOffsets_;
     CodeOffset traceLoggerScriptTextIdOffset_;
 
     BaselineCompilerShared(JSContext* cx, TempAllocator& alloc, JSScript* script);
 
-    BaselineICEntry* allocateICEntry(ICStub* stub, ICEntry::Kind kind) {
+    ICEntry* allocateICEntry(ICStub* stub, ICEntry::Kind kind) {
         if (!stub)
             return nullptr;
 
         // Create the entry and add it to the vector.
-        if (!icEntries_.append(BaselineICEntry(script->pcToOffset(pc), kind))) {
+        if (!icEntries_.append(ICEntry(script->pcToOffset(pc), kind))) {
             ReportOutOfMemory(cx);
             return nullptr;
         }
-        BaselineICEntry& vecEntry = icEntries_.back();
+        ICEntry& vecEntry = icEntries_.back();
 
         // Set the first stub for the IC entry to the fallback stub
         vecEntry.setFirstStub(stub);
 
         // Return pointer to the IC entry
         return &vecEntry;
     }
 
     // Append an ICEntry without a stub.
     bool appendICEntry(ICEntry::Kind kind, uint32_t returnOffset) {
-        BaselineICEntry entry(script->pcToOffset(pc), kind);
+        ICEntry entry(script->pcToOffset(pc), kind);
         entry.setReturnOffset(CodeOffset(returnOffset));
         if (!icEntries_.append(entry)) {
             ReportOutOfMemory(cx);
             return false;
         }
         return true;
     }
 
