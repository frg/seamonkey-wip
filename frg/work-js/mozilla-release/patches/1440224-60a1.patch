# HG changeset patch
# User Dragan Mladjenovic <dragan.mladjenovic>
# Date 1519235079 -3600
#      Wed Feb 21 18:44:39 2018 +0100
# Node ID 7c8ebeb71a5f467b3c2d11aca5f61c36a9c180d4
# Parent  227ad4438ccfd8b9738f65e34602f3faf19529c6
Bug 1440224 : [MIPS] Preserve FCSR state during wasm interrupt ; r=lth

diff --git a/js/src/wasm/WasmStubs.cpp b/js/src/wasm/WasmStubs.cpp
--- a/js/src/wasm/WasmStubs.cpp
+++ b/js/src/wasm/WasmStubs.cpp
@@ -1422,16 +1422,24 @@ GenerateUnalignedExit(MacroAssembler& ma
 }
 
 #if defined(JS_CODEGEN_ARM)
 static const LiveRegisterSet AllRegsExceptPCSP(
     GeneralRegisterSet(Registers::AllMask & ~((uint32_t(1) << Registers::sp) |
                                               (uint32_t(1) << Registers::pc))),
     FloatRegisterSet(FloatRegisters::AllDoubleMask));
 static_assert(!SupportsSimd, "high lanes of SIMD registers need to be saved too.");
+#elif defined(JS_CODEGEN_MIPS32) || defined(JS_CODEGEN_MIPS64)
+static const LiveRegisterSet AllUserRegsExceptSP(
+    GeneralRegisterSet(Registers::AllMask & ~((uint32_t(1) << Registers::k0) |
+                                              (uint32_t(1) << Registers::k1) |
+                                              (uint32_t(1) << Registers::sp) |
+                                              (uint32_t(1) << Registers::zero))),
+    FloatRegisterSet(FloatRegisters::AllDoubleMask));
+static_assert(!SupportsSimd, "high lanes of SIMD registers need to be saved too.");
 #else
 static const LiveRegisterSet AllRegsExceptSP(
     GeneralRegisterSet(Registers::AllMask & ~(uint32_t(1) << Registers::StackPointer)),
     FloatRegisterSet(FloatRegisters::AllMask));
 #endif
 
 // The async interrupt-callback exit is called from arbitrarily-interrupted wasm
 // code. It calls into the WasmHandleExecutionInterrupt to determine whether we must
@@ -1482,51 +1490,52 @@ GenerateInterruptExit(MacroAssembler& ma
     masm.PopFlags();
 
     // Return to the resumePC stored into this stack slot above.
     MOZ_ASSERT(masm.framePushed() == 0);
     masm.ret();
 #elif defined(JS_CODEGEN_MIPS32) || defined(JS_CODEGEN_MIPS64)
     // Reserve space to store resumePC and HeapReg.
     masm.subFromStackPtr(Imm32(2 * sizeof(intptr_t)));
-    // set to zero so we can use masm.framePushed() below.
+    // Set to zero so we can use masm.framePushed() below.
     masm.setFramePushed(0);
-    static_assert(!SupportsSimd, "high lanes of SIMD registers need to be saved too.");
-    // save all registers,except sp. After this stack is alligned.
-    masm.PushRegsInMask(AllRegsExceptSP);
+
+    // Save all registers, except sp.
+    masm.PushRegsInMask(AllUserRegsExceptSP);
 
-    // Save the stack pointer in a non-volatile register.
+    // Save the stack pointer and FCSR in a non-volatile registers.
     masm.moveStackPtrTo(s0);
+    masm.as_cfc1(s1, Assembler::FCSR);
+
     // Align the stack.
     masm.ma_and(StackPointer, StackPointer, Imm32(~(ABIStackAlignment - 1)));
 
     // Store HeapReg into the reserved space.
     masm.storePtr(HeapReg, Address(s0, masm.framePushed() + sizeof(intptr_t)));
 
 # ifdef USES_O32_ABI
     // MIPS ABI requires rewserving stack for registes $a0 to $a3.
     masm.subFromStackPtr(Imm32(4 * sizeof(intptr_t)));
 # endif
 
     masm.assertStackAlignment(ABIStackAlignment);
     masm.call(SymbolicAddress::HandleExecutionInterrupt);
 
-# ifdef USES_O32_ABI
-    masm.addToStackPtr(Imm32(4 * sizeof(intptr_t)));
-# endif
-
     masm.branchTestPtr(Assembler::Zero, ReturnReg, ReturnReg, throwLabel);
 
     // This will restore stack to the address before the call.
     masm.moveToStackPtr(s0);
 
+    // Restore FCSR.
+    masm.as_ctc1(s1, Assembler::FCSR);
+
     // Store resumePC into the reserved space.
     masm.storePtr(ReturnReg, Address(s0, masm.framePushed()));
 
-    masm.PopRegsInMask(AllRegsExceptSP);
+    masm.PopRegsInMask(AllUserRegsExceptSP);
 
     // Pop resumePC into PC. Clobber HeapReg to make the jump and restore it
     // during jump delay slot.
     masm.loadPtr(Address(StackPointer, 0), HeapReg);
     // Reclaim the reserve space.
     masm.addToStackPtr(Imm32(2 * sizeof(intptr_t)));
     masm.as_jr(HeapReg);
     masm.loadPtr(Address(StackPointer, -int32_t(sizeof(intptr_t))), HeapReg);
