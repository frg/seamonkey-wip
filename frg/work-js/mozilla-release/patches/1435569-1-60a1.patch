# HG changeset patch
# User Matthew Gaudet <mgaudet@mozilla.com>
# Date 1518564500 18000
# Node ID 114d2a3202c0ddf706461952d63a14d35611f734
# Parent  763f3eabebc9aa5f02f811e7b003529ad9280052
Bug 1435569: Change CacheIRSpewer to allow less redundency in spewing. r=tcampbell

This patch adds an inner class to CacheIRSpewer that manages access to the
CacheIRSpewer and dramatically simplifies the consuming code.

Note: this changes the CacheIRSpewer to no longer use LockGuard; instead
the raw mutex is managed by CacheIRSpewer. This is because the RAII nature
of CacheIRSpewer::Guard prevents constructing the LockGuard if-and-only-if
the spewer is enabled.

diff --git a/js/src/jit/BaselineIC.cpp b/js/src/jit/BaselineIC.cpp
--- a/js/src/jit/BaselineIC.cpp
+++ b/js/src/jit/BaselineIC.cpp
@@ -913,17 +913,17 @@ DoSetElemFallback(JSContext* cx, Baselin
                 else if (gen.shouldUnlinkPreliminaryObjectStubs())
                     StripPreliminaryObjectStubs(cx, stub);
 
                 JitSpew(JitSpew_BaselineIC, "  Attached CacheIR stub");
                 SetUpdateStubData(newStub->toCacheIR_Updated(), gen.typeCheckInfo());
                 return true;
             }
         } else {
-            gen.trackNotAttached();
+            gen.trackAttached(nullptr);
         }
         if (!attached && !isTemporarilyUnoptimizable)
             stub->state().trackNotAttached();
     }
 
     return true;
 }
 
@@ -1579,17 +1579,17 @@ DoSetPropFallback(JSContext* cx, Baselin
                     newStub->toCacheIR_Updated()->notePreliminaryObject();
                 else if (gen.shouldUnlinkPreliminaryObjectStubs())
                     StripPreliminaryObjectStubs(cx, stub);
 
                 JitSpew(JitSpew_BaselineIC, "  Attached CacheIR stub");
                 SetUpdateStubData(newStub->toCacheIR_Updated(), gen.typeCheckInfo());
             }
         } else {
-            gen.trackNotAttached();
+            gen.trackAttached(nullptr);
         }
         if (!attached && !isTemporarilyUnoptimizable)
             stub->state().trackNotAttached();
     }
 
     if (!attached && !isTemporarilyUnoptimizable)
         stub->noteUnoptimizableAccess();
 
diff --git a/js/src/jit/CacheIR.cpp b/js/src/jit/CacheIR.cpp
--- a/js/src/jit/CacheIR.cpp
+++ b/js/src/jit/CacheIR.cpp
@@ -211,17 +211,17 @@ GetPropIRGenerator::tryAttachStub()
                 return true;
             if (tryAttachXrayCrossCompartmentWrapper(obj, objId, id))
                 return true;
             if (tryAttachFunction(obj, objId, id))
                 return true;
             if (tryAttachProxy(obj, objId, id))
                 return true;
 
-            trackNotAttached();
+            trackAttached(nullptr);
             return false;
         }
 
         MOZ_ASSERT(cacheKind_ == CacheKind::GetElem || cacheKind_ == CacheKind::GetElemSuper);
 
         if (tryAttachProxyElement(obj, objId))
             return true;
 
@@ -232,48 +232,48 @@ GetPropIRGenerator::tryAttachStub()
                 return true;
             if (tryAttachDenseElement(obj, objId, index, indexId))
                 return true;
             if (tryAttachDenseElementHole(obj, objId, index, indexId))
                 return true;
             if (tryAttachArgumentsObjectArg(obj, objId, index, indexId))
                 return true;
 
-            trackNotAttached();
+            trackAttached(nullptr);
             return false;
         }
 
-        trackNotAttached();
+        trackAttached(nullptr);
         return false;
     }
 
     if (nameOrSymbol) {
         if (tryAttachPrimitive(valId, id))
             return true;
         if (tryAttachStringLength(valId, id))
             return true;
         if (tryAttachMagicArgumentsName(valId, id))
             return true;
 
-        trackNotAttached();
+        trackAttached(nullptr);
         return false;
     }
 
     if (idVal_.isInt32()) {
         ValOperandId indexId = getElemKeyValueId();
         if (tryAttachStringChar(valId, indexId))
             return true;
         if (tryAttachMagicArgument(valId, indexId))
             return true;
 
-        trackNotAttached();
+        trackAttached(nullptr);
         return false;
     }
 
-    trackNotAttached();
+    trackAttached(nullptr);
     return false;
 }
 
 bool
 GetPropIRGenerator::tryAttachIdempotentStub()
 {
     // For idempotent ICs, only attach stubs which we can be sure have no side
     // effects and produce a result which the MIR in the calling code is able
@@ -2116,39 +2116,19 @@ GetPropIRGenerator::tryAttachProxyElemen
     trackAttached("ProxyElement");
     return true;
 }
 
 void
 GetPropIRGenerator::trackAttached(const char* name)
 {
 #ifdef JS_CACHEIR_SPEW
-    CacheIRSpewer& sp = CacheIRSpewer::singleton();
-    if (sp.enabled()) {
-        LockGuard<Mutex> guard(sp.lock());
-        sp.beginCache(guard, *this);
-        sp.valueProperty(guard, "base", val_);
-        sp.valueProperty(guard, "property", idVal_);
-        sp.attached(guard, name);
-        sp.endCache(guard);
-    }
-#endif
-}
-
-void
-GetPropIRGenerator::trackNotAttached()
-{
-#ifdef JS_CACHEIR_SPEW
-    CacheIRSpewer& sp = CacheIRSpewer::singleton();
-    if (sp.enabled()) {
-        LockGuard<Mutex> guard(sp.lock());
-        sp.beginCache(guard, *this);
-        sp.valueProperty(guard, "base", val_);
-        sp.valueProperty(guard, "property", idVal_);
-        sp.endCache(guard);
+    if (const CacheIRSpewer::Guard& sp = CacheIRSpewer::Guard(*this, name)) {
+        sp.valueProperty("base", val_);
+        sp.valueProperty("property", idVal_);
     }
 #endif
 }
 
 void
 IRGenerator::emitIdGuard(ValOperandId valId, jsid id)
 {
     if (JSID_IS_SYMBOL(id)) {
@@ -2207,17 +2187,17 @@ GetNameIRGenerator::tryAttachStub()
 
     if (tryAttachGlobalNameValue(envId, id))
         return true;
     if (tryAttachGlobalNameGetter(envId, id))
         return true;
     if (tryAttachEnvironmentName(envId, id))
         return true;
 
-    trackNotAttached();
+    trackAttached(nullptr);
     return false;
 }
 
 bool
 CanAttachGlobalName(JSContext* cx, Handle<LexicalEnvironmentObject*> globalLexical, HandleId id,
                     MutableHandleNativeObject holder, MutableHandleShape shape)
 {
     // The property must be found, and it must be found as a normal data property.
@@ -2431,39 +2411,19 @@ GetNameIRGenerator::tryAttachEnvironment
     trackAttached("EnvironmentName");
     return true;
 }
 
 void
 GetNameIRGenerator::trackAttached(const char* name)
 {
 #ifdef JS_CACHEIR_SPEW
-    CacheIRSpewer& sp = CacheIRSpewer::singleton();
-    if (sp.enabled()) {
-        LockGuard<Mutex> guard(sp.lock());
-        sp.beginCache(guard, *this);
-        sp.valueProperty(guard, "base", ObjectValue(*env_));
-        sp.valueProperty(guard, "property", StringValue(name_));
-        sp.attached(guard, name);
-        sp.endCache(guard);
-    }
-#endif
-}
-
-void
-GetNameIRGenerator::trackNotAttached()
-{
-#ifdef JS_CACHEIR_SPEW
-    CacheIRSpewer& sp = CacheIRSpewer::singleton();
-    if (sp.enabled()) {
-        LockGuard<Mutex> guard(sp.lock());
-        sp.beginCache(guard, *this);
-        sp.valueProperty(guard, "base", ObjectValue(*env_));
-        sp.valueProperty(guard, "property", StringValue(name_));
-        sp.endCache(guard);
+    if (const CacheIRSpewer::Guard& sp = CacheIRSpewer::Guard(*this, name)) {
+        sp.valueProperty("base", ObjectValue(*env_));
+        sp.valueProperty("property", StringValue(name_));
     }
 #endif
 }
 
 BindNameIRGenerator::BindNameIRGenerator(JSContext* cx, HandleScript script, jsbytecode* pc,
                                          ICState::Mode mode, HandleObject env,
                                          HandlePropertyName name)
   : IRGenerator(cx, script, pc, CacheKind::BindName, mode),
@@ -2481,17 +2441,17 @@ BindNameIRGenerator::tryAttachStub()
     ObjOperandId envId(writer.setInputOperandId(0));
     RootedId id(cx_, NameToId(name_));
 
     if (tryAttachGlobalName(envId, id))
         return true;
     if (tryAttachEnvironmentName(envId, id))
         return true;
 
-    trackNotAttached();
+    trackAttached(nullptr);
     return false;
 }
 
 bool
 BindNameIRGenerator::tryAttachGlobalName(ObjOperandId objId, HandleId id)
 {
     if (!IsGlobalOp(JSOp(*pc_)) || script_->hasNonSyntacticScope())
         return false;
@@ -2589,39 +2549,19 @@ BindNameIRGenerator::tryAttachEnvironmen
     trackAttached("EnvironmentName");
     return true;
 }
 
 void
 BindNameIRGenerator::trackAttached(const char* name)
 {
 #ifdef JS_CACHEIR_SPEW
-    CacheIRSpewer& sp = CacheIRSpewer::singleton();
-    if (sp.enabled()) {
-        LockGuard<Mutex> guard(sp.lock());
-        sp.beginCache(guard, *this);
-        sp.valueProperty(guard, "base", ObjectValue(*env_));
-        sp.valueProperty(guard, "property", StringValue(name_));
-        sp.attached(guard, name);
-        sp.endCache(guard);
-    }
-#endif
-}
-
-void
-BindNameIRGenerator::trackNotAttached()
-{
-#ifdef JS_CACHEIR_SPEW
-    CacheIRSpewer& sp = CacheIRSpewer::singleton();
-    if (sp.enabled()) {
-        LockGuard<Mutex> guard(sp.lock());
-        sp.beginCache(guard, *this);
-        sp.valueProperty(guard, "base", ObjectValue(*env_));
-        sp.valueProperty(guard, "property", StringValue(name_));
-        sp.endCache(guard);
+    if (const CacheIRSpewer::Guard& sp = CacheIRSpewer::Guard(*this, name)) {
+        sp.valueProperty("base", ObjectValue(*env_));
+        sp.valueProperty("property", StringValue(name_));
     }
 #endif
 }
 
 HasPropIRGenerator::HasPropIRGenerator(JSContext* cx, HandleScript script, jsbytecode* pc,
                                        CacheKind cacheKind, ICState::Mode mode,
                                        HandleValue idVal, HandleValue val)
   : IRGenerator(cx, script, pc, cacheKind, mode),
@@ -2947,17 +2887,17 @@ HasPropIRGenerator::tryAttachStub()
 
     AutoAssertNoPendingException aanpe(cx_);
 
     // NOTE: Argument order is PROPERTY, OBJECT
     ValOperandId keyId(writer.setInputOperandId(0));
     ValOperandId valId(writer.setInputOperandId(1));
 
     if (!val_.isObject()) {
-        trackNotAttached();
+        trackAttached(nullptr);
         return false;
     }
     RootedObject obj(cx_, &val_.toObject());
     ObjOperandId objId = writer.guardIsObject(valId);
 
     // Optimize Proxies
     if (tryAttachProxyElement(obj, objId, keyId))
         return true;
@@ -2970,67 +2910,47 @@ HasPropIRGenerator::tryAttachStub()
     }
 
     if (nameOrSymbol) {
         if (tryAttachNamedProp(obj, objId, id, keyId))
             return true;
         if (tryAttachDoesNotExist(obj, objId, id, keyId))
             return true;
 
-        trackNotAttached();
+        trackAttached(nullptr);
         return false;
     }
 
     uint32_t index;
     Int32OperandId indexId;
     if (maybeGuardInt32Index(idVal_, keyId, &index, &indexId)) {
         if (tryAttachDense(obj, objId, index, indexId))
             return true;
         if (tryAttachDenseHole(obj, objId, index, indexId))
             return true;
         if (tryAttachTypedArray(obj, objId, index, indexId))
             return true;
         if (tryAttachSparse(obj, objId, index, indexId))
             return true;
 
-        trackNotAttached();
+        trackAttached(nullptr);
         return false;
     }
 
-    trackNotAttached();
+    trackAttached(nullptr);
     return false;
 }
 
 void
 HasPropIRGenerator::trackAttached(const char* name)
 {
 #ifdef JS_CACHEIR_SPEW
-    CacheIRSpewer& sp = CacheIRSpewer::singleton();
-    if (sp.enabled()) {
-        LockGuard<Mutex> guard(sp.lock());
-        sp.beginCache(guard, *this);
-        sp.valueProperty(guard, "base", val_);
-        sp.valueProperty(guard, "property", idVal_);
-        sp.attached(guard, name);
-        sp.endCache(guard);
-    }
-#endif
-}
-
-void
-HasPropIRGenerator::trackNotAttached()
-{
-#ifdef JS_CACHEIR_SPEW
-    CacheIRSpewer& sp = CacheIRSpewer::singleton();
-    if (sp.enabled()) {
-        LockGuard<Mutex> guard(sp.lock());
-        sp.beginCache(guard, *this);
-        sp.valueProperty(guard, "base", val_);
-        sp.valueProperty(guard, "property", idVal_);
-        sp.endCache(guard);
+    if (const CacheIRSpewer::Guard& sp = CacheIRSpewer::Guard(*this, name)) {
+        sp.valueProperty("base", val_);
+        sp.valueProperty("property", idVal_);
     }
 #endif
 }
 
 bool
 IRGenerator::maybeGuardInt32Index(const Value& index, ValOperandId indexId,
                                   uint32_t* int32Index, Int32OperandId* int32IndexId)
 {
@@ -3384,41 +3304,20 @@ SetPropIRGenerator::tryAttachTypedObject
     trackAttached("TypedObject");
     return true;
 }
 
 void
 SetPropIRGenerator::trackAttached(const char* name)
 {
 #ifdef JS_CACHEIR_SPEW
-    CacheIRSpewer& sp = CacheIRSpewer::singleton();
-    if (sp.enabled()) {
-        LockGuard<Mutex> guard(sp.lock());
-        sp.beginCache(guard, *this);
-        sp.valueProperty(guard, "base", lhsVal_);
-        sp.valueProperty(guard, "property", idVal_);
-        sp.valueProperty(guard, "value", rhsVal_);
-        sp.attached(guard, name);
-        sp.endCache(guard);
-    }
-#endif
-}
-
-void
-SetPropIRGenerator::trackNotAttached()
-{
-#ifdef JS_CACHEIR_SPEW
-    CacheIRSpewer& sp = CacheIRSpewer::singleton();
-    if (sp.enabled()) {
-        LockGuard<Mutex> guard(sp.lock());
-        sp.beginCache(guard, *this);
-        sp.valueProperty(guard, "base", lhsVal_);
-        sp.valueProperty(guard, "property", idVal_);
-        sp.valueProperty(guard, "value", rhsVal_);
-        sp.endCache(guard);
+    if (const CacheIRSpewer::Guard& sp = CacheIRSpewer::Guard(*this, name)) {
+        sp.valueProperty("base", lhsVal_);
+        sp.valueProperty("property", idVal_);
+        sp.valueProperty("value", rhsVal_);
     }
 #endif
 }
 
 static bool
 IsCacheableSetPropCallNative(JSObject* obj, JSObject* holder, Shape* shape)
 {
     if (!shape || !IsCacheableProtoChain(obj, holder))
@@ -4223,59 +4122,59 @@ InstanceOfIRGenerator::InstanceOfIRGener
 bool
 InstanceOfIRGenerator::tryAttachStub()
 {
     MOZ_ASSERT(cacheKind_ == CacheKind::InstanceOf);
     AutoAssertNoPendingException aanpe(cx_);
 
     // Ensure RHS is a function -- could be a Proxy, which the IC isn't prepared to handle.
     if (!rhsObj_->is<JSFunction>()) {
-        trackNotAttached();
+        trackAttached(nullptr);
         return false;
     }
 
     HandleFunction fun = rhsObj_.as<JSFunction>();
 
     if (fun->isBoundFunction()) {
-        trackNotAttached();
+        trackAttached(nullptr);
         return false;
     }
 
     // If the user has supplied their own @@hasInstance method we shouldn't
     // clobber it.
     if (!js::FunctionHasDefaultHasInstance(fun, cx_->wellKnownSymbols())) {
-        trackNotAttached();
+        trackAttached(nullptr);
         return false;
     }
 
     // Refuse to optimize any function whose [[Prototype]] isn't
     // Function.prototype.
     if (!fun->hasStaticPrototype() || fun->hasUncacheableProto()) {
-        trackNotAttached();
+        trackAttached(nullptr);
         return false;
     }
 
     Value funProto = cx_->global()->getPrototype(JSProto_Function);
     if (!funProto.isObject() || fun->staticPrototype() != &funProto.toObject()) {
-        trackNotAttached();
+        trackAttached(nullptr);
         return false;
     }
 
     // Ensure that the function's prototype slot is the same.
     Shape* shape = fun->lookupPure(cx_->names().prototype);
     if (!shape || !shape->isDataProperty()) {
-        trackNotAttached();
+        trackAttached(nullptr);
         return false;
     }
 
     uint32_t slot = shape->slot();
 
     MOZ_ASSERT(fun->numFixedSlots() == 0, "Stub code relies on this");
     if (!fun->getSlot(slot).isObject()) {
-        trackNotAttached();
+        trackAttached(nullptr);
         return false;
     }
 
     JSObject* prototypeObject = &fun->getSlot(slot).toObject();
 
     // Abstract Objects
     ValOperandId lhs(writer.setInputOperandId(0));
     ValOperandId rhs(writer.setInputOperandId(1));
@@ -4295,39 +4194,19 @@ InstanceOfIRGenerator::tryAttachStub()
     trackAttached("InstanceOf");
     return true;
 }
 
 void
 InstanceOfIRGenerator::trackAttached(const char* name)
 {
 #ifdef JS_CACHEIR_SPEW
-    CacheIRSpewer& sp = CacheIRSpewer::singleton();
-    if (sp.enabled()) {
-        LockGuard<Mutex> guard(sp.lock());
-        sp.beginCache(guard, *this);
-        sp.valueProperty(guard, "lhs", lhsVal_);
-        sp.valueProperty(guard, "rhs", ObjectValue(*rhsObj_));
-        sp.attached(guard, name);
-        sp.endCache(guard);
-    }
-#endif
-}
-
-void
-InstanceOfIRGenerator::trackNotAttached()
-{
-#ifdef JS_CACHEIR_SPEW
-    CacheIRSpewer& sp = CacheIRSpewer::singleton();
-    if (sp.enabled()) {
-        LockGuard<Mutex> guard(sp.lock());
-        sp.beginCache(guard, *this);
-        sp.valueProperty(guard, "lhs", lhsVal_);
-        sp.valueProperty(guard, "rhs", ObjectValue(*rhsObj_));
-        sp.endCache(guard);
+    if (const CacheIRSpewer::Guard& sp = CacheIRSpewer::Guard(*this, name)) {
+        sp.valueProperty("lhs", lhsVal_);
+        sp.valueProperty("rhs", ObjectValue(*rhsObj_));
     }
 #endif
 }
 
 TypeOfIRGenerator::TypeOfIRGenerator(JSContext* cx, HandleScript script, jsbytecode* pc,
                                      ICState::Mode mode, HandleValue value)
   : IRGenerator(cx, script, pc, CacheKind::TypeOf, mode),
     val_(value)
@@ -4708,43 +4587,60 @@ CallIRGenerator::tryAttachStub()
 
     return false;
 }
 
 void
 CallIRGenerator::trackAttached(const char* name)
 {
 #ifdef JS_CACHEIR_SPEW
-    CacheIRSpewer& sp = CacheIRSpewer::singleton();
-    if (sp.enabled()) {
-        LockGuard<Mutex> guard(sp.lock());
-        sp.beginCache(guard, *this);
-        sp.valueProperty(guard, "callee", callee_);
-        sp.valueProperty(guard, "thisval", thisval_);
-        sp.valueProperty(guard, "argc", Int32Value(argc_));
-        sp.attached(guard, name);
-        sp.endCache(guard);
+    if (const CacheIRSpewer::Guard& sp = CacheIRSpewer::Guard(*this, name)) {
+        sp.valueProperty("callee", callee_);
+        sp.valueProperty("thisval", thisval_);
+        sp.valueProperty("argc", Int32Value(argc_));
     }
 #endif
 }
 
+// Class which holds a shape pointer for use when caches might reference data in other zones.
+static const Class shapeContainerClass = {
+    "ShapeContainer",
+    JSCLASS_HAS_RESERVED_SLOTS(1)
+};
+
+static const size_t SHAPE_CONTAINER_SLOT = 0;
+
+JSObject*
+jit::NewWrapperWithObjectShape(JSContext* cx, HandleNativeObject obj)
+{
+    MOZ_ASSERT(cx->compartment() != obj->compartment());
+
+    RootedObject wrapper(cx);
+    {
+        AutoCompartment ac(cx, obj);
+        wrapper = NewObjectWithClassProto(cx, &shapeContainerClass, nullptr);
+        if (!obj)
+            return nullptr;
+        wrapper->as<NativeObject>().setSlot(SHAPE_CONTAINER_SLOT, PrivateGCThingValue(obj->lastProperty()));
+    }
+    if (!JS_WrapObject(cx, &wrapper))
+        return nullptr;
+    MOZ_ASSERT(IsWrapper(wrapper));
+    return wrapper;
+}
+
 void
-CallIRGenerator::trackNotAttached()
+jit::LoadShapeWrapperContents(MacroAssembler& masm, Register obj, Register dst, Label* failure)
 {
-#ifdef JS_CACHEIR_SPEW
-    CacheIRSpewer& sp = CacheIRSpewer::singleton();
-    if (sp.enabled()) {
-        LockGuard<Mutex> guard(sp.lock());
-        sp.beginCache(guard, *this);
-        sp.valueProperty(guard, "callee", callee_);
-        sp.valueProperty(guard, "thisval", thisval_);
-        sp.valueProperty(guard, "argc", Int32Value(argc_));
-        sp.endCache(guard);
-    }
-#endif
+    masm.loadPtr(Address(obj, ProxyObject::offsetOfReservedSlots()), dst);
+    Address privateAddr(dst, detail::ProxyReservedSlots::offsetOfPrivateSlot());
+    masm.branchTestObject(Assembler::NotEqual, privateAddr, failure);
+    masm.unboxObject(privateAddr, dst);
+    masm.unboxNonDouble(Address(dst, NativeObject::getFixedSlotOffset(SHAPE_CONTAINER_SLOT)), dst,
+                        JSVAL_TYPE_PRIVATE_GCTHING);
 }
 
 CompareIRGenerator::CompareIRGenerator(JSContext* cx, HandleScript script, jsbytecode* pc,
                                        ICState::Mode mode, JSOp op,
                                        HandleValue lhsVal, HandleValue rhsVal)
   : IRGenerator(cx, script, pc, CacheKind::Compare, mode),
     op_(op), lhsVal_(lhsVal), rhsVal_(rhsVal)
 { }
@@ -4816,86 +4712,47 @@ CompareIRGenerator::tryAttachStub()
     if (IsEqualityOp(op_)) {
         if (tryAttachString(lhsId, rhsId))
             return true;
         if (tryAttachObject(lhsId, rhsId))
             return true;
         if (tryAttachSymbol(lhsId, rhsId))
             return true;
 
-        trackNotAttached();
+        trackAttached(nullptr);
         return false;
     }
 
-    trackNotAttached();
+    trackAttached(nullptr);
     return false;
 }
 
 void
 CompareIRGenerator::trackAttached(const char* name)
 {
 #ifdef JS_CACHEIR_SPEW
-    CacheIRSpewer& sp = CacheIRSpewer::singleton();
-    if (sp.enabled()) {
-        LockGuard<Mutex> guard(sp.lock());
-        sp.beginCache(guard, *this);
-        sp.valueProperty(guard, "lhs", lhsVal_);
-        sp.valueProperty(guard, "rhs", rhsVal_);
-        sp.attached(guard, name);
-        sp.endCache(guard);
-    }
-#endif
-}
-
-void
-CompareIRGenerator::trackNotAttached()
-{
-#ifdef JS_CACHEIR_SPEW
-    CacheIRSpewer& sp = CacheIRSpewer::singleton();
-    if (sp.enabled()) {
-        LockGuard<Mutex> guard(sp.lock());
-        sp.beginCache(guard, *this);
-        sp.valueProperty(guard, "lhs", lhsVal_);
-        sp.valueProperty(guard, "rhs", rhsVal_);
-        sp.endCache(guard);
+    if (const CacheIRSpewer::Guard& sp = CacheIRSpewer::Guard(*this, name)) {
+        sp.valueProperty("lhs", lhsVal_);
+        sp.valueProperty("rhs", rhsVal_);
     }
 #endif
 }
 
 ToBoolIRGenerator::ToBoolIRGenerator(JSContext* cx, HandleScript script, jsbytecode* pc, ICState::Mode mode,
                                      HandleValue val)
   : IRGenerator(cx, script, pc, CacheKind::ToBool, mode),
     val_(val)
 {}
 
 void
 ToBoolIRGenerator::trackAttached(const char* name)
 {
 #ifdef JS_CACHEIR_SPEW
-    CacheIRSpewer& sp = CacheIRSpewer::singleton();
-    if (sp.enabled()) {
-        LockGuard<Mutex> guard(sp.lock());
-        sp.beginCache(guard, *this);
-        sp.valueProperty(guard, "val", val_);
-        sp.attached(guard, name);
-        sp.endCache(guard);
-    }
-#endif
-}
-
-void
-ToBoolIRGenerator::trackNotAttached()
-{
-#ifdef JS_CACHEIR_SPEW
-    CacheIRSpewer& sp = CacheIRSpewer::singleton();
-    if (sp.enabled()) {
-        LockGuard<Mutex> guard(sp.lock());
-        sp.beginCache(guard, *this);
-        sp.valueProperty(guard, "val", val_);
-        sp.endCache(guard);
+    if (const CacheIRSpewer::Guard& sp = CacheIRSpewer::Guard(*this, name)) {
+        sp.valueProperty("val", val_);
     }
 #endif
 }
 
 bool
 ToBoolIRGenerator::tryAttachStub()
 {
     AutoAssertNoPendingException aanpe(cx_);
@@ -4908,17 +4765,17 @@ ToBoolIRGenerator::tryAttachStub()
         return true;
     if (tryAttachNullOrUndefined())
         return true;
     if (tryAttachObject())
         return true;
     if (tryAttachSymbol())
         return true;
 
-    trackNotAttached();
+    trackAttached(nullptr);
     return false;
 }
 
 bool
 ToBoolIRGenerator::tryAttachInt32()
 {
     if (!val_.isInt32())
         return false;
@@ -5006,78 +4863,22 @@ GetIntrinsicIRGenerator::GetIntrinsicIRG
   : IRGenerator(cx, script, pc, CacheKind::GetIntrinsic, mode)
   , val_(val)
 {}
 
 void
 GetIntrinsicIRGenerator::trackAttached(const char* name)
 {
 #ifdef JS_CACHEIR_SPEW
-    CacheIRSpewer& sp = CacheIRSpewer::singleton();
-    if (sp.enabled()) {
-        LockGuard<Mutex> guard(sp.lock());
-        sp.beginCache(guard, *this);
-        sp.valueProperty(guard, "val", val_);
-        sp.attached(guard, name);
-        sp.endCache(guard);
-    }
-#endif
-}
-
-void
-GetIntrinsicIRGenerator::trackNotAttached()
-{
-#ifdef JS_CACHEIR_SPEW
-    CacheIRSpewer& sp = CacheIRSpewer::singleton();
-    if (sp.enabled()) {
-        LockGuard<Mutex> guard(sp.lock());
-        sp.beginCache(guard, *this);
-        sp.valueProperty(guard, "val", val_);
-        sp.endCache(guard);
+    if (const CacheIRSpewer::Guard& sp = CacheIRSpewer::Guard(*this, name)) {
+        sp.valueProperty("val", val_);
     }
 #endif
 }
 
 bool
 GetIntrinsicIRGenerator::tryAttachStub()
 {
     writer.loadValueResult(val_);
     writer.returnFromIC();
     trackAttached("GetIntrinsic");
     return true;
 }
-// Class which holds a shape pointer for use when caches might reference data in other zones.
-static const Class shapeContainerClass = {
-    "ShapeContainer",
-    JSCLASS_HAS_RESERVED_SLOTS(1)
-};
-
-static const size_t SHAPE_CONTAINER_SLOT = 0;
-
-JSObject*
-jit::NewWrapperWithObjectShape(JSContext* cx, HandleNativeObject obj)
-{
-    MOZ_ASSERT(cx->compartment() != obj->compartment());
-
-    RootedObject wrapper(cx);
-    {
-        AutoCompartment ac(cx, obj);
-        wrapper = NewObjectWithClassProto(cx, &shapeContainerClass, nullptr);
-        if (!obj)
-            return nullptr;
-        wrapper->as<NativeObject>().setSlot(SHAPE_CONTAINER_SLOT, PrivateGCThingValue(obj->lastProperty()));
-    }
-    if (!JS_WrapObject(cx, &wrapper))
-        return nullptr;
-    MOZ_ASSERT(IsWrapper(wrapper));
-    return wrapper;
-}
-
-void
-jit::LoadShapeWrapperContents(MacroAssembler& masm, Register obj, Register dst, Label* failure)
-{
-    masm.loadPtr(Address(obj, ProxyObject::offsetOfReservedSlots()), dst);
-    Address privateAddr(dst, detail::ProxyReservedSlots::offsetOfPrivateSlot());
-    masm.branchTestObject(Assembler::NotEqual, privateAddr, failure);
-    masm.unboxObject(privateAddr, dst);
-    masm.unboxNonDouble(Address(dst, NativeObject::getFixedSlotOffset(SHAPE_CONTAINER_SLOT)), dst,
-                        JSVAL_TYPE_PRIVATE_GCTHING);
-}
diff --git a/js/src/jit/CacheIR.h b/js/src/jit/CacheIR.h
--- a/js/src/jit/CacheIR.h
+++ b/js/src/jit/CacheIR.h
@@ -1345,17 +1345,16 @@ class MOZ_RAII GetPropIRGenerator : publ
     // due to GVN.
     bool idempotent() const { return pc_ == nullptr; }
 
     // If this is a GetElem cache, emit instructions to guard the incoming Value
     // matches |id|.
     void maybeEmitIdGuard(jsid id);
 
     void trackAttached(const char* name);
-    void trackNotAttached();
 
   public:
     GetPropIRGenerator(JSContext* cx, HandleScript script, jsbytecode* pc, CacheKind cacheKind,
                        ICState::Mode mode, bool* isTemporarilyUnoptimizable, HandleValue val,
                        HandleValue idVal, HandleValue receiver,
                        GetPropertyResultFlags resultFlags);
 
     bool tryAttachStub();
@@ -1375,17 +1374,16 @@ class MOZ_RAII GetNameIRGenerator : publ
     HandleObject env_;
     HandlePropertyName name_;
 
     bool tryAttachGlobalNameValue(ObjOperandId objId, HandleId id);
     bool tryAttachGlobalNameGetter(ObjOperandId objId, HandleId id);
     bool tryAttachEnvironmentName(ObjOperandId objId, HandleId id);
 
     void trackAttached(const char* name);
-    void trackNotAttached();
 
   public:
     GetNameIRGenerator(JSContext* cx, HandleScript script, jsbytecode* pc, ICState::Mode mode,
                        HandleObject env, HandlePropertyName name);
 
     bool tryAttachStub();
 };
 
@@ -1394,17 +1392,16 @@ class MOZ_RAII BindNameIRGenerator : pub
 {
     HandleObject env_;
     HandlePropertyName name_;
 
     bool tryAttachGlobalName(ObjOperandId objId, HandleId id);
     bool tryAttachEnvironmentName(ObjOperandId objId, HandleId id);
 
     void trackAttached(const char* name);
-    void trackNotAttached();
 
   public:
     BindNameIRGenerator(JSContext* cx, HandleScript script, jsbytecode* pc, ICState::Mode mode,
                         HandleObject env, HandlePropertyName name);
 
     bool tryAttachStub();
 };
 
@@ -1498,27 +1495,25 @@ class MOZ_RAII SetPropIRGenerator : publ
     bool tryAttachDOMProxyUnshadowed(HandleObject obj, ObjOperandId objId, HandleId id,
                                      ValOperandId rhsId);
     bool tryAttachDOMProxyExpando(HandleObject obj, ObjOperandId objId, HandleId id,
                                   ValOperandId rhsId);
     bool tryAttachProxy(HandleObject obj, ObjOperandId objId, HandleId id, ValOperandId rhsId);
     bool tryAttachProxyElement(HandleObject obj, ObjOperandId objId, ValOperandId rhsId);
     bool tryAttachMegamorphicSetElement(HandleObject obj, ObjOperandId objId, ValOperandId rhsId);
 
-    void trackAttached(const char* name);
-
   public:
     SetPropIRGenerator(JSContext* cx, HandleScript script, jsbytecode* pc, CacheKind cacheKind,
                        ICState::Mode mode, bool* isTemporarilyUnoptimizable, HandleValue lhsVal,
                        HandleValue idVal, HandleValue rhsVal, bool needsTypeBarrier = true,
                        bool maybeHasExtraIndexedProps = true);
 
     bool tryAttachStub();
     bool tryAttachAddSlotStub(HandleObjectGroup oldGroup, HandleShape oldShape);
-    void trackNotAttached();
+    void trackAttached(const char* name);
 
     bool shouldUnlinkPreliminaryObjectStubs() const {
         return preliminaryObjectAction_ == PreliminaryObjectAction::Unlink;
     }
     bool shouldNotePreliminaryObjectStub() const {
         return preliminaryObjectAction_ == PreliminaryObjectAction::NotePreliminary;
     }
 
@@ -1561,33 +1556,31 @@ class MOZ_RAII HasPropIRGenerator : publ
     bool tryAttachSlotDoesNotExist(JSObject* obj, ObjOperandId objId,
                                    jsid key, ValOperandId keyId);
     bool tryAttachDoesNotExist(HandleObject obj, ObjOperandId objId,
                                HandleId key, ValOperandId keyId);
     bool tryAttachProxyElement(HandleObject obj, ObjOperandId objId,
                                ValOperandId keyId);
 
     void trackAttached(const char* name);
-    void trackNotAttached();
 
   public:
     // NOTE: Argument order is PROPERTY, OBJECT
     HasPropIRGenerator(JSContext* cx, HandleScript script, jsbytecode* pc, CacheKind cacheKind,
                        ICState::Mode mode, HandleValue idVal, HandleValue val);
 
     bool tryAttachStub();
 };
 
 class MOZ_RAII InstanceOfIRGenerator : public IRGenerator
 {
     HandleValue lhsVal_;
     HandleObject rhsObj_;
 
     void trackAttached(const char* name);
-    void trackNotAttached();
   public:
     InstanceOfIRGenerator(JSContext*, HandleScript, jsbytecode*, ICState::Mode,
                           HandleValue, HandleObject);
 
     bool tryAttachStub();
 };
 
 class MOZ_RAII TypeOfIRGenerator : public IRGenerator
@@ -1627,17 +1620,16 @@ class MOZ_RAII CallIRGenerator : public 
     PropertyTypeCheckInfo typeCheckInfo_;
     BaselineCacheIRStubKind cacheIRStubKind_;
 
     bool tryAttachStringSplit();
     bool tryAttachArrayPush();
     bool tryAttachArrayJoin();
 
     void trackAttached(const char* name);
-    void trackNotAttached();
 
   public:
     CallIRGenerator(JSContext* cx, HandleScript script, jsbytecode* pc,
                     JSOp op, ICCall_Fallback* stub, ICState::Mode mode,
                     uint32_t argc, HandleValue callee, HandleValue thisval,
                     HandleValueArray args);
 
     bool tryAttachStub();
@@ -1657,17 +1649,16 @@ class MOZ_RAII CompareIRGenerator : publ
     HandleValue lhsVal_;
     HandleValue rhsVal_;
 
     bool tryAttachString(ValOperandId lhsId, ValOperandId rhsId);
     bool tryAttachObject(ValOperandId lhsId, ValOperandId rhsId);
     bool tryAttachSymbol(ValOperandId lhsId, ValOperandId rhsId);
 
     void trackAttached(const char* name);
-    void trackNotAttached();
 
   public:
     CompareIRGenerator(JSContext* cx, HandleScript, jsbytecode* pc, ICState::Mode mode,
                        JSOp op, HandleValue lhsVal, HandleValue rhsVal);
 
     bool tryAttachStub();
 };
 
@@ -1678,31 +1669,29 @@ class MOZ_RAII ToBoolIRGenerator : publi
     bool tryAttachInt32();
     bool tryAttachDouble();
     bool tryAttachString();
     bool tryAttachSymbol();
     bool tryAttachNullOrUndefined();
     bool tryAttachObject();
 
     void trackAttached(const char* name);
-    void trackNotAttached();
 
   public:
     ToBoolIRGenerator(JSContext* cx, HandleScript, jsbytecode* pc, ICState::Mode mode,
                       HandleValue val);
 
     bool tryAttachStub();
 };
 
 class MOZ_RAII GetIntrinsicIRGenerator : public IRGenerator
 {
     HandleValue val_;
 
     void trackAttached(const char* name);
-    void trackNotAttached();
 
   public:
     GetIntrinsicIRGenerator(JSContext* cx, HandleScript, jsbytecode* pc, ICState::Mode,
                             HandleValue val);
 
     bool tryAttachStub();
 };
 
diff --git a/js/src/jit/CacheIRSpewer.cpp b/js/src/jit/CacheIRSpewer.cpp
--- a/js/src/jit/CacheIRSpewer.cpp
+++ b/js/src/jit/CacheIRSpewer.cpp
@@ -68,17 +68,17 @@ CacheIRSpewer::init()
         return false;
     output.put("[");
 
     json.emplace(output);
     return true;
 }
 
 void
-CacheIRSpewer::beginCache(LockGuard<Mutex>&, const IRGenerator& gen)
+CacheIRSpewer::beginCache(const IRGenerator& gen)
 {
     MOZ_ASSERT(enabled());
     JSONPrinter& j = json.ref();
     const char* filename = gen.script_->filename();
     j.beginObject();
     j.property("name", CacheKindNames[uint8_t(gen.cacheKind_)]);
     j.property("file", filename ? filename : "null");
     j.property("mode", int(gen.mode_));
@@ -116,17 +116,17 @@ QuoteString(GenericPrinter& out, JSLinea
     JS::AutoCheckCannotGC nogc;
     if (str->hasLatin1Chars())
         QuoteString(out, str->latin1Chars(nogc), str->length());
     else
         QuoteString(out, str->twoByteChars(nogc), str->length());
 }
 
 void
-CacheIRSpewer::valueProperty(LockGuard<Mutex>&, const char* name, const Value& v)
+CacheIRSpewer::valueProperty(const char* name, const Value& v)
 {
     MOZ_ASSERT(enabled());
     JSONPrinter& j = json.ref();
 
     j.beginObjectProperty(name);
 
     const char* type = InformalValueTypeName(v);
     if (v.isInt32())
@@ -148,22 +148,22 @@ CacheIRSpewer::valueProperty(LockGuard<M
         j.formatProperty("value", "%p (shape: %p)", &v.toObject(),
                          v.toObject().maybeShape());
     }
 
     j.endObject();
 }
 
 void
-CacheIRSpewer::attached(LockGuard<Mutex>&, const char* name)
+CacheIRSpewer::attached(const char* name)
 {
     MOZ_ASSERT(enabled());
     json.ref().property("attached", name);
 }
 
 void
-CacheIRSpewer::endCache(LockGuard<Mutex>&)
+CacheIRSpewer::endCache()
 {
     MOZ_ASSERT(enabled());
     json.ref().endObject();
 }
 
 #endif /* JS_CACHEIR_SPEW */
diff --git a/js/src/jit/CacheIRSpewer.h b/js/src/jit/CacheIRSpewer.h
--- a/js/src/jit/CacheIRSpewer.h
+++ b/js/src/jit/CacheIRSpewer.h
@@ -22,33 +22,67 @@ namespace jit {
 
 class CacheIRSpewer
 {
     Mutex outputLock;
     Fprinter output;
     mozilla::Maybe<JSONPrinter> json;
     static CacheIRSpewer cacheIRspewer;
 
-  public:
-
     CacheIRSpewer();
     ~CacheIRSpewer();
 
-    static CacheIRSpewer& singleton() { return cacheIRspewer; }
-
-    bool init();
     bool enabled() { return json.isSome(); }
 
     // These methods can only be called when enabled() is true.
     Mutex& lock() { MOZ_ASSERT(enabled()); return outputLock; }
 
-    void beginCache(LockGuard<Mutex>&, const IRGenerator& generator);
-    void valueProperty(LockGuard<Mutex>&, const char* name, const Value& v);
-    void attached(LockGuard<Mutex>&, const char* name);
-    void endCache(LockGuard<Mutex>&);
+    void beginCache(const IRGenerator& generator);
+    void valueProperty(const char* name, const Value& v);
+    void attached(const char* name);
+    void endCache();
+
+  public:
+    static CacheIRSpewer& singleton() { return cacheIRspewer; }
+    bool init();
+
+    class MOZ_RAII Guard {
+        CacheIRSpewer& sp_;
+        const IRGenerator& gen_;
+        const char* name_;
+
+      public:
+        Guard(const IRGenerator& gen, const char* name)
+          : sp_(CacheIRSpewer::singleton()),
+            gen_(gen),
+            name_(name)
+        {
+          if (sp_.enabled()) {
+            sp_.lock().lock();
+            sp_.beginCache(gen_);
+          }
+        }
+
+        ~Guard() {
+          if (sp_.enabled()) {
+            if (name_ != nullptr)
+              sp_.attached(name_);
+            sp_.endCache();
+            sp_.lock().unlock();
+          }
+        }
+
+        void valueProperty(const char* name, const Value& v) const {
+          sp_.valueProperty(name, v);
+        }
+
+        explicit operator bool() const {
+          return sp_.enabled();
+        }
+    };
 };
 
 } // namespace jit
 } // namespace js
 
 #endif /* JS_CACHEIR_SPEW */
 
 #endif /* jit_CacheIRSpewer_h */
diff --git a/js/src/jit/IonIC.cpp b/js/src/jit/IonIC.cpp
--- a/js/src/jit/IonIC.cpp
+++ b/js/src/jit/IonIC.cpp
@@ -324,17 +324,17 @@ IonSetPropertyIC::update(JSContext* cx, 
         jsbytecode* pc = ic->pc();
         SetPropIRGenerator gen(cx, script, pc, ic->kind(), ic->state().mode(),
                                &isTemporarilyUnoptimizable,
                                objv, idVal, rhs, ic->needsTypeBarrier(), ic->guardHoles());
         if (gen.tryAttachAddSlotStub(oldGroup, oldShape)) {
             ic->attachCacheIRStub(cx, gen.writerRef(), gen.cacheKind(), ionScript, &attached,
                                   gen.typeCheckInfo());
         } else {
-            gen.trackNotAttached();
+            gen.trackAttached(nullptr);
         }
 
         if (!attached && !isTemporarilyUnoptimizable)
             ic->state().trackNotAttached();
     }
 
     return true;
 }
