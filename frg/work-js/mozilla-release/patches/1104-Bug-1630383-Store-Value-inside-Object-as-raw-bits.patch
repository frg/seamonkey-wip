From 54b3050fddb851d28182327a80987e3e8189c2c4 Mon Sep 17 00:00:00 2001
From: hawkeye116477 <hawkeye116477@gmail.com>
Date: Tue, 24 Aug 2021 17:13:32 +0200
Subject: [PATCH 1104/1328] Bug 1630383: Store Value inside Object as raw bits

The natural way to represent v8::Object is as a thin wrapper around a JS::Value. However, Value is MOZ_NON_PARAM and alignas(8), and irregexp uses objects as parameters. There are two plausible solutions: stop marking Value as MOZ_NON_PARAM, or stop storing a Value inside Object. I wrote a set of patches to do the former, but nbp pointed out in the bug I created that "the intent of alignas(8) was to ensure that 'doubles' are well aligned when loaded/stored as floating points values."

If we want to keep the annotations on Value, then we have to change Object instead. I tried rewriting it to store raw bits, and the patches seem alright, so I'm going with that unless somebody has a good reason not to.
---
 js/public/Value.h               | 2 +-
 js/src/new-regexp/regexp-shim.h | 9 +++++----
 2 files changed, 6 insertions(+), 5 deletions(-)

diff --git a/js/public/Value.h b/js/public/Value.h
index 831fe3b4719f..0b7e31acf7b1 100644
--- a/js/public/Value.h
+++ b/js/public/Value.h
@@ -692,7 +692,7 @@ class MOZ_NON_PARAM alignas(8) Value
         return data.s.payload.u32;
     }
 
-    uint64_t asRawBits() const {
+    constexpr uint64_t asRawBits() const {
         return data.asBits;
     }
 
diff --git a/js/src/new-regexp/regexp-shim.h b/js/src/new-regexp/regexp-shim.h
index c99cf5a2c089..a3a3fa3943f1 100644
--- a/js/src/new-regexp/regexp-shim.h
+++ b/js/src/new-regexp/regexp-shim.h
@@ -486,7 +486,7 @@ class Object {
  public:
   // The default object constructor in V8 stores a nullptr,
   // which has its low bit clear and is interpreted as Smi(0).
-  constexpr Object() : value_(JS::Int32Value(0)) {}
+  constexpr Object() : asBits_(JS::Int32Value(0).asRawBits()) {}
 
   Object(const JS::Value& value) {
     setValue(value);
@@ -502,13 +502,14 @@ class Object {
   }
 
   JS::Value value() const {
-    return value_;
+    return JS::Value::fromRawBits(asBits_);
   }
+
  protected:
   void setValue(const JS::Value& val) {
-    value_ = val;
+    asBits_ = val.asRawBits();
   }
-  JS::Value value_;
+  uint64_t asBits_;
 } JS_HAZ_GC_POINTER;
 
 class Smi : public Object {
-- 
2.33.0.windows.2

