# HG changeset patch
# User Jeff Walden <jwalden@mit.edu>
# Date 1528362967 25200
#      Thu Jun 07 02:16:07 2018 -0700
# Node ID e90575cf96ce99f89e8b195a58853dee01d2e79d
# Parent  b6821b80af877083d6a39b879ffd37709d9a635f
Bug 1467336 - Rename ungetCharIgnoreEOL to ungetCodeUnit.  r=arai

diff --git a/js/src/frontend/TokenStream.cpp b/js/src/frontend/TokenStream.cpp
--- a/js/src/frontend/TokenStream.cpp
+++ b/js/src/frontend/TokenStream.cpp
@@ -532,18 +532,18 @@ TokenStreamChars<char16_t, AnyCharsAcces
 
 // This gets the next code unit -- the next numeric sub-unit of source text,
 // possibly smaller than a full code point.  It is simple and stupid, and it
 // doesn't understand EOL, update line counters, or anything like that.  If you
 // use it to consume an EOL sequence, line counters *will not* be correct for
 // subsequent code.
 //
 // Only use this if (a) the resulting code unit is guaranteed to be ungotten
-// (by ungetCharIgnoreEOL()) if it's an EOL, and (b) the line-related state
-// (lineno, linebase) is not used before it's ungotten.
+// (by ungetCodeUnit()) if it's an EOL, and (b) the line-related state (lineno,
+// linebase) is not used before it's ungotten.
 template<typename CharT, class AnyCharsAccess>
 int32_t
 GeneralTokenStreamChars<CharT, AnyCharsAccess>::getCodeUnit()
 {
     if (MOZ_LIKELY(sourceUnits.hasRawChars()))
         return sourceUnits.getCodeUnit();
 
     anyCharsAccess().flags.isEOF = true;
@@ -570,17 +570,17 @@ GeneralTokenStreamChars<CharT, AnyCharsA
         anyCharsAccess().undoInternalUpdateLineInfoForEOL();
     } else {
         MOZ_ASSERT(sourceUnits.peekCodeUnit() == c);
     }
 }
 
 template<typename CharT>
 void
-TokenStreamCharsBase<CharT>::ungetCharIgnoreEOL(int32_t c)
+TokenStreamCharsBase<CharT>::ungetCodeUnit(int32_t c)
 {
     if (c == EOF)
         return;
 
     MOZ_ASSERT(!sourceUnits.atStart());
     sourceUnits.ungetCodeUnit();
 }
 
@@ -592,17 +592,17 @@ TokenStreamChars<char16_t, AnyCharsAcces
 
     unsigned numUnits = 0;
     char16_t units[2];
     unicode::UTF16Encode(codePoint, units, &numUnits);
 
     MOZ_ASSERT(numUnits == 1 || numUnits == 2);
 
     while (numUnits-- > 0)
-        ungetCharIgnoreEOL(units[numUnits]);
+        ungetCodeUnit(units[numUnits]);
 }
 
 // Return true iff |n| raw characters can be read from this without reading past
 // EOF, and copy those characters into |cp| if so.  The characters are not
 // consumed: use skipChars(n) to do so after checking that the consumed
 // characters had appropriate values.
 template<typename CharT, class AnyCharsAccess>
 bool
@@ -613,17 +613,17 @@ TokenStreamSpecific<CharT, AnyCharsAcces
         int32_t c = getCodeUnit();
         if (c == EOF)
             break;
 
         cp[i] = char16_t(c);
     }
 
     for (int j = i - 1; j >= 0; j--)
-        ungetCharIgnoreEOL(cp[j]);
+        ungetCodeUnit(cp[j]);
 
     return i == n;
 }
 
 template<typename CharT>
 size_t
 SourceUnits<CharT>::findEOLMax(size_t start, size_t max)
 {
@@ -1004,17 +1004,17 @@ TokenStreamSpecific<CharT, AnyCharsAcces
 // value) if we found a Unicode escape sequence.  Otherwise, return 0.  In both
 // cases, do not advance along the buffer.
 template<typename CharT, class AnyCharsAccess>
 uint32_t
 TokenStreamSpecific<CharT, AnyCharsAccess>::peekUnicodeEscape(uint32_t* codePoint)
 {
     int32_t c = getCodeUnit();
     if (c != 'u') {
-        ungetCharIgnoreEOL(c);
+        ungetCodeUnit(c);
         return 0;
     }
 
     CharT cp[3];
     uint32_t length;
     c = getCodeUnit();
     if (JS7_ISHEX(c) && peekChars(3, cp) &&
         JS7_ISHEX(cp[0]) && JS7_ISHEX(cp[1]) && JS7_ISHEX(cp[2]))
@@ -1025,18 +1025,18 @@ TokenStreamSpecific<CharT, AnyCharsAcces
                      JS7_UNHEX(cp[2]);
         length = 5;
     } else if (c == '{') {
         length = peekExtendedUnicodeEscape(codePoint);
     } else {
         length = 0;
     }
 
-    ungetCharIgnoreEOL(c);
-    ungetCharIgnoreEOL('u');
+    ungetCodeUnit(c);
+    ungetCodeUnit('u');
     return length;
 }
 
 template<typename CharT, class AnyCharsAccess>
 uint32_t
 TokenStreamSpecific<CharT, AnyCharsAccess>::peekExtendedUnicodeEscape(uint32_t* codePoint)
 {
     // The opening brace character was already read.
@@ -1061,21 +1061,21 @@ TokenStreamSpecific<CharT, AnyCharsAcces
     uint32_t length;
     if (c == '}' && (leadingZeros > 0 || i > 0) && code <= unicode::NonBMPMax) {
         *codePoint = code;
         length = leadingZeros + i + 3;
     } else {
         length = 0;
     }
 
-    ungetCharIgnoreEOL(c);
+    ungetCodeUnit(c);
     while (i--)
-        ungetCharIgnoreEOL(cp[i]);
+        ungetCodeUnit(cp[i]);
     while (leadingZeros--)
-        ungetCharIgnoreEOL('0');
+        ungetCodeUnit('0');
 
     return length;
 }
 
 template<typename CharT, class AnyCharsAccess>
 uint32_t
 TokenStreamSpecific<CharT, AnyCharsAccess>::matchUnicodeEscapeIdStart(uint32_t* codePoint)
 {
@@ -1188,18 +1188,18 @@ TokenStreamSpecific<CharT, AnyCharsAcces
             break;
 
         consumeKnownChar(c);
 
         // Debugging directives can occur in both single- and multi-line
         // comments. If we're currently inside a multi-line comment, we also
         // need to recognize multi-line comment terminators.
         if (isMultiline && c == '*' && matchChar('/')) {
-            ungetCharIgnoreEOL('/');
-            ungetCharIgnoreEOL('*');
+            ungetCodeUnit('/');
+            ungetCodeUnit('*');
             break;
         }
 
         if (!tokenbuf.append(c))
             return false;
     } while (true);
 
     if (tokenbuf.empty()) {
@@ -1316,17 +1316,17 @@ TokenStreamChars<char16_t, AnyCharsAcces
     MOZ_ASSERT(unicode::IsLeadSurrogate(lead),
                "matchMultiUnitCodepoint should have ensured |lead| is a lead "
                "surrogate");
 
     int32_t maybeTrail = getCodeUnit();
     if (MOZ_LIKELY(unicode::IsTrailSurrogate(maybeTrail))) {
         *codePoint = unicode::UTF16Decode(lead, maybeTrail);
     } else {
-        ungetCharIgnoreEOL(maybeTrail);
+        ungetCodeUnit(maybeTrail);
         *codePoint = 0;
     }
 }
 
 template<typename CharT, class AnyCharsAccess>
 bool
 TokenStreamSpecific<CharT, AnyCharsAccess>::putIdentInTokenbuf(const CharT* identStart)
 {
@@ -1395,17 +1395,17 @@ TokenStreamSpecific<CharT, AnyCharsAcces
 
         if (!unicode::IsIdentifierPart(char16_t(c))) {
             uint32_t qc;
             if (c != '\\' || !matchUnicodeEscapeIdent(&qc))
                 break;
             escaping = IdentifierEscapes::SawUnicodeEscape;
         }
     }
-    ungetCharIgnoreEOL(c);
+    ungetCodeUnit(c);
 
     const CharT* chars;
     size_t length;
     if (escaping == IdentifierEscapes::SawUnicodeEscape) {
         // Identifiers containing Unicode escapes have to be converted into
         // tokenbuf before atomizing.
         if (!putIdentInTokenbuf(identStart))
             return false;
@@ -1521,17 +1521,17 @@ template<typename CharT, class AnyCharsA
 void
 GeneralTokenStreamChars<CharT, AnyCharsAccess>::consumeRestOfSingleLineComment()
 {
     int32_t c;
     do {
         c = getCodeUnit();
     } while (c != EOF && !SourceUnits::isRawEOLChar(c));
 
-    ungetCharIgnoreEOL(c);
+    ungetCodeUnit(c);
 }
 
 template<typename CharT, class AnyCharsAccess>
 MOZ_MUST_USE bool
 TokenStreamSpecific<CharT, AnyCharsAccess>::decimalNumber(int c, TokenStart start,
                                                           const CharT* numStart,
                                                           Modifier modifier, TokenKind* out)
 {
@@ -1544,17 +1544,17 @@ TokenStreamSpecific<CharT, AnyCharsAcces
     // Consume integral component digits.
     while (IsAsciiDigit(c))
         c = getCodeUnit();
 
     // Numbers contain no escapes, so we can read directly from |sourceUnits|.
     double dval;
     DecimalPoint decimalPoint = NoDecimal;
     if (c != '.' && c != 'e' && c != 'E') {
-        ungetCharIgnoreEOL(c);
+        ungetCodeUnit(c);
 
         // Most numbers are pure decimal integers without fractional component
         // or exponential notation.  Handle that with optimized code.
         if (!GetDecimalInteger(anyCharsAccess().cx, numStart, sourceUnits.addressOfNextCodeUnit(),
                                &dval))
         {
             return false;
         }
@@ -1570,28 +1570,28 @@ TokenStreamSpecific<CharT, AnyCharsAcces
         // Consume any exponential notation.
         if (c == 'e' || c == 'E') {
             c = getCodeUnit();
             if (c == '+' || c == '-')
                 c = getCodeUnit();
 
             // Exponential notation must contain at least one digit.
             if (!IsAsciiDigit(c)) {
-                ungetCharIgnoreEOL(c);
+                ungetCodeUnit(c);
                 error(JSMSG_MISSING_EXPONENT);
                 return false;
             }
 
             // Consume exponential digits.
             do {
                 c = getCodeUnit();
             } while (IsAsciiDigit(c));
         }
 
-        ungetCharIgnoreEOL(c);
+        ungetCodeUnit(c);
 
         const CharT* dummy;
         if (!js_strtod(anyCharsAccess().cx, numStart, sourceUnits.addressOfNextCodeUnit(), &dummy,
                        &dval))
         {
            return false;
         }
     }
@@ -1618,17 +1618,17 @@ TokenStreamSpecific<CharT, AnyCharsAcces
             if (unicode::IsIdentifierStart(codePoint)) {
                 // This will properly point at the start of the code point.
                 error(JSMSG_IDSTART_AFTER_NUMBER);
                 return false;
             }
         } else {
             // If not a multi-unit code point, we only need to unget the single
             // code unit consumed.
-            ungetCharIgnoreEOL(c);
+            ungetCodeUnit(c);
         }
     }
 
     noteBadToken.release();
     newNumberToken(dval, decimalPoint, start, modifier, out);
     return true;
 }
 
@@ -1783,45 +1783,45 @@ TokenStreamSpecific<CharT, AnyCharsAcces
 
             int radix;
             const CharT* numStart;
             c = getCodeUnit();
             if (c == 'x' || c == 'X') {
                 radix = 16;
                 c = getCodeUnit();
                 if (!JS7_ISHEX(c)) {
-                    ungetCharIgnoreEOL(c);
+                    ungetCodeUnit(c);
                     reportError(JSMSG_MISSING_HEXDIGITS);
                     return badToken();
                 }
 
                 // one past the '0x'
                 numStart = sourceUnits.addressOfNextCodeUnit() - 1;
 
                 while (JS7_ISHEX(c))
                     c = getCodeUnit();
             } else if (c == 'b' || c == 'B') {
                 radix = 2;
                 c = getCodeUnit();
                 if (c != '0' && c != '1') {
-                    ungetCharIgnoreEOL(c);
+                    ungetCodeUnit(c);
                     reportError(JSMSG_MISSING_BINARY_DIGITS);
                     return badToken();
                 }
 
                 // one past the '0b'
                 numStart = sourceUnits.addressOfNextCodeUnit() - 1;
 
                 while (c == '0' || c == '1')
                     c = getCodeUnit();
             } else if (c == 'o' || c == 'O') {
                 radix = 8;
                 c = getCodeUnit();
                 if (c < '0' || c > '7') {
-                    ungetCharIgnoreEOL(c);
+                    ungetCodeUnit(c);
                     reportError(JSMSG_MISSING_OCTAL_DIGITS);
                     return badToken();
                 }
 
                 // one past the '0o'
                 numStart = sourceUnits.addressOfNextCodeUnit() - 1;
 
                 while ('0' <= c && c <= '7')
@@ -1852,17 +1852,17 @@ TokenStreamSpecific<CharT, AnyCharsAcces
                     c = getCodeUnit();
                 } while (IsAsciiDigit(c));
             } else {
                 // '0' not followed by [XxBbOo0-9];  scan as a decimal number.
                 numStart = sourceUnits.addressOfNextCodeUnit() - 1;
 
                 return decimalNumber(c, start, numStart, modifier, ttp);
             }
-            ungetCharIgnoreEOL(c);
+            ungetCodeUnit(c);
 
             if (c != EOF) {
                 if (unicode::IsIdentifierStart(char16_t(c))) {
                     error(JSMSG_IDSTART_AFTER_NUMBER);
                     return badToken();
                 }
 
                 consumeKnownCharIgnoreEOL(c);
@@ -1879,17 +1879,17 @@ TokenStreamSpecific<CharT, AnyCharsAcces
                         // This will properly point at the start of the code
                         // point.
                         error(JSMSG_IDSTART_AFTER_NUMBER);
                         return badToken();
                     }
                 } else {
                     // If not a multi-unit code point, we only need to unget
                     // the single code unit consumed.
-                    ungetCharIgnoreEOL(c);
+                    ungetCodeUnit(c);
                 }
             }
 
             double dval;
             const char16_t* dummy;
             if (!GetPrefixInteger(anyCharsAccess().cx, numStart,
                                   sourceUnits.addressOfNextCodeUnit(), radix, &dummy, &dval))
             {
@@ -1921,17 +1921,17 @@ TokenStreamSpecific<CharT, AnyCharsAcces
             }
 
             if (c == '.') {
                 if (matchChar('.')) {
                     simpleKind = TokenKind::TripleDot;
                     break;
                 }
             }
-            ungetCharIgnoreEOL(c);
+            ungetCodeUnit(c);
 
             simpleKind = TokenKind::Dot;
             break;
 
           case '=':
             if (matchChar('='))
                 simpleKind = matchChar('=') ? TokenKind::StrictEq : TokenKind::Eq;
             else if (matchChar('>'))
@@ -1954,17 +1954,17 @@ TokenStreamSpecific<CharT, AnyCharsAcces
                                       sourceUnits.addressOfNextCodeUnit() - escapeLength - 1,
                                       IdentifierEscapes::SawUnicodeEscape, modifier, ttp);
             }
 
             // We could point "into" a mistyped escape, e.g. for "\u{41H}" we
             // could point at the 'H'.  But we don't do that now, so the
             // character after the '\' isn't necessarily bad, so just point at
             // the start of the actually-invalid escape.
-            ungetCharIgnoreEOL('\\');
+            ungetCodeUnit('\\');
             error(JSMSG_BAD_ESCAPE);
             return badToken();
           }
 
           case '|':
             if (matchChar('|'))
                 simpleKind = TokenKind::Or;
 #ifdef ENABLE_PIPELINE_OPERATOR
@@ -1997,19 +1997,19 @@ TokenStreamSpecific<CharT, AnyCharsAcces
             if (anyCharsAccess().options().allowHTMLComments) {
                 // Treat HTML begin-comment as comment-till-end-of-line.
                 if (matchChar('!')) {
                     if (matchChar('-')) {
                         if (matchChar('-')) {
                             consumeRestOfSingleLineComment();
                             continue;
                         }
-                        ungetCharIgnoreEOL('-');
+                        ungetCodeUnit('-');
                     }
-                    ungetCharIgnoreEOL('!');
+                    ungetCodeUnit('!');
                 }
             }
             if (matchChar('<'))
                 simpleKind = matchChar('=') ? TokenKind::LshAssign : TokenKind::Lsh;
             else
                 simpleKind = matchChar('=') ? TokenKind::Le : TokenKind::Lt;
             break;
 
@@ -2035,17 +2035,17 @@ TokenStreamSpecific<CharT, AnyCharsAcces
             // Look for a single-line comment.
             if (matchChar('/')) {
                 c = getCodeUnit();
                 if (c == '@' || c == '#') {
                     bool shouldWarn = c == '@';
                     if (!getDirectives(false, shouldWarn))
                         return false;
                 } else {
-                    ungetCharIgnoreEOL(c);
+                    ungetCodeUnit(c);
                 }
 
                 consumeRestOfSingleLineComment();
                 continue;
             }
 
             // Look for a multi-line comment.
             if (matchChar('*')) {
@@ -2134,17 +2134,17 @@ TokenStreamSpecific<CharT, AnyCharsAcces
                         MOZ_ASSERT(sourceUnits.offset() > 0);
                         char buf[2] = { char(c), '\0' };
                         errorAt(sourceUnits.offset() - 1, JSMSG_BAD_REGEXP_FLAG, buf);
                         return badToken();
                     }
 
                     reflags = RegExpFlag(reflags | flag);
                 }
-                ungetCharIgnoreEOL(c);
+                ungetCodeUnit(c);
 
                 newRegExpToken(reflags, start, modifier, ttp);
                 return true;
             }
 
             simpleKind = matchChar('=') ? TokenKind::DivAssign : TokenKind::Div;
             break;
 
@@ -2209,17 +2209,17 @@ TokenStreamSpecific<CharT, AnyCharsAcces
         this->badToken();
     });
 
     // We need to detect any of these chars:  " or ', \n (or its
     // equivalents), \\, EOF.  Because we detect EOL sequences here and
     // put them back immediately, we can use getCodeUnit().
     while ((c = getCodeUnit()) != untilChar) {
         if (c == EOF) {
-            ungetCharIgnoreEOL(c);
+            ungetCodeUnit(c);
             const char delimiters[] = { untilChar, untilChar, '\0' };
             error(JSMSG_EOF_BEFORE_END_OF_LITERAL, delimiters);
             return false;
         }
 
         if (c == '\\') {
             // When parsing templates, we don't immediately report errors for
             // invalid escapes; these are handled by the parser.
@@ -2283,17 +2283,17 @@ TokenStreamSpecific<CharT, AnyCharsAcces
                             break;
                         }
 
                         if (!JS7_ISHEX(c)) {
                             if (parsingTemplate) {
                                 // We put the character back so that we read
                                 // it on the next pass, which matters if it
                                 // was '`' or '\'.
-                                ungetCharIgnoreEOL(c);
+                                ungetCodeUnit(c);
 
                                 TokenStreamAnyChars& anyChars = anyCharsAccess();
                                 anyChars.setInvalidTemplateEscape(start,
                                                                   InvalidEscapeType::Unicode);
                                 valid = false;
                                 break;
                             }
                             reportInvalidEscapeError(start, InvalidEscapeType::Unicode);
@@ -2335,17 +2335,17 @@ TokenStreamSpecific<CharT, AnyCharsAcces
                     JS7_ISHEX(cp[0]) && JS7_ISHEX(cp[1]) && JS7_ISHEX(cp[2]))
                 {
                     c = (JS7_UNHEX(c2) << 12) |
                         (JS7_UNHEX(cp[0]) << 8) |
                         (JS7_UNHEX(cp[1]) << 4) |
                         JS7_UNHEX(cp[2]);
                     skipChars(3);
                 } else {
-                    ungetCharIgnoreEOL(c2);
+                    ungetCodeUnit(c2);
                     uint32_t start = sourceUnits.offset() - 2;
                     if (parsingTemplate) {
                         TokenStreamAnyChars& anyChars = anyCharsAccess();
                         anyChars.setInvalidTemplateEscape(start, InvalidEscapeType::Unicode);
                         continue;
                     }
                     reportInvalidEscapeError(start, InvalidEscapeType::Unicode);
                     return false;
@@ -2410,17 +2410,17 @@ TokenStreamSpecific<CharT, AnyCharsAcces
 
                     c = char16_t(val);
                 }
                 break;
             }
         } else if (c == '\r' || c == '\n') {
             if (!parsingTemplate) {
                 // String literals don't allow ASCII line breaks.
-                ungetCharIgnoreEOL(c);
+                ungetCodeUnit(c);
                 const char delimiters[] = { untilChar, untilChar, '\0' };
                 error(JSMSG_EOL_BEFORE_END_OF_STRING, delimiters);
                 return false;
             }
 
             if (c == '\r') {
                 c = '\n';
 
diff --git a/js/src/frontend/TokenStream.h b/js/src/frontend/TokenStream.h
--- a/js/src/frontend/TokenStream.h
+++ b/js/src/frontend/TokenStream.h
@@ -1011,17 +1011,17 @@ class SourceUnits
     /** Next char to get. */
     const CharT* ptr;
 };
 
 template<typename CharT>
 class TokenStreamCharsBase
 {
   protected:
-    void ungetCharIgnoreEOL(int32_t c);
+    void ungetCodeUnit(int32_t c);
 
   public:
     using CharBuffer = Vector<CharT, 32>;
 
     TokenStreamCharsBase(JSContext* cx, const CharT* chars, size_t length, size_t startOffset);
 
     static MOZ_ALWAYS_INLINE JSAtom*
     atomizeChars(JSContext* cx, const CharT* chars, size_t length);
@@ -1185,20 +1185,20 @@ class GeneralTokenStreamChars
         Token* token = newToken(TokenKind::RegExp, start, modifier, out);
         token->setRegExpFlags(reflags);
     }
 
     MOZ_COLD bool badToken();
 
     int32_t getCodeUnit();
 
-    void ungetCharIgnoreEOL(int32_t c) {
+    void ungetCodeUnit(int32_t c) {
         MOZ_ASSERT_IF(c == EOF, anyCharsAccess().flags.isEOF);
 
-        CharsSharedBase::ungetCharIgnoreEOL(c);
+        CharsSharedBase::ungetCodeUnit(c);
     }
 
     void ungetChar(int32_t c);
 
     /**
      * Consume characters til EOL/EOF following the start of a single-line
      * comment, without consuming the EOL/EOF.
      */
@@ -1225,17 +1225,17 @@ class TokenStreamChars<char16_t, AnyChar
     using typename GeneralCharsBase::TokenStreamSpecific;
 
     void matchMultiUnitCodePointSlow(char16_t lead, uint32_t* codePoint);
 
   protected:
     using GeneralCharsBase::anyCharsAccess;
     using GeneralCharsBase::getCodeUnit;
     using GeneralCharsBase::sourceUnits;
-    using CharsSharedBase::ungetCharIgnoreEOL;
+    using CharsSharedBase::ungetCodeUnit;
     using GeneralCharsBase::updateLineInfoForEOL;
 
     using GeneralCharsBase::GeneralCharsBase;
 
     // |c| must be the code unit just gotten.  If it and the subsequent code
     // unit form a valid surrogate pair, get the second code unit, set
     // |*codePoint| to the code point encoded by the surrogate pair, and return
     // true.  Otherwise do not get a second code unit, set |*codePoint = 0|,
@@ -1355,18 +1355,18 @@ class MOZ_STACK_CLASS TokenStreamSpecifi
     using GeneralCharsBase::newAtomToken;
     using GeneralCharsBase::newNameToken;
     using GeneralCharsBase::newNumberToken;
     using GeneralCharsBase::newRegExpToken;
     using GeneralCharsBase::newSimpleToken;
     using CharsSharedBase::sourceUnits;
     using CharsSharedBase::tokenbuf;
     using GeneralCharsBase::ungetChar;
-    using CharsSharedBase::ungetCharIgnoreEOL;
     using CharsBase::ungetCodePointIgnoreEOL;
+    using CharsSharedBase::ungetCodeUnit;
     using GeneralCharsBase::updateLineInfoForEOL;
 
     template<typename CharU> friend class TokenStreamPosition;
 
   public:
     TokenStreamSpecific(JSContext* cx, const ReadOnlyCompileOptions& options,
                         const CharT* base, size_t length);
 
