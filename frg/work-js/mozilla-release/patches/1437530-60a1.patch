# HG changeset patch
# User Andre Bargull <andre.bargull@gmail.com>
# Date 1519045649 28800
# Node ID 9929099d4c410e5e46750232856349564f6a9ab4
# Parent  4b66c9a7b2bea41c1de7be484a3717e6758ca6f1
Bug 1437530: Cache template literal objects per call site again. r=arai

diff --git a/js/public/MemoryMetrics.h b/js/public/MemoryMetrics.h
--- a/js/public/MemoryMetrics.h
+++ b/js/public/MemoryMetrics.h
@@ -769,17 +769,16 @@ struct CompartmentStats
     macro(Other,   MallocHeap, compartmentTables) \
     macro(Other,   MallocHeap, innerViewsTable) \
     macro(Other,   MallocHeap, lazyArrayBuffersTable) \
     macro(Other,   MallocHeap, objectMetadataTable) \
     macro(Other,   MallocHeap, crossCompartmentWrappersTable) \
     macro(Other,   MallocHeap, savedStacksSet) \
     macro(Other,   MallocHeap, varNamesSet) \
     macro(Other,   MallocHeap, nonSyntacticLexicalScopesTable) \
-    macro(Other,   MallocHeap, templateLiteralMap) \
     macro(Other,   MallocHeap, jitCompartment) \
     macro(Other,   MallocHeap, privateData) \
     macro(Other,   MallocHeap, scriptCountsMap)
 
     CompartmentStats()
       : FOR_EACH_SIZE(ZERO_SIZE)
         classInfo(),
         extra(),
diff --git a/js/src/gc/GC.cpp b/js/src/gc/GC.cpp
--- a/js/src/gc/GC.cpp
+++ b/js/src/gc/GC.cpp
@@ -2514,17 +2514,16 @@ GCRuntime::sweepZoneAfterCompacting(Zone
 
     if (jit::JitZone* jitZone = zone->jitZone())
         jitZone->sweep(fop);
 
     for (CompartmentsInZoneIter c(zone); !c.done(); c.next()) {
         c->objectGroups.sweep(fop);
         c->sweepRegExps();
         c->sweepSavedStacks();
-        c->sweepTemplateLiteralMap();
         c->sweepVarNames();
         c->sweepGlobalObject();
         c->sweepSelfHostingScriptSource();
         c->sweepDebugEnvironments();
         c->sweepJitCompartment(fop);
         c->sweepNativeIterators();
         c->sweepTemplateObjects();
     }
@@ -5414,17 +5413,16 @@ SweepRegExps(GCParallelTask* task)
 static void
 SweepMisc(GCParallelTask* task)
 {
     JSRuntime* runtime = task->runtime();
     for (SweepGroupCompartmentsIter c(runtime); !c.done(); c.next()) {
         c->sweepGlobalObject();
         c->sweepTemplateObjects();
         c->sweepSavedStacks();
-        c->sweepTemplateLiteralMap();
         c->sweepSelfHostingScriptSource();
         c->sweepNativeIterators();
     }
 }
 
 static void
 SweepCompressionTasks(GCParallelTask* task)
 {
diff --git a/js/src/jit/BaselineCompiler.cpp b/js/src/jit/BaselineCompiler.cpp
--- a/js/src/jit/BaselineCompiler.cpp
+++ b/js/src/jit/BaselineCompiler.cpp
@@ -1688,21 +1688,21 @@ BaselineCompiler::emit_JSOP_OBJECT()
     frame.push(ObjectValue(*script->getObject(pc)));
     return true;
 }
 
 bool
 BaselineCompiler::emit_JSOP_CALLSITEOBJ()
 {
     RootedObject cso(cx, script->getObject(pc));
-    RootedArrayObject raw(cx, &script->getObject(GET_UINT32_INDEX(pc) + 1)->as<ArrayObject>());
+    RootedObject raw(cx, script->getObject(GET_UINT32_INDEX(pc) + 1));
     if (!cso || !raw)
         return false;
 
-    if (!cx->compartment()->getTemplateLiteralObject(cx, raw, &cso))
+    if (!ProcessCallSiteObjOperation(cx, cso, raw))
         return false;
 
     frame.push(ObjectValue(*cso));
     return true;
 }
 
 typedef JSObject* (*CloneRegExpObjectFn)(JSContext*, Handle<RegExpObject*>);
 static const VMFunction CloneRegExpObjectInfo =
diff --git a/js/src/jit/IonBuilder.cpp b/js/src/jit/IonBuilder.cpp
--- a/js/src/jit/IonBuilder.cpp
+++ b/js/src/jit/IonBuilder.cpp
@@ -2239,26 +2239,17 @@ IonBuilder::inspectOpcode(JSOp op)
       case JSOP_DELELEM:
       case JSOP_STRICTDELELEM:
         return jsop_delelem();
 
       case JSOP_REGEXP:
         return jsop_regexp(info().getRegExp(pc));
 
       case JSOP_CALLSITEOBJ:
-        if (info().analysisMode() == Analysis_ArgumentsUsage) {
-            // When analyzing arguments usage, it is possible that the
-            // template object is not yet canonicalized. Push an incorrect
-            // object; it does not matter for arguments analysis.
-            pushConstant(ObjectValue(*info().getObject(pc)));
-        } else {
-            ArrayObject* raw = &script()->getObject(GET_UINT32_INDEX(pc) + 1)->as<ArrayObject>();
-            JSObject* obj = script()->compartment()->getExistingTemplateLiteralObject(raw);
-            pushConstant(ObjectValue(*obj));
-        }
+        pushConstant(ObjectValue(*info().getObject(pc)));
         return Ok();
 
       case JSOP_OBJECT:
         return jsop_object(info().getObject(pc));
 
       case JSOP_CLASSCONSTRUCTOR:
         return jsop_classconstructor();
 
diff --git a/js/src/tests/jstests.list b/js/src/tests/jstests.list
--- a/js/src/tests/jstests.list
+++ b/js/src/tests/jstests.list
@@ -451,12 +451,20 @@ skip script test262/annexB/built-ins/Fun
 # test262 importer merges all includes in a per directory shell.js file, breaking this harness test case.
 skip script test262/harness/detachArrayBuffer.js
 
 
 ####################################################
 # Tests disabled due to invalid test expectations  #
 ####################################################
 
+# https://github.com/tc39/test262/pull/972
+skip script test262/language/expressions/tagged-template/cache-identical-source-new-function.js
+skip script test262/language/expressions/tagged-template/cache-differing-expressions-new-function.js
+skip script test262/language/expressions/tagged-template/cache-differing-expressions.js
+skip script test262/language/expressions/tagged-template/cache-identical-source.js
+skip script test262/language/expressions/tagged-template/cache-differing-expressions-eval.js
+skip script test262/language/expressions/tagged-template/cache-identical-source-eval.js
+
 # https://github.com/tc39/test262/issues/1566
 random script test262/built-ins/Atomics/wake/wake-all-on-loc.js
 random script test262/built-ins/Atomics/wake/wake-all.js
 random script test262/built-ins/Atomics/wake/wake-in-order.js
diff --git a/js/src/tests/non262/template-strings/tagTempl.js b/js/src/tests/non262/template-strings/tagTempl.js
--- a/js/src/tests/non262/template-strings/tagTempl.js
+++ b/js/src/tests/non262/template-strings/tagTempl.js
@@ -198,17 +198,17 @@ assertEq(args`${Object.doesNotHaveThisPr
 var callSiteObj = [];
 callSiteObj[0] = cooked`aa${4}bb`;
 for (var i = 1; i < 3; i++)
     callSiteObj[i] = cooked`aa${4}bb`;
 
 // Same call site object behavior
 assertEq(callSiteObj[1], callSiteObj[2]);
 // Template objects are canonicalized
-assertEq(callSiteObj[0], callSiteObj[1]);
+assertEq(callSiteObj[0] !== callSiteObj[1], true);
 assertEq("raw" in callSiteObj[0], true);
 
 // Array length
 assertEq(callSiteObj[0].raw.length, 2);
 assertEq(callSiteObj[0].length, 2);
 
 // Frozen objects
 assertEq(Object.isFrozen(callSiteObj[0]), true);
@@ -222,17 +222,17 @@ check(new ((cs, sub) => function(){ retu
 
 var a = [];
 function test() {
     var x = callSite => callSite;
     for (var i = 0; i < 2; i++)
         a[i] = eval("x``");
 }
 test();
-assertEq(a[0], a[1]);
+assertEq(a[0] !== a[1], true);
 
 // Test that |obj.method`template`| works
 var newObj = {
     methodRetThis : function () {
         return this;
     },
     methodRetCooked : function (a) {
         return a;
diff --git a/js/src/vm/Interpreter-inl.h b/js/src/vm/Interpreter-inl.h
--- a/js/src/vm/Interpreter-inl.h
+++ b/js/src/vm/Interpreter-inl.h
@@ -679,16 +679,34 @@ InitArrayElemOperation(JSContext* cx, js
     } else {
         if (!DefineDataElement(cx, obj, index, val, JSPROP_ENUMERATE))
             return false;
     }
 
     return true;
 }
 
+static MOZ_ALWAYS_INLINE bool
+ProcessCallSiteObjOperation(JSContext* cx, HandleObject cso, HandleObject raw)
+{
+    MOZ_ASSERT(cso->is<ArrayObject>());
+    MOZ_ASSERT(raw->is<ArrayObject>());
+
+    if (cso->nonProxyIsExtensible()) {
+        RootedValue rawValue(cx, ObjectValue(*raw));
+        if (!DefineDataProperty(cx, cso, cx->names().raw, rawValue, 0))
+            return false;
+        if (!FreezeObject(cx, raw))
+            return false;
+        if (!FreezeObject(cx, cso))
+            return false;
+    }
+    return true;
+}
+
 #define RELATIONAL_OP(OP)                                                     \
     JS_BEGIN_MACRO                                                            \
         /* Optimize for two int-tagged operands (typical loop control). */    \
         if (lhs.isInt32() && rhs.isInt32()) {                                 \
             *res = lhs.toInt32() OP rhs.toInt32();                            \
         } else {                                                              \
             if (!ToPrimitive(cx, JSTYPE_NUMBER, lhs))                         \
                 return false;                                                 \
diff --git a/js/src/vm/Interpreter.cpp b/js/src/vm/Interpreter.cpp
--- a/js/src/vm/Interpreter.cpp
+++ b/js/src/vm/Interpreter.cpp
@@ -3341,17 +3341,17 @@ CASE(JSOP_OBJECT)
 }
 END_CASE(JSOP_OBJECT)
 
 CASE(JSOP_CALLSITEOBJ)
 {
     ReservedRooted<JSObject*> cso(&rootObject0, script->getObject(REGS.pc));
     ReservedRooted<JSObject*> raw(&rootObject1, script->getObject(GET_UINT32_INDEX(REGS.pc) + 1));
 
-    if (!cx->compartment()->getTemplateLiteralObject(cx, raw.as<ArrayObject>(), &cso))
+    if (!ProcessCallSiteObjOperation(cx, cso, raw))
         goto error;
 
     PUSH_OBJECT(*cso);
 }
 END_CASE(JSOP_CALLSITEOBJ)
 
 CASE(JSOP_REGEXP)
 {
diff --git a/js/src/vm/JSCompartment.cpp b/js/src/vm/JSCompartment.cpp
--- a/js/src/vm/JSCompartment.cpp
+++ b/js/src/vm/JSCompartment.cpp
@@ -151,17 +151,16 @@ JSCompartment::init(JSContext* maybecx)
     }
 
     enumerators = NativeIterator::allocateSentinel(maybecx);
     if (!enumerators)
         return false;
 
     if (!savedStacks_.init() ||
         !varNames_.init() ||
-        !templateLiteralMap_.init() ||
         !iteratorCache.init())
     {
         if (maybecx)
             ReportOutOfMemory(maybecx);
         return false;
     }
 
     return true;
@@ -615,80 +614,16 @@ JSCompartment::addToVarNames(JSContext* 
 
     if (varNames_.put(name))
         return true;
 
     ReportOutOfMemory(cx);
     return false;
 }
 
-/* static */ HashNumber
-TemplateRegistryHashPolicy::hash(const Lookup& lookup)
-{
-    size_t length = lookup->as<NativeObject>().getDenseInitializedLength();
-    HashNumber hash = 0;
-    for (uint32_t i = 0; i < length; i++) {
-        JSAtom& lookupAtom = lookup->as<NativeObject>().getDenseElement(i).toString()->asAtom();
-        hash = mozilla::AddToHash(hash, lookupAtom.hash());
-    }
-    return hash;
-}
-
-/* static */ bool
-TemplateRegistryHashPolicy::match(const Key& key, const Lookup& lookup)
-{
-    size_t length = lookup->as<NativeObject>().getDenseInitializedLength();
-    if (key->as<NativeObject>().getDenseInitializedLength() != length)
-        return false;
-
-    for (uint32_t i = 0; i < length; i++) {
-        JSAtom* a = &key->as<NativeObject>().getDenseElement(i).toString()->asAtom();
-        JSAtom* b = &lookup->as<NativeObject>().getDenseElement(i).toString()->asAtom();
-        if (a != b)
-            return false;
-    }
-
-    return true;
-}
-
-bool
-JSCompartment::getTemplateLiteralObject(JSContext* cx, HandleArrayObject rawStrings,
-                                        MutableHandleObject templateObj)
-{
-    if (TemplateRegistry::AddPtr p = templateLiteralMap_.lookupForAdd(rawStrings)) {
-        templateObj.set(p->value());
-
-        // The template object must have been frozen when it was added to the
-        // registry.
-        MOZ_ASSERT(!templateObj->nonProxyIsExtensible());
-    } else {
-        MOZ_ASSERT(templateObj->nonProxyIsExtensible());
-        RootedValue rawValue(cx, ObjectValue(*rawStrings));
-        if (!DefineDataProperty(cx, templateObj, cx->names().raw, rawValue, 0))
-            return false;
-        if (!FreezeObject(cx, rawStrings))
-            return false;
-        if (!FreezeObject(cx, templateObj))
-            return false;
-
-        if (!templateLiteralMap_.relookupOrAdd(p, rawStrings, templateObj))
-            return false;
-    }
-
-    return true;
-}
-
-JSObject*
-JSCompartment::getExistingTemplateLiteralObject(ArrayObject* rawStrings)
-{
-    TemplateRegistry::Ptr p = templateLiteralMap_.lookup(rawStrings);
-    MOZ_ASSERT(p);
-    return p->value();
-}
-
 void
 JSCompartment::traceOutgoingCrossCompartmentWrappers(JSTracer* trc)
 {
     MOZ_ASSERT(JS::CurrentThreadIsHeapMajorCollecting());
     MOZ_ASSERT(!zone()->isCollectingFromAnyThread() || trc->runtime()->gc.isHeapCompacting());
 
     for (NonStringWrapperEnum e(this); !e.empty(); e.popFront()) {
         if (e.front().key().is<JSObject*>()) {
@@ -720,20 +655,16 @@ void
 JSCompartment::traceGlobal(JSTracer* trc)
 {
     // Trace things reachable from the compartment's global. Note that these
     // edges must be swept too in case the compartment is live but the global is
     // not.
 
     savedStacks_.trace(trc);
 
-    // The template registry strongly holds everything in it by design and
-    // spec.
-    templateLiteralMap_.trace(trc);
-
     // Atoms are always tenured.
     if (!JS::CurrentThreadIsHeapMinorCollecting())
         varNames_.trace(trc);
 }
 
 void
 JSCompartment::traceRoots(JSTracer* trc, js::gc::GCRuntime::TraceOrMarkRuntime traceOrMark)
 {
@@ -832,22 +763,16 @@ JSCompartment::sweepAfterMinorGC(JSTrace
 
 void
 JSCompartment::sweepSavedStacks()
 {
     savedStacks_.sweep();
 }
 
 void
-JSCompartment::sweepTemplateLiteralMap()
-{
-    templateLiteralMap_.sweep();
-}
-
-void
 JSCompartment::sweepGlobalObject()
 {
     if (global_ && IsAboutToBeFinalized(&global_))
         global_.set(nullptr);
 }
 
 void
 JSCompartment::sweepSelfHostingScriptSource()
@@ -1093,17 +1018,16 @@ JSCompartment::clearTables()
 
     // No scripts should have run in this compartment. This is used when
     // merging a compartment that has been used off thread into another
     // compartment and zone.
     MOZ_ASSERT(crossCompartmentWrappers.empty());
     MOZ_ASSERT(!jitCompartment_);
     MOZ_ASSERT(!debugEnvs);
     MOZ_ASSERT(enumerators->next() == enumerators);
-    MOZ_ASSERT(templateLiteralMap_.empty());
 
     objectGroups.clearTables();
     if (savedStacks_.initialized())
         savedStacks_.clear();
     if (varNames_.initialized())
         varNames_.clear();
 }
 
@@ -1376,17 +1300,16 @@ JSCompartment::addSizeOfIncludingThis(mo
                                       size_t* compartmentTables,
                                       size_t* innerViewsArg,
                                       size_t* lazyArrayBuffersArg,
                                       size_t* objectMetadataTablesArg,
                                       size_t* crossCompartmentWrappersArg,
                                       size_t* savedStacksSet,
                                       size_t* varNamesSet,
                                       size_t* nonSyntacticLexicalEnvironmentsArg,
-                                      size_t* templateLiteralMap,
                                       size_t* jitCompartment,
                                       size_t* privateData,
                                       size_t* scriptCountsMapArg)
 {
     *compartmentObject += mallocSizeOf(this);
     objectGroups.addSizeOfExcludingThis(mallocSizeOf, tiAllocationSiteTables,
                                         tiArrayTypeTables, tiObjectTypeTables,
                                         compartmentTables);
@@ -1398,17 +1321,16 @@ JSCompartment::addSizeOfIncludingThis(mo
     if (objectMetadataTable)
         *objectMetadataTablesArg += objectMetadataTable->sizeOfIncludingThis(mallocSizeOf);
     *crossCompartmentWrappersArg += crossCompartmentWrappers.sizeOfExcludingThis(mallocSizeOf);
     *savedStacksSet += savedStacks_.sizeOfExcludingThis(mallocSizeOf);
     *varNamesSet += varNames_.sizeOfExcludingThis(mallocSizeOf);
     if (nonSyntacticLexicalEnvironments_)
         *nonSyntacticLexicalEnvironmentsArg +=
             nonSyntacticLexicalEnvironments_->sizeOfIncludingThis(mallocSizeOf);
-    *templateLiteralMap += templateLiteralMap_.sizeOfExcludingThis(mallocSizeOf);
     if (jitCompartment_)
         *jitCompartment += jitCompartment_->sizeOfIncludingThis(mallocSizeOf);
 
     auto callback = runtime_->sizeOfIncludingThisCompartmentCallback;
     if (callback)
         *privateData += callback(mallocSizeOf, this);
 
     if (scriptCountsMap) {
diff --git a/js/src/vm/JSCompartment.h b/js/src/vm/JSCompartment.h
--- a/js/src/vm/JSCompartment.h
+++ b/js/src/vm/JSCompartment.h
@@ -20,17 +20,16 @@
 #include "gc/Barrier.h"
 #include "gc/NurseryAwareHashMap.h"
 #include "gc/Zone.h"
 #include "vm/ArrayBufferObject.h"
 #include "vm/GlobalObject.h"
 #include "vm/ReceiverGuard.h"
 #include "vm/RegExpShared.h"
 #include "vm/SavedStacks.h"
-#include "vm/TemplateRegistry.h"
 #include "vm/Time.h"
 #include "wasm/WasmCompartment.h"
 
 namespace js {
 
 namespace jit {
 class JitCompartment;
 } // namespace jit
@@ -774,17 +773,16 @@ struct JSCompartment
                                 size_t* compartmentTables,
                                 size_t* innerViews,
                                 size_t* lazyArrayBuffers,
                                 size_t* objectMetadataTables,
                                 size_t* crossCompartmentWrappers,
                                 size_t* savedStacksSet,
                                 size_t* varNamesSet,
                                 size_t* nonSyntacticLexicalScopes,
-                                size_t* templateLiteralMap,
                                 size_t* jitCompartment,
                                 size_t* privateData,
                                 size_t* scriptCountsMapArg);
 
     // Object group tables and other state in the compartment.
     js::ObjectGroupCompartment   objectGroups;
 
 #ifdef JSGC_HASH_TABLE_CHECKS
@@ -817,22 +815,16 @@ struct JSCompartment
     js::wasm::Compartment wasm;
 
   private:
     // All non-syntactic lexical environments in the compartment. These are kept in
     // a map because when loading scripts into a non-syntactic environment, we need
     // to use the same lexical environment to persist lexical bindings.
     js::ObjectWeakMap* nonSyntacticLexicalEnvironments_;
 
-    // The realm's [[TemplateMap]], used for mapping template literals to
-    // unique template objects used in evaluation of tagged template literals.
-    //
-    // See ES 12.2.9.3.
-    js::TemplateRegistry templateLiteralMap_;
-
   public:
     /*
      * During GC, stores the head of a list of incoming pointers from gray cells.
      *
      * The objects in the list are either cross-compartment wrappers, or
      * debugger wrapper objects.  The list link is either in the second extra
      * slot for the former, or a special slot for the latter.
      */
@@ -950,17 +942,16 @@ struct JSCompartment
     /* Whether to preserve JIT code on non-shrinking GCs. */
     bool preserveJitCode() { return creationOptions_.preserveJitCode(); }
 
     void sweepAfterMinorGC(JSTracer* trc);
     void sweepMapAndSetObjectsAfterMinorGC();
 
     void sweepCrossCompartmentWrappers();
     void sweepSavedStacks();
-    void sweepTemplateLiteralMap();
     void sweepGlobalObject();
     void sweepSelfHostingScriptSource();
     void sweepJitCompartment(js::FreeOp* fop);
     void sweepRegExps();
     void sweepDebugEnvironments();
     void sweepNativeIterators();
     void sweepTemplateObjects();
     void sweepVarNames();
@@ -996,26 +987,16 @@ struct JSCompartment
         varNames_.remove(name);
     }
 
     // Whether the given name is in [[VarNames]].
     bool isInVarNames(JS::Handle<JSAtom*> name) {
         return varNames_.has(name);
     }
 
-    // Get a unique template object given a JS array of raw template strings
-    // and a template object. If a template object is found in template
-    // registry, that object is returned. Otherwise, the passed-in templateObj
-    // is added to the registry.
-    bool getTemplateLiteralObject(JSContext* cx, js::HandleArrayObject rawStrings,
-                                  js::MutableHandleObject templateObj);
-
-    // Per above, but an entry must already exist in the template registry.
-    JSObject* getExistingTemplateLiteralObject(js::ArrayObject* rawStrings);
-
     void findOutgoingEdges(js::gc::ZoneComponentFinder& finder);
 
     js::DtoaCache dtoaCache;
     js::NewProxyCache newProxyCache;
 
     // Random number generator for Math.random().
     mozilla::Maybe<mozilla::non_crypto::XorShift128PlusRNG> randomNumberGenerator;
 
diff --git a/js/src/vm/MemoryMetrics.cpp b/js/src/vm/MemoryMetrics.cpp
--- a/js/src/vm/MemoryMetrics.cpp
+++ b/js/src/vm/MemoryMetrics.cpp
@@ -356,17 +356,16 @@ StatsCompartmentCallback(JSContext* cx, 
                                         &cStats.compartmentTables,
                                         &cStats.innerViewsTable,
                                         &cStats.lazyArrayBuffersTable,
                                         &cStats.objectMetadataTable,
                                         &cStats.crossCompartmentWrappersTable,
                                         &cStats.savedStacksSet,
                                         &cStats.varNamesSet,
                                         &cStats.nonSyntacticLexicalScopesTable,
-                                        &cStats.templateLiteralMap,
                                         &cStats.jitCompartment,
                                         &cStats.privateData,
                                         &cStats.scriptCountsMap);
 }
 
 static void
 StatsArenaCallback(JSRuntime* rt, void* data, gc::Arena* arena,
                    JS::TraceKind traceKind, size_t thingSize)
diff --git a/js/src/vm/TemplateRegistry.h b/js/src/vm/TemplateRegistry.h
deleted file mode 100644
--- a/js/src/vm/TemplateRegistry.h
+++ /dev/null
@@ -1,38 +0,0 @@
-/* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
- * vim: set ts=8 sts=4 et sw=4 tw=99:
- * This Source Code Form is subject to the terms of the Mozilla Public
- * License, v. 2.0. If a copy of the MPL was not distributed with this
- * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
-
-#ifndef vm_TemplateRegistry_h
-#define vm_TemplateRegistry_h
-
-#include "gc/Marking.h"
-#include "js/GCHashTable.h"
-#include "vm/JSObject.h"
-
-namespace js {
-
-// Data structures to maintain unique template objects mapped to by lists of
-// raw strings.
-//
-// See ES 12.2.9.3.
-
-struct TemplateRegistryHashPolicy
-{
-    // For use as HashPolicy. Expects keys as arrays of atoms.
-    using Key = JSObject*;
-    using Lookup = JSObject*;
-
-    static HashNumber hash(const Lookup& lookup);
-    static bool match(const Key& key, const Lookup& lookup);
-};
-
-using TemplateRegistry = JS::GCHashMap<JSObject*,
-                                       JSObject*,
-                                       TemplateRegistryHashPolicy,
-                                       SystemAllocPolicy>;
-
-} // namespace js
-
-#endif // vm_TemplateRegistery_h
diff --git a/js/xpconnect/src/XPCJSRuntime.cpp b/js/xpconnect/src/XPCJSRuntime.cpp
--- a/js/xpconnect/src/XPCJSRuntime.cpp
+++ b/js/xpconnect/src/XPCJSRuntime.cpp
@@ -1825,20 +1825,16 @@ ReportCompartmentStats(const JS::Compart
     ZCREPORT_BYTES(cJSPathPrefix + NS_LITERAL_CSTRING("saved-stacks-set"),
         cStats.savedStacksSet,
         "The saved stacks set.");
 
     ZCREPORT_BYTES(cJSPathPrefix + NS_LITERAL_CSTRING("non-syntactic-lexical-scopes-table"),
         cStats.nonSyntacticLexicalScopesTable,
         "The non-syntactic lexical scopes table.");
 
-    ZCREPORT_BYTES(cJSPathPrefix + NS_LITERAL_CSTRING("template-literal-map"),
-        cStats.templateLiteralMap,
-        "The template literal registry.");
-
     ZCREPORT_BYTES(cJSPathPrefix + NS_LITERAL_CSTRING("jit-compartment"),
         cStats.jitCompartment,
         "The JIT compartment.");
 
     ZCREPORT_BYTES(cJSPathPrefix + NS_LITERAL_CSTRING("private-data"),
         cStats.privateData,
         "Extra data attached to the compartment by XPConnect, including "
         "its wrapped-js.");
