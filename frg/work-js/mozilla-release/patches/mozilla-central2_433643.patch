# HG changeset patch
# User Andrew Halberstadt <ahalberstadt@mozilla.com>
# Date 1535377186 0
#      Mon Aug 27 13:39:46 2018 +0000
# Node ID f664646a2e7c6554a738fe724c7a021fd9c591d7
# Parent  38549f3a51879ed4efc1c0a55c85e88198ed1c05
Bug 1460856 - [mozlint] Suppress warnings by default r=Standard8,sylvestre

As of this patch, any lint issue at the "warning" level will *only* be displayed
if --warnings is passed in. This gives us some flexibility to track issues that
are "recommended to fix" but that aren't required (aka don't cause a backout).
I think it would be ideal if the reviewbot ran with warnings enabled, and CI
ran without warnings. This way these issues will be surfaced to developers
(and hopefully get fixed prior to landing), but developers will always be able
to ignore them if the situation warrants it.

Since the last change converted everything to use errors, this patch should
be a no-op for now. However as we move forward (and potentially decide that
certain types of lint issues should result in a backout), this feature will
start seeing more and more use.

Depends on D3820

Differential Revision: https://phabricator.services.mozilla.com/D3821

diff --git a/python/mozlint/mozlint/cli.py b/python/mozlint/mozlint/cli.py
--- a/python/mozlint/mozlint/cli.py
+++ b/python/mozlint/mozlint/cli.py
@@ -30,16 +30,22 @@ class MozlintParser(ArgumentParser):
                   "are run for all the appropriate files.",
           }],
         [['--list'],
          {'dest': 'list_linters',
           'default': False,
           'action': 'store_true',
           'help': "List all available linters and exit.",
           }],
+        [['-W', '--warnings'],
+         {'dest': 'show_warnings',
+          'default': False,
+          'action': 'store_true',
+          'help': "Display and fail on warnings in addition to errors.",
+          }],
         [['-f', '--format'],
          {'dest': 'fmt',
           'default': 'stylish',
           'choices': all_formatters.keys(),
           'help': "Formatter to use. Defaults to 'stylish'.",
           }],
         [['-n', '--no-filter'],
          {'dest': 'use_filters',
diff --git a/python/mozlint/mozlint/result.py b/python/mozlint/mozlint/result.py
--- a/python/mozlint/mozlint/result.py
+++ b/python/mozlint/mozlint/result.py
@@ -13,34 +13,45 @@ class ResultSummary(object):
 
     def __init__(self):
         self.reset()
 
     def reset(self):
         self.issues = defaultdict(list)
         self.failed_run = set()
         self.failed_setup = set()
+        self.suppressed_warnings = defaultdict(int)
 
     @property
     def returncode(self):
         if self.issues or self.failed:
             return 1
         return 0
 
     @property
     def failed(self):
         return self.failed_setup | self.failed_run
 
+    @property
+    def total_issues(self):
+        return sum([len(v) for v in self.issues.values()])
+
+    @property
+    def total_suppressed_warnings(self):
+        return sum(self.suppressed_warnings.values())
+
     def update(self, other):
         """Merge results from another ResultSummary into this one."""
         for path, obj in other.issues.items():
             self.issues[path].extend(obj)
 
         self.failed_run |= other.failed_run
         self.failed_setup |= other.failed_setup
+        for k, v in other.suppressed_warnings.items():
+            self.suppressed_warnings[k] += v
 
 
 class Issue(object):
     """Represents a single lint issue and its related metadata.
 
     :param linter: name of the linter that flagged this error
     :param path: path to the file containing the error
     :param message: text describing the error
diff --git a/python/mozlint/mozlint/roller.py b/python/mozlint/mozlint/roller.py
--- a/python/mozlint/mozlint/roller.py
+++ b/python/mozlint/mozlint/roller.py
@@ -44,16 +44,20 @@ def _run_worker(config, paths, **lintarg
     finally:
         sys.stdout.flush()
 
     if not isinstance(res, (list, tuple)):
         if res:
             result.failed_run.add(config['name'])
     else:
         for r in res:
+            if not lintargs.get('show_warnings') and r.level == 'warning':
+                result.suppressed_warnings[r.path] += 1
+                continue
+
             result.issues[r.path].append(r)
     return result
 
 
 class InterruptableQueue(Queue):
     """A multiprocessing.Queue that catches KeyboardInterrupt when a worker is
     blocking on it and returns None.
 
diff --git a/python/mozlint/test/linters/warning.yml b/python/mozlint/test/linters/warning.yml
new file mode 100644
--- /dev/null
+++ b/python/mozlint/test/linters/warning.yml
@@ -0,0 +1,13 @@
+---
+WarningLinter:
+    description: >-
+        Make sure the string foobar never appears in browser js
+        files because it is bad, but not too bad (just a warning)
+    rule: no-foobar
+    level: warning
+    include:
+        - '**/*.js'
+        - '**/*.jsm'
+    type: string
+    extensions: ['.js', 'jsm']
+    payload: foobar
diff --git a/python/mozlint/test/test_roller.py b/python/mozlint/test/test_roller.py
--- a/python/mozlint/test/test_roller.py
+++ b/python/mozlint/test/test_roller.py
@@ -110,16 +110,32 @@ def test_roll_with_invalid_extension(lin
 def test_roll_with_failure_code(lint, lintdir, files):
     lint.read(os.path.join(lintdir, 'badreturncode.yml'))
 
     result = lint.roll(files, num_procs=1)
     assert len(result.issues) == 0
     assert result.failed == set(['BadReturnCodeLinter'])
 
 
+def test_roll_warnings(lint, lintdir, files):
+    lint.read(os.path.join(lintdir, 'warning.yml'))
+    result = lint.roll(files)
+    assert len(result.issues) == 0
+    assert result.total_issues == 0
+    assert len(result.suppressed_warnings) == 1
+    assert result.total_suppressed_warnings == 2
+
+    lint.lintargs['show_warnings'] = True
+    result = lint.roll(files)
+    assert len(result.issues) == 1
+    assert result.total_issues == 2
+    assert len(result.suppressed_warnings) == 0
+    assert result.total_suppressed_warnings == 0
+
+
 def fake_run_worker(config, paths, **lintargs):
     result = ResultSummary()
     result.issues['count'].append(1)
     return result
 
 
 @pytest.mark.skipif(platform.system() == 'Windows',
                     reason="monkeypatch issues with multiprocessing on Windows")
