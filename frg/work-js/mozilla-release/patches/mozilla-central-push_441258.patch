# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1539603755 0
#      Mon Oct 15 11:42:35 2018 +0000
# Node ID b449c4991dc4153716f7d53173a14f7a29284616
# Parent  992c0bd2f327fd11fa790814162621f77080eb6b
Bug 1496378 part 5 - Clean up ArrayBufferViewObject::trace. r=jwalden

Depends on D7724

Differential Revision: https://phabricator.services.mozilla.com/D7727

diff --git a/js/src/vm/ArrayBufferViewObject.cpp b/js/src/vm/ArrayBufferViewObject.cpp
--- a/js/src/vm/ArrayBufferViewObject.cpp
+++ b/js/src/vm/ArrayBufferViewObject.cpp
@@ -20,59 +20,39 @@ using namespace js;
  * This method is used to trace TypedArrayObjects and DataViewObjects. We need
  * a custom tracer to move the object's data pointer if its owner was moved and
  * stores its data inline.
  */
 /* static */ void
 ArrayBufferViewObject::trace(JSTracer* trc, JSObject* objArg)
 {
     NativeObject* obj = &objArg->as<NativeObject>();
-    HeapSlot& bufSlot = obj->getFixedSlotRef(TypedArrayObject::BUFFER_SLOT);
-    TraceEdge(trc, &bufSlot, "typedarray.buffer");
+    HeapSlot& bufSlot = obj->getFixedSlotRef(BUFFER_SLOT);
+    TraceEdge(trc, &bufSlot, "ArrayBufferViewObject.buffer");
 
     // Update obj's data pointer if it moved.
     if (bufSlot.isObject()) {
         if (IsArrayBuffer(&bufSlot.toObject())) {
             ArrayBufferObject& buf = AsArrayBuffer(MaybeForwarded(&bufSlot.toObject()));
-            uint32_t offset = uint32_t(obj->getFixedSlot(TypedArrayObject::BYTEOFFSET_SLOT).toInt32());
+            uint32_t offset = uint32_t(obj->getFixedSlot(BYTEOFFSET_SLOT).toInt32());
             MOZ_ASSERT(offset <= INT32_MAX);
 
-            if (buf.forInlineTypedObject()) {
-                MOZ_ASSERT(buf.dataPointer() != nullptr);
-
-                // The data is inline with an InlineTypedObject associated with the
-                // buffer. Get a new address for the typed object if it moved.
-                JSObject* view = buf.firstView();
-
-                // Mark the object to move it into the tenured space.
-                TraceManuallyBarrieredEdge(trc, &view, "typed array nursery owner");
-                MOZ_ASSERT(view->is<InlineTypedObject>());
-                MOZ_ASSERT(view != obj);
-
-                size_t nfixed = obj->numFixedSlotsMaybeForwarded();
-                void* srcData = obj->getPrivate(nfixed);
-                void* dstData = view->as<InlineTypedObject>().inlineTypedMemForGC() + offset;
-                obj->setPrivateUnbarriered(nfixed, dstData);
+            // We don't expose the underlying ArrayBuffer for typed objects,
+            // and we don't allow constructing a TypedObject from an arbitrary
+            // ArrayBuffer, so we should never have a TypedArray/DataView with
+            // a buffer that has TypedObject views.
+            MOZ_RELEASE_ASSERT(!buf.forInlineTypedObject());
 
-                // We can't use a direct forwarding pointer here, as there might
-                // not be enough bytes available, and other views might have data
-                // pointers whose forwarding pointers would overlap this one.
-                if (trc->isTenuringTracer()) {
-                    Nursery& nursery = trc->runtime()->gc.nursery();
-                    nursery.maybeSetForwardingPointer(trc, srcData, dstData, /* direct = */ false);
-                }
-            } else {
-                MOZ_ASSERT_IF(buf.dataPointer() == nullptr, offset == 0);
+            MOZ_ASSERT_IF(buf.dataPointer() == nullptr, offset == 0);
 
-                // The data may or may not be inline with the buffer. The buffer
-                // can only move during a compacting GC, in which case its
-                // objectMoved hook has already updated the buffer's data pointer.
-                size_t nfixed = obj->numFixedSlotsMaybeForwarded();
-                obj->setPrivateUnbarriered(nfixed, buf.dataPointer() + offset);
-            }
+            // The data may or may not be inline with the buffer. The buffer
+            // can only move during a compacting GC, in which case its
+            // objectMoved hook has already updated the buffer's data pointer.
+            size_t nfixed = obj->numFixedSlotsMaybeForwarded();
+            obj->setPrivateUnbarriered(nfixed, buf.dataPointer() + offset);
         }
     }
 }
 
 template <>
 bool
 JSObject::is<js::ArrayBufferViewObject>() const
 {
diff --git a/js/src/vm/TypedArrayObject.cpp b/js/src/vm/TypedArrayObject.cpp
--- a/js/src/vm/TypedArrayObject.cpp
+++ b/js/src/vm/TypedArrayObject.cpp
@@ -128,23 +128,16 @@ TypedArrayObject::assertZeroLengthArrayD
 {
     if (length() == 0 && !hasBuffer()) {
         uint8_t* end = fixedData(TypedArrayObject::FIXED_DATA_START);
         MOZ_ASSERT(end[0] == ZeroLengthArrayData);
     }
 }
 #endif
 
-/* static */ void
-TypedArrayObject::trace(JSTracer* trc, JSObject* objArg)
-{
-    // Handle all tracing required when the object has a buffer.
-    ArrayBufferViewObject::trace(trc, objArg);
-}
-
 void
 TypedArrayObject::finalize(FreeOp* fop, JSObject* obj)
 {
     MOZ_ASSERT(!IsInsideNursery(obj));
     TypedArrayObject* curObj = &obj->as<TypedArrayObject>();
 
     // Template objects or discarded objects (which didn't have enough room
     // for inner elements). Don't have anything to free.
@@ -2030,17 +2023,17 @@ static const ClassOps TypedArrayClassOps
     nullptr,                 /* enumerate   */
     nullptr,                 /* newEnumerate */
     nullptr,                 /* resolve     */
     nullptr,                 /* mayResolve  */
     TypedArrayObject::finalize, /* finalize    */
     nullptr,                 /* call        */
     nullptr,                 /* hasInstance */
     nullptr,                 /* construct   */
-    TypedArrayObject::trace, /* trace  */
+    ArrayBufferViewObject::trace, /* trace  */
 };
 
 static const ClassExtension TypedArrayClassExtension = {
     nullptr,
     TypedArrayObject::objectMoved,
 };
 
 #define IMPL_TYPED_ARRAY_PROPERTIES(_type)                                     \
diff --git a/js/src/vm/TypedArrayObject.h b/js/src/vm/TypedArrayObject.h
--- a/js/src/vm/TypedArrayObject.h
+++ b/js/src/vm/TypedArrayObject.h
@@ -146,17 +146,16 @@ class TypedArrayObject : public ArrayBuf
      * Byte length above which created typed arrays will have singleton types
      * regardless of the context in which they are created. This only applies to
      * typed arrays created with an existing ArrayBuffer.
      */
     static const uint32_t SINGLETON_BYTE_LENGTH = 1024 * 1024 * 10;
 
     static bool isOriginalLengthGetter(Native native);
 
-    static void trace(JSTracer* trc, JSObject* obj);
     static void finalize(FreeOp* fop, JSObject* obj);
     static size_t objectMoved(JSObject* obj, JSObject* old);
 
     /* Initialization bits */
 
     template<Value ValueGetter(const TypedArrayObject* tarr)>
     static bool
     GetterImpl(JSContext* cx, const CallArgs& args)
