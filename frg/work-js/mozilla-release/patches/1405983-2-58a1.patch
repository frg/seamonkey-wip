# HG changeset patch
# User Daisuke Akatsuka <dakatsuka@mozilla.com>
# Date 1507600287 -32400
# Node ID 34563d832147afe87b369f49f555e0ccc8268aa4
# Parent  a0ab31fb3b8019e9309481ccff6a210ed0e8a277
Bug 1405983 - Part 2: Modify tests for delay. r=pbro

MozReview-Commit-ID: 6ByuW2Q33Vf

diff --git a/devtools/client/animationinspector/test/browser_animation_timeline_iterationStart.js b/devtools/client/animationinspector/test/browser_animation_timeline_iterationStart.js
--- a/devtools/client/animationinspector/test/browser_animation_timeline_iterationStart.js
+++ b/devtools/client/animationinspector/test/browser_animation_timeline_iterationStart.js
@@ -43,26 +43,46 @@ function checkAnimationTooltip(el, {iter
   }).replace(".", "\\.");
   let iterationStartString = iterationStart.toString().replace(".", "\\.");
 
   let regex = new RegExp("Iteration start: " + iterationStartString +
                          " \\(" + iterationStartTimeString + "s\\)");
   ok(title.match(regex), "The tooltip shows the expected iteration start");
 }
 
-function checkProgressAtStartingTime(el, { iterationStart }) {
+function checkProgressAtStartingTime(el, { delay, iterationStart }) {
   info("Check the progress of starting time");
   const groupEls = el.querySelectorAll("svg g");
   groupEls.forEach(groupEl => {
     const pathEl = groupEl.querySelector(".iteration-path");
     const pathSegList = pathEl.pathSegList;
     const pathSeg = pathSegList.getItem(1);
     const progress = pathSeg.y;
     is(progress, iterationStart % 1,
        `The progress at starting point should be ${ iterationStart % 1 }`);
+
+    if (delay) {
+      const delayPathEl = groupEl.querySelector(".delay-path");
+      const delayPathSegList = delayPathEl.pathSegList;
+      const delayStartingPathSeg = delayPathSegList.getItem(1);
+      const delayEndingPathSeg =
+        delayPathSegList.getItem(delayPathSegList.numberOfItems - 2);
+      const startingX = 0;
+      const endingX = delay;
+      is(delayStartingPathSeg.x, startingX,
+         `The x of starting point should be ${ startingX }`);
+      is(delayStartingPathSeg.y, progress,
+         "The y of starting point should be same to starting point of iteration-path "
+         + progress);
+      is(delayEndingPathSeg.x, endingX,
+         `The x of ending point should be ${ endingX }`);
+      is(delayStartingPathSeg.y, progress,
+         "The y of ending point should be same to starting point of iteration-path "
+         + progress);
+    }
   });
 }
 
 function checkKeyframeOffset(timeBlockEl, frameEl, {iterationStart}) {
   info("Check that the first keyframe is offset correctly");
 
   let start = getKeyframeOffset(frameEl);
   is(start, 0, "The frame offset for iteration start");
diff --git a/devtools/client/animationinspector/test/browser_animation_timeline_shows_delay.js b/devtools/client/animationinspector/test/browser_animation_timeline_shows_delay.js
--- a/devtools/client/animationinspector/test/browser_animation_timeline_shows_delay.js
+++ b/devtools/client/animationinspector/test/browser_animation_timeline_shows_delay.js
@@ -74,26 +74,36 @@ function checkPath(animationEl, state) {
 
     // Check delay path coordinates.
     const pathSegList = delayPathEl.pathSegList;
     const startingPathSeg = pathSegList.getItem(0);
     const endingPathSeg = pathSegList.getItem(pathSegList.numberOfItems - 2);
     if (state.delay < 0) {
       ok(delayPathEl.classList.contains("negative"),
          "The delay path should have 'negative' class");
+      const expectedY = 0;
       const startingX = state.delay;
       const endingX = 0;
       is(startingPathSeg.x, startingX,
          `The x of starting point should be ${ startingX }`);
+      is(startingPathSeg.y, expectedY,
+         `The y of starting point should be ${ expectedY }`);
       is(endingPathSeg.x, endingX,
          `The x of ending point should be ${ endingX }`);
+      is(endingPathSeg.y, expectedY,
+         `The y of ending point should be ${ expectedY }`);
     } else {
       ok(!delayPathEl.classList.contains("negative"),
          "The delay path should not have 'negative' class");
+      const expectedY = 0;
       const startingX = 0;
       const endingX = state.delay;
       is(startingPathSeg.x, startingX,
          `The x of starting point should be ${ startingX }`);
+      is(startingPathSeg.y, expectedY,
+         `The y of starting point should be ${ expectedY }`);
       is(endingPathSeg.x, endingX,
          `The x of ending point should be ${ endingX }`);
+      is(endingPathSeg.y, expectedY,
+         `The y of ending point should be ${ expectedY }`);
     }
   });
 }
diff --git a/devtools/client/animationinspector/test/doc_script_animation.html b/devtools/client/animationinspector/test/doc_script_animation.html
--- a/devtools/client/animationinspector/test/doc_script_animation.html
+++ b/devtools/client/animationinspector/test/doc_script_animation.html
@@ -15,22 +15,29 @@
     background: green;
   }
 
   #target3 {
     width: 50px;
     height: 50px;
     background: blue;
   }
+
+  #target4 {
+    width: 50px;
+    height: 50px;
+    background: pink;
+  }
   </style>
 </head>
 <body>
   <div id="target1"></div>
   <div id="target2"></div>
   <div id="target3"></div>
+  <div id="target4"></div>
 
   <script>
     /* globals KeyframeEffect, Animation */
     "use strict";
 
     let animations = [{
       id: "target1",
       frames: [{ opacity: 0, offset: 0 }, { opacity: 1, offset: 1 }],
@@ -53,19 +60,31 @@
       id: "target3",
       frames: [{ opacity: 0, offset: 0 }, { opacity: 1, offset: 1 }],
       timing: {
         duration: 100,
         iterations: 1.5,
         iterationStart: 2.5,
         fill: "both"
       }
+    }, {
+      id: "target4",
+      frames: [{ transform: "translate(0px)", offset: 0 },
+               { transform: "translate(100px)", offset: 1 }],
+      timing: {
+        duration: 100,
+        iterations: 2,
+        iterationStart: 0.5,
+        delay: 10,
+        fill: "both"
+      }
     }];
 
     for (let {id, frames, timing} of animations) {
       let effect = new KeyframeEffect(document.getElementById(id),
                                       frames, timing);
       let animation = new Animation(effect, document.timeline);
       animation.play();
+      animation.pause();
     }
   </script>
 </body>
 </html>
