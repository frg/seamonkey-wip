# HG changeset patch
# User Mauricio Collares <mauricio@collares.org>
# Date 1517495052 21600
# Node ID ace32c1626126c245acf42ecc16acbf7e1981d48
# Parent  1ff4ddfe4206dcc82a685c42ccfdffc4517fa6be
servo: Merge #19924 - style: Move border-image-repeat outside of mako (from emilio:bim); r=emilio

style: Move border-image-repeat outside of mako.

This is a rebased / nitpick-addressed / bug-fixed version of #19021, and with a commit from #19668 renaming the two `RepeatKeyword`s to different names.

Source-Repo: https://github.com/servo/servo
Source-Revision: d222c9501b69af324ed1bdc04adefd3b17fabb59

diff --git a/servo/components/style/properties/gecko.mako.rs b/servo/components/style/properties/gecko.mako.rs
--- a/servo/components/style/properties/gecko.mako.rs
+++ b/servo/components/style/properties/gecko.mako.rs
@@ -1768,45 +1768,45 @@ fn static_assert() {
 
     <% impl_style_sides("border_image_outset") %>
 
     <%
     border_image_repeat_keywords = ["Stretch", "Repeat", "Round", "Space"]
     %>
 
     pub fn set_border_image_repeat(&mut self, v: longhands::border_image_repeat::computed_value::T) {
-        use properties::longhands::border_image_repeat::computed_value::RepeatKeyword;
+        use values::specified::border::BorderImageRepeatKeyword;
         use gecko_bindings::structs::StyleBorderImageRepeat;
 
         % for i, side in enumerate(["H", "V"]):
             self.gecko.mBorderImageRepeat${side} = match v.${i} {
                 % for keyword in border_image_repeat_keywords:
-                RepeatKeyword::${keyword} => StyleBorderImageRepeat::${keyword},
+                BorderImageRepeatKeyword::${keyword} => StyleBorderImageRepeat::${keyword},
                 % endfor
             };
         % endfor
     }
 
     pub fn copy_border_image_repeat_from(&mut self, other: &Self) {
         self.gecko.mBorderImageRepeatH = other.gecko.mBorderImageRepeatH;
         self.gecko.mBorderImageRepeatV = other.gecko.mBorderImageRepeatV;
     }
 
     pub fn reset_border_image_repeat(&mut self, other: &Self) {
         self.copy_border_image_repeat_from(other)
     }
 
     pub fn clone_border_image_repeat(&self) -> longhands::border_image_repeat::computed_value::T {
-        use properties::longhands::border_image_repeat::computed_value::RepeatKeyword;
+        use values::specified::border::BorderImageRepeatKeyword;
         use gecko_bindings::structs::StyleBorderImageRepeat;
 
         % for side in ["H", "V"]:
         let servo_${side.lower()} = match self.gecko.mBorderImageRepeat${side} {
             % for keyword in border_image_repeat_keywords:
-            StyleBorderImageRepeat::${keyword} => RepeatKeyword::${keyword},
+            StyleBorderImageRepeat::${keyword} => BorderImageRepeatKeyword::${keyword},
             % endfor
         };
         % endfor
         longhands::border_image_repeat::computed_value::T(servo_h, servo_v)
     }
 
     <% impl_style_sides("border_image_width") %>
 
@@ -3875,48 +3875,48 @@ fn static_assert() {
             image_layers_field = "mImage"
             struct_name = "Background"
         else:
             image_layers_field = "mMask"
             struct_name = "SVG"
     %>
 
     <%self:simple_image_array_property name="repeat" shorthand="${shorthand}" field_name="mRepeat">
-        use values::specified::background::RepeatKeyword;
+        use values::specified::background::BackgroundRepeatKeyword;
         use gecko_bindings::structs::nsStyleImageLayers_Repeat;
         use gecko_bindings::structs::StyleImageLayerRepeat;
 
-        fn to_ns(repeat: RepeatKeyword) -> StyleImageLayerRepeat {
+        fn to_ns(repeat: BackgroundRepeatKeyword) -> StyleImageLayerRepeat {
             match repeat {
-                RepeatKeyword::Repeat => StyleImageLayerRepeat::Repeat,
-                RepeatKeyword::Space => StyleImageLayerRepeat::Space,
-                RepeatKeyword::Round => StyleImageLayerRepeat::Round,
-                RepeatKeyword::NoRepeat => StyleImageLayerRepeat::NoRepeat,
+                BackgroundRepeatKeyword::Repeat => StyleImageLayerRepeat::Repeat,
+                BackgroundRepeatKeyword::Space => StyleImageLayerRepeat::Space,
+                BackgroundRepeatKeyword::Round => StyleImageLayerRepeat::Round,
+                BackgroundRepeatKeyword::NoRepeat => StyleImageLayerRepeat::NoRepeat,
             }
         }
 
         let repeat_x = to_ns(servo.0);
         let repeat_y = to_ns(servo.1);
         nsStyleImageLayers_Repeat {
               mXRepeat: repeat_x,
               mYRepeat: repeat_y,
         }
     </%self:simple_image_array_property>
 
     pub fn clone_${shorthand}_repeat(&self) -> longhands::${shorthand}_repeat::computed_value::T {
         use properties::longhands::${shorthand}_repeat::single_value::computed_value::T;
-        use values::specified::background::RepeatKeyword;
+        use values::specified::background::BackgroundRepeatKeyword;
         use gecko_bindings::structs::StyleImageLayerRepeat;
 
-        fn to_servo(repeat: StyleImageLayerRepeat) -> RepeatKeyword {
+        fn to_servo(repeat: StyleImageLayerRepeat) -> BackgroundRepeatKeyword {
             match repeat {
-                StyleImageLayerRepeat::Repeat => RepeatKeyword::Repeat,
-                StyleImageLayerRepeat::Space => RepeatKeyword::Space,
-                StyleImageLayerRepeat::Round => RepeatKeyword::Round,
-                StyleImageLayerRepeat::NoRepeat => RepeatKeyword::NoRepeat,
+                StyleImageLayerRepeat::Repeat => BackgroundRepeatKeyword::Repeat,
+                StyleImageLayerRepeat::Space => BackgroundRepeatKeyword::Space,
+                StyleImageLayerRepeat::Round => BackgroundRepeatKeyword::Round,
+                StyleImageLayerRepeat::NoRepeat => BackgroundRepeatKeyword::NoRepeat,
                 _ => panic!("Found unexpected value in style struct for ${shorthand}_repeat property"),
             }
         }
 
         longhands::${shorthand}_repeat::computed_value::T (
             self.gecko.${image_layers_field}.mLayers.iter()
                 .take(self.gecko.${image_layers_field}.mRepeatCount as usize)
                 .map(|ref layer| {
diff --git a/servo/components/style/properties/longhand/border.mako.rs b/servo/components/style/properties/longhand/border.mako.rs
--- a/servo/components/style/properties/longhand/border.mako.rs
+++ b/servo/components/style/properties/longhand/border.mako.rs
@@ -219,70 +219,25 @@
     parse_method="parse_non_negative",
     initial_value="computed::LengthOrNumberRect::all(computed::LengthOrNumber::zero())",
     initial_specified_value="specified::LengthOrNumberRect::all(specified::LengthOrNumber::zero())",
     spec="https://drafts.csswg.org/css-backgrounds/#border-image-outset",
     animation_value_type="discrete",
     flags="APPLIES_TO_FIRST_LETTER",
     boxed=True)}
 
-<%helpers:longhand name="border-image-repeat" animation_value_type="discrete"
-                   flags="APPLIES_TO_FIRST_LETTER"
-                   spec="https://drafts.csswg.org/css-backgrounds/#border-image-repeat">
-    pub mod computed_value {
-        pub use super::RepeatKeyword;
-
-        #[derive(Clone, Debug, MallocSizeOf, PartialEq, ToCss)]
-        pub struct T(pub RepeatKeyword, pub RepeatKeyword);
-    }
-
-    #[derive(Clone, Debug, MallocSizeOf, PartialEq, ToCss)]
-    pub struct SpecifiedValue(pub RepeatKeyword,
-                              pub Option<RepeatKeyword>);
-
-    #[cfg_attr(feature = "servo", derive(Deserialize, Serialize))]
-    #[derive(Clone, Copy, Debug, Eq, MallocSizeOf, Parse, PartialEq, ToCss)]
-    pub enum RepeatKeyword {
-        Stretch,
-        Repeat,
-        Round,
-        Space,
-    }
-
-    #[inline]
-    pub fn get_initial_value() -> computed_value::T {
-        computed_value::T(RepeatKeyword::Stretch, RepeatKeyword::Stretch)
-    }
-
-    #[inline]
-    pub fn get_initial_specified_value() -> SpecifiedValue {
-        SpecifiedValue(RepeatKeyword::Stretch, None)
-    }
-
-    impl ToComputedValue for SpecifiedValue {
-        type ComputedValue = computed_value::T;
-
-        #[inline]
-        fn to_computed_value(&self, _context: &Context) -> computed_value::T {
-            computed_value::T(self.0, self.1.unwrap_or(self.0))
-        }
-        #[inline]
-        fn from_computed_value(computed: &computed_value::T) -> Self {
-            SpecifiedValue(computed.0, Some(computed.1))
-        }
-    }
-
-    pub fn parse<'i, 't>(_context: &ParserContext, input: &mut Parser<'i, 't>)
-                         -> Result<SpecifiedValue, ParseError<'i>> {
-        let first = RepeatKeyword::parse(input)?;
-        let second = input.try(RepeatKeyword::parse).ok();
-
-        Ok(SpecifiedValue(first, second))
-    }
-</%helpers:longhand>
+${helpers.predefined_type(
+    "border-image-repeat",
+    "BorderImageRepeat",
+    "computed::BorderImageRepeat::stretch()",
+    initial_specified_value="specified::BorderImageRepeat::stretch()",
+    animation_value_type="discrete",
+    spec="https://drafts.csswg.org/css-backgrounds/#the-border-image-repeat",
+    flags="APPLIES_TO_FIRST_LETTER",
+)}
 
 ${helpers.predefined_type("border-image-width", "BorderImageWidth",
     initial_value="computed::BorderImageWidth::all(computed::BorderImageSideWidth::one())",
     initial_specified_value="specified::BorderImageWidth::all(specified::BorderImageSideWidth::one())",
     spec="https://drafts.csswg.org/css-backgrounds/#border-image-width",
     animation_value_type="discrete",
     flags="APPLIES_TO_FIRST_LETTER",
     boxed=True)}
diff --git a/servo/components/style/values/computed/background.rs b/servo/components/style/values/computed/background.rs
--- a/servo/components/style/values/computed/background.rs
+++ b/servo/components/style/values/computed/background.rs
@@ -7,17 +7,17 @@
 use properties::animated_properties::RepeatableListAnimatable;
 use properties::longhands::background_size::computed_value::T as BackgroundSizeList;
 use std::fmt::{self, Write};
 use style_traits::{CssWriter, ToCss};
 use values::animated::{ToAnimatedValue, ToAnimatedZero};
 use values::computed::{Context, ToComputedValue};
 use values::computed::length::LengthOrPercentageOrAuto;
 use values::generics::background::BackgroundSize as GenericBackgroundSize;
-use values::specified::background::{BackgroundRepeat as SpecifiedBackgroundRepeat, RepeatKeyword};
+use values::specified::background::{BackgroundRepeat as SpecifiedBackgroundRepeat, BackgroundRepeatKeyword};
 
 /// A computed value for the `background-size` property.
 pub type BackgroundSize = GenericBackgroundSize<LengthOrPercentageOrAuto>;
 
 impl BackgroundSize {
     /// Returns `auto auto`.
     pub fn auto() -> Self {
         GenericBackgroundSize::Explicit {
@@ -81,33 +81,37 @@ impl ToAnimatedValue for BackgroundSizeL
         BackgroundSizeList(ToAnimatedValue::from_animated_value(animated.0))
     }
 }
 
 /// The computed value of the `background-repeat` property:
 ///
 /// https://drafts.csswg.org/css-backgrounds/#the-background-repeat
 #[derive(Clone, Debug, MallocSizeOf, PartialEq)]
-pub struct BackgroundRepeat(pub RepeatKeyword, pub RepeatKeyword);
+pub struct BackgroundRepeat(pub BackgroundRepeatKeyword, pub BackgroundRepeatKeyword);
 
 impl BackgroundRepeat {
     /// Returns the `repeat repeat` value.
     pub fn repeat() -> Self {
-        BackgroundRepeat(RepeatKeyword::Repeat, RepeatKeyword::Repeat)
+        BackgroundRepeat(BackgroundRepeatKeyword::Repeat, BackgroundRepeatKeyword::Repeat)
     }
 }
 
 impl ToCss for BackgroundRepeat {
     fn to_css<W>(&self, dest: &mut CssWriter<W>) -> fmt::Result
     where
         W: Write,
     {
         match (self.0, self.1) {
-            (RepeatKeyword::Repeat, RepeatKeyword::NoRepeat) => dest.write_str("repeat-x"),
-            (RepeatKeyword::NoRepeat, RepeatKeyword::Repeat) => dest.write_str("repeat-y"),
+            (BackgroundRepeatKeyword::Repeat, BackgroundRepeatKeyword::NoRepeat) => {
+                dest.write_str("repeat-x")
+            },
+            (BackgroundRepeatKeyword::NoRepeat, BackgroundRepeatKeyword::Repeat) => {
+                dest.write_str("repeat-y")
+            },
             (horizontal, vertical) => {
                 horizontal.to_css(dest)?;
                 if horizontal != vertical {
                     dest.write_str(" ")?;
                     vertical.to_css(dest)?;
                 }
                 Ok(())
             },
@@ -117,36 +121,36 @@ impl ToCss for BackgroundRepeat {
 
 impl ToComputedValue for SpecifiedBackgroundRepeat {
     type ComputedValue = BackgroundRepeat;
 
     #[inline]
     fn to_computed_value(&self, _: &Context) -> Self::ComputedValue {
         match *self {
             SpecifiedBackgroundRepeat::RepeatX => {
-                BackgroundRepeat(RepeatKeyword::Repeat, RepeatKeyword::NoRepeat)
+                BackgroundRepeat(BackgroundRepeatKeyword::Repeat, BackgroundRepeatKeyword::NoRepeat)
             }
             SpecifiedBackgroundRepeat::RepeatY => {
-                BackgroundRepeat(RepeatKeyword::NoRepeat, RepeatKeyword::Repeat)
+                BackgroundRepeat(BackgroundRepeatKeyword::NoRepeat, BackgroundRepeatKeyword::Repeat)
             }
             SpecifiedBackgroundRepeat::Keywords(horizontal, vertical) => {
                 BackgroundRepeat(horizontal, vertical.unwrap_or(horizontal))
             }
         }
     }
 
     #[inline]
     fn from_computed_value(computed: &Self::ComputedValue) -> Self {
         // FIXME(emilio): Why can't this just be:
         //   SpecifiedBackgroundRepeat::Keywords(computed.0, computed.1)
         match (computed.0, computed.1) {
-            (RepeatKeyword::Repeat, RepeatKeyword::NoRepeat) => {
+            (BackgroundRepeatKeyword::Repeat, BackgroundRepeatKeyword::NoRepeat) => {
                 SpecifiedBackgroundRepeat::RepeatX
             }
-            (RepeatKeyword::NoRepeat, RepeatKeyword::Repeat) => {
+            (BackgroundRepeatKeyword::NoRepeat, BackgroundRepeatKeyword::Repeat) => {
                 SpecifiedBackgroundRepeat::RepeatY
             }
             (horizontal, vertical) => {
                 SpecifiedBackgroundRepeat::Keywords(horizontal, Some(vertical))
             }
         }
     }
 }
diff --git a/servo/components/style/values/computed/border.rs b/servo/components/style/values/computed/border.rs
--- a/servo/components/style/values/computed/border.rs
+++ b/servo/components/style/values/computed/border.rs
@@ -1,25 +1,28 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 //! Computed types for CSS values related to borders.
 
 use app_units::Au;
+use std::fmt::{self, Write};
+use style_traits::{ToCss, CssWriter};
 use values::animated::ToAnimatedZero;
-use values::computed::{Number, NumberOrPercentage};
+use values::computed::{Context, Number, NumberOrPercentage, ToComputedValue};
 use values::computed::length::{LengthOrPercentage, NonNegativeLength};
 use values::generics::border::BorderCornerRadius as GenericBorderCornerRadius;
 use values::generics::border::BorderImageSideWidth as GenericBorderImageSideWidth;
 use values::generics::border::BorderImageSlice as GenericBorderImageSlice;
 use values::generics::border::BorderRadius as GenericBorderRadius;
 use values::generics::border::BorderSpacing as GenericBorderSpacing;
 use values::generics::rect::Rect;
 use values::generics::size::Size;
+use values::specified::border::{BorderImageRepeat as SpecifiedBorderImageRepeat, BorderImageRepeatKeyword};
 
 /// A computed value for the `border-image-width` property.
 pub type BorderImageWidth = Rect<BorderImageSideWidth>;
 
 /// A computed value for a single side of a `border-image-width` property.
 pub type BorderImageSideWidth = GenericBorderImageSideWidth<LengthOrPercentage, Number>;
 
 /// A computed value for the `border-image-slice` property.
@@ -76,8 +79,49 @@ impl ToAnimatedZero for BorderSpacing {
 
 impl ToAnimatedZero for BorderCornerRadius {
     #[inline]
     fn to_animated_zero(&self) -> Result<Self, ()> {
         // FIXME(nox): Why?
         Err(())
     }
 }
+
+/// The computed value of the `border-image-repeat` property:
+///
+/// https://drafts.csswg.org/css-backgrounds/#the-border-image-repeat
+#[derive(Clone, Debug, MallocSizeOf, PartialEq)]
+pub struct BorderImageRepeat(pub BorderImageRepeatKeyword, pub BorderImageRepeatKeyword);
+
+impl BorderImageRepeat {
+    /// Returns the `stretch` value.
+    pub fn stretch() -> Self {
+        BorderImageRepeat(BorderImageRepeatKeyword::Stretch, BorderImageRepeatKeyword::Stretch)
+    }
+}
+
+impl ToCss for BorderImageRepeat {
+    fn to_css<W>(&self, dest: &mut CssWriter<W>) -> fmt::Result
+    where
+        W: Write,
+    {
+        self.0.to_css(dest)?;
+        if self.0 != self.1 {
+            dest.write_str(" ")?;
+            self.1.to_css(dest)?;
+        }
+        Ok(())
+    }
+}
+
+impl ToComputedValue for SpecifiedBorderImageRepeat {
+    type ComputedValue = BorderImageRepeat;
+
+    #[inline]
+    fn to_computed_value(&self, _: &Context) -> Self::ComputedValue {
+        BorderImageRepeat(self.0, self.1.unwrap_or(self.0))
+    }
+
+    #[inline]
+    fn from_computed_value(computed: &Self::ComputedValue) -> Self {
+        SpecifiedBorderImageRepeat(computed.0, Some(computed.1))
+    }
+}
diff --git a/servo/components/style/values/computed/mod.rs b/servo/components/style/values/computed/mod.rs
--- a/servo/components/style/values/computed/mod.rs
+++ b/servo/components/style/values/computed/mod.rs
@@ -32,17 +32,17 @@ use super::specified;
 pub use app_units::Au;
 pub use properties::animated_properties::TransitionProperty;
 #[cfg(feature = "gecko")]
 pub use self::align::{AlignItems, AlignContent, JustifyContent, SelfAlignment, JustifyItems};
 #[cfg(feature = "gecko")]
 pub use self::align::{AlignSelf, JustifySelf};
 pub use self::angle::Angle;
 pub use self::background::{BackgroundSize, BackgroundRepeat};
-pub use self::border::{BorderImageSlice, BorderImageWidth, BorderImageSideWidth};
+pub use self::border::{BorderImageRepeat, BorderImageSlice, BorderImageWidth, BorderImageSideWidth};
 pub use self::border::{BorderRadius, BorderCornerRadius, BorderSpacing};
 pub use self::font::{FontSize, FontSizeAdjust, FontSynthesis, FontWeight, FontVariantAlternates};
 pub use self::font::{FontFamily, FontLanguageOverride, FontVariationSettings, FontVariantEastAsian};
 pub use self::font::{FontVariantLigatures, FontVariantNumeric, FontFeatureSettings};
 pub use self::font::{MozScriptLevel, MozScriptMinSize, MozScriptSizeMultiplier, XTextZoom, XLang};
 pub use self::box_::{AnimationIterationCount, AnimationName, Display, OverscrollBehavior, Contain};
 pub use self::box_::{OverflowClipBox, ScrollSnapType, TouchAction, VerticalAlign, WillChange};
 pub use self::color::{Color, ColorPropertyValue, RGBAColor};
diff --git a/servo/components/style/values/specified/background.rs b/servo/components/style/values/specified/background.rs
--- a/servo/components/style/values/specified/background.rs
+++ b/servo/components/style/values/specified/background.rs
@@ -43,62 +43,62 @@ impl BackgroundSize {
             height: LengthOrPercentageOrAuto::Auto,
         }
     }
 }
 
 /// One of the keywords for `background-repeat`.
 #[derive(Clone, Copy, Debug, Eq, MallocSizeOf, Parse, PartialEq, ToComputedValue, ToCss)]
 #[allow(missing_docs)]
-pub enum RepeatKeyword {
+pub enum BackgroundRepeatKeyword {
     Repeat,
     Space,
     Round,
     NoRepeat,
 }
 
 /// The specified value for the `background-repeat` property.
 ///
 /// https://drafts.csswg.org/css-backgrounds/#the-background-repeat
 #[derive(Clone, Debug, MallocSizeOf, PartialEq, ToCss)]
 pub enum BackgroundRepeat {
     /// `repeat-x`
     RepeatX,
     /// `repeat-y`
     RepeatY,
     /// `[repeat | space | round | no-repeat]{1,2}`
-    Keywords(RepeatKeyword, Option<RepeatKeyword>),
+    Keywords(BackgroundRepeatKeyword, Option<BackgroundRepeatKeyword>),
 }
 
 impl BackgroundRepeat {
     /// Returns the `repeat` value.
     #[inline]
     pub fn repeat() -> Self {
-        BackgroundRepeat::Keywords(RepeatKeyword::Repeat, None)
+        BackgroundRepeat::Keywords(BackgroundRepeatKeyword::Repeat, None)
     }
 }
 
 impl Parse for BackgroundRepeat {
     fn parse<'i, 't>(
         _context: &ParserContext,
         input: &mut Parser<'i, 't>,
     ) -> Result<Self, ParseError<'i>> {
         let ident = input.expect_ident_cloned()?;
 
         match_ignore_ascii_case! { &ident,
             "repeat-x" => return Ok(BackgroundRepeat::RepeatX),
             "repeat-y" => return Ok(BackgroundRepeat::RepeatY),
             _ => {},
         }
 
-        let horizontal = match RepeatKeyword::from_ident(&ident) {
+        let horizontal = match BackgroundRepeatKeyword::from_ident(&ident) {
             Ok(h) => h,
             Err(()) => {
                 return Err(input.new_custom_error(
                     SelectorParseErrorKind::UnexpectedIdent(ident.clone())
                 ));
             }
         };
 
-        let vertical = input.try(RepeatKeyword::parse).ok();
+        let vertical = input.try(BackgroundRepeatKeyword::parse).ok();
         Ok(BackgroundRepeat::Keywords(horizontal, vertical))
     }
 }
diff --git a/servo/components/style/values/specified/border.rs b/servo/components/style/values/specified/border.rs
--- a/servo/components/style/values/specified/border.rs
+++ b/servo/components/style/values/specified/border.rs
@@ -166,8 +166,44 @@ impl Parse for BorderSpacing {
         context: &ParserContext,
         input: &mut Parser<'i, 't>
     ) -> Result<Self, ParseError<'i>> {
         Size::parse_with(context, input, |context, input| {
             Length::parse_non_negative_quirky(context, input, AllowQuirks::Yes).map(From::from)
         }).map(GenericBorderSpacing)
     }
 }
+
+/// A single border-image-repeat keyword.
+#[allow(missing_docs)]
+#[cfg_attr(feature = "servo", derive(Deserialize, Serialize))]
+#[derive(Clone, Copy, Debug, Eq, MallocSizeOf, Parse, PartialEq, ToCss)]
+pub enum BorderImageRepeatKeyword {
+    Stretch,
+    Repeat,
+    Round,
+    Space,
+}
+
+/// The specified value for the `border-image-repeat` property.
+///
+/// https://drafts.csswg.org/css-backgrounds/#the-border-image-repeat
+#[derive(Clone, Debug, MallocSizeOf, PartialEq, ToCss)]
+pub struct BorderImageRepeat(pub BorderImageRepeatKeyword, pub Option<BorderImageRepeatKeyword>);
+
+impl BorderImageRepeat {
+    /// Returns the `stretch` value.
+    #[inline]
+    pub fn stretch() -> Self {
+        BorderImageRepeat(BorderImageRepeatKeyword::Stretch, None)
+    }
+}
+
+impl Parse for BorderImageRepeat {
+    fn parse<'i, 't>(
+        _context: &ParserContext,
+        input: &mut Parser<'i, 't>,
+    ) -> Result<Self, ParseError<'i>> {
+        let horizontal = BorderImageRepeatKeyword::parse(input)?;
+        let vertical = input.try(BorderImageRepeatKeyword::parse).ok();
+        Ok(BorderImageRepeat(horizontal, vertical))
+    }
+}
diff --git a/servo/components/style/values/specified/mod.rs b/servo/components/style/values/specified/mod.rs
--- a/servo/components/style/values/specified/mod.rs
+++ b/servo/components/style/values/specified/mod.rs
@@ -27,17 +27,17 @@ use values::specified::calc::CalcNode;
 pub use properties::animated_properties::TransitionProperty;
 pub use self::angle::Angle;
 #[cfg(feature = "gecko")]
 pub use self::align::{AlignContent, JustifyContent, AlignItems, ContentDistribution, SelfAlignment, JustifyItems};
 #[cfg(feature = "gecko")]
 pub use self::align::{AlignSelf, JustifySelf};
 pub use self::background::{BackgroundRepeat, BackgroundSize};
 pub use self::border::{BorderCornerRadius, BorderImageSlice, BorderImageWidth};
-pub use self::border::{BorderImageSideWidth, BorderRadius, BorderSideWidth, BorderSpacing};
+pub use self::border::{BorderImageRepeat, BorderImageSideWidth, BorderRadius, BorderSideWidth, BorderSpacing};
 pub use self::font::{FontSize, FontSizeAdjust, FontSynthesis, FontWeight, FontVariantAlternates};
 pub use self::font::{FontFamily, FontLanguageOverride, FontVariationSettings, FontVariantEastAsian};
 pub use self::font::{FontVariantLigatures, FontVariantNumeric, FontFeatureSettings};
 pub use self::font::{MozScriptLevel, MozScriptMinSize, MozScriptSizeMultiplier, XTextZoom, XLang};
 pub use self::box_::{AnimationIterationCount, AnimationName, Display, OverscrollBehavior, Contain};
 pub use self::box_::{OverflowClipBox, ScrollSnapType, TouchAction, VerticalAlign, WillChange};
 pub use self::color::{Color, ColorPropertyValue, RGBAColor};
 pub use self::effects::{BoxShadow, Filter, SimpleShadow};
