# HG changeset patch
# User Julian Descottes <jdescottes@mozilla.com>
# Date 1515083145 -3600
# Node ID 357cf743b5821101c7458431563917d7db509be7
# Parent  2755d43a3c81c3d0fc6e4f0ff47d94d678b7919b
Bug 1272774 - allow listTabs to return favicon data from PlacesUtils;r=ochameau

MozReview-Commit-ID: 8bkn3mG6YkL

diff --git a/devtools/client/aboutdebugging/components/tabs/Panel.js b/devtools/client/aboutdebugging/components/tabs/Panel.js
--- a/devtools/client/aboutdebugging/components/tabs/Panel.js
+++ b/devtools/client/aboutdebugging/components/tabs/Panel.js
@@ -46,39 +46,32 @@ class TabsPanel extends Component {
     this.update();
   }
 
   componentWillUnmount() {
     let { client } = this.props;
     client.removeListener("tabListChanged", this.update);
   }
 
-  update() {
-    this.props.client.mainRoot.listTabs().then(({ tabs }) => {
-      // Filter out closed tabs (represented as `null`).
-      tabs = tabs.filter(tab => !!tab);
-      tabs.forEach(tab => {
-        // FIXME Also try to fetch low-res favicon. But we should use actor
-        // support for this to get the high-res one (bug 1061654).
-        let url = new URL(tab.url);
-        if (url.protocol.startsWith("http")) {
-          let prePath = url.origin;
-          let idx = url.pathname.lastIndexOf("/");
-          if (idx === -1) {
-            prePath += url.pathname;
-          } else {
-            prePath += url.pathname.substr(0, idx);
-          }
-          tab.icon = prePath + "/favicon.ico";
-        } else {
-          tab.icon = "chrome://devtools/skin/images/globe.svg";
-        }
-      });
-      this.setState({ tabs });
-    });
+  async update() {
+    let { tabs } = await this.props.client.mainRoot.listTabs({ favicons: true });
+
+    // Filter out closed tabs (represented as `null`).
+    tabs = tabs.filter(tab => !!tab);
+
+    for (let tab of tabs) {
+      if (tab.favicon) {
+        let base64Favicon = btoa(String.fromCharCode.apply(String, tab.favicon));
+        tab.icon = "data:image/png;base64," + base64Favicon;
+      } else {
+        tab.icon = "chrome://devtools/skin/images/globe.svg";
+      }
+    }
+
+    this.setState({ tabs });
   }
 
   render() {
     let { client, connect, id } = this.props;
     let { tabs } = this.state;
 
     return dom.div({
       id: id + "-panel",
diff --git a/devtools/client/aboutdebugging/test/browser_tabs.js b/devtools/client/aboutdebugging/test/browser_tabs.js
--- a/devtools/client/aboutdebugging/test/browser_tabs.js
+++ b/devtools/client/aboutdebugging/test/browser_tabs.js
@@ -24,16 +24,33 @@ add_task(function* () {
   info("Wait for the tab to appear in the list with the correct name");
   let container = yield waitUntilTabContainer("foo", document);
 
   info("Wait until the title to update");
   yield waitUntil(() => {
     return container.querySelector(".target-name").title === TAB_URL;
   }, 100);
 
+  let icon = container.querySelector(".target-icon");
+  ok(icon && icon.src, "Tab icon found and src attribute is not empty");
+
+  info("Check if the tab icon is a valid image");
+  yield new Promise(r => {
+    let image = new Image();
+    image.onload = () => {
+      ok(true, "Favicon is not a broken image");
+      r();
+    };
+    image.onerror = () => {
+      ok(false, "Favicon is a broken image");
+      r();
+    };
+    image.src = icon.src;
+  });
+
   // Finally, close the tab
   yield removeTab(newTab);
 
   info("Wait until the tab container is removed");
   yield waitUntil(() => !getTabContainer("foo", document), 100);
 
   // Check that the tab disappeared from the UI
   names = [...tabsElement.querySelectorAll("#tabs .target-name")];
diff --git a/devtools/server/actors/root.js b/devtools/server/actors/root.js
--- a/devtools/server/actors/root.js
+++ b/devtools/server/actors/root.js
@@ -281,17 +281,17 @@ RootActor.prototype = {
    * the next listTabs request.
    *
    * ⚠ WARNING ⚠ This can be a very expensive operation, especially if there are many
    * open tabs.  It will cause us to visit every tab, load a frame script, start a
    * debugger server, and read some data.  With lazy tab support (bug 906076), this
    * would trigger any lazy tabs to be loaded, greatly increasing resource usage.  Avoid
    * this method whenever possible.
    */
-  onListTabs: async function () {
+  onListTabs: async function (request) {
     let tabList = this._parameters.tabList;
     if (!tabList) {
       return { from: this.actorID, error: "noTabs",
                message: "This root actor has no browser tabs." };
     }
 
     // Now that a client has requested the list of tabs, we reattach the onListChanged
     // listener in order to be notified if the list of tabs changes again in the future.
@@ -300,17 +300,18 @@ RootActor.prototype = {
     // Walk the tab list, accumulating the array of tab actors for the reply, and moving
     // all the actors to a new ActorPool. We'll replace the old tab actor pool with the
     // one we build here, thus retiring any actors that didn't get listed again, and
     // preparing any new actors to receive packets.
     let newActorPool = new ActorPool(this.conn);
     let tabActorList = [];
     let selected;
 
-    let tabActors = await tabList.getList();
+    let options = request.options || {};
+    let tabActors = await tabList.getList(options);
     for (let tabActor of tabActors) {
       if (tabActor.exited) {
         // Tab actor may have exited while we were gathering the list.
         continue;
       }
       if (tabActor.selected) {
         selected = tabActorList.length;
       }
diff --git a/devtools/server/actors/webbrowser.js b/devtools/server/actors/webbrowser.js
--- a/devtools/server/actors/webbrowser.js
+++ b/devtools/server/actors/webbrowser.js
@@ -15,16 +15,17 @@ var DevToolsUtils = require("devtools/sh
 
 loader.lazyRequireGetter(this, "RootActor", "devtools/server/actors/root", true);
 loader.lazyRequireGetter(this, "BrowserAddonActor", "devtools/server/actors/addon", true);
 loader.lazyRequireGetter(this, "WebExtensionParentActor", "devtools/server/actors/webextension-parent", true);
 loader.lazyRequireGetter(this, "WorkerActorList", "devtools/server/actors/worker-list", true);
 loader.lazyRequireGetter(this, "ServiceWorkerRegistrationActorList", "devtools/server/actors/worker-list", true);
 loader.lazyRequireGetter(this, "ProcessActorList", "devtools/server/actors/process", true);
 loader.lazyImporter(this, "AddonManager", "resource://gre/modules/AddonManager.jsm");
+loader.lazyImporter(this, "PlacesUtils", "resource://gre/modules/PlacesUtils.jsm");
 
 /**
  * Browser-specific actors.
  */
 
 /**
  * Yield all windows of type |windowType|, from the oldest window to the
  * youngest, using nsIWindowMediator::getEnumerator. We're usually
@@ -251,17 +252,17 @@ BrowserTabList.prototype._getChildren = 
   return gBrowser.browsers.filter(browser => {
     // Filter tabs that are closing. listTabs calls made right after TabClose
     // events still list tabs in process of being closed.
     let tab = gBrowser.getTabForBrowser(browser);
     return !tab.closing;
   });
 };
 
-BrowserTabList.prototype.getList = function () {
+BrowserTabList.prototype.getList = function (browserActorOptions) {
   let topXULWindow = Services.wm.getMostRecentWindow(
     DebuggerServer.chromeWindowType);
   let selectedBrowser = null;
   if (topXULWindow) {
     selectedBrowser = this._getSelectedBrowser(topXULWindow);
   }
 
   // As a sanity check, make sure all the actors presently in our map get
@@ -274,17 +275,17 @@ BrowserTabList.prototype.getList = funct
   // the actors. Thus, the sequence yielded is always a snapshot of the
   // actors that were live when we began the iteration.
 
   let actorPromises = [];
 
   for (let browser of this._getBrowsers()) {
     let selected = browser === selectedBrowser;
     actorPromises.push(
-      this._getActorForBrowser(browser)
+      this._getActorForBrowser(browser, browserActorOptions)
           .then(actor => {
             // Set the 'selected' properties on all actors correctly.
             actor.selected = selected;
             return actor;
           }, e => {
             if (e.error === "tabDestroyed") {
               // Return null if a tab was destroyed while retrieving the tab list.
               return null;
@@ -304,25 +305,28 @@ BrowserTabList.prototype.getList = funct
   this._checkListening();
 
   return promise.all(actorPromises).then(values => {
     // Filter out null values if we received a tabDestroyed error.
     return values.filter(value => value != null);
   });
 };
 
-BrowserTabList.prototype._getActorForBrowser = function (browser) {
+/**
+ * @param browserActorOptions see options argument of BrowserTabActor constructor.
+ */
+BrowserTabList.prototype._getActorForBrowser = function (browser, browserActorOptions) {
   // Do we have an existing actor for this browser? If not, create one.
   let actor = this._actorByBrowser.get(browser);
   if (actor) {
     this._foundCount++;
-    return actor.update();
+    return actor.update(browserActorOptions);
   }
 
-  actor = new BrowserTabActor(this._connection, browser);
+  actor = new BrowserTabActor(this._connection, browser, browserActorOptions);
   this._actorByBrowser.set(browser, actor);
   this._checkListening();
   return actor.connect();
 };
 
 BrowserTabList.prototype.getTab = function ({ outerWindowID, tabId }) {
   if (typeof outerWindowID == "number") {
     // First look for in-process frames with this ID
@@ -690,78 +694,110 @@ exports.BrowserTabList = BrowserTabList;
 /**
  * Creates a tab actor for handling requests to a single browser frame.
  * Both <xul:browser> and <iframe mozbrowser> are supported.
  * This actor is a shim that connects to a ContentActor in a remote browser process.
  * All RDP packets get forwarded using the message manager.
  *
  * @param connection The main RDP connection.
  * @param browser <xul:browser> or <iframe mozbrowser> element to connect to.
+ * @param options
+ *        - {Boolean} favicons: true if the form should include the favicon for the tab.
  */
-function BrowserTabActor(connection, browser) {
+function BrowserTabActor(connection, browser, options = {}) {
   this._conn = connection;
   this._browser = browser;
   this._form = null;
   this.exited = false;
+  this.options = options;
 }
 
 BrowserTabActor.prototype = {
-  connect() {
+  async connect() {
     let onDestroy = () => {
       if (this._deferredUpdate) {
         // Reject the update promise if the tab was destroyed while requesting an update
         this._deferredUpdate.reject({
           error: "tabDestroyed",
           message: "Tab destroyed while performing a BrowserTabActor update"
         });
       }
       this.exit();
     };
     let connect = DebuggerServer.connectToChild(this._conn, this._browser, onDestroy);
-    return connect.then(form => {
-      this._form = form;
-      return this;
-    });
+    let form = await connect;
+
+    this._form = form;
+    if (this.options.favicons) {
+      this._form.favicon = await this.getFaviconData();
+    }
+
+    return this;
   },
 
   get _tabbrowser() {
     if (this._browser && typeof this._browser.getTabBrowser == "function") {
       return this._browser.getTabBrowser();
     }
     return null;
   },
 
   get _mm() {
     // Get messageManager from XUL browser (which might be a specialized tunnel for RDM)
     // or else fallback to asking the frameLoader itself.
     return this._browser.messageManager ||
            this._browser.frameLoader.messageManager;
   },
 
-  update() {
+  async getFaviconData() {
+    try {
+      let { data } = await PlacesUtils.promiseFaviconData(this._form.url);
+      return data;
+    } catch (e) {
+      // Favicon unavailable for this url.
+      return null;
+    }
+  },
+
+  /**
+   * @param {Object} options
+   *        See BrowserTabActor constructor.
+   */
+  async update(options = {}) {
+    // Update the BrowserTabActor options.
+    this.options = options;
+
     // If the child happens to be crashed/close/detach, it won't have _form set,
     // so only request form update if some code is still listening on the other
     // side.
-    if (!this.exited) {
-      this._deferredUpdate = defer();
+    if (this.exited) {
+      return this.connect();
+    }
+
+    let form = await new Promise(resolve => {
       let onFormUpdate = msg => {
         // There may be more than just one childtab.js up and running
         if (this._form.actor != msg.json.actor) {
           return;
         }
         this._mm.removeMessageListener("debug:form", onFormUpdate);
-        this._form = msg.json;
-        this._deferredUpdate.resolve(this);
+
+        resolve(msg.json);
       };
+
       this._mm.addMessageListener("debug:form", onFormUpdate);
       this._mm.sendAsyncMessage("debug:form");
-      return this._deferredUpdate.promise;
+    });
+
+    this._form = form;
+    if (this.options.favicons) {
+      this._form.favicon = await this.getFaviconData();
     }
 
-    return this.connect();
+    return this;
   },
 
   /**
    * If we don't have a title from the content side because it's a zombie tab, try to find
    * it on the chrome side.
    */
   get title() {
     // On Fennec, we can check the session store data for zombie tabs
@@ -804,16 +840,17 @@ BrowserTabActor.prototype = {
     // restored) are a good example.  In such cases, try to look up values for these
     // fields using other data in the parent process.
     if (!form.title) {
       form.title = this.title;
     }
     if (!form.url) {
       form.url = this.url;
     }
+
     return form;
   },
 
   exit() {
     this._browser = null;
     this._form = null;
     this.exited = true;
   },
diff --git a/devtools/shared/client/debugger-client.js b/devtools/shared/client/debugger-client.js
--- a/devtools/shared/client/debugger-client.js
+++ b/devtools/shared/client/debugger-client.js
@@ -310,18 +310,18 @@ DebuggerClient.prototype = {
 
     return deferred.promise;
   },
 
   /*
    * This function exists only to preserve DebuggerClient's interface;
    * new code should say 'client.mainRoot.listTabs()'.
    */
-  listTabs: function (onResponse) {
-    return this.mainRoot.listTabs(onResponse);
+  listTabs: function (options, onResponse) {
+    return this.mainRoot.listTabs(options, onResponse);
   },
 
   /*
    * This function exists only to preserve DebuggerClient's interface;
    * new code should say 'client.mainRoot.listAddons()'.
    */
   listAddons: function (onResponse) {
     return this.mainRoot.listAddons(onResponse);
diff --git a/devtools/shared/client/root-client.js b/devtools/shared/client/root-client.js
--- a/devtools/shared/client/root-client.js
+++ b/devtools/shared/client/root-client.js
@@ -1,16 +1,16 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 "use strict";
 
 const { Ci } = require("chrome");
-const {DebuggerClient} = require("devtools/shared/client/debugger-client");
+const { arg, DebuggerClient } = require("devtools/shared/client/debugger-client");
 
 /**
  * A RootClient object represents a root actor on the server. Each
  * DebuggerClient keeps a RootClient instance representing the root actor
  * for the initial connection; DebuggerClient's 'listTabs' and
  * 'listChildProcesses' methods forward to that root actor.
  *
  * @param client object
@@ -43,20 +43,23 @@ RootClient.prototype = {
    * browser.  This can replace usages of `listTabs` that only wanted the global actors
    * and didn't actually care about tabs.
    */
   getRoot: DebuggerClient.requester({ type: "getRoot" }),
 
    /**
    * List the open tabs.
    *
+   * @param object options
+   *        Optional flags for listTabs:
+   *        - boolean favicons: return favicon data
    * @param function onResponse
    *        Called with the response packet.
    */
-  listTabs: DebuggerClient.requester({ type: "listTabs" }),
+  listTabs: DebuggerClient.requester({ type: "listTabs", options: arg(0) }),
 
   /**
    * List the installed addons.
    *
    * @param function onResponse
    *        Called with the response packet.
    */
   listAddons: DebuggerClient.requester({ type: "listAddons" }),
