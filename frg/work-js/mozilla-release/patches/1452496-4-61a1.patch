# HG changeset patch
# User Christoph Kerschbaumer <ckerschb@christophkerschbaumer.com>
# Date 1523530428 -7200
# Node ID 87b552f9c09b68a255eeb84c5c554384b0c1231a
# Parent  a51575f3764c5fafe21d2c3579386b110f48b1ac
Bug 1452496: Test for discarding same-site cookies using inline scripts in cross origin context. r=mgoodwin

diff --git a/dom/security/test/general/file_same_site_cookies_from_script.sjs b/dom/security/test/general/file_same_site_cookies_from_script.sjs
new file mode 100644
--- /dev/null
+++ b/dom/security/test/general/file_same_site_cookies_from_script.sjs
@@ -0,0 +1,49 @@
+// Custom *.sjs file specifically for the needs of Bug 1452496
+
+const SET_COOKIE_FRAME = `
+  <!DOCTYPE html>
+  <html>
+  <head>
+    <title>Bug 1452496 - Do not allow same-site cookies in cross site context</title>
+  </head>
+  <body>
+    <script type="application/javascript">
+      document.cookie = "myKey=sameSiteCookieInlineScript;SameSite=strict";
+    </script>
+  </body>
+  </html>`;
+
+const GET_COOKIE_FRAME = `
+  <!DOCTYPE html>
+  <html>
+  <head>
+    <title>Bug 1452496 - Do not allow same-site cookies in cross site context</title>
+  </head>
+  <body>
+    <script type="application/javascript">
+      let cookie = document.cookie;
+      // now reset the cookie for the next test
+      document.cookie = "myKey=;" + "expires=Thu, 01 Jan 1970 00:00:00 GMT";
+      window.parent.postMessage({result: cookie}, 'http://mochi.test:8888');
+    </script>
+  </body>
+  </html>`;
+
+function handleRequest(request, response)
+{
+  // avoid confusing cache behaviors
+  response.setHeader("Cache-Control", "no-cache", false);
+
+  if (request.queryString === "setSameSiteCookieUsingInlineScript") {
+    response.write(SET_COOKIE_FRAME);
+    return;
+  }
+
+  if (request.queryString === "getCookieFrame") {
+    response.write(GET_COOKIE_FRAME);
+    return;
+  }
+
+  // we should never get here, but just in case return something unexpected
+  response.write("D'oh");
+}
diff --git a/dom/security/test/general/mochitest.ini b/dom/security/test/general/mochitest.ini
--- a/dom/security/test/general/mochitest.ini
+++ b/dom/security/test/general/mochitest.ini
@@ -5,23 +5,25 @@ support-files =
   file_block_script_wrong_mime_server.sjs
   file_block_toplevel_data_navigation.html
   file_block_toplevel_data_navigation2.html
   file_block_toplevel_data_navigation3.html
   file_block_toplevel_data_redirect.sjs
   file_same_site_cookies_subrequest.sjs
   file_same_site_cookies_toplevel_nav.sjs
   file_same_site_cookies_cross_origin_context.sjs
+  file_same_site_cookies_from_script.sjs
 
 [test_contentpolicytype_targeted_link_iframe.html]
 [test_nosniff.html]
 [test_block_script_wrong_mime.html]
 [test_block_toplevel_data_navigation.html]
 skip-if = toolkit == 'android' # intermittent failure
 [test_block_toplevel_data_img_navigation.html]
 skip-if = toolkit == 'android' # intermittent failure
 [test_allow_opening_data_pdf.html]
 skip-if = toolkit == 'android'
 [test_allow_opening_data_json.html]
 skip-if = toolkit == 'android'
 [test_same_site_cookies_subrequest.html]
 [test_same_site_cookies_toplevel_nav.html]
 [test_same_site_cookies_cross_origin_context.html]
+[test_same_site_cookies_from_script.html]
diff --git a/dom/security/test/general/test_same_site_cookies_from_script.html b/dom/security/test/general/test_same_site_cookies_from_script.html
new file mode 100644
--- /dev/null
+++ b/dom/security/test/general/test_same_site_cookies_from_script.html
@@ -0,0 +1,86 @@
+<!DOCTYPE HTML>
+<html>
+<head>
+  <title>Bug 1452496 - Do not allow same-site cookies in cross site context</title>
+  <script type="text/javascript" src="/tests/SimpleTest/SimpleTest.js"></script>
+  <link rel="stylesheet" type="text/css" href="/tests/SimpleTest/test.css" />
+</head>
+<body>
+
+<iframe id="setCookieFrame"></iframe>
+<iframe id="getCookieFrame"></iframe>
+
+<script class="testbody" type="text/javascript">
+
+/*
+ * Description of the test:
+ * 1) We load an iframe which tries to set a same site cookie using an
+ *    inline script in top-level context of http://mochi.test.
+ * 2) We load an iframe from http://example.com and check if the cookie
+ *    is available.
+ * 3) We observe that:
+ *    (a) same site cookie is available in same origin context.
+ *    (a) same site cookie has been discarded in a cross origin context.
+ */
+
+SimpleTest.waitForExplicitFinish();
+
+const SAME_ORIGIN = "http://mochi.test:8888/";
+const CROSS_ORIGIN = "http://example.com/";
+const PATH = "tests/dom/security/test/general/file_same_site_cookies_from_script.sjs";
+
+let curTest = 0;
+
+var tests = [
+  {
+    description: "same-site cookie inline script within same-site context",
+    setCookieSrc: SAME_ORIGIN + PATH + "?setSameSiteCookieUsingInlineScript",
+    getCookieSrc: SAME_ORIGIN + PATH + "?getCookieFrame",
+    result: "myKey=sameSiteCookieInlineScript",
+  },
+  {
+    description: "same-site cookie inline script within cross-site context",
+    setCookieSrc: CROSS_ORIGIN + PATH + "?setSameSiteCookieUsingInlineScript",
+    getCookieSrc: CROSS_ORIGIN + PATH + "?getCookieFrame",
+    result: "", // same-site cookie should be discarded in cross site context
+  },
+];
+
+window.addEventListener("message", receiveMessage);
+function receiveMessage(event) {
+  is(event.data.result, tests[curTest].result, tests[curTest].description);
+  curTest += 1;
+
+  // lets see if we ran all the tests
+  if (curTest == tests.length) {
+    window.removeEventListener("message", receiveMessage);
+    SimpleTest.finish();
+    return;
+  }
+  // otherwise it's time to run the next test
+  setCookieAndInitTest();
+}
+
+function setupQueryResultAndRunTest() {
+  let getCookieFrame = document.getElementById("getCookieFrame");
+  getCookieFrame.src = tests[curTest].getCookieSrc;
+}
+
+function setCookieAndInitTest() {
+  var cookieFrame = document.getElementById("setCookieFrame");
+  setCookieFrame.onload = function() {
+    ok(true, "trying to set cookie for test (" + tests[curTest].description + ")");
+    setupQueryResultAndRunTest();
+  }
+  setCookieFrame.onerror = function() {
+    ok(false, "could not load image for test (" + tests[curTest].description + ")");
+  }
+  cookieFrame.src =  tests[curTest].setCookieSrc;
+}
+
+// fire up the test
+setCookieAndInitTest();
+
+</script>
+</body>
+</html>
