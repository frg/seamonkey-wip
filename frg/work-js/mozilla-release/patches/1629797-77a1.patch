# HG changeset patch
# User Ricky Stewart <rstewart@mozilla.com>
# Date 1587482293 0
# Node ID 2033db20b4418b9bfecdc1ceac766789d98150a4
# Parent  7d9533027552971c02c21580cffa91ff2aaf52f6
Bug 1629797 - Don't rely on subprocess's default encoding in configure, and instead interpret the binary output ourselves. r=glandium

This matches up to what we were doing before the Python 3 switch. Subprocess.Popen's default encoding doesn't necessarily match up to the system encoding as reported by mozbuild.util.system_encoding, and pre-Python 3.6 the constructor doesn't have `encoding` or `errors` parameters for no apparent reason. In the meantime, do the decoding manually.

Differential Revision: https://phabricator.services.mozilla.com/D71193

diff --git a/build/moz.configure/util.configure b/build/moz.configure/util.configure
--- a/build/moz.configure/util.configure
+++ b/build/moz.configure/util.configure
@@ -29,20 +29,26 @@ def configure_error(message):
 @imports(_from='mozbuild.util', _import='system_encoding')
 def get_cmd_output(*args, **kwargs):
     log.debug('Executing: `%s`', quote(*args))
     proc = subprocess.Popen(
         args, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
         # On Python 2 on Windows, close_fds prevents the process from inheriting
         # stdout/stderr. Elsewhere, it simply prevents it from inheriting extra
         # file descriptors, which is what we want.
-        close_fds=os.name != 'nt', universal_newlines=True, **kwargs)
+        close_fds=os.name != 'nt', **kwargs)
     stdout, stderr = proc.communicate()
-    stdout = six.ensure_text(stdout, encoding=system_encoding, errors='replace')
-    stderr = six.ensure_text(stderr, encoding=system_encoding, errors='replace')
+    # Normally we would set the `encoding` and `errors` arguments in the
+    # constructor to subprocess.Popen, but those arguments were added in 3.6
+    # and we need to support back to 3.5, so instead we need to do this
+    # nonsense.
+    stdout = six.ensure_text(stdout, encoding=system_encoding,
+                             errors='replace').replace('\r\n', '\n')
+    stderr = six.ensure_text(stderr, encoding=system_encoding,
+                             errors='replace').replace('\r\n', '\n')
     return proc.wait(), stdout, stderr
 
 
 # A wrapper to obtain a process' output that returns the output generated
 # by running the given command if it exits normally, and streams that
 # output to log.debug and calls die or the given error callback if it
 # does not.
 @imports(_from='mozbuild.configure.util', _import='LineIO')

