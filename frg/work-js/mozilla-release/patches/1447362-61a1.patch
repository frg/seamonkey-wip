# HG changeset patch
# User Andre Bargull <andre.bargull@gmail.com>
# Date 1521642097 25200
# Node ID f56e2332d673cdb4c68ce924a9102a9039f0056f
# Parent  970118ae6d9eb3414ab14ddbc19bbf82c273800f
Bug 1447362: Avoid Atomize calls when binding a bound function. r=jandem

diff --git a/js/src/jit/CodeGenerator.cpp b/js/src/jit/CodeGenerator.cpp
--- a/js/src/jit/CodeGenerator.cpp
+++ b/js/src/jit/CodeGenerator.cpp
@@ -13344,19 +13344,19 @@ CodeGenerator::visitFinishBoundFunctionI
                       Imm32(JSFunction::INTERPRETED_LAZY |
                             JSFunction::RESOLVED_NAME |
                             JSFunction::RESOLVED_LENGTH),
                       slowPath);
 
     Label notBoundTarget, loadName;
     masm.branchTest32(Assembler::Zero, temp1, Imm32(JSFunction::BOUND_FUN), &notBoundTarget);
     {
-        // Target's name atom doesn't contain the bound function prefix, so we
-        // need to call into the VM.
-        masm.branchTest32(Assembler::Zero, temp1,
+        // Call into the VM if the target's name atom contains the bound
+        // function prefix.
+        masm.branchTest32(Assembler::NonZero, temp1,
                           Imm32(JSFunction::HAS_BOUND_FUNCTION_NAME_PREFIX), slowPath);
 
         // We also take the slow path when target's length isn't an int32.
         masm.branchTestInt32(Assembler::NotEqual, Address(target, boundLengthOffset), slowPath);
 
         // Bound functions reuse HAS_GUESSED_ATOM for HAS_BOUND_FUNCTION_NAME_PREFIX,
         // so skip the guessed atom check below.
         static_assert(JSFunction::HAS_BOUND_FUNCTION_NAME_PREFIX == JSFunction::HAS_GUESSED_ATOM,
diff --git a/js/src/vm/JSFunction.cpp b/js/src/vm/JSFunction.cpp
--- a/js/src/vm/JSFunction.cpp
+++ b/js/src/vm/JSFunction.cpp
@@ -55,16 +55,17 @@
 #include "vm/JSScript-inl.h"
 #include "vm/Stack-inl.h"
 
 using namespace js;
 using namespace js::gc;
 using namespace js::frontend;
 
 using mozilla::ArrayLength;
+using mozilla::CheckedInt;
 using mozilla::Maybe;
 using mozilla::Some;
 
 static bool
 fun_enumerate(JSContext* cx, HandleObject obj)
 {
     MOZ_ASSERT(obj->is<JSFunction>());
 
@@ -532,17 +533,17 @@ fun_resolve(JSContext* cx, HandleObject 
                 return true;
 
             if (!JSFunction::getUnresolvedLength(cx, fun, &v))
                 return false;
         } else {
             if (fun->hasResolvedName())
                 return true;
 
-            RootedAtom name(cx);
+            RootedString name(cx);
             if (!JSFunction::getUnresolvedName(cx, fun, &name))
                 return false;
 
             // Don't define an own .name property for unnamed functions.
             if (!name)
                 return true;
 
             v.setString(name);
@@ -1027,22 +1028,25 @@ js::FunctionToString(JSContext* cx, Hand
                 if (!out.append('*'))
                     return nullptr;
             }
         }
 
         if (fun->explicitName()) {
             if (!out.append(' '))
                 return nullptr;
-            if (fun->isBoundFunction() && !fun->hasBoundFunctionNamePrefix()) {
-                if (!out.append(cx->names().boundWithSpace))
+
+            if (fun->isBoundFunction()) {
+                JSLinearString* boundName = JSFunction::getBoundFunctionName(cx, fun);
+                if (!boundName || !out.append(boundName))
+                    return nullptr;
+            } else {
+                if (!out.append(fun->explicitName()))
                     return nullptr;
             }
-            if (!out.append(fun->explicitName()))
-                return nullptr;
         }
 
         if (fun->isInterpreted() &&
             (!fun->isSelfHostedBuiltin() ||
              fun->infallibleIsDefaultClassConstructor(cx)))
         {
             // Default class constructors should always haveSource except;
             //
@@ -1316,51 +1320,100 @@ JSFunction::getUnresolvedLength(JSContex
     uint16_t length;
     if (!JSFunction::getLength(cx, fun, &length))
         return false;
 
     v.setInt32(length);
     return true;
 }
 
-/* static */ bool
-JSFunction::getUnresolvedName(JSContext* cx, HandleFunction fun, MutableHandleAtom v)
+JSAtom*
+JSFunction::infallibleGetUnresolvedName(JSContext* cx)
 {
-    MOZ_ASSERT(!IsInternalFunctionObject(*fun));
-    MOZ_ASSERT(!fun->hasResolvedName());
+    MOZ_ASSERT(!IsInternalFunctionObject(*this));
+    MOZ_ASSERT(!hasResolvedName());
+
+    if (JSAtom* name = explicitOrInferredName())
+        return name;
+
+    // Unnamed class expressions should not get a .name property at all.
+    if (isClassConstructor())
+        return nullptr;
 
-    JSAtom* name = fun->explicitOrInferredName();
-    if (fun->isClassConstructor()) {
-        // Unnamed class expressions should not get a .name property at all.
-        if (name)
-            v.set(name);
+    return cx->names().empty;
+}
+
+/* static */ bool
+JSFunction::getUnresolvedName(JSContext* cx, HandleFunction fun, MutableHandleString v)
+{
+    if (fun->isBoundFunction()) {
+        JSLinearString* name = JSFunction::getBoundFunctionName(cx, fun);
+        if (!name)
+            return false;
+
+        v.set(name);
         return true;
     }
 
-    if (fun->isBoundFunction() && !fun->hasBoundFunctionNamePrefix()) {
-        // Bound functions are never unnamed.
-        MOZ_ASSERT(name);
+    v.set(fun->infallibleGetUnresolvedName(cx));
+    return true;
+}
 
-        if (name->length() > 0) {
-            StringBuffer sb(cx);
-            if (!sb.append(cx->names().boundWithSpace) || !sb.append(name))
-                return false;
+/* static */ JSLinearString*
+JSFunction::getBoundFunctionName(JSContext* cx, HandleFunction fun)
+{
+    MOZ_ASSERT(fun->isBoundFunction());
+    JSAtom* name = fun->explicitName();
+
+    // Bound functions are never unnamed.
+    MOZ_ASSERT(name);
 
-            name = sb.finishAtom();
-            if (!name)
-                return false;
-        } else {
-            name = cx->names().boundWithSpace;
-        }
+    // If the bound function prefix is present, return the name as is.
+    if (fun->hasBoundFunctionNamePrefix())
+        return name;
 
-        fun->setPrefixedBoundFunctionName(name);
+    // Otherwise return "bound " * (number of bound function targets) + name.
+    size_t boundTargets = 0;
+    for (JSFunction* boundFn = fun; boundFn->isBoundFunction(); ) {
+        boundTargets++;
+
+        JSObject* target = boundFn->getBoundFunctionTarget();
+        if (!target->is<JSFunction>())
+            break;
+        boundFn = &target->as<JSFunction>();
     }
 
-    v.set(name != nullptr ? name : cx->names().empty);
-    return true;
+    // |function /*unnamed*/ (){...}.bind()| is a common case, handle it here.
+    if (name->empty() && boundTargets == 1)
+        return cx->names().boundWithSpace;
+
+    static constexpr char boundWithSpaceChars[] = "bound ";
+    static constexpr size_t boundWithSpaceCharsLength =
+        ArrayLength(boundWithSpaceChars) - 1; // No trailing '\0'.
+    MOZ_ASSERT(StringEqualsAscii(cx->names().boundWithSpace, boundWithSpaceChars));
+
+    StringBuffer sb(cx);
+    if (name->hasTwoByteChars() && !sb.ensureTwoByteChars())
+        return nullptr;
+
+    CheckedInt<size_t> len(boundTargets);
+    len *= boundWithSpaceCharsLength;
+    len += name->length();
+    if (!len.isValid()) {
+        ReportAllocationOverflow(cx);
+        return nullptr;
+    }
+    if (!sb.reserve(len.value()))
+        return nullptr;
+
+    while (boundTargets--)
+        sb.infallibleAppend(boundWithSpaceChars, boundWithSpaceCharsLength);
+    sb.infallibleAppendSubstring(name, 0, name->length());
+
+    return sb.finishString();
 }
 
 static const js::Value&
 BoundFunctionEnvironmentSlotValue(const JSFunction* fun, uint32_t slotIndex)
 {
     MOZ_ASSERT(fun->isBoundFunction());
     MOZ_ASSERT(fun->environment()->is<CallObject>());
     CallObject* callObject = &fun->environment()->as<CallObject>();
@@ -1396,16 +1449,28 @@ JSFunction::getBoundFunctionArgument(uns
 }
 
 size_t
 JSFunction::getBoundFunctionArgumentCount() const
 {
     return GetBoundFunctionArguments(this)->length();
 }
 
+static JSAtom*
+AppendBoundFunctionPrefix(JSContext* cx, JSString* str)
+{
+    static constexpr char boundWithSpaceChars[] = "bound ";
+    MOZ_ASSERT(StringEqualsAscii(cx->names().boundWithSpace, boundWithSpaceChars));
+
+    StringBuffer sb(cx);
+    if (!sb.append(boundWithSpaceChars) || !sb.append(str))
+        return nullptr;
+    return sb.finishAtom();
+}
+
 /* static */ bool
 JSFunction::finishBoundFunctionInit(JSContext* cx, HandleFunction bound, HandleObject targetObj,
                                     int32_t argCount)
 {
     bound->setIsBoundFunction();
     MOZ_ASSERT(bound->getBoundFunctionTarget() == targetObj);
 
     // 9.4.1.3 BoundFunctionCreate, steps 1, 3-5, 8-12 (Already performed).
@@ -1452,43 +1517,66 @@ JSFunction::finishBoundFunctionInit(JSCo
         }
 
         // 19.2.3.2 Function.prototype.bind, step 7 (implicit).
     }
 
     // 19.2.3.2 Function.prototype.bind, step 8.
     bound->setExtendedSlot(BOUND_FUN_LENGTH_SLOT, NumberValue(length));
 
+    MOZ_ASSERT(!bound->hasGuessedAtom());
+
     // Try to avoid invoking the resolve hook.
-    RootedAtom name(cx);
+    JSAtom* name = nullptr;
     if (targetObj->is<JSFunction>() && !targetObj->as<JSFunction>().hasResolvedName()) {
-        if (!JSFunction::getUnresolvedName(cx, targetObj.as<JSFunction>(), &name))
-            return false;
+        JSFunction* targetFn = &targetObj->as<JSFunction>();
+
+        // If the target is a bound function with a prefixed name, we can't
+        // lazily compute the full name in getBoundFunctionName(), therefore
+        // we need to append the bound function name prefix here.
+        if (targetFn->isBoundFunction() && targetFn->hasBoundFunctionNamePrefix()) {
+            name = AppendBoundFunctionPrefix(cx, targetFn->explicitName());
+            if (!name)
+                return false;
+            bound->setPrefixedBoundFunctionName(name);
+        } else {
+            name = targetFn->infallibleGetUnresolvedName(cx);
+            if (name)
+                bound->setAtom(name);
+        }
     }
 
     // 19.2.3.2 Function.prototype.bind, steps 9-11.
     if (!name) {
         // 19.2.3.2 Function.prototype.bind, step 9.
         RootedValue targetName(cx);
         if (!GetProperty(cx, targetObj, targetObj, cx->names().name, &targetName))
             return false;
 
         // 19.2.3.2 Function.prototype.bind, step 10.
-        if (targetName.isString() && !targetName.toString()->empty()) {
+        if (!targetName.isString())
+            targetName.setString(cx->names().empty);
+
+        // If the target itself is a bound function (with a resolved name), we
+        // can't compute the full name in getBoundFunctionName() based only on
+        // the number of bound target functions, therefore we need to store
+        // the complete prefixed name here.
+        if (targetObj->is<JSFunction>() && targetObj->as<JSFunction>().isBoundFunction()) {
+            name = AppendBoundFunctionPrefix(cx, targetName.toString());
+            if (!name)
+                return false;
+            bound->setPrefixedBoundFunctionName(name);
+        } else {
             name = AtomizeString(cx, targetName.toString());
             if (!name)
                 return false;
-        } else {
-            name = cx->names().empty;
+            bound->setAtom(name);
         }
     }
 
-    MOZ_ASSERT(!bound->hasGuessedAtom());
-    bound->setAtom(name);
-
     return true;
 }
 
 /* static */ bool
 JSFunction::createScriptForLazilyInterpretedFunction(JSContext* cx, HandleFunction fun)
 {
     MOZ_ASSERT(fun->isInterpretedLazy());
 
diff --git a/js/src/vm/JSFunction.h b/js/src/vm/JSFunction.h
--- a/js/src/vm/JSFunction.h
+++ b/js/src/vm/JSFunction.h
@@ -336,18 +336,22 @@ class JSFunction : public js::NativeObje
             lazyScript()->setAsyncKind(asyncKind);
         else
             nonLazyScript()->setAsyncKind(asyncKind);
     }
 
     static bool getUnresolvedLength(JSContext* cx, js::HandleFunction fun,
                                     js::MutableHandleValue v);
 
+    JSAtom* infallibleGetUnresolvedName(JSContext* cx);
+
     static bool getUnresolvedName(JSContext* cx, js::HandleFunction fun,
-                                  js::MutableHandleAtom v);
+                                  js::MutableHandleString v);
+
+    static JSLinearString* getBoundFunctionName(JSContext* cx, js::HandleFunction fun);
 
     JSAtom* explicitName() const {
         return (hasInferredName() || hasGuessedAtom()) ? nullptr : atom_.get();
     }
     JSAtom* explicitOrInferredName() const {
         return hasGuessedAtom() ? nullptr : atom_.get();
     }
 
