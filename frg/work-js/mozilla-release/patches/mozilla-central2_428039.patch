# HG changeset patch
# User Mike Shal <mshal@mozilla.com>
# Date 1530805739 14400
#      Thu Jul 05 11:48:59 2018 -0400
# Node ID f316cbb31c28d2b128b5d44255a982002db7bfb8
# Parent  262d541a474f4d8b5eef4b78353b2e4b721b6079
Bug 1473667 - Handle multiple GENERATED_FILES outputs in the faster make backend; r=gps

For files that come from the objdir in install manifests (eg: '!buildid.h'),
the FasterMake backend runs 'make -C dirname filename' to build the file
with the RecursiveMake backend before the install manifest is processed.
This works fine for GENERATED_FILES that produce a single target, but if
the GENERATED_FILES invocation produces multiple targets, the FasterMake
backend will run the command multiple times. Eg:

GENERATED_FILES += [('output1', 'output2')]
...
make -C dirname output1
make -C dirname output2

These invocations may be run in parallel, and would produce both output1
and output2 twice.

Instead we can track the GeneratedFile objects as they are emitted, and
only add the first output as a dependency on the install manifest. The
RecursiveMake backend is then only invoked once for the whole group of
GENERATED_FILES.

MozReview-Commit-ID: 6mvkHow2V2i

diff --git a/python/mozbuild/mozbuild/backend/fastermake.py b/python/mozbuild/mozbuild/backend/fastermake.py
--- a/python/mozbuild/mozbuild/backend/fastermake.py
+++ b/python/mozbuild/mozbuild/backend/fastermake.py
@@ -8,16 +8,17 @@ from mozbuild.backend.base import Partia
 from mozbuild.backend.common import CommonBackend
 from mozbuild.frontend.context import (
     ObjDirPath,
 )
 from mozbuild.frontend.data import (
     ChromeManifestEntry,
     FinalTargetPreprocessedFiles,
     FinalTargetFiles,
+    GeneratedFile,
     JARManifest,
     LocalizedFiles,
     LocalizedPreprocessedFiles,
     XPIDLFile,
 )
 from mozbuild.makeutil import Makefile
 from mozbuild.util import OrderedDefaultDict
 from mozpack.manifests import InstallManifest
@@ -32,16 +33,18 @@ class FasterMakeBackend(CommonBackend, P
 
         self._install_manifests = OrderedDefaultDict(InstallManifest)
 
         self._dependencies = OrderedDefaultDict(list)
         self._l10n_dependencies = OrderedDefaultDict(list)
 
         self._has_xpidl = False
 
+        self._generated_files_map = {}
+
     def _add_preprocess(self, obj, path, dest, target=None, **kwargs):
         if target is None:
             target = mozpath.basename(path)
         # This matches what PP_TARGETS do in config/rules.
         if target.endswith('.in'):
             target = target[:-3]
         if target.endswith('.css'):
             kwargs['marker'] = '%'
@@ -106,29 +109,45 @@ class FasterMakeBackend(CommonBackend, P
                                 target)
                     else:
                         self._install_manifests[obj.install_target].add_link(
                             src,
                             mozpath.join(path, f.target_basename)
                         )
                     if isinstance(f, ObjDirPath):
                         dep_target = 'install-%s' % obj.install_target
-                        self._dependencies[dep_target].append(
-                            mozpath.relpath(f.full_path,
-                                            self.environment.topobjdir))
+                        dep = mozpath.relpath(f.full_path, self.environment.topobjdir)
+                        if dep in self._generated_files_map:
+                            # Only the first output file is specified as a
+                            # dependency. If there are multiple output files
+                            # from a single GENERATED_FILES invocation that are
+                            # installed, we only want to run the command once.
+                            dep = self._generated_files_map[dep]
+                        self._dependencies[dep_target].append(dep)
 
         elif isinstance(obj, ChromeManifestEntry) and \
                 obj.install_target.startswith('dist/bin'):
             top_level = mozpath.join(obj.install_target, 'chrome.manifest')
             if obj.path != top_level:
                 entry = 'manifest %s' % mozpath.relpath(obj.path,
                                                         obj.install_target)
                 self._manifest_entries[top_level].add(entry)
             self._manifest_entries[obj.path].add(str(obj.entry))
 
+        elif isinstance(obj, GeneratedFile):
+            if obj.outputs:
+                first_output = mozpath.relpath(mozpath.join(obj.objdir, obj.outputs[0]), self.environment.topobjdir)
+                for o in obj.outputs[1:]:
+                    fullpath = mozpath.join(obj.objdir, o)
+                    self._generated_files_map[mozpath.relpath(fullpath, self.environment.topobjdir)] = first_output
+            # We don't actually handle GeneratedFiles, we just need to know if
+            # we can build multiple of them from a single make invocation in the
+            # faster backend.
+            return False
+
         elif isinstance(obj, XPIDLFile):
             self._has_xpidl = True
             # We're not actually handling XPIDL files.
             return False
 
         else:
             return False
 
