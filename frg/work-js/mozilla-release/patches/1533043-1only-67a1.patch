# HG changeset patch
# User Andrew Halberstadt <ahalberstadt@mozilla.com>
# Date 1552488697 0
# Node ID 203ed6d898c152b7c93c80fe52f01fa5b9141b8f
# Parent  82bf29782b7822387b72f46b4bb4743ef2ae0707
Bug 1533043 - [python-test] Add ability for individual tests to have pypi dependencies, r=davehunt

Sometimes tools install pypi at runtime via mach (e.g self.install_pip_package
/ self.install_pip_requirements). It's difficult to test these modules with
pytest because we usually won't be going through mach.

This gives tests the ability to depend on external pypi packages the same way
they might get installed when running via mach.

Note, I only added support for requirements.txt here because
python/mozbuild/mozbuild/virtualenv.py's 'install_pip_package' function is
completely busted with modern pip. And the pip used with |mach python-test| is
more modern than the one used with the regular build venv due to pipenv. We'll
need to fix this eventually, but that's another bug for another day.

Differential Revision: https://phabricator.services.mozilla.com/D22784

diff --git a/python/mach_commands.py b/python/mach_commands.py
--- a/python/mach_commands.py
+++ b/python/mach_commands.py
@@ -157,39 +157,42 @@ class MachCommands(MachCommandBase):
                 self.log(logging.INFO, 'python-test', {'line': line.rstrip()}, '{line}')
 
             if ret and not return_code:
                 self.log(logging.ERROR, 'python-test', {'test_path': test_path, 'ret': ret},
                          'Setting retcode to {ret} from {test_path}')
             return return_code or ret
 
         with ThreadPoolExecutor(max_workers=self.jobs) as executor:
-            futures = [executor.submit(self._run_python_test, test['path'])
+            futures = [executor.submit(self._run_python_test, test)
                        for test in parallel]
 
             try:
                 for future in as_completed(futures):
                     return_code = on_test_finished(future.result())
             except KeyboardInterrupt:
                 # Hack to force stop currently running threads.
                 # https://gist.github.com/clchiou/f2608cbe54403edb0b13
                 executor._threads.clear()
                 thread._threads_queues.clear()
                 raise
 
         for test in sequential:
-            return_code = on_test_finished(self._run_python_test(test['path']))
+            return_code = on_test_finished(self._run_python_test(test))
 
         self.log(logging.INFO, 'python-test', {'return_code': return_code},
                  'Return code from mach python-test: {return_code}')
         return return_code
 
-    def _run_python_test(self, test_path):
+    def _run_python_test(self, test):
         from mozprocess import ProcessHandler
 
+        if test.get('requirements'):
+            self.virtualenv_manager.install_pip_requirements(test['requirements'], quiet=True)
+
         output = []
 
         def _log(line):
             # Buffer messages if more than one worker to avoid interleaving
             if self.jobs > 1:
                 output.append(line)
             else:
                 self.log(logging.INFO, 'python-test', {'line': line.rstrip()}, '{line}')
@@ -204,29 +207,29 @@ class MachCommands(MachCommandBase):
                     file_displayed_test.append(True)
 
             # Hack to make sure treeherder highlights pytest failures
             if 'FAILED' in line.rsplit(' ', 1)[-1]:
                 line = line.replace('FAILED', 'TEST-UNEXPECTED-FAIL')
 
             _log(line)
 
-        _log(test_path)
-        cmd = [self.virtualenv_manager.python_path, test_path]
+        _log(test['path'])
+        cmd = [self.virtualenv_manager.python_path, test['path']]
         env = os.environ.copy()
         env[b'PYTHONDONTWRITEBYTECODE'] = b'1'
 
         proc = ProcessHandler(cmd, env=env, processOutputLine=_line_handler, storeOutput=False)
         proc.run()
 
         return_code = proc.wait()
 
         if not file_displayed_test:
             _log('TEST-UNEXPECTED-FAIL | No test output (missing mozunit.main() '
-                 'call?): {}'.format(test_path))
+                 'call?): {}'.format(test['path']))
 
         if self.verbose:
             if return_code != 0:
-                _log('Test failed: {}'.format(test_path))
+                _log('Test failed: {}'.format(test['path']))
             else:
-                _log('Test passed: {}'.format(test_path))
+                _log('Test passed: {}'.format(test['path']))
 
-        return output, return_code, test_path
+        return output, return_code, test['path']
