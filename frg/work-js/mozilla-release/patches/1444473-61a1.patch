# HG changeset patch
# User Nicolas B. Pierron <nicolas.b.pierron@gmail.com>
# Date 1521550453 0
#      Tue Mar 20 12:54:13 2018 +0000
# Node ID 814eccdff53e11792ade29f383275f45f3872196
# Parent  6e6cc524dbdfc4782d3da8a2321b98d9cfc424c2
Bug 1444473 - Spectre: Add Jit inline caches mitigation for values returned from C++. r=jandem

diff --git a/js/src/jit/BaselineCacheIRCompiler.cpp b/js/src/jit/BaselineCacheIRCompiler.cpp
--- a/js/src/jit/BaselineCacheIRCompiler.cpp
+++ b/js/src/jit/BaselineCacheIRCompiler.cpp
@@ -593,16 +593,18 @@ BaselineCacheIRCompiler::emitMegamorphic
         masm.callWithABI(JS_FUNC_TO_DATA_PTR(void*, (GetNativeDataProperty<false>)));
     masm.mov(ReturnReg, scratch2);
     masm.PopRegsInMask(volatileRegs);
 
     masm.loadTypedOrValue(Address(masm.getStackPointer(), 0), output);
     masm.adjustStack(sizeof(Value));
 
     masm.branchIfFalseBool(scratch2, failure->label());
+    if (JitOptions.spectreJitToCxxCalls)
+        masm.speculationBarrier();
     return true;
 }
 
 bool
 BaselineCacheIRCompiler::emitMegamorphicStoreSlot()
 {
     Register obj = allocator.useRegister(masm, reader.objOperandId());
     Address nameAddr = stubAddress(reader.stubOffset());
diff --git a/js/src/jit/CacheIRCompiler.cpp b/js/src/jit/CacheIRCompiler.cpp
--- a/js/src/jit/CacheIRCompiler.cpp
+++ b/js/src/jit/CacheIRCompiler.cpp
@@ -2821,16 +2821,18 @@ CacheIRCompiler::emitMegamorphicLoadSlot
 
     Label ok;
     uint32_t framePushed = masm.framePushed();
     masm.branchIfTrueBool(scratch, &ok);
     masm.adjustStack(sizeof(Value));
     masm.jump(failure->label());
 
     masm.bind(&ok);
+    if (JitOptions.spectreJitToCxxCalls)
+        masm.speculationBarrier();
     masm.setFramePushed(framePushed);
     masm.loadTypedOrValue(Address(masm.getStackPointer(), 0), output);
     masm.adjustStack(sizeof(Value));
     return true;
 }
 
 bool
 CacheIRCompiler::emitMegamorphicHasPropResult()
diff --git a/js/src/jit/IonCacheIRCompiler.cpp b/js/src/jit/IonCacheIRCompiler.cpp
--- a/js/src/jit/IonCacheIRCompiler.cpp
+++ b/js/src/jit/IonCacheIRCompiler.cpp
@@ -1018,16 +1018,18 @@ IonCacheIRCompiler::emitMegamorphicLoadS
         masm.callWithABI(JS_FUNC_TO_DATA_PTR(void*, (GetNativeDataProperty<false>)));
     masm.mov(ReturnReg, scratch2);
     masm.PopRegsInMask(volatileRegs);
 
     masm.loadTypedOrValue(Address(masm.getStackPointer(), 0), output);
     masm.adjustStack(sizeof(Value));
 
     masm.branchIfFalseBool(scratch2, failure->label());
+    if (JitOptions.spectreJitToCxxCalls)
+        masm.speculationBarrier();
     return true;
 }
 
 bool
 IonCacheIRCompiler::emitMegamorphicStoreSlot()
 {
     Register obj = allocator.useRegister(masm, reader.objOperandId());
     PropertyName* name = stringStubField(reader.stubOffset())->asAtom().asPropertyName();
@@ -1211,16 +1213,19 @@ IonCacheIRCompiler::emitCallNativeGetter
 
     // Test for failure.
     masm.branchIfFalseBool(ReturnReg, masm.exceptionLabel());
 
     // Load the outparam vp[0] into output register(s).
     Address outparam(masm.getStackPointer(), IonOOLNativeExitFrameLayout::offsetOfResult());
     masm.loadValue(outparam, output.valueReg());
 
+    if (JitOptions.spectreJitToCxxCalls)
+        masm.speculationBarrier();
+
     masm.adjustStack(IonOOLNativeExitFrameLayout::Size(0));
     return true;
 }
 
 bool
 IonCacheIRCompiler::emitCallProxyGetResult()
 {
     AutoSaveLiveRegisters save(*this);
@@ -1270,16 +1275,20 @@ IonCacheIRCompiler::emitCallProxyGetResu
 
     // Test for failure.
     masm.branchIfFalseBool(ReturnReg, masm.exceptionLabel());
 
     // Load the outparam vp[0] into output register(s).
     Address outparam(masm.getStackPointer(), IonOOLProxyExitFrameLayout::offsetOfResult());
     masm.loadValue(outparam, output.valueReg());
 
+    // Spectre mitigation in case of speculative execution within C++ code.
+    if (JitOptions.spectreJitToCxxCalls)
+        masm.speculationBarrier();
+
     // masm.leaveExitFrame & pop locals
     masm.adjustStack(IonOOLProxyExitFrameLayout::Size());
     return true;
 }
 
 typedef bool (*ProxyGetPropertyByValueFn)(JSContext*, HandleObject, HandleValue, MutableHandleValue);
 static const VMFunction ProxyGetPropertyByValueInfo =
     FunctionInfo<ProxyGetPropertyByValueFn>(ProxyGetPropertyByValue, "ProxyGetPropertyByValue");
