# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1588368115 0
#      Fri May 01 21:21:55 2020 +0000
# Node ID cb27cbe57e4d06b8260e0811b2d7b4017c83a486
# Parent  e63bf85520a1503a48e1669d7304336179660b7d
Bug 1634623 - Collect js configure arguments separately from old configure arguments. r=firefox-build-system-reviewers,rstewart

js subconfigure used to be invoked from old-configure, and the
transition out of old-configure preserved the code sharing.

prepare_configure_options currently collects all the old configure
arguments, separating js_option arguments from js_option environment
variables. This distinction is necessary for old configure, but not
for python configure, so we can now handle all the js_options
similarly.

Differential Revision: https://phabricator.services.mozilla.com/D73409

diff --git a/build/moz.configure/init.configure b/build/moz.configure/init.configure
--- a/build/moz.configure/init.configure
+++ b/build/moz.configure/init.configure
@@ -881,30 +881,30 @@ def host_os_kernel_major_version(host):
 
 
 set_config('HOST_MAJOR_VERSION', host_os_kernel_major_version)
 
 # Autoconf needs these set
 
 
 @depends(host)
-def host_for_old_configure(host):
+def host_for_sub_configure(host):
     return '--host=%s' % host.alias
 
 
-add_old_configure_arg(host_for_old_configure)
+add_old_configure_arg(host_for_sub_configure)
 
 
 @depends(target)
-def target_for_old_configure(target):
+def target_for_sub_configure(target):
     target_alias = target.alias
     return '--target=%s' % target_alias
 
 
-add_old_configure_arg(target_for_old_configure)
+add_old_configure_arg(target_for_sub_configure)
 
 
 # These variables are for compatibility with the current moz.builds and
 # old-configure. Eventually, we'll want to canonicalize better.
 @depends(target)
 def target_variables(target):
     if target.kernel == 'kFreeBSD':
         os_target = 'GNU/kFreeBSD'
@@ -1062,24 +1062,30 @@ def build_project(include_project_config
 set_config('MOZ_BUILD_APP', build_project)
 set_define('MOZ_BUILD_APP', build_project)
 add_old_configure_assignment('MOZ_BUILD_APP', build_project)
 
 
 # This is temporary until js/src/configure and configure are merged.
 # Use instead of option() in js/moz.configure and more generally, for
 # options that are shared between configure and js/src/configure.
+@dependable
+def js_configure_args():
+    return []
+
 @template
 def js_option(*args, **kwargs):
     opt = option(*args, **kwargs)
 
-    @depends(opt.option, build_project, when=kwargs.get('when'))
-    def js_option(value, build_project):
+    @depends(js_configure_args, opt.option, build_project, when=kwargs.get('when'))
+    def js_option(js_configure_args, value, build_project):
         if build_project != 'js':
-            return value.format(opt.option)
+            arg = value.format(opt.option)
+            js_configure_args.append(arg)
+            return arg
 
     add_old_configure_arg(js_option)
 
 
 # set RELEASE_OR_BETA and NIGHTLY_BUILD variables depending on the cycle we're in
 # The logic works like this:
 # - if we have "a1" in GRE_MILESTONE, we're building Nightly (define NIGHTLY_BUILD)
 # - otherwise, if we have "a" in GRE_MILESTONE, we're building Nightly or Aurora
diff --git a/js/sub.configure b/js/sub.configure
--- a/js/sub.configure
+++ b/js/sub.configure
@@ -1,27 +1,27 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
-@depends(check_build_environment, prepare_configure_options, prepare_mozconfig,
-         old_configure, old_configure_assignments, '--cache-file')
+@depends(host_for_sub_configure, target_for_sub_configure, check_build_environment,
+         js_configure_args, prepare_mozconfig, old_configure,
+         old_configure_assignments, '--cache-file')
 @imports('errno')
-@imports('itertools')
 @imports('logging')
 @imports('os')
 @imports('pickle')
 @imports('sys')
 @imports(_from='__main__', _import='config_status')
 @imports(_from='__builtin__', _import='OSError')
 @imports(_from='__builtin__', _import='open')
 @imports(_from='__builtin__', _import='object')
 @imports(_from='mozbuild.configure', _import='ConfigureSandbox')
 @imports(_from='mozbuild.configure.util', _import='ConfigureOutputHandler')
-def js_subconfigure(build_env, prepare_configure_options, mozconfig,
+def js_subconfigure(host, target, build_env, js_configure_args, mozconfig,
                     old_configure, old_configure_assignments, cache_file):
 
     class PrefixOutput(object):
         def __init__(self, prefix, fh):
             self._fh = fh
             self._begin_line = True
             self._prefix = prefix
 
@@ -41,21 +41,19 @@ def js_subconfigure(build_env, prepare_c
     for handler in logger.handlers:
         handler.setFormatter(formatter)
         if isinstance(handler, ConfigureOutputHandler):
             handler._stdout = PrefixOutput('js/src> ', handler._stdout)
 
     substs = dict(old_configure['substs'])
     assignments = dict(old_configure_assignments)
     environ = dict(os.environ)
-    if prepare_configure_options.extra_env:
-        environ.update(prepare_configure_options.extra_env)
 
-    options = [
-        o for o in prepare_configure_options.options
+    options = [host, target] + [
+        o for o in js_configure_args
         # --with-system-nspr will have been converted into the relevant $NSPR_CFLAGS
         # and $NSPR_LIBS.
         if not o.startswith('--with-system-nspr')
     ]
 
     if not substs.get('JS_HAS_INTL_API'):
         options.append('--without-intl-api')
 
@@ -84,24 +82,24 @@ def js_subconfigure(build_env, prepare_c
         ('CC', 'CXX', 'CPPFLAGS', 'CFLAGS', 'CXXFLAGS', 'LDFLAGS', 'HOST_CC',
          'HOST_CXXFLAGS', 'HOST_LDFLAGS'))
 
     # Variables that were explicitly exported from old-configure, and those
     # explicitly set in the environment when invoking old-configure, were
     # automatically inherited from subconfigure. We assume the relevant ones
     # have a corresponding AC_SUBST in old-configure, making them available
     # in `substs`.
-    for var in itertools.chain((
+    for var in (
         'MOZ_SYSTEM_ZLIB', 'MOZ_ZLIB_CFLAGS', 'MOZ_ZLIB_LIBS',
         'MOZ_APP_NAME', 'MOZ_APP_REMOTINGNAME', 'MOZ_DEV_EDITION',
         'STLPORT_LIBS', 'DIST', 'MOZ_LINKER', 'ZLIB_IN_MOZGLUE', 'RANLIB',
         'AR', 'CPP', 'CC', 'CXX', 'CPPFLAGS', 'CFLAGS', 'CXXFLAGS',
         'LDFLAGS', 'HOST_CC', 'HOST_CXX', 'HOST_CPPFLAGS',
         'HOST_CXXFLAGS', 'HOST_LDFLAGS'
-    ), prepare_configure_options.extra_env):
+    ):
         if var not in from_assignment and var in substs:
             value = substs[var]
         elif var in assignments:
             value = assignments[var]
         elif mozconfig and var in mozconfig and \
                 not mozconfig[var][1].startswith('removed'):
             value = mozconfig[var][0]
         else:
