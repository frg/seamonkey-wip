From 069016a8ed1f702e2072076d832fe0df84960bce Mon Sep 17 00:00:00 2001
From: hawkeye116477 <hawkeye116477@gmail.com>
Date: Tue, 24 Aug 2021 19:13:28 +0200
Subject: [PATCH 1106/1328] Bug 1607405 - Validate regexp derived from pattern
 attribute before using it.

In particular, this correctly treats as invalid patterns like "a)(b" that only "become" valid due to the addition of the (?:) non-capturing group, that's originally used to allow the addition of ^ and $ anchors.
---
 dom/base/nsContentUtils.cpp | 23 ++++++++++++++++-------
 1 file changed, 16 insertions(+), 7 deletions(-)

diff --git a/dom/base/nsContentUtils.cpp b/dom/base/nsContentUtils.cpp
index 05e2ea2787f6..e49ebd1132b2 100644
--- a/dom/base/nsContentUtils.cpp
+++ b/dom/base/nsContentUtils.cpp
@@ -7287,6 +7287,19 @@ nsContentUtils::IsPatternMatching(nsAString& aValue, nsAString& aPattern,
   // regexp evaluation, not actual script execution.
   JSAutoCompartment ac(cx, xpc::UnprivilegedJunkScope());
 
+  // Check if the pattern by itself is valid first, and not that it only becomes
+  // valid once we add ^(?: and )$.
+  {
+    JS::Rooted<JSObject*> testRe(
+        cx, JS::NewUCRegExpObject(
+                cx, static_cast<char16_t*>(aPattern.BeginWriting()),
+                aPattern.Length(), JS::RegExpFlag::Unicode));
+    if (!testRe) {
+      ReportPatternCompileFailure(aPattern, aDocument, cx);
+      return true;
+    }
+  }
+
   // The pattern has to match the entire value.
   aPattern.Insert(NS_LITERAL_STRING("^(?:"), 0);
   aPattern.AppendLiteral(")$");
@@ -7295,13 +7308,9 @@ nsContentUtils::IsPatternMatching(nsAString& aValue, nsAString& aPattern,
     JS::NewUCRegExpObject(cx,
                          static_cast<char16_t*>(aPattern.BeginWriting()),
                          aPattern.Length(), JS::RegExpFlag::Unicode));
-  if (!re) {
-    // Remove extra patterns added above to report with the original pattern.
-    aPattern.Cut(0, 4);
-    aPattern.Cut(aPattern.Length() - 2, 2);
-    ReportPatternCompileFailure(aPattern, aDocument, cx);
-    return true;
-  }
+
+  // We checked that the pattern is valid above.
+  MOZ_ASSERT(re, "Adding ^(?: and )$ shouldn't make a valid regexp invalid");
 
   JS::Rooted<JS::Value> rval(cx, JS::NullValue());
   size_t idx = 0;
-- 
2.33.0.windows.2

