# HG changeset patch
# User Matthew Gaudet <mgaudet@mozilla.com>
# Date 1531169536 14400
# Node ID 76ed6d31be85c63eab788714d65b8ae787660700
# Parent  58e93d58965c4f57f20625929d33e9cafb6e0777
Bug 1469044: [Part 2] Remove remainder of MathCache r=jandem

diff --git a/js/public/MemoryMetrics.h b/js/public/MemoryMetrics.h
--- a/js/public/MemoryMetrics.h
+++ b/js/public/MemoryMetrics.h
@@ -555,17 +555,16 @@ struct RuntimeSizes
 {
 #define FOR_EACH_SIZE(macro) \
     macro(_, MallocHeap, object) \
     macro(_, MallocHeap, atomsTable) \
     macro(_, MallocHeap, atomsMarkBitmaps) \
     macro(_, MallocHeap, contexts) \
     macro(_, MallocHeap, temporary) \
     macro(_, MallocHeap, interpreterStack) \
-    macro(_, MallocHeap, mathCache) \
     macro(_, MallocHeap, sharedImmutableStringsCache) \
     macro(_, MallocHeap, sharedIntlData) \
     macro(_, MallocHeap, uncompressedSourceCache) \
     macro(_, MallocHeap, scriptData) \
     macro(_, MallocHeap, tracelogger) \
     macro(_, MallocHeap, wasmRuntime) \
     macro(_, MallocHeap, jitLazyLink)
 
diff --git a/js/src/jit/CodeGenerator.cpp b/js/src/jit/CodeGenerator.cpp
--- a/js/src/jit/CodeGenerator.cpp
+++ b/js/src/jit/CodeGenerator.cpp
@@ -7584,86 +7584,79 @@ void
 CodeGenerator::visitMathFunctionD(LMathFunctionD* ins)
 {
     Register temp = ToRegister(ins->temp());
     FloatRegister input = ToFloatRegister(ins->input());
     MOZ_ASSERT(ToFloatRegister(ins->output()) == ReturnDoubleReg);
 
     masm.setupUnalignedABICall(temp);
 
-    const MathCache* mathCache = ins->mir()->cache();
-    if (mathCache) {
-        masm.movePtr(ImmPtr(mathCache), temp);
-        masm.passABIArg(temp);
-    }
     masm.passABIArg(input, MoveOp::DOUBLE);
 
-#   define MAYBE_CACHED(fcn) (mathCache ? (void*)fcn ## _impl : (void*)fcn ## _uncached)
-
     void* funptr = nullptr;
     switch (ins->mir()->function()) {
       case MMathFunction::Log:
-        funptr = JS_FUNC_TO_DATA_PTR(void*, MAYBE_CACHED(js::math_log));
+        funptr = JS_FUNC_TO_DATA_PTR(void*, js::math_log_uncached);
         break;
       case MMathFunction::Sin:
-        funptr = JS_FUNC_TO_DATA_PTR(void*, MAYBE_CACHED(js::math_sin));
+        funptr = JS_FUNC_TO_DATA_PTR(void*, js::math_sin_uncached);
         break;
       case MMathFunction::Cos:
-        funptr = JS_FUNC_TO_DATA_PTR(void*, MAYBE_CACHED(js::math_cos));
+        funptr = JS_FUNC_TO_DATA_PTR(void*, js::math_cos_uncached);
         break;
       case MMathFunction::Exp:
-        funptr = JS_FUNC_TO_DATA_PTR(void*, MAYBE_CACHED(js::math_exp));
+        funptr = JS_FUNC_TO_DATA_PTR(void*, js::math_exp_uncached);
         break;
       case MMathFunction::Tan:
-        funptr = JS_FUNC_TO_DATA_PTR(void*, MAYBE_CACHED(js::math_tan));
+        funptr = JS_FUNC_TO_DATA_PTR(void*, js::math_tan_uncached);
         break;
       case MMathFunction::ATan:
-        funptr = JS_FUNC_TO_DATA_PTR(void*, MAYBE_CACHED(js::math_atan));
+        funptr = JS_FUNC_TO_DATA_PTR(void*, js::math_atan_uncached);
         break;
       case MMathFunction::ASin:
-        funptr = JS_FUNC_TO_DATA_PTR(void*, MAYBE_CACHED(js::math_asin));
+        funptr = JS_FUNC_TO_DATA_PTR(void*, js::math_asin_uncached);
         break;
       case MMathFunction::ACos:
-        funptr = JS_FUNC_TO_DATA_PTR(void*, MAYBE_CACHED(js::math_acos));
+        funptr = JS_FUNC_TO_DATA_PTR(void*, js::math_acos_uncached);
         break;
       case MMathFunction::Log10:
-        funptr = JS_FUNC_TO_DATA_PTR(void*, MAYBE_CACHED(js::math_log10));
+        funptr = JS_FUNC_TO_DATA_PTR(void*, js::math_log10_uncached);
         break;
       case MMathFunction::Log2:
-        funptr = JS_FUNC_TO_DATA_PTR(void*, MAYBE_CACHED(js::math_log2));
+        funptr = JS_FUNC_TO_DATA_PTR(void*, js::math_log2_uncached);
         break;
       case MMathFunction::Log1P:
-        funptr = JS_FUNC_TO_DATA_PTR(void*, MAYBE_CACHED(js::math_log1p));
+        funptr = JS_FUNC_TO_DATA_PTR(void*, js::math_log1p_uncached);
         break;
       case MMathFunction::ExpM1:
-        funptr = JS_FUNC_TO_DATA_PTR(void*, MAYBE_CACHED(js::math_expm1));
+        funptr = JS_FUNC_TO_DATA_PTR(void*, js::math_expm1_uncached);
         break;
       case MMathFunction::CosH:
-        funptr = JS_FUNC_TO_DATA_PTR(void*, MAYBE_CACHED(js::math_cosh));
+        funptr = JS_FUNC_TO_DATA_PTR(void*, js::math_cosh_uncached);
         break;
       case MMathFunction::SinH:
-        funptr = JS_FUNC_TO_DATA_PTR(void*, MAYBE_CACHED(js::math_sinh));
+        funptr = JS_FUNC_TO_DATA_PTR(void*, js::math_sinh_uncached);
         break;
       case MMathFunction::TanH:
-        funptr = JS_FUNC_TO_DATA_PTR(void*, MAYBE_CACHED(js::math_tanh));
+        funptr = JS_FUNC_TO_DATA_PTR(void*, js::math_tanh_uncached);
         break;
       case MMathFunction::ACosH:
-        funptr = JS_FUNC_TO_DATA_PTR(void*, MAYBE_CACHED(js::math_acosh));
+        funptr = JS_FUNC_TO_DATA_PTR(void*, js::math_acosh_uncached);
         break;
       case MMathFunction::ASinH:
-        funptr = JS_FUNC_TO_DATA_PTR(void*, MAYBE_CACHED(js::math_asinh));
+        funptr = JS_FUNC_TO_DATA_PTR(void*, js::math_asinh_uncached);
         break;
       case MMathFunction::ATanH:
-        funptr = JS_FUNC_TO_DATA_PTR(void*, MAYBE_CACHED(js::math_atanh));
+        funptr = JS_FUNC_TO_DATA_PTR(void*, js::math_atanh_uncached);
         break;
       case MMathFunction::Trunc:
         funptr = JS_FUNC_TO_DATA_PTR(void*, js::math_trunc_uncached);
         break;
       case MMathFunction::Cbrt:
-        funptr = JS_FUNC_TO_DATA_PTR(void*, MAYBE_CACHED(js::math_cbrt));
+        funptr = JS_FUNC_TO_DATA_PTR(void*, js::math_cbrt_uncached);
         break;
       case MMathFunction::Floor:
         funptr = JS_FUNC_TO_DATA_PTR(void*, js::math_floor_impl);
         break;
       case MMathFunction::Ceil:
         funptr = JS_FUNC_TO_DATA_PTR(void*, js::math_ceil_impl);
         break;
       case MMathFunction::Round:
@@ -8964,34 +8957,25 @@ CodeGenerator::visitSinCos(LSinCos *lir)
     Register params = ToRegister(lir->temp2());
     FloatRegister input = ToFloatRegister(lir->input());
     FloatRegister outputSin = ToFloatRegister(lir->outputSin());
     FloatRegister outputCos = ToFloatRegister(lir->outputCos());
 
     masm.reserveStack(sizeof(double) * 2);
     masm.moveStackPtrTo(params);
 
-    const MathCache* mathCache = lir->mir()->cache();
-
     masm.setupUnalignedABICall(temp);
-    if (mathCache) {
-        masm.movePtr(ImmPtr(mathCache), temp);
-        masm.passABIArg(temp);
-    }
-
-#define MAYBE_CACHED_(fcn) (mathCache ? (void*)fcn ## _impl : (void*)fcn ## _uncached)
 
     masm.passABIArg(input, MoveOp::DOUBLE);
     masm.passABIArg(MoveOperand(params, sizeof(double), MoveOperand::EFFECTIVE_ADDRESS),
                                 MoveOp::GENERAL);
     masm.passABIArg(MoveOperand(params, 0, MoveOperand::EFFECTIVE_ADDRESS),
                                 MoveOp::GENERAL);
 
-    masm.callWithABI(JS_FUNC_TO_DATA_PTR(void*, MAYBE_CACHED_(js::math_sincos)));
-#undef MAYBE_CACHED_
+    masm.callWithABI(JS_FUNC_TO_DATA_PTR(void*, js::math_sincos_uncached));
 
     masm.loadDouble(Address(masm.getStackPointer(), 0), outputCos);
     masm.loadDouble(Address(masm.getStackPointer(), sizeof(double)), outputSin);
     masm.freeStack(sizeof(double) * 2);
 }
 
 typedef ArrayObject* (*StringSplitFn)(JSContext*, HandleObjectGroup, HandleString, HandleString, uint32_t);
 static const VMFunction StringSplitInfo =
diff --git a/js/src/jit/Ion.cpp b/js/src/jit/Ion.cpp
--- a/js/src/jit/Ion.cpp
+++ b/js/src/jit/Ion.cpp
@@ -1314,18 +1314,17 @@ OptimizeSinCos(MIRGraph &graph)
                 continue;
             }
 
             JitSpew(JitSpew_Sincos, "Found, at least, a pair sin/cos. Adding sincos in block %d",
                     block->id());
             // Adding the MSinCos and replacing the parameters of the
             // sin(x)/cos(x) to sin(sincos(x))/cos(sincos(x)).
             MSinCos *insSinCos = MSinCos::New(graph.alloc(),
-                                              insFunc->input(),
-                                              insFunc->toMathFunction()->cache());
+                                              insFunc->input());
             insSinCos->setImplicitlyUsedUnchecked();
             block->insertBefore(insFunc, insSinCos);
             for (MUseDefIterator uses(insFunc->input()); uses; )
             {
                 MDefinition* def = uses.def();
                 uses++;
                 if (!def->isInstruction())
                     continue;
diff --git a/js/src/jit/MCallOptimize.cpp b/js/src/jit/MCallOptimize.cpp
--- a/js/src/jit/MCallOptimize.cpp
+++ b/js/src/jit/MCallOptimize.cpp
@@ -507,22 +507,20 @@ IonBuilder::inlineMathFunction(CallInfo&
     if (callInfo.argc() != 1)
         return InliningStatus_NotInlined;
 
     if (getInlineReturnType() != MIRType::Double)
         return InliningStatus_NotInlined;
     if (!IsNumberType(callInfo.getArg(0)->type()))
         return InliningStatus_NotInlined;
 
-    const MathCache* cache = TlsContext.get()->caches().maybeGetMathCache();
-
     callInfo.fun()->setImplicitlyUsedUnchecked();
     callInfo.thisArg()->setImplicitlyUsedUnchecked();
 
-    MMathFunction* ins = MMathFunction::New(alloc(), callInfo.getArg(0), function, cache);
+    MMathFunction* ins = MMathFunction::New(alloc(), callInfo.getArg(0), function);
     current->add(ins);
     current->push(ins);
     return InliningStatus_Inlined;
 }
 
 IonBuilder::InliningResult
 IonBuilder::inlineArray(CallInfo& callInfo)
 {
@@ -1150,18 +1148,17 @@ IonBuilder::inlineMathFloor(CallInfo& ca
 
         if (returnType == MIRType::Double) {
             callInfo.setImplicitlyUsedUnchecked();
 
             MInstruction* ins = nullptr;
             if (MNearbyInt::HasAssemblerSupport(RoundingMode::Down)) {
                 ins = MNearbyInt::New(alloc(), callInfo.getArg(0), argType, RoundingMode::Down);
             } else {
-                ins = MMathFunction::New(alloc(), callInfo.getArg(0), MMathFunction::Floor,
-                                         /* cache */ nullptr);
+                ins = MMathFunction::New(alloc(), callInfo.getArg(0), MMathFunction::Floor);
             }
 
             current->add(ins);
             current->push(ins);
             return InliningStatus_Inlined;
         }
     }
 
@@ -1204,18 +1201,17 @@ IonBuilder::inlineMathCeil(CallInfo& cal
 
         if (returnType == MIRType::Double) {
             callInfo.setImplicitlyUsedUnchecked();
 
             MInstruction* ins = nullptr;
             if (MNearbyInt::HasAssemblerSupport(RoundingMode::Up)) {
                 ins = MNearbyInt::New(alloc(), callInfo.getArg(0), argType, RoundingMode::Up);
             } else {
-                ins = MMathFunction::New(alloc(), callInfo.getArg(0), MMathFunction::Ceil,
-                                         /* cache */ nullptr);
+                ins = MMathFunction::New(alloc(), callInfo.getArg(0), MMathFunction::Ceil);
             }
 
             current->add(ins);
             current->push(ins);
             return InliningStatus_Inlined;
         }
     }
 
@@ -1276,18 +1272,17 @@ IonBuilder::inlineMathRound(CallInfo& ca
         MRound* ins = MRound::New(alloc(), callInfo.getArg(0));
         current->add(ins);
         current->push(ins);
         return InliningStatus_Inlined;
     }
 
     if (IsFloatingPointType(argType) && returnType == MIRType::Double) {
         callInfo.setImplicitlyUsedUnchecked();
-        MMathFunction* ins = MMathFunction::New(alloc(), callInfo.getArg(0), MMathFunction::Round,
-                                                /* cache */ nullptr);
+        MMathFunction* ins = MMathFunction::New(alloc(), callInfo.getArg(0), MMathFunction::Round);
         current->add(ins);
         current->push(ins);
         return InliningStatus_Inlined;
     }
 
     return InliningStatus_NotInlined;
 }
 
@@ -1521,18 +1516,17 @@ IonBuilder::inlineMathTrunc(CallInfo& ca
         if (returnType == MIRType::Double) {
             callInfo.setImplicitlyUsedUnchecked();
 
             MInstruction* ins = nullptr;
             if (MNearbyInt::HasAssemblerSupport(RoundingMode::TowardsZero)) {
                 ins = MNearbyInt::New(alloc(), callInfo.getArg(0), argType,
                                       RoundingMode::TowardsZero);
             } else {
-                ins = MMathFunction::New(alloc(), callInfo.getArg(0), MMathFunction::Trunc,
-                                         /* cache */ nullptr);
+                ins = MMathFunction::New(alloc(), callInfo.getArg(0), MMathFunction::Trunc);
             }
 
             current->add(ins);
             current->push(ins);
             return InliningStatus_Inlined;
         }
     }
 
diff --git a/js/src/jit/MIR.h b/js/src/jit/MIR.h
--- a/js/src/jit/MIR.h
+++ b/js/src/jit/MIR.h
@@ -7130,37 +7130,34 @@ class MMathFunction
         Cbrt,
         Floor,
         Ceil,
         Round
     };
 
   private:
     Function function_;
-    const MathCache* cache_;
 
     // A nullptr cache means this function will neither access nor update the cache.
-    MMathFunction(MDefinition* input, Function function, const MathCache* cache)
-      : MUnaryInstruction(classOpcode, input), function_(function), cache_(cache)
+    MMathFunction(MDefinition* input, Function function)
+      : MUnaryInstruction(classOpcode, input), function_(function)
     {
         setResultType(MIRType::Double);
         specialization_ = MIRType::Double;
         setMovable();
     }
 
   public:
     INSTRUCTION_HEADER(MathFunction)
     TRIVIAL_NEW_WRAPPERS
 
     Function function() const {
         return function_;
     }
-    const MathCache* cache() const {
-        return cache_;
-    }
+
     bool congruentTo(const MDefinition* ins) const override {
         if (!ins->isMathFunction())
             return false;
         if (ins->toMathFunction()->function() != function())
             return false;
         return congruentIfOperandsEqual(ins);
     }
 
@@ -7810,46 +7807,41 @@ class MStringConvertCase
         return mode_;
     }
 };
 
 class MSinCos
   : public MUnaryInstruction,
     public FloatingPointPolicy<0>::Data
 {
-    const MathCache* cache_;
-
-    MSinCos(MDefinition *input, const MathCache *cache)
-      : MUnaryInstruction(classOpcode, input),
-        cache_(cache)
+
+    explicit MSinCos(MDefinition *input)
+      : MUnaryInstruction(classOpcode, input)
     {
         setResultType(MIRType::SinCosDouble);
         specialization_ = MIRType::Double;
         setMovable();
     }
 
   public:
     INSTRUCTION_HEADER(SinCos)
 
-    static MSinCos *New(TempAllocator &alloc, MDefinition *input, const MathCache *cache)
-    {
-        return new (alloc) MSinCos(input, cache);
+    static MSinCos *New(TempAllocator &alloc, MDefinition *input)
+    {
+        return new (alloc) MSinCos(input);
     }
     AliasSet getAliasSet() const override {
         return AliasSet::None();
     }
     bool congruentTo(const MDefinition *ins) const override {
         return congruentIfOperandsEqual(ins);
     }
     bool possiblyCalls() const override {
         return true;
     }
-    const MathCache* cache() const {
-        return cache_;
-    }
 };
 
 class MStringSplit
   : public MBinaryInstruction,
     public MixPolicy<StringPolicy<0>, StringPolicy<1> >::Data
 {
     CompilerObjectGroup group_;
 
diff --git a/js/src/jsapi-tests/testJitRangeAnalysis.cpp b/js/src/jsapi-tests/testJitRangeAnalysis.cpp
--- a/js/src/jsapi-tests/testJitRangeAnalysis.cpp
+++ b/js/src/jsapi-tests/testJitRangeAnalysis.cpp
@@ -106,17 +106,16 @@ BEGIN_TEST(testJitRangeAnalysis_MathSign
 
     return true;
 }
 END_TEST(testJitRangeAnalysis_MathSign)
 
 BEGIN_TEST(testJitRangeAnalysis_MathSignBeta)
 {
     MinimalFunc func;
-    MathCache cache;
 
     MBasicBlock* entry = func.createEntryBlock();
     MBasicBlock* thenBlock = func.createBlock(entry);
     MBasicBlock* elseBlock = func.createBlock(entry);
     MBasicBlock* elseThenBlock = func.createBlock(elseBlock);
     MBasicBlock* elseElseBlock = func.createBlock(elseBlock);
 
     // if (p < 0)
diff --git a/js/src/jsmath.cpp b/js/src/jsmath.cpp
--- a/js/src/jsmath.cpp
+++ b/js/src/jsmath.cpp
@@ -125,31 +125,16 @@ static const JSConstDoubleSpec math_cons
     {"LN2"    ,  M_LN2     },
     {"LN10"   ,  M_LN10    },
     {"PI"     ,  M_PI      },
     {"SQRT2"  ,  M_SQRT2   },
     {"SQRT1_2",  M_SQRT1_2 },
     {nullptr  ,  0         }
 };
 
-MathCache::MathCache() {
-    memset(table, 0, sizeof(table));
-
-    /* See comments in lookup(). */
-    MOZ_ASSERT(IsNegativeZero(-0.0));
-    MOZ_ASSERT(!IsNegativeZero(+0.0));
-    MOZ_ASSERT(hash(-0.0, MathCache::Sin) != hash(+0.0, MathCache::Sin));
-}
-
-size_t
-MathCache::sizeOfIncludingThis(mozilla::MallocSizeOf mallocSizeOf)
-{
-    return mallocSizeOf(this);
-}
-
 typedef double (*UnaryMathFunctionType)(double);
 
 template <UnaryMathFunctionType F>
 static bool
 math_function(JSContext* cx, HandleValue val, MutableHandleValue res)
 {
     double x;
     if (!ToNumber(cx, val, &x))
@@ -202,63 +187,42 @@ js::math_abs(JSContext* cx, unsigned arg
         args.rval().setNaN();
         return true;
     }
 
     return math_abs_handle(cx, args[0], args.rval());
 }
 
 double
-js::math_acos_impl(MathCache* cache, double x)
-{
-    AutoUnsafeCallWithABI unsafe;
-    return cache->lookup(fdlibm::acos, x, MathCache::Acos);
-}
-
-double
 js::math_acos_uncached(double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return fdlibm::acos(x);
 }
 
 bool
 js::math_acos(JSContext* cx, unsigned argc, Value* vp)
 {
     return math_function<math_acos_uncached>(cx, argc, vp);
 }
 
 double
-js::math_asin_impl(MathCache* cache, double x)
-{
-    AutoUnsafeCallWithABI unsafe;
-    return cache->lookup(fdlibm::asin, x, MathCache::Asin);
-}
-
-double
 js::math_asin_uncached(double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return fdlibm::asin(x);
 }
 
 bool
 js::math_asin(JSContext* cx, unsigned argc, Value* vp)
 {
     return math_function<math_asin_uncached>(cx, argc, vp);
 }
 
 double
-js::math_atan_impl(MathCache* cache, double x)
-{
-    AutoUnsafeCallWithABI unsafe;
-    return cache->lookup(fdlibm::atan, x, MathCache::Atan);
-}
-
-double
 js::math_atan_uncached(double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return fdlibm::atan(x);
 }
 
 bool
 js::math_atan(JSContext* cx, unsigned argc, Value* vp)
@@ -348,43 +312,29 @@ js::math_clz32(JSContext* cx, unsigned a
         return true;
     }
 
     args.rval().setInt32(mozilla::CountLeadingZeroes32(n));
     return true;
 }
 
 double
-js::math_cos_impl(MathCache* cache, double x)
-{
-    AutoUnsafeCallWithABI unsafe;
-    return cache->lookup(cos, x, MathCache::Cos);
-}
-
-double
 js::math_cos_uncached(double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return cos(x);
 }
 
 bool
 js::math_cos(JSContext* cx, unsigned argc, Value* vp)
 {
     return math_function<math_cos_uncached>(cx, argc, vp);
 }
 
 double
-js::math_exp_impl(MathCache* cache, double x)
-{
-    AutoUnsafeCallWithABI unsafe;
-    return cache->lookup(fdlibm::exp, x, MathCache::Exp);
-}
-
-double
 js::math_exp_uncached(double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return fdlibm::exp(x);
 }
 
 bool
 js::math_exp(JSContext* cx, unsigned argc, Value* vp)
@@ -475,22 +425,16 @@ js::math_fround(JSContext* cx, unsigned 
     if (args.length() == 0) {
         args.rval().setNaN();
         return true;
     }
 
     return RoundFloat32(cx, args[0], args.rval());
 }
 
-double
-js::math_log_impl(MathCache* cache, double x)
-{
-    AutoUnsafeCallWithABI unsafe;
-    return cache->lookup(fdlibm::log, x, MathCache::Log);
-}
 
 double
 js::math_log_uncached(double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return fdlibm::log(x);
 }
 
@@ -802,23 +746,16 @@ js::math_round(JSContext* cx, unsigned a
         args.rval().setNaN();
         return true;
     }
 
     return math_round_handle(cx, args[0], args.rval());
 }
 
 double
-js::math_sin_impl(MathCache* cache, double x)
-{
-    AutoUnsafeCallWithABI unsafe;
-    return cache->lookup(sin, x, MathCache::Sin);
-}
-
-double
 js::math_sin_uncached(double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return sin(x);
 }
 
 bool
 js::math_sin_handle(JSContext* cx, HandleValue val, MutableHandleValue res)
@@ -841,272 +778,167 @@ js::math_sincos_uncached(double x, doubl
 #elif defined(HAVE___SINCOS)
     __sincos(x, sin, cos);
 #else
     *sin = js::math_sin_uncached(x);
     *cos = js::math_cos_uncached(x);
 #endif
 }
 
-void
-js::math_sincos_impl(MathCache* mathCache, double x, double *sin, double *cos)
-{
-    AutoUnsafeCallWithABI unsafe;
-    unsigned indexSin;
-    unsigned indexCos;
-    bool hasSin = mathCache->isCached(x, MathCache::Sin, sin, &indexSin);
-    bool hasCos = mathCache->isCached(x, MathCache::Cos, cos, &indexCos);
-    if (!(hasSin || hasCos)) {
-        js::math_sincos_uncached(x, sin, cos);
-        mathCache->store(MathCache::Sin, x, *sin, indexSin);
-        mathCache->store(MathCache::Cos, x, *cos, indexCos);
-        return;
-    }
-
-    if (!hasSin)
-        *sin = js::math_sin_impl(mathCache, x);
-
-    if (!hasCos)
-        *cos = js::math_cos_impl(mathCache, x);
-}
-
 double
 js::math_sqrt_uncached(double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return sqrt(x);
 }
 
-double
-js::math_sqrt_impl(MathCache* cache, double x)
-{
-    AutoUnsafeCallWithABI unsafe;
-    return cache->lookup(sqrt, x, MathCache::Sqrt);
-}
-
 bool
 js::math_sqrt_handle(JSContext* cx, HandleValue number, MutableHandleValue result)
 {
     return math_function<math_sqrt_uncached>(cx, number, result);
 }
 
 bool
 js::math_sqrt(JSContext* cx, unsigned argc, Value* vp)
 {
     return math_function<math_sqrt_uncached>(cx, argc, vp);
 }
 
 double
-js::math_tan_impl(MathCache* cache, double x)
-{
-    AutoUnsafeCallWithABI unsafe;
-    return cache->lookup(tan, x, MathCache::Tan);
-}
-
-double
 js::math_tan_uncached(double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return tan(x);
 }
 
 bool
 js::math_tan(JSContext* cx, unsigned argc, Value* vp)
 {
     return math_function<math_tan_uncached>(cx, argc, vp);
 }
 
 double
-js::math_log10_impl(MathCache* cache, double x)
-{
-    AutoUnsafeCallWithABI unsafe;
-    return cache->lookup(fdlibm::log10, x, MathCache::Log10);
-}
-
-double
 js::math_log10_uncached(double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return fdlibm::log10(x);
 }
 
 bool
 js::math_log10(JSContext* cx, unsigned argc, Value* vp)
 {
     return math_function<math_log10_uncached>(cx, argc, vp);
 }
 
 double
-js::math_log2_impl(MathCache* cache, double x)
-{
-    AutoUnsafeCallWithABI unsafe;
-    return cache->lookup(fdlibm::log2, x, MathCache::Log2);
-}
-
-double
 js::math_log2_uncached(double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return fdlibm::log2(x);
 }
 
 bool
 js::math_log2(JSContext* cx, unsigned argc, Value* vp)
 {
     return math_function<math_log2_uncached>(cx, argc, vp);
 }
 
 double
-js::math_log1p_impl(MathCache* cache, double x)
-{
-    AutoUnsafeCallWithABI unsafe;
-    return cache->lookup(fdlibm::log1p, x, MathCache::Log1p);
-}
-
-double
 js::math_log1p_uncached(double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return fdlibm::log1p(x);
 }
 
 bool
 js::math_log1p(JSContext* cx, unsigned argc, Value* vp)
 {
     return math_function<math_log1p_uncached>(cx, argc, vp);
 }
 
 double
-js::math_expm1_impl(MathCache* cache, double x)
-{
-    AutoUnsafeCallWithABI unsafe;
-    return cache->lookup(fdlibm::expm1, x, MathCache::Expm1);
-}
-
-double
 js::math_expm1_uncached(double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return fdlibm::expm1(x);
 }
 
 bool
 js::math_expm1(JSContext* cx, unsigned argc, Value* vp)
 {
     return math_function<math_expm1_uncached>(cx, argc, vp);
 }
 
 double
-js::math_cosh_impl(MathCache* cache, double x)
-{
-    AutoUnsafeCallWithABI unsafe;
-    return cache->lookup(fdlibm::cosh, x, MathCache::Cosh);
-}
-
-double
 js::math_cosh_uncached(double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return fdlibm::cosh(x);
 }
 
 bool
 js::math_cosh(JSContext* cx, unsigned argc, Value* vp)
 {
     return math_function<math_cosh_uncached>(cx, argc, vp);
 }
 
 double
-js::math_sinh_impl(MathCache* cache, double x)
-{
-    AutoUnsafeCallWithABI unsafe;
-    return cache->lookup(fdlibm::sinh, x, MathCache::Sinh);
-}
-
-double
 js::math_sinh_uncached(double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return fdlibm::sinh(x);
 }
 
 bool
 js::math_sinh(JSContext* cx, unsigned argc, Value* vp)
 {
     return math_function<math_sinh_uncached>(cx, argc, vp);
 }
 
-double
-js::math_tanh_impl(MathCache* cache, double x)
-{
-    AutoUnsafeCallWithABI unsafe;
-    return cache->lookup(fdlibm::tanh, x, MathCache::Tanh);
-}
 
 double
 js::math_tanh_uncached(double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return fdlibm::tanh(x);
 }
 
 bool
 js::math_tanh(JSContext* cx, unsigned argc, Value* vp)
 {
     return math_function<math_tanh_uncached>(cx, argc, vp);
 }
 
 double
-js::math_acosh_impl(MathCache* cache, double x)
-{
-    AutoUnsafeCallWithABI unsafe;
-    return cache->lookup(fdlibm::acosh, x, MathCache::Acosh);
-}
-
-double
 js::math_acosh_uncached(double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return fdlibm::acosh(x);
 }
 
 bool
 js::math_acosh(JSContext* cx, unsigned argc, Value* vp)
 {
     return math_function<math_acosh_uncached>(cx, argc, vp);
 }
 
 double
-js::math_asinh_impl(MathCache* cache, double x)
-{
-    AutoUnsafeCallWithABI unsafe;
-    return cache->lookup(fdlibm::asinh, x, MathCache::Asinh);
-}
-
-double
 js::math_asinh_uncached(double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return fdlibm::asinh(x);
 }
 
 bool
 js::math_asinh(JSContext* cx, unsigned argc, Value* vp)
 {
     return math_function<math_asinh_uncached>(cx, argc, vp);
 }
 
 double
-js::math_atanh_impl(MathCache* cache, double x)
-{
-    AutoUnsafeCallWithABI unsafe;
-    return cache->lookup(fdlibm::atanh, x, MathCache::Atanh);
-}
-
-double
 js::math_atanh_uncached(double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return fdlibm::atanh(x);
 }
 
 bool
 js::math_atanh(JSContext* cx, unsigned argc, Value* vp)
@@ -1284,23 +1116,16 @@ js::math_sign(JSContext* cx, unsigned ar
         args.rval().setNaN();
         return true;
     }
 
     return math_sign_handle(cx, args[0], args.rval());
 }
 
 double
-js::math_cbrt_impl(MathCache* cache, double x)
-{
-    AutoUnsafeCallWithABI unsafe;
-    return cache->lookup(fdlibm::cbrt, x, MathCache::Cbrt);
-}
-
-double
 js::math_cbrt_uncached(double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return fdlibm::cbrt(x);
 }
 
 bool
 js::math_cbrt(JSContext* cx, unsigned argc, Value* vp)
diff --git a/js/src/jsmath.h b/js/src/jsmath.h
--- a/js/src/jsmath.h
+++ b/js/src/jsmath.h
@@ -15,78 +15,16 @@
 
 namespace js {
 
 struct Class;
 class GlobalObject;
 
 typedef double (*UnaryFunType)(double);
 
-class MathCache
-{
-  public:
-    enum MathFuncId {
-        Zero,
-        Sin, Cos, Tan, Sinh, Cosh, Tanh, Asin, Acos, Atan, Asinh, Acosh, Atanh,
-        Sqrt, Log, Log10, Log2, Log1p, Exp, Expm1, Cbrt
-    };
-
-  private:
-    static const unsigned SizeLog2 = 12;
-    static const unsigned Size = 1 << SizeLog2;
-    struct Entry { double in; MathFuncId id; double out; };
-    Entry table[Size];
-
-  public:
-    MathCache();
-
-    unsigned hash(double x, MathFuncId id) {
-        union { double d; struct { uint32_t one, two; } s; } u = { x };
-        uint32_t hash32 = u.s.one ^ u.s.two;
-        hash32 += uint32_t(id) << 8;
-        uint16_t hash16 = uint16_t(hash32 ^ (hash32 >> 16));
-        return (hash16 & (Size - 1)) ^ (hash16 >> (16 - SizeLog2));
-    }
-
-    /*
-     * N.B. lookup uses double-equality. This is only safe if hash() maps +0
-     * and -0 to different table entries, which is asserted in MathCache().
-     */
-    double lookup(UnaryFunType f, double x, MathFuncId id) {
-        unsigned index = hash(x, id);
-        Entry& e = table[index];
-        if (e.in == x && e.id == id)
-            return e.out;
-        e.in = x;
-        e.id = id;
-        return e.out = f(x);
-    }
-
-    bool isCached(double x, MathFuncId id, double *r, unsigned *index) {
-        *index = hash(x, id);
-        Entry& e = table[*index];
-        if (e.in == x && e.id == id) {
-            *r = e.out;
-            return true;
-        }
-        return false;
-    }
-
-    void store(MathFuncId id, double x, double v, unsigned index) {
-        Entry &e = table[index];
-        if (e.in == x && e.id == id)
-            return;
-        e.in = x;
-        e.id = id;
-        e.out = v;
-    }
-
-    size_t sizeOfIncludingThis(mozilla::MallocSizeOf mallocSizeOf);
-};
-
 /*
  * JS math functions.
  */
 
 extern const Class MathClass;
 
 extern JSObject*
 InitMathClass(JSContext* cx, Handle<GlobalObject*> global);
@@ -121,38 +59,32 @@ extern double
 math_min_impl(double x, double y);
 
 extern bool
 math_min(JSContext* cx, unsigned argc, js::Value* vp);
 
 extern double
 math_sqrt_uncached(double x);
 
-extern double
-math_sqrt_impl(MathCache* cache, double x);
-
 extern bool
 math_sqrt_handle(JSContext* cx, js::HandleValue number, js::MutableHandleValue result);
 
 extern bool
 math_sqrt(JSContext* cx, unsigned argc, js::Value* vp);
 
 extern bool
 math_pow(JSContext* cx, unsigned argc, js::Value* vp);
 
 extern bool
 minmax_impl(JSContext* cx, bool max, js::HandleValue a, js::HandleValue b,
             js::MutableHandleValue res);
 
 extern void
 math_sincos_uncached(double x, double *sin, double *cos);
 
-extern void
-math_sincos_impl(MathCache* mathCache, double x, double *sin, double *cos);
-
 extern bool
 math_imul_handle(JSContext* cx, HandleValue lhs, HandleValue rhs, MutableHandleValue res);
 
 extern bool
 math_imul(JSContext* cx, unsigned argc, js::Value* vp);
 
 extern bool
 RoundFloat32(JSContext* cx, HandleValue v, float* out);
@@ -162,61 +94,46 @@ RoundFloat32(JSContext* cx, HandleValue 
 
 extern bool
 math_fround(JSContext* cx, unsigned argc, js::Value* vp);
 
 extern bool
 math_log(JSContext* cx, unsigned argc, js::Value* vp);
 
 extern double
-math_log_impl(MathCache* cache, double x);
-
-extern double
 math_log_uncached(double x);
 
 extern bool
 math_log_handle(JSContext* cx, HandleValue val, MutableHandleValue res);
 
 extern bool
 math_sin(JSContext* cx, unsigned argc, js::Value* vp);
 
 extern double
-math_sin_impl(MathCache* cache, double x);
-
-extern double
 math_sin_uncached(double x);
 
 extern bool
 math_sin_handle(JSContext* cx, HandleValue val, MutableHandleValue res);
 
 extern bool
 math_cos(JSContext* cx, unsigned argc, js::Value* vp);
 
 extern double
-math_cos_impl(MathCache* cache, double x);
-
-extern double
 math_cos_uncached(double x);
 
 extern bool
 math_exp(JSContext* cx, unsigned argc, js::Value* vp);
 
 extern double
-math_exp_impl(MathCache* cache, double x);
-
-extern double
 math_exp_uncached(double x);
 
 extern bool
 math_tan(JSContext* cx, unsigned argc, js::Value* vp);
 
 extern double
-math_tan_impl(MathCache* cache, double x);
-
-extern double
 math_tan_uncached(double x);
 
 extern bool
 math_log10(JSContext* cx, unsigned argc, js::Value* vp);
 
 extern bool
 math_log2(JSContext* cx, unsigned argc, js::Value* vp);
 
@@ -282,37 +199,28 @@ math_atan2_handle(JSContext* cx, HandleV
 
 extern bool
 math_atan2(JSContext* cx, unsigned argc, Value* vp);
 
 extern double
 ecmaAtan2(double x, double y);
 
 extern double
-math_atan_impl(MathCache* cache, double x);
-
-extern double
 math_atan_uncached(double x);
 
 extern bool
 math_atan(JSContext* cx, unsigned argc, js::Value* vp);
 
 extern double
-math_asin_impl(MathCache* cache, double x);
-
-extern double
 math_asin_uncached(double x);
 
 extern bool
 math_asin(JSContext* cx, unsigned argc, js::Value* vp);
 
 extern double
-math_acos_impl(MathCache* cache, double x);
-
-extern double
 math_acos_uncached(double x);
 
 extern bool
 math_acos(JSContext* cx, unsigned argc, js::Value* vp);
 
 extern bool
 math_ceil_handle(JSContext* cx, HandleValue value, MutableHandleValue res);
 
@@ -351,73 +259,43 @@ math_roundf_impl(float x);
 
 extern double
 powi(double x, int32_t y);
 
 extern double
 ecmaPow(double x, double y);
 
 extern double
-math_log10_impl(MathCache* cache, double x);
-
-extern double
 math_log10_uncached(double x);
 
 extern double
-math_log2_impl(MathCache* cache, double x);
-
-extern double
 math_log2_uncached(double x);
 
 extern double
-math_log1p_impl(MathCache* cache, double x);
-
-extern double
 math_log1p_uncached(double x);
 
 extern double
-math_expm1_impl(MathCache* cache, double x);
-
-extern double
 math_expm1_uncached(double x);
 
 extern double
-math_cosh_impl(MathCache* cache, double x);
-
-extern double
 math_cosh_uncached(double x);
 
 extern double
-math_sinh_impl(MathCache* cache, double x);
-
-extern double
 math_sinh_uncached(double x);
 
 extern double
-math_tanh_impl(MathCache* cache, double x);
-
-extern double
 math_tanh_uncached(double x);
 
 extern double
-math_acosh_impl(MathCache* cache, double x);
-
-extern double
 math_acosh_uncached(double x);
 
 extern double
-math_asinh_impl(MathCache* cache, double x);
-
-extern double
 math_asinh_uncached(double x);
 
 extern double
-math_atanh_impl(MathCache* cache, double x);
-
-extern double
 math_atanh_uncached(double x);
 
 extern double
 math_trunc_uncached(double x);
 
 extern float
 math_truncf_impl(float x);
 
@@ -426,16 +304,13 @@ math_trunc_handle(JSContext* cx, HandleV
 
 extern double
 math_sign_uncached(double x);
 
 extern bool
 math_sign_handle(JSContext* cx, HandleValue v, MutableHandleValue r);
 
 extern double
-math_cbrt_impl(MathCache* cache, double x);
-
-extern double
 math_cbrt_uncached(double x);
 
 } /* namespace js */
 
 #endif /* jsmath_h */
diff --git a/js/src/vm/Caches.cpp b/js/src/vm/Caches.cpp
--- a/js/src/vm/Caches.cpp
+++ b/js/src/vm/Caches.cpp
@@ -7,31 +7,16 @@
 #include "vm/Caches-inl.h"
 
 #include "mozilla/PodOperations.h"
 
 using namespace js;
 
 using mozilla::PodZero;
 
-MathCache*
-RuntimeCaches::createMathCache(JSContext* cx)
-{
-    MOZ_ASSERT(!mathCache_);
-
-    auto newMathCache = MakeUnique<MathCache>();
-    if (!newMathCache) {
-        ReportOutOfMemory(cx);
-        return nullptr;
-    }
-
-    mathCache_ = std::move(newMathCache);
-    return mathCache_.get();
-}
-
 bool
 RuntimeCaches::init()
 {
     if (!evalCache.init())
         return false;
 
     return true;
 }
diff --git a/js/src/vm/Caches.h b/js/src/vm/Caches.h
--- a/js/src/vm/Caches.h
+++ b/js/src/vm/Caches.h
@@ -235,36 +235,25 @@ class NewObjectCache
         // Initialize with barriers
         dst->initGroup(src->group());
         dst->initShape(src->shape());
     }
 };
 
 class RuntimeCaches
 {
-    UniquePtr<js::MathCache> mathCache_;
-
-    js::MathCache* createMathCache(JSContext* cx);
-
   public:
     js::GSNCache gsnCache;
     js::EnvironmentCoordinateNameCache envCoordinateNameCache;
     js::NewObjectCache newObjectCache;
     js::UncompressedSourceCache uncompressedSourceCache;
     js::EvalCache evalCache;
 
     bool init();
 
-    js::MathCache* getMathCache(JSContext* cx) {
-        return mathCache_ ? mathCache_.get() : createMathCache(cx);
-    }
-    js::MathCache* maybeGetMathCache() {
-        return mathCache_.get();
-    }
-
     void purgeForMinorGC(JSRuntime* rt) {
         newObjectCache.clearNurseryObjects(rt);
         evalCache.sweep();
     }
 
     void purgeForCompaction() {
         newObjectCache.purge();
         if (evalCache.initialized())
diff --git a/js/src/vm/Runtime.cpp b/js/src/vm/Runtime.cpp
--- a/js/src/vm/Runtime.cpp
+++ b/js/src/vm/Runtime.cpp
@@ -370,19 +370,16 @@ JSRuntime::addSizeOfIncludingThis(mozill
     rtSizes->contexts += cx->sizeOfExcludingThis(mallocSizeOf);
     rtSizes->temporary += cx->tempLifoAlloc().sizeOfExcludingThis(mallocSizeOf);
     rtSizes->interpreterStack += cx->interpreterStack().sizeOfExcludingThis(mallocSizeOf);
 #ifdef JS_TRACE_LOGGING
     if (cx->traceLogger)
         rtSizes->tracelogger += cx->traceLogger->sizeOfIncludingThis(mallocSizeOf);
 #endif
 
-    if (MathCache* cache = caches().maybeGetMathCache())
-        rtSizes->mathCache += cache->sizeOfIncludingThis(mallocSizeOf);
-
     rtSizes->uncompressedSourceCache +=
         caches().uncompressedSourceCache.sizeOfExcludingThis(mallocSizeOf);
 
     rtSizes->gc.nurseryCommitted += gc.nursery().sizeOfHeapCommitted();
     rtSizes->gc.nurseryMallocedBuffers += gc.nursery().sizeOfMallocedBuffers(mallocSizeOf);
     gc.storeBuffer().addSizeOfExcludingThis(mallocSizeOf, &rtSizes->gc);
 
     if (sharedImmutableStrings_) {
diff --git a/js/xpconnect/src/XPCJSRuntime.cpp b/js/xpconnect/src/XPCJSRuntime.cpp
--- a/js/xpconnect/src/XPCJSRuntime.cpp
+++ b/js/xpconnect/src/XPCJSRuntime.cpp
@@ -1882,20 +1882,16 @@ ReportJSRuntimeExplicitTreeStats(const J
         KIND_HEAP, rtStats.runtime.temporary,
         "Transient data (mostly parse nodes) held by the JSRuntime during "
         "compilation.");
 
     RREPORT_BYTES(rtPath + NS_LITERAL_CSTRING("runtime/interpreter-stack"),
         KIND_HEAP, rtStats.runtime.interpreterStack,
         "JS interpreter frames.");
 
-    RREPORT_BYTES(rtPath + NS_LITERAL_CSTRING("runtime/math-cache"),
-        KIND_HEAP, rtStats.runtime.mathCache,
-        "The math cache.");
-
     RREPORT_BYTES(rtPath + NS_LITERAL_CSTRING("runtime/shared-immutable-strings-cache"),
         KIND_HEAP, rtStats.runtime.sharedImmutableStringsCache,
         "Immutable strings (such as JS scripts' source text) shared across all JSRuntimes.");
 
     RREPORT_BYTES(rtPath + NS_LITERAL_CSTRING("runtime/shared-intl-data"),
         KIND_HEAP, rtStats.runtime.sharedIntlData,
         "Shared internationalization data.");
 

