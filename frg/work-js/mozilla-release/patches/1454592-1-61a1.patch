# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1524222247 -7200
#      Fri Apr 20 13:04:07 2018 +0200
# Node ID 15ad0dfe3a389ab162df3729ce54dcc0280823db
# Parent  b03b4bee868e467fa527a54b7867daea1902a97d
Bug 1454592 part 1 - Make some constructors constexpr. r=jonco

diff --git a/js/src/ds/BitArray.h b/js/src/ds/BitArray.h
--- a/js/src/ds/BitArray.h
+++ b/js/src/ds/BitArray.h
@@ -27,16 +27,18 @@ class BitArray
     static const size_t numSlots = nbits / bitsPerElement + (nbits % bitsPerElement == 0 ? 0 : 1);
     static const size_t paddingBits = (numSlots * bitsPerElement) - nbits;
     static_assert(paddingBits < bitsPerElement, "More padding bits than expected.");
     static const WordT paddingMask = WordT(-1) >> paddingBits;
 
     WordT map[numSlots];
 
   public:
+    constexpr BitArray() : map() {};
+
     void clear(bool value) {
         memset(map, value ? 0xFF : 0, sizeof(map));
         if (value)
             map[numSlots - 1] &= paddingMask;
     }
 
     inline bool get(size_t offset) const {
         size_t index;
diff --git a/js/src/gc/StoreBuffer.cpp b/js/src/gc/StoreBuffer.cpp
--- a/js/src/gc/StoreBuffer.cpp
+++ b/js/src/gc/StoreBuffer.cpp
@@ -110,33 +110,25 @@ StoreBuffer::addSizeOfExcludingThis(mozi
     sizes->storeBufferCells      += bufferCell.sizeOfExcludingThis(mallocSizeOf);
     sizes->storeBufferSlots      += bufferSlot.sizeOfExcludingThis(mallocSizeOf);
     sizes->storeBufferWholeCells += bufferWholeCell.sizeOfExcludingThis(mallocSizeOf);
     sizes->storeBufferGenerics   += bufferGeneric.sizeOfExcludingThis(mallocSizeOf);
 }
 
 ArenaCellSet ArenaCellSet::Empty;
 
-ArenaCellSet::ArenaCellSet()
-  : arena(nullptr)
-  , next(nullptr)
-#ifdef DEBUG
-  , minorGCNumberAtCreation(0)
-#endif
-{}
-
 ArenaCellSet::ArenaCellSet(Arena* arena, ArenaCellSet* next)
   : arena(arena)
   , next(next)
 #ifdef DEBUG
   , minorGCNumberAtCreation(arena->zone->runtimeFromMainThread()->gc.minorGCCount())
 #endif
 {
     MOZ_ASSERT(arena);
-    bits.clear(false);
+    MOZ_ASSERT(bits.isAllClear());
 }
 
 ArenaCellSet*
 StoreBuffer::WholeCellBuffer::allocateCellSet(Arena* arena)
 {
     Zone* zone = arena->zone;
     JSRuntime* rt = zone->runtimeFromMainThread();
     if (!rt->gc.nursery().isEnabled())
diff --git a/js/src/gc/StoreBuffer.h b/js/src/gc/StoreBuffer.h
--- a/js/src/gc/StoreBuffer.h
+++ b/js/src/gc/StoreBuffer.h
@@ -520,17 +520,23 @@ class ArenaCellSet
 
 #ifdef DEBUG
     // The minor GC number when this was created. This object should not survive
     // past the next minor collection.
     const uint64_t minorGCNumberAtCreation;
 #endif
 
     // Construct the empty sentinel object.
-    ArenaCellSet();
+    constexpr ArenaCellSet()
+      : arena(nullptr)
+      , next(nullptr)
+#ifdef DEBUG
+      , minorGCNumberAtCreation(0)
+#endif
+    {}
 
   public:
     ArenaCellSet(Arena* arena, ArenaCellSet* next);
 
     bool hasCell(const TenuredCell* cell) const {
         return hasCell(getCellIndex(cell));
     }
 
diff --git a/js/src/jit/VMFunctions.cpp b/js/src/jit/VMFunctions.cpp
--- a/js/src/jit/VMFunctions.cpp
+++ b/js/src/jit/VMFunctions.cpp
@@ -39,23 +39,16 @@ namespace jit {
 
 AutoDetectInvalidation::AutoDetectInvalidation(JSContext* cx, MutableHandleValue rval)
   : cx_(cx),
     ionScript_(GetTopJitJSScript(cx)->ionScript()),
     rval_(rval),
     disabled_(false)
 { }
 
-void
-VMFunction::addToFunctions()
-{
-    this->next = functions;
-    functions = this;
-}
-
 bool
 InvokeFunction(JSContext* cx, HandleObject obj, bool constructing, bool ignoresReturnValue,
                uint32_t argc, Value* argv, MutableHandleValue rval)
 {
     TraceLoggerThread* logger = TraceLoggerForCurrentThread(cx);
     TraceLogStartEvent(logger, TraceLogger_Call);
 
     AutoArrayRooter argvRoot(cx, argc + 1 + constructing, argv);
diff --git a/js/src/jit/VMFunctions.h b/js/src/jit/VMFunctions.h
--- a/js/src/jit/VMFunctions.h
+++ b/js/src/jit/VMFunctions.h
@@ -256,39 +256,41 @@ struct VMFunction
         outParamRootType(outParamRootType),
         outParam(outParam),
         returnType(returnType),
         extraValuesToPop(extraValuesToPop),
         expectTailCall(expectTailCall)
     { }
 
     VMFunction(const VMFunction& o)
-      : next(nullptr),
+      : next(functions),
         wrapped(o.wrapped),
 #ifdef JS_TRACE_LOGGING
         name_(o.name_),
 #endif
         argumentRootTypes(o.argumentRootTypes),
         argumentProperties(o.argumentProperties),
         argumentPassedInFloatRegs(o.argumentPassedInFloatRegs),
         explicitArgs(o.explicitArgs),
         outParamRootType(o.outParamRootType),
         outParam(o.outParam),
         returnType(o.returnType),
         extraValuesToPop(o.extraValuesToPop),
         expectTailCall(o.expectTailCall)
     {
+        // Add this to the global list of VMFunctions.
+        functions = this;
+
         // Check for valid failure/return type.
         MOZ_ASSERT_IF(outParam != Type_Void,
                       returnType == Type_Void ||
                       returnType == Type_Bool);
         MOZ_ASSERT(returnType == Type_Void ||
                    returnType == Type_Bool ||
                    returnType == Type_Object);
-        addToFunctions();
     }
 
     typedef const VMFunction* Lookup;
 
     static HashNumber hash(const VMFunction* f) {
         // The hash is based on the wrapped function, not the VMFunction*, to
         // avoid generating duplicate wrapper code.
         HashNumber hash = 0;
@@ -312,20 +314,16 @@ struct VMFunction
         MOZ_ASSERT(f1->argumentProperties == f2->argumentProperties);
         MOZ_ASSERT(f1->argumentPassedInFloatRegs == f2->argumentPassedInFloatRegs);
         MOZ_ASSERT(f1->outParam == f2->outParam);
         MOZ_ASSERT(f1->returnType == f2->returnType);
         MOZ_ASSERT(f1->argumentRootTypes == f2->argumentRootTypes);
         MOZ_ASSERT(f1->outParamRootType == f2->outParamRootType);
         return true;
     }
-
-  private:
-    // Add this to the global list of VMFunctions.
-    void addToFunctions();
 };
 
 template <class> struct TypeToDataType { /* Unexpected return type for a VMFunction. */ };
 template <> struct TypeToDataType<void> { static const DataType result = Type_Void; };
 template <> struct TypeToDataType<bool> { static const DataType result = Type_Bool; };
 template <> struct TypeToDataType<JSObject*> { static const DataType result = Type_Object; };
 template <> struct TypeToDataType<JSFunction*> { static const DataType result = Type_Object; };
 template <> struct TypeToDataType<NativeObject*> { static const DataType result = Type_Object; };
diff --git a/js/src/vm/Printer.cpp b/js/src/vm/Printer.cpp
--- a/js/src/vm/Printer.cpp
+++ b/js/src/vm/Printer.cpp
@@ -41,21 +41,16 @@ private:
 
     js::GenericPrinter& printer;
 };
 
 }
 
 namespace js {
 
-GenericPrinter::GenericPrinter()
-  : hadOOM_(false)
-{
-}
-
 void
 GenericPrinter::reportOutOfMemory()
 {
     if (hadOOM_)
         return;
     hadOOM_ = true;
 }
 
@@ -392,25 +387,22 @@ QuoteString(JSContext* cx, JSString* str
 
 Fprinter::Fprinter(FILE* fp)
   : file_(nullptr),
     init_(false)
 {
     init(fp);
 }
 
-Fprinter::Fprinter()
-  : file_(nullptr),
-    init_(false)
-{ }
-
+#ifdef DEBUG
 Fprinter::~Fprinter()
 {
     MOZ_ASSERT_IF(init_, !file_);
 }
+#endif
 
 bool
 Fprinter::init(const char* path)
 {
     MOZ_ASSERT(!file_);
     file_ = fopen(path, "w");
     if (!file_)
         return false;
diff --git a/js/src/vm/Printer.h b/js/src/vm/Printer.h
--- a/js/src/vm/Printer.h
+++ b/js/src/vm/Printer.h
@@ -25,17 +25,19 @@ class LifoAlloc;
 // This class is useful to make generic printers which can work either with a
 // file backend, with a buffer allocated with an JSContext or a link-list
 // of chunks allocated with a LifoAlloc.
 class GenericPrinter
 {
   protected:
     bool                  hadOOM_;     // whether reportOutOfMemory() has been called.
 
-    GenericPrinter();
+    constexpr GenericPrinter()
+      : hadOOM_(false)
+    {}
 
   public:
     // Puts |len| characters from |s| at the current position and
     // return true on success, false on failure.
     virtual bool put(const char* s, size_t len) = 0;
     virtual void flush() { /* Do nothing */ }
 
     inline bool put(const char* s) {
@@ -134,18 +136,25 @@ class Sprinter final : public GenericPri
 class Fprinter final : public GenericPrinter
 {
   private:
     FILE*                   file_;
     bool                    init_;
 
   public:
     explicit Fprinter(FILE* fp);
-    Fprinter();
+
+    constexpr Fprinter()
+      : file_(nullptr),
+        init_(false)
+    {}
+
+#ifdef DEBUG
     ~Fprinter();
+#endif
 
     // Initialize this printer, returns false on error.
     MOZ_MUST_USE bool init(const char* path);
     void init(FILE* fp);
     bool isInitialized() const {
         return file_ != nullptr;
     }
     void flush() override;
