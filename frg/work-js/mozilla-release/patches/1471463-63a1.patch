# HG changeset patch
# User Jeff Walden <jwalden@mit.edu>
# Date 1530131157 25200
# Node ID e20c1d6852e4fb05f4fce71523d698cb9ebf28fe
# Parent  3224a50463e3de16def6fd0d77389e2f2201b379
Bug 1471463 - Move TokenStreamCharsBase::tokenbuf and related functions into a new TokenStreamCharsShared base class, make the character type always char16_t, and rename it/them to 'charBuffer' for clarity.  r=arai

diff --git a/js/src/frontend/Parser.cpp b/js/src/frontend/Parser.cpp
--- a/js/src/frontend/Parser.cpp
+++ b/js/src/frontend/Parser.cpp
@@ -9089,43 +9089,42 @@ ParseNode*
 Parser<FullParseHandler, CharT>::newRegExp()
 {
     MOZ_ASSERT(!options().selfHostingMode);
 
     static_assert(mozilla::IsSame<CharT, char16_t>::value,
                   "code below will need changing for UTF-8 handling");
 
     // Create the regexp and check its syntax.
-    const CharT* chars = tokenStream.getTokenbuf().begin();
-    size_t length = tokenStream.getTokenbuf().length();
+    const auto& chars = tokenStream.getCharBuffer();
     RegExpFlag flags = anyChars.currentToken().regExpFlags();
 
     Rooted<RegExpObject*> reobj(context);
-    reobj = RegExpObject::create(context, chars, length, flags, anyChars, alloc, TenuredObject);
+    reobj = RegExpObject::create(context, chars.begin(), chars.length(), flags, anyChars, alloc,
+                                 TenuredObject);
     if (!reobj)
         return null();
 
     return handler.newRegExp(reobj, pos(), *this);
 }
 
 template <typename CharT>
 SyntaxParseHandler::Node
 Parser<SyntaxParseHandler, CharT>::newRegExp()
 {
     MOZ_ASSERT(!options().selfHostingMode);
 
     static_assert(mozilla::IsSame<CharT, char16_t>::value,
                   "code below will need changing for UTF-8 handling");
 
     // Only check the regexp's syntax, but don't create a regexp object.
-    const CharT* chars = tokenStream.getTokenbuf().begin();
-    size_t length = tokenStream.getTokenbuf().length();
+    const auto& chars = tokenStream.getCharBuffer();
     RegExpFlag flags = anyChars.currentToken().regExpFlags();
 
-    mozilla::Range<const CharT> source(chars, length);
+    mozilla::Range<const CharT> source(chars.begin(), chars.length());
     if (!js::irregexp::ParsePatternSyntax(anyChars, alloc, source, flags & UnicodeFlag))
         return null();
 
     return handler.newRegExp(SyntaxParseHandler::NodeGeneric, pos(), *this);
 }
 
 template <class ParseHandler, typename CharT>
 typename ParseHandler::Node
diff --git a/js/src/frontend/TokenStream.cpp b/js/src/frontend/TokenStream.cpp
--- a/js/src/frontend/TokenStream.cpp
+++ b/js/src/frontend/TokenStream.cpp
@@ -8,20 +8,20 @@
 
 #include "frontend/TokenStream.h"
 
 #include "mozilla/ArrayUtils.h"
 #include "mozilla/Attributes.h"
 #include "mozilla/IntegerTypeTraits.h"
 #include "mozilla/Likely.h"
 #include "mozilla/MemoryChecking.h"
-#include "mozilla/PodOperations.h"
 #include "mozilla/ScopeExit.h"
 #include "mozilla/TextUtils.h"
 
+#include <algorithm>
 #include <ctype.h>
 #include <stdarg.h>
 #include <stdio.h>
 #include <string.h>
 #include <utility>
 
 #include "jsexn.h"
 #include "jsnum.h"
@@ -39,17 +39,16 @@
 #include "vm/Realm.h"
 
 using mozilla::ArrayLength;
 using mozilla::AssertedCast;
 using mozilla::IsAscii;
 using mozilla::IsAsciiAlpha;
 using mozilla::IsAsciiDigit;
 using mozilla::MakeScopeExit;
-using mozilla::PodCopy;
 
 struct ReservedWordInfo
 {
     const char* chars;         // C string with reserved word text
     js::frontend::TokenKind tokentype;
 };
 
 static const ReservedWordInfo reservedWords[] = {
@@ -430,18 +429,18 @@ TokenStreamAnyChars::TokenStreamAnyChars
     isExprEnding[size_t(TokenKind::Rp)] = true;
     isExprEnding[size_t(TokenKind::Rb)] = true;
     isExprEnding[size_t(TokenKind::Rc)] = true;
 }
 
 template<typename CharT>
 TokenStreamCharsBase<CharT>::TokenStreamCharsBase(JSContext* cx, const CharT* chars, size_t length,
                                                   size_t startOffset)
-  : sourceUnits(chars, length, startOffset),
-    tokenbuf(cx)
+  : TokenStreamCharsShared(cx),
+    sourceUnits(chars, length, startOffset)
 {}
 
 template<typename CharT, class AnyCharsAccess>
 TokenStreamSpecific<CharT, AnyCharsAccess>::TokenStreamSpecific(JSContext* cx,
                                                                 const ReadOnlyCompileOptions& options,
                                                                 const CharT* base, size_t length)
   : TokenStreamChars<CharT, AnyCharsAccess>(cx, base, length, options.scriptSourceOffset)
 {}
@@ -1161,28 +1160,27 @@ TokenStreamSpecific<CharT, AnyCharsAcces
     bool res = getDisplayURL(isMultiline, shouldWarnDeprecated) &&
                getSourceMappingURL(isMultiline, shouldWarnDeprecated);
     if (!res)
         badToken();
 
     return res;
 }
 
-template<>
 MOZ_MUST_USE bool
-TokenStreamCharsBase<char16_t>::copyTokenbufTo(JSContext* cx,
-                                               UniquePtr<char16_t[], JS::FreePolicy>* destination)
+TokenStreamCharsShared::copyCharBufferTo(JSContext* cx,
+                                         UniquePtr<char16_t[], JS::FreePolicy>* destination)
 {
-    size_t length = tokenbuf.length();
+    size_t length = charBuffer.length();
 
     *destination = cx->make_pod_array<char16_t>(length + 1);
     if (!*destination)
         return false;
 
-    PodCopy(destination->get(), tokenbuf.begin(), length);
+    std::copy(charBuffer.begin(), charBuffer.end(), destination->get());
     (*destination)[length] = '\0';
     return true;
 }
 
 template<typename CharT, class AnyCharsAccess>
 MOZ_MUST_USE bool
 TokenStreamSpecific<CharT, AnyCharsAccess>::getDirective(bool isMultiline,
                                                          bool shouldWarnDeprecated,
@@ -1204,17 +1202,17 @@ TokenStreamSpecific<CharT, AnyCharsAcces
         return true;
 
     if (shouldWarnDeprecated) {
         if (!warning(JSMSG_DEPRECATED_PRAGMA, errorMsgPragma))
             return false;
     }
 
     sourceUnits.skipCodeUnits(directiveLength);
-    tokenbuf.clear();
+    charBuffer.clear();
 
     do {
         int32_t unit = peekCodeUnit();
         if (unit == EOF)
             break;
 
         if (MOZ_LIKELY(isAsciiCodePoint(unit))) {
             if (unicode::IsSpaceOrBOM2(unit))
@@ -1225,42 +1223,42 @@ TokenStreamSpecific<CharT, AnyCharsAcces
             // Debugging directives can occur in both single- and multi-line
             // comments. If we're currently inside a multi-line comment, we
             // also must recognize multi-line comment terminators.
             if (isMultiline && unit == '*' && peekCodeUnit() == '/') {
                 ungetCodeUnit('*');
                 break;
             }
 
-            if (!tokenbuf.append(unit))
+            if (!charBuffer.append(unit))
                 return false;
 
             continue;
         }
 
         int32_t codePoint;
         if (!getCodePoint(&codePoint))
             return false;
 
         if (unicode::IsSpaceOrBOM2(codePoint)) {
             ungetNonAsciiNormalizedCodePoint(codePoint);
             break;
         }
 
-        if (!appendCodePointToTokenbuf(codePoint))
+        if (!appendCodePointToCharBuffer(codePoint))
             return false;
     } while (true);
 
-    if (tokenbuf.empty()) {
+    if (charBuffer.empty()) {
         // The directive's URL was missing, but comments can contain anything,
         // so it isn't an error.
         return true;
     }
 
-    return copyTokenbufTo(anyCharsAccess().cx, destination);
+    return copyCharBufferTo(anyCharsAccess().cx, destination);
 }
 
 template<typename CharT, class AnyCharsAccess>
 bool
 TokenStreamSpecific<CharT, AnyCharsAccess>::getDisplayURL(bool isMultiline,
                                                           bool shouldWarnDeprecated)
 {
     // Match comments of the form "//# sourceURL=<url>" or
@@ -1330,58 +1328,57 @@ GeneralTokenStreamChars<CharT, AnyCharsA
     // erroneous token has been seen, sourceUnits will not be consulted again.
     // This is true because the parser will deal with the illegal token by
     // aborting parsing immediately.
     sourceUnits.poisonInDebug();
 
     return false;
 };
 
-template<>
 MOZ_MUST_USE bool
-TokenStreamCharsBase<char16_t>::appendCodePointToTokenbuf(uint32_t codePoint)
+TokenStreamCharsShared::appendCodePointToCharBuffer(uint32_t codePoint)
 {
     char16_t units[2];
     unsigned numUnits = 0;
     unicode::UTF16Encode(codePoint, units, &numUnits);
 
     MOZ_ASSERT(numUnits == 1 || numUnits == 2,
                "UTF-16 code points are only encoded in one or two units");
 
-    if (!tokenbuf.append(units[0]))
+    if (!charBuffer.append(units[0]))
         return false;
 
     if (numUnits == 1)
         return true;
 
-    return tokenbuf.append(units[1]);
+    return charBuffer.append(units[1]);
 }
 
 template<typename CharT, class AnyCharsAccess>
 bool
-TokenStreamSpecific<CharT, AnyCharsAccess>::putIdentInTokenbuf(const CharT* identStart)
+TokenStreamSpecific<CharT, AnyCharsAccess>::putIdentInCharBuffer(const CharT* identStart)
 {
     const CharT* const originalAddress = sourceUnits.addressOfNextCodeUnit();
     sourceUnits.setAddressOfNextCodeUnit(identStart);
 
     auto restoreNextRawCharAddress =
         MakeScopeExit([this, originalAddress]() {
             this->sourceUnits.setAddressOfNextCodeUnit(originalAddress);
         });
 
-    tokenbuf.clear();
+    charBuffer.clear();
     do {
         int32_t unit = getCodeUnit();
         if (unit == EOF)
             break;
 
         uint32_t codePoint;
         if (MOZ_LIKELY(isAsciiCodePoint(unit))) {
             if (MOZ_LIKELY(unicode::IsIdentifierPart(char16_t(unit)))) {
-                if (!tokenbuf.append(unit))
+                if (!charBuffer.append(unit))
                     return false;
 
                 continue;
             }
 
             if (unit != '\\' || !matchUnicodeEscapeIdent(&codePoint))
                 break;
         } else {
@@ -1400,17 +1397,17 @@ TokenStreamSpecific<CharT, AnyCharsAcces
                 // revert line/column updates.  The ASCII code path never
                 // updates line/column state, so only Unicode separators gotten
                 // by |getNonAsciiCodePoint| require this.
                 anyCharsAccess().undoInternalUpdateLineInfoForEOL();
             }
             break;
         }
 
-        if (!appendCodePointToTokenbuf(codePoint))
+        if (!appendCodePointToCharBuffer(codePoint))
             return false;
     } while (true);
 
     return true;
 }
 
 template<typename CharT, class AnyCharsAccess>
 MOZ_MUST_USE bool
@@ -1458,21 +1455,21 @@ TokenStreamSpecific<CharT, AnyCharsAcces
         }
     }
 
     const CharT* chars;
     size_t length;
     if (escaping == IdentifierEscapes::SawUnicodeEscape) {
         // Identifiers containing Unicode escapes have to be converted into
         // tokenbuf before atomizing.
-        if (!putIdentInTokenbuf(identStart))
+        if (!putIdentInCharBuffer(identStart))
             return false;
 
-        chars = tokenbuf.begin();
-        length = tokenbuf.length();
+        chars = charBuffer.begin();
+        length = charBuffer.length();
     } else {
         // Escape-free identifiers can be created directly from sourceUnits.
         chars = identStart;
         length = sourceUnits.addressOfNextCodeUnit() - identStart;
 
         // Represent reserved words lacking escapes as reserved word tokens.
         if (const ReservedWordInfo* rw = FindReservedWord(chars, length)) {
             noteBadToken.release();
@@ -1681,30 +1678,30 @@ TokenStreamSpecific<CharT, AnyCharsAcces
     return true;
 }
 
 template<typename CharT, class AnyCharsAccess>
 MOZ_MUST_USE bool
 TokenStreamSpecific<CharT, AnyCharsAccess>::regexpLiteral(TokenStart start, TokenKind* out)
 {
     MOZ_ASSERT(sourceUnits.previousCodeUnit() == '/');
-    tokenbuf.clear();
+    charBuffer.clear();
 
     auto ProcessNonAsciiCodePoint = [this](CharT lead) {
         int32_t codePoint;
         if (!this->getNonAsciiCodePoint(lead, &codePoint))
             return false;
 
         if (codePoint == '\n') {
             this->ungetLineTerminator();
             this->reportError(JSMSG_UNTERMINATED_REGEXP);
             return false;
         }
 
-        return this->appendCodePointToTokenbuf(codePoint);
+        return this->appendCodePointToCharBuffer(codePoint);
     };
 
     auto ReportUnterminatedRegExp = [this](CharT unit) {
         this->ungetCodeUnit(unit);
         this->error(JSMSG_UNTERMINATED_REGEXP);
     };
 
     bool inCharClass = false;
@@ -1712,17 +1709,17 @@ TokenStreamSpecific<CharT, AnyCharsAcces
         int32_t unit = getCodeUnit();
         if (unit == EOF) {
             ReportUnterminatedRegExp(unit);
             return badToken();
         }
 
         if (MOZ_LIKELY(isAsciiCodePoint(unit))) {
             if (unit == '\\')  {
-                if (!tokenbuf.append(unit))
+                if (!charBuffer.append(unit))
                     return badToken();
 
                 unit = getCodeUnit();
                 if (unit == EOF) {
                     ReportUnterminatedRegExp(unit);
                     return badToken();
                 }
 
@@ -1743,17 +1740,17 @@ TokenStreamSpecific<CharT, AnyCharsAcces
                 break;
             }
 
             if (unit == '\r' || unit == '\n') {
                 ReportUnterminatedRegExp(unit);
                 return badToken();
             }
 
-            if (!tokenbuf.append(unit))
+            if (!charBuffer.append(unit))
                 return badToken();
         } else {
             if (!ProcessNonAsciiCodePoint(unit))
                 return badToken();
         }
     } while (true);
 
     int32_t unit;
@@ -2301,17 +2298,17 @@ TokenStreamSpecific<CharT, AnyCharsAcces
 {
     MOZ_ASSERT(untilChar == '\'' || untilChar == '"' || untilChar == '`',
                "unexpected string/template literal delimiter");
 
     bool parsingTemplate = (untilChar == '`');
     bool templateHead = false;
 
     TokenStart start(sourceUnits, -1);
-    tokenbuf.clear();
+    charBuffer.clear();
 
     // Run the bad-token code for every path out of this function except the
     // one success-case.
     auto noteBadToken = MakeScopeExit([this]() {
         this->badToken();
     });
 
     auto ReportPrematureEndOfLiteral = [this, untilChar](unsigned errnum) {
@@ -2359,26 +2356,26 @@ TokenStreamSpecific<CharT, AnyCharsAcces
                 anyCharsAccess().updateFlagsForEOL();
 
                 codePoint = unit;
             } else {
                 if (!getNonAsciiCodePoint(unit, &codePoint))
                     return false;
             }
 
-            if (!appendCodePointToTokenbuf(codePoint))
+            if (!appendCodePointToCharBuffer(codePoint))
                 return false;
 
             continue;
         }
 
         if (unit == '\\') {
             // When parsing templates, we don't immediately report errors for
             // invalid escapes; these are handled by the parser.  We don't
-            // append to tokenbuf in those cases because it won't be read.
+            // append to charBuffer in those cases because it won't be read.
             unit = getCodeUnit();
             if (unit == EOF) {
                 ReportPrematureEndOfLiteral(JSMSG_EOF_IN_ESCAPE_IN_LITERAL);
                 return false;
             }
 
             // Non-ASCII |unit| isn't handled by code after this, so dedicate
             // an unlikely special-case to it and then continue.
@@ -2387,17 +2384,17 @@ TokenStreamSpecific<CharT, AnyCharsAcces
                 if (!getNonAsciiCodePoint(unit, &codePoint))
                     return false;
 
                 // If we consumed U+2028 LINE SEPARATOR or U+2029 PARAGRAPH
                 // SEPARATOR, they'll be normalized to '\n'.  '\' followed by
                 // LineContinuation represents no code points, so don't append
                 // in this case.
                 if (codePoint != '\n') {
-                    if (!tokenbuf.append(unit))
+                    if (!charBuffer.append(unit))
                         return false;
                 }
 
                 continue;
             }
 
             switch (static_cast<CharT>(unit)) {
               case 'b': unit = '\b'; break;
@@ -2496,17 +2493,17 @@ TokenStreamSpecific<CharT, AnyCharsAcces
 
                         first = false;
                     } while (true);
 
                     if (!valid)
                         continue;
 
                     MOZ_ASSERT(code <= unicode::NonBMPMax);
-                    if (!appendCodePointToTokenbuf(code))
+                    if (!appendCodePointToCharBuffer(code))
                         return false;
 
                     continue;
                 } // end of delimited Unicode escape handling
 
                 // Otherwise it must be a fixed-length \uXXXX Unicode escape.
                 // If it isn't, this is usually an error -- but if this is a
                 // template literal, we must defer error reporting because
@@ -2603,17 +2600,17 @@ TokenStreamSpecific<CharT, AnyCharsAcces
                     }
                 }
 
                 unit = char16_t(val);
                 break;
               } // default
             }
 
-            if (!tokenbuf.append(unit))
+            if (!charBuffer.append(unit))
                 return false;
 
             continue;
         } // (unit == '\\')
 
         if (unit == '\r' || unit == '\n') {
             if (!parsingTemplate) {
                 // String literals don't allow ASCII line breaks.
@@ -2634,21 +2631,21 @@ TokenStreamSpecific<CharT, AnyCharsAcces
                 return false;
 
             anyCharsAccess().updateFlagsForEOL();
         } else if (parsingTemplate && unit == '$' && matchCodeUnit('{')) {
             templateHead = true;
             break;
         }
 
-        if (!tokenbuf.append(unit))
+        if (!charBuffer.append(unit))
             return false;
     }
 
-    JSAtom* atom = atomizeChars(anyCharsAccess().cx, tokenbuf.begin(), tokenbuf.length());
+    JSAtom* atom = atomizeChars(anyCharsAccess().cx, charBuffer.begin(), charBuffer.length());
     if (!atom)
         return false;
 
     noteBadToken.release();
 
     MOZ_ASSERT_IF(!parsingTemplate, !templateHead);
 
     TokenKind kind = !parsingTemplate
diff --git a/js/src/frontend/TokenStream.h b/js/src/frontend/TokenStream.h
--- a/js/src/frontend/TokenStream.h
+++ b/js/src/frontend/TokenStream.h
@@ -330,33 +330,53 @@ struct Token
     };
     friend class TokenStreamShared;
 
   public:
     // WARNING: TokenStreamPosition assumes that the only GC things a Token
     //          includes are atoms.  DON'T ADD NON-ATOM GC THING POINTERS HERE
     //          UNLESS YOU ADD ADDITIONAL ROOTING TO THAT CLASS.
 
-    TokenKind           type;           // char value or above enumerator
-    TokenPos            pos;            // token position in file
+    /** The type of this token. */
+    TokenKind type;
+
+    /** The token's position in the overall script. */
+    TokenPos pos;
+
     union {
       private:
         friend struct Token;
-        PropertyName*   name;          // non-numeric atom
-        JSAtom*         atom;          // potentially-numeric atom
+
+        /** Non-numeric atom. */
+        PropertyName* name;
+
+        /** Potentially-numeric atom. */
+        JSAtom* atom;
+
         struct {
-            double      value;          // floating point number
-            DecimalPoint decimalPoint;  // literal contains '.'
+            /** Numeric literal's value. */
+            double value;
+
+            /** Does the numeric literal contain a '.'? */
+            DecimalPoint decimalPoint;
         } number;
-        RegExpFlag      reflags;        // regexp flags; use tokenbuf to access
-                                        //   regexp chars
+
+        /** Regular expression flags; use charBuffer to access source chars. */
+        RegExpFlag reflags;
     } u;
+
 #ifdef DEBUG
-    Modifier modifier;                  // Modifier used to get this token
-    ModifierException modifierException; // Exception for this modifier
+    /** The modifier used to get this token. */
+    Modifier modifier;
+
+    /**
+     * Exception for this modifier to permit modifier mismatches in certain
+     * situations.
+     */
+    ModifierException modifierException;
 #endif
 
     // Mutators
 
     void setName(PropertyName* name) {
         MOZ_ASSERT(type == TokenKind::Name);
         u.name = name;
     }
@@ -1046,44 +1066,65 @@ class SourceUnits
 
     /** Limit for quick bounds check. */
     const CharT* limit_;
 
     /** Next char to get. */
     const CharT* ptr;
 };
 
+class TokenStreamCharsShared
+{
+    // Using char16_t (not CharT) is a simplifying decision that hopefully
+    // eliminates the need for a UTF-8 regular expression parser and makes
+    // |copyCharBufferTo| markedly simpler.
+    using CharBuffer = Vector<char16_t, 32>;
+
+  protected:
+    /**
+     * Character buffer transiently used to store sequences of identifier or
+     * string code points when such can't be directly processed from the
+     * original source text (e.g. because it contains escapes).
+     */
+    CharBuffer charBuffer;
+
+  protected:
+    explicit TokenStreamCharsShared(JSContext* cx)
+      : charBuffer(cx)
+    {}
+
+    MOZ_MUST_USE bool appendCodePointToCharBuffer(uint32_t codePoint);
+
+    MOZ_MUST_USE bool copyCharBufferTo(JSContext* cx,
+                                       UniquePtr<char16_t[], JS::FreePolicy>* destination);
+
+  public:
+    CharBuffer& getCharBuffer() { return charBuffer; }
+};
+
 template<typename CharT>
 class TokenStreamCharsBase
+  : public TokenStreamCharsShared
 {
   protected:
     void ungetCodeUnit(int32_t c) {
         if (c == EOF)
             return;
 
         sourceUnits.ungetCodeUnit();
     }
 
   public:
-    using CharBuffer = Vector<CharT, 32>;
-
     TokenStreamCharsBase(JSContext* cx, const CharT* chars, size_t length, size_t startOffset);
 
     static MOZ_ALWAYS_INLINE JSAtom*
     atomizeChars(JSContext* cx, const CharT* chars, size_t length);
 
-    const CharBuffer& getTokenbuf() const { return tokenbuf; }
-
-    MOZ_MUST_USE bool copyTokenbufTo(JSContext* cx,
-                                     UniquePtr<char16_t[], JS::FreePolicy>* destination);
-
     using SourceUnits = frontend::SourceUnits<CharT>;
 
-    MOZ_MUST_USE bool appendCodePointToTokenbuf(uint32_t codePoint);
-
     // |expect| cannot be an EOL char.
     bool matchCodeUnit(int32_t expect) {
         MOZ_ASSERT(expect != EOF, "shouldn't be matching EOFs");
         MOZ_ASSERT(!SourceUnits::isRawEOLChar(expect));
         return MOZ_LIKELY(!sourceUnits.atEnd()) && sourceUnits.matchCodeUnit(expect);
     }
 
   protected:
@@ -1097,30 +1138,30 @@ class TokenStreamCharsBase
 #ifdef DEBUG
         CharT next =
 #endif
             sourceUnits.getCodeUnit();
         MOZ_ASSERT(next == unit, "must be consuming the correct unit");
     }
 
     MOZ_MUST_USE bool
-    fillWithTemplateStringContents(CharBuffer& charbuf, const CharT* cur, const CharT* end) {
+    fillCharBufferWithTemplateStringContents(const CharT* cur, const CharT* end) {
         while (cur < end) {
             // U+2028 LINE SEPARATOR and U+2029 PARAGRAPH SEPARATOR are
             // interpreted literally inside template literal contents; only
             // literal CRLF sequences are normalized to '\n'.  See
             // <https://tc39.github.io/ecma262/#sec-static-semantics-tv-and-trv>.
             CharT ch = *cur;
             if (ch == '\r') {
                 ch = '\n';
                 if ((cur + 1 < end) && (*(cur + 1) == '\n'))
                     cur++;
             }
 
-            if (!charbuf.append(ch))
+            if (!charBuffer.append(ch))
                 return false;
 
             cur++;
         }
 
         return true;
     }
 
@@ -1131,19 +1172,16 @@ class TokenStreamCharsBase
      */
     static constexpr MOZ_ALWAYS_INLINE MOZ_MUST_USE bool isAsciiCodePoint(CharT unit) {
         return mozilla::IsAscii(unit);
     }
 
   protected:
     /** Code units in the source code being tokenized. */
     SourceUnits sourceUnits;
-
-    /** Current token string buffer. */
-    CharBuffer tokenbuf;
 };
 
 template<>
 /* static */ MOZ_ALWAYS_INLINE JSAtom*
 TokenStreamCharsBase<char16_t>::atomizeChars(JSContext* cx, const char16_t* chars, size_t length)
 {
     return AtomizeChars(cx, chars, length);
 }
@@ -1168,17 +1206,17 @@ class TokenStart
 
     uint32_t offset() const { return startOffset_; }
 };
 
 template<typename CharT, class AnyCharsAccess>
 class GeneralTokenStreamChars
   : public TokenStreamCharsBase<CharT>
 {
-    using CharsSharedBase = TokenStreamCharsBase<CharT>;
+    using CharsBase = TokenStreamCharsBase<CharT>;
 
     Token* newTokenInternal(TokenKind kind, TokenStart start, TokenKind* out);
 
     /**
      * Allocates a new Token from the given offset to the current offset,
      * ascribes it the given kind, and sets |*out| to that kind.
      */
     Token* newToken(TokenKind kind, TokenStart start, TokenStreamShared::Modifier modifier,
@@ -1196,22 +1234,22 @@ class GeneralTokenStreamChars
 
         return token;
     }
 
     uint32_t matchUnicodeEscape(uint32_t* codePoint);
     uint32_t matchExtendedUnicodeEscape(uint32_t* codePoint);
 
   protected:
-    using typename CharsSharedBase::SourceUnits;
+    using typename CharsBase::SourceUnits;
 
-    using CharsSharedBase::sourceUnits;
+    using CharsBase::sourceUnits;
 
-  public:
-    using CharsSharedBase::CharsSharedBase;
+  protected:
+    using CharsBase::CharsBase;
 
     TokenStreamAnyChars& anyCharsAccess() {
         return AnyCharsAccess::anyChars(this);
     }
 
     const TokenStreamAnyChars& anyCharsAccess() const {
         return AnyCharsAccess::anyChars(this);
     }
@@ -1280,61 +1318,61 @@ class GeneralTokenStreamChars
 
         anyCharsAccess().flags.isEOF = true;
         return EOF;
     }
 
     void ungetCodeUnit(int32_t c) {
         MOZ_ASSERT_IF(c == EOF, anyCharsAccess().flags.isEOF);
 
-        CharsSharedBase::ungetCodeUnit(c);
+        CharsBase::ungetCodeUnit(c);
     }
 
     void ungetChar(int32_t c);
 
     /**
      * Consume characters til EOL/EOF following the start of a single-line
      * comment, without consuming the EOL/EOF.
      */
     void consumeRestOfSingleLineComment();
 
     MOZ_MUST_USE MOZ_ALWAYS_INLINE bool updateLineInfoForEOL() {
         return anyCharsAccess().internalUpdateLineInfoForEOL(sourceUnits.offset());
     }
 
-  protected:
     uint32_t matchUnicodeEscapeIdStart(uint32_t* codePoint);
     bool matchUnicodeEscapeIdent(uint32_t* codePoint);
 };
 
 template<typename CharT, class AnyCharsAccess> class TokenStreamChars;
 
 template<class AnyCharsAccess>
 class TokenStreamChars<char16_t, AnyCharsAccess>
   : public GeneralTokenStreamChars<char16_t, AnyCharsAccess>
 {
   private:
-    using Self = TokenStreamChars<char16_t, AnyCharsAccess>;
+    using CharsBase = TokenStreamCharsBase<char16_t>;
     using GeneralCharsBase = GeneralTokenStreamChars<char16_t, AnyCharsAccess>;
-    using CharsSharedBase = TokenStreamCharsBase<char16_t>;
+    using Self = TokenStreamChars<char16_t, AnyCharsAccess>;
 
     using GeneralCharsBase::asSpecific;
 
     using typename GeneralCharsBase::TokenStreamSpecific;
 
   protected:
     using GeneralCharsBase::anyCharsAccess;
     using GeneralCharsBase::getCodeUnit;
-    using CharsSharedBase::isAsciiCodePoint;
-    using GeneralCharsBase::sourceUnits;
-    using CharsSharedBase::ungetCodeUnit;
+    using CharsBase::isAsciiCodePoint;
+    using CharsBase::sourceUnits;
+    using GeneralCharsBase::ungetCodeUnit;
     using GeneralCharsBase::updateLineInfoForEOL;
 
     using typename GeneralCharsBase::SourceUnits;
 
+  protected:
     using GeneralCharsBase::GeneralCharsBase;
 
     // Try to get the next code point, normalizing '\r', '\r\n', '\n', and the
     // Unicode line/paragraph separators into '\n'.  Also updates internal
     // line-counter state.  Return true on success and store the character in
     // |*c|.  Return false and leave |*c| undefined on failure.
     MOZ_MUST_USE bool getCodePoint(int32_t* cp);
 
@@ -1472,70 +1510,68 @@ class TokenStreamChars<char16_t, AnyChar
 //
 template<typename CharT, class AnyCharsAccess>
 class MOZ_STACK_CLASS TokenStreamSpecific
   : public TokenStreamChars<CharT, AnyCharsAccess>,
     public TokenStreamShared,
     public ErrorReporter
 {
   public:
-    using CharsBase = TokenStreamChars<CharT, AnyCharsAccess>;
+    using CharsBase = TokenStreamCharsBase<CharT>;
     using GeneralCharsBase = GeneralTokenStreamChars<CharT, AnyCharsAccess>;
-    using CharsSharedBase = TokenStreamCharsBase<CharT>;
+    using SpecializedCharsBase = TokenStreamChars<CharT, AnyCharsAccess>;
 
     using Position = TokenStreamPosition<CharT>;
 
     // Anything inherited through a base class whose type depends upon this
     // class's template parameters can only be accessed through a dependent
     // name: prefixed with |this|, by explicit qualification, and so on.  (This
     // is so that references to inherited fields are statically distinguishable
     // from references to names outside of the class.)  This is tedious and
     // onerous.
     //
     // As an alternative, we directly add every one of these functions to this
     // class, using explicit qualification to address the dependent-name
     // problem.  |this| or other qualification is no longer necessary -- at
     // cost of this ever-changing laundry list of |using|s.  So it goes.
   public:
     using GeneralCharsBase::anyCharsAccess;
-    using CharsSharedBase::getTokenbuf;
 
   private:
-    using typename CharsSharedBase::CharBuffer;
-    using typename CharsSharedBase::SourceUnits;
+    using typename CharsBase::SourceUnits;
 
   private:
-    using CharsSharedBase::appendCodePointToTokenbuf;
-    using CharsSharedBase::atomizeChars;
+    using TokenStreamCharsShared::appendCodePointToCharBuffer;
+    using CharsBase::atomizeChars;
     using GeneralCharsBase::badToken;
-    using CharsSharedBase::consumeKnownCodeUnit;
+    using TokenStreamCharsShared::charBuffer;
+    using CharsBase::consumeKnownCodeUnit;
     using GeneralCharsBase::consumeRestOfSingleLineComment;
-    using CharsSharedBase::copyTokenbufTo;
-    using CharsSharedBase::fillWithTemplateStringContents;
-    using CharsBase::getChar;
-    using CharsBase::getCodePoint;
+    using TokenStreamCharsShared::copyCharBufferTo;
+    using CharsBase::fillCharBufferWithTemplateStringContents;
+    using SpecializedCharsBase::getChar;
+    using SpecializedCharsBase::getCodePoint;
     using GeneralCharsBase::getCodeUnit;
-    using CharsBase::getFullAsciiCodePoint;
-    using CharsBase::getNonAsciiCodePoint;
-    using CharsSharedBase::isAsciiCodePoint;
-    using CharsSharedBase::matchCodeUnit;
+    using SpecializedCharsBase::getFullAsciiCodePoint;
+    using SpecializedCharsBase::getNonAsciiCodePoint;
+    using CharsBase::isAsciiCodePoint;
+    using CharsBase::matchCodeUnit;
     using GeneralCharsBase::matchUnicodeEscapeIdent;
     using GeneralCharsBase::matchUnicodeEscapeIdStart;
     using GeneralCharsBase::newAtomToken;
     using GeneralCharsBase::newNameToken;
     using GeneralCharsBase::newNumberToken;
     using GeneralCharsBase::newRegExpToken;
     using GeneralCharsBase::newSimpleToken;
-    using CharsSharedBase::peekCodeUnit;
-    using CharsSharedBase::sourceUnits;
-    using CharsSharedBase::tokenbuf;
+    using CharsBase::peekCodeUnit;
+    using CharsBase::sourceUnits;
     using GeneralCharsBase::ungetChar;
-    using CharsBase::ungetCodePointIgnoreEOL;
-    using CharsSharedBase::ungetCodeUnit;
-    using CharsBase::ungetNonAsciiNormalizedCodePoint;
+    using SpecializedCharsBase::ungetCodePointIgnoreEOL;
+    using GeneralCharsBase::ungetCodeUnit;
+    using SpecializedCharsBase::ungetNonAsciiNormalizedCodePoint;
     using GeneralCharsBase::updateLineInfoForEOL;
 
     template<typename CharU> friend class TokenStreamPosition;
 
   public:
     TokenStreamSpecific(JSContext* cx, const ReadOnlyCompileOptions& options,
                         const CharT* base, size_t length);
 
@@ -1628,21 +1664,20 @@ class MOZ_STACK_CLASS TokenStreamSpecifi
         if (anyChars.currentToken().type == TokenKind::TemplateHead) {
             // Of the form    |`...${|   or   |}...${|
             end = sourceUnits.codeUnitPtrAt(anyChars.currentToken().pos.end - 2);
         } else {
             // NO_SUBS_TEMPLATE is of the form   |`...`|   or   |}...`|
             end = sourceUnits.codeUnitPtrAt(anyChars.currentToken().pos.end - 1);
         }
 
-        CharBuffer charbuf(anyChars.cx);
-        if (!fillWithTemplateStringContents(charbuf, cur, end))
+        if (!fillCharBufferWithTemplateStringContents(cur, end))
             return nullptr;
 
-        return atomizeChars(anyChars.cx, charbuf.begin(), charbuf.length());
+        return atomizeChars(anyChars.cx, charBuffer.begin(), charBuffer.length());
     }
 
   private:
     // This is private because it should only be called by the tokenizer while
     // tokenizing not by, for example, BytecodeEmitter.
     bool reportStrictModeError(unsigned errorNumber, ...);
 
     void reportInvalidEscapeError(uint32_t offset, InvalidEscapeType type) {
@@ -1660,17 +1695,17 @@ class MOZ_STACK_CLASS TokenStreamSpecifi
                 errorAt(offset, JSMSG_UNICODE_OVERFLOW, "escape sequence");
                 return;
             case InvalidEscapeType::Octal:
                 errorAt(offset, JSMSG_DEPRECATED_OCTAL);
                 return;
         }
     }
 
-    MOZ_MUST_USE bool putIdentInTokenbuf(const CharT* identStart);
+    MOZ_MUST_USE bool putIdentInCharBuffer(const CharT* identStart);
 
     /**
      * Tokenize a decimal number that begins at |numStart| into the provided
      * token.
      *
      * |unit| must be one of these values:
      *
      *   1. The first decimal digit in the integral part of a decimal number
