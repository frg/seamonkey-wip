# HG changeset patch
# User Andrew Osmond <aosmond@mozilla.com>
# Date 1519842892 18000
# Node ID e96c578f2a9e45e702d472bd735d3afd0036d8ed
# Parent  80abfdca5231c275125327d9b6af7d0ea2683d87
Bug 523950 - Part 6. Add DecoderFactory::CloneAnimationDecoder to clone an existing image decoder. r=tnikkel

Used later in the patch series, this API allows one to get an identical
decoder to the one given, but decodes from the beginning.

diff --git a/image/DecoderFactory.cpp b/image/DecoderFactory.cpp
--- a/image/DecoderFactory.cpp
+++ b/image/DecoderFactory.cpp
@@ -230,16 +230,39 @@ DecoderFactory::CreateAnimationDecoder(D
   }
 
   // Return the surface provider in its IDecodingTask guise.
   RefPtr<IDecodingTask> task = provider.get();
   task.forget(aOutTask);
   return NS_OK;
 }
 
+/* static */ already_AddRefed<Decoder>
+DecoderFactory::CloneAnimationDecoder(Decoder* aDecoder)
+{
+  MOZ_ASSERT(aDecoder);
+  MOZ_ASSERT(aDecoder->HasAnimation());
+
+  RefPtr<Decoder> decoder = GetDecoder(aDecoder->GetType(), nullptr,
+                                       /* aIsRedecode = */ true);
+  MOZ_ASSERT(decoder, "Should have a decoder now");
+
+  // Initialize the decoder.
+  decoder->SetMetadataDecode(false);
+  decoder->SetIterator(aDecoder->GetSourceBuffer()->Iterator());
+  decoder->SetDecoderFlags(aDecoder->GetDecoderFlags());
+  decoder->SetSurfaceFlags(aDecoder->GetSurfaceFlags());
+
+  if (NS_FAILED(decoder->Init())) {
+    return nullptr;
+  }
+
+  return decoder.forget();
+}
+
 /* static */ already_AddRefed<IDecodingTask>
 DecoderFactory::CreateMetadataDecoder(DecoderType aType,
                                       NotNull<RasterImage*> aImage,
                                       NotNull<SourceBuffer*> aSourceBuffer)
 {
   if (aType == DecoderType::UNKNOWN) {
     return nullptr;
   }
diff --git a/image/DecoderFactory.h b/image/DecoderFactory.h
--- a/image/DecoderFactory.h
+++ b/image/DecoderFactory.h
@@ -90,32 +90,43 @@ public:
    *               notifications as decoding progresses.
    * @param aSourceBuffer The SourceBuffer which the decoder will read its data
    *                      from.
    * @param aIntrinsicSize The intrinsic size of the image, normally obtained
    *                       during the metadata decode.
    * @param aDecoderFlags Flags specifying the behavior of this decoder.
    * @param aSurfaceFlags Flags specifying the type of output this decoder
    *                      should produce.
+   * @param aCurrentFrame The current frame the decoder should auto advance to.
    * @param aOutTask Task representing the decoder.
    * @return NS_OK if the decoder has been created/initialized successfully;
    *         NS_ERROR_ALREADY_INITIALIZED if there is already an active decoder
    *           for this image;
    *         Else some other unrecoverable error occurred.
    */
   static nsresult
   CreateAnimationDecoder(DecoderType aType,
                          NotNull<RasterImage*> aImage,
                          NotNull<SourceBuffer*> aSourceBuffer,
                          const gfx::IntSize& aIntrinsicSize,
                          DecoderFlags aDecoderFlags,
                          SurfaceFlags aSurfaceFlags,
+                         size_t aCurrentFrame,
                          IDecodingTask** aOutTask);
 
   /**
+   * Creates and initializes a decoder for animated images, cloned from the
+   * given decoder.
+   *
+   * @param aDecoder Decoder to clone.
+   */
+  static already_AddRefed<Decoder>
+  CloneAnimationDecoder(Decoder* aDecoder);
+
+  /**
    * Creates and initializes a metadata decoder of type @aType. This decoder
    * will only decode the image's header, extracting metadata like the size of
    * the image. No actual image data will be decoded and no surfaces will be
    * allocated. The decoder will send notifications to @aImage.
    *
    * @param aType Which type of decoder to create - JPEG, PNG, etc.
    * @param aImage The image will own the decoder and which should receive
    *               notifications as decoding progresses.
