# HG changeset patch
# User Tom Prince <mozilla@hocat.ca>
# Date 1517433172 25200
#      Wed Jan 31 14:12:52 2018 -0700
# Node ID 818afccb3106049c8fd520f747d2cb0a1ecf6361
# Parent  62038298d7568f5963d5e56cdfaedc3d967e1389
Bug 1429603: Get rid of mock references in desktop L10N mozharness script; r=Callek

Differential Revision: https://phabricator.services.mozilla.com/D522

diff --git a/testing/mozharness/scripts/desktop_l10n.py b/testing/mozharness/scripts/desktop_l10n.py
--- a/testing/mozharness/scripts/desktop_l10n.py
+++ b/testing/mozharness/scripts/desktop_l10n.py
@@ -23,17 +23,16 @@ from mozharness.base.errors import BaseE
 from mozharness.base.script import BaseScript
 from mozharness.base.transfer import TransferMixin
 from mozharness.base.vcs.vcsbase import VCSMixin
 from mozharness.mozilla.buildbot import BuildbotMixin
 from mozharness.mozilla.purge import PurgeMixin
 from mozharness.mozilla.building.buildbase import MakeUploadOutputParser
 from mozharness.mozilla.l10n.locales import LocalesMixin
 from mozharness.mozilla.mar import MarMixin
-from mozharness.mozilla.mock import MockMixin
 from mozharness.mozilla.release import ReleaseMixin
 from mozharness.mozilla.signing import SigningMixin
 from mozharness.mozilla.updates.balrog import BalrogMixin
 from mozharness.mozilla.taskcluster_helper import Taskcluster
 from mozharness.base.python import VirtualenvMixin
 
 try:
     import simplejson as json
@@ -65,17 +64,17 @@ configuration_tokens = ('branch',
 runtime_config_tokens = ('buildid', 'version', 'locale', 'from_buildid',
                          'abs_objdir', 'revision',
                          'to_buildid', 'en_us_binary_url',
                          'en_us_installer_binary_url', 'mar_tools_url',
                          'post_upload_extra', 'who')
 
 
 # DesktopSingleLocale {{{1
-class DesktopSingleLocale(LocalesMixin, ReleaseMixin, MockMixin, BuildbotMixin,
+class DesktopSingleLocale(LocalesMixin, ReleaseMixin, BuildbotMixin,
                           VCSMixin, SigningMixin, PurgeMixin, BaseScript,
                           BalrogMixin, MarMixin, VirtualenvMixin, TransferMixin):
     """Manages desktop repacks"""
     config_options = [[
         ['--balrog-config', ],
         {"action": "extend",
          "dest": "config_files",
          "type": "string",
@@ -153,17 +152,17 @@ class DesktopSingleLocale(LocalesMixin, 
         {"action": "store",
          "dest": "en_us_installer_url",
          "type": "string",
          "help": "Specify the url of the en-us binary"}
     ], [
         ["--disable-mock"], {
          "dest": "disable_mock",
          "action": "store_true",
-         "help": "do not run under mock despite what gecko-config says"}
+         "help": "(deprecated) no-op for CLI compatability with mobile_l10n.py"}
     ]]
 
     def __init__(self, require_config_file=True):
         # fxbuild style:
         buildscript_kwargs = {
             'all_actions': [
                 "clobber",
                 "pull",
@@ -214,19 +213,16 @@ class DesktopSingleLocale(LocalesMixin, 
         self.enUS_revision = None
         self.version = None
         self.upload_urls = {}
         self.locales_property = {}
         self.pushdate = None
         # upload_files is a dictionary of files to upload, keyed by locale.
         self.upload_files = {}
 
-        if 'mock_target' in self.config:
-            self.enable_mock()
-
     def _pre_config_lock(self, rw_config):
         """replaces 'configuration_tokens' with their values, before the
            configuration gets locked. If some of the configuration_tokens
            are not present, stops the execution of the script"""
         # since values as branch, platform are mandatory, can replace them in
         # in the configuration before it is locked down
         # mandatory tokens
         for token in configuration_tokens:
@@ -697,22 +693,17 @@ class DesktopSingleLocale(LocalesMixin, 
 
     def _mach_configure(self):
         """calls mach configure"""
         env = self.query_bootstrap_env()
         target = ["configure"]
         return self._mach(target=target, env=env)
 
     def _get_mach_executable(self):
-        python = sys.executable
-        # A mock environment is a special case, the system python isn't
-        # available there
-        if 'mock_target' in self.config:
-            python = 'python2.7'
-        return [python, 'mach']
+        return [sys.executable, 'mach']
 
     def _get_make_executable(self):
         config = self.config
         dirs = self.query_abs_dirs()
         if config.get('enable_mozmake'):  # e.g. windows
             make = r"/".join([dirs['abs_mozilla_dir'], 'mozmake.exe'])
             # mysterious subprocess errors, let's try to fix this path...
             make = make.replace('\\', '/')
@@ -971,20 +962,16 @@ class DesktopSingleLocale(LocalesMixin, 
         dirs = self.query_abs_dirs()
         toolchains = os.environ.get('MOZ_TOOLCHAINS')
         manifest_src = os.environ.get('TOOLTOOL_MANIFEST')
         if not manifest_src:
             manifest_src = config.get('tooltool_manifest_src')
         if not manifest_src and not toolchains:
             return
         python = sys.executable
-        # A mock environment is a special case, the system python isn't
-        # available there
-        if 'mock_target' in self.config:
-            python = 'python2.7'
 
         cmd = [
             python, '-u',
             os.path.join(dirs['abs_mozilla_dir'], 'mach'),
             'artifact',
             'toolchain',
             '-v',
             '--retry', '4',
@@ -1020,19 +1007,17 @@ class DesktopSingleLocale(LocalesMixin, 
             self.warning('Skipping S3 file upload: No taskcluster credentials.')
             return
 
         # We need to activate the virtualenv so that we can import taskcluster
         # (and its dependent modules, like requests and hawk).  Normally we
         # could create the virtualenv as an action, but due to some odd
         # dependencies with query_build_env() being called from build(), which
         # is necessary before the virtualenv can be created.
-        self.disable_mock()
         self.create_virtualenv()
-        self.enable_mock()
         self.activate_virtualenv()
 
         branch = self.config['branch']
         revision = self._query_revision()
         repo = self.query_l10n_repo()
         if not repo:
             self.fatal("Unable to determine repository for querying the push info.")
         pushinfo = self.vcs_query_pushinfo(repo, revision, vcs='hg')
