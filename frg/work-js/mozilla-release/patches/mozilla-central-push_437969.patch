# HG changeset patch
# User Robin Templeton <robin@igalia.com>
# Date 1537841889 0
#      Tue Sep 25 02:18:09 2018 +0000
# Node ID 3dace40ce193168f0b3c2102076747f0b99cdaab
# Parent  8db8a228536dedac30934430b8c62fa889e5b7eb
bug 1492669 - Implement BigInt support for relational comparison operators. r=jandem

Differential Revision: https://phabricator.services.mozilla.com/D6558

diff --git a/js/src/vm/BigIntType.cpp b/js/src/vm/BigIntType.cpp
--- a/js/src/vm/BigIntType.cpp
+++ b/js/src/vm/BigIntType.cpp
@@ -23,16 +23,18 @@
 #include "js/Initialization.h"
 #include "js/Utility.h"
 #include "vm/JSContext.h"
 #include "vm/SelfHosting.h"
 
 using namespace js;
 
 using mozilla::Maybe;
+using mozilla::Some;
+using mozilla::Nothing;
 using mozilla::Range;
 using mozilla::RangedPtr;
 
 // The following functions are wrappers for use with
 // mp_set_memory_functions. GMP passes extra arguments to the realloc
 // and free functions not needed by the JS allocation interface.
 // js_malloc has the signature expected for GMP's malloc function, so no
 // wrapper is required.
@@ -725,16 +727,92 @@ BigInt::looselyEqual(JSContext* cx, Hand
     if (rhs.isNumber()) {
         return equal(lhs, rhs.toNumber());
     }
 
     // Step 13.
     return false;
 }
 
+// BigInt proposal section 1.1.12. BigInt::lessThan ( x, y )
+bool
+BigInt::lessThan(BigInt* x, BigInt* y)
+{
+    return mpz_cmp(x->num_, y->num_) < 0;
+}
+
+Maybe<bool>
+BigInt::lessThan(BigInt* lhs, double rhs)
+{
+    if (mozilla::IsNaN(rhs)) {
+        return Maybe<bool>(Nothing());
+    }
+    return Some(mpz_cmp_d(lhs->num_, rhs) < 0);
+}
+
+Maybe<bool>
+BigInt::lessThan(double lhs, BigInt* rhs)
+{
+    if (mozilla::IsNaN(lhs)) {
+        return Maybe<bool>(Nothing());
+    }
+    return Some(-mpz_cmp_d(rhs->num_, lhs) < 0);
+}
+
+JS::Result<Maybe<bool>>
+BigInt::lessThan(JSContext* cx, HandleBigInt lhs, HandleString rhs)
+{
+    RootedBigInt rhsBigInt(cx);
+    MOZ_TRY_VAR(rhsBigInt, StringToBigInt(cx, rhs, 0));
+    if (!rhsBigInt) {
+        return Maybe<bool>(Nothing());
+    }
+    return Some(lessThan(lhs, rhsBigInt));
+}
+
+JS::Result<Maybe<bool>>
+BigInt::lessThan(JSContext* cx, HandleString lhs, HandleBigInt rhs)
+{
+    RootedBigInt lhsBigInt(cx);
+    MOZ_TRY_VAR(lhsBigInt, StringToBigInt(cx, lhs, 0));
+    if (!lhsBigInt) {
+        return Maybe<bool>(Nothing());
+    }
+    return Some(lessThan(lhsBigInt, rhs));
+}
+
+JS::Result<Maybe<bool>>
+BigInt::lessThan(JSContext* cx, HandleValue lhs, HandleValue rhs)
+{
+    if (lhs.isBigInt()) {
+        if (rhs.isString()) {
+            RootedBigInt lhsBigInt(cx, lhs.toBigInt());
+            RootedString rhsString(cx, rhs.toString());
+            return lessThan(cx, lhsBigInt, rhsString);
+        }
+
+        if (rhs.isNumber()) {
+            return lessThan(lhs.toBigInt(), rhs.toNumber());
+        }
+
+        MOZ_ASSERT(rhs.isBigInt());
+        return Some(lessThan(lhs.toBigInt(), rhs.toBigInt()));
+    }
+
+    MOZ_ASSERT(rhs.isBigInt());
+    if (lhs.isString()) {
+        RootedString lhsString(cx, lhs.toString());
+        RootedBigInt rhsBigInt(cx, rhs.toBigInt());
+        return lessThan(cx, lhsString, rhsBigInt);
+    }
+
+    MOZ_ASSERT(lhs.isNumber());
+    return lessThan(lhs.toNumber(), rhs.toBigInt());
+}
+
 JSLinearString*
 BigInt::toString(JSContext* cx, BigInt* x, uint8_t radix)
 {
     MOZ_ASSERT(2 <= radix && radix <= 36);
     // We need two extra chars for '\0' and potentially '-'.
     size_t strSize = mpz_sizeinbase(x->num_, 10) + 2;
     UniqueChars str(js_pod_malloc<char>(strSize));
     if (!str) {
diff --git a/js/src/vm/BigIntType.h b/js/src/vm/BigIntType.h
--- a/js/src/vm/BigIntType.h
+++ b/js/src/vm/BigIntType.h
@@ -111,16 +111,26 @@ class BigInt final : public js::gc::Tenu
 
     static double numberValue(BigInt* x);
     static JSLinearString* toString(JSContext* cx, BigInt* x, uint8_t radix);
 
     static bool equal(BigInt* lhs, BigInt* rhs);
     static bool equal(BigInt* lhs, double rhs);
     static JS::Result<bool> looselyEqual(JSContext* cx, HandleBigInt lhs, HandleValue rhs);
 
+    static bool lessThan(BigInt* x, BigInt* y);
+
+    // These methods return Nothing when the non-BigInt operand is NaN
+    // or a string that can't be interpreted as a BigInt.
+    static mozilla::Maybe<bool> lessThan(BigInt* lhs, double rhs);
+    static mozilla::Maybe<bool> lessThan(double lhs, BigInt* rhs);
+    static JS::Result<mozilla::Maybe<bool>> lessThan(JSContext* cx, HandleBigInt lhs, HandleString rhs);
+    static JS::Result<mozilla::Maybe<bool>> lessThan(JSContext* cx, HandleString lhs, HandleBigInt rhs);
+    static JS::Result<mozilla::Maybe<bool>> lessThan(JSContext* cx, HandleValue lhs, HandleValue rhs);
+
     // Return the length in bytes of the representation used by
     // writeBytes.
     static size_t byteLength(BigInt* x);
 
     // Write a little-endian representation of a BigInt's absolute value
     // to a byte array.
     static void writeBytes(BigInt* x, mozilla::RangedPtr<uint8_t> buffer);
 };
diff --git a/js/src/vm/Interpreter-inl.h b/js/src/vm/Interpreter-inl.h
--- a/js/src/vm/Interpreter-inl.h
+++ b/js/src/vm/Interpreter-inl.h
@@ -4,16 +4,18 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef vm_Interpreter_inl_h
 #define vm_Interpreter_inl_h
 
 #include "vm/Interpreter.h"
 
+#include "mozilla/Maybe.h"
+
 #include "jsnum.h"
 
 #include "builtin/String.h"
 #include "jit/Ion.h"
 #include "vm/ArgumentsObject.h"
 #include "vm/Realm.h"
 
 #include "vm/EnvironmentObject-inl.h"
@@ -750,61 +752,161 @@ ProcessCallSiteObjOperation(JSContext* c
         }
         if (!FreezeObject(cx, cso)) {
             return false;
         }
     }
     return true;
 }
 
-#define RELATIONAL_OP(OP)                                                     \
-    JS_BEGIN_MACRO                                                            \
-        /* Optimize for two int-tagged operands (typical loop control). */    \
-        if (lhs.isInt32() && rhs.isInt32()) {                                 \
-            *res = lhs.toInt32() OP rhs.toInt32();                            \
-        } else {                                                              \
-            if (!ToPrimitive(cx, JSTYPE_NUMBER, lhs))                         \
-                return false;                                                 \
-            if (!ToPrimitive(cx, JSTYPE_NUMBER, rhs))                         \
-                return false;                                                 \
-            if (lhs.isString() && rhs.isString()) {                           \
-                JSString* l = lhs.toString();                                 \
-                JSString* r = rhs.toString();                                 \
-                int32_t result;                                               \
-                if (!CompareStrings(cx, l, r, &result))                       \
-                    return false;                                             \
-                *res = result OP 0;                                           \
-            } else {                                                          \
-                double l, r;                                                  \
-                if (!ToNumber(cx, lhs, &l) || !ToNumber(cx, rhs, &r))         \
-                    return false;                                             \
-                *res = (l OP r);                                              \
-            }                                                                 \
-        }                                                                     \
-        return true;                                                          \
-    JS_END_MACRO
+// BigInt proposal 3.2.4 Abstract Relational Comparison
+// Returns Nothing when at least one operand is a NaN, or when
+// ToNumeric or StringToBigInt can't interpret a string as a numeric
+// value. (These cases correspond to a NaN result in the spec.)
+// Otherwise, return a boolean to indicate whether lhs is less than
+// rhs. The operands must be primitives; the caller is responsible for
+// evaluating them in the correct order.
+static MOZ_ALWAYS_INLINE JS::Result<mozilla::Maybe<bool>>
+LessThanImpl(JSContext* cx, MutableHandleValue lhs, MutableHandleValue rhs)
+{
+    // Steps 1 and 2 are performed by the caller.
+
+    // Step 3.
+    if (lhs.isString() && rhs.isString()) {
+        JSString* l = lhs.toString();
+        JSString* r = rhs.toString();
+        int32_t result;
+        if (!CompareStrings(cx, l, r, &result)) {
+            return cx->alreadyReportedError();
+        }
+        return mozilla::Some(result < 0);
+    }
+
+#ifdef ENABLE_BIGINT
+    // Step 4a.
+    if (lhs.isBigInt() && rhs.isString()) {
+        return BigInt::lessThan(cx, lhs, rhs);
+    }
 
-static MOZ_ALWAYS_INLINE bool
-LessThanOperation(JSContext* cx, MutableHandleValue lhs, MutableHandleValue rhs, bool* res) {
-    RELATIONAL_OP(<);
+    // Step 4b.
+    if (lhs.isString() && rhs.isBigInt()) {
+        return BigInt::lessThan(cx, lhs, rhs);
+    }
+#endif
+
+    // Steps 4c and 4d.
+    if (!ToNumeric(cx, lhs) || !ToNumeric(cx, rhs)) {
+        return cx->alreadyReportedError();
+    }
+
+#ifdef ENABLE_BIGINT
+    // Steps 4e-j.
+    if (lhs.isBigInt() || rhs.isBigInt()) {
+        return BigInt::lessThan(cx, lhs, rhs);
+    }
+#endif
+
+    // Step 4e for Number operands.
+    MOZ_ASSERT(lhs.isNumber() && rhs.isNumber());
+    double lhsNum = lhs.toNumber();
+    double rhsNum = rhs.toNumber();
+
+    if (mozilla::IsNaN(lhsNum) || mozilla::IsNaN(rhsNum)) {
+        return mozilla::Maybe<bool>(mozilla::Nothing());
+    }
+
+    return mozilla::Some(lhsNum < rhsNum);
 }
 
 static MOZ_ALWAYS_INLINE bool
-LessThanOrEqualOperation(JSContext* cx, MutableHandleValue lhs, MutableHandleValue rhs, bool* res) {
-    RELATIONAL_OP(<=);
+LessThanOperation(JSContext* cx, MutableHandleValue lhs, MutableHandleValue rhs, bool* res)
+{
+    if (lhs.isInt32() && rhs.isInt32()) {
+        *res = lhs.toInt32() < rhs.toInt32();
+        return true;
+    }
+
+    if (!ToPrimitive(cx, JSTYPE_NUMBER, lhs)) {
+        return false;
+    }
+
+    if (!ToPrimitive(cx, JSTYPE_NUMBER, rhs)) {
+        return false;
+    }
+
+    mozilla::Maybe<bool> tmpResult;
+    JS_TRY_VAR_OR_RETURN_FALSE(cx, tmpResult, LessThanImpl(cx, lhs, rhs));
+    *res = tmpResult.valueOr(false);
+    return true;
 }
 
 static MOZ_ALWAYS_INLINE bool
-GreaterThanOperation(JSContext* cx, MutableHandleValue lhs, MutableHandleValue rhs, bool* res) {
-    RELATIONAL_OP(>);
+LessThanOrEqualOperation(JSContext* cx, MutableHandleValue lhs, MutableHandleValue rhs, bool* res)
+{
+    if (lhs.isInt32() && rhs.isInt32()) {
+        *res = lhs.toInt32() <= rhs.toInt32();
+        return true;
+    }
+
+    if (!ToPrimitive(cx, JSTYPE_NUMBER, lhs)) {
+        return false;
+    }
+
+    if (!ToPrimitive(cx, JSTYPE_NUMBER, rhs)) {
+        return false;
+    }
+
+    mozilla::Maybe<bool> tmpResult;
+    JS_TRY_VAR_OR_RETURN_FALSE(cx, tmpResult, LessThanImpl(cx, rhs, lhs));
+    *res = !tmpResult.valueOr(true);
+    return true;
 }
 
 static MOZ_ALWAYS_INLINE bool
-GreaterThanOrEqualOperation(JSContext* cx, MutableHandleValue lhs, MutableHandleValue rhs, bool* res) {
-    RELATIONAL_OP(>=);
+GreaterThanOperation(JSContext* cx, MutableHandleValue lhs, MutableHandleValue rhs, bool* res)
+{
+    if (lhs.isInt32() && rhs.isInt32()) {
+        *res = lhs.toInt32() > rhs.toInt32();
+        return true;
+    }
+
+    if (!ToPrimitive(cx, JSTYPE_NUMBER, lhs)) {
+        return false;
+    }
+
+    if (!ToPrimitive(cx, JSTYPE_NUMBER, rhs)) {
+        return false;
+    }
+
+    mozilla::Maybe<bool> tmpResult;
+    JS_TRY_VAR_OR_RETURN_FALSE(cx, tmpResult, LessThanImpl(cx, rhs, lhs));
+    *res = tmpResult.valueOr(false);
+    return true;
+}
+
+static MOZ_ALWAYS_INLINE bool
+GreaterThanOrEqualOperation(JSContext* cx, MutableHandleValue lhs, MutableHandleValue rhs, bool* res)
+{
+    if (lhs.isInt32() && rhs.isInt32()) {
+        *res = lhs.toInt32() >= rhs.toInt32();
+        return true;
+    }
+
+    if (!ToPrimitive(cx, JSTYPE_NUMBER, lhs)) {
+        return false;
+    }
+
+    if (!ToPrimitive(cx, JSTYPE_NUMBER, rhs)) {
+        return false;
+    }
+
+    mozilla::Maybe<bool> tmpResult;
+    JS_TRY_VAR_OR_RETURN_FALSE(cx, tmpResult, LessThanImpl(cx, lhs, rhs));
+    *res = !tmpResult.valueOr(true);
+    return true;
 }
 
 static MOZ_ALWAYS_INLINE bool
 BitNot(JSContext* cx, MutableHandleValue in, MutableHandleValue out)
 {
     if (!ToInt32OrBigInt(cx, in)) {
         return false;
     }
