# HG changeset patch
# User Jeff Walden <jwalden@mit.edu>
# Date 1518541194 28800
# Node ID 6902e5dc198cf250f7f788d8f4617bd489cdf277
# Parent  1f43bd6b5b057e8b28ceccc32e1f8baf57d7c633
Bug 1437731 - Convert a |uint16_t * uint16_t| to |1U * uint16_t * uint16_t| to avoid integer promotion of the original two operands resulting in signed integer overflow given the right runtime inputs.  r=froydnj

diff --git a/js/src/builtin/SIMD.cpp b/js/src/builtin/SIMD.cpp
--- a/js/src/builtin/SIMD.cpp
+++ b/js/src/builtin/SIMD.cpp
@@ -750,17 +750,25 @@ struct Identity {
 };
 template<typename T>
 struct Abs {
     static T apply(T x) { return mozilla::Abs(x); }
 };
 template<typename T>
 struct Neg {
     using MaybeUnsignedT = typename detail::MaybeMakeUnsigned<T>::Type;
-    static T apply(T x) { return MaybeUnsignedT(-1) * MaybeUnsignedT(x); }
+    static T apply(T x) {
+        // Prepend |1U| to force integral promotion through *unsigned* types.
+        // Otherwise when |T = uint16_t| and |int| is 32-bit, we could have
+        // |uint16_t(-1) * uint16_t(65535)| which would really be
+        // |int(65535) * int(65535)|, but as |4294836225 > 2147483647| would
+        // perform signed integer overflow.
+        // https://stackoverflow.com/questions/24795651/whats-the-best-c-way-to-multiply-unsigned-integers-modularly-safely
+        return static_cast<MaybeUnsignedT>(1U * MaybeUnsignedT(-1) * MaybeUnsignedT(x));
+    }
 };
 template<typename T>
 struct Not {
     static T apply(T x) { return ~x; }
 };
 template<typename T>
 struct LogicalNot {
     static T apply(T x) { return !x; }
