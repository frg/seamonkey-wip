# HG changeset patch
# User Boris Zbarsky <bzbarsky@mit.edu>
# Date 1553169153 0
#      Thu Mar 21 11:52:33 2019 +0000
# Node ID 664d49ea52757224083e93a9742e676bb8689ef4
# Parent  1133a148d29c657567b6985e08af96adab3328f7
Bug 1536736.  Allow constexpr things in the MOZ_CAN_RUN_SCRIPT analysis.  r=andi

Since these are compile-time constants, they can't exactly go away on us due to
running script, right?

Differential Revision: https://phabricator.services.mozilla.com/D24195

diff --git a/build/clang-plugin/CanRunScriptChecker.cpp b/build/clang-plugin/CanRunScriptChecker.cpp
--- a/build/clang-plugin/CanRunScriptChecker.cpp
+++ b/build/clang-plugin/CanRunScriptChecker.cpp
@@ -105,16 +105,20 @@ void CanRunScriptChecker::registerMatche
               anyOf(hasOverloadedOperatorName("*"),
                     hasOverloadedOperatorName("->")),
               hasAnyArgument(LocalKnownLive),
               argumentCountIs(1)
             )
           ),
           // and which is not a parameter of the parent function,
           unless(declRefExpr(to(parmVarDecl()))),
+          // and which is not a constexpr variable, since that must be
+          // computable at compile-time and therefore isn't going to be going
+          // away.
+          unless(declRefExpr(to(varDecl(isConstexpr())))),
           // and which is not a default arg with value nullptr, since those are
           // always safe.
           unless(cxxDefaultArgExpr(isNullDefaultArg())),
           // and which is not a literal nullptr
           unless(cxxNullPtrLiteralExpr()),
           // and which is not a dereference of a parameter of the parent
           // function (including "this"),
           unless(
@@ -125,17 +129,22 @@ void CanRunScriptChecker::registerMatche
                   // If we're doing *someArg, the argument of the dereference is
                   // an ImplicitCastExpr LValueToRValue which has the
                   // DeclRefExpr as an argument.  We could try to match that
                   // explicitly with a custom matcher (none of the built-in
                   // matchers seem to match on the thing being cast for an
                   // implicitCastExpr), but it's simpler to just use
                   // ignoreTrivials to strip off the cast.
                   ignoreTrivials(declRefExpr(to(parmVarDecl()))),
-                  cxxThisExpr()
+                  cxxThisExpr(),
+                  // We also allow dereferencing a constexpr variable here,
+                  // since that will just end up with a reference to the
+                  // compile-time-constant thing.  Again, use ignoreTrivials()
+                  // to stip off the LValueToRValue cast.
+                  ignoreTrivials(declRefExpr(to(varDecl(isConstexpr()))))
                 )
               )
             )
           ),
           // and which is not a MOZ_KnownLive wrapped value.
           unless(
             anyOf(
               MozKnownLiveCall,
diff --git a/build/clang-plugin/tests/TestCanRunScript.cpp b/build/clang-plugin/tests/TestCanRunScript.cpp
--- a/build/clang-plugin/tests/TestCanRunScript.cpp
+++ b/build/clang-plugin/tests/TestCanRunScript.cpp
@@ -432,8 +432,33 @@ struct DisallowRefPtrTArrayElement {
   TArray<RefPtr<RefCountedBase>> mArray;
   MOZ_CAN_RUN_SCRIPT void foo() {
     mArray[0]->method_test(); // expected-error {{arguments must all be strong refs or parent parameters when calling a function marked as MOZ_CAN_RUN_SCRIPT (including the implicit object argument)}}
   }
   MOZ_CAN_RUN_SCRIPT void bar() {
     test2(mArray[0]); // expected-error {{arguments must all be strong refs or parent parameters when calling a function marked as MOZ_CAN_RUN_SCRIPT (including the implicit object argument)}}
   }
 };
+
+struct AllowConstexprMembers {
+  static constexpr RefCountedBase* mRefCounted = nullptr;
+  MOZ_CAN_RUN_SCRIPT void foo() {
+    mRefCounted->method_test();
+  }
+  MOZ_CAN_RUN_SCRIPT void bar() {
+    test2(mRefCounted);
+  }
+  MOZ_CAN_RUN_SCRIPT void baz() {
+    test_ref(*mRefCounted);
+  }
+};
+
+MOZ_CAN_RUN_SCRIPT void test_constexpr_1() {
+  AllowConstexprMembers::mRefCounted->method_test();
+}
+
+MOZ_CAN_RUN_SCRIPT void test_constexpr_2() {
+  test2(AllowConstexprMembers::mRefCounted);
+}
+
+MOZ_CAN_RUN_SCRIPT void test_constexpr_3() {
+  test_ref(*AllowConstexprMembers::mRefCounted);
+}
