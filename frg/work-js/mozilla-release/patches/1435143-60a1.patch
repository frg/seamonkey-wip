# HG changeset patch
# User Martin Roinson <mrobinson@igalia.com>
# Date 1517994116 -3600
# Node ID d3dbee5cf6820dec8eaeba39d377c7dd91f40cec
# Parent  63f0b20ac6ac69e7038b36dbd838bffbb174eb12
Bug 1435143 - Properly unwrap Maybe<WrScrollId> for the root scroll frame. r=kats

When comparing a Maybe<WrScrollId> to another WrScrollId we need to properly
handle the case where Nothing() signifies the root scroll frame. This is
equivalent to calling scrollId.valueOr(FrameMetrics::NULL_SCROLL_ID), as was
done before WrScrolLId replaced ViewId in the WebRender ScrollingLayersHelper.
We also have DisplayListBuilder::TopmostScrollId always return a value instead
of a Maybe, since an empty clip stack means that the current scroll id is that
of the root scroll frame.

Previously Nothing() was not equivalent to WrScrollId { 0 }, which caused the
ScrollingLayersHelper to fill the mClipAndScroll value and push another
set of clip and scroll nodes onto the WebRender display list builder.

MozReview-Commit-ID: CeatZlRXtuI

diff --git a/gfx/layers/wr/ScrollingLayersHelper.cpp b/gfx/layers/wr/ScrollingLayersHelper.cpp
--- a/gfx/layers/wr/ScrollingLayersHelper.cpp
+++ b/gfx/layers/wr/ScrollingLayersHelper.cpp
@@ -119,48 +119,49 @@ ScrollingLayersHelper::BeginItem(nsDispl
   // generated while processing aItem. However those display items only care
   // about the topmost clip on the stack. If that were all we cared about we
   // would only need to push one thing here and we would be done. However, we
   // also care about the ScrollingLayersHelper instance that might be created
   // for nested display items, in the case where aItem is a wrapper item. The
   // nested ScrollingLayersHelper may rely on things like TopmostScrollId and
   // TopmostClipId, so now we need to push at most two things onto the stack.
 
-  Maybe<wr::WrScrollId> leafmostId = ids.first;
+  wr::WrScrollId rootId = wr::WrScrollId { 0 };
+  wr::WrScrollId leafmostId = ids.first.valueOr(rootId);
+
   FrameMetrics::ViewID viewId = aItem->GetActiveScrolledRoot()
       ? aItem->GetActiveScrolledRoot()->GetViewId()
       : FrameMetrics::NULL_SCROLL_ID;
-  Maybe<wr::WrScrollId> scrollId =
-      mBuilder->GetScrollIdForDefinedScrollLayer(viewId);
-  MOZ_ASSERT(scrollId.isSome());
+  wr::WrScrollId scrollId =
+      mBuilder->GetScrollIdForDefinedScrollLayer(viewId).valueOr(rootId);
 
   // If the leafmost ASR is not the same as the item's ASR then we are dealing
   // with a case where the item's clip chain is scrolled by something other than
   // the item's ASR. So for those cases we need to use the ClipAndScroll API.
   bool needClipAndScroll = (leafmostId != scrollId);
 
   // The other scenario where we need to push a ClipAndScroll is when we are
   // in a nested display item where the enclosing item pushed a ClipAndScroll,
   // and our clip chain extends from that item's clip chain. To check this we
   // want to make sure that (a) we are inside a ClipAndScroll, and (b) nothing
   // else was pushed onto mBuilder's stack since that ClipAndScroll.
   if (!needClipAndScroll &&
       mBuilder->TopmostScrollId() == scrollId &&
       !mBuilder->TopmostIsClip()) {
     if (auto cs = EnclosingClipAndScroll()) {
-      MOZ_ASSERT(cs->first == *scrollId);
+      MOZ_ASSERT(cs->first == scrollId);
       needClipAndScroll = true;
     }
   }
 
   // If we don't need a ClipAndScroll, ensure the item's ASR is at the top of
   // the scroll stack
   if (!needClipAndScroll && mBuilder->TopmostScrollId() != scrollId) {
     MOZ_ASSERT(leafmostId == scrollId); // because !needClipAndScroll
-    clips.mScrollId = scrollId;
+    clips.mScrollId = Some(scrollId);
   }
   // And ensure the leafmost clip, if scrolled by that ASR, is at the top of the
   // stack.
   if (ids.second && clip->mASR == leafmostASR) {
     clips.mClipId = ids.second;
   }
   // If we need the ClipAndScroll, we want to replace the topmost scroll layer
   // with the item's ASR but preseve the topmost clip (which is scrolled by
@@ -169,17 +170,17 @@ ScrollingLayersHelper::BeginItem(nsDispl
     // If mClipId is set that means we want to push it such that it's going
     // to be the TopmostClipId(), but we haven't actually pushed it yet.
     // But we still want to take that instead of the actual current TopmostClipId().
     Maybe<wr::WrClipId> clipId = clips.mClipId;
     if (!clipId) {
       clipId = mBuilder->TopmostClipId();
     }
 
-    clips.mClipAndScroll = Some(std::make_pair(*scrollId, clipId));
+    clips.mClipAndScroll = Some(std::make_pair(scrollId, clipId));
   }
 
   clips.Apply(mBuilder);
   mItemClipStack.push_back(clips);
 
   SLH_LOG("done setup for %p\n", aItem);
 }
 
@@ -294,17 +295,19 @@ ScrollingLayersHelper::RecurseAndDefineC
       // But if the ASRs are different, this is the outermost clip that's
       // still inside aAsr, and we need to make it a child of aAsr rather
       // than aChain->mParent.
       ancestorIds.second = Nothing();
     }
   } else {
     MOZ_ASSERT(!ancestorIds.second);
     FrameMetrics::ViewID viewId = aChain->mASR ? aChain->mASR->GetViewId() : FrameMetrics::NULL_SCROLL_ID;
-    auto scrollId = mBuilder->GetScrollIdForDefinedScrollLayer(viewId);
+
+    wr::WrScrollId rootId = wr::WrScrollId { 0 };
+    auto scrollId = mBuilder->GetScrollIdForDefinedScrollLayer(viewId).valueOr(rootId);
     if (mBuilder->TopmostScrollId() == scrollId) {
       if (mBuilder->TopmostIsClip()) {
         // If aChain->mASR is already the topmost scroll layer on the stack, but
         // but there was another clip pushed *on top* of that ASR, then that clip
         // shares the ASR, and we need to make our clip a child of that clip, which
         // in turn will already be a descendant of the correct ASR.
         // This covers the cases where e.g. the Gecko display list has nested items,
         // and the clip chain on the nested item implicitly extends from the clip
@@ -323,17 +326,17 @@ ScrollingLayersHelper::RecurseAndDefineC
         // item had a clip scrolled by a different ASR than the item itself),
         // then we have need to propagate that behaviour as well. For example if
         // the enclosing display item pushed a ClipAndScroll with (scrollid=S,
         // clipid=C), then then clip we're defining here (call it D) needs to be
         // defined as a child of C, and we'll need to push the ClipAndScroll
         // (S, D) for this item. This hunk of code ensures that we define D
         // as a child of C, and when we set the needClipAndScroll flag elsewhere
         // in this file we make sure to set it for this scenario.
-        MOZ_ASSERT(Some(cs->first) == scrollId);
+        MOZ_ASSERT(cs->first == scrollId);
         ancestorIds.first = Nothing();
         ancestorIds.second = cs->second;
       }
     }
   }
   // At most one of the ancestor pair should be defined here, and the one that
   // is defined will be the parent clip for the new clip that we're defining.
   MOZ_ASSERT(!(ancestorIds.first && ancestorIds.second));
diff --git a/gfx/tests/reftest/1435143-ref.html b/gfx/tests/reftest/1435143-ref.html
new file mode 100644
--- /dev/null
+++ b/gfx/tests/reftest/1435143-ref.html
@@ -0,0 +1,23 @@
+<!DOCTYPE html>
+<html>
+  <head>
+    <style>
+    #green {
+        position: absolute;
+        background: green;
+        border-radius: 1px;
+        transform: translateX(100px);
+    }
+    #text {
+        visibility: hidden;
+    }
+    </style>
+  </head>
+
+  <body>
+    <div id="header">
+      <div id="green"><span id="text">Text.</span></div>
+    </div>
+  </body>
+</html>
+
diff --git a/gfx/tests/reftest/1435143.html b/gfx/tests/reftest/1435143.html
new file mode 100644
--- /dev/null
+++ b/gfx/tests/reftest/1435143.html
@@ -0,0 +1,26 @@
+<!DOCTYPE html>
+<html>
+  <head>
+    <style>
+    #header {
+        position: fixed;
+    }
+    #green {
+        position: absolute;
+        background: green;
+        border-radius: 1px;
+        transform: translateX(100px);
+    }
+    #text {
+        visibility: hidden;
+    }
+    </style>
+  </head>
+
+  <body>
+    <div id="header">
+      <div id="green"><span id="text">Text.</span></div>
+    </div>
+  </body>
+</html>
+
diff --git a/gfx/tests/reftest/reftest.list b/gfx/tests/reftest/reftest.list
--- a/gfx/tests/reftest/reftest.list
+++ b/gfx/tests/reftest/reftest.list
@@ -5,8 +5,9 @@ fuzzy-if(Android,8,1000) == 709477-1.htm
 skip-if(!asyncPan) == 1086723.html 1086723-ref.html
 == 853889-1.html 853889-1-ref.html
 skip-if(Android) fuzzy-if(skiaContent,1,587) == 1143303-1.svg pass.svg
 fuzzy(100,30) == 1149923.html 1149923-ref.html # use fuzzy due to few distorted pixels caused by border-radius
 == 1131264-1.svg pass.svg
 == 1419528.html 1419528-ref.html
 == 1424673.html 1424673-ref.html
 == 1429411.html 1429411-ref.html
+== 1435143.html 1435143-ref.html
diff --git a/gfx/webrender_bindings/WebRenderAPI.cpp b/gfx/webrender_bindings/WebRenderAPI.cpp
--- a/gfx/webrender_bindings/WebRenderAPI.cpp
+++ b/gfx/webrender_bindings/WebRenderAPI.cpp
@@ -1309,25 +1309,25 @@ DisplayListBuilder::TopmostClipId()
   for (auto it = mClipStack.crbegin(); it != mClipStack.crend(); it++) {
     if (it->is<wr::WrClipId>()) {
       return Some(it->as<wr::WrClipId>());
     }
   }
   return Nothing();
 }
 
-Maybe<wr::WrScrollId>
+wr::WrScrollId
 DisplayListBuilder::TopmostScrollId()
 {
   for (auto it = mClipStack.crbegin(); it != mClipStack.crend(); it++) {
     if (it->is<wr::WrScrollId>()) {
-      return Some(it->as<wr::WrScrollId>());
+      return it->as<wr::WrScrollId>();
     }
   }
-  return Nothing();
+  return wr::WrScrollId { 0 };
 }
 
 bool
 DisplayListBuilder::TopmostIsClip()
 {
   if (mClipStack.empty()) {
     return false;
   }
diff --git a/gfx/webrender_bindings/WebRenderAPI.h b/gfx/webrender_bindings/WebRenderAPI.h
--- a/gfx/webrender_bindings/WebRenderAPI.h
+++ b/gfx/webrender_bindings/WebRenderAPI.h
@@ -430,17 +430,17 @@ public:
                      const wr::BorderRadius& aBorderRadius,
                      const wr::BoxShadowClipMode& aClipMode);
 
   // Returns the clip id that was most recently pushed with PushClip and that
   // has not yet been popped with PopClip. Return Nothing() if the clip stack
   // is empty.
   Maybe<wr::WrClipId> TopmostClipId();
   // Same as TopmostClipId() but for scroll layers.
-  Maybe<wr::WrScrollId> TopmostScrollId();
+  wr::WrScrollId TopmostScrollId();
   // If the topmost item on the stack is a clip or a scroll layer
   bool TopmostIsClip();
 
   // Set the hit-test info to be used for all display items until the next call
   // to SetHitTestInfo or ClearHitTestInfo.
   void SetHitTestInfo(const layers::FrameMetrics::ViewID& aScrollId,
                       gfx::CompositorHitTestInfo aHitInfo);
   // Clears the hit-test info so that subsequent display items will not have it.
diff --git a/gfx/webrender_bindings/WebRenderTypes.h b/gfx/webrender_bindings/WebRenderTypes.h
--- a/gfx/webrender_bindings/WebRenderTypes.h
+++ b/gfx/webrender_bindings/WebRenderTypes.h
@@ -766,16 +766,20 @@ struct WrClipId {
 // Corresponds to a clip id for for a scroll frame in webrender. Similar
 // to WrClipId but a separate struct so we don't get them mixed up in C++.
 struct WrScrollId {
   uint64_t id;
 
   bool operator==(const WrScrollId& other) const {
     return id == other.id;
   }
+
+  bool operator!=(const WrScrollId& other) const {
+    return id != other.id;
+  }
 };
 
 // Corresponds to a clip id for a position:sticky clip in webrender. Similar
 // to WrClipId but a separate struct so we don't get them mixed up in C++.
 struct WrStickyId {
   uint64_t id;
 
   bool operator==(const WrStickyId& other) const {
