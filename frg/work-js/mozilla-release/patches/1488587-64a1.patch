# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1536025771 -32400
# Node ID c1f1cedf0a329fc8ae5170555dc5a5c2118a1987
# Parent  91f29ffcd4d57eac0289fe181af3cc1e1351d970
Bug 1488587 - Don't default to lld when building with clang LTO. r=dmajor

As described in c2b5cf7bde83, it is still preferable to build with BFD
ld when doing clang LTO, and one of the reasons we defaulted to lld in
the first place is that we didn't have the LLVM gold plugin on
automation, which, as of bug 1488307, we now have.

Differential Revision: https://phabricator.services.mozilla.com/D4987

diff --git a/build/moz.configure/toolchain.configure b/build/moz.configure/toolchain.configure
--- a/build/moz.configure/toolchain.configure
+++ b/build/moz.configure/toolchain.configure
@@ -1882,30 +1882,25 @@ option('--enable-gold',
 imply_option('--enable-linker', 'gold', when='--enable-gold')
 
 js_option('--enable-linker', nargs=1,
           help='Select the linker {bfd, gold, ld64, lld, lld-*}',
           when=is_linker_option_enabled)
 
 
 @depends('--enable-linker', c_compiler, developer_options, '--enable-gold',
-         extra_toolchain_flags, target, lto.enabled,
-         when=is_linker_option_enabled)
+         extra_toolchain_flags, target, when=is_linker_option_enabled)
 @checking('for linker', lambda x: x.KIND)
 @imports('os')
 @imports('shutil')
 def select_linker(linker, c_compiler, developer_options, enable_gold,
-                  toolchain_flags, target, lto):
+                  toolchain_flags, target):
 
     if linker:
         linker = linker[0]
-    elif lto and c_compiler.type == 'clang' and target.kernel != 'Darwin':
-        # If no linker was explicitly given, and building with clang for non-macOS,
-        # prefer lld. For macOS, we prefer ld64, or whatever the default linker is.
-        linker = 'lld'
     else:
         linker = None
 
     def is_valid_linker(linker):
         if target.kernel == 'Darwin':
             valid_linkers = ('ld64', 'lld')
         else:
             valid_linkers = ('bfd', 'gold', 'lld')
