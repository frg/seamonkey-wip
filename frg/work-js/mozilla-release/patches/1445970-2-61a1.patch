# HG changeset patch
# User Ted Campbell <tcampbell@mozilla.com>
# Date 1521144481 14400
#      Thu Mar 15 16:08:01 2018 -0400
# Node ID fa371afe65787ef2d43b3d81c6b393928371e263
# Parent  7447cde0139912715f547088334e0af5ef2407f6
Bug 1445970 - Fix ARM64 simulator on MSVC / Windows. r=jandem,lth

More of the same MSVC fixes. Also fix static asserts and memory fence
intrinsics.

MozReview-Commit-ID: Gyv9AUtNCXr

diff --git a/js/src/jit/arm64/Architecture-arm64.h b/js/src/jit/arm64/Architecture-arm64.h
--- a/js/src/jit/arm64/Architecture-arm64.h
+++ b/js/src/jit/arm64/Architecture-arm64.h
@@ -280,17 +280,17 @@ class FloatRegisters
     // d31 is the ScratchFloatReg.
     static const SetType NonAllocatableMask = (SetType(1) << FloatRegisters::d31) * SpreadCoefficient;
 
     static const SetType AllocatableMask = AllMask & ~NonAllocatableMask;
     union RegisterContent {
         float s;
         double d;
     };
-    enum Kind {
+    enum Kind : uint8_t {
         Double,
         Single
     };
 };
 
 // In bytes: slots needed for potential memory->memory move spills.
 //   +8 for cycles
 //   +8 for gpr spills
@@ -448,18 +448,18 @@ struct FloatRegister
     }
 
     static TypedRegisterSet<FloatRegister> ReduceSetForPush(const TypedRegisterSet<FloatRegister>& s);
     static uint32_t GetSizeInBytes(const TypedRegisterSet<FloatRegister>& s);
     static uint32_t GetPushSizeInBytes(const TypedRegisterSet<FloatRegister>& s);
     uint32_t getRegisterDumpOffsetInBytes();
 
   public:
-    Code code_ : 8;
-    FloatRegisters::Kind k_ : 1;
+    Code code_;
+    FloatRegisters::Kind k_;
 };
 
 template <> inline FloatRegister::SetType
 FloatRegister::LiveAsIndexableSet<RegTypeName::Float32>(SetType set)
 {
     return set & FloatRegisters::AllSingleMask;
 }
 
diff --git a/js/src/jit/arm64/MacroAssembler-arm64.h b/js/src/jit/arm64/MacroAssembler-arm64.h
--- a/js/src/jit/arm64/MacroAssembler-arm64.h
+++ b/js/src/jit/arm64/MacroAssembler-arm64.h
@@ -225,21 +225,21 @@ class MacroAssemblerCompat : public vixl
         pop(v.valueReg());
     }
     void pop(const FloatRegister& f) {
         vixl::MacroAssembler::Pop(ARMRegister(f.code(), 64));
     }
 
     void implicitPop(uint32_t args) {
         MOZ_ASSERT(args % sizeof(intptr_t) == 0);
-        adjustFrame(-args);
+        adjustFrame(0 - args);
     }
     void Pop(ARMRegister r) {
         vixl::MacroAssembler::Pop(r);
-        adjustFrame(- r.size() / 8);
+        adjustFrame(0 - r.size() / 8);
     }
     // FIXME: This is the same on every arch.
     // FIXME: If we can share framePushed_, we can share this.
     // FIXME: Or just make it at the highest level.
     CodeOffset PushWithPatch(ImmWord word) {
         framePushed_ += sizeof(word.value);
         return pushWithPatch(word);
     }
diff --git a/js/src/jit/arm64/vixl/Assembler-vixl.cpp b/js/src/jit/arm64/vixl/Assembler-vixl.cpp
--- a/js/src/jit/arm64/vixl/Assembler-vixl.cpp
+++ b/js/src/jit/arm64/vixl/Assembler-vixl.cpp
@@ -33,30 +33,30 @@
 namespace vixl {
 
 // CPURegList utilities.
 CPURegister CPURegList::PopLowestIndex() {
   if (IsEmpty()) {
     return NoCPUReg;
   }
   int index = CountTrailingZeros(list_);
-  VIXL_ASSERT((1 << index) & list_);
+  VIXL_ASSERT((1ULL << index) & list_);
   Remove(index);
   return CPURegister(index, size_, type_);
 }
 
 
 CPURegister CPURegList::PopHighestIndex() {
   VIXL_ASSERT(IsValid());
   if (IsEmpty()) {
     return NoCPUReg;
   }
   int index = CountLeadingZeros(list_);
   index = kRegListSizeInBits - 1 - index;
-  VIXL_ASSERT((1 << index) & list_);
+  VIXL_ASSERT((1ULL << index) & list_);
   Remove(index);
   return CPURegister(index, size_, type_);
 }
 
 
 bool CPURegList::IsValid() const {
   if ((type_ == CPURegister::kRegister) ||
       (type_ == CPURegister::kVRegister)) {
diff --git a/js/src/jit/arm64/vixl/Disasm-vixl.cpp b/js/src/jit/arm64/vixl/Disasm-vixl.cpp
--- a/js/src/jit/arm64/vixl/Disasm-vixl.cpp
+++ b/js/src/jit/arm64/vixl/Disasm-vixl.cpp
@@ -3122,17 +3122,17 @@ int Disassembler::SubstituteImmediateFie
           } else if (strncmp(format, "IVMIImm8", strlen("IVMIImm8")) == 0) {
             uint64_t imm8 = instr->ImmNEONabcdefgh();
             AppendToOutput("#0x%" PRIx64, imm8);
             return strlen("IVMIImm8");
           } else if (strncmp(format, "IVMIImm", strlen("IVMIImm")) == 0) {
             uint64_t imm8 = instr->ImmNEONabcdefgh();
             uint64_t imm = 0;
             for (int i = 0; i < 8; ++i) {
-              if (imm8 & (1 << i)) {
+              if (imm8 & (1ULL << i)) {
                 imm |= (UINT64_C(0xff) << (8 * i));
               }
             }
             AppendToOutput("#0x%" PRIx64, imm);
             return strlen("IVMIImm");
           } else if (strncmp(format, "IVMIShiftAmt1",
                              strlen("IVMIShiftAmt1")) == 0) {
             int cmode = instr->NEONCmode();
diff --git a/js/src/jit/arm64/vixl/Globals-vixl.h b/js/src/jit/arm64/vixl/Globals-vixl.h
--- a/js/src/jit/arm64/vixl/Globals-vixl.h
+++ b/js/src/jit/arm64/vixl/Globals-vixl.h
@@ -76,20 +76,19 @@ const int MBytes = 1024 * KBytes;
   #define VIXL_ASSERT(condition) ((void) 0)
   #define VIXL_CHECK(condition) ((void) 0)
   #define VIXL_UNIMPLEMENTED() ((void) 0)
   #define VIXL_UNREACHABLE() MOZ_MAKE_COMPILER_ASSUME_IS_UNREACHABLE()
 #endif
 // This is not as powerful as template based assertions, but it is simple.
 // It assumes that the descriptions are unique. If this starts being a problem,
 // we can switch to a different implemention.
-#define VIXL_CONCAT(a, b) a##b
+#define VIXL_S(x) #x
 #define VIXL_STATIC_ASSERT_LINE(line, condition) \
-  typedef char VIXL_CONCAT(STATIC_ASSERT_LINE_, line)[(condition) ? 1 : -1] \
-  __attribute__((unused))
+    static_assert(condition, "STATIC_ASSERT_LINE_" VIXL_S(line))
 #define VIXL_STATIC_ASSERT(condition) \
     VIXL_STATIC_ASSERT_LINE(__LINE__, condition)
 
 template <typename T1>
 inline void USE(T1) {}
 
 template <typename T1, typename T2>
 inline void USE(T1, T2) {}
diff --git a/js/src/jit/arm64/vixl/Instructions-vixl.cpp b/js/src/jit/arm64/vixl/Instructions-vixl.cpp
--- a/js/src/jit/arm64/vixl/Instructions-vixl.cpp
+++ b/js/src/jit/arm64/vixl/Instructions-vixl.cpp
@@ -324,21 +324,21 @@ int32_t Instruction::ImmBranchMaxForward
       return 0;
   }
 }
 
 int32_t Instruction::ImmBranchMinBackwardOffset(ImmBranchRangeType range_type)
 {
   switch(range_type) {
     case TestBranchRangeType:
-      return -int32_t(1 << ImmTestBranch_width) / 2 * kInstructionSize;
+      return -int32_t(1 << ImmTestBranch_width) / int32_t(2 * kInstructionSize);
     case CondBranchRangeType:
-      return -int32_t(1 << ImmCondBranch_width) / 2 * kInstructionSize;
+      return -int32_t(1 << ImmCondBranch_width) / int32_t(2 * kInstructionSize);
     case UncondBranchRangeType:
-      return -int32_t(1 << ImmUncondBranch_width) / 2 * kInstructionSize;
+      return -int32_t(1 << ImmUncondBranch_width) / int32_t(2 * kInstructionSize);
     default:
       VIXL_UNREACHABLE();
       return 0;
   }
 }
 
 const Instruction* Instruction::ImmPCOffsetTarget() const {
   const Instruction * base = this;
diff --git a/js/src/jit/arm64/vixl/MozAssembler-vixl.cpp b/js/src/jit/arm64/vixl/MozAssembler-vixl.cpp
--- a/js/src/jit/arm64/vixl/MozAssembler-vixl.cpp
+++ b/js/src/jit/arm64/vixl/MozAssembler-vixl.cpp
@@ -534,37 +534,37 @@ struct PoolHeader {
     // The size is in units of Instruction (4bytes), not byte.
     union {
       struct {
         uint32_t size : 15;
 
 	// "Natural" guards are part of the normal instruction stream,
 	// while "non-natural" guards are inserted for the sole purpose
 	// of skipping around a pool.
-        bool isNatural : 1;
+        uint32_t isNatural : 1;
         uint32_t ONES : 16;
       };
       uint32_t data;
     };
 
     Header(int size_, bool isNatural_)
       : size(size_),
         isNatural(isNatural_),
         ONES(0xffff)
     { }
 
     Header(uint32_t data)
       : data(data)
     {
-      JS_STATIC_ASSERT(sizeof(Header) == sizeof(uint32_t));
+      VIXL_STATIC_ASSERT(sizeof(Header) == sizeof(uint32_t));
       VIXL_ASSERT(ONES == 0xffff);
     }
 
     uint32_t raw() const {
-      JS_STATIC_ASSERT(sizeof(Header) == sizeof(uint32_t));
+      VIXL_STATIC_ASSERT(sizeof(Header) == sizeof(uint32_t));
       return data;
     }
   };
 
   PoolHeader(int size_, bool isNatural_)
     : data(Header(size_, isNatural_).raw())
   { }
 
diff --git a/js/src/jit/arm64/vixl/Simulator-vixl.cpp b/js/src/jit/arm64/vixl/Simulator-vixl.cpp
--- a/js/src/jit/arm64/vixl/Simulator-vixl.cpp
+++ b/js/src/jit/arm64/vixl/Simulator-vixl.cpp
@@ -28,16 +28,18 @@
 
 #ifdef JS_SIMULATOR_ARM64
 
 #include "jit/arm64/vixl/Simulator-vixl.h"
 
 #include <cmath>
 #include <string.h>
 
+#include "jit/AtomicOperations.h"
+
 namespace vixl {
 
 const Instruction* Simulator::kEndOfSimAddress = NULL;
 
 void SimSystemRegister::SetBits(int msb, int lsb, uint32_t bits) {
   int width = msb - lsb + 1;
   VIXL_ASSERT(is_uintn(width, bits) || is_intn(width, bits));
 
@@ -1335,28 +1337,28 @@ void Simulator::VisitLoadStoreExclusive(
         set_xreg(rt2, Read<uint64_t>(address + element_size), NoRegLog);
         break;
       default:
         VIXL_UNREACHABLE();
     }
 
     if (is_acquire_release) {
       // Approximate load-acquire by issuing a full barrier after the load.
-      __sync_synchronize();
+      js::jit::AtomicOperations::fenceSeqCst();
     }
 
     LogRead(address, rt, GetPrintRegisterFormatForSize(element_size));
     if (is_pair) {
       LogRead(address + element_size, rt2,
               GetPrintRegisterFormatForSize(element_size));
     }
   } else {
     if (is_acquire_release) {
       // Approximate store-release by issuing a full barrier before the store.
-      __sync_synchronize();
+      js::jit::AtomicOperations::fenceSeqCst();
     }
 
     bool do_store = true;
     if (is_exclusive) {
       do_store = local_monitor_.IsExclusive(address, access_size) &&
                  global_monitor_.IsExclusive(address, access_size);
       set_wreg(rs, do_store ? 0 : 1);
 
@@ -2325,17 +2327,17 @@ void Simulator::VisitSystem(const Instru
   } else if (instr->Mask(SystemHintFMask) == SystemHintFixed) {
     VIXL_ASSERT(instr->Mask(SystemHintMask) == HINT);
     switch (instr->ImmHint()) {
       case NOP: break;
       case CSDB: break;
       default: VIXL_UNIMPLEMENTED();
     }
   } else if (instr->Mask(MemBarrierFMask) == MemBarrierFixed) {
-    __sync_synchronize();
+    js::jit::AtomicOperations::fenceSeqCst();
   } else if ((instr->Mask(SystemSysFMask) == SystemSysFixed)) {
     switch (instr->Mask(SystemSysMask)) {
       case SYS: SysOp_W(instr->SysOp(), xreg(instr->Rt())); break;
       default: VIXL_UNIMPLEMENTED();
     }
   } else {
     VIXL_UNIMPLEMENTED();
   }
@@ -3271,17 +3273,17 @@ void Simulator::VisitNEONModifiedImmedia
     case 0x7:
       if (cmode_0 == 0 && op_bit == 0) {
         vform = q ? kFormat16B : kFormat8B;
         imm = imm8;
       } else if (cmode_0 == 0 && op_bit == 1) {
         vform = q ? kFormat2D : kFormat1D;
         imm = 0;
         for (int i = 0; i < 8; ++i) {
-          if (imm8 & (1 << i)) {
+          if (imm8 & (1ULL << i)) {
             imm |= (UINT64_C(0xff) << (8 * i));
           }
         }
       } else {  // cmode_0 == 1, cmode == 0xf.
         if (op_bit == 0) {
           vform = q ? kFormat4S : kFormat2S;
           imm = float_to_rawbits(instr->ImmNEONFP32());
         } else if (q == 1) {
diff --git a/js/src/jit/arm64/vixl/Utils-vixl.h b/js/src/jit/arm64/vixl/Utils-vixl.h
--- a/js/src/jit/arm64/vixl/Utils-vixl.h
+++ b/js/src/jit/arm64/vixl/Utils-vixl.h
@@ -174,17 +174,17 @@ inline double FusedMultiplyAdd(double op
 
 
 inline float FusedMultiplyAdd(float op1, float op2, float a) {
   return fmaf(op1, op2, a);
 }
 
 
 inline uint64_t LowestSetBit(uint64_t value) {
-  return value & -value;
+  return value & (0 - value);
 }
 
 
 template<typename T>
 inline int HighestSetBitPosition(T value) {
   VIXL_ASSERT(value != 0);
   return (sizeof(value) * 8 - 1) - CountLeadingZeros(value);
 }
@@ -211,17 +211,17 @@ T ReverseBits(T value) {
   }
   return result;
 }
 
 
 template <typename T>
 T ReverseBytes(T value, int block_bytes_log2) {
   VIXL_ASSERT((sizeof(value) == 4) || (sizeof(value) == 8));
-  VIXL_ASSERT((1U << block_bytes_log2) <= sizeof(value));
+  VIXL_ASSERT((1ULL << block_bytes_log2) <= sizeof(value));
   // Split the 64-bit value into an 8-bit array, where b[0] is the least
   // significant byte, and b[7] is the most significant.
   uint8_t bytes[8];
   uint64_t mask = 0xff00000000000000;
   for (int i = 7; i >= 0; i--) {
     bytes[i] = (static_cast<uint64_t>(value) & mask) >> (i * 8);
     mask >>= 8;
   }
