# HG changeset patch
# User Valentin Gosu <valentin.gosu@gmail.com>
# Date 1516745584 -3600
# Node ID 2c575454e11e66ab84ddb103afe4f52ebd7e9d48
# Parent  730c78f10ebb682dad5eedb5b1d42aa61bf497fc
Bug 1432187 - Add nsIStandardURLMutator interface r=mayhemer

MozReview-Commit-ID: 4IbdO2gMwox

diff --git a/netwerk/base/nsIStandardURL.idl b/netwerk/base/nsIStandardURL.idl
--- a/netwerk/base/nsIStandardURL.idl
+++ b/netwerk/base/nsIStandardURL.idl
@@ -1,16 +1,17 @@
 /* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "nsIMutable.idl"
 
 interface nsIURI;
+interface nsIURIMutator;
 
 /**
  * nsIStandardURL defines the interface to an URL with the standard
  * file path format common to protocols like http, ftp, and file.
  * It supports initialization from a relative path and provides
  * some customization on how URLs are normalized.
  */
 [scriptable, uuid(babd6cca-ebe7-4329-967c-d6b9e33caa81)]
@@ -35,16 +36,28 @@ interface nsIStandardURL : nsIMutable
     /**
      * blah:foo/bar    => blah:///foo/bar
      * blah:/foo/bar   => blah:///foo/bar
      * blah://foo/bar  => blah://foo/bar
      * blah:///foo/bar => blah:///foo/bar
      */
     const unsigned long URLTYPE_NO_AUTHORITY    = 3;
 
+    void init(in unsigned long aUrlType,
+              in long aDefaultPort,
+              in AUTF8String aSpec,
+              in string aOriginCharset,
+              in nsIURI aBaseURI);
+
+    void setDefaultPort(in long aNewDefaultPort);
+};
+
+[scriptable, builtinclass, uuid(fc894e98-23a1-43cd-a7fe-72876f8ea2ee)]
+interface nsIStandardURLMutator : nsISupports
+{
     /**
      * Initialize a standard URL.
      *
      * @param aUrlType       - one of the URLTYPE_ flags listed above.
      * @param aDefaultPort   - if the port parsed from the URL string matches
      *                         this port, then the port will be removed from the
      *                         canonical form of the URL.
      * @param aSpec          - URL string.
@@ -55,26 +68,26 @@ interface nsIStandardURL : nsIMutable
      *                         null, then provide aBaseURI implements this
      *                         interface, the origin charset of aBaseURI will
      *                         be assumed, otherwise defaulting to UTF-8 (i.e.,
      *                         no charset transformation from aSpec).
      * @param aBaseURI       - if null, aSpec must specify an absolute URI.
      *                         otherwise, aSpec will be resolved relative
      *                         to aBaseURI.
      */
-    void init(in unsigned long aUrlType,
-              in long aDefaultPort,
-              in AUTF8String aSpec,
-              in string aOriginCharset,
-              in nsIURI aBaseURI);
+    nsIURIMutator init(in unsigned long aUrlType,
+                       in long aDefaultPort,
+                       in AUTF8String aSpec,
+                       in string aOriginCharset,
+                       in nsIURI aBaseURI);
 
     /**
      * Set the default port.
      *
      * Note: If this object is already using its default port (i.e. if it has
      * mPort == -1), then it will now implicitly be using the new default port.
      *
      * @param aNewDefaultPort - if the URI has (or is later given) a port that
      *                          matches this default, then we won't include a
      *                          port number in the canonical form of the URL.
      */
-    void setDefaultPort(in long aNewDefaultPort);
+    nsIURIMutator setDefaultPort(in long aNewDefaultPort);
 };
diff --git a/netwerk/base/nsStandardURL.cpp b/netwerk/base/nsStandardURL.cpp
--- a/netwerk/base/nsStandardURL.cpp
+++ b/netwerk/base/nsStandardURL.cpp
@@ -2219,17 +2219,17 @@ nsStandardURL::SetPath(const nsACString 
         mBasename.mLen = -1;
         mExtension.mLen = -1;
         mQuery.mLen = -1;
         mRef.mLen = -1;
     }
     return NS_OK;
 }
 
-NS_IMPL_ISUPPORTS(nsStandardURL::Mutator, nsIURISetters, nsIURIMutator)
+NS_IMPL_ISUPPORTS(nsStandardURL::Mutator, nsIURISetters, nsIURIMutator, nsIStandardURLMutator)
 
 NS_IMETHODIMP
 nsStandardURL::Mutate(nsIURIMutator** aMutator)
 {
     RefPtr<nsStandardURL::Mutator> mutator = new nsStandardURL::Mutator();
     nsresult rv = mutator->InitFromURI(this);
     if (NS_FAILED(rv)) {
         return rv;
diff --git a/netwerk/base/nsStandardURL.h b/netwerk/base/nsStandardURL.h
--- a/netwerk/base/nsStandardURL.h
+++ b/netwerk/base/nsStandardURL.h
@@ -311,21 +311,59 @@ public:
 #ifdef DEBUG_DUMP_URLS_AT_SHUTDOWN
     void PrintSpec() const { printf("  %s\n", mSpec.get()); }
 #endif
 
 public:
     class Mutator
         : public nsIURIMutator
         , public BaseURIMutator<nsStandardURL>
+        , public nsIStandardURLMutator
     {
         NS_DECL_ISUPPORTS
         NS_FORWARD_SAFE_NSIURISETTERS_RET(mURI)
         NS_DEFINE_NSIMUTATOR_COMMON
 
+        MOZ_MUST_USE NS_IMETHOD
+        Init(uint32_t aURLType, int32_t aDefaultPort,
+             const nsACString& aSpec, const char* aCharset, nsIURI* aBaseURI,
+             nsIURIMutator** aMutator) override
+        {
+            if (aMutator) {
+                nsCOMPtr<nsIURIMutator> mutator = this;
+                mutator.forget(aMutator);
+            }
+            RefPtr<nsStandardURL> uri;
+            if (mURI) {
+              // We don't need to instantiate a new object we already have one
+              mURI.swap(uri);
+            } else {
+              uri = new nsStandardURL();
+            }
+            nsresult rv = uri->Init(aURLType, aDefaultPort, aSpec, aCharset, aBaseURI);
+            if (NS_FAILED(rv)) {
+                return rv;
+            }
+            mURI = uri;
+            return NS_OK;
+        }
+
+        MOZ_MUST_USE NS_IMETHODIMP
+        SetDefaultPort(int32_t aNewDefaultPort, nsIURIMutator** aMutator) override
+        {
+            if (!mURI) {
+                return NS_ERROR_NULL_POINTER;
+            }
+            if (aMutator) {
+                nsCOMPtr<nsIURIMutator> mutator = this;
+                mutator.forget(aMutator);
+            }
+            return mURI->SetDefaultPort(aNewDefaultPort);
+        }
+
         explicit Mutator() { }
     private:
         virtual ~Mutator() { }
 
         friend class nsStandardURL;
     };
 };
 
