# HG changeset patch
# User Julian Descottes <jdescottes@mozilla.com>
# Date 1507625012 -7200
# Node ID caef338370a531902aea56a36f3c8f9b6e41c501
# Parent  962e601962b41aa0657c3c72a76de934a9a33bac
Bug 1407426 - add smoke test for debugger in browser content toolbox;r=jlast

This test is a simpler version of the breakpoints test, but runs from the
browser content toolbox. We don't particularly assess that the debugger can
see more sources than the web content sources, but at least this ensures that
the debugger is correctly initialized.

MozReview-Commit-ID: 5rSb7z3HP4F

diff --git a/devtools/client/debugger/new/test/mochitest/browser.ini b/devtools/client/debugger/new/test/mochitest/browser.ini
--- a/devtools/client/debugger/new/test/mochitest/browser.ini
+++ b/devtools/client/debugger/new/test/mochitest/browser.ini
@@ -62,16 +62,18 @@ support-files =
 [browser_dbg-async-stepping.js]
 [browser_dbg-breaking.js]
 [browser_dbg-breaking-from-console.js]
 [browser_dbg-breakpoints.js]
 [browser_dbg-breakpoints-toggle.js]
 [browser_dbg-breakpoints-reloading.js]
 skip-if = true # Bug 1383576
 [browser_dbg-breakpoints-cond.js]
+[browser_dbg-browser-content-toolbox.js]
+skip-if = !e10s # This test is only valid in e10s
 [browser_dbg-call-stack.js]
 [browser_dbg-scopes.js]
 [browser_dbg-chrome-create.js]
 [browser_dbg-chrome-debugging.js]
 skip-if = debug # bug 1374187
 [browser_dbg-console.js]
 [browser_dbg-debugger-buttons.js]
 [browser_dbg-editor-gutter.js]
@@ -102,10 +104,10 @@ skip-if = true # Bug 1393121, 1393299
 [browser_dbg-sourcemaps.js]
 [browser_dbg-sourcemaps-reloading.js]
 [browser_dbg-sourcemaps2.js]
 [browser_dbg-sourcemaps-bogus.js]
 [browser_dbg-sources.js]
 [browser_dbg-tabs.js]
 [browser_dbg-toggling-tools.js]
 [browser_dbg-wasm-sourcemaps.js]
-skip-if = true 
+skip-if = true
 [browser_dbg-reload.js]
diff --git a/devtools/client/debugger/new/test/mochitest/browser_dbg-browser-content-toolbox.js b/devtools/client/debugger/new/test/mochitest/browser_dbg-browser-content-toolbox.js
new file mode 100644
--- /dev/null
+++ b/devtools/client/debugger/new/test/mochitest/browser_dbg-browser-content-toolbox.js
@@ -0,0 +1,73 @@
+/* -*- indent-tabs-mode: nil; js-indent-level: 2 -*- */
+/* vim: set ft=javascript ts=2 et sw=2 tw=80: */
+/* Any copyright is dedicated to the Public Domain.
+ * http://creativecommons.org/publicdomain/zero/1.0/ */
+
+"use strict";
+
+/**
+ * Tests that the debugger is succesfully loaded in the Browser Content Toolbox.
+ */
+
+const {gDevToolsBrowser} = require("devtools/client/framework/devtools-browser");
+
+function toggleBreakpoint(dbg, index) {
+  const bp = findElement(dbg, "breakpointItem", index);
+  const input = bp.querySelector("input");
+  input.click();
+}
+
+async function disableBreakpoint(dbg, index) {
+  const disabled = waitForDispatch(dbg, "DISABLE_BREAKPOINT");
+  toggleBreakpoint(dbg, index);
+  await disabled;
+}
+
+async function enableBreakpoint(dbg, index) {
+  const enabled = waitForDispatch(dbg, "ENABLE_BREAKPOINT");
+  toggleBreakpoint(dbg, index);
+  await enabled;
+}
+
+function findBreakpoint(dbg, url, line) {
+  const { selectors: { getBreakpoint }, getState } = dbg;
+  const source = findSource(dbg, url);
+  return getBreakpoint(getState(), { sourceId: source.id, line });
+}
+
+add_task(async function() {
+  clearDebuggerPreferences();
+
+  info("Open a tab pointing to doc-scripts.html");
+  await addTab(EXAMPLE_URL + "doc-scripts.html");
+
+  info("Open the Browser Content Toolbox");
+  let toolbox = await gDevToolsBrowser.openContentProcessToolbox(gBrowser);
+
+  info("Wait for the debugger to be ready");
+  await toolbox.getPanelWhenReady("jsdebugger");
+
+  let dbg = createDebuggerContext(toolbox);
+  ok(dbg, "Debugger context is available");
+
+  info("Create a breakpoint");
+  await selectSource(dbg, "simple2");
+  await addBreakpoint(dbg, "simple2", 3);
+
+  info("Disable the breakpoint");
+  await disableBreakpoint(dbg, 1);
+  let bp = findBreakpoint(dbg, "simple2", 3);
+  is(bp.disabled, true, "breakpoint is disabled");
+
+  info("Enable the breakpoint");
+  await enableBreakpoint(dbg, 1);
+  bp = findBreakpoint(dbg, "simple2", 3);
+  is(bp.disabled, false, "breakpoint is enabled");
+
+  info("Close the browser toolbox window");
+  let onToolboxDestroyed = toolbox.once("destroyed");
+  toolbox.win.top.close();
+  await onToolboxDestroyed;
+
+  info("Toolbox is destroyed");
+});
diff --git a/devtools/client/debugger/new/test/mochitest/head.js b/devtools/client/debugger/new/test/mochitest/head.js
--- a/devtools/client/debugger/new/test/mochitest/head.js
+++ b/devtools/client/debugger/new/test/mochitest/head.js
@@ -361,33 +361,40 @@ function createDebuggerContext(toolbox) 
     getState: store.getState,
     store: store,
     client: client,
     toolbox: toolbox,
     win: win
   };
 }
 
+
+/**
+ * Clear all the debugger related preferences.
+ */
+function clearDebuggerPreferences() {
+  Services.prefs.clearUserPref("devtools.debugger.pause-on-exceptions");
+  Services.prefs.clearUserPref("devtools.debugger.ignore-caught-exceptions");
+  Services.prefs.clearUserPref("devtools.debugger.tabs");
+  Services.prefs.clearUserPref("devtools.debugger.pending-selected-location");
+  Services.prefs.clearUserPref("devtools.debugger.pending-breakpoints");
+  Services.prefs.clearUserPref("devtools.debugger.expressions");
+}
+
 /**
  * Intilializes the debugger.
  *
  * @memberof mochitest
  * @param {String} url
- * @param {Array} sources
  * @return {Promise} dbg
  * @static
  */
-function initDebugger(url, ...sources) {
+function initDebugger(url) {
   return Task.spawn(function*() {
-    Services.prefs.clearUserPref("devtools.debugger.pause-on-exceptions");
-    Services.prefs.clearUserPref("devtools.debugger.ignore-caught-exceptions");
-    Services.prefs.clearUserPref("devtools.debugger.tabs");
-    Services.prefs.clearUserPref("devtools.debugger.pending-selected-location");
-    Services.prefs.clearUserPref("devtools.debugger.pending-breakpoints");
-    Services.prefs.clearUserPref("devtools.debugger.expressions");
+    clearDebuggerPreferences();
     const toolbox = yield openNewTabAndToolbox(EXAMPLE_URL + url, "jsdebugger");
     return createDebuggerContext(toolbox);
   });
 }
 
 window.resumeTest = undefined;
 /**
  * Pause the test and let you interact with the debugger.
