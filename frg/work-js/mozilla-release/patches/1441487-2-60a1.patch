# HG changeset patch
# User Dragan Mladjenovic <dragan.mladjenovic>
# Date 1519730122 -3600
#      Tue Feb 27 12:15:22 2018 +0100
# Node ID 1e97b5ef21b648fa1aa2f05d9c266c456a65c9e9
# Parent  6f2c1264ab95bc661d14ef3f5c17c202f739cd12
Bug 1441487 - [MIPS64] - Make simulator detect improper usage of 32-bit arithmetic r=lth

diff --git a/js/src/jit/mips64/Simulator-mips64.cpp b/js/src/jit/mips64/Simulator-mips64.cpp
--- a/js/src/jit/mips64/Simulator-mips64.cpp
+++ b/js/src/jit/mips64/Simulator-mips64.cpp
@@ -49,16 +49,22 @@
 #define U16(v)  static_cast<uint16_t>(v)
 #define I32(v)  static_cast<int32_t>(v)
 #define U32(v)  static_cast<uint32_t>(v)
 #define I64(v)  static_cast<int64_t>(v)
 #define U64(v)  static_cast<uint64_t>(v)
 #define I128(v) static_cast<__int128_t>(v)
 #define U128(v) static_cast<__uint128_t>(v)
 
+#define I32_CHECK(v) \
+({ \
+    MOZ_ASSERT(I64(I32(v)) == I64(v)); \
+    I32((v)); \
+})
+
 namespace js {
 namespace jit {
 
 static const Instr kCallRedirInstr = op_special | MAX_BREAK_CODE << FunctionBits | ff_break;
 
 // Utils functions.
 static uint32_t
 GetFCSRConditionBit(uint32_t cc)
@@ -2616,34 +2622,34 @@ Simulator::configureTypeRegister(SimInst
       case op_special:
         switch (instr->functionFieldRaw()) {
           case ff_jr:
           case ff_jalr:
             next_pc = getRegister(instr->rsValue());
             return_addr_reg = instr->rdValue();
             break;
           case ff_sll:
-            alu_out = I32(rt) << sa;
+            alu_out = I64(I32(rt) << sa);
             break;
           case ff_dsll:
             alu_out = rt << sa;
             break;
           case ff_dsll32:
             alu_out = rt << (sa + 32);
             break;
           case ff_srl:
             if (rs_reg == 0) {
                 // Regular logical right shift of a word by a fixed number of
                 // bits instruction. RS field is always equal to 0.
-                alu_out = I32(U32(rt) >> sa);
+                alu_out = I64(I32(U32(I32_CHECK(rt)) >> sa));
             } else {
                 // Logical right-rotate of a word by a fixed number of bits. This
                 // is special case of SRL instruction, added in MIPS32 Release 2.
                 // RS field is equal to 00001.
-                alu_out = I32((U32(rt) >> sa) | (U32(rt) << (32 - sa)));
+                alu_out = I64(I32((U32(I32_CHECK(rt)) >> sa) | (U32(I32_CHECK(rt)) << (32 - sa))));
             }
             break;
           case ff_dsrl:
             if (rs_reg == 0) {
                 // Regular logical right shift of a double word by a fixed number of
                 // bits instruction. RS field is always equal to 0.
                 alu_out = U64(rt) >> sa;
             } else {
@@ -2661,110 +2667,110 @@ Simulator::configureTypeRegister(SimInst
             } else {
                 // Logical right-rotate of a double word by a fixed number of bits. This
                 // is special case of DSRL instruction, added in MIPS64 Release 2.
                 // RS field is equal to 00001.
                 alu_out = (U64(rt) >> (sa + 32)) | (U64(rt) << (64 - (sa + 32)));
             }
             break;
           case ff_sra:
-            alu_out = I32(rt) >> sa;
+            alu_out = I64(I32_CHECK(rt)) >> sa;
             break;
           case ff_dsra:
             alu_out = rt >> sa;
             break;
           case ff_dsra32:
             alu_out = rt >> (sa + 32);
             break;
           case ff_sllv:
-            alu_out = I32(rt) << rs;
+            alu_out = I64(I32(rt) << rs);
             break;
           case ff_dsllv:
             alu_out = rt << rs;
             break;
           case ff_srlv:
             if (sa == 0) {
                 // Regular logical right-shift of a word by a variable number of
                 // bits instruction. SA field is always equal to 0.
-                alu_out = I32(U32(rt) >> rs);
+                alu_out = I64(I32(U32(I32_CHECK(rt)) >> rs));
             } else {
                 // Logical right-rotate of a word by a variable number of bits.
                 // This is special case od SRLV instruction, added in MIPS32
                 // Release 2. SA field is equal to 00001.
-                alu_out = I32((U32(rt) >> rs) | (U32(rt) << (32 - rs)));
+                alu_out = I64(I32((U32(I32_CHECK(rt)) >> rs) | (U32(I32_CHECK(rt)) << (32 - rs))));
             }
             break;
           case ff_dsrlv:
             if (sa == 0) {
                 // Regular logical right-shift of a double word by a variable number of
                 // bits instruction. SA field is always equal to 0.
                 alu_out = U64(rt) >> rs;
             } else {
                 // Logical right-rotate of a double word by a variable number of bits.
                 // This is special case od DSRLV instruction, added in MIPS64
                 // Release 2. SA field is equal to 00001.
                 alu_out = (U64(rt) >> rs) | (U64(rt) << (64 - rs));
             }
             break;
           case ff_srav:
-            alu_out = I32(rt) >> rs;
+            alu_out = I64(I32_CHECK(rt) >> rs);
             break;
           case ff_dsrav:
             alu_out = rt >> rs;
             break;
           case ff_mfhi:
             alu_out = getRegister(HI);
             break;
           case ff_mflo:
             alu_out = getRegister(LO);
             break;
           case ff_mult:
-            i128hilo = I32(rs) * I32(rt);
+            i128hilo = I64(U32(I32_CHECK(rs))) * I64(U32(I32_CHECK(rt)));
             break;
           case ff_dmult:
             i128hilo = I128(rs) * I128(rt);
             break;
           case ff_multu:
-            u128hilo = U32(rs) * U32(rt);
+            u128hilo = U64(U32(I32_CHECK(rs))) * U64(U32(I32_CHECK(rt)));
             break;
           case ff_dmultu:
             u128hilo = U128(rs) * U128(rt);
             break;
           case ff_add:
-            alu_out = I32(rs) + I32(rt);
+            alu_out = I32_CHECK(rs) + I32_CHECK(rt);
             if ((alu_out << 32) != (alu_out << 31))
               exceptions[kIntegerOverflow] = 1;
             alu_out = I32(alu_out);
             break;
           case ff_dadd:
             temp = I128(rs) + I128(rt);
             if ((temp << 64) != (temp << 63))
               exceptions[kIntegerOverflow] = 1;
             alu_out = I64(temp);
             break;
           case ff_addu:
-            alu_out = I32(U32(rs) + U32(rt));
+            alu_out = I32(I32_CHECK(rs) + I32_CHECK(rt));
             break;
           case ff_daddu:
             alu_out = rs + rt;
             break;
           case ff_sub:
-            alu_out = I32(rs) - I32(rt);
+            alu_out = I32_CHECK(rs) - I32_CHECK(rt);
             if ((alu_out << 32) != (alu_out << 31))
               exceptions[kIntegerUnderflow] = 1;
             alu_out = I32(alu_out);
             break;
           case ff_dsub:
             temp = I128(rs) - I128(rt);
             if ((temp << 64) != (temp << 63))
               exceptions[kIntegerUnderflow] = 1;
             alu_out = I64(temp);
             break;
           case ff_subu:
-            alu_out = I32(U32(rs) - U32(rt));
+            alu_out = I32(I32_CHECK(rs) - I32_CHECK(rt));
             break;
           case ff_dsubu:
             alu_out = rs - rt;
             break;
           case ff_and:
             alu_out = rs & rt;
             break;
           case ff_or:
@@ -2772,17 +2778,17 @@ Simulator::configureTypeRegister(SimInst
             break;
           case ff_xor:
             alu_out = rs ^ rt;
             break;
           case ff_nor:
             alu_out = ~(rs | rt);
             break;
           case ff_slt:
-            alu_out = rs < rt ? 1 : 0;
+            alu_out = I32_CHECK(rs) < I32_CHECK(rt) ? 1 : 0;
             break;
           case ff_sltu:
             alu_out = U64(rs) < U64(rt) ? 1 : 0;
             break;
           case ff_sync:
             break;
             // Break and trap instructions.
           case ff_break:
@@ -2807,36 +2813,36 @@ Simulator::configureTypeRegister(SimInst
             do_interrupt = rs != rt;
             break;
           case ff_movn:
           case ff_movz:
           case ff_movci:
             // No action taken on decode.
             break;
           case ff_div:
-            if (I32(rs) == INT_MIN && I32(rt) == -1) {
+            if (I32_CHECK(rs) == INT_MIN && I32_CHECK(rt) == -1) {
                 i128hilo = U32(INT_MIN);
             } else {
-                uint32_t div = I32(rs) / I32(rt);
-                uint32_t mod = I32(rs) % I32(rt);
+                uint32_t div = I32_CHECK(rs) / I32_CHECK(rt);
+                uint32_t mod = I32_CHECK(rs) % I32_CHECK(rt);
                 i128hilo = (I64(mod) << 32) | div;
             }
             break;
           case ff_ddiv:
-            if (I32(rs) == INT_MIN && I32(rt) == -1) {
+            if (I32_CHECK(rs) == INT_MIN && I32_CHECK(rt) == -1) {
                 i128hilo = U64(INT64_MIN);
             } else {
                 uint64_t div = rs / rt;
                 uint64_t mod = rs % rt;
                 i128hilo = (I128(mod) << 64) | div;
             }
             break;
           case ff_divu: {
-                uint32_t div = U32(rs) / U32(rt);
-                uint32_t mod = U32(rs) % U32(rt);
+                uint32_t div = U32(I32_CHECK(rs)) / U32(I32_CHECK(rt));
+                uint32_t mod = U32(I32_CHECK(rs)) % U32(I32_CHECK(rt));
                 i128hilo = (U64(mod) << 32) | div;
             }
             break;
           case ff_ddivu:
             if (0 == rt) {
                 i128hilo = (I128(Unpredictable) << 64) | I64(Unpredictable);
             } else {
                 uint64_t div = U64(rs) / U64(rt);
@@ -2846,20 +2852,20 @@ Simulator::configureTypeRegister(SimInst
             break;
           default:
             MOZ_CRASH();
         };
         break;
       case op_special2:
         switch (instr->functionFieldRaw()) {
           case ff_mul:
-            alu_out = I32(I32(rs) * I32(rt));  // Only the lower 32 bits are kept.
+            alu_out = I32(I32_CHECK(rs) * I32_CHECK(rt));  // Only the lower 32 bits are kept.
             break;
           case ff_clz:
-            alu_out = U32(rs) ? __builtin_clz(U32(rs)) : 32;
+            alu_out = U32(I32_CHECK(rs)) ? __builtin_clz(U32(I32_CHECK(rs))) : 32;
             break;
           case ff_dclz:
             alu_out = U64(rs) ? __builtin_clzl(U64(rs)) : 64;
             break;
           default:
             MOZ_CRASH();
         };
         break;
@@ -2870,17 +2876,17 @@ Simulator::configureTypeRegister(SimInst
             uint16_t msb = rd_reg;
             // Interpret sa field as 5-bit lsb of insert.
             uint16_t lsb = sa;
             uint16_t size = msb - lsb + 1;
             uint32_t mask = (1 << size) - 1;
             if (lsb > msb)
               alu_out = Unpredictable;
             else
-              alu_out = (U32(rt) & ~(mask << lsb)) | ((U32(rs) & mask) << lsb);
+              alu_out = (U32(I32_CHECK(rt)) & ~(mask << lsb)) | ((U32(I32_CHECK(rs)) & mask) << lsb);
             break;
           }
           case ff_dins: {   // Mips64r2 instruction.
             // Interpret rd field as 5-bit msb of insert.
             uint16_t msb = rd_reg;
             // Interpret sa field as 5-bit lsb of insert.
             uint16_t lsb = sa;
             uint16_t size = msb - lsb + 1;
@@ -2919,17 +2925,17 @@ Simulator::configureTypeRegister(SimInst
             uint16_t msb = rd_reg;
             // Interpret sa field as 5-bit lsb of extract.
             uint16_t lsb = sa;
             uint16_t size = msb + 1;
             uint32_t mask = (1 << size) - 1;
             if ((lsb + msb) > 31)
               alu_out = Unpredictable;
             else
-              alu_out = (U32(rs) & (mask << lsb)) >> lsb;
+              alu_out = (U32(I32_CHECK(rs)) & (mask << lsb)) >> lsb;
             break;
           }
           case ff_dext: {   // Mips64r2 instruction.
             // Interpret rd field as 5-bit msb of extract.
             uint16_t msb = rd_reg;
             // Interpret sa field as 5-bit lsb of extract.
             uint16_t lsb = sa;
             uint16_t size = msb + 1;
@@ -2960,19 +2966,19 @@ Simulator::configureTypeRegister(SimInst
             if ((lsb + msb + 1) > 64)
               alu_out = Unpredictable;
             else
               alu_out = (U64(rs) & (mask << lsb)) >> lsb;
             break;
           }
           case ff_bshfl: {   // Mips32r2 instruction.
             if (16 == sa) // seb
-              alu_out = I64(I8(rt));
-            else if (24 == sa) // seh
-              alu_out = I64(I16(rt));
+              alu_out = I64(I8(I32_CHECK(rt)));
+             else if (24 == sa) // seh
+              alu_out = I64(I16(I32_CHECK(rt)));
             break;
           }
           default:
             MOZ_CRASH();
         };
         break;
       default:
         MOZ_CRASH();
@@ -3667,29 +3673,29 @@ Simulator::decodeTypeImmediate(SimInstru
       case op_blez:
         do_branch = rs <= 0;
         break;
       case op_bgtz:
         do_branch = rs  > 0;
         break;
         // ------------- Arithmetic instructions.
       case op_addi:
-        alu_out = I32(rs) + se_imm16;
+        alu_out = I32_CHECK(rs) + se_imm16;
         if ((alu_out << 32) != (alu_out << 31))
           exceptions[kIntegerOverflow] = 1;
-        alu_out = I32(alu_out);
+        alu_out = I32_CHECK(alu_out);
         break;
       case op_daddi:
         temp = alu_out = rs + se_imm16;
         if ((temp << 64) != (temp << 63))
           exceptions[kIntegerOverflow] = 1;
         alu_out = I64(temp);
         break;
       case op_addiu:
-        alu_out = I32(I32(rs) + se_imm16);
+        alu_out = I32(I32_CHECK(rs) + se_imm16);
         break;
       case op_daddiu:
         alu_out = rs + se_imm16;
         break;
       case op_slti:
         alu_out = (rs < se_imm16) ? 1 : 0;
         break;
       case op_sltiu:
