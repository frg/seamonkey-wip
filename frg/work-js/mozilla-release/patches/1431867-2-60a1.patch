# HG changeset patch
# User Nika Layzell <nika@thelayzells.com>
# Date 1516391121 18000
# Node ID 280594a068332a0a8640356249b3f513229ad8ab
# Parent  a20affd3643b7f4adfad13d66b3a0c4e84f595a9
Bug 1431867 - Part 2: Add move constructors and assignment operators to IPDL unions, r=mccr8

MozReview-Commit-ID: IQVODEfMGi9

diff --git a/dom/clients/manager/ClientSourceOpChild.cpp.1431867needs1423328.later b/dom/clients/manager/ClientSourceOpChild.cpp.1431867needs1423328.later
new file mode 100644
--- /dev/null
+++ b/dom/clients/manager/ClientSourceOpChild.cpp.1431867needs1423328.later
@@ -0,0 +1,21 @@
+--- ClientSourceOpChild.cpp
++++ ClientSourceOpChild.cpp
+@@ -49,17 +49,17 @@ ClientSourceOpChild::DoSourceOp(Method a
+   // they encounter an error they should just immediately resolve or reject
+   // the promise as appropriate.
+   MOZ_DIAGNOSTIC_ASSERT(promise);
+ 
+   // Capture 'this' is safe here because we disconnect the promise
+   // ActorDestroy() which ensures nethier lambda is called if the
+   // actor is destroyed before the source operation completes.
+   promise->Then(target, __func__,
+-    [this, aArgs] (const mozilla::dom::ClientOpResult& aResult) {
++    [this] (const mozilla::dom::ClientOpResult& aResult) {
+       mPromiseRequestHolder.Complete();
+       Unused << PClientSourceOpChild::Send__delete__(this, aResult);
+     },
+     [this] (nsresult aRv) {
+       mPromiseRequestHolder.Complete();
+       Unused << PClientSourceOpChild::Send__delete__(this, aRv);
+     })->Track(mPromiseRequestHolder);
+ }
diff --git a/ipc/ipdl/ipdl/lower.py b/ipc/ipdl/ipdl/lower.py
--- a/ipc/ipdl/ipdl/lower.py
+++ b/ipc/ipdl/ipdl/lower.py
@@ -597,23 +597,32 @@ def _cxxConstRefType(ipdltype, side):
     return t
 
 def _cxxTypeNeedsMove(ipdltype):
     return ipdltype.isIPDL() and (ipdltype.isArray() or
                                   ipdltype.isShmem() or
                                   ipdltype.isByteBuf() or
                                   ipdltype.isEndpoint())
 
+def _cxxTypeCanMove(ipdltype):
+    return not (ipdltype.isIPDL() and ipdltype.isActor())
+
 def _cxxMoveRefType(ipdltype, side):
     t = _cxxBareType(ipdltype, side)
     if _cxxTypeNeedsMove(ipdltype):
         t.ref = 2
         return t
     return _cxxConstRefType(ipdltype, side)
 
+def _cxxForceMoveRefType(ipdltype, side):
+    assert _cxxTypeCanMove(ipdltype)
+    t = _cxxBareType(ipdltype, side)
+    t.ref = 2
+    return t
+
 def _cxxPtrToType(ipdltype, side):
     t = _cxxBareType(ipdltype, side)
     if ipdltype.isIPDL() and ipdltype.isActor():
         t.ptr = 0
         t.ptrptr = 1
         return t
     t.ptr = 1
     return t
@@ -692,16 +701,21 @@ necessarily a C++ reference."""
         """Return this decl's C++ Type with outparam semantics."""
         t = self.bareType(side)
         if self.ipdltype.isIPDL() and self.ipdltype.isActor():
             t.ptr = 0;  t.ptrptr = 1
             return t
         t.ptr = 1
         return t
 
+    def forceMoveType(self, side):
+        """Return this decl's C++ Type with forced move semantics."""
+        assert _cxxTypeCanMove(self.ipdltype)
+        return _cxxForceMoveRefType(self.ipdltype, side)
+
 ##--------------------------------------------------
 
 class HasFQName:
     def fqClassName(self):
         return self.decl.type.fullname()
 
 class _CompoundTypeComponent(_HybridDecl):
     def __init__(self, ipdltype, name, side, ct):
@@ -719,16 +733,18 @@ class _CompoundTypeComponent(_HybridDecl
     def constRefType(self, side=None):
         return _HybridDecl.constRefType(self, self.side)
     def ptrToType(self, side=None):
         return _HybridDecl.ptrToType(self, self.side)
     def constPtrToType(self, side=None):
         return _HybridDecl.constPtrToType(self, self.side)
     def inType(self, side=None):
         return _HybridDecl.inType(self, self.side)
+    def forceMoveType(self, side=None):
+        return _HybridDecl.forceMoveType(self, self.side)
 
 
 class StructDecl(ipdl.ast.StructDecl, HasFQName):
     @staticmethod
     def upgrade(structDecl):
         assert isinstance(structDecl, ipdl.ast.StructDecl)
         structDecl.__class__ = StructDecl
 
@@ -2011,17 +2027,17 @@ def _generateCxxStruct(sd):
 
     return forwarddeclstmts, fulldecltypes, struct
 
 ##--------------------------------------------------
 
 def _generateCxxUnion(ud):
     # This Union class basically consists of a type (enum) and a
     # union for storage.  The union can contain POD and non-POD
-    # types.  Each type needs a copy ctor, assignment operator,
+    # types.  Each type needs a copy/move ctor, assignment operators,
     # and dtor.
     #
     # Rather than templating this class and only providing
     # specializations for the types we support, which is slightly
     # "unsafe" in that C++ code can add additional specializations
     # without the IPDL compiler's knowledge, we instead explicitly
     # implement non-templated methods for each supported type.
     #
@@ -2036,28 +2052,31 @@ def _generateCxxUnion(ud):
     #  - entry in the type enum
     #  - entry in the storage union
     #  - [type]ptr() method to get a type* from the underlying union
     #  - same as above to get a const type*
     #  - typedef to hack around placement delete limitations
     # (public)
     #  - placement delete case for dtor
     #  - copy ctor
+    #  - move ctor
     #  - case in generic copy ctor
-    #  - operator= impl
+    #  - copy operator= impl
+    #  - move operator= impl
     #  - case in generic operator=
     #  - operator [type&]
     #  - operator [const type&] const
     #  - [type&] get_[type]()
     #  - [const type&] get_[type]() const
     #
     cls = Class(ud.name, final=1)
     # const Union&, i.e., Union type with inparam semantics
     inClsType = Type(ud.name, const=1, ref=1)
     refClsType = Type(ud.name, ref=1)
+    rvalueRefClsType = Type(ud.name, ref=2)
     typetype = Type('Type')
     valuetype = Type('Value')
     mtypevar = ExprVar('mType')
     mvaluevar = ExprVar('mValue')
     maybedtorvar = ExprVar('MaybeDestroy')
     assertsanityvar = ExprVar('AssertSanity')
     tnonevar = ExprVar('T__None')
     tlastvar = ExprVar('T__Last')
@@ -2074,16 +2093,19 @@ def _generateCxxUnion(ud):
     def callMaybeDestroy(newTypeVar):
         return ExprCall(maybedtorvar, args=[ newTypeVar ])
 
     def maybeReconstruct(memb, newTypeVar):
         ifdied = StmtIf(callMaybeDestroy(newTypeVar))
         ifdied.addifstmt(StmtExpr(memb.callCtor()))
         return ifdied
 
+    def voidCast(expr):
+        return ExprCast(expr, Type.VOID, static=1)
+
     # compute all the typedefs and forward decls we need to make
     gettypedeps = _ComputeTypeDeps(ud.decl.type)
     for c in ud.components:
         c.ipdltype.accept(gettypedeps)
 
     usingTypedefs = gettypedeps.usingTypedefs
     forwarddeclstmts = gettypedeps.forwardDeclStmts
     fulldecltypes = gettypedeps.fullDeclTypes
@@ -2193,26 +2215,35 @@ def _generateCxxUnion(ud):
     cls.addstmts([
         Label.PUBLIC,
         ConstructorDefn(
             ConstructorDecl(ud.name, force_inline=1),
             memberinits=[ ExprMemberInit(mtypevar, [ tnonevar ]) ]),
         Whitespace.NL
     ])
 
-    # Union(const T&) copy ctors
+    # Union(const T&) copy & Union(T&&) move ctors
     othervar = ExprVar('aOther')
     for c in ud.components:
         copyctor = ConstructorDefn(ConstructorDecl(
             ud.name, params=[ Decl(c.inType(), othervar.name) ]))
         copyctor.addstmts([
             StmtExpr(c.callCtor(othervar)),
             StmtExpr(ExprAssn(mtypevar, c.enumvar())) ])
         cls.addstmts([ copyctor, Whitespace.NL ])
 
+        if not _cxxTypeCanMove(c.ipdltype):
+            continue
+        movector = ConstructorDefn(ConstructorDecl(
+            ud.name, params=[ Decl(c.forceMoveType(), othervar.name) ]))
+        movector.addstmts([
+            StmtExpr(c.callCtor(ExprMove(othervar))),
+            StmtExpr(ExprAssn(mtypevar, c.enumvar())) ])
+        cls.addstmts([ movector, Whitespace.NL ])
+
     # Union(const Union&) copy ctor
     copyctor = ConstructorDefn(ConstructorDecl(
         ud.name, params=[ Decl(inClsType, othervar.name) ]))
     othertype = ud.callType(othervar)
     copyswitch = StmtSwitch(othertype)
     for c in ud.components:
         copyswitch.addcase(
             CaseLabel(c.enum()),
@@ -2229,46 +2260,101 @@ def _generateCxxUnion(ud):
         StmtBlock([ _logicError('unreached'), StmtReturn() ]))
     copyctor.addstmts([
         StmtExpr(callAssertSanity(uvar=othervar)),
         copyswitch,
         StmtExpr(ExprAssn(mtypevar, othertype))
     ])
     cls.addstmts([ copyctor, Whitespace.NL ])
 
+    # Union(Union&&) move ctor
+    movector = ConstructorDefn(ConstructorDecl(
+        ud.name, params=[ Decl(rvalueRefClsType, othervar.name) ]))
+    othertypevar = ExprVar("t")
+    moveswitch = StmtSwitch(othertypevar)
+    for c in ud.components:
+        case = StmtBlock()
+        if c.recursive:
+            # This is sound as we set othervar.mTypeVar to T__None after the
+            # switch. The pointer in the union will be left dangling.
+            case.addstmts([
+                # ptr_C() = other.ptr_C()
+                StmtExpr(ExprAssn(c.callGetPtr(),
+                                  ExprCall(ExprSelect(othervar, '.', ExprVar(c.getPtrName())))))
+            ])
+        else:
+            case.addstmts([
+                # new ... (Move(other.get_C()))
+                StmtExpr(c.callCtor(ExprMove(ExprCall(ExprSelect(othervar, '.', c.getTypeName()))))),
+                # other.MaybeDestroy(T__None)
+                StmtExpr(voidCast(ExprCall(ExprSelect(othervar, '.', maybedtorvar), args=[ tnonevar ]))),
+            ])
+        case.addstmts([ StmtBreak() ])
+        moveswitch.addcase(CaseLabel(c.enum()), case)
+    moveswitch.addcase(CaseLabel(tnonevar.name),
+                       StmtBlock([ StmtBreak() ]))
+    moveswitch.addcase(
+        DefaultLabel(),
+        StmtBlock([ _logicError('unreached'), StmtReturn() ]))
+    movector.addstmts([
+        StmtExpr(callAssertSanity(uvar=othervar)),
+        StmtDecl(Decl(typetype, othertypevar.name), init=ud.callType(othervar)),
+        moveswitch,
+        StmtExpr(ExprAssn(ExprSelect(othervar, '.', mtypevar), tnonevar)),
+        StmtExpr(ExprAssn(mtypevar, othertypevar))
+    ])
+    cls.addstmts([ movector, Whitespace.NL ])
+
     # ~Union()
     dtor = DestructorDefn(DestructorDecl(ud.name))
     # The void cast prevents Coverity from complaining about missing return
     # value checks.
-    dtor.addstmt(StmtExpr(ExprCast(callMaybeDestroy(tnonevar), Type.VOID,
-                                   static=1)))
+    dtor.addstmt(StmtExpr(voidCast(callMaybeDestroy(tnonevar))))
     cls.addstmts([ dtor, Whitespace.NL ])
 
     # type()
     typemeth = MethodDefn(MethodDecl('type', ret=typetype,
                                      const=1, force_inline=1))
     typemeth.addstmt(StmtReturn(mtypevar))
     cls.addstmts([ typemeth, Whitespace.NL ])
 
-    # Union& operator=(const T&) methods
+    # Union& operator= methods
     rhsvar = ExprVar('aRhs')
     for c in ud.components:
+        # Union& operator=(const T&)
         opeq = MethodDefn(MethodDecl(
             'operator=',
             params=[ Decl(c.inType(), rhsvar.name) ],
             ret=refClsType))
         opeq.addstmts([
             # might need to placement-delete old value first
             maybeReconstruct(c, c.enumvar()),
             StmtExpr(c.callOperatorEq(rhsvar)),
             StmtExpr(ExprAssn(mtypevar, c.enumvar())),
             StmtReturn(ExprDeref(ExprVar.THIS))
         ])
         cls.addstmts([ opeq, Whitespace.NL ])
 
+        # Union& operator=(T&&)
+        if not _cxxTypeCanMove(c.ipdltype):
+            continue
+
+        opeq = MethodDefn(MethodDecl(
+            'operator=',
+            params=[ Decl(c.forceMoveType(), rhsvar.name) ],
+            ret=refClsType))
+        opeq.addstmts([
+            # might need to placement-delete old value first
+            maybeReconstruct(c, c.enumvar()),
+            StmtExpr(c.callOperatorEq(ExprMove(rhsvar))),
+            StmtExpr(ExprAssn(mtypevar, c.enumvar())),
+            StmtReturn(ExprDeref(ExprVar.THIS))
+        ])
+        cls.addstmts([ opeq, Whitespace.NL ])
+
     # Union& operator=(const Union&)
     opeq = MethodDefn(MethodDecl(
         'operator=',
         params=[ Decl(inClsType, rhsvar.name) ],
         ret=refClsType))
     rhstypevar = ExprVar('t')
     opeqswitch = StmtSwitch(rhstypevar)
     for c in ud.components:
@@ -2295,16 +2381,61 @@ def _generateCxxUnion(ud):
         StmtExpr(callAssertSanity(uvar=rhsvar)),
         StmtDecl(Decl(typetype, rhstypevar.name), init=ud.callType(rhsvar)),
         opeqswitch,
         StmtExpr(ExprAssn(mtypevar, rhstypevar)),
         StmtReturn(ExprDeref(ExprVar.THIS))
     ])
     cls.addstmts([ opeq, Whitespace.NL ])
 
+    # Union& operator=(Union&&)
+    opeq = MethodDefn(MethodDecl(
+        'operator=',
+        params=[ Decl(rvalueRefClsType, rhsvar.name) ],
+        ret=refClsType))
+    rhstypevar = ExprVar('t')
+    opeqswitch = StmtSwitch(rhstypevar)
+    for c in ud.components:
+        case = StmtBlock()
+        if c.recursive:
+            case.addstmts([
+                StmtExpr(voidCast(callMaybeDestroy(tnonevar))),
+                StmtExpr(ExprAssn(c.callGetPtr(),
+                                  ExprCall(ExprSelect(rhsvar, '.', ExprVar(c.getPtrName()))))),
+            ])
+        else:
+            case.addstmts([
+                maybeReconstruct(c, rhstypevar),
+                StmtExpr(c.callOperatorEq(
+                    ExprMove(ExprCall(ExprSelect(rhsvar, '.', c.getTypeName()))))),
+                # other.MaybeDestroy(T__None)
+                StmtExpr(voidCast(ExprCall(ExprSelect(rhsvar, '.', maybedtorvar), args=[ tnonevar ]))),
+            ])
+        case.addstmts([ StmtBreak() ])
+        opeqswitch.addcase(CaseLabel(c.enum()), case)
+    opeqswitch.addcase(
+        CaseLabel(tnonevar.name),
+        # The void cast prevents Coverity from complaining about missing return
+        # value checks.
+        StmtBlock([ StmtExpr(voidCast(callMaybeDestroy(rhstypevar))),
+                    StmtBreak() ])
+    )
+    opeqswitch.addcase(
+        DefaultLabel(),
+        StmtBlock([ _logicError('unreached'), StmtBreak() ]))
+    opeq.addstmts([
+        StmtExpr(callAssertSanity(uvar=rhsvar)),
+        StmtDecl(Decl(typetype, rhstypevar.name), init=ud.callType(rhsvar)),
+        opeqswitch,
+        StmtExpr(ExprAssn(ExprSelect(rhsvar, '.', mtypevar), tnonevar)),
+        StmtExpr(ExprAssn(mtypevar, rhstypevar)),
+        StmtReturn(ExprDeref(ExprVar.THIS))
+    ])
+    cls.addstmts([ opeq, Whitespace.NL ])
+
     # bool operator==(const T&)
     for c in ud.components:
         opeqeq = MethodDefn(MethodDecl(
             'operator==',
             params=[ Decl(c.inType(), rhsvar.name) ],
             ret=Type.BOOL,
             const=1))
         opeqeq.addstmt(StmtReturn(ExprBinary(
