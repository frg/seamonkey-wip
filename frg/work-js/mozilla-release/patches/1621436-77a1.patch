# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1586391672 0
# Node ID 7d5a0c7bf181c579d7c59ce1e3782cf677451877
# Parent  4eadb8dda02e3d41499044290ec8778b3cb5ab63
Bug 1621436 - Run process_install_manifest with python3. r=rstewart

Differential Revision: https://phabricator.services.mozilla.com/D70156

diff --git a/Makefile.in b/Makefile.in
--- a/Makefile.in
+++ b/Makefile.in
@@ -108,27 +108,27 @@ endif
 .PHONY: $(addprefix install-,$(install_manifests))
 $(addprefix install-,$(install_manifests)): install-%: $(install_manifest_depends)
 ifneq (,$(filter FasterMake+RecursiveMake,$(BUILD_BACKENDS)))
 	@# If we're using the hybrid FasterMake/RecursiveMake backend, we want
 	@# to ensure the FasterMake end doesn't have install manifests for the
 	@# same directory, because that would blow up
 	$(if $(wildcard _build_manifests/install/$(subst /,_,$*)),$(if $(wildcard faster/install_$(subst /,_,$*)*),$(error FasterMake and RecursiveMake ends of the hybrid build system want to handle $*)))
 endif
-	$(addprefix $(call py_action,process_install_manifest,--track install_$(subst /,_,$*).track $*) ,$(wildcard _build_manifests/install/$(subst /,_,$*)))
+	$(addprefix $(call py3_action,process_install_manifest,--track install_$(subst /,_,$*).track $*) ,$(wildcard _build_manifests/install/$(subst /,_,$*)))
 
 # Dummy wrapper rule to allow the faster backend to piggy back
 $(addprefix install-,$(subst /,_,$(filter dist/%,$(install_manifests)))): install-dist_%: install-dist/% ;
 
 .PHONY: install-tests
 install-tests: install-test-files
 
 .PHONY: install-test-files
 install-test-files:
-	$(call py_action,process_install_manifest,--track install__test_files.track _tests _build_manifests/install/_test_files)
+	$(call py3_action,process_install_manifest,--track install__test_files.track _tests _build_manifests/install/_test_files)
 
 include $(topsrcdir)/build/moz-automation.mk
 
 # dist and _tests should be purged during cleaning. However, we don't want them
 # purged during PGO builds because they contain some auto-generated files.
 ifneq ($(filter-out maybe_clobber_profiledbuild,$(MAKECMDGOALS)),)
 GARBAGE_DIRS += dist _tests
 endif
diff --git a/config/faster/rules.mk b/config/faster/rules.mk
--- a/config/faster/rules.mk
+++ b/config/faster/rules.mk
@@ -75,17 +75,17 @@ endif
 # corresponding install manifests are named correspondingly, with forward
 # slashes replaced with underscores, and prefixed with `install_`. That is,
 # the install manifest for `dist/bin` would be `install_dist_bin`.
 $(addprefix install-,$(INSTALL_MANIFESTS)): install-%: $(addprefix $(TOPOBJDIR)/,buildid.h source-repo.h)
 	@# For now, force preprocessed files to be reprocessed every time.
 	@# The overhead is not that big, and this avoids waiting for proper
 	@# support for defines tracking in process_install_manifest.
 	@touch install_$(subst /,_,$*)
-	$(PYTHON) -m mozbuild.action.process_install_manifest \
+	$(PYTHON3) -m mozbuild.action.process_install_manifest \
 		--track install_$(subst /,_,$*).track \
 		$(TOPOBJDIR)/$* \
 		-DAB_CD=en-US \
 		$(ACDEFINES) \
 		install_$(subst /,_,$*)
 
 # ============================================================================
 # Below is a set of additional dependencies and variables used to build things
diff --git a/js/src/build/Makefile.in b/js/src/build/Makefile.in
--- a/js/src/build/Makefile.in
+++ b/js/src/build/Makefile.in
@@ -53,17 +53,17 @@ install:: ../js-config.h
 #          js/src/ds/* public headers
 #        gc/
 #          js/src/gc/* public headers
 #        mozilla/
 #          mfbt headers
 #
 
 install::
-	$(call py_action,process_install_manifest,--track install_dist_include.track --no-symlinks $(DESTDIR)$(includedir)/$(JS_LIBRARY_NAME) $(DEPTH)/_build_manifests/install/dist_include)
+	$(call py_action3,process_install_manifest,--track install_dist_include.track --no-symlinks $(DESTDIR)$(includedir)/$(JS_LIBRARY_NAME) $(DEPTH)/_build_manifests/install/dist_include)
 
 #
 # END SpiderMonkey header installation
 #############################################
 
 # Install versioned script, for parallel installability in Linux distributions
 install:: js-config
 	cp $^ js$(MOZJS_MAJOR_VERSION)-config
diff --git a/python/mozbuild/mozpack/files.py b/python/mozbuild/mozpack/files.py
--- a/python/mozbuild/mozpack/files.py
+++ b/python/mozbuild/mozpack/files.py
@@ -345,17 +345,20 @@ class AbsoluteSymlinkFile(File):
 
         # The logic in this function is complicated by the fact that symlinks
         # aren't universally supported. So, where symlinks aren't supported, we
         # fall back to file copying. Keep in mind that symlink support is
         # per-filesystem, not per-OS.
 
         # Handle the simple case where symlinks are definitely not supported by
         # falling back to file copy.
-        if not hasattr(os, 'symlink'):
+        # Python 3 supports symlinks on Windows, but for some reason, this has
+        # weird consequences on automation (but not in a local build). Exclude
+        # Windows for now until we figure out what the cause is.
+        if not hasattr(os, 'symlink') or platform.system() == 'Windows':
             return File.copy(self, dest, skip_if_older=skip_if_older)
 
         # Always verify the symlink target path exists.
         if not os.path.exists(self.path):
             raise ErrorMessage('Symlink target path does not exist: %s' % self.path)
 
         st = None
 
@@ -547,17 +550,18 @@ class PreprocessedFile(BaseFile):
             assert isinstance(dest, Dest)
 
         # We have to account for the case where the destination exists and is a
         # symlink to something. Since we know the preprocessor is certainly not
         # going to create a symlink, we can just remove the existing one. If the
         # destination is not a symlink, we leave it alone, since we're going to
         # overwrite its contents anyway.
         # If symlinks aren't supported at all, we can skip this step.
-        if hasattr(os, 'symlink'):
+        # See comment in AbsoluteSymlinkFile about Windows.
+        if hasattr(os, 'symlink') and platform.system() != 'Windows':
             if os.path.islink(dest.path):
                 os.remove(dest.path)
 
         pp_deps = set(self.extra_depends)
 
         # If a dependency file was specified, and it exists, add any
         # dependencies from that file to our list.
         if self.depfile and os.path.exists(self.depfile):
diff --git a/python/mozbuild/mozpack/test/test_files.py b/python/mozbuild/mozpack/test/test_files.py
--- a/python/mozbuild/mozpack/test/test_files.py
+++ b/python/mozbuild/mozpack/test/test_files.py
@@ -55,16 +55,17 @@ from mozpack.chrome.manifest import (
     ManifestResource,
     ManifestLocale,
     ManifestOverride,
 )
 import unittest
 import mozfile
 import mozunit
 import os
+import platform
 import random
 import six
 import sys
 import tarfile
 import mozpack.path as mozpath
 from tempfile import mkdtemp
 from io import BytesIO
 from xpt import Typelib
@@ -72,17 +73,18 @@ from xpt import Typelib
 
 class TestWithTmpDir(unittest.TestCase):
     def setUp(self):
         self.tmpdir = mkdtemp()
 
         self.symlink_supported = False
         self.hardlink_supported = False
 
-        if hasattr(os, 'symlink'):
+        # See comment in mozpack.files.AbsoluteSymlinkFile
+        if hasattr(os, 'symlink') and platform.system() != 'Windows':
             dummy_path = self.tmppath('dummy_file')
             with open(dummy_path, 'a'):
                 pass
 
             try:
                 os.symlink(dummy_path, self.tmppath('dummy_symlink'))
                 os.remove(self.tmppath('dummy_symlink'))
             except EnvironmentError:
