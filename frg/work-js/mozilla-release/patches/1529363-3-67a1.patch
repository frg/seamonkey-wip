# HG changeset patch
# User Ian Moody <moz-ian@perix.co.uk>
# Date 1551910407 0
# Node ID fdb89003ea1348d0ce1b896d72038af51b1fe1d6
# Parent  a15a1fa9db944902328ff1453e10bd08601e6d63
Bug 1529363 - Fix SetDesktopBackground preview. r=paolo

Bug 1498274 removed the enclosing groupbox, which also removed the XBL box that
bounded the image size.  Add a vbox to do so.
Also add a test that checks the image's sizes match its natural sizes.

Differential Revision: https://phabricator.services.mozilla.com/D20547

diff --git a/browser/components/shell/content/setDesktopBackground.xul b/browser/components/shell/content/setDesktopBackground.xul
--- a/browser/components/shell/content/setDesktopBackground.xul
+++ b/browser/components/shell/content/setDesktopBackground.xul
@@ -50,21 +50,23 @@
       <spacer flex="1"/>
       <label value="&color.label;"/>
       <colorpicker id="desktopColor"
                    type="button" 
                    onchange="gSetBackground.updateColor(this.color);"/> 
     </hbox>
 #endif
 
-    <stack>
-      <!-- if width and height are not present, they default to 300x150 and stretch the stack -->
-      <html:canvas id="screen" width="1" height="1" role="presentation"/>
-      <image id="monitor"/>
-    </stack>
+    <vbox align="center">
+      <stack>
+        <!-- if width and height are not present, they default to 300x150 and stretch the stack -->
+        <html:canvas id="screen" width="1" height="1" role="presentation"/>
+        <image id="monitor"/>
+      </stack>
+    </vbox>
 
 #ifdef XP_MACOSX
     <separator/>
     
     <hbox align="right">
       <button id="setDesktopBackground"
               label="&setDesktopBackground.title;" 
               oncommand="gSetBackground.setDesktopBackground();"/>
diff --git a/browser/components/shell/test/browser.ini b/browser/components/shell/test/browser.ini
--- a/browser/components/shell/test/browser.ini
+++ b/browser/components/shell/test/browser.ini
@@ -1,8 +1,9 @@
 [DEFAULT]
 
 [browser_420786.js]
 skip-if = os != "linux"
 [browser_633221.js]
 skip-if = os != "linux"
 [browser_1119088.js]
 skip-if = os != "mac"
+[browser_setDesktopBackgroundPreview.js]
diff --git a/browser/components/shell/test/browser_setDesktopBackgroundPreview.js b/browser/components/shell/test/browser_setDesktopBackgroundPreview.js
new file mode 100644
--- /dev/null
+++ b/browser/components/shell/test/browser_setDesktopBackgroundPreview.js
@@ -0,0 +1,60 @@
+/* Any copyright is dedicated to the Public Domain.
+ * http://creativecommons.org/publicdomain/zero/1.0/ */
+
+/**
+ * Check whether the preview image for setDesktopBackground is rendered
+ * correctly, without stretching
+ */
+
+add_task(async function() {
+  await BrowserTestUtils.withNewTab({
+    gBrowser,
+    url: "about:logo",
+  }, async (browser) => {
+    const dialogLoad = BrowserTestUtils.domWindowOpened(
+      null,
+      async win => {
+        await BrowserTestUtils.waitForEvent(win, "load");
+        Assert.equal(
+          win.document.documentElement.getAttribute("windowtype"),
+          "Shell:SetDesktopBackground",
+          "Opened correct window"
+        );
+        return true;
+      }
+    );
+
+    const image = content.document.images[0];
+    EventUtils.synthesizeMouseAtCenter(image, { type: "contextmenu" });
+
+    const menu = document.getElementById("contentAreaContextMenu");
+    await BrowserTestUtils.waitForPopupEvent(menu, "shown");
+    document.getElementById("context-setDesktopBackground").click();
+
+    // Need to explicitly close the menu (and wait for it), otherwise it fails
+    // verify/later tests
+    const menuClosed = BrowserTestUtils.waitForPopupEvent(menu, "hidden");
+    menu.hidePopup();
+
+    const win = await dialogLoad;
+
+    /* setDesktopBackground.js does a setTimeout to wait for correct
+       dimensions. If we don't wait here we could read the monitor image
+       URL before it's changed to the widescreen version */
+    await TestUtils.waitForTick();
+
+    const img = win.document.getElementById("monitor");
+    const measure = new Image();
+    const measureLoad = BrowserTestUtils.waitForEvent(measure, "load");
+    measure.src =
+      getComputedStyle(img).listStyleImage.slice(4, -1).replace(/"/g, "");
+    await measureLoad;
+
+    Assert.equal(img.clientWidth, measure.naturalWidth, "Monitor image correct width");
+    Assert.equal(img.clientHeight, measure.naturalHeight, "Monitor image correct height");
+
+    win.close();
+
+    await menuClosed;
+  });
+});
