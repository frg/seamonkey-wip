# HG changeset patch
# User Jed Davis <jld@mozilla.com>
# Date 1518137202 25200
# Node ID 9dcf26ff6a6e910ed28745ea3604319a5536fcb1
# Parent  0d0720b89986630e31760495804f2215d7abd69d
Bug 1434528 - Adjust sandbox feature detection to deal with Ubuntu guest accounts. r=gcp

Guest sessions on Ubuntu (and maybe other distributions that use
LightDM?) apply an AppArmor policy that allows CLONE_NEWUSER but doesn't
allow using any of the capabilities it grants, or even configuring the
new user namespace.

This patch causes those environments to be detected as not supporting
unprivileged user namespaces, because for all practical purposes they
don't.

MozReview-Commit-ID: HVkoBakRwaA

diff --git a/security/sandbox/linux/SandboxInfo.cpp b/security/sandbox/linux/SandboxInfo.cpp
--- a/security/sandbox/linux/SandboxInfo.cpp
+++ b/security/sandbox/linux/SandboxInfo.cpp
@@ -171,17 +171,21 @@ CanCreateUserNamespace()
 #else
     // If something else can cause that call to fail, we's like to know
     // about it; the right way to handle it might not be the same.
     MOZ_ASSERT(false);
 #endif
     return false;
   }
 
-  pid_t pid = syscall(__NR_clone, SIGCHLD | CLONE_NEWUSER,
+  // Bug 1434528: In addition to CLONE_NEWUSER, do something that uses
+  // the new capabilities (in this case, cloning another namespace) to
+  // detect AppArmor policies that allow CLONE_NEWUSER but don't allow
+  // doing anything useful with it.
+  pid_t pid = syscall(__NR_clone, SIGCHLD | CLONE_NEWUSER | CLONE_NEWPID,
                       nullptr, nullptr, nullptr, nullptr);
   if (pid == 0) {
     // In the child.  Do as little as possible.
     _exit(0);
   }
   if (pid == -1) {
     // Failure.
     MOZ_ASSERT(errno == EINVAL || // unsupported
