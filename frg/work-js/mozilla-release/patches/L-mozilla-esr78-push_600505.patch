# HG changeset patch
# User Jonathan Kew <jkew@mozilla.com>
# Date 1617183520 -3600
#      Wed Mar 31 10:38:40 2021 +0100
# Node ID 106fff71bae26aa8c372966e8a4f6745e36db8e1
# Parent  d378607f9c0f99dd0024dc17d065b9dbfb89c4d2
Bug 1699835 - Check that references in cached fontGroup match current presContext. r=lsalzman, a=tjr

diff --git a/dom/canvas/CanvasRenderingContext2D.cpp b/dom/canvas/CanvasRenderingContext2D.cpp
--- a/dom/canvas/CanvasRenderingContext2D.cpp
+++ b/dom/canvas/CanvasRenderingContext2D.cpp
@@ -4018,51 +4018,69 @@ TextMetrics* CanvasRenderingContext2D::D
     Redraw();
   }
 
   aError = NS_OK;
   return nullptr;
 }
 
 gfxFontGroup* CanvasRenderingContext2D::GetCurrentFontStyle() {
-  // use lazy initilization for the font group since it's rather expensive
-  if (!CurrentState().fontGroup) {
+  // Use lazy (re)initialization for the fontGroup since it's rather expensive.
+
+  RefPtr<PresShell> presShell = GetPresShell();
+  gfxTextPerfMetrics* tp = nullptr;
+  FontMatchingStats* fontStats = nullptr;
+  if (presShell && !presShell->IsDestroying()) {
+    nsPresContext* pc = presShell->GetPresContext();
+    tp = pc->GetTextPerfMetrics();
+    fontStats = pc->GetFontMatchingStats();
+  }
+
+  // If we have a cached fontGroup, check that it is valid for the current
+  // prescontext; if not, we need to discard and re-create it.
+  RefPtr<gfxFontGroup>& fontGroup = CurrentState().fontGroup;
+  if (fontGroup) {
+    if (fontGroup->GetFontMatchingStats() != fontStats ||
+        fontGroup->GetTextPerfMetrics() != tp) {
+      fontGroup = nullptr;
+    }
+  }
+
+  if (!fontGroup) {
     ErrorResult err;
     NS_NAMED_LITERAL_STRING(kDefaultFontStyle, "10px sans-serif");
     static float kDefaultFontSize = 10.0;
-    RefPtr<PresShell> presShell = GetPresShell();
-    bool fontUpdated = SetFontInternal(kDefaultFontStyle, err);
+    // If the font has already been set, we're re-creating the fontGroup
+    // and should re-use the existing font attribute; if not, we initialize
+    // it to the canvas default.
+    const nsString& currentFont = CurrentState().font;
+    bool fontUpdated = SetFontInternal(
+        currentFont.IsEmpty() ? kDefaultFontStyle : currentFont, err);
     if (err.Failed() || !fontUpdated) {
       err.SuppressException();
       gfxFontStyle style;
       style.size = kDefaultFontSize;
-      gfxTextPerfMetrics* tp = nullptr;
-      FontMatchingStats* fontStats = nullptr;
-      if (presShell && !presShell->IsDestroying()) {
-        tp = presShell->GetPresContext()->GetTextPerfMetrics();
-        fontStats = presShell->GetPresContext()->GetFontMatchingStats();
-      }
       int32_t perDevPixel, perCSSPixel;
       GetAppUnitsValues(&perDevPixel, &perCSSPixel);
       gfxFloat devToCssSize = gfxFloat(perDevPixel) / gfxFloat(perCSSPixel);
-      CurrentState().fontGroup = gfxPlatform::GetPlatform()->CreateFontGroup(
+      fontGroup = gfxPlatform::GetPlatform()->CreateFontGroup(
           FontFamilyList(StyleGenericFontFamily::SansSerif), &style, tp,
           fontStats, nullptr, devToCssSize);
-      if (CurrentState().fontGroup) {
+      if (fontGroup) {
         CurrentState().font = kDefaultFontStyle;
       } else {
         NS_ERROR("Default canvas font is invalid");
       }
     }
   } else {
     // The fontgroup needs to check if its cached families/faces are valid.
-    CurrentState().fontGroup->CheckForUpdatedPlatformList();
-  }
-
-  return CurrentState().fontGroup;
+    fontGroup->CheckForUpdatedPlatformList();
+  }
+
+  return fontGroup;
 }
 
 //
 // line caps/joins
 //
 
 void CanvasRenderingContext2D::SetLineCap(const nsAString& aLinecapStyle) {
   CapStyle cap;
diff --git a/gfx/src/nsDeviceContext.cpp b/gfx/src/nsDeviceContext.cpp
--- a/gfx/src/nsDeviceContext.cpp
+++ b/gfx/src/nsDeviceContext.cpp
@@ -144,17 +144,18 @@ already_AddRefed<nsFontMetrics> nsFontCa
   // First check our cache
   // start from the end, which is where we put the most-recent-used element
   const int32_t n = mFontMetrics.Length() - 1;
   for (int32_t i = n; i >= 0; --i) {
     nsFontMetrics* fm = mFontMetrics[i];
     if (fm->Font().Equals(aFont) &&
         fm->GetUserFontSet() == aParams.userFontSet &&
         fm->Language() == language &&
-        fm->Orientation() == aParams.orientation) {
+        fm->Orientation() == aParams.orientation &&
+        fm->GetThebesFontGroup()->GetFontMatchingStats() == aParams.fontStats) {
       if (i != n) {
         // promote it to the end of the cache
         mFontMetrics.RemoveElementAt(i);
         mFontMetrics.AppendElement(fm);
       }
       fm->GetThebesFontGroup()->UpdateUserFonts();
       return do_AddRef(fm);
     }
diff --git a/gfx/thebes/gfxTextRun.h b/gfx/thebes/gfxTextRun.h
--- a/gfx/thebes/gfxTextRun.h
+++ b/gfx/thebes/gfxTextRun.h
@@ -1039,17 +1039,19 @@ class gfxFontGroup final : public gfxTex
   // in the text run word cache.  For font groups based on stylesheets with no
   // @font-face rule, this always returns 0.
   uint64_t GetGeneration();
 
   // generation of the latest fontset rebuild, 0 when no fontset present
   uint64_t GetRebuildGeneration();
 
   // used when logging text performance
-  gfxTextPerfMetrics* GetTextPerfMetrics() { return mTextPerf; }
+  gfxTextPerfMetrics* GetTextPerfMetrics() const { return mTextPerf; }
+
+  FontMatchingStats* GetFontMatchingStats() const { return mFontMatchingStats; }
 
   // This will call UpdateUserFonts() if the user font set is changed.
   void SetUserFontSet(gfxUserFontSet* aUserFontSet);
 
   void ClearCachedData() {
     mUnderlineOffset = UNDERLINE_OFFSET_NOT_SET;
     mSkipDrawing = false;
     mHyphenWidth = -1;
