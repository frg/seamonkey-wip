# HG changeset patch
# User Tooru Fujisawa <arai_a@mac.com>
# Date 1531916132 -32400
# Node ID 6edce1e74d5aeb53247e9c22d4e3b5fce3810d8b
# Parent  7ef7a9541ba809cbb6d7ba378fd4ba2351b535cc
Bug 1463979 - Part 1: Store a pointer to enclosing LazyScript into LazyScript. r=jimb,sfink

diff --git a/js/src/frontend/BytecodeCompiler.cpp b/js/src/frontend/BytecodeCompiler.cpp
--- a/js/src/frontend/BytecodeCompiler.cpp
+++ b/js/src/frontend/BytecodeCompiler.cpp
@@ -715,18 +715,20 @@ class MOZ_STACK_CLASS AutoAssertFunction
 #endif
     }
 };
 
 bool
 frontend::CompileLazyFunction(JSContext* cx, Handle<LazyScript*> lazy, const char16_t* chars, size_t length)
 {
     MOZ_ASSERT(cx->compartment() == lazy->functionNonDelazifying()->compartment());
-    // We can't be running this script unless we've run its parent.
-    MOZ_ASSERT(!lazy->isEnclosingScriptLazy());
+    // We can only compile functions whose parents have previously been
+    // compiled, because compilation requires full information about the
+    // function's immediately enclosing scope.
+    MOZ_ASSERT(lazy->enclosingScriptHasEverBeenCompiled());
 
     AutoAssertReportedException assertException(cx);
     Rooted<JSFunction*> fun(cx, lazy->functionNonDelazifying());
     AutoAssertFunctionDelazificationCompletion delazificationCompletion(cx, fun);
 
     CompileOptions options(cx);
     options.setMutedErrors(lazy->mutedErrors())
            .setFileAndLine(lazy->filename(), lazy->lineno())
diff --git a/js/src/frontend/BytecodeEmitter.cpp b/js/src/frontend/BytecodeEmitter.cpp
--- a/js/src/frontend/BytecodeEmitter.cpp
+++ b/js/src/frontend/BytecodeEmitter.cpp
@@ -5647,24 +5647,17 @@ BytecodeEmitter::emitFunction(ParseNode*
     // make a deep clone of its contents.
     if (fun->isInterpreted()) {
         bool singleton = checkRunOnceContext();
         if (!JSFunction::setTypeForScriptedFunction(cx, fun, singleton))
             return false;
 
         SharedContext* outersc = sc;
         if (fun->isInterpretedLazy()) {
-            // We need to update the static scope chain regardless of whether
-            // the LazyScript has already been initialized, due to the case
-            // where we previously successfully compiled an inner function's
-            // lazy script but failed to compile the outer script after the
-            // fact. If we attempt to compile the outer script again, the
-            // static scope chain will be newly allocated and will mismatch
-            // the previously compiled LazyScript's.
-            fun->lazyScript()->setEnclosingScope(innermostScope());
+            funbox->setEnclosingScopeForInnerLazyFunction(innermostScope());
             if (emittingRunOnceLambda)
                 fun->lazyScript()->setTreatAsRunOnce();
         } else {
             MOZ_ASSERT_IF(outersc->strict(), funbox->strictScript);
 
             // Inherit most things (principals, version, etc) from the
             // parent.  Use default values for the rest.
             Rooted<JSScript*> parent(cx, script);
@@ -9135,16 +9128,18 @@ CGObjectList::finish(ObjectArray* array)
     MOZ_ASSERT(length == array->length);
 
     js::GCPtrObject* cursor = array->vector + array->length;
     ObjectBox* objbox = lastbox;
     do {
         --cursor;
         MOZ_ASSERT(!*cursor);
         MOZ_ASSERT(objbox->object->isTenured());
+        if (objbox->isFunctionBox())
+            objbox->asFunctionBox()->finish();
         *cursor = objbox->object;
     } while ((objbox = objbox->emitLink) != nullptr);
     MOZ_ASSERT(cursor == array->vector);
 }
 
 void
 CGScopeList::finish(ScopeArray* array)
 {
diff --git a/js/src/frontend/Parser.cpp b/js/src/frontend/Parser.cpp
--- a/js/src/frontend/Parser.cpp
+++ b/js/src/frontend/Parser.cpp
@@ -507,17 +507,20 @@ FunctionBox::FunctionBox(JSContext* cx, 
 void
 FunctionBox::initFromLazyFunction()
 {
     JSFunction* fun = function();
     if (fun->lazyScript()->isDerivedClassConstructor())
         setDerivedClassConstructor();
     if (fun->lazyScript()->needsHomeObject())
         setNeedsHomeObject();
-    enclosingScope_ = fun->lazyScript()->enclosingScope();
+    if (fun->lazyScript()->hasEnclosingScope())
+        enclosingScope_ = fun->lazyScript()->enclosingScope();
+    else
+        enclosingScope_ = nullptr;
     initWithEnclosingScope(enclosingScope_);
 }
 
 void
 FunctionBox::initStandaloneFunction(Scope* enclosingScope)
 {
     // Standalone functions are Function or Generator constructors and are
     // always scoped to the global.
@@ -589,16 +592,37 @@ FunctionBox::initWithEnclosingScope(Scop
     } else {
         computeAllowSyntax(enclosingScope);
         computeThisBinding(enclosingScope);
     }
 
     computeInWith(enclosingScope);
 }
 
+void
+FunctionBox::setEnclosingScopeForInnerLazyFunction(Scope* enclosingScope)
+{
+    MOZ_ASSERT(isLazyFunctionWithoutEnclosingScope());
+
+    // For lazy functions inside a function which is being compiled, we cache
+    // the incomplete scope object while compiling, and store it to the
+    // LazyScript once the enclosing script successfully finishes compilation
+    // in FunctionBox::finish.
+    enclosingScope_ = enclosingScope;
+}
+
+void
+FunctionBox::finish()
+{
+    if (!isLazyFunctionWithoutEnclosingScope())
+        return;
+    MOZ_ASSERT(enclosingScope_);
+    function()->lazyScript()->setEnclosingScope(enclosingScope_);
+}
+
 template <class ParseHandler, typename CharT>
 inline typename GeneralParser<ParseHandler, CharT>::FinalParser*
 GeneralParser<ParseHandler, CharT>::asFinalParser()
 {
     static_assert(mozilla::IsBaseOf<GeneralParser<ParseHandler, CharT>, FinalParser>::value,
                   "inheritance relationship required by the static_cast<> below");
 
     return static_cast<FinalParser*>(this);
diff --git a/js/src/frontend/SharedContext.h b/js/src/frontend/SharedContext.h
--- a/js/src/frontend/SharedContext.h
+++ b/js/src/frontend/SharedContext.h
@@ -292,16 +292,26 @@ SharedContext::asEvalContext()
     return static_cast<EvalSharedContext*>(this);
 }
 
 class FunctionBox : public ObjectBox, public SharedContext
 {
     // The parser handles tracing the fields below via the ObjectBox linked
     // list.
 
+    // This field is used for two purposes:
+    //   * If this FunctionBox refers to the function being compiled, this field
+    //     holds its enclosing scope, used for compilation.
+    //   * If this FunctionBox refers to a lazy child of the function being
+    //     compiled, this field holds the child's immediately enclosing scope.
+    //     Once compilation succeeds, we will store it in the child's
+    //     LazyScript.  (Debugger may become confused if LazyScripts refer to
+    //     partially initialized enclosing scopes, so we must avoid storing the
+    //     scope in the LazyScript until compilation has completed
+    //     successfully.)
     Scope* enclosingScope_;
 
     // Names from the named lambda scope, if a named lambda.
     LexicalScope::Data* namedLambdaBindings_;
 
     // Names from the function scope.
     FunctionScope::Data* functionScopeBindings_;
 
@@ -413,24 +423,44 @@ class FunctionBox : public ObjectBox, pu
         MOZ_ASSERT(context->zone()->hasKeptAtoms());
         return MutableHandle<VarScope::Data*>::fromMarkedLocation(&extraVarScopeBindings_);
     }
 
     void initFromLazyFunction();
     void initStandaloneFunction(Scope* enclosingScope);
     void initWithEnclosingParseContext(ParseContext* enclosing, FunctionSyntaxKind kind);
 
+    inline bool isLazyFunctionWithoutEnclosingScope() const {
+        return function()->isInterpretedLazy() &&
+               !function()->lazyScript()->hasEnclosingScope();
+    }
+    void setEnclosingScopeForInnerLazyFunction(Scope* enclosingScope);
+    void finish();
+
     JSFunction* function() const { return &object->as<JSFunction>(); }
 
     Scope* compilationEnclosingScope() const override {
         // This method is used to distinguish the outermost SharedContext. If
         // a FunctionBox is the outermost SharedContext, it must be a lazy
         // function.
-        MOZ_ASSERT_IF(function()->isInterpretedLazy(),
+
+        // If the function is lazy and it has enclosing scope, the function is
+        // being delazified.  In that case the enclosingScope_ field is copied
+        // from the lazy function at the beginning of delazification and should
+        // keep pointing the same scope.
+        MOZ_ASSERT_IF(function()->isInterpretedLazy() &&
+                      function()->lazyScript()->hasEnclosingScope(),
                       enclosingScope_ == function()->lazyScript()->enclosingScope());
+
+        // If this FunctionBox is a lazy child of the function we're actually
+        // compiling, then it is not the outermost SharedContext, so this
+        // method should return nullptr."
+        if (isLazyFunctionWithoutEnclosingScope())
+            return nullptr;
+
         return enclosingScope_;
     }
 
     bool needsCallObjectRegardlessOfBindings() const {
         return hasExtensibleScope() ||
                needsHomeObject() ||
                isDerivedClassConstructor() ||
                isGenerator() ||
diff --git a/js/src/gc/Marking.cpp b/js/src/gc/Marking.cpp
--- a/js/src/gc/Marking.cpp
+++ b/js/src/gc/Marking.cpp
@@ -934,18 +934,20 @@ LazyScript::traceChildren(JSTracer* trc)
         TraceWeakEdge(trc, &script_, "script");
 
     if (function_)
         TraceEdge(trc, &function_, "function");
 
     if (sourceObject_)
         TraceEdge(trc, &sourceObject_, "sourceObject");
 
-    if (enclosingScope_)
-        TraceEdge(trc, &enclosingScope_, "enclosingScope");
+    if (enclosingLazyScriptOrScope_) {
+        TraceGenericPointerRoot(trc, reinterpret_cast<Cell**>(enclosingLazyScriptOrScope_.unsafeUnbarrieredForTracing()),
+                                "enclosingScope or enclosingLazyScript");
+    }
 
     // We rely on the fact that atoms are always tenured.
     JSAtom** closedOverBindings = this->closedOverBindings();
     for (auto i : IntegerRange(numClosedOverBindings())) {
         if (closedOverBindings[i])
             TraceManuallyBarrieredEdge(trc, &closedOverBindings[i], "closedOverBinding");
     }
 
@@ -960,18 +962,20 @@ js::GCMarker::eagerlyMarkChildren(LazySc
         noteWeakEdge(thing->script_.unsafeUnbarrieredForTracing());
 
     if (thing->function_)
         traverseEdge(thing, static_cast<JSObject*>(thing->function_));
 
     if (thing->sourceObject_)
         traverseEdge(thing, static_cast<JSObject*>(thing->sourceObject_));
 
-    if (thing->enclosingScope_)
-        traverseEdge(thing, static_cast<Scope*>(thing->enclosingScope_));
+    if (thing->enclosingLazyScriptOrScope_) {
+        TraceManuallyBarrieredGenericPointerEdge(this, reinterpret_cast<Cell**>(thing->enclosingLazyScriptOrScope_.unsafeUnbarrieredForTracing()),
+                                                 "enclosingScope or enclosingLazyScript");
+    }
 
     // We rely on the fact that atoms are always tenured.
     JSAtom** closedOverBindings = thing->closedOverBindings();
     for (auto i : IntegerRange(thing->numClosedOverBindings())) {
         if (closedOverBindings[i])
             traverseEdge(thing, static_cast<JSString*>(closedOverBindings[i]));
     }
 
diff --git a/js/src/vm/Debugger.cpp b/js/src/vm/Debugger.cpp
--- a/js/src/vm/Debugger.cpp
+++ b/js/src/vm/Debugger.cpp
@@ -4568,20 +4568,21 @@ class MOZ_STACK_CLASS Debugger::ScriptQu
     }
 
     /*
      * If |script| matches this query, append it to |vector| or place it in
      * |innermostForRealm|, as appropriate. Set |oom| if an out of memory
      * condition occurred.
      */
     void consider(JSScript* script, const JS::AutoRequireNoGC& nogc) {
-        // We check for presence of script->code() because it is possible that
-        // the script was created and thus exposed to GC, but *not* fully
-        // initialized from fullyInit{FromEmitter,Trivial} due to errors.
-        if (oom || script->selfHosted() || !script->code())
+        // We check for presence of script->isUncompleted() because it is
+        // possible that the script was created and thus exposed to GC, but
+        // *not* fully initialized from fullyInit{FromEmitter,Trivial} due to
+        // errors.
+        if (oom || script->selfHosted() || script->isUncompleted())
             return;
         Realm* realm = script->realm();
         if (!realms.has(realm))
             return;
         if (urlCString.ptr()) {
             bool gotFilename = false;
             if (script->filename() && strcmp(script->filename(), urlCString.ptr()) == 0)
                 gotFilename = true;
diff --git a/js/src/vm/JSFunction.cpp b/js/src/vm/JSFunction.cpp
--- a/js/src/vm/JSFunction.cpp
+++ b/js/src/vm/JSFunction.cpp
@@ -1586,35 +1586,43 @@ JSFunction::createScriptForLazilyInterpr
         // chain of their inner functions, or in the case of eval, possibly
         // eval'd inner functions. This prohibits re-lazification as
         // StaticScopeIter queries needsCallObject of those functions, which
         // requires a non-lazy script.  Note that if this ever changes,
         // XDRRelazificationInfo will have to be fixed.
         bool canRelazify = !lazy->numInnerFunctions() && !lazy->hasDirectEval();
 
         if (script) {
+            // This function is non-canonical function, and the canonical
+            // function is already delazified.
             fun->setUnlazifiedScript(script);
             // Remember the lazy script on the compiled script, so it can be
             // stored on the function again in case of re-lazification.
             if (canRelazify)
                 script->setLazyScript(lazy);
             return true;
         }
 
         if (fun != lazy->functionNonDelazifying()) {
+            // This function is non-canonical function, and the canonical
+            // function is lazy.
+            // Delazify the canonical function, which will result in calling
+            // this function again with the canonical function.
             if (!LazyScript::functionDelazifying(cx, lazy))
                 return false;
             script = lazy->functionNonDelazifying()->nonLazyScript();
             if (!script)
                 return false;
 
             fun->setUnlazifiedScript(script);
             return true;
         }
 
+        // This is lazy canonical-function.
+
         MOZ_ASSERT(lazy->scriptSource()->hasSourceData());
 
         // Parse and compile the script from source.
         size_t lazyLength = lazy->sourceEnd() - lazy->sourceStart();
         UncompressedSourceCache::AutoHoldEntry holder;
         ScriptSource::PinnedChars chars(cx, lazy->scriptSource(), holder,
                                         lazy->sourceStart(), lazyLength);
         if (!chars.get())
@@ -2123,17 +2131,17 @@ js::CanReuseScriptForClone(JS::Realm* re
     // example.
     if (IsSyntacticEnvironment(newParent))
         return true;
 
     // We need to clone the script if we're not already marked as having a
     // non-syntactic scope.
     return fun->hasScript()
         ? fun->nonLazyScript()->hasNonSyntacticScope()
-        : fun->lazyScript()->enclosingScope()->hasOnChain(ScopeKind::NonSyntactic);
+        : fun->lazyScript()->hasNonSyntacticScope();
 }
 
 static inline JSFunction*
 NewFunctionClone(JSContext* cx, HandleFunction fun, NewObjectKind newKind,
                  gc::AllocKind allocKind, HandleObject proto)
 {
     RootedObject cloneProto(cx, proto);
     if (!proto && (fun->isGenerator() || fun->isAsync())) {
diff --git a/js/src/vm/JSFunction.h b/js/src/vm/JSFunction.h
--- a/js/src/vm/JSFunction.h
+++ b/js/src/vm/JSFunction.h
@@ -559,16 +559,19 @@ class JSFunction : public js::NativeObje
             initScript(script);
         }
         return nonLazyScript();
     }
 
     // The state of a JSFunction whose script errored out during bytecode
     // compilation. Such JSFunctions are only reachable via GC iteration and
     // not from script.
+    // If u.scripted.s.script_ is non-null, the pointed JSScript is guaranteed
+    // to be complete (see the comment above JSScript::initFromFunctionBox
+    // callsite in JSScript::fullyInitFromEmitter).
     bool hasUncompletedScript() const {
         MOZ_ASSERT(hasScript());
         return !u.scripted.s.script_;
     }
 
     JSScript* nonLazyScript() const {
         MOZ_ASSERT(!hasUncompletedScript());
         return u.scripted.s.script_;
diff --git a/js/src/vm/JSScript.cpp b/js/src/vm/JSScript.cpp
--- a/js/src/vm/JSScript.cpp
+++ b/js/src/vm/JSScript.cpp
@@ -268,19 +268,19 @@ XDRRelazificationInfo(XDRState<mode>* xd
             // JSFunction::createScriptForLazilyInterpretedFunction.
             MOZ_ASSERT(lazy->numInnerFunctions() == 0);
         }
 
         MOZ_TRY(xdr->codeUint64(&packedFields));
 
         if (mode == XDR_DECODE) {
             RootedScriptSourceObject sourceObject(cx, &script->scriptSourceUnwrap());
-            lazy.set(LazyScript::Create(cx, fun, script, enclosingScope, sourceObject,
-                                        packedFields, sourceStart, sourceEnd, toStringStart,
-                                        lineno, column));
+            lazy.set(LazyScript::CreateForXDR(cx, fun, script, enclosingScope, sourceObject,
+                                              packedFields, sourceStart, sourceEnd, toStringStart,
+                                              lineno, column));
             if (!lazy)
                 return xdr->fail(JS::TranscodeResult_Throw);
 
             lazy->setToStringEnd(toStringEnd);
 
             // As opposed to XDRLazyScript, we need to restore the runtime bits
             // of the script, as we are trying to match the fact this function
             // has already been parsed and that it would need to be re-lazified.
@@ -946,19 +946,19 @@ js::XDRLazyScript(XDRState<mode>* xdr, H
         MOZ_TRY(xdr->codeUint32(&sourceEnd));
         MOZ_TRY(xdr->codeUint32(&toStringStart));
         MOZ_TRY(xdr->codeUint32(&toStringEnd));
         MOZ_TRY(xdr->codeUint32(&lineno));
         MOZ_TRY(xdr->codeUint32(&column));
         MOZ_TRY(xdr->codeUint64(&packedFields));
 
         if (mode == XDR_DECODE) {
-            lazy.set(LazyScript::Create(cx, fun, nullptr, enclosingScope, sourceObject,
-                                        packedFields, sourceStart, sourceEnd, toStringStart,
-                                        lineno, column));
+            lazy.set(LazyScript::CreateForXDR(cx, fun, nullptr, enclosingScope, sourceObject,
+                                              packedFields, sourceStart, sourceEnd, toStringStart,
+                                              lineno, column));
             if (!lazy)
                 return xdr->fail(JS::TranscodeResult_Throw);
             lazy->setToStringEnd(toStringEnd);
             fun->initLazyScript(lazy);
         }
     }
 
     // Code closed-over bindings.
@@ -970,18 +970,21 @@ js::XDRLazyScript(XDRState<mode>* xdr, H
         GCPtrFunction* innerFunctions = lazy->innerFunctions();
         size_t numInnerFunctions = lazy->numInnerFunctions();
         for (size_t i = 0; i < numInnerFunctions; i++) {
             if (mode == XDR_ENCODE)
                 func = innerFunctions[i];
 
             MOZ_TRY(XDRInterpretedFunction(xdr, nullptr, sourceObject, &func));
 
-            if (mode == XDR_DECODE)
+            if (mode == XDR_DECODE) {
                 innerFunctions[i] = func;
+                if (innerFunctions[i]->isInterpretedLazy())
+                    innerFunctions[i]->lazyScript()->setEnclosingLazyScript(lazy);
+            }
         }
     }
 
     return Ok();
 }
 
 template XDRResult
 js::XDRLazyScript(XDRState<XDR_ENCODE>*, HandleScope, HandleScriptSourceObject,
@@ -2947,16 +2950,22 @@ JSScript::initFromModuleContext(HandleSc
 
 /* static */ bool
 JSScript::fullyInitFromEmitter(JSContext* cx, HandleScript script, BytecodeEmitter* bce)
 {
     /* The counts of indexed things must be checked during code generation. */
     MOZ_ASSERT(bce->atomIndices->count() <= INDEX_LIMIT);
     MOZ_ASSERT(bce->objectList.length <= INDEX_LIMIT);
 
+    uint64_t nslots = bce->maxFixedSlots + static_cast<uint64_t>(bce->maxStackDepth);
+    if (nslots > UINT32_MAX) {
+        bce->reportError(nullptr, JSMSG_NEED_DIET, js_script_str);
+        return false;
+    }
+
     uint32_t mainLength = bce->offset();
     uint32_t prologueLength = bce->prologueOffset();
     uint32_t nsrcnotes;
     if (!bce->finishTakingSrcNotes(&nsrcnotes))
         return false;
     uint32_t natoms = bce->atomIndices->count();
     if (!partiallyInit(cx, script,
                        bce->scopeList.length(), bce->constList.length(), bce->objectList.length,
@@ -2969,16 +2978,21 @@ JSScript::fullyInitFromEmitter(JSContext
     MOZ_ASSERT(script->mainOffset() == 0);
     script->mainOffset_ = prologueLength;
 
     script->lineno_ = bce->firstLine;
 
     if (!script->createScriptData(cx, prologueLength + mainLength, nsrcnotes, natoms))
         return false;
 
+    // Any fallible operation after JSScript::createScriptData should reset
+    // JSScript.scriptData_, in order to treat this script as uncompleted,
+    // in JSScript::isUncompleted.
+    // JSScript::shareScriptData resets it before returning false.
+
     jsbytecode* code = script->code();
     PodCopy<jsbytecode>(code, bce->prologue.code.begin(), prologueLength);
     PodCopy<jsbytecode>(code + prologueLength, bce->main.code.begin(), mainLength);
     bce->copySrcNotes((jssrcnote*)(code + script->length()), nsrcnotes);
     InitAtomMap(*bce->atomIndices, script->atoms());
 
     if (!script->shareScriptData(cx))
         return false;
@@ -2993,28 +3007,25 @@ JSScript::fullyInitFromEmitter(JSContext
         bce->tryNoteList.finish(script->trynotes());
     if (bce->scopeNoteList.length() != 0)
         bce->scopeNoteList.finish(script->scopeNotes(), prologueLength);
     script->bitFields_.strict_ = bce->sc->strict();
     script->bitFields_.explicitUseStrict_ = bce->sc->hasExplicitUseStrict();
     script->bitFields_.bindingsAccessedDynamically_ = bce->sc->bindingsAccessedDynamically();
     script->bitFields_.hasSingletons_ = bce->hasSingletons;
 
-    uint64_t nslots = bce->maxFixedSlots + static_cast<uint64_t>(bce->maxStackDepth);
-    if (nslots > UINT32_MAX) {
-        bce->reportError(nullptr, JSMSG_NEED_DIET, js_script_str);
-        return false;
-    }
-
     script->nfixed_ = bce->maxFixedSlots;
     script->nslots_ = nslots;
     script->bodyScopeIndex_ = bce->bodyScopeIndex;
     script->bitFields_.hasNonSyntacticScope_ =
         bce->outermostScope()->hasOnChain(ScopeKind::NonSyntactic);
 
+    // There shouldn't be any fallible operation after initFromFunctionBox,
+    // JSFunction::hasUncompletedScript relies on the fact that the existence
+    // of the pointer to JSScript means the pointed JSScript is complete.
     if (bce->sc->isFunctionBox())
         initFromFunctionBox(script, bce->sc->asFunctionBox());
     else if (bce->sc->isModuleContext())
         initFromModuleContext(script);
 
     // Copy yield offsets last, as the generator kind is set in
     // initFromFunctionBox.
     if (bce->yieldAndAwaitOffsetList.length() != 0)
@@ -4196,17 +4207,16 @@ JSScript::formalLivesInArgumentsObject(u
 }
 
 LazyScript::LazyScript(JSFunction* fun, ScriptSourceObject& sourceObject,
                        void* table, uint64_t packedFields,
                        uint32_t sourceStart, uint32_t sourceEnd,
                        uint32_t toStringStart, uint32_t lineno, uint32_t column)
   : script_(nullptr),
     function_(fun),
-    enclosingScope_(nullptr),
     sourceObject_(&sourceObject),
     table_(table),
     packedFields_(packedFields),
     sourceStart_(sourceStart),
     sourceEnd_(sourceEnd),
     toStringStart_(toStringStart),
     toStringEnd_(sourceEnd),
     lineno_(lineno),
@@ -4223,21 +4233,36 @@ void
 LazyScript::initScript(JSScript* script)
 {
     MOZ_ASSERT(script);
     MOZ_ASSERT(!script_.unbarrieredGet());
     script_.set(script);
 }
 
 void
+LazyScript::setEnclosingLazyScript(LazyScript* enclosingLazyScript)
+{
+    MOZ_ASSERT(enclosingLazyScript);
+
+    // We never change an existing LazyScript.
+    MOZ_ASSERT(!hasEnclosingLazyScript());
+
+    // Enclosing scopes never transition back to enclosing lazy scripts.
+    MOZ_ASSERT(!hasEnclosingScope());
+
+    enclosingLazyScriptOrScope_ = enclosingLazyScript;
+}
+
+void
 LazyScript::setEnclosingScope(Scope* enclosingScope)
 {
-    // This method may be called to update the enclosing scope. See comment
-    // above the callsite in BytecodeEmitter::emitFunction.
-    enclosingScope_ = enclosingScope;
+    MOZ_ASSERT(enclosingScope);
+    MOZ_ASSERT(!hasEnclosingScope());
+
+    enclosingLazyScriptOrScope_ = enclosingScope;
 }
 
 ScriptSourceObject&
 LazyScript::sourceObject() const
 {
     return sourceObject_->as<ScriptSourceObject>();
 }
 
@@ -4324,28 +4349,31 @@ LazyScript::Create(JSContext* cx, Handle
     if (!res)
         return nullptr;
 
     JSAtom** resClosedOverBindings = res->closedOverBindings();
     for (size_t i = 0; i < res->numClosedOverBindings(); i++)
         resClosedOverBindings[i] = closedOverBindings[i];
 
     GCPtrFunction* resInnerFunctions = res->innerFunctions();
-    for (size_t i = 0; i < res->numInnerFunctions(); i++)
+    for (size_t i = 0; i < res->numInnerFunctions(); i++) {
         resInnerFunctions[i].init(innerFunctions[i]);
+        if (resInnerFunctions[i]->isInterpretedLazy())
+            resInnerFunctions[i]->lazyScript()->setEnclosingLazyScript(res);
+    }
 
     return res;
 }
 
 /* static */ LazyScript*
-LazyScript::Create(JSContext* cx, HandleFunction fun,
-                   HandleScript script, HandleScope enclosingScope,
-                   HandleScriptSourceObject sourceObject,
-                   uint64_t packedFields, uint32_t sourceStart, uint32_t sourceEnd,
-                   uint32_t toStringStart, uint32_t lineno, uint32_t column)
+LazyScript::CreateForXDR(JSContext* cx, HandleFunction fun,
+                         HandleScript script, HandleScope enclosingScope,
+                         HandleScriptSourceObject sourceObject,
+                         uint64_t packedFields, uint32_t sourceStart, uint32_t sourceEnd,
+                         uint32_t toStringStart, uint32_t lineno, uint32_t column)
 {
     // Dummy atom which is not a valid property name.
     RootedAtom dummyAtom(cx, cx->names().comma);
 
     // Dummy function which is not a valid function as this is the one which is
     // holding this lazy script.
     HandleFunction dummyFun = fun;
 
@@ -4362,20 +4390,21 @@ LazyScript::Create(JSContext* cx, Handle
     for (i = 0, num = res->numClosedOverBindings(); i < num; i++)
         closedOverBindings[i] = dummyAtom;
 
     GCPtrFunction* functions = res->innerFunctions();
     for (i = 0, num = res->numInnerFunctions(); i < num; i++)
         functions[i].init(dummyFun);
 
     // Set the enclosing scope of the lazy function. This value should only be
-    // non-null if we have a non-lazy enclosing script.
-    // LazyScript::isEnclosingScriptLazy relies on the enclosing scope being
-    // null if we're nested inside another lazy function.
-    MOZ_ASSERT(!res->enclosingScope());
+    // set if we have a non-lazy enclosing script at this point.
+    // LazyScript::enclosingScriptHasEverBeenCompiled relies on the enclosing
+    // scope being non-null if we have ever been nested inside non-lazy
+    // function.
+    MOZ_ASSERT(!res->hasEnclosingScope());
     if (enclosingScope)
         res->setEnclosingScope(enclosingScope);
 
     MOZ_ASSERT(!res->hasScript());
     if (script)
         res->initScript(script);
 
     return res;
@@ -4389,34 +4418,16 @@ LazyScript::initRuntimeFields(uint64_t p
         uint64_t packed;
     };
 
     packed = packedFields;
     p_.hasBeenCloned = p.hasBeenCloned;
     p_.treatAsRunOnce = p.treatAsRunOnce;
 }
 
-bool
-LazyScript::hasUncompletedEnclosingScript() const
-{
-    // It can happen that we created lazy scripts while compiling an enclosing
-    // script, but we errored out while compiling that script. When we iterate
-    // over lazy script in a compartment, we might see lazy scripts that never
-    // escaped to script and should be ignored.
-    //
-    // If the enclosing scope is a function with a null script or has a script
-    // without code, it was not successfully compiled.
-
-    if (!enclosingScope() || !enclosingScope()->is<FunctionScope>())
-        return false;
-
-    JSFunction* fun = enclosingScope()->as<FunctionScope>().canonicalFunction();
-    return !fun->hasScript() || fun->hasUncompletedScript() || !fun->nonLazyScript()->code();
-}
-
 void
 JSScript::updateJitCodeRaw(JSRuntime* rt)
 {
     MOZ_ASSERT(rt);
     if (hasBaselineScript() && baseline->hasPendingIonBuilder()) {
         MOZ_ASSERT(!isIonCompilingOffThread());
         jitCodeRaw_ = rt->jitRuntime()->lazyLinkStub().value;
         jitCodeSkipArgCheck_ = jitCodeRaw_;
diff --git a/js/src/vm/JSScript.h b/js/src/vm/JSScript.h
--- a/js/src/vm/JSScript.h
+++ b/js/src/vm/JSScript.h
@@ -1260,16 +1260,22 @@ class JSScript : public js::gc::TenuredC
     }
 
     // Script bytecode is immutable after creation.
     jsbytecode* code() const {
         if (!scriptData_)
             return nullptr;
         return scriptData_->code();
     }
+    bool isUncompleted() const {
+        // code() becomes non-null only if this script is complete.
+        // See the comment in JSScript::fullyInitFromEmitter.
+        return !code();
+    }
+
     size_t length() const {
         MOZ_ASSERT(scriptData_);
         return scriptData_->codeLength();
     }
 
     jsbytecode* codeEnd() const { return code() + length(); }
 
     jsbytecode* lastPC() const {
@@ -2139,18 +2145,92 @@ class LazyScript : public gc::TenuredCel
     // If non-nullptr, the script has been compiled and this is a forwarding
     // pointer to the result. This is a weak pointer: after relazification, we
     // can collect the script if there are no other pointers to it.
     WeakRef<JSScript*> script_;
 
     // Original function with which the lazy script is associated.
     GCPtrFunction function_;
 
-    // Scope in which the script is nested.
-    GCPtrScope enclosingScope_;
+    // This field holds one of:
+    //   * LazyScript in which the script is nested.  This case happens if the
+    //     enclosing script is lazily parsed and have never been compiled.
+    //
+    //     This is used by the debugger to delazify the enclosing scripts
+    //     recursively.  The all ancestor LazyScripts in this linked-list are
+    //     kept alive as long as this LazyScript is alive, which doesn't result
+    //     in keeping them unnecessarily alive outside of the debugger for the
+    //     following reasons:
+    //
+    //       * Outside of the debugger, a LazyScript is visible to user (which
+    //         means the LazyScript can be pointed from somewhere else than the
+    //         enclosing script) only if the enclosing script is compiled and
+    //         executed.  While compiling the enclosing script, this field is
+    //         changed to point the enclosing scope.  So the enclosing
+    //         LazyScript is no more in the list.
+    //       * Before the enclosing script gets compiled, this LazyScript is
+    //         kept alive only if the outermost LazyScript in the list is kept
+    //         alive.
+    //       * Once this field is changed to point the enclosing scope, this
+    //         field will never point the enclosing LazyScript again, since
+    //         relazification is not performed on non-leaf scripts.
+    //
+    //   * Scope in which the script is nested.  This case happens if the
+    //     enclosing script has ever been compiled.
+    //
+    //   * nullptr for incomplete (initial or failure) state
+    //
+    // This field should be accessed via accessors:
+    //   * enclosingScope
+    //   * setEnclosingScope (cannot be called twice)
+    //   * enclosingLazyScript
+    //   * setEnclosingLazyScript (cannot be called twice)
+    // after checking:
+    //   * hasEnclosingLazyScript
+    //   * hasEnclosingScope
+    //
+    // The transition of fields are following:
+    //
+    //  o                               o
+    //  | when function is lazily       | when decoded from XDR,
+    //  | parsed inside a function      | and enclosing script is lazy
+    //  | which is lazily parsed        | (CreateForXDR without enclosingScope)
+    //  | (Create)                      |
+    //  v                               v
+    // +---------+                     +---------+
+    // | nullptr |                     | nullptr |
+    // +---------+                     +---------+
+    //  |                               |
+    //  | when enclosing function is    | when enclosing script is decoded
+    //  | lazily parsed and this        | and this script's function is put
+    //  | script's function is put      | into innerFunctions()
+    //  | into innerFunctions()         | (setEnclosingLazyScript)
+    //  | (setEnclosingLazyScript)      |
+    //  |                               |
+    //  |                               |     o
+    //  |                               |     | when function is lazily
+    //  |                               |     | parsed inside a function
+    //  |                               |     | which is eagerly parsed
+    //  |                               |     | (Create)
+    //  v                               |     v
+    // +----------------------+         |    +---------+
+    // | enclosing LazyScript |<--------+    | nullptr |
+    // +----------------------+              +---------+
+    //  |                                     |
+    //  v                                     |
+    //  +<------------------------------------+
+    //  |
+    //  | when the enclosing script     o
+    //  | is successfully compiled      | when decoded from XDR,
+    //  | (setEnclosingScope)           | and enclosing script is not lazy
+    //  v                               | (CreateForXDR with enclosingScope)
+    // +-----------------+              |
+    // | enclosing Scope |<-------------+
+    // +-----------------+
+    GCPtr<TenuredCell*> enclosingLazyScriptOrScope_;
 
     // ScriptSourceObject. We leave this set to nullptr until we generate
     // bytecode for our immediate parent. This is never a CCW; we don't clone
     // LazyScripts into other compartments.
     GCPtrObject sourceObject_;
 
     // Heap allocated table with any free variables or inner functions.
     void* table_;
@@ -2241,21 +2321,21 @@ class LazyScript : public gc::TenuredCel
     // innerFunctions with dummy values to be replaced in a later initialization
     // phase.
     //
     // The "script" argument to this function can be null.  If it's non-null,
     // then this LazyScript should be associated with the given JSScript.
     //
     // The sourceObject and enclosingScope arguments may be null if the
     // enclosing function is also lazy.
-    static LazyScript* Create(JSContext* cx, HandleFunction fun,
-                              HandleScript script, HandleScope enclosingScope,
-                              HandleScriptSourceObject sourceObject,
-                              uint64_t packedData, uint32_t begin, uint32_t end,
-                              uint32_t toStringStart, uint32_t lineno, uint32_t column);
+    static LazyScript* CreateForXDR(JSContext* cx, HandleFunction fun,
+                                    HandleScript script, HandleScope enclosingScope,
+                                    HandleScriptSourceObject sourceObject,
+                                    uint64_t packedData, uint32_t begin, uint32_t end,
+                                    uint32_t toStringStart, uint32_t lineno, uint32_t column);
 
     void initRuntimeFields(uint64_t packedFields);
 
     static inline JSFunction* functionDelazifying(JSContext* cx, Handle<LazyScript*>);
     JSFunction* functionNonDelazifying() const {
         return function_;
     }
 
@@ -2266,31 +2346,50 @@ class LazyScript : public gc::TenuredCel
     }
     const JSScript* maybeScriptUnbarriered() const {
         return script_.unbarrieredGet();
     }
     bool hasScript() const {
         return bool(script_);
     }
 
+    bool hasEnclosingScope() const {
+        return enclosingLazyScriptOrScope_ &&
+               enclosingLazyScriptOrScope_->is<Scope>();
+    }
+    bool hasEnclosingLazyScript() const {
+        return enclosingLazyScriptOrScope_ &&
+               enclosingLazyScriptOrScope_->is<LazyScript>();
+    }
+
+    LazyScript* enclosingLazyScript() const {
+        MOZ_ASSERT(hasEnclosingLazyScript());
+        return enclosingLazyScriptOrScope_->as<LazyScript>();
+    }
+    void setEnclosingLazyScript(LazyScript* enclosingLazyScript);
+
     Scope* enclosingScope() const {
-        return enclosingScope_;
+        MOZ_ASSERT(hasEnclosingScope());
+        return enclosingLazyScriptOrScope_->as<Scope>();
+    }
+    void setEnclosingScope(Scope* enclosingScope);
+
+    bool hasNonSyntacticScope() const {
+        return enclosingScope()->hasOnChain(ScopeKind::NonSyntactic);
     }
 
     ScriptSourceObject& sourceObject() const;
     ScriptSource* scriptSource() const {
         return sourceObject().source();
     }
     ScriptSource* maybeForwardedScriptSource() const;
     bool mutedErrors() const {
         return scriptSource()->mutedErrors();
     }
 
-    void setEnclosingScope(Scope* enclosingScope);
-
     uint32_t numClosedOverBindings() const {
         return p_.numClosedOverBindings;
     }
     JSAtom** closedOverBindings() {
         return (JSAtom**)table_;
     }
 
     uint32_t numInnerFunctions() const {
@@ -2435,23 +2534,24 @@ class LazyScript : public gc::TenuredCel
     }
 
     void setToStringEnd(uint32_t toStringEnd) {
         MOZ_ASSERT(toStringStart_ <= toStringEnd);
         MOZ_ASSERT(toStringEnd_ >= sourceEnd_);
         toStringEnd_ = toStringEnd;
     }
 
-    // Returns true if the enclosing script failed to compile.
-    // See the comment in the definition for more details.
-    bool hasUncompletedEnclosingScript() const;
-
-    // Returns true if the enclosing script is also lazy.
-    bool isEnclosingScriptLazy() const {
-        return !enclosingScope_;
+    // Returns true if the enclosing script has ever been compiled.
+    // Once the enclosing script is compiled, the scope chain is created.
+    // This LazyScript is delazify-able as long as it has the enclosing scope,
+    // even if the enclosing JSScript is GCed.
+    // The enclosing JSScript can be GCed later if the enclosing scope is not
+    // FunctionScope or ModuleScope.
+    bool enclosingScriptHasEverBeenCompiled() const {
+        return hasEnclosingScope();
     }
 
     friend class GCMarker;
     void traceChildren(JSTracer* trc);
     void finalize(js::FreeOp* fop);
 
     static const JS::TraceKind TraceKind = JS::TraceKind::LazyScript;
 
diff --git a/js/src/vm/Realm.cpp b/js/src/vm/Realm.cpp
--- a/js/src/vm/Realm.cpp
+++ b/js/src/vm/Realm.cpp
@@ -702,17 +702,17 @@ AddLazyFunctionsForRealm(JSContext* cx, 
         if (gc::IsAboutToBeFinalizedUnbarriered(&fun) ||
             fun->realm() != cx->realm())
         {
             continue;
         }
 
         if (fun->isInterpretedLazy()) {
             LazyScript* lazy = fun->lazyScriptOrNull();
-            if (lazy && !lazy->isEnclosingScriptLazy() && !lazy->hasUncompletedEnclosingScript()) {
+            if (lazy && lazy->enclosingScriptHasEverBeenCompiled()) {
                 if (!lazyFunctions.append(fun))
                     return false;
             }
         }
     }
 
     return true;
 }
