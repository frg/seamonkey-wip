From 6f84b7d0faed65593eff303fd92ffc49ea35a367 Mon Sep 17 00:00:00 2001
From: hawkeye116477 <hawkeye116477@gmail.com>
Date: Sun, 29 Aug 2021 19:17:10 +0200
Subject: [PATCH 1123/1328] Bug 1637913: Sync up JSRegExp::maxCaptureCount with
 RegExpMacroAssembler::maxRegisters

The RegExpCompiler constructor asserts that the number of registers (aka local stack slots) needed to store all the captures for a regexp does not exceed the maximum number of registers (1 << 16). The parser already enforces a maximum number of captures, but the cap is too high. For `n` captures, we need `(n+1)*2` registers: 1 for the beginning and end of each capture, plus the implicit "entire match" capture.

If the number of captures in a regexp is between `(1<<15) - 1` and `1<<16`, the parser will not report an error, but the compiler will assert. We could fix this by checking the number of captures, like V8 does [here](https://github.com/v8/v8/blob/dbf9ff6155d29390c3282e866972e5fa8591f7ce/src/regexp/regexp.cc#L718-L722), but we get better error messages if we just adjust maxCaptures.
---
 js/src/jit-test/tests/regexp/huge-02.js | 13 +++++++++++++
 js/src/new-regexp/regexp-shim.cc        |  4 ++++
 js/src/new-regexp/regexp-shim.h         |  6 ++++--
 3 files changed, 21 insertions(+), 2 deletions(-)
 create mode 100644 js/src/jit-test/tests/regexp/huge-02.js

diff --git a/js/src/jit-test/tests/regexp/huge-02.js b/js/src/jit-test/tests/regexp/huge-02.js
new file mode 100644
index 000000000000..1d0d94e10c7f
--- /dev/null
+++ b/js/src/jit-test/tests/regexp/huge-02.js
@@ -0,0 +1,13 @@
+var interestingCaptureNums = [(1 << 14),
+                              (1 << 15) - 1,
+                              (1 << 15),
+                              (1 << 15) + 1,
+                              (1 << 16)]
+
+for (let i of interestingCaptureNums) {
+    print(i);
+    try {
+        var source = Array(i).join("(") + "a" + Array(i).join(")");
+        RegExp(source).exec("a");
+    } catch {}
+}
diff --git a/js/src/new-regexp/regexp-shim.cc b/js/src/new-regexp/regexp-shim.cc
index 33907bc8c74d..ea5451e98e2d 100644
--- a/js/src/new-regexp/regexp-shim.cc
+++ b/js/src/new-regexp/regexp-shim.cc
@@ -9,6 +9,7 @@
 
 #include <iostream>
 
+#include "new-regexp/regexp-macro-assembler.h"
 #include "new-regexp/regexp-shim.h"
 #include "new-regexp/regexp-stack.h"
 
@@ -206,6 +207,9 @@ Isolate::InternalizeString(const Vector<const uint8_t>& str);
 template Handle<String>
 Isolate::InternalizeString(const Vector<const char16_t>& str);
 
+static_assert(JSRegExp::RegistersForCaptureCount(JSRegExp::kMaxCaptures) <=
+              RegExpMacroAssembler::kMaxRegisterCount);
+
 // TODO: Map flags to jitoptions
 bool FLAG_correctness_fuzzer_suppressions = false;
 bool FLAG_enable_regexp_unaligned_accesses = false;
diff --git a/js/src/new-regexp/regexp-shim.h b/js/src/new-regexp/regexp-shim.h
index 71788376fcac..9fe18484102a 100644
--- a/js/src/new-regexp/regexp-shim.h
+++ b/js/src/new-regexp/regexp-shim.h
@@ -924,7 +924,9 @@ class JSRegExp : public HeapObject {
   }
 
   // Each capture (including the match itself) needs two registers.
-  static int RegistersForCaptureCount(int count) { return (count + 1) * 2; }
+  static constexpr int RegistersForCaptureCount(int count) {
+    return (count + 1) * 2;
+  }
 
   inline int MaxRegisterCount() const {
     return inner()->getMaxRegisters();
@@ -935,7 +937,7 @@ class JSRegExp : public HeapObject {
   // ******************************
 
   // Maximum number of captures allowed.
-  static constexpr int kMaxCaptures = 1 << 16;
+  static constexpr int kMaxCaptures = (1 << 15) - 1;
 
   // **************************************************
   // JSRegExp::Flags
-- 
2.33.0.windows.2

