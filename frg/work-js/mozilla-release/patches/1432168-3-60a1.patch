# HG changeset patch
# User Matthew Gaudet <mgaudet@mozilla.com>
# Date 1516976692 18000
#      Fri Jan 26 09:24:52 2018 -0500
# Node ID 8cba4fe278078537b6922e62250344d0030177d5
# Parent  c6d4bfe57dd814c0eb5e9ede7edf42dc457d649b
Bug 1432168 - Make guardType(..., JSVAL_TYPE_DOUBLE) check for Double, and not Number. r=tcampbell

To make this change, replace calls to guardType that currently have DOUBLE as the value
with ones that use a new guardNumber, which tests for Numbers instead.

diff --git a/js/src/jit/CacheIR.cpp b/js/src/jit/CacheIR.cpp
--- a/js/src/jit/CacheIR.cpp
+++ b/js/src/jit/CacheIR.cpp
@@ -4343,17 +4343,21 @@ TypeOfIRGenerator::tryAttachStub()
 }
 
 bool
 TypeOfIRGenerator::tryAttachPrimitive(ValOperandId valId)
 {
     if (!val_.isPrimitive())
         return false;
 
-    writer.guardType(valId, val_.isNumber() ? JSVAL_TYPE_DOUBLE : val_.extractNonDoubleType());
+    if (val_.isNumber())
+        writer.guardIsNumber(valId);
+    else
+        writer.guardType(valId,  val_.extractNonDoubleType());
+
     writer.loadStringResult(TypeName(js::TypeOfValue(val_), cx_->names()));
     writer.returnFromIC();
 
     return true;
 }
 
 bool
 TypeOfIRGenerator::tryAttachObject(ValOperandId valId)
diff --git a/js/src/jit/CacheIR.h b/js/src/jit/CacheIR.h
--- a/js/src/jit/CacheIR.h
+++ b/js/src/jit/CacheIR.h
@@ -164,16 +164,17 @@ enum class CacheKind : uint8_t
 extern const char* CacheKindNames[];
 
 #define CACHE_IR_OPS(_)                   \
     _(GuardIsObject)                      \
     _(GuardIsObjectOrNull)                \
     _(GuardIsNullOrUndefined)             \
     _(GuardIsString)                      \
     _(GuardIsSymbol)                      \
+    _(GuardIsNumber)                      \
     _(GuardIsInt32Index)                  \
     _(GuardType)                          \
     _(GuardShape)                         \
     _(GuardGroup)                         \
     _(GuardProto)                         \
     _(GuardClass)                         /* Guard an object class, per GuardClassKind */ \
     _(GuardAnyClass)                      /* Guard an arbitrary class for an object */ \
     _(GuardCompartment)                   \
@@ -525,16 +526,19 @@ class MOZ_RAII CacheIRWriter : public JS
         return SymbolOperandId(val.id());
     }
     Int32OperandId guardIsInt32Index(ValOperandId val) {
         Int32OperandId res(nextOperandId_++);
         writeOpWithOperandId(CacheOp::GuardIsInt32Index, val);
         writeOperandId(res);
         return res;
     }
+    void guardIsNumber(ValOperandId val) {
+        writeOpWithOperandId(CacheOp::GuardIsNumber, val);
+    }
     void guardType(ValOperandId val, JSValueType type) {
         writeOpWithOperandId(CacheOp::GuardType, val);
         static_assert(sizeof(type) == sizeof(uint8_t), "JSValueType should fit in a byte");
         buffer_.writeByte(uint32_t(type));
     }
     void guardIsObjectOrNull(ValOperandId val) {
         writeOpWithOperandId(CacheOp::GuardIsObjectOrNull, val);
     }
diff --git a/js/src/jit/CacheIRCompiler.cpp b/js/src/jit/CacheIRCompiler.cpp
--- a/js/src/jit/CacheIRCompiler.cpp
+++ b/js/src/jit/CacheIRCompiler.cpp
@@ -1202,16 +1202,35 @@ CacheIRCompiler::emitFailurePath(size_t 
         return false;
 
     masm.bind(failure.label());
     allocator.restoreInputState(masm);
     return true;
 }
 
 bool
+CacheIRCompiler::emitGuardIsNumber()
+{
+    ValOperandId inputId = reader.valOperandId();
+    JSValueType knownType = allocator.knownType(inputId);
+
+    // Doubles and ints are numbers!
+    if (knownType == JSVAL_TYPE_DOUBLE || knownType == JSVAL_TYPE_INT32)
+        return true;
+
+    ValueOperand input = allocator.useValueRegister(masm, inputId);
+    FailurePath* failure;
+    if (!addFailurePath(&failure))
+        return false;
+
+    masm.branchTestNumber(Assembler::NotEqual, input, failure->label());
+    return true;
+}
+
+bool
 CacheIRCompiler::emitGuardIsObject()
 {
     ValOperandId inputId = reader.valOperandId();
     if (allocator.knownType(inputId) == JSVAL_TYPE_OBJECT)
         return true;
 
     ValueOperand input = allocator.useValueRegister(masm, inputId);
     FailurePath* failure;
@@ -1367,17 +1386,17 @@ CacheIRCompiler::emitGuardType()
         break;
       case JSVAL_TYPE_SYMBOL:
         masm.branchTestSymbol(Assembler::NotEqual, input, failure->label());
         break;
       case JSVAL_TYPE_INT32:
         masm.branchTestInt32(Assembler::NotEqual, input, failure->label());
         break;
       case JSVAL_TYPE_DOUBLE:
-        masm.branchTestNumber(Assembler::NotEqual, input, failure->label());
+        masm.branchTestDouble(Assembler::NotEqual, input, failure->label());
         break;
       case JSVAL_TYPE_BOOLEAN:
         masm.branchTestBoolean(Assembler::NotEqual, input, failure->label());
         break;
       case JSVAL_TYPE_UNDEFINED:
         masm.branchTestUndefined(Assembler::NotEqual, input, failure->label());
         break;
       case JSVAL_TYPE_NULL:
diff --git a/js/src/jit/CacheIRCompiler.h b/js/src/jit/CacheIRCompiler.h
--- a/js/src/jit/CacheIRCompiler.h
+++ b/js/src/jit/CacheIRCompiler.h
@@ -15,16 +15,17 @@ namespace jit {
 // The ops below are defined in CacheIRCompiler and codegen is shared between
 // BaselineCacheIRCompiler and IonCacheIRCompiler.
 #define CACHE_IR_SHARED_OPS(_)            \
     _(GuardIsObject)                      \
     _(GuardIsNullOrUndefined)             \
     _(GuardIsObjectOrNull)                \
     _(GuardIsString)                      \
     _(GuardIsSymbol)                      \
+    _(GuardIsNumber)                      \
     _(GuardIsInt32Index)                  \
     _(GuardType)                          \
     _(GuardClass)                         \
     _(GuardIsNativeFunction)              \
     _(GuardIsNativeObject)                \
     _(GuardIsProxy)                       \
     _(GuardNotDOMProxy)                   \
     _(GuardSpecificInt32Immediate)        \
