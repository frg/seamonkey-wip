# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1590787792 0
# Node ID 23f9231f76bf57f5b8a1928c575f3ed1c503f44e
# Parent  7259300b087bec02ec5e320795970ab824c0db31
Bug 1635764 - Move --with-system-icu and --with-intl-api to python configure. r=firefox-build-system-reviewers,rstewart

At the same time, because it's now simpler to do so, set the right data
file name for big-endians, even though we don't have or produce it
(bug #1264836). Also remove USE_ICU, which is redundant with
JS_HAS_INTL_API, and actively break the build at configure time when
using --without-intl-api with Firefox because this hasn't actually
worked for close to 3 years (since bug 1402048).

Differential Revision: https://phabricator.services.mozilla.com/D77416

diff --git a/aclocal.m4 b/aclocal.m4
--- a/aclocal.m4
+++ b/aclocal.m4
@@ -12,17 +12,16 @@ builtin(include, build/autoconf/pkg.m4)d
 builtin(include, build/autoconf/codeset.m4)dnl
 builtin(include, build/autoconf/altoptions.m4)dnl
 builtin(include, build/autoconf/mozprog.m4)dnl
 builtin(include, build/autoconf/mozheader.m4)dnl
 builtin(include, build/autoconf/compiler-opts.m4)dnl
 builtin(include, build/autoconf/expandlibs.m4)dnl
 builtin(include, build/autoconf/arch.m4)dnl
 builtin(include, build/autoconf/android.m4)dnl
-builtin(include, build/autoconf/icu.m4)dnl
 builtin(include, build/autoconf/clang-plugin.m4)dnl
 builtin(include, build/autoconf/alloc.m4)dnl
 builtin(include, build/autoconf/sanitize.m4)dnl
 
 MOZ_PROG_CHECKMSYS()
 
 # Read the user's .mozconfig script.  We can't do this in
 # configure.in: autoconf puts the argument parsing code above anything
diff --git a/build/autoconf/icu.m4 b/build/autoconf/icu.m4
deleted file mode 100644
--- a/build/autoconf/icu.m4
+++ /dev/null
@@ -1,99 +0,0 @@
-dnl This Source Code Form is subject to the terms of the Mozilla Public
-dnl License, v. 2.0. If a copy of the MPL was not distributed with this
-dnl file, You can obtain one at http://mozilla.org/MPL/2.0/.
-
-dnl Set the MOZ_ICU_VERSION variable to denote the current version of the
-dnl ICU library, as well as a few other things.
-
-AC_DEFUN([MOZ_CONFIG_ICU], [
-
-MOZ_SYSTEM_ICU=
-MOZ_ARG_WITH_BOOL(system-icu,
-[  --with-system-icu
-                          Use system ICU (located with pkgconfig)],
-    MOZ_SYSTEM_ICU=1)
-
-if test -n "$MOZ_SYSTEM_ICU"; then
-    PKG_CHECK_MODULES(MOZ_ICU, icu-i18n >= 67.1)
-    CFLAGS="$CFLAGS $MOZ_ICU_CFLAGS"
-    CXXFLAGS="$CXXFLAGS $MOZ_ICU_CFLAGS"
-    AC_DEFINE(MOZ_SYSTEM_ICU)
-fi
-
-AC_SUBST(MOZ_SYSTEM_ICU)
-
-MOZ_ARG_WITH_STRING(intl-api,
-[  --with-intl-api, --without-intl-api
-    Determine the status of the ECMAScript Internationalization API.  The first
-    (or lack of any of these) builds and exposes the API.  The second doesn't
-    build ICU at all.],
-    _INTL_API=$withval)
-
-JS_HAS_INTL_API=
-case "$_INTL_API" in
-no)
-    ;;
-yes)
-    JS_HAS_INTL_API=1
-    ;;
-*)
-    AC_MSG_ERROR([Invalid value passed to --with-intl-api: $_INTL_API])
-    ;;
-esac
-
-if test -n "$JS_HAS_INTL_API"; then
-    USE_ICU=1
-fi
-
-if test -n "$JS_HAS_INTL_API"; then
-    AC_DEFINE(JS_HAS_INTL_API)
-fi
-
-dnl Settings for the implementation of the ECMAScript Internationalization API
-if test -n "$USE_ICU"; then
-    icudir="$_topsrcdir/intl/icu/source"
-    if test ! -d "$icudir"; then
-        icudir="$_topsrcdir/../../intl/icu/source"
-        if test ! -d "$icudir"; then
-            AC_MSG_ERROR([Cannot find the ICU directory])
-        fi
-    fi
-
-    version=`sed -n 's/^[[[:space:]]]*#[[:space:]]*define[[:space:]][[:space:]]*U_ICU_VERSION_MAJOR_NUM[[:space:]][[:space:]]*\([0-9][0-9]*\)[[:space:]]*$/\1/p' "$icudir/common/unicode/uvernum.h"`
-    if test x"$version" = x; then
-       AC_MSG_ERROR([cannot determine icu version number from uvernum.h header file $lineno])
-    fi
-    MOZ_ICU_VERSION="$version"
-
-    # TODO: the l is actually endian-dependent
-    # We could make this set as 'l' or 'b' for little or big, respectively,
-    # but we'd need to check in a big-endian version of the file.
-    ICU_DATA_FILE="icudt${version}l.dat"
-fi
-
-AC_SUBST(MOZ_ICU_VERSION)
-AC_SUBST(JS_HAS_INTL_API)
-AC_SUBST(USE_ICU)
-AC_SUBST(ICU_DATA_FILE)
-
-if test -n "$USE_ICU"; then
-    dnl Source files that use ICU should have control over which parts of the ICU
-    dnl namespace they want to use.
-    AC_DEFINE(U_USING_ICU_NAMESPACE,0)
-
-    if test -z "$MOZ_SYSTEM_ICU"; then
-        case "$OS_TARGET:$CPU_ARCH" in
-        WINNT:aarch64)
-            dnl we use non-yasm, non-GNU as solutions here.
-            ;;
-        *)
-            if test -z "$YASM" -a -z "$GNU_AS" -a "$COMPILE_ENVIRONMENT"; then
-                AC_MSG_ERROR([Building ICU requires either yasm or a GNU assembler. If you do not have either of those available for this platform you must use --without-intl-api])
-            fi
-            ;;
-        esac
-        dnl We build ICU as a static library.
-        AC_DEFINE(U_STATIC_IMPLEMENTATION)
-    fi
-fi
-])
diff --git a/build/moz.configure/old.configure b/build/moz.configure/old.configure
--- a/build/moz.configure/old.configure
+++ b/build/moz.configure/old.configure
@@ -237,21 +237,19 @@ def old_configure_options(*options):
     '--prefix',
     '--with-android-distribution-directory',
     '--with-android-max-sdk',
     '--with-android-min-sdk',
     '--with-app-basename',
     '--with-app-name',
     '--with-branding',
     '--with-distribution-id',
-    '--with-intl-api',
     '--with-macbundlename-prefix',
     '--with-nss-exec-prefix',
     '--with-nss-prefix',
-    '--with-system-icu',
     '--with-system-libevent',
     '--with-system-nss',
     '--with-system-png',
     '--with-user-appdir',
     '--x-includes',
     '--x-libraries',
 )
 def prepare_configure_options(host, target, all_options, *options):
diff --git a/js/app.mozbuild b/js/app.mozbuild
--- a/js/app.mozbuild
+++ b/js/app.mozbuild
@@ -21,17 +21,17 @@ if CONFIG['JS_STANDALONE'] and CONFIG['O
 DIRS += [
     '/config/external/fdlibm',
     '/config/external/nspr',
     '/config/external/zlib',
     '/memory',
     '/mozglue',
 ]
 
-if CONFIG['USE_ICU']:
+if CONFIG['JS_HAS_INTL_API']:
     DIRS += [
         '/config/external/icu',
     ]
 
 if CONFIG['COMPILE_ENVIRONMENT'] and CONFIG['BUILD_CTYPES']:
     DIRS += [
         '/config/external/ffi',
     ]
diff --git a/js/moz.configure b/js/moz.configure
--- a/js/moz.configure
+++ b/js/moz.configure
@@ -715,8 +715,69 @@ set_define('EDITLINE', True, when=editli
 
 js_option('--with-jitreport-granularity', default='3', choices=('0', '1', '2', '3'),
           help='Default granularity at which to report JIT code to external tools '
                '(0 - no info, 1 - code ranges for while functions only, '
                '2 - per-line information, 3 - per-op information)')
 
 set_define('JS_DEFAULT_JITREPORT_GRANULARITY',
            depends_if('--with-jitreport-granularity')(lambda value: value[0]))
+
+
+# ECMAScript Internationalization API Support (uses ICU)
+# ======================================================
+js_option('--with-system-icu', help='Use system ICU')
+
+system_icu = pkg_check_modules('MOZ_ICU', 'icu-i18n >= 67.1', when='--with-system-icu')
+
+set_config('MOZ_SYSTEM_ICU', True, when=system_icu)
+set_define('MOZ_SYSTEM_ICU', True, when=system_icu)
+
+js_option('--without-intl-api', help='Disable ECMAScript Internationalization API')
+
+@depends('--with-intl-api', building_js)
+def check_intl_api(enabled, building_js):
+    if not enabled and not building_js:
+        die('--without-intl-api is not supported')
+
+set_config('JS_HAS_INTL_API', True, when='--with-intl-api')
+set_define('JS_HAS_INTL_API', True, when='--with-intl-api')
+
+@depends(check_build_environment, when='--with-intl-api')
+@imports(_from='__builtin__', _import='open')
+@imports(_from='__builtin__', _import='ValueError')
+def icu_version(build_env):
+    path = os.path.join(build_env.topsrcdir, 'intl', 'icu', 'source', 'common',
+                        'unicode', 'uvernum.h')
+    with open(path, encoding='utf-8') as fh:
+        for line in fh:
+            if line.startswith('#define'):
+                define = line.split(None, 3)
+                if len(define) == 3 and define[1] == 'U_ICU_VERSION_MAJOR_NUM':
+                    try:
+                        return str(int(define[2]))
+                    except ValueError:
+                        pass
+    die('Cannot determine ICU version number from uvernum.h header file')
+
+set_config('MOZ_ICU_VERSION', icu_version)
+
+@depends(icu_version, target, when='--with-intl-api')
+def icu_data_file(version, target):
+    # target.endianness is always 'big' or 'little'
+    return 'icudt%s%s.dat' % (version, target.endianness[0])
+
+set_config('ICU_DATA_FILE', icu_data_file)
+
+# Source files that use ICU should have control over which parts of the ICU
+# namespace they want to use.
+set_define('U_USING_ICU_NAMESPACE', '0', when='--with-intl-api')
+
+# We build ICU as a static library.
+set_define('U_STATIC_IMPLEMENTATION', True, when=depends(system_icu)(lambda x: not x))
+
+@depends(yasm, gnu_as, target, compile_environment)
+def can_build_data_file(yasm, gnu_as, target, compile_environment):
+    if not compile_environment or (target.kernel == 'WINNT' and target.cpu == 'aarch64'):
+        return
+    if not yasm and not gnu_as:
+        die('Building ICU requires either yasm or a GNU assembler. If you do not have '
+            'either of those available for this platform you must use --without-intl-api')
diff --git a/js/src/aclocal.m4 b/js/src/aclocal.m4
--- a/js/src/aclocal.m4
+++ b/js/src/aclocal.m4
@@ -11,17 +11,16 @@ builtin(include, ../../build/autoconf/pk
 builtin(include, ../../build/autoconf/codeset.m4)dnl
 builtin(include, ../../build/autoconf/altoptions.m4)dnl
 builtin(include, ../../build/autoconf/mozprog.m4)dnl
 builtin(include, ../../build/autoconf/mozheader.m4)dnl
 builtin(include, ../../build/autoconf/compiler-opts.m4)dnl
 builtin(include, ../../build/autoconf/expandlibs.m4)dnl
 builtin(include, ../../build/autoconf/arch.m4)dnl
 builtin(include, ../../build/autoconf/android.m4)dnl
-builtin(include, ../../build/autoconf/icu.m4)dnl
 builtin(include, ../../build/autoconf/clang-plugin.m4)dnl
 builtin(include, ../../build/autoconf/alloc.m4)dnl
 builtin(include, ../../build/autoconf/sanitize.m4)dnl
 
 define([__MOZ_AC_INIT_PREPARE], defn([AC_INIT_PREPARE]))
 define([AC_INIT_PREPARE],
 [if test -z "$srcdir"; then
   srcdir=`dirname "[$]0"`
diff --git a/js/src/old-configure.in b/js/src/old-configure.in
--- a/js/src/old-configure.in
+++ b/js/src/old-configure.in
@@ -1349,25 +1349,16 @@ AC_SUBST_LIST(MOZ_FIX_LINK_PATHS)
 
 AC_SUBST(MOZ_POST_PROGRAM_COMMAND)
 
 AC_SUBST(MOZ_APP_NAME)
 AC_SUBST(MOZ_APP_DISPLAYNAME)
 
 AC_SUBST(MOZ_PKG_SPECIAL)
 
-dnl ========================================================
-dnl ECMAScript Internationalization API Support (uses ICU)
-dnl ========================================================
-
-dnl top-level configure may override this with --without-intl-api
-_INTL_API=yes
-
-MOZ_CONFIG_ICU()
-
 dnl Echo the CFLAGS to remove extra whitespace.
 CFLAGS=`echo \
 	$_COMPILATION_CFLAGS \
 	$CFLAGS`
 
 CXXFLAGS=`echo \
 	$_WARNINGS_CXXFLAGS \
 	$_COMPILATION_CXXFLAGS \
diff --git a/js/sub.configure b/js/sub.configure
--- a/js/sub.configure
+++ b/js/sub.configure
@@ -44,19 +44,16 @@ def js_subconfigure(host, target, build_
             handler._stdout = PrefixOutput('js/src> ', handler._stdout)
 
     substs = dict(old_configure['substs'])
     assignments = dict(old_configure_assignments)
     environ = dict(os.environ)
 
     options = [host, target] +  js_configure_args
 
-    if not substs.get('JS_HAS_INTL_API'):
-        options.append('--without-intl-api')
-
     options.append('--prefix=%s/dist' % build_env.topobjdir)
 
     if substs.get('ZLIB_IN_MOZGLUE'):
         substs['MOZ_ZLIB_LIBS'] = ''
 
     environ['MOZILLA_CENTRAL_PATH'] = build_env.topsrcdir
     if 'MOZ_BUILD_APP' in environ:
         del environ['MOZ_BUILD_APP']
diff --git a/old-configure.in b/old-configure.in
--- a/old-configure.in
+++ b/old-configure.in
@@ -2814,28 +2814,16 @@ if test -n "$MOZ_TELEMETRY_REPORTING" ||
   AC_DEFINE(MOZ_DATA_REPORTING)
   AC_SUBST(MOZ_DATA_REPORTING)
 fi
 
 dnl win32 options
 AC_SUBST(WIN32_REDIST_DIR)
 AC_SUBST(WIN_UCRT_REDIST_DIR)
 
-dnl ========================================================
-dnl ICU Support
-dnl ========================================================
-
-_INTL_API=yes
-
-if test "$MOZ_WIDGET_TOOLKIT" = "cocoa"; then
-    USE_ICU=1
-fi
-
-MOZ_CONFIG_ICU()
-
 dnl Echo the CFLAGS to remove extra whitespace.
 CFLAGS=`echo \
     $_COMPILATION_CFLAGS \
     $CFLAGS`
 
 CXXFLAGS=`echo \
     $_WARNINGS_CXXFLAGS \
     $_COMPILATION_CXXFLAGS \
diff --git a/python/mozbuild/mozbuild/frontend/context.py b/python/mozbuild/mozbuild/frontend/context.py
--- a/python/mozbuild/mozbuild/frontend/context.py
+++ b/python/mozbuild/mozbuild/frontend/context.py
@@ -529,17 +529,17 @@ class CompileFlags(TargetCompileFlags):
             ('LIBRARY_DEFINES', None, ('CXXFLAGS', 'CFLAGS')),
             ('BASE_INCLUDES', ['-I%s' % main_src_dir, '-I%s' % context.objdir],
              ('CXXFLAGS', 'CFLAGS')),
             ('LOCAL_INCLUDES', None, ('CXXFLAGS', 'CFLAGS')),
             ('EXTRA_INCLUDES', ['-I%s/dist/include' % context.config.topobjdir],
              ('CXXFLAGS', 'CFLAGS')),
             ('OS_INCLUDES', list(itertools.chain(*(context.config.substs.get(v, []) for v in (
                 'NSPR_CFLAGS', 'NSS_CFLAGS', 'MOZ_JPEG_CFLAGS', 'MOZ_PNG_CFLAGS',
-                'MOZ_ZLIB_CFLAGS', 'MOZ_PIXMAN_CFLAGS')))),
+                'MOZ_ZLIB_CFLAGS', 'MOZ_PIXMAN_CFLAGS', 'MOZ_ICU_CFLAGS')))),
              ('CXXFLAGS', 'CFLAGS')),
             ('DSO', context.config.substs.get('DSO_CFLAGS'),
              ('CXXFLAGS', 'CFLAGS')),
             ('DSO_PIC', context.config.substs.get('DSO_PIC_CFLAGS'),
              ('CXXFLAGS', 'CFLAGS')),
             ('RTL', None, ('CXXFLAGS', 'CFLAGS')),
             ('OS_COMPILE_CFLAGS', context.config.substs.get('OS_COMPILE_CFLAGS'),
              ('CFLAGS',)),
diff --git a/toolkit/library/moz.build b/toolkit/library/moz.build
--- a/toolkit/library/moz.build
+++ b/toolkit/library/moz.build
@@ -143,21 +143,16 @@ USE_LIBS += [
     'lgpllibs',
     'nspr',
     'nss',
     'psshparser',
     'sqlite',
     'zlib',
 ]
 
-if CONFIG['USE_ICU']:
-    USE_LIBS += [
-        'icu',
-    ]
-
 if CONFIG['MOZ_WIDGET_TOOLKIT'] == 'gtk':
     USE_LIBS += [
         'mozgtk_stub',
     ]
 
 if CONFIG['MOZ_WAYLAND']:
     USE_LIBS += [
         'mozwayland',

