# HG changeset patch
# User Nathan Froyd <froydnj@mozilla.com>
# Date 1546993900 18000
# Node ID 7adf8f3377828112210c2070055798e79c01f066
# Parent  cca8a87b82a51ee20fedd21d525eaf395726a213
Bug 1513615 - part 2 - move some code around in PostTimerEvent; r=glandium

Doing this code movement separately will ideally make the next part of
this work easier to review.  The idea is that we want to extract all the
necessary information from `timer` before we pass ownership of it into
the newly-allocated nsTimerEvent.

diff --git a/xpcom/threads/TimerThread.cpp b/xpcom/threads/TimerThread.cpp
--- a/xpcom/threads/TimerThread.cpp
+++ b/xpcom/threads/TimerThread.cpp
@@ -757,35 +757,36 @@ TimerThread::PostTimerEvent(already_AddR
   // XXX we may want to reuse this nsTimerEvent in the case of repeating timers.
 
   // Since we already addref'd 'timer', we don't need to addref here.
   // We will release either in ~nsTimerEvent(), or pass the reference back to
   // the caller. We need to copy the generation number from this timer into the
   // event, so we can avoid firing a timer that was re-initialized after being
   // canceled.
 
+#ifdef MOZ_TASK_TRACER
+  // During the dispatch of TimerEvent, we overwrite the current TraceInfo
+  // partially with the info saved in timer earlier, and restore it back by
+  // AutoSaveCurTraceInfo.
+  AutoSaveCurTraceInfo saveCurTraceInfo;
+  (timer->GetTracedTask()).SetTLSTraceInfo();
+#endif
+
+  nsCOMPtr<nsIEventTarget> target = timer->mEventTarget;
+
   void* p = nsTimerEvent::operator new(sizeof(nsTimerEvent));
   if (!p) {
     return timer.forget();
   }
   RefPtr<nsTimerEvent> event = ::new (KnownNotNull, p) nsTimerEvent();
 
   if (MOZ_LOG_TEST(GetTimerLog(), LogLevel::Debug)) {
     event->mInitTime = TimeStamp::Now();
   }
 
-#ifdef MOZ_TASK_TRACER
-  // During the dispatch of TimerEvent, we overwrite the current TraceInfo
-  // partially with the info saved in timer earlier, and restore it back by
-  // AutoSaveCurTraceInfo.
-  AutoSaveCurTraceInfo saveCurTraceInfo;
-  (timer->GetTracedTask()).SetTLSTraceInfo();
-#endif
-
-  nsCOMPtr<nsIEventTarget> target = timer->mEventTarget;
   event->SetTimer(timer.forget());
 
   nsresult rv;
   {
     // We release mMonitor around the Dispatch because if this timer is targeted
     // at the TimerThread we'll deadlock.
     MonitorAutoUnlock unlock(mMonitor);
     rv = target->Dispatch(event, NS_DISPATCH_NORMAL);
