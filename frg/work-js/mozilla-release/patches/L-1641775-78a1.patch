# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1590772287 0
# Node ID 4ba16619f4cb6955ea1f99ec53301ee820839f3a
# Parent  9cd9dbfb1c6bd3ee67c9c4b6fa6806191ce4075b
Bug 1641775 - Move --with-system-nspr to python configure. r=firefox-build-system-reviewers,rstewart

Versions of NSPR >= 4.10 come with a pkg-config file. We currently
depend on 4.9.2 for spidermonkey, but much more recent versions for
Firefox. 4.10 is less than a year newer than 4.9.2, and 4.10 is 7 years
old, so bumping the requirement to 4.10 is not really a big deal.

With the use of pkg-config, --with-nspr-cflags and --with-nspr-libs are
not needed.

None of the AC_TRY_COMPILE tests were any useful because
PR_STATIC_ASSERT and PR_UINT64 have been when we look for them since
4.8.6 and 4.9 respectively.

Differential Revision: https://phabricator.services.mozilla.com/D77412

diff --git a/aclocal.m4 b/aclocal.m4
--- a/aclocal.m4
+++ b/aclocal.m4
@@ -2,18 +2,16 @@ dnl
 dnl Local autoconf macros used with mozilla
 dnl The contents of this file are under the Public Domain.
 dnl
 
 builtin(include, build/autoconf/hotfixes.m4)dnl
 builtin(include, build/autoconf/hooks.m4)dnl
 builtin(include, build/autoconf/config.status.m4)dnl
 builtin(include, build/autoconf/toolchain.m4)dnl
-builtin(include, build/autoconf/nspr.m4)dnl
-builtin(include, build/autoconf/nspr-build.m4)dnl
 builtin(include, build/autoconf/nss.m4)dnl
 builtin(include, build/autoconf/pkg.m4)dnl
 builtin(include, build/autoconf/codeset.m4)dnl
 builtin(include, build/autoconf/altoptions.m4)dnl
 builtin(include, build/autoconf/mozprog.m4)dnl
 builtin(include, build/autoconf/mozheader.m4)dnl
 builtin(include, build/autoconf/compiler-opts.m4)dnl
 builtin(include, build/autoconf/expandlibs.m4)dnl
diff --git a/build/autoconf/nspr-build.m4 b/build/autoconf/nspr-build.m4
deleted file mode 100644
--- a/build/autoconf/nspr-build.m4
+++ /dev/null
@@ -1,190 +0,0 @@
-dnl This Source Code Form is subject to the terms of the Mozilla Public
-dnl License, v. 2.0. If a copy of the MPL was not distributed with this
-dnl file, You can obtain one at http://mozilla.org/MPL/2.0/.
-
-AC_DEFUN([MOZ_CONFIG_NSPR], [
-
-ifelse([$1],,define(CONFIGURING_JS,yes))
-
-dnl Possible ways this can be called:
-dnl   from toplevel configure:
-dnl     JS_STANDALONE=  MOZ_BUILD_APP!=js
-dnl   from js/src/configure invoked by toplevel configure:
-dnl     JS_STANDALONE=  MOZ_BUILD_APP=js
-dnl   from standalone js/src/configure:
-dnl     JS_STANDALONE=1 MOZ_BUILD_APP=js
-
-dnl ========================================================
-dnl = Find the right NSPR to use.
-dnl ========================================================
-MOZ_ARG_WITH_STRING(nspr-cflags,
-[  --with-nspr-cflags=FLAGS
-                          Pass FLAGS to CC when building code that uses NSPR.
-                          Use this when there's no accurate nspr-config
-                          script available.  This is the case when building
-                          SpiderMonkey as part of the Mozilla tree: the
-                          top-level configure script computes NSPR flags
-                          that accomodate the quirks of that environment.],
-    NSPR_CFLAGS=$withval)
-MOZ_ARG_WITH_STRING(nspr-libs,
-[  --with-nspr-libs=LIBS   Pass LIBS to LD when linking code that uses NSPR.
-                          See --with-nspr-cflags for more details.],
-    NSPR_LIBS=$withval)
-
-ifdef([CONFIGURING_JS],[
-    MOZ_ARG_ENABLE_BOOL(nspr-build,
-[  --enable-nspr-build     Build NSPR from source tree],
-        MOZ_BUILD_NSPR=1,
-        MOZ_BUILD_NSPR=)
-])
-
-if test "$MOZ_BUILD_APP" != js || test -n "$JS_STANDALONE"; then
-  _IS_OUTER_CONFIGURE=1
-fi
-
-MOZ_ARG_WITH_BOOL(system-nspr,
-[  --with-system-nspr      Use an NSPR that is already built and installed.
-                          Use the 'nspr-config' script in the current path,
-                          or look for the script in the directories given with
-                          --with-nspr-exec-prefix or --with-nspr-prefix.
-                          (Those flags are only checked if you specify
-                          --with-system-nspr.)],
-    _USE_SYSTEM_NSPR=1 )
-
-JS_WITHOUT_NSPR=unset
-ifdef([CONFIGURING_JS],[
-    if test -n "$JS_STANDALONE"; then
-      if test -z "$_HAS_NSPR"; then
-        JS_WITHOUT_NSPR_DEFAULT=1
-      fi
-    fi
-])
-
-dnl Pass at most one of
-dnl   --with-system-nspr
-dnl   --with-nspr-cflags/libs
-dnl   --enable-nspr-build
-
-AC_MSG_CHECKING([NSPR selection])
-nspr_opts=
-which_nspr=default
-if test -n "$_USE_SYSTEM_NSPR"; then
-    nspr_opts="x$nspr_opts"
-    which_nspr="system"
-fi
-if test -n "$NSPR_CFLAGS" -o -n "$NSPR_LIBS"; then
-    nspr_opts="x$nspr_opts"
-    which_nspr="command-line"
-fi
-if test -n "$MOZ_BUILD_NSPR"; then
-    nspr_opts="x$nspr_opts"
-    which_nspr="source-tree"
-fi
-if test "$JS_WITHOUT_NSPR" = unset; then
-    JS_WITHOUT_NSPR=
-else
-    nspr_opts="x$nspr_opts"
-    which_nspr="without-nspr"
-fi
-
-if test -z "$nspr_opts"; then
-    if test "$MOZ_BUILD_APP" != js; then
-      dnl Toplevel configure defaults to using nsprpub from the source tree
-      MOZ_BUILD_NSPR=1
-      which_nspr="source-tree"
-    else
-      dnl JS configure defaults to no NSPR, falling back to nsprpub.
-      JS_WITHOUT_NSPR="$JS_WITHOUT_NSPR_DEFAULT"
-      if test -z "$JS_WITHOUT_NSPR"; then
-        MOZ_BUILD_NSPR=1
-        which_nspr="source-tree"
-      else
-        which_nspr="without-nspr"
-      fi
-   fi
-fi
-
-if test -z "$nspr_opts" || test "$nspr_opts" = x; then
-    AC_MSG_RESULT($which_nspr)
-else
-    AC_MSG_ERROR([only one way of using NSPR may be selected. See 'configure --help'.])
-fi
-
-AC_SUBST(MOZ_BUILD_NSPR)
-
-if test "$MOZ_BUILD_APP" = js; then
-  if test "$JS_WITHOUT_NSPR" = 1; then
-    AC_DEFINE(JS_WITHOUT_NSPR)
-  fi
-  AC_SUBST(JS_WITHOUT_NSPR)
-fi
-
-# A (sub)configure invoked by the toplevel configure will always receive
-# --with-nspr-libs on the command line. It will never need to figure out
-# anything itself.
-if test -n "$_IS_OUTER_CONFIGURE"; then
-
-if test -n "$_USE_SYSTEM_NSPR"; then
-    AM_PATH_NSPR($NSPR_MINVER, [MOZ_SYSTEM_NSPR=1], [AC_MSG_ERROR([you do not have NSPR installed or your version is older than $NSPR_MINVER.])])
-fi
-
-if test -n "$MOZ_SYSTEM_NSPR" -o -n "$NSPR_CFLAGS" -o -n "$NSPR_LIBS"; then
-    _SAVE_CFLAGS=$CFLAGS
-    CFLAGS="$CFLAGS $NSPR_CFLAGS"
-    AC_TRY_COMPILE([#include "prtypes.h"],
-                [#ifndef PR_STATIC_ASSERT
-                 #error PR_STATIC_ASSERT not defined or requires including prtypes.h
-                 #endif],
-                ,
-                AC_MSG_ERROR([system NSPR does not support PR_STATIC_ASSERT or including prtypes.h does not provide it]))
-    AC_TRY_COMPILE([#include "prtypes.h"],
-                [#ifndef PR_UINT64
-                 #error PR_UINT64 not defined or requires including prtypes.h
-                 #endif],
-                ,
-                AC_MSG_ERROR([system NSPR does not support PR_UINT64 or including prtypes.h does not provide it]))
-    CFLAGS=$_SAVE_CFLAGS
-    NSPR_INCLUDE_DIR=`echo ${NSPR_CFLAGS} | sed -e 's/.*-I\([[^ ]]*\).*/\1/'`
-    NSPR_LIB_DIR=`echo ${NSPR_LIBS} | sed -e 's/.*-L\([[^ ]]*\).*/\1/'`
-elif test -z "$JS_WITHOUT_NSPR"; then
-    NSPR_INCLUDE_DIR="${DIST}/include/nspr"
-    NSPR_CFLAGS="-I${NSPR_INCLUDE_DIR}"
-    if test -n "$GNU_CC"; then
-        if test -n "$MOZ_FOLD_LIBS"; then
-           NSPR_LIB_DIR=${DIST}/lib
-        else
-           NSPR_LIB_DIR=${DIST}/bin
-        fi
-        NSPR_LIBS="-L${NSPR_LIB_DIR} -lnspr${NSPR_VERSION} -lplc${NSPR_VERSION} -lplds${NSPR_VERSION}"
-    else
-        # NSS needs actual static libs to link to, and this is where they are.
-        NSPR_LIBS="${DIST}/lib/nspr${NSPR_VERSION}.lib ${DIST}/lib/plc${NSPR_VERSION}.lib ${DIST}/lib/plds${NSPR_VERSION}.lib "
-        NSPR_LIB_DIR="${DIST}/lib"
-    fi
-fi
-
-AC_SUBST_LIST(NSPR_CFLAGS)
-AC_SUBST(NSPR_INCLUDE_DIR)
-AC_SUBST(NSPR_LIB_DIR)
-
-PKGCONF_REQUIRES_PRIVATE="Requires.private: nspr"
-if test -n "$MOZ_SYSTEM_NSPR"; then
-    _SAVE_CFLAGS=$CFLAGS
-    CFLAGS="$CFLAGS $NSPR_CFLAGS"
-    AC_TRY_COMPILE([#include "prlog.h"],
-                [#ifndef PR_STATIC_ASSERT
-                 #error PR_STATIC_ASSERT not defined
-                 #endif],
-                ,
-                AC_MSG_ERROR([system NSPR does not support PR_STATIC_ASSERT]))
-    CFLAGS=$_SAVE_CFLAGS
-    # piggy back on $MOZ_SYSTEM_NSPR to set a variable for the nspr check for js.pc
-    PKGCONF_REQUIRES_PRIVATE="Requires.private: nspr >= $NSPR_MINVER"
-elif test -n "$JS_WITHOUT_NSPR"; then
-    PKGCONF_REQUIRES_PRIVATE=
-fi
-AC_SUBST([PKGCONF_REQUIRES_PRIVATE])
-
-fi # _IS_OUTER_CONFIGURE
-
-])
diff --git a/build/autoconf/nspr.m4 b/build/autoconf/nspr.m4
deleted file mode 100644
--- a/build/autoconf/nspr.m4
+++ /dev/null
@@ -1,110 +0,0 @@
-# -*- tab-width: 4; -*-
-# Configure paths for NSPR
-# Public domain - Chris Seawood <cls@seawood.org> 2001-04-05
-# Based upon gtk.m4 (also PD) by Owen Taylor
-
-dnl AM_PATH_NSPR([MINIMUM-VERSION, [ACTION-IF-FOUND [, ACTION-IF-NOT-FOUND]]])
-dnl Test for NSPR, and define NSPR_CFLAGS and NSPR_LIBS
-dnl
-dnl If the nspr-config script is available, use it to find the
-dnl appropriate CFLAGS and LIBS, and to check for the required
-dnl version, and run ACTION-IF-FOUND.
-dnl
-dnl Otherwise, if NO_NSPR_CONFIG_SYSTEM_VERSION is set, we use it,
-dnl NO_NSPR_CONFIG_SYSTEM_CFLAGS, and NO_NSPR_CONFIG_SYSTEM_LIBS to
-dnl provide default values, and run ACTION-IF-FOUND.  (Some systems
-dnl ship NSPR without nspr-config, but can glean the appropriate flags
-dnl and version.)
-dnl
-dnl Otherwise, run ACTION-IF-NOT-FOUND.
-AC_DEFUN([AM_PATH_NSPR],
-[dnl
-
-AC_ARG_WITH(nspr-prefix,
-	[  --with-nspr-prefix=PFX  Prefix where NSPR is installed],
-	nspr_config_prefix="$withval",
-	nspr_config_prefix="")
-
-AC_ARG_WITH(nspr-exec-prefix,
-	[  --with-nspr-exec-prefix=PFX
-                          Exec prefix where NSPR is installed],
-	nspr_config_exec_prefix="$withval",
-	nspr_config_exec_prefix="")
-
-	if test -n "$nspr_config_exec_prefix"; then
-		nspr_config_args="$nspr_config_args --exec-prefix=$nspr_config_exec_prefix"
-		if test -z "$NSPR_CONFIG"; then
-			NSPR_CONFIG=$nspr_config_exec_prefix/bin/nspr-config
-		fi
-	fi
-	if test -n "$nspr_config_prefix"; then
-		nspr_config_args="$nspr_config_args --prefix=$nspr_config_prefix"
-		if test -z "$NSPR_CONFIG"; then
-			NSPR_CONFIG=$nspr_config_prefix/bin/nspr-config
-		fi
-	fi
-
-	unset ac_cv_path_NSPR_CONFIG
-	AC_PATH_PROG(NSPR_CONFIG, nspr-config, no)
-	min_nspr_version=ifelse([$1], ,4.0.0,$1)
-	AC_MSG_CHECKING(for NSPR - version >= $min_nspr_version)
-
-	no_nspr=""
-	if test "$NSPR_CONFIG" != "no"; then
-		NSPR_CFLAGS=`$NSPR_CONFIG $nspr_config_args --cflags`
-		NSPR_LIBS=`$NSPR_CONFIG $nspr_config_args --libs`
-		NSPR_VERSION_STRING=`$NSPR_CONFIG $nspr_config_args --version`	
-	elif test -n "${NO_NSPR_CONFIG_SYSTEM_VERSION}"; then
-	    NSPR_CFLAGS="${NO_NSPR_CONFIG_SYSTEM_CFLAGS}"
-		NSPR_LIBS="${NO_NSPR_CONFIG_SYSTEM_LDFLAGS}"
-		NSPR_VERSION_STRING="$NO_NSPR_CONFIG_SYSTEM_VERSION"
-	else
-	    no_nspr="yes"
-	fi
-
-	if test -z "$no_nspr"; then
-		nspr_config_major_version=`echo $NSPR_VERSION_STRING | \
-			sed 's/\([[0-9]]*\)\.\([[0-9]]*\)\(\.\([[0-9]]*\)\)\{0,1\}/\1/'`
-		nspr_config_minor_version=`echo $NSPR_VERSION_STRING | \
-			sed 's/\([[0-9]]*\)\.\([[0-9]]*\)\(\.\([[0-9]]*\)\)\{0,1\}/\2/'`
-		nspr_config_micro_version=`echo $NSPR_VERSION_STRING | \
-			sed 's/\([[0-9]]*\)\.\([[0-9]]*\)\(\.\([[0-9]]*\)\)\{0,1\}/\4/'`
-		if test -z "$nspr_config_micro_version"; then
-			nspr_config_micro_version="0"
-		fi
-
-		min_nspr_major_version=`echo $min_nspr_version | \
-			sed 's/\([[0-9]]*\)\.\([[0-9]]*\)\(\.\([[0-9]]*\)\)\{0,1\}/\1/'`
-		min_nspr_minor_version=`echo $min_nspr_version | \
-			sed 's/\([[0-9]]*\)\.\([[0-9]]*\)\(\.\([[0-9]]*\)\)\{0,1\}/\2/'`
-		min_nspr_micro_version=`echo $min_nspr_version | \
-			sed 's/\([[0-9]]*\)\.\([[0-9]]*\)\(\.\([[0-9]]*\)\)\{0,1\}/\4/'`
-		if test -z "$min_nspr_micro_version"; then
-			min_nspr_micro_version="0"
-		fi
-
-		if test "$nspr_config_major_version" -ne "$min_nspr_major_version"; then
-			no_nspr="yes"
-		elif test "$nspr_config_major_version" -eq "$min_nspr_major_version" &&
-		     test "$nspr_config_minor_version" -lt "$min_nspr_minor_version"; then
-			no_nspr="yes"
-		elif test "$nspr_config_major_version" -eq "$min_nspr_major_version" &&
-		     test "$nspr_config_minor_version" -eq "$min_nspr_minor_version" &&
-		     test "$nspr_config_micro_version" -lt "$min_nspr_micro_version"; then
-			no_nspr="yes"
-		fi
-	fi
-
-	if test -z "$no_nspr"; then
-		AC_MSG_RESULT(yes)
-		ifelse([$2], , :, [$2])     
-	else
-		AC_MSG_RESULT(no)
-		ifelse([$3], , :, [$3])
-	fi
-
-
-	AC_SUBST_LIST(NSPR_CFLAGS)
-	AC_SUBST_LIST(NSPR_LIBS)
-
-])
diff --git a/build/moz.configure/nspr.configure b/build/moz.configure/nspr.configure
new file mode 100644
--- /dev/null
+++ b/build/moz.configure/nspr.configure
@@ -0,0 +1,100 @@
+# -*- Mode: python; indent-tabs-mode: nil; tab-width: 40 -*-
+# vim: set filetype=python:
+# This Source Code Form is subject to the terms of the Mozilla Public
+# License, v. 2.0. If a copy of the MPL was not distributed with this
+# file, You can obtain one at http://mozilla.org/MPL/2.0/.
+
+# Top-level configure defaults to building NSPR from source. Standalone JS
+# doesn't.
+js_option('--enable-nspr-build', when=building_js,
+          help='{Build|Do not build} NSPR from source tree')
+
+@depends('--enable-nspr-build', when=building_js)
+def enable_nspr_build(enable):
+    if enable:
+        return enable
+
+js_option('--with-system-nspr', help='Use system NSPR')
+
+@depends(enable_nspr_build, '--with-system-nspr')
+def build_nspr(nspr_build, system_nspr):
+    if nspr_build is not None and nspr_build.origin != 'default':
+        if nspr_build and system_nspr:
+            die('Cannot use both --enable-nspr-build and --with-system-nspr')
+        if not nspr_build and not system_nspr:
+            die('Cannot use --disable-nspr-build without --with-system-nspr')
+    return not system_nspr
+
+set_config('MOZ_BUILD_NSPR', True, when=build_nspr)
+set_config('MOZ_SYSTEM_NSPR', True, when='--with-system-nspr')
+
+@depends(build_nspr, '--with-system-nspr', building_js)
+def js_without_nspr(build_nspr, system_nspr, building_js):
+    if building_js:
+        return not build_nspr and not system_nspr
+
+set_config('JS_WITHOUT_NSPR', True, when=js_without_nspr)
+set_define('JS_WITHOUT_NSPR', True, when=js_without_nspr)
+
+@depends(building_js)
+def nspr_minver(building_js):
+    if building_js:
+        return 'nspr >= 4.10'
+    return 'nspr >= 4.25'
+
+nspr_pkg = pkg_check_modules('NSPR', nspr_minver, when='--with-system-nspr')
+
+@depends_if(nspr_pkg)
+def nspr_pkg(nspr_pkg):
+    def extract(prefix, list):
+        for item in list:
+            if item.startswith(prefix):
+                return item[len(prefix):]
+        return ''
+
+    include_dir = extract('-I', nspr_pkg.cflags)
+    lib_dir = extract('-L', nspr_pkg.libs)
+    return namespace(
+        cflags=nspr_pkg.cflags,
+        include_dir=include_dir,
+        libs=nspr_pkg.libs,
+        lib_dir=lib_dir,
+    )
+
+@depends(nspr_minver, when='--with-system-nspr')
+def pkgconf_requires_private(nspr_minver):
+    return 'Requires.private: %s' % nspr_minver
+
+set_config('PKGCONF_REQUIRES_PRIVATE', pkgconf_requires_private)
+
+# pkg_check_modules takes care of NSPR_CFLAGS and NSPR_LIBS when using --with-system-nspr.
+@depends(check_build_environment, c_compiler, fold_libs, when=build_nspr)
+def nspr_config(build_env, c_compiler, fold_libs):
+    libs = ['nspr4', 'plc4', 'plds4']
+    if c_compiler.type == 'clang-cl':
+        lib_dir = os.path.join(build_env.dist, 'lib')
+        libs = [os.path.join(lib_dir, '%s.lib' % lib) for lib in libs]
+    else:
+        lib_dir = os.path.join(build_env.dist, 'lib' if fold_libs else 'bin')
+        libs = ['-L%s' % lib_dir] + ['-l%s' % lib for lib in libs]
+
+    include_dir = os.path.join(build_env.dist, 'include', 'nspr')
+    return namespace(
+        cflags=['-I%s' % include_dir],
+        include_dir=include_dir,
+        libs=libs,
+        lib_dir=lib_dir,
+    )
+
+set_config('NSPR_CFLAGS', nspr_config.cflags, when=nspr_config)
+set_config('NSPR_LIBS', nspr_config.libs, when=nspr_config)
+
+set_config('NSPR_INCLUDE_DIR', nspr_config.include_dir, when=nspr_config)
+set_config('NSPR_LIB_DIR', nspr_config.lib_dir, when=nspr_config)
+set_config('NSPR_INCLUDE_DIR', nspr_pkg.include_dir, when=nspr_pkg)
+set_config('NSPR_LIB_DIR', nspr_pkg.lib_dir, when=nspr_pkg)
+
+add_old_configure_assignment('NSPR_CFLAGS', nspr_config.cflags, when=nspr_config)
+add_old_configure_assignment('NSPR_LIBS', nspr_config.libs, when=nspr_config)
+add_old_configure_assignment('NSPR_CFLAGS', nspr_pkg.cflags, when=nspr_pkg)
+add_old_configure_assignment('NSPR_LIBS', nspr_pkg.libs, when=nspr_pkg)
diff --git a/build/moz.configure/old.configure b/build/moz.configure/old.configure
--- a/build/moz.configure/old.configure
+++ b/build/moz.configure/old.configure
@@ -215,17 +215,16 @@ def old_configure_options(*options):
     '--enable-debug-js-modules',
     '--enable-dump-painting',
     '--enable-extensions',
     '--enable-libproxy',
     '--enable-logrefcnt',
     '--enable-mobile-optimize',
     '--enable-necko-wifi',
     '--enable-negotiateauth',
-    '--enable-nspr-build',
     '--enable-official-branding',
     '--enable-parental-controls',
     '--enable-pref-extensions',
     '--enable-sandbox',
     '--enable-startupcache',
     '--enable-system-cairo',
     '--enable-system-extension-dirs',
     '--enable-system-pixman',
@@ -242,25 +241,20 @@ def old_configure_options(*options):
     '--with-app-basename',
     '--with-app-name',
     '--with-branding',
     '--with-debug-label',
     '--with-distribution-id',
     '--with-intl-api',
     '--with-jitreport-granularity',
     '--with-macbundlename-prefix',
-    '--with-nspr-cflags',
-    '--with-nspr-exec-prefix',
-    '--with-nspr-libs',
-    '--with-nspr-prefix',
     '--with-nss-exec-prefix',
     '--with-nss-prefix',
     '--with-system-icu',
     '--with-system-libevent',
-    '--with-system-nspr',
     '--with-system-nss',
     '--with-system-png',
     '--with-user-appdir',
     '--x-includes',
     '--x-libraries',
 )
 def prepare_configure_options(host, target, all_options, *options):
     # old-configure only supports the options listed in @old_configure_options
diff --git a/js/moz.configure b/js/moz.configure
--- a/js/moz.configure
+++ b/js/moz.configure
@@ -28,16 +28,18 @@ js_option('--enable-smoosh', default=Fal
 @depends('--enable-smoosh')
 def enable_smoosh(value):
     if value:
         return True
 
 set_config('JS_ENABLE_SMOOSH', enable_smoosh)
 set_define('JS_ENABLE_SMOOSH', enable_smoosh)
 
+include('../build/moz.configure/nspr.configure',
+        when='--enable-compile-environment')
 include('../build/moz.configure/rust.configure',
         when='--enable-compile-environment')
 include('../build/moz.configure/bindgen.configure',
         when='--enable-compile-environment')
 
 @depends('JS_STANDALONE')
 def js_standalone(value):
     if value:
diff --git a/js/src/aclocal.m4 b/js/src/aclocal.m4
--- a/js/src/aclocal.m4
+++ b/js/src/aclocal.m4
@@ -3,18 +3,16 @@ dnl Local autoconf macros used with mozi
 dnl The contents of this file are under the Public Domain.
 dnl
 
 builtin(include, ../../build/autoconf/hotfixes.m4)dnl
 builtin(include, ../../build/autoconf/hooks.m4)dnl
 builtin(include, ../../build/autoconf/config.status.m4)dnl
 builtin(include, ../../build/autoconf/toolchain.m4)dnl
 builtin(include, ../../build/autoconf/pkg.m4)dnl
-builtin(include, ../../build/autoconf/nspr.m4)dnl
-builtin(include, ../../build/autoconf/nspr-build.m4)dnl
 builtin(include, ../../build/autoconf/codeset.m4)dnl
 builtin(include, ../../build/autoconf/altoptions.m4)dnl
 builtin(include, ../../build/autoconf/mozprog.m4)dnl
 builtin(include, ../../build/autoconf/mozheader.m4)dnl
 builtin(include, ../../build/autoconf/compiler-opts.m4)dnl
 builtin(include, ../../build/autoconf/expandlibs.m4)dnl
 builtin(include, ../../build/autoconf/arch.m4)dnl
 builtin(include, ../../build/autoconf/android.m4)dnl
diff --git a/js/src/old-configure.in b/js/src/old-configure.in
--- a/js/src/old-configure.in
+++ b/js/src/old-configure.in
@@ -38,21 +38,16 @@ dnl ====================================
 _SUBDIR_CXXFLAGS="$CXXFLAGS"
 _SUBDIR_LDFLAGS="$LDFLAGS"
 _SUBDIR_HOST_CC="$HOST_CC"
 _SUBDIR_HOST_CFLAGS="$HOST_CFLAGS"
 _SUBDIR_HOST_CXXFLAGS="$HOST_CXXFLAGS"
 _SUBDIR_HOST_LDFLAGS="$HOST_LDFLAGS"
 _SUBDIR_CONFIG_ARGS="$ac_configure_args"
 
-dnl Set the version number of the libs included with mozilla
-dnl ========================================================
-NSPR_VERSION=4
-NSPR_MINVER=4.9.2
-
 dnl Set the minimum version of toolkit libs used by mozilla
 dnl ========================================================
 W32API_VERSION=3.14
 
 dnl Set various checks
 dnl ========================================================
 MISSING_X=
 
@@ -1125,18 +1120,16 @@ dnl are defined in build/autoconf/altopt
 
 dnl ========================================================
 dnl =
 dnl = Check for external package dependencies
 dnl =
 dnl ========================================================
 MOZ_ARG_HEADER(External Packages)
 
-MOZ_CONFIG_NSPR(js)
-
 if test -n "$ZLIB_IN_MOZGLUE"; then
     AC_DEFINE(ZLIB_IN_MOZGLUE)
 fi
 AC_SUBST(ZLIB_IN_MOZGLUE)
 
 dnl ========================================================
 dnl =
 dnl = Application
@@ -1410,18 +1403,16 @@ HOST_CFLAGS=`echo \
     $_COMPILATION_HOST_CFLAGS \
     $HOST_CFLAGS`
 
 HOST_CXXFLAGS=`echo \
     $_WARNINGS_HOST_CXXFLAGS \
     $_COMPILATION_HOST_CXXFLAGS \
     $HOST_CXXFLAGS`
 
-AC_SUBST(MOZ_SYSTEM_NSPR)
-
 OS_CFLAGS="$CFLAGS"
 OS_CXXFLAGS="$CXXFLAGS"
 OS_CPPFLAGS="$CPPFLAGS"
 OS_COMPILE_CFLAGS="$COMPILE_CFLAGS"
 OS_COMPILE_CXXFLAGS="$COMPILE_CXXFLAGS"
 OS_LDFLAGS="$LDFLAGS"
 OS_LIBS="$LIBS"
 AC_SUBST_LIST(OS_CFLAGS)
diff --git a/js/sub.configure b/js/sub.configure
--- a/js/sub.configure
+++ b/js/sub.configure
@@ -42,32 +42,21 @@ def js_subconfigure(host, target, build_
         handler.setFormatter(formatter)
         if isinstance(handler, ConfigureOutputHandler):
             handler._stdout = PrefixOutput('js/src> ', handler._stdout)
 
     substs = dict(old_configure['substs'])
     assignments = dict(old_configure_assignments)
     environ = dict(os.environ)
 
-    options = [host, target] + [
-        o for o in js_configure_args
-        # --with-system-nspr will have been converted into the relevant $NSPR_CFLAGS
-        # and $NSPR_LIBS.
-        if not o.startswith('--with-system-nspr')
-    ]
+    options = [host, target] +  js_configure_args
 
     if not substs.get('JS_HAS_INTL_API'):
         options.append('--without-intl-api')
 
-    if substs.get('NSPR_CFLAGS') or substs.get('NSPR_LIBS'):
-        options.append(
-            '--with-nspr-cflags=%s' % ' '.join(substs.get('NSPR_CFLAGS', [])))
-        options.append(
-            '--with-nspr-libs=%s' % ' '.join(substs.get('NSPR_LIBS', [])))
-
     options.append('--prefix=%s/dist' % build_env.topobjdir)
 
     if substs.get('ZLIB_IN_MOZGLUE'):
         substs['MOZ_ZLIB_LIBS'] = ''
 
     environ['MOZILLA_CENTRAL_PATH'] = build_env.topsrcdir
     if 'MOZ_BUILD_APP' in environ:
         del environ['MOZ_BUILD_APP']
diff --git a/moz.configure b/moz.configure
--- a/moz.configure
+++ b/moz.configure
@@ -258,16 +258,18 @@ set_define('MOZ_DLL_PREFIX', depends(lib
 set_define('MOZ_DLL_SUFFIX', depends(library_name_info.dll.suffix)(lambda s: '"%s"' % s))
 set_config('WASM_OBJ_SUFFIX', 'wasm')
 
 # Make `profiling` available to this file even when js/moz.configure
 # doesn't end up included.
 profiling = dependable(False)
 # Same for js_standalone
 js_standalone = dependable(False)
+# Same for fold_libs
+fold_libs = dependable(False)
 
 include(include_project_configure)
 
 @depends('--help')
 @imports(_from='mozbuild.backend', _import='backends')
 def build_backends_choices(_):
     return tuple(backends)
 
diff --git a/old-configure.in b/old-configure.in
--- a/old-configure.in
+++ b/old-configure.in
@@ -41,18 +41,16 @@ dnl ====================================
 _SUBDIR_HOST_CFLAGS="$HOST_CFLAGS"
 _SUBDIR_HOST_CXXFLAGS="$HOST_CXXFLAGS"
 _SUBDIR_HOST_LDFLAGS="$HOST_LDFLAGS"
 _SUBDIR_CONFIG_ARGS="$ac_configure_args"
 
 dnl Set the version number of the libs included with mozilla
 dnl ========================================================
 MOZPNG=10635
-NSPR_VERSION=4
-NSPR_MINVER=4.25
 NSS_VERSION=3
 
 dnl Set the minimum version of toolkit libs used by mozilla
 dnl ========================================================
 GLIB_VERSION=2.22
 # 2_26 is the earliest version we can set GLIB_VERSION_MIN_REQUIRED.
 # The macro won't be used when compiling with earlier versions anyway.
 GLIB_VERSION_MIN_REQUIRED=GLIB_VERSION_2_26
@@ -1394,18 +1392,16 @@ dnl are defined in build/autoconf/altopt
 
 dnl ========================================================
 dnl =
 dnl = Check for external package dependencies
 dnl =
 dnl ========================================================
 MOZ_ARG_HEADER(External Packages)
 
-MOZ_CONFIG_NSPR()
-
 dnl ========================================================
 dnl system libevent Support
 dnl ========================================================
 MOZ_ARG_WITH_STRING(system-libevent,
 [  --with-system-libevent[=PFX]
                           Use system libevent [installed at prefix PFX]],
     LIBEVENT_DIR=$withval)
 
@@ -2880,18 +2876,16 @@ HOST_CXXFLAGS=`echo \
     $_COMPILATION_HOST_CXXFLAGS \
     $HOST_CXXFLAGS`
 
 AC_SUBST(MOZ_SYSTEM_PNG)
 
 AC_SUBST_LIST(MOZ_PNG_CFLAGS)
 AC_SUBST_LIST(MOZ_PNG_LIBS)
 
-AC_SUBST(MOZ_SYSTEM_NSPR)
-
 AC_SUBST(MOZ_SYSTEM_NSS)
 
 HOST_CMFLAGS="-x objective-c -fobjc-exceptions"
 HOST_CMMFLAGS="-x objective-c++ -fobjc-exceptions"
 OS_COMPILE_CMFLAGS="-x objective-c -fobjc-exceptions"
 OS_COMPILE_CMMFLAGS="-x objective-c++ -fobjc-exceptions"
 if test "$MOZ_WIDGET_TOOLKIT" = uikit; then
   OS_COMPILE_CMFLAGS="$OS_COMPILE_CMFLAGS -fobjc-abi-version=2 -fobjc-legacy-dispatch"

