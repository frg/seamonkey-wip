# HG changeset patch
# User Daisuke Akatsuka <dakatsuka@mozilla.com>
# Date 1520927118 -32400
# Node ID e9c4e18b5b6f8a3a1b5b159844707093b0d25377
# Parent  3f304e5bcd0e3196f7a6e11c1dd80c2d4e23f4b7
Bug 1431573 - Part 3: Implement rewind button. r=gl

MozReview-Commit-ID: H7XX5rUIqZG

diff --git a/devtools/client/inspector/animation/animation.js b/devtools/client/inspector/animation/animation.js
--- a/devtools/client/inspector/animation/animation.js
+++ b/devtools/client/inspector/animation/animation.js
@@ -24,16 +24,17 @@ const { isAllAnimationEqual } = require(
 class AnimationInspector {
   constructor(inspector, win) {
     this.inspector = inspector;
     this.win = win;
 
     this.getAnimatedPropertyMap = this.getAnimatedPropertyMap.bind(this);
     this.getComputedStyle = this.getComputedStyle.bind(this);
     this.getNodeFromActor = this.getNodeFromActor.bind(this);
+    this.rewindAnimationsCurrentTime = this.rewindAnimationsCurrentTime.bind(this);
     this.selectAnimation = this.selectAnimation.bind(this);
     this.setAnimationsPlayState = this.setAnimationsPlayState.bind(this);
     this.setDetailVisibility = this.setDetailVisibility.bind(this);
     this.simulateAnimation = this.simulateAnimation.bind(this);
     this.toggleElementPicker = this.toggleElementPicker.bind(this);
     this.update = this.update.bind(this);
     this.onElementPickerStarted = this.onElementPickerStarted.bind(this);
     this.onElementPickerStopped = this.onElementPickerStopped.bind(this);
@@ -56,16 +57,17 @@ class AnimationInspector {
       onHideBoxModelHighlighter,
     } = this.inspector.getPanel("boxmodel").getComponentProps();
 
     const {
       emit: emitEventForTest,
       getAnimatedPropertyMap,
       getComputedStyle,
       getNodeFromActor,
+      rewindAnimationsCurrentTime,
       selectAnimation,
       setAnimationsPlayState,
       setDetailVisibility,
       simulateAnimation,
       toggleElementPicker,
     } = this;
 
     const target = this.inspector.target;
@@ -80,16 +82,17 @@ class AnimationInspector {
       App(
         {
           emitEventForTest,
           getAnimatedPropertyMap,
           getComputedStyle,
           getNodeFromActor,
           onHideBoxModelHighlighter,
           onShowBoxModelHighlighterForNode,
+          rewindAnimationsCurrentTime,
           selectAnimation,
           setAnimationsPlayState,
           setDetailVisibility,
           setSelectedNode,
           simulateAnimation,
           toggleElementPicker,
         }
       )
@@ -214,16 +217,22 @@ class AnimationInspector {
   onSidebarResized(type, size) {
     if (!this.isPanelVisible()) {
       return;
     }
 
     this.inspector.store.dispatch(updateSidebarSize(size));
   }
 
+  async rewindAnimationsCurrentTime() {
+    const animations = this.state.animations;
+    await this.animationsFront.setCurrentTimes(animations, 0, true);
+    this.updateAnimations(animations);
+  }
+
   selectAnimation(animation) {
     this.inspector.store.dispatch(updateSelectedAnimation(animation));
   }
 
   async setAnimationsPlayState(doPlay) {
     if (doPlay) {
       await this.animationsFront.playAll();
     } else {
diff --git a/devtools/client/inspector/animation/components/AnimationToolbar.js b/devtools/client/inspector/animation/components/AnimationToolbar.js
--- a/devtools/client/inspector/animation/components/AnimationToolbar.js
+++ b/devtools/client/inspector/animation/components/AnimationToolbar.js
@@ -4,35 +4,43 @@
 
 "use strict";
 
 const { createFactory, PureComponent } = require("devtools/client/shared/vendor/react");
 const dom = require("devtools/client/shared/vendor/react-dom-factories");
 const PropTypes = require("devtools/client/shared/vendor/react-prop-types");
 
 const PauseResumeButton = createFactory(require("./PauseResumeButton"));
+const RewindButton = createFactory(require("./RewindButton"));
 
 class AnimationToolbar extends PureComponent {
   static get propTypes() {
     return {
       animations: PropTypes.arrayOf(PropTypes.object).isRequired,
+      rewindAnimationsCurrentTime: PropTypes.func.isRequired,
       setAnimationsPlayState: PropTypes.func.isRequired,
     };
   }
 
   render() {
     const {
       animations,
+      rewindAnimationsCurrentTime,
       setAnimationsPlayState,
     } = this.props;
 
     return dom.div(
       {
         className: "animation-toolbar devtools-toolbar",
       },
+      RewindButton(
+        {
+          rewindAnimationsCurrentTime,
+        }
+      ),
       PauseResumeButton(
         {
           animations,
           setAnimationsPlayState,
         }
       )
     );
   }
diff --git a/devtools/client/inspector/animation/components/App.js b/devtools/client/inspector/animation/components/App.js
--- a/devtools/client/inspector/animation/components/App.js
+++ b/devtools/client/inspector/animation/components/App.js
@@ -21,16 +21,17 @@ class App extends PureComponent {
       animations: PropTypes.arrayOf(PropTypes.object).isRequired,
       detailVisibility: PropTypes.bool.isRequired,
       emitEventForTest: PropTypes.func.isRequired,
       getAnimatedPropertyMap: PropTypes.func.isRequired,
       getComputedStyle: PropTypes.func.isRequired,
       getNodeFromActor: PropTypes.func.isRequired,
       onHideBoxModelHighlighter: PropTypes.func.isRequired,
       onShowBoxModelHighlighterForNode: PropTypes.func.isRequired,
+      rewindAnimationsCurrentTime: PropTypes.func.isRequired,
       selectAnimation: PropTypes.func.isRequired,
       setAnimationsPlayState: PropTypes.func.isRequired,
       setDetailVisibility: PropTypes.func.isRequired,
       setSelectedNode: PropTypes.func.isRequired,
       simulateAnimation: PropTypes.func.isRequired,
       toggleElementPicker: PropTypes.func.isRequired,
     };
   }
@@ -44,16 +45,17 @@ class App extends PureComponent {
       animations,
       detailVisibility,
       emitEventForTest,
       getAnimatedPropertyMap,
       getComputedStyle,
       getNodeFromActor,
       onHideBoxModelHighlighter,
       onShowBoxModelHighlighterForNode,
+      rewindAnimationsCurrentTime,
       selectAnimation,
       setAnimationsPlayState,
       setDetailVisibility,
       setSelectedNode,
       simulateAnimation,
       toggleElementPicker,
     } = this.props;
 
@@ -62,16 +64,17 @@ class App extends PureComponent {
         id: "animation-container",
         className: detailVisibility ? "animation-detail-visible" : "",
       },
       animations.length ?
       [
         AnimationToolbar(
           {
             animations,
+            rewindAnimationsCurrentTime,
             setAnimationsPlayState,
           }
         ),
         SplitBox({
           className: "animation-container-splitter",
           endPanel: AnimationDetailContainer(
             {
               emitEventForTest,
diff --git a/devtools/client/inspector/animation/components/RewindButton.js b/devtools/client/inspector/animation/components/RewindButton.js
new file mode 100644
--- /dev/null
+++ b/devtools/client/inspector/animation/components/RewindButton.js
@@ -0,0 +1,33 @@
+/* This Source Code Form is subject to the terms of the Mozilla Public
+ * License, v. 2.0. If a copy of the MPL was not distributed with this
+ * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
+
+"use strict";
+
+const { PureComponent } = require("devtools/client/shared/vendor/react");
+const dom = require("devtools/client/shared/vendor/react-dom-factories");
+const PropTypes = require("devtools/client/shared/vendor/react-prop-types");
+
+const { getStr } = require("../utils/l10n");
+
+class RewindButton extends PureComponent {
+  static get propTypes() {
+    return {
+      rewindAnimationsCurrentTime: PropTypes.func.isRequired,
+    };
+  }
+
+  render() {
+    const { rewindAnimationsCurrentTime } = this.props;
+
+    return dom.button(
+      {
+        className: "rewind-button devtools-button",
+        onClick: rewindAnimationsCurrentTime,
+        title: getStr("timeline.rewindButtonTooltip"),
+      }
+    );
+  }
+}
+
+module.exports = RewindButton;
diff --git a/devtools/client/inspector/animation/components/moz.build b/devtools/client/inspector/animation/components/moz.build
--- a/devtools/client/inspector/animation/components/moz.build
+++ b/devtools/client/inspector/animation/components/moz.build
@@ -23,9 +23,10 @@ DevToolsModules(
     'AnimationTimelineTickItem.js',
     'AnimationTimelineTickList.js',
     'AnimationToolbar.js',
     'App.js',
     'KeyframesProgressTickItem.js',
     'KeyframesProgressTickList.js',
     'NoAnimationPanel.js',
     'PauseResumeButton.js',
+    'RewindButton.js',
 )
diff --git a/devtools/client/themes/animation.css b/devtools/client/themes/animation.css
--- a/devtools/client/themes/animation.css
+++ b/devtools/client/themes/animation.css
@@ -10,32 +10,34 @@
   --fast-track-image: url("images/animation-fast-track.svg");
   --fill-color-cssanimation: var(--theme-contrast-background);
   --fill-color-csstransition: var(--theme-highlight-blue);
   --fill-color-scriptanimation: var(--theme-graphs-green);
   --graph-right-offset: 10px;
   --keyframe-marker-shadow-color: #c4c4c4;
   --pause-image: url(chrome://devtools/skin/images/pause.svg);
   --resume-image: url(chrome://devtools/skin/images/play.svg);
+  --rewind-image: url(chrome://devtools/skin/images/rewind.svg);
   --sidebar-width: 200px;
   --stroke-color-cssanimation: var(--theme-highlight-lightorange);
   --stroke-color-csstransition: var(--theme-highlight-bluegrey);
   --stroke-color-scriptanimation: var(--theme-highlight-green);
   --tick-line-style: 0.5px solid rgba(128, 136, 144, 0.5);
 }
 
 :root.theme-dark {
   --animation-even-background-color: rgba(255, 255, 255, 0.05);
   --keyframe-marker-shadow-color: #818181;
 }
 
 :root.theme-firebug {
   --command-pick-image: url(chrome://devtools/skin/images/firebug/command-pick.svg);
   --pause-image: url(chrome://devtools/skin/images/firebug/pause.svg);
   --resume-image: url(chrome://devtools/skin/images/firebug/play.svg);
+  --rewind-image: url(chrome://devtools/skin/images/firebug/rewind.svg);
 }
 
 /* Root element of animation inspector */
 #animation-container {
   display: flex;
   flex-direction: column;
   height: 100%;
 }
@@ -60,16 +62,20 @@
 .pause-resume-button::before {
   background-image: var(--pause-image);
 }
 
 .pause-resume-button.paused::before {
   background-image: var(--resume-image);
 }
 
+.rewind-button::before {
+  background-image: var(--rewind-image);
+}
+
 /* Animation List Container */
 .animation-list-container {
   display: flex;
   flex-direction: column;
   height: 100%;
   overflow: hidden;
   width: 100%;
 }
