# HG changeset patch
# User Tooru Fujisawa <arai_a@mac.com>
# Date 1528354740 -32400
#      Thu Jun 07 15:59:00 2018 +0900
# Node ID 233ac49a9f9afe6c0e1595eb367b0da618dcce09
# Parent  213ee9ed734c1ce7c3a6375643bced426bae02f6
Bug 1460154 - Part 1: Rename IfThenElseEmitter to IfEmitter. r=Yoric

diff --git a/js/src/frontend/BytecodeEmitter.cpp b/js/src/frontend/BytecodeEmitter.cpp
--- a/js/src/frontend/BytecodeEmitter.cpp
+++ b/js/src/frontend/BytecodeEmitter.cpp
@@ -1931,58 +1931,58 @@ class MOZ_STACK_CLASS TryEmitter
 // Class for emitting bytecode for blocks like if-then-else.
 //
 // This class can be used to emit single if-then-else block, or cascading
 // else-if blocks.
 //
 // Usage: (check for the return value is omitted for simplicity)
 //
 //   `if (cond) then_block`
-//     IfThenElseEmitter ifThen(this);
+//     IfEmitter ifThen(this);
 //     emit(cond);
 //     ifThen.emitThen();
 //     emit(then_block);
 //     ifThen.emitEnd();
 //
 //   `if (cond) then_block else else_block`
-//     IfThenElseEmitter ifThenElse(this);
+//     IfEmitter ifThenElse(this);
 //     emit(cond);
 //     ifThenElse.emitThenElse();
 //     emit(then_block);
 //     ifThenElse.emitElse();
 //     emit(else_block);
 //     ifThenElse.emitEnd();
 //
 //   `if (c1) b1 else if (c2) b2 else if (c3) b3 else b4`
-//     IfThenElseEmitter ifThenElse(this);
+//     IfEmitter ifThenElse(this);
 //     emit(c1);
 //     ifThenElse.emitThenElse();
 //     emit(b1);
 //     ifThenElse.emitElseIf();
 //     emit(c2);
 //     ifThenElse.emitThenElse();
 //     emit(b2);
 //     ifThenElse.emitElseIf();
 //     emit(c3);
 //     ifThenElse.emitThenElse();
 //     emit(b3);
 //     ifThenElse.emitElse();
 //     emit(b4);
 //     ifThenElse.emitEnd();
 //
 //   `cond ? then_expr : else_expr`
-//     IfThenElseEmitter condElse(this);
+//     IfEmitter condElse(this);
 //     emit(cond);
 //     condElse.emitCond();
 //     emit(then_block);
 //     condElse.emitElse();
 //     emit(else_block);
 //     condElse.emitEnd();
 //
-class MOZ_STACK_CLASS IfThenElseEmitter
+class MOZ_STACK_CLASS IfEmitter
 {
     BytecodeEmitter* bce_;
 
     // Jump around the then clause, to the beginning of the else clause.
     JumpList jumpAroundThen_;
 
     // Jump around the else clause, to the end of the entire branch.
     JumpList jumpsAroundElse_;
@@ -2040,27 +2040,27 @@ class MOZ_STACK_CLASS IfThenElseEmitter
 
         // After calling emitEnd.
         End
     };
     State state_;
 #endif
 
   public:
-    explicit IfThenElseEmitter(BytecodeEmitter* bce)
+    explicit IfEmitter(BytecodeEmitter* bce)
       : bce_(bce),
         thenDepth_(0)
 #ifdef DEBUG
       , pushed_(0)
       , calculatedPushed_(false)
       , state_(State::Start)
 #endif
     {}
 
-    ~IfThenElseEmitter()
+    ~IfEmitter()
     {}
 
   private:
     MOZ_MUST_USE bool emitIfInternal(SrcNoteType type) {
         // Emit an annotated branch-if-false around the then part.
         if (!bce_->newSrcNote(type))
             return false;
         if (!bce_->emitJump(JSOP_IFEQ, &jumpAroundThen_))
@@ -2298,17 +2298,17 @@ class ForOfLoopControl : public LoopCont
         // If ITER is undefined, it means the exception is thrown by
         // IteratorClose for non-local jump, and we should't perform
         // IteratorClose again here.
         if (!bce->emit1(JSOP_UNDEFINED))          // ITER ... EXCEPTION ITER UNDEF
             return false;
         if (!bce->emit1(JSOP_STRICTNE))           // ITER ... EXCEPTION NE
             return false;
 
-        IfThenElseEmitter ifIteratorIsNotClosed(bce);
+        IfEmitter ifIteratorIsNotClosed(bce);
         if (!ifIteratorIsNotClosed.emitThen())    // ITER ... EXCEPTION
             return false;
 
         MOZ_ASSERT(slotFromTop == unsigned(bce->stackDepth - iterDepth_));
         if (!bce->emitDupAt(slotFromTop))         // ITER ... EXCEPTION ITER
             return false;
         if (!emitIteratorCloseInInnermostScope(bce, CompletionKind::Throw))
             return false;                         // ITER ... EXCEPTION
@@ -2322,17 +2322,17 @@ class ForOfLoopControl : public LoopCont
         // If any yields were emitted, then this for-of loop is inside a star
         // generator and must handle the case of Generator.return. Like in
         // yield*, it is handled with a finally block.
         uint32_t numYieldsEmitted = bce->yieldAndAwaitOffsetList.numYields;
         if (numYieldsEmitted > numYieldsAtBeginCodeNeedingIterClose_) {
             if (!tryCatch_->emitFinally())
                 return false;
 
-            IfThenElseEmitter ifGeneratorClosing(bce);
+            IfEmitter ifGeneratorClosing(bce);
             if (!bce->emit1(JSOP_ISGENCLOSING))   // ITER ... FTYPE FVALUE CLOSING
                 return false;
             if (!ifGeneratorClosing.emitThen())   // ITER ... FTYPE FVALUE
                 return false;
             if (!bce->emitDupAt(slotFromTop + 1)) // ITER ... FTYPE FVALUE ITER
                 return false;
             if (!emitIteratorCloseInInnermostScope(bce, CompletionKind::Normal))
                 return false;                     // ITER ... FTYPE FVALUE
@@ -5501,17 +5501,17 @@ BytecodeEmitter::emitIteratorCloseInScop
     //
     // Get the "return" method.
     if (!emitAtomOp(cx->names().return_, JSOP_CALLPROP))  // ... ITER RET
         return false;
 
     // Step 4.
     //
     // Do nothing if "return" is undefined or null.
-    IfThenElseEmitter ifReturnMethodIsDefined(this);
+    IfEmitter ifReturnMethodIsDefined(this);
     if (!emitPushNotUndefinedOrNull())                    // ... ITER RET NOT-UNDEF-OR-NULL
         return false;
 
     if (!ifReturnMethodIsDefined.emitThenElse())          // ... ITER RET
         return false;
 
     if (completionKind == CompletionKind::Throw) {
         // 7.4.6 IteratorClose ( iterator, completion )
@@ -5880,17 +5880,17 @@ BytecodeEmitter::emitDestructuringOpsArr
             //
             // Non-first elements should emit if-else depending on the
             // member pattern, below.
             if (!emit1(JSOP_POP))                                 // ... OBJ NEXT ITER *LREF
                 return false;
         }
 
         if (member->isKind(ParseNodeKind::Spread)) {
-            IfThenElseEmitter ifThenElse(this);
+            IfEmitter ifThenElse(this);
             if (!isFirst) {
                 // If spread is not the first element of the pattern,
                 // iterator can already be completed.
                                                                   // ... OBJ NEXT ITER *LREF DONE
                 if (!ifThenElse.emitThenElse())                   // ... OBJ NEXT ITER *LREF
                     return false;
 
                 if (!emitUint32Operand(JSOP_NEWARRAY, 0))         // ... OBJ NEXT ITER *LREF ARRAY
@@ -5937,17 +5937,17 @@ BytecodeEmitter::emitDestructuringOpsArr
         }
 
         ParseNode* pndefault = nullptr;
         if (member->isKind(ParseNodeKind::Assign))
             pndefault = member->pn_right;
 
         MOZ_ASSERT(!member->isKind(ParseNodeKind::Spread));
 
-        IfThenElseEmitter ifAlreadyDone(this);
+        IfEmitter ifAlreadyDone(this);
         if (!isFirst) {
                                                                   // ... OBJ NEXT ITER *LREF DONE
             if (!ifAlreadyDone.emitThenElse())                    // ... OBJ NEXT ITER *LREF
                 return false;
 
             if (!emit1(JSOP_UNDEFINED))                           // ... OBJ NEXT ITER *LREF UNDEF
                 return false;
             if (!emit1(JSOP_NOP_DESTRUCTURING))                   // ... OBJ NEXT ITER *LREF UNDEF
@@ -5974,17 +5974,17 @@ BytecodeEmitter::emitDestructuringOpsArr
         if (!emitAtomOp(cx->names().done, JSOP_GETPROP))          // ... OBJ NEXT ITER *LREF RESULT DONE
             return false;
 
         if (!emit1(JSOP_DUP))                                     // ... OBJ NEXT ITER *LREF RESULT DONE DONE
             return false;
         if (!emit2(JSOP_UNPICK, emitted + 2))                     // ... OBJ NEXT ITER DONE *LREF RESULT DONE
             return false;
 
-        IfThenElseEmitter ifDone(this);
+        IfEmitter ifDone(this);
         if (!ifDone.emitThenElse())                               // ... OBJ NEXT ITER DONE *LREF RESULT
             return false;
 
         if (!emit1(JSOP_POP))                                     // ... OBJ NEXT ITER DONE *LREF
             return false;
         if (!emit1(JSOP_UNDEFINED))                               // ... OBJ NEXT ITER DONE *LREF UNDEF
             return false;
         if (!emit1(JSOP_NOP_DESTRUCTURING))                       // ... OBJ NEXT ITER DONE *LREF UNDEF
@@ -6026,17 +6026,17 @@ BytecodeEmitter::emitDestructuringOpsArr
             if (!emit1(JSOP_POP))                                 // ... OBJ NEXT ITER DONE
                 return false;
         }
     }
 
     // The last DONE value is on top of the stack. If not DONE, call
     // IteratorClose.
                                                                   // ... OBJ NEXT ITER DONE
-    IfThenElseEmitter ifDone(this);
+    IfEmitter ifDone(this);
     if (!ifDone.emitThenElse())                                   // ... OBJ NEXT ITER
         return false;
     if (!emitPopN(2))                                             // ... OBJ
         return false;
     if (!ifDone.emitElse())                                       // ... OBJ NEXT ITER
         return false;
     if (!emit1(JSOP_SWAP))                                        // ... OBJ ITER NEXT
         return false;
@@ -6924,17 +6924,17 @@ BytecodeEmitter::emitTry(ParseNode* pn)
         return false;
 
     return true;
 }
 
 bool
 BytecodeEmitter::emitIf(ParseNode* pn)
 {
-    IfThenElseEmitter ifThenElse(this);
+    IfEmitter ifThenElse(this);
 
   if_again:
     /* Emit code for the condition before pushing stmtInfo. */
     if (!emitTreeInBranch(pn->pn_kid1))
         return false;
 
     ParseNode* elseNode = pn->pn_kid3;
     if (elseNode) {
@@ -7154,17 +7154,17 @@ BytecodeEmitter::emitAsyncIterator()
     // Convert iterable to iterator.
     if (!emit1(JSOP_DUP))                                         // OBJ OBJ
         return false;
     if (!emit2(JSOP_SYMBOL, uint8_t(JS::SymbolCode::asyncIterator))) // OBJ OBJ @@ASYNCITERATOR
         return false;
     if (!emitElemOpBase(JSOP_CALLELEM))                           // OBJ ITERFN
         return false;
 
-    IfThenElseEmitter ifAsyncIterIsUndefined(this);
+    IfEmitter ifAsyncIterIsUndefined(this);
     if (!emitPushNotUndefinedOrNull())                            // OBJ ITERFN !UNDEF-OR-NULL
         return false;
     if (!emit1(JSOP_NOT))                                         // OBJ ITERFN UNDEF-OR-NULL
         return false;
     if (!ifAsyncIterIsUndefined.emitThenElse())                   // OBJ ITERFN
         return false;
 
     if (!emit1(JSOP_POP))                                         // OBJ
@@ -7472,17 +7472,17 @@ BytecodeEmitter::emitForOf(ParseNode* fo
         if (!emitIteratorNext(forOfHead, iterKind, allowSelfHostedIter))
             return false;                                 // NEXT ITER RESULT
 
         if (!emit1(JSOP_DUP))                             // NEXT ITER RESULT RESULT
             return false;
         if (!emitAtomOp(cx->names().done, JSOP_GETPROP))  // NEXT ITER RESULT DONE
             return false;
 
-        IfThenElseEmitter ifDone(this);
+        IfEmitter ifDone(this);
 
         if (!ifDone.emitThen())                           // NEXT ITER RESULT
             return false;
 
         // Remove RESULT from the stack to release it.
         if (!emit1(JSOP_POP))                             // NEXT ITER
             return false;
         if (!emit1(JSOP_UNDEFINED))                       // NEXT ITER UNDEF
@@ -8666,17 +8666,17 @@ BytecodeEmitter::emitYieldStar(ParseNode
         return false;
     if (!emit1(JSOP_DUP))                                 // NEXT ITER RESULT EXCEPTION ITER THROW THROW
         return false;
     if (!emit1(JSOP_UNDEFINED))                           // NEXT ITER RESULT EXCEPTION ITER THROW THROW UNDEFINED
         return false;
     if (!emit1(JSOP_EQ))                                  // NEXT ITER RESULT EXCEPTION ITER THROW ?EQL
         return false;
 
-    IfThenElseEmitter ifThrowMethodIsNotDefined(this);
+    IfEmitter ifThrowMethodIsNotDefined(this);
     if (!ifThrowMethodIsNotDefined.emitThen())            // NEXT ITER RESULT EXCEPTION ITER THROW
         return false;
     savedDepthTemp = stackDepth;
     if (!emit1(JSOP_POP))                                 // NEXT ITER RESULT EXCEPTION ITER
         return false;
     // ES 14.4.13, YieldExpression : yield * AssignmentExpression, step 5.b.iii.2
     //
     // If the iterator does not have a "throw" method, it calls IteratorClose
@@ -8722,17 +8722,17 @@ BytecodeEmitter::emitYieldStar(ParseNode
     if (!tryCatch.emitFinally())
          return false;
 
     // ES 14.4.13, yield * AssignmentExpression, step 5.c
     //
     // Call iterator.return() for receiving a "forced return" completion from
     // the generator.
 
-    IfThenElseEmitter ifGeneratorClosing(this);
+    IfEmitter ifGeneratorClosing(this);
     if (!emit1(JSOP_ISGENCLOSING))                        // NEXT ITER RESULT FTYPE FVALUE CLOSING
         return false;
     if (!ifGeneratorClosing.emitThen())                   // NEXT ITER RESULT FTYPE FVALUE
         return false;
 
     // Step ii.
     //
     // Get the "return" method.
@@ -8741,17 +8741,17 @@ BytecodeEmitter::emitYieldStar(ParseNode
     if (!emit1(JSOP_DUP))                                 // NEXT ITER RESULT FTYPE FVALUE ITER ITER
         return false;
     if (!emitAtomOp(cx->names().return_, JSOP_CALLPROP))  // NEXT ITER RESULT FTYPE FVALUE ITER RET
         return false;
 
     // Step iii.
     //
     // Do nothing if "return" is undefined or null.
-    IfThenElseEmitter ifReturnMethodIsDefined(this);
+    IfEmitter ifReturnMethodIsDefined(this);
     if (!emitPushNotUndefinedOrNull())                    // NEXT ITER RESULT FTYPE FVALUE ITER RET NOT-UNDEF-OR-NULL
         return false;
 
     // Step iv.
     //
     // Call "return" with the argument passed to Generator.prototype.return,
     // which is currently in rval.value.
     if (!ifReturnMethodIsDefined.emitThenElse())          // NEXT ITER OLDRESULT FTYPE FVALUE ITER RET
@@ -8774,17 +8774,17 @@ BytecodeEmitter::emitYieldStar(ParseNode
     // Step v.
     if (!emitCheckIsObj(CheckIsObjectKind::IteratorReturn)) // NEXT ITER OLDRESULT FTYPE FVALUE RESULT
         return false;
 
     // Steps vi-viii.
     //
     // Check if the returned object from iterator.return() is done. If not,
     // continuing yielding.
-    IfThenElseEmitter ifReturnDone(this);
+    IfEmitter ifReturnDone(this);
     if (!emit1(JSOP_DUP))                                 // NEXT ITER OLDRESULT FTYPE FVALUE RESULT RESULT
         return false;
     if (!emitAtomOp(cx->names().done, JSOP_GETPROP))      // NEXT ITER OLDRESULT FTYPE FVALUE RESULT DONE
         return false;
     if (!ifReturnDone.emitThenElse())                     // NEXT ITER OLDRESULT FTYPE FVALUE RESULT
         return false;
     if (!emitAtomOp(cx->names().value, JSOP_GETPROP))     // NEXT ITER OLDRESULT FTYPE FVALUE VALUE
         return false;
@@ -9514,17 +9514,17 @@ BytecodeEmitter::emitCallOrNew(ParseNode
                 // Repush the callee as new.target
                 if (!emitDupAt(argc + 1))
                     return false;
             }
         }
     } else {
         ParseNode* args = pn2->pn_next;
         bool emitOptCode = (argc == 1) && isRestParameter(args->pn_kid);
-        IfThenElseEmitter ifNotOptimizable(this);
+        IfEmitter ifNotOptimizable(this);
 
         if (emitOptCode) {
             // Emit a preparation code to optimize the spread call with a rest
             // parameter:
             //
             //   function f(...args) {
             //     g(...args);
             //   }
@@ -9789,17 +9789,17 @@ BytecodeEmitter::emitLabeledStatement(co
 bool
 BytecodeEmitter::emitConditionalExpression(ConditionalExpression& conditional,
                                            ValueUsage valueUsage /* = ValueUsage::WantValue */)
 {
     /* Emit the condition, then branch if false to the else part. */
     if (!emitTree(&conditional.condition()))
         return false;
 
-    IfThenElseEmitter ifThenElse(this);
+    IfEmitter ifThenElse(this);
     if (!ifThenElse.emitCond())
         return false;
 
     if (!emitTreeInBranch(&conditional.thenExpression(), valueUsage))
         return false;
 
     if (!ifThenElse.emitElse())
         return false;
@@ -10648,17 +10648,17 @@ BytecodeEmitter::emitClass(ParseNode* pn
     //
     //   EmitPropertyList(...)
 
     // This is kind of silly. In order to the get the home object defined on
     // the constructor, we have to make it second, but we want the prototype
     // on top for EmitPropertyList, because we expect static properties to be
     // rarer. The result is a few more swaps than we would like. Such is life.
     if (heritageExpression) {
-        IfThenElseEmitter ifThenElse(this);
+        IfEmitter ifThenElse(this);
 
         if (!emitTree(heritageExpression))                      // ... HERITAGE
             return false;
 
         // Heritage must be null or a non-generator constructor
         if (!emit1(JSOP_CHECKCLASSHERITAGE))                    // ... HERITAGE
             return false;
 
