# HG changeset patch
# User Matthew Gaudet <mgaudet@mozilla.com>
# Date 1521742378 14400
#      Thu Mar 22 14:12:58 2018 -0400
# Node ID cc58162b8a3a7ba5c153c01bb211afd24b4323f9
# Parent  50d88eac66ca91857d88457c33540ba46c949730
Bug 1438727: [Part 2] Implement a subset of JSOP_SUB in CacheIR r=jandem

diff --git a/js/src/jit/BaselineIC.cpp b/js/src/jit/BaselineIC.cpp
--- a/js/src/jit/BaselineIC.cpp
+++ b/js/src/jit/BaselineIC.cpp
@@ -4947,16 +4947,17 @@ DoCacheIRBinaryArithFallback(JSContext* 
 {
     RootedScript script(cx, frame->script());
     jsbytecode* pc = stub->icEntry()->pc(script);
     JSOp op = JSOp(*pc);
 
     // Ensure we're only generating for an enabled opcode.
     switch(op) {
       case JSOP_ADD:
+      case JSOP_SUB:
         break;
       default:
         return false; // Fallback to shared IC.
     }
 
     FallbackICSpew(cx, stub, "CacheIRBinaryArith(%s,%d,%d)", CodeName[op],
             int(lhs.isDouble() ? JSVAL_TYPE_DOUBLE : lhs.extractNonDoubleType()),
             int(rhs.isDouble() ? JSVAL_TYPE_DOUBLE : rhs.extractNonDoubleType()));
diff --git a/js/src/jit/CacheIR.cpp b/js/src/jit/CacheIR.cpp
--- a/js/src/jit/CacheIR.cpp
+++ b/js/src/jit/CacheIR.cpp
@@ -5111,17 +5111,17 @@ BinaryArithIRGenerator::tryAttachStub()
 
     trackAttached(IRGenerator::NotAttached);
     return false;
 }
 
 bool
 BinaryArithIRGenerator::tryAttachDouble()
 {
-    if (op_ != JSOP_ADD)
+    if (op_ != JSOP_ADD && op_ != JSOP_SUB)
         return false;
 
     if (!lhs_.isDouble() || !rhs_.isDouble() || !res_.isDouble())
         return false;
 
     if (!cx_->runtime()->jitSupportsFloatingPoint)
         return false;
 
@@ -5131,42 +5131,50 @@ BinaryArithIRGenerator::tryAttachDouble(
     writer.guardIsNumber(lhsId);
     writer.guardIsNumber(rhsId);
 
     switch (op_) {
        case JSOP_ADD:
         writer.doubleAddResult(lhsId, rhsId);
         trackAttached("BinaryArith.Double.Add");
         break;
+      case JSOP_SUB:
+        writer.doubleSubResult(lhsId, rhsId);
+        trackAttached("BinaryArith.Double.Sub");
+        break;
       default:
         MOZ_CRASH("Unhandled Op");
     }
     writer.returnFromIC();
     return true;
 }
 
 bool
 BinaryArithIRGenerator::tryAttachInt32()
 {
-    if (op_ != JSOP_ADD)
+    if (op_ != JSOP_ADD && op_ != JSOP_SUB)
         return false;
 
     if (!lhs_.isInt32() || !rhs_.isInt32())
         return false;
 
     ValOperandId lhsId(writer.setInputOperandId(0));
     ValOperandId rhsId(writer.setInputOperandId(1));
 
     Int32OperandId lhsIntId = writer.guardIsInt32(lhsId);
     Int32OperandId rhsIntId = writer.guardIsInt32(rhsId);
 
     switch (op_) {
       case JSOP_ADD:
         writer.int32AddResult(lhsIntId, rhsIntId);
         trackAttached("BinaryArith.Int32.Add");
         break;
+      case JSOP_SUB:
+        writer.int32SubResult(lhsIntId, rhsIntId);
+        trackAttached("BinaryArith.Int32.Sub");
+        break;
       default:
         MOZ_CRASH("Unhandled op in tryAttachInt32");
     }
 
     writer.returnFromIC();
     return true;
 }
diff --git a/js/src/jit/CacheIR.h b/js/src/jit/CacheIR.h
--- a/js/src/jit/CacheIR.h
+++ b/js/src/jit/CacheIR.h
@@ -287,17 +287,19 @@ extern const char* CacheKindNames[];
     _(CallProxyHasPropResult)             \
     _(CallObjectHasSparseElementResult)   \
     _(LoadUndefinedResult)                \
     _(LoadBooleanResult)                  \
     _(LoadStringResult)                   \
     _(LoadInstanceOfObjectResult)         \
     _(LoadTypeOfObjectResult)             \
     _(DoubleAddResult)                    \
+    _(DoubleSubResult)                    \
     _(Int32AddResult)                     \
+    _(Int32SubResult)                     \
     _(Int32NotResult)                     \
     _(Int32NegationResult)                \
     _(DoubleNegationResult)               \
     _(LoadInt32TruthyResult)              \
     _(LoadDoubleTruthyResult)             \
     _(LoadStringTruthyResult)             \
     _(LoadObjectTruthyResult)             \
     _(LoadValueResult)                    \
@@ -991,20 +993,28 @@ class MOZ_RAII CacheIRWriter : public JS
         writeOperandId(id);
         buffer_.writeByte(uint32_t(hasOwn));
     }
 
     void doubleAddResult(ValOperandId lhsId, ValOperandId rhsId) {
         writeOpWithOperandId(CacheOp::DoubleAddResult, lhsId);
         writeOperandId(rhsId);
     }
+    void doubleSubResult(ValOperandId lhsId, ValOperandId rhsId) {
+        writeOpWithOperandId(CacheOp::DoubleSubResult, lhsId);
+        writeOperandId(rhsId);
+    }
     void int32AddResult(Int32OperandId lhs, Int32OperandId rhs) {
         writeOpWithOperandId(CacheOp::Int32AddResult, lhs);
         writeOperandId(rhs);
     }
+    void int32SubResult(Int32OperandId lhs, Int32OperandId rhs) {
+        writeOpWithOperandId(CacheOp::Int32SubResult, lhs);
+        writeOperandId(rhs);
+    }
     void int32NotResult(Int32OperandId id) {
         writeOpWithOperandId(CacheOp::Int32NotResult, id);
     }
     void int32NegationResult(Int32OperandId id) {
         writeOpWithOperandId(CacheOp::Int32NegationResult, id);
     }
     void doubleNegationResult(ValOperandId val) {
         writeOpWithOperandId(CacheOp::DoubleNegationResult, val);
diff --git a/js/src/jit/CacheIRCompiler.cpp b/js/src/jit/CacheIRCompiler.cpp
--- a/js/src/jit/CacheIRCompiler.cpp
+++ b/js/src/jit/CacheIRCompiler.cpp
@@ -1932,16 +1932,29 @@ CacheIRCompiler::emitDoubleAddResult()
     allocator.loadDouble(masm, reader.valOperandId(), FloatReg0);
     allocator.loadDouble(masm, reader.valOperandId(), FloatReg1);
 
     masm.addDouble(FloatReg1, FloatReg0);
     masm.boxDouble(FloatReg0, output.valueReg(), FloatReg0);
 
     return true;
 }
+bool
+CacheIRCompiler::emitDoubleSubResult()
+{
+    AutoOutputRegister output(*this);
+
+    allocator.loadDouble(masm, reader.valOperandId(), FloatReg0);
+    allocator.loadDouble(masm, reader.valOperandId(), FloatReg1);
+
+    masm.subDouble(FloatReg1, FloatReg0);
+    masm.boxDouble(FloatReg0, output.valueReg(), FloatReg0);
+
+    return true;
+}
 
 bool
 CacheIRCompiler::emitInt32AddResult()
 {
     AutoOutputRegister output(*this);
     Register lhs = allocator.useRegister(masm, reader.int32OperandId());
     Register rhs = allocator.useRegister(masm, reader.int32OperandId());
 
@@ -1949,16 +1962,32 @@ CacheIRCompiler::emitInt32AddResult()
     if (!addFailurePath(&failure))
         return false;
 
     masm.branchAdd32(Assembler::Overflow, lhs, rhs, failure->label());
     EmitStoreResult(masm, rhs, JSVAL_TYPE_INT32, output);
 
     return true;
 }
+bool
+CacheIRCompiler::emitInt32SubResult()
+{
+    AutoOutputRegister output(*this);
+    Register lhs = allocator.useRegister(masm, reader.int32OperandId());
+    Register rhs = allocator.useRegister(masm, reader.int32OperandId());
+
+    FailurePath* failure;
+    if (!addFailurePath(&failure))
+        return false;
+
+    masm.branchSub32(Assembler::Overflow, rhs, lhs, failure->label());
+    EmitStoreResult(masm, lhs, JSVAL_TYPE_INT32, output);
+
+    return true;
+}
 
 bool
 CacheIRCompiler::emitInt32NegationResult()
 {
     AutoOutputRegister output(*this);
     Register val = allocator.useRegister(masm, reader.int32OperandId());
 
     FailurePath* failure;
diff --git a/js/src/jit/CacheIRCompiler.h b/js/src/jit/CacheIRCompiler.h
--- a/js/src/jit/CacheIRCompiler.h
+++ b/js/src/jit/CacheIRCompiler.h
@@ -46,18 +46,20 @@ namespace jit {
     _(LoadEnclosingEnvironment)           \
     _(LoadWrapperTarget)                  \
     _(LoadValueTag)                       \
     _(LoadDOMExpandoValue)                \
     _(LoadDOMExpandoValueIgnoreGeneration)\
     _(LoadUndefinedResult)                \
     _(LoadBooleanResult)                  \
     _(LoadInt32ArrayLengthResult)         \
+    _(DoubleAddResult)                    \
+    _(DoubleSubResult)                    \
     _(Int32AddResult)                     \
-    _(DoubleAddResult)                    \
+    _(Int32SubResult)                     \
     _(Int32NegationResult)                \
     _(Int32NotResult)                     \
     _(DoubleNegationResult)               \
     _(TruncateDoubleToUInt32)             \
     _(LoadArgumentsObjectLengthResult)    \
     _(LoadFunctionLengthResult)           \
     _(LoadStringLengthResult)             \
     _(LoadStringCharResult)               \
diff --git a/js/src/jit/IonBuilder.cpp b/js/src/jit/IonBuilder.cpp
--- a/js/src/jit/IonBuilder.cpp
+++ b/js/src/jit/IonBuilder.cpp
@@ -3565,23 +3565,23 @@ IonBuilder::arithTrySharedStub(bool* emi
       case JSOP_NEG:
       case JSOP_BITNOT:
         MOZ_ASSERT_IF(op == JSOP_MUL,
                       left->maybeConstantValue() && left->maybeConstantValue()->toInt32() == -1);
         MOZ_ASSERT_IF(op != JSOP_MUL, !left);
         stub = MUnaryCache::New(alloc(), right);
         break;
       case JSOP_ADD:
+      case JSOP_SUB:
         // If not disabled, prefer the cache IR stub.
         if (!JitOptions.disableCacheIRBinaryArith) {
             stub = MBinaryCache::New(alloc(), left, right);
             break;
         }
         MOZ_FALLTHROUGH;
-      case JSOP_SUB:
       case JSOP_MUL:
       case JSOP_DIV:
       case JSOP_MOD:
       case JSOP_POW:
         stub = MBinarySharedStub::New(alloc(), left, right);
         break;
       default:
         MOZ_CRASH("unsupported arith");
diff --git a/js/src/jit/IonIC.cpp b/js/src/jit/IonIC.cpp
--- a/js/src/jit/IonIC.cpp
+++ b/js/src/jit/IonIC.cpp
@@ -566,16 +566,20 @@ IonBinaryArithIC::update(JSContext* cx, 
 
     // Perform the compare operation.
     switch(op) {
       case JSOP_ADD:
         // Do an add.
         if (!AddValues(cx, &lhsCopy, &rhsCopy, ret))
             return false;
         break;
+      case JSOP_SUB:
+        if (!SubValues(cx, &lhsCopy, &rhsCopy, ret))
+            return false;
+        break;
       default:
         MOZ_CRASH("Unhandled binary arith op");
     }
 
     if (ic->state().maybeTransition())
         ic->discardStubs(cx->zone());
 
     if (ic->state().canAttachStub()) {
