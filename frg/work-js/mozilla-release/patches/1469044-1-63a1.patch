# HG changeset patch
# User Matthew Gaudet <mgaudet@mozilla.com>
# Date 1531166127 14400
# Node ID 58e93d58965c4f57f20625929d33e9cafb6e0777
# Parent  13efb4f58614a7bb20ca3149b0ca50cf92c7a13d
Bug 1469044: [Part 1] Remove caching from math_function helper r=jandem

diff --git a/js/src/jsmath.cpp b/js/src/jsmath.cpp
--- a/js/src/jsmath.cpp
+++ b/js/src/jsmath.cpp
@@ -140,33 +140,29 @@ MathCache::MathCache() {
 }
 
 size_t
 MathCache::sizeOfIncludingThis(mozilla::MallocSizeOf mallocSizeOf)
 {
     return mallocSizeOf(this);
 }
 
-typedef double (*UnaryMathFunctionType)(MathCache* cache, double);
+typedef double (*UnaryMathFunctionType)(double);
 
 template <UnaryMathFunctionType F>
 static bool
 math_function(JSContext* cx, HandleValue val, MutableHandleValue res)
 {
     double x;
     if (!ToNumber(cx, val, &x))
         return false;
 
-    MathCache* mathCache = cx->caches().getMathCache(cx);
-    if (!mathCache)
-        return false;
-
     // NB: Always stored as a double so the math function can be inlined
     // through MMathFunction.
-    double z = F(mathCache, x);
+    double z = F(x);
     res.setDouble(z);
     return true;
 }
 
 template <UnaryMathFunctionType F>
 static bool
 math_function(JSContext* cx, unsigned argc, Value* vp)
 {
@@ -222,17 +218,17 @@ js::math_acos_uncached(double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return fdlibm::acos(x);
 }
 
 bool
 js::math_acos(JSContext* cx, unsigned argc, Value* vp)
 {
-    return math_function<math_acos_impl>(cx, argc, vp);
+    return math_function<math_acos_uncached>(cx, argc, vp);
 }
 
 double
 js::math_asin_impl(MathCache* cache, double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return cache->lookup(fdlibm::asin, x, MathCache::Asin);
 }
@@ -242,17 +238,17 @@ js::math_asin_uncached(double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return fdlibm::asin(x);
 }
 
 bool
 js::math_asin(JSContext* cx, unsigned argc, Value* vp)
 {
-    return math_function<math_asin_impl>(cx, argc, vp);
+    return math_function<math_asin_uncached>(cx, argc, vp);
 }
 
 double
 js::math_atan_impl(MathCache* cache, double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return cache->lookup(fdlibm::atan, x, MathCache::Atan);
 }
@@ -262,17 +258,17 @@ js::math_atan_uncached(double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return fdlibm::atan(x);
 }
 
 bool
 js::math_atan(JSContext* cx, unsigned argc, Value* vp)
 {
-    return math_function<math_atan_impl>(cx, argc, vp);
+    return math_function<math_atan_uncached>(cx, argc, vp);
 }
 
 double
 js::ecmaAtan2(double y, double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return fdlibm::atan2(y, x);
 }
@@ -368,17 +364,17 @@ js::math_cos_uncached(double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return cos(x);
 }
 
 bool
 js::math_cos(JSContext* cx, unsigned argc, Value* vp)
 {
-    return math_function<math_cos_impl>(cx, argc, vp);
+    return math_function<math_cos_uncached>(cx, argc, vp);
 }
 
 double
 js::math_exp_impl(MathCache* cache, double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return cache->lookup(fdlibm::exp, x, MathCache::Exp);
 }
@@ -388,17 +384,17 @@ js::math_exp_uncached(double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return fdlibm::exp(x);
 }
 
 bool
 js::math_exp(JSContext* cx, unsigned argc, Value* vp)
 {
-    return math_function<math_exp_impl>(cx, argc, vp);
+    return math_function<math_exp_uncached>(cx, argc, vp);
 }
 
 double
 js::math_floor_impl(double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return fdlibm::floor(x);
 }
@@ -496,23 +492,23 @@ js::math_log_uncached(double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return fdlibm::log(x);
 }
 
 bool
 js::math_log_handle(JSContext* cx, HandleValue val, MutableHandleValue res)
 {
-    return math_function<math_log_impl>(cx, val, res);
+    return math_function<math_log_uncached>(cx, val, res);
 }
 
 bool
 js::math_log(JSContext* cx, unsigned argc, Value* vp)
 {
-    return math_function<math_log_impl>(cx, argc, vp);
+    return math_function<math_log_uncached>(cx, argc, vp);
 }
 
 double
 js::math_max_impl(double x, double y)
 {
     AutoUnsafeCallWithABI unsafe;
 
     // Math.max(num, NaN) => NaN, Math.max(-0, +0) => +0
@@ -822,23 +818,23 @@ js::math_sin_uncached(double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return sin(x);
 }
 
 bool
 js::math_sin_handle(JSContext* cx, HandleValue val, MutableHandleValue res)
 {
-    return math_function<math_sin_impl>(cx, val, res);
+    return math_function<math_sin_uncached>(cx, val, res);
 }
 
 bool
 js::math_sin(JSContext* cx, unsigned argc, Value* vp)
 {
-    return math_function<math_sin_impl>(cx, argc, vp);
+    return math_function<math_sin_uncached>(cx, argc, vp);
 }
 
 void
 js::math_sincos_uncached(double x, double *sin, double *cos)
 {
     AutoUnsafeCallWithABI unsafe;
 #if defined(HAVE_SINCOS)
     sincos(x, sin, cos);
@@ -868,32 +864,39 @@ js::math_sincos_impl(MathCache* mathCach
     if (!hasSin)
         *sin = js::math_sin_impl(mathCache, x);
 
     if (!hasCos)
         *cos = js::math_cos_impl(mathCache, x);
 }
 
 double
+js::math_sqrt_uncached(double x)
+{
+    AutoUnsafeCallWithABI unsafe;
+    return sqrt(x);
+}
+
+double
 js::math_sqrt_impl(MathCache* cache, double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return cache->lookup(sqrt, x, MathCache::Sqrt);
 }
 
 bool
 js::math_sqrt_handle(JSContext* cx, HandleValue number, MutableHandleValue result)
 {
-    return math_function<math_sqrt_impl>(cx, number, result);
+    return math_function<math_sqrt_uncached>(cx, number, result);
 }
 
 bool
 js::math_sqrt(JSContext* cx, unsigned argc, Value* vp)
 {
-    return math_function<math_sqrt_impl>(cx, argc, vp);
+    return math_function<math_sqrt_uncached>(cx, argc, vp);
 }
 
 double
 js::math_tan_impl(MathCache* cache, double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return cache->lookup(tan, x, MathCache::Tan);
 }
@@ -903,17 +906,17 @@ js::math_tan_uncached(double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return tan(x);
 }
 
 bool
 js::math_tan(JSContext* cx, unsigned argc, Value* vp)
 {
-    return math_function<math_tan_impl>(cx, argc, vp);
+    return math_function<math_tan_uncached>(cx, argc, vp);
 }
 
 double
 js::math_log10_impl(MathCache* cache, double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return cache->lookup(fdlibm::log10, x, MathCache::Log10);
 }
@@ -923,17 +926,17 @@ js::math_log10_uncached(double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return fdlibm::log10(x);
 }
 
 bool
 js::math_log10(JSContext* cx, unsigned argc, Value* vp)
 {
-    return math_function<math_log10_impl>(cx, argc, vp);
+    return math_function<math_log10_uncached>(cx, argc, vp);
 }
 
 double
 js::math_log2_impl(MathCache* cache, double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return cache->lookup(fdlibm::log2, x, MathCache::Log2);
 }
@@ -943,17 +946,17 @@ js::math_log2_uncached(double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return fdlibm::log2(x);
 }
 
 bool
 js::math_log2(JSContext* cx, unsigned argc, Value* vp)
 {
-    return math_function<math_log2_impl>(cx, argc, vp);
+    return math_function<math_log2_uncached>(cx, argc, vp);
 }
 
 double
 js::math_log1p_impl(MathCache* cache, double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return cache->lookup(fdlibm::log1p, x, MathCache::Log1p);
 }
@@ -963,17 +966,17 @@ js::math_log1p_uncached(double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return fdlibm::log1p(x);
 }
 
 bool
 js::math_log1p(JSContext* cx, unsigned argc, Value* vp)
 {
-    return math_function<math_log1p_impl>(cx, argc, vp);
+    return math_function<math_log1p_uncached>(cx, argc, vp);
 }
 
 double
 js::math_expm1_impl(MathCache* cache, double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return cache->lookup(fdlibm::expm1, x, MathCache::Expm1);
 }
@@ -983,17 +986,17 @@ js::math_expm1_uncached(double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return fdlibm::expm1(x);
 }
 
 bool
 js::math_expm1(JSContext* cx, unsigned argc, Value* vp)
 {
-    return math_function<math_expm1_impl>(cx, argc, vp);
+    return math_function<math_expm1_uncached>(cx, argc, vp);
 }
 
 double
 js::math_cosh_impl(MathCache* cache, double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return cache->lookup(fdlibm::cosh, x, MathCache::Cosh);
 }
@@ -1003,17 +1006,17 @@ js::math_cosh_uncached(double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return fdlibm::cosh(x);
 }
 
 bool
 js::math_cosh(JSContext* cx, unsigned argc, Value* vp)
 {
-    return math_function<math_cosh_impl>(cx, argc, vp);
+    return math_function<math_cosh_uncached>(cx, argc, vp);
 }
 
 double
 js::math_sinh_impl(MathCache* cache, double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return cache->lookup(fdlibm::sinh, x, MathCache::Sinh);
 }
@@ -1023,17 +1026,17 @@ js::math_sinh_uncached(double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return fdlibm::sinh(x);
 }
 
 bool
 js::math_sinh(JSContext* cx, unsigned argc, Value* vp)
 {
-    return math_function<math_sinh_impl>(cx, argc, vp);
+    return math_function<math_sinh_uncached>(cx, argc, vp);
 }
 
 double
 js::math_tanh_impl(MathCache* cache, double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return cache->lookup(fdlibm::tanh, x, MathCache::Tanh);
 }
@@ -1043,17 +1046,17 @@ js::math_tanh_uncached(double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return fdlibm::tanh(x);
 }
 
 bool
 js::math_tanh(JSContext* cx, unsigned argc, Value* vp)
 {
-    return math_function<math_tanh_impl>(cx, argc, vp);
+    return math_function<math_tanh_uncached>(cx, argc, vp);
 }
 
 double
 js::math_acosh_impl(MathCache* cache, double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return cache->lookup(fdlibm::acosh, x, MathCache::Acosh);
 }
@@ -1063,17 +1066,17 @@ js::math_acosh_uncached(double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return fdlibm::acosh(x);
 }
 
 bool
 js::math_acosh(JSContext* cx, unsigned argc, Value* vp)
 {
-    return math_function<math_acosh_impl>(cx, argc, vp);
+    return math_function<math_acosh_uncached>(cx, argc, vp);
 }
 
 double
 js::math_asinh_impl(MathCache* cache, double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return cache->lookup(fdlibm::asinh, x, MathCache::Asinh);
 }
@@ -1083,17 +1086,17 @@ js::math_asinh_uncached(double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return fdlibm::asinh(x);
 }
 
 bool
 js::math_asinh(JSContext* cx, unsigned argc, Value* vp)
 {
-    return math_function<math_asinh_impl>(cx, argc, vp);
+    return math_function<math_asinh_uncached>(cx, argc, vp);
 }
 
 double
 js::math_atanh_impl(MathCache* cache, double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return cache->lookup(fdlibm::atanh, x, MathCache::Atanh);
 }
@@ -1103,17 +1106,17 @@ js::math_atanh_uncached(double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return fdlibm::atanh(x);
 }
 
 bool
 js::math_atanh(JSContext* cx, unsigned argc, Value* vp)
 {
-    return math_function<math_atanh_impl>(cx, argc, vp);
+    return math_function<math_atanh_uncached>(cx, argc, vp);
 }
 
 double
 js::ecmaHypot(double x, double y)
 {
     AutoUnsafeCallWithABI unsafe;
     return fdlibm::hypot(x, y);
 }
@@ -1297,17 +1300,17 @@ js::math_cbrt_uncached(double x)
 {
     AutoUnsafeCallWithABI unsafe;
     return fdlibm::cbrt(x);
 }
 
 bool
 js::math_cbrt(JSContext* cx, unsigned argc, Value* vp)
 {
-    return math_function<math_cbrt_impl>(cx, argc, vp);
+    return math_function<math_cbrt_uncached>(cx, argc, vp);
 }
 
 static bool
 math_toSource(JSContext* cx, unsigned argc, Value* vp)
 {
     CallArgs args = CallArgsFromVp(argc, vp);
     args.rval().setString(cx->names().Math);
     return true;
diff --git a/js/src/jsmath.h b/js/src/jsmath.h
--- a/js/src/jsmath.h
+++ b/js/src/jsmath.h
@@ -119,16 +119,19 @@ math_max(JSContext* cx, unsigned argc, j
 
 extern double
 math_min_impl(double x, double y);
 
 extern bool
 math_min(JSContext* cx, unsigned argc, js::Value* vp);
 
 extern double
+math_sqrt_uncached(double x);
+
+extern double
 math_sqrt_impl(MathCache* cache, double x);
 
 extern bool
 math_sqrt_handle(JSContext* cx, js::HandleValue number, js::MutableHandleValue result);
 
 extern bool
 math_sqrt(JSContext* cx, unsigned argc, js::Value* vp);
 

