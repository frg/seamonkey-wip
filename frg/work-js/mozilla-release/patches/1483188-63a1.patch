# HG changeset patch
# User Tooru Fujisawa <arai_a@mac.com>
# Date 1534899694 -32400
#      Wed Aug 22 10:01:34 2018 +0900
# Node ID 569397778fcde1ff2243e7d83a263ce2f51674f0
# Parent  8c6a05c61d397cc70412052f36670b2c452c1ec1
Bug 1483188 - Fix the assertion for reaction record fields. r=anba

diff --git a/js/src/builtin/Promise.cpp b/js/src/builtin/Promise.cpp
--- a/js/src/builtin/Promise.cpp
+++ b/js/src/builtin/Promise.cpp
@@ -2557,20 +2557,21 @@ CommonPerformPromiseAllRace(JSContext *c
                     JS_ReportErrorNumberASCII(cx, GetErrorMessage, nullptr, JSMSG_DEAD_OBJECT);
                     return false;
                 }
                 ar.emplace(cx, nextPromiseObj);
                 if (!cx->compartment()->wrap(cx, &blockedPromise))
                     return false;
             }
 
-            // If either the object to depend on or the object that gets
-            // blocked isn't a, maybe-wrapped, Promise instance, we ignore it.
-            // All this does is lose some small amount of debug information in
-            // scenarios that are highly unlikely to occur in useful code.
+            // If either the object to depend on (`nextPromiseObj`) or the
+            // object that gets blocked (`resultPromise`) isn't a,
+            // maybe-wrapped, Promise instance, we ignore it. All this does is
+            // lose some small amount of debug information in scenarios that
+            // are highly unlikely to occur in useful code.
             if (nextPromiseObj->is<PromiseObject>() && resultPromise->is<PromiseObject>()) {
                 Handle<PromiseObject*> promise = nextPromiseObj.as<PromiseObject>();
                 if (!AddDummyPromiseReactionForDebugger(cx, promise, blockedPromise))
                     return false;
             }
         }
     }
 }
@@ -2994,34 +2995,68 @@ Promise_static_species(JSContext* cx, un
     // Step 1: Return the this value.
     args.rval().set(args.thisv());
     return true;
 }
 
 // ES2016, 25.4.5.1, implemented in Promise.js.
 
 enum class IncumbentGlobalObject {
-    Yes, No
+    // Do not use the incumbent global, this is a special case used by the
+    // debugger.
+    No,
+
+    // Use incumbent global, this is the normal operation.
+    Yes
 };
 
 static PromiseReactionRecord*
 NewReactionRecord(JSContext* cx, Handle<PromiseCapability> resultCapability,
                   HandleValue onFulfilled, HandleValue onRejected,
                   IncumbentGlobalObject incumbentGlobalObjectOption)
 {
-    // Either of the following conditions must be met:
-    //   * resultCapability.promise is a PromiseObject
-    //   * resultCapability.resolve and resultCapability.resolve are callable
-    // except for Async Generator, there resultPromise can be nullptr.
 #ifdef DEBUG
-    if (resultCapability.promise() && !resultCapability.promise()->is<PromiseObject>()) {
-        MOZ_ASSERT(resultCapability.resolve());
-        MOZ_ASSERT(IsCallable(resultCapability.resolve()));
-        MOZ_ASSERT(resultCapability.reject());
-        MOZ_ASSERT(IsCallable(resultCapability.reject()));
+    if (resultCapability.promise()) {
+        if (incumbentGlobalObjectOption == IncumbentGlobalObject::Yes) {
+            if (resultCapability.promise()->is<PromiseObject>()) {
+                // If `resultCapability.promise` is a Promise object,
+                // `resultCapability.{resolve,reject}` may be optimized out,
+                // but if they're not, they should be callable.
+                MOZ_ASSERT_IF(resultCapability.resolve(),
+                              IsCallable(resultCapability.resolve()));
+                MOZ_ASSERT_IF(resultCapability.reject(),
+                              IsCallable(resultCapability.reject()));
+            } else {
+                // If `resultCapability.promise` is a non-Promise object
+                // (including wrapped Promise object),
+                // `resultCapability.{resolve,reject}` should be callable.
+                MOZ_ASSERT(resultCapability.resolve());
+                MOZ_ASSERT(IsCallable(resultCapability.resolve()));
+                MOZ_ASSERT(resultCapability.reject());
+                MOZ_ASSERT(IsCallable(resultCapability.reject()));
+            }
+        } else {
+            // For debugger usage, `resultCapability.promise` should be a
+            // maybe-wrapped Promise object. The other fields are not used.
+            //
+            // This is the only case where we allow `resolve` and `reject` to
+            // be null when the `promise` field is not a PromiseObject.
+            JSObject* unwrappedPromise = UncheckedUnwrap(resultCapability.promise());
+            MOZ_ASSERT(unwrappedPromise->is<PromiseObject>());
+            MOZ_ASSERT(!resultCapability.resolve());
+            MOZ_ASSERT(!resultCapability.reject());
+        }
+    } else {
+        // `resultCapability.promise` is null for the following cases:
+        //   * resulting Promise is known to be unused
+        //   * Async Generator
+        // In any case, other fields are also not used.
+        MOZ_ASSERT(!resultCapability.resolve());
+        MOZ_ASSERT(!resultCapability.reject());
+        MOZ_ASSERT(incumbentGlobalObjectOption == IncumbentGlobalObject::Yes);
     }
 #endif
 
     // Ensure the onFulfilled handler has the expected type.
     MOZ_ASSERT(onFulfilled.isInt32() || onFulfilled.isObjectOrNull());
     MOZ_ASSERT_IF(onFulfilled.isObject(), IsCallable(onFulfilled));
     MOZ_ASSERT_IF(onFulfilled.isInt32(),
                   0 <= onFulfilled.toInt32() && onFulfilled.toInt32() < PromiseHandlerLimit);
@@ -4013,16 +4048,19 @@ AddPromiseReaction(JSContext* cx, Handle
 
 static MOZ_MUST_USE bool
 AddDummyPromiseReactionForDebugger(JSContext* cx, Handle<PromiseObject*> promise,
                                    HandleObject dependentPromise)
 {
     if (promise->state() != JS::PromiseState::Pending)
         return true;
 
+    // `dependentPromise` should be a maybe-wrapped Promise.
+    MOZ_ASSERT(UncheckedUnwrap(dependentPromise)->is<PromiseObject>());
+
     // Leave resolve and reject as null.
     Rooted<PromiseCapability> capability(cx);
     capability.promise().set(dependentPromise);
 
     Rooted<PromiseReactionRecord*> reaction(cx, NewReactionRecord(cx, capability,
                                                                   NullHandleValue, NullHandleValue,
                                                                   IncumbentGlobalObject::No));
     if (!reaction)
diff --git a/js/src/jit-test/tests/auto-regress/bug1483188.js b/js/src/jit-test/tests/auto-regress/bug1483188.js
new file mode 100644
--- /dev/null
+++ b/js/src/jit-test/tests/auto-regress/bug1483188.js
@@ -0,0 +1,13 @@
+var P = newGlobal().eval(`
+(class extends Promise {
+    static resolve(o) {
+        return o;
+    }
+});
+`);
+var alwaysPending = new Promise(() => {});
+function neverCalled() {
+    assertEq(true, false);
+}
+P.race([alwaysPending]).then(neverCalled, neverCalled);
+
