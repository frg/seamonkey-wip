# HG changeset patch
# User Frank-Rainer Grahl <frgrahl@gmx.net>
# Date 1690631530 -7200
# Parent  47e6b371b658c6572f0b641fc14d52d972ab2ec6
No Bug - Switch to new regexp V8 engine additional fixes. r=frg a=frg

Fix x86 build and do not require turning on c++17 right now.

diff --git a/js/src/irregexp/RegExpAPI.cpp b/js/src/irregexp/RegExpAPI.cpp
--- a/js/src/irregexp/RegExpAPI.cpp
+++ b/js/src/irregexp/RegExpAPI.cpp
@@ -628,21 +628,21 @@ bool CompilePattern(JSContext* cx, Mutab
 
 template <typename CharT>
 RegExpRunStatus ExecuteRaw(jit::JitCode* code, const CharT* chars,
                            size_t length, size_t startIndex,
                            VectorMatchPairs* matches) {
   InputOutputData data(chars, chars + length, startIndex, matches);
 
   static_assert(RegExpRunStatus_Error ==
-                v8::internal::RegExp::kInternalRegExpException);
+                v8::internal::RegExp::kInternalRegExpException, "e1");
   static_assert(RegExpRunStatus_Success ==
-                v8::internal::RegExp::kInternalRegExpSuccess);
+                v8::internal::RegExp::kInternalRegExpSuccess, "e2");
   static_assert(RegExpRunStatus_Success_NotFound ==
-                v8::internal::RegExp::kInternalRegExpFailure);
+                v8::internal::RegExp::kInternalRegExpFailure, "e3");
 
   typedef int (*RegExpCodeSignature)(InputOutputData*);
   auto function = reinterpret_cast<RegExpCodeSignature>(code->raw());
   {
     JS::AutoSuppressGCAnalysis nogc;
     return (RegExpRunStatus)CALL_GENERATED_1(function, &data);
   }
 }
@@ -652,21 +652,21 @@ RegExpRunStatus Interpret(JSContext* cx,
                           VectorMatchPairs* matches) {
   MOZ_ASSERT(re->getByteCode(input->hasLatin1Chars()));
 
   HandleScope handleScope(cx->isolate);
   V8HandleRegExp wrappedRegExp(v8::internal::JSRegExp(re), cx->isolate);
   V8HandleString wrappedInput(v8::internal::String(input), cx->isolate);
 
   static_assert(RegExpRunStatus_Error ==
-                v8::internal::RegExp::kInternalRegExpException);
+                v8::internal::RegExp::kInternalRegExpException, "e4");
   static_assert(RegExpRunStatus_Success ==
-                v8::internal::RegExp::kInternalRegExpSuccess);
+                v8::internal::RegExp::kInternalRegExpSuccess, "e5");
   static_assert(RegExpRunStatus_Success_NotFound ==
-                v8::internal::RegExp::kInternalRegExpFailure);
+                v8::internal::RegExp::kInternalRegExpFailure, "e6");
 
   RegExpRunStatus status =
       (RegExpRunStatus)IrregexpInterpreter::MatchForCallFromRuntime(
           cx->isolate, wrappedRegExp, wrappedInput, matches->pairsRaw(),
           matches->pairCount() * 2, startIndex);
 
   MOZ_ASSERT(status == RegExpRunStatus_Error ||
              status == RegExpRunStatus_Success ||
diff --git a/js/src/irregexp/RegExpShim.cpp b/js/src/irregexp/RegExpShim.cpp
--- a/js/src/irregexp/RegExpShim.cpp
+++ b/js/src/irregexp/RegExpShim.cpp
@@ -239,17 +239,17 @@ Handle<String> Isolate::InternalizeStrin
 }
 
 template Handle<String> Isolate::InternalizeString(
     const Vector<const uint8_t>& str);
 template Handle<String> Isolate::InternalizeString(
     const Vector<const char16_t>& str);
 
 static_assert(JSRegExp::RegistersForCaptureCount(JSRegExp::kMaxCaptures) <=
-              RegExpMacroAssembler::kMaxRegisterCount);
+              RegExpMacroAssembler::kMaxRegisterCount, "e1");
 
 bool FLAG_trace_regexp_assembler = false;
 bool FLAG_trace_regexp_bytecodes = false;
 bool FLAG_trace_regexp_parser = false;
 bool FLAG_trace_regexp_peephole_optimization = false;
 
 }  // namespace internal
 }  // namespace v8
diff --git a/js/src/irregexp/RegExpShim.h b/js/src/irregexp/RegExpShim.h
--- a/js/src/irregexp/RegExpShim.h
+++ b/js/src/irregexp/RegExpShim.h
@@ -43,18 +43,28 @@ class Isolate;
 class RegExpMatchInfo;
 class RegExpStack;
 
 }  // namespace internal
 }  // namespace v8
 
 #define V8_WARN_UNUSED_RESULT MOZ_MUST_USE
 #define V8_EXPORT_PRIVATE MOZ_EXPORT
-#define V8_FALLTHROUGH [[fallthrough]]
+#define V8_FALLTHROUGH MOZ_FALLTHROUGH
+
+// Detect support for C++17 [[nodiscard]]
+#if !defined(__has_cpp_attribute)
+#define __has_cpp_attribute(name) 0
+#endif  // !defined(__has_cpp_attribute)
+
+#if __has_cpp_attribute(nodiscard)
 #define V8_NODISCARD [[nodiscard]]
+#else
+#define V8_NODISCARD
+#endif  // __has_cpp_attribute(nodiscard)
 
 #define FATAL(x) MOZ_CRASH(x)
 #define UNREACHABLE() MOZ_CRASH("unreachable code")
 #define UNIMPLEMENTED() MOZ_CRASH("unimplemented code")
 #define STATIC_ASSERT(exp) static_assert(exp, #exp)
 
 #define DCHECK MOZ_ASSERT
 #define DCHECK_EQ(lhs, rhs) MOZ_ASSERT((lhs) == (rhs))
diff --git a/js/src/irregexp/util/ZoneShim.h b/js/src/irregexp/util/ZoneShim.h
--- a/js/src/irregexp/util/ZoneShim.h
+++ b/js/src/irregexp/util/ZoneShim.h
@@ -241,17 +241,18 @@ class ZoneList final : public ZoneObject
     T temp = element;
     Resize(new_capacity, zone);
     data_[length_++] = temp;
   }
 
   // Resize the list.
   void Resize(int new_capacity, Zone* zone) {
     MOZ_ASSERT(length_ <= new_capacity);
-    static_assert(std::is_trivially_copyable<T>::value);
+    static_assert(std::is_trivially_copyable<T>::value,
+                  "std::is_trivially_copyable<T>::value");
     T* new_data = zone->NewArray<T>(new_capacity);
     if (length_ > 0) {
       memcpy(new_data, data_, length_ * sizeof(T));
     }
     data_ = new_data;
     capacity_ = new_capacity;
   }
 
diff --git a/js/src/vm/RegExpShared.h b/js/src/vm/RegExpShared.h
--- a/js/src/vm/RegExpShared.h
+++ b/js/src/vm/RegExpShared.h
@@ -130,16 +130,19 @@ class RegExpShared : public gc::TenuredC
   uint32_t numNamedCaptures_ = {};
   uint32_t* namedCaptureIndices_ = {};
   GCPtr<PlainObject*> groupsTemplate_ = {};
 
     size_t             pairCount_;
 
     RegExpCompilation  compilationArray[2];
 
+  // Make x86 build happy.
+  size_t             dummy_;
+
     static int CompilationIndex(bool latin1) { return latin1 ? 0 : 1; }
 
     // Tables referenced by JIT code.
     JitCodeTables tables;
 
     /* Internal functions. */
     RegExpShared(JSAtom* source, JS::RegExpFlags flags);
 
