# HG changeset patch
# User Matthew Gaudet <mgaudet@mozilla.com>
# Date 1524592947 14400
#      Tue Apr 24 14:02:27 2018 -0400
# Node ID b96020a79cfaae592a5fcd3c870db533afd2d729
# Parent  6b709b53ff34781cea1ff6b941aafe4a2d60b359
Bug 1437842: [Part 13] Convert IsSharedArrayBuffer to GuardToSharedArrayBuffer r=jandem

diff --git a/js/src/builtin/TypedArray.js b/js/src/builtin/TypedArray.js
--- a/js/src/builtin/TypedArray.js
+++ b/js/src/builtin/TypedArray.js
@@ -1,37 +1,37 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 function ViewedArrayBufferIfReified(tarray) {
     assert(IsTypedArray(tarray), "non-typed array asked for its buffer");
 
     var buf = UnsafeGetReservedSlot(tarray, JS_TYPEDARRAYLAYOUT_BUFFER_SLOT);
-    assert(buf === null || (IsObject(buf) && (GuardToArrayBuffer(buf) !== null || IsSharedArrayBuffer(buf))),
+    assert(buf === null || (IsObject(buf) && (GuardToArrayBuffer(buf) !== null || GuardToSharedArrayBuffer(buf) !== null)),
            "unexpected value in buffer slot");
     return buf;
 }
 
 function IsDetachedBuffer(buffer) {
     // A typed array with a null buffer has never had its buffer exposed to
     // become detached.
     if (buffer === null)
         return false;
 
-    assert(GuardToArrayBuffer(buffer) !== null || IsSharedArrayBuffer(buffer),
+    assert(GuardToArrayBuffer(buffer) !== null || GuardToSharedArrayBuffer(buffer) !== null,
            "non-ArrayBuffer passed to IsDetachedBuffer");
 
     // Shared array buffers are not detachable.
     //
     // This check is more expensive than desirable, but IsDetachedBuffer is
     // only hot for non-shared memory in SetFromNonTypedArray, so there is an
     // optimization in place there to avoid incurring the cost here.  An
     // alternative is to give SharedArrayBuffer the same layout as ArrayBuffer.
-    if (IsSharedArrayBuffer(buffer))
+    if ((buffer = GuardToArrayBuffer(buffer)) === null)
         return false;
 
     var flags = UnsafeGetInt32FromReservedSlot(buffer, JS_ARRAYBUFFER_FLAGS_SLOT);
     return (flags & JS_ARRAYBUFFER_DETACHED_FLAG) !== 0;
 }
 
 function TypedArrayLengthMethod() {
     return TypedArrayLength(this);
@@ -886,17 +886,18 @@ function SetFromNonTypedArray(target, ar
         ThrowRangeError(JSMSG_BAD_INDEX);
 
     // Step 22.
     var k = 0;
 
     // Optimization: if the buffer is shared then it is not detachable
     // and also not inline, so avoid checking overhead inside the loop in
     // that case.
-    var isShared = targetBuffer !== null && IsSharedArrayBuffer(targetBuffer);
+    var isShared = targetBuffer !== null
+                   && (targetBuffer = GuardToSharedArrayBuffer(targetBuffer)) !== null;
 
     // Steps 12-15, 21, 23-24.
     while (targetOffset < limitOffset) {
         // Steps 24a-c.
         var kNumber = ToNumber(src[k]);
 
         // Step 24d.  This explicit check will be unnecessary when we implement
         // throw-on-getting/setting-element-in-detached-buffer semantics.
@@ -1704,18 +1705,18 @@ function SharedArrayBufferSpecies() {
 // Shared memory and atomics proposal 6.2.1.5.3 (30 Oct 2016)
 // http://tc39.github.io/ecmascript_sharedmem/shmem.html
 function SharedArrayBufferSlice(start, end) {
     // Step 1.
     var O = this;
 
     // Steps 2-4,
     // This function is not generic.
-    if (!IsObject(O) || !IsSharedArrayBuffer(O)) {
-        return callFunction(CallSharedArrayBufferMethodIfWrapped, O, start, end,
+    if (!IsObject(O) || (O = GuardToSharedArrayBuffer(O)) === null) {
+        return callFunction(CallSharedArrayBufferMethodIfWrapped, this, start, end,
                             "SharedArrayBufferSlice");
     }
 
     // Step 5.
     var len = SharedArrayBufferByteLength(O);
 
     // Step 6.
     var relativeStart = ToInteger(start);
@@ -1738,33 +1739,35 @@ function SharedArrayBufferSlice(start, e
     // Step 11
     var ctor = SpeciesConstructor(O, GetBuiltinConstructor("SharedArrayBuffer"));
 
     // Step 12.
     var new_ = new ctor(newLen);
 
     // Step 13.
     var isWrapped = false;
-    if (!IsSharedArrayBuffer(new_)) {
+    var newObj;
+    if ((newObj = GuardToSharedArrayBuffer(new_)) === null) {
         if (!IsWrappedSharedArrayBuffer(new_))
             ThrowTypeError(JSMSG_NON_SHARED_ARRAY_BUFFER_RETURNED);
         isWrapped = true;
+        newObj = new_;
     }
 
     // Step 14.
-    if (new_ === O)
+    if (newObj === O)
         ThrowTypeError(JSMSG_SAME_SHARED_ARRAY_BUFFER_RETURNED);
 
     // Steb 14b.
-    if (SharedArrayBuffersMemorySame(new_, O))
+    if (SharedArrayBuffersMemorySame(newObj, O))
         ThrowTypeError(JSMSG_SAME_SHARED_ARRAY_BUFFER_RETURNED);
 
     // Step 15.
-    var actualLen = PossiblyWrappedSharedArrayBufferByteLength(new_);
+    var actualLen = PossiblyWrappedSharedArrayBufferByteLength(newObj);
     if (actualLen < newLen)
         ThrowTypeError(JSMSG_SHORT_SHARED_ARRAY_BUFFER_RETURNED, newLen, actualLen);
 
     // Steps 16-18.
-    SharedArrayBufferCopyData(new_, 0, O, first | 0, newLen | 0, isWrapped);
+    SharedArrayBufferCopyData(newObj, 0, O, first | 0, newLen | 0, isWrapped);
 
     // Step 19.
     return new_;
 }
diff --git a/js/src/jit/InlinableNatives.h b/js/src/jit/InlinableNatives.h
--- a/js/src/jit/InlinableNatives.h
+++ b/js/src/jit/InlinableNatives.h
@@ -146,17 +146,17 @@
                                     \
     _(IntrinsicNewArrayIterator)    \
     _(IntrinsicNewStringIterator)   \
                                     \
     _(IntrinsicGuardToArrayBuffer)  \
     _(IntrinsicArrayBufferByteLength) \
     _(IntrinsicPossiblyWrappedArrayBufferByteLength) \
                                     \
-    _(IntrinsicIsSharedArrayBuffer) \
+    _(IntrinsicGuardToSharedArrayBuffer) \
                                     \
     _(TypedArrayConstructor)        \
     _(IntrinsicIsTypedArray)        \
     _(IntrinsicIsPossiblyWrappedTypedArray) \
     _(IntrinsicTypedArrayLength)    \
     _(IntrinsicPossiblyWrappedTypedArrayLength)    \
     _(IntrinsicSetDisjointTypedElements) \
                                     \
diff --git a/js/src/jit/MCallOptimize.cpp b/js/src/jit/MCallOptimize.cpp
--- a/js/src/jit/MCallOptimize.cpp
+++ b/js/src/jit/MCallOptimize.cpp
@@ -352,18 +352,18 @@ IonBuilder::inlineNativeCall(CallInfo& c
       case InlinableNative::IntrinsicGuardToArrayBuffer:
         return inlineGuardToClass(callInfo, &ArrayBufferObject::class_);
       case InlinableNative::IntrinsicArrayBufferByteLength:
         return inlineArrayBufferByteLength(callInfo);
       case InlinableNative::IntrinsicPossiblyWrappedArrayBufferByteLength:
         return inlinePossiblyWrappedArrayBufferByteLength(callInfo);
 
       // SharedArrayBuffer intrinsics.
-      case InlinableNative::IntrinsicIsSharedArrayBuffer:
-        return inlineHasClass(callInfo, &SharedArrayBufferObject::class_);
+      case InlinableNative::IntrinsicGuardToSharedArrayBuffer:
+        return inlineGuardToClass(callInfo, &SharedArrayBufferObject::class_);
 
       // TypedArray intrinsics.
       case InlinableNative::TypedArrayConstructor:
         return inlineTypedArray(callInfo, target->native());
       case InlinableNative::IntrinsicIsTypedArray:
         return inlineIsTypedArray(callInfo);
       case InlinableNative::IntrinsicIsPossiblyWrappedTypedArray:
         return inlineIsPossiblyWrappedTypedArray(callInfo);
diff --git a/js/src/vm/SelfHosting.cpp b/js/src/vm/SelfHosting.cpp
--- a/js/src/vm/SelfHosting.cpp
+++ b/js/src/vm/SelfHosting.cpp
@@ -2422,19 +2422,19 @@ static const JSFunctionSpec intrinsic_fu
     JS_FN("IsSuspendedGenerator",    intrinsic_IsSuspendedGenerator,    1,0),
 
     JS_FN("GeneratorIsRunning",      intrinsic_GeneratorIsRunning,      1,0),
     JS_FN("GeneratorSetClosed",      intrinsic_GeneratorSetClosed,      1,0),
 
     JS_INLINABLE_FN("GuardToArrayBuffer",
           intrinsic_GuardToBuiltin<ArrayBufferObject>,             1,0,
           IntrinsicGuardToArrayBuffer),
-    JS_INLINABLE_FN("IsSharedArrayBuffer",
-          intrinsic_IsInstanceOfBuiltin<SharedArrayBufferObject>,       1,0,
-          IntrinsicIsSharedArrayBuffer),
+    JS_INLINABLE_FN("GuardToSharedArrayBuffer",
+          intrinsic_GuardToBuiltin<SharedArrayBufferObject>,       1,0,
+          IntrinsicGuardToSharedArrayBuffer),
     JS_FN("IsWrappedArrayBuffer",
           intrinsic_IsWrappedInstanceOfBuiltin<ArrayBufferObject>,      1,0),
     JS_FN("IsWrappedSharedArrayBuffer",
           intrinsic_IsWrappedInstanceOfBuiltin<SharedArrayBufferObject>, 1,0),
 
     JS_INLINABLE_FN("ArrayBufferByteLength",
                     intrinsic_ArrayBufferByteLength<ArrayBufferObject>, 1,0,
                     IntrinsicArrayBufferByteLength),
