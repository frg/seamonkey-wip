# HG changeset patch
# User Nathan Froyd <froydnj@mozilla.com>
# Date 1556046917 0
#      Tue Apr 23 19:15:17 2019 +0000
# Node ID 473cfd3b34305a44dccb75fc24b1bf15ab772d20
# Parent  0c8058d19e04d79cc7b78a8a8f616f10b66c33b3
Bug 1545502 - move fuzzing options to toolchain.configure; r=firefox-build-system-reviewers,chmanchester

We moved fuzzing options out of `toolkit/moz.configure` into `js`'s
configure a while back, but we seem to have snuck some fuzzing-related
options into `toolchain.configure` in the interim.  But we can't make
the `toolchain.configure` bits depend on the `js` bits; let's just put
everything in `toolchain.configure`.

Differential Revision: https://phabricator.services.mozilla.com/D28084

diff --git a/build/moz.configure/toolchain.configure b/build/moz.configure/toolchain.configure
--- a/build/moz.configure/toolchain.configure
+++ b/build/moz.configure/toolchain.configure
@@ -2204,17 +2204,61 @@ add_gcc_flag(
     when=libstdcxx_version(
         'MOZ_LIBSTDCXX_TARGET_VERSION', cxx_compiler))
 add_gcc_flag(
     '-D_GLIBCXX_USE_CXX11_ABI=0', host_cxx_compiler,
     when=libstdcxx_version(
         'MOZ_LIBSTDCXX_HOST_VERSION', host_cxx_compiler))
 
 
+# Support various fuzzing options
+# ==============================================================
+js_option('--enable-fuzzing', help='Enable fuzzing support')
+
+@depends('--enable-fuzzing')
+def enable_fuzzing(value):
+    if value:
+        return True
+
+@depends(try_compile(body='__AFL_COMPILER;',
+                     check_msg='for AFL compiler',
+                     when='--enable-fuzzing'))
+def enable_aflfuzzer(afl):
+    if afl:
+        return True
+
+@depends(enable_fuzzing,
+         enable_aflfuzzer,
+         c_compiler,
+         target)
+def enable_libfuzzer(fuzzing, afl, c_compiler, target):
+    if fuzzing and not afl and c_compiler.type == 'clang' and target.os != 'Android':
+        return True
+
+@depends(enable_fuzzing,
+         enable_aflfuzzer,
+         enable_libfuzzer)
+def enable_fuzzing_interfaces(fuzzing, afl, libfuzzer):
+    if fuzzing and (afl or libfuzzer):
+        return True
+
+set_config('FUZZING', enable_fuzzing)
+set_define('FUZZING', enable_fuzzing)
+
+set_config('LIBFUZZER', enable_libfuzzer)
+set_define('LIBFUZZER', enable_libfuzzer)
+add_old_configure_assignment('LIBFUZZER', enable_libfuzzer)
+
+set_config('FUZZING_INTERFACES', enable_fuzzing_interfaces)
+set_define('FUZZING_INTERFACES', enable_fuzzing_interfaces)
+add_old_configure_assignment('FUZZING_INTERFACES', enable_fuzzing_interfaces)
+
+
 @depends(c_compiler.try_compile(flags=['-fsanitize=fuzzer-no-link'],
+         when=enable_fuzzing,
          check_msg='whether the C compiler supports -fsanitize=fuzzer-no-link'))
 def libfuzzer_flags(value):
     if value:
         no_link_flag_supported = True
         # recommended for (and only supported by) clang >= 6
         use_flags = ['-fsanitize=fuzzer-no-link']
     else:
         no_link_flag_supported = False
diff --git a/js/moz.configure b/js/moz.configure
--- a/js/moz.configure
+++ b/js/moz.configure
@@ -416,59 +416,16 @@ set_define('JS_HAS_CTYPES', js_has_ctype
 
 @depends('--enable-ctypes', '--enable-compile-environment')
 def ctypes_and_compile_environment(ctypes, compile_environment):
     return ctypes and compile_environment
 
 include('ffi.configure', when=ctypes_and_compile_environment)
 
 
-# Support various fuzzing options
-# ==============================================================
-with only_when('--enable-compile-environment'):
-    js_option('--enable-fuzzing', help='Enable fuzzing support')
-
-    @depends('--enable-fuzzing')
-    def enable_fuzzing(value):
-        if value:
-            return True
-
-    @depends(try_compile(body='__AFL_COMPILER;',
-                         check_msg='for AFL compiler',
-                         when='--enable-fuzzing'))
-    def enable_aflfuzzer(afl):
-        if afl:
-            return True
-
-    @depends(enable_fuzzing,
-             enable_aflfuzzer,
-             c_compiler,
-             target)
-    def enable_libfuzzer(fuzzing, afl, c_compiler, target):
-        if fuzzing and not afl and c_compiler.type == 'clang' and target.os != 'Android':
-            return True
-
-    @depends(enable_fuzzing,
-             enable_aflfuzzer,
-             enable_libfuzzer)
-    def enable_fuzzing_interfaces(fuzzing, afl, libfuzzer):
-        if fuzzing and (afl or libfuzzer):
-            return True
-
-    set_config('FUZZING', enable_fuzzing)
-    set_define('FUZZING', enable_fuzzing)
-
-    set_config('LIBFUZZER', enable_libfuzzer)
-    set_define('LIBFUZZER', enable_libfuzzer)
-    add_old_configure_assignment('LIBFUZZER', enable_libfuzzer)
-
-    set_config('FUZZING_INTERFACES', enable_fuzzing_interfaces)
-    set_define('FUZZING_INTERFACES', enable_fuzzing_interfaces)
-    add_old_configure_assignment('FUZZING_INTERFACES', enable_fuzzing_interfaces)
-
 # Enable pipeline operator
 # ===================================================
 js_option('--enable-pipeline-operator', default=False, help='Enable pipeline operator')
 
 @depends('--enable-pipeline-operator')
 def enable_pipeline_operator(value):
     if value:
         return True
