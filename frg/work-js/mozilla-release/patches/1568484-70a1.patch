# HG changeset patch
# User Bastien Abadie <bastien@mozilla.com>
# Date 1564155557 0
# Node ID 639f502ded6b1d8db390b625b65692622a3c943f
# Parent  3f1f32c7ebb0958a1a6a53ea60a5cb467b1cdbae
Bug 1568484 - Output issues relative paths in mozlint, r=ahal

Differential Revision: https://phabricator.services.mozilla.com/D39162

diff --git a/python/mozlint/mozlint/result.py b/python/mozlint/mozlint/result.py
--- a/python/mozlint/mozlint/result.py
+++ b/python/mozlint/mozlint/result.py
@@ -1,24 +1,31 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import
 
 from collections import defaultdict
 from json import dumps, JSONEncoder
+import os
 
 
 class ResultSummary(object):
     """Represents overall result state from an entire lint run."""
+    root = None
 
-    def __init__(self):
+    def __init__(self, root):
         self.reset()
 
+        # Store the repository root folder to be able to build
+        # Issues relative paths to that folder
+        if ResultSummary.root is None:
+            ResultSummary.root = root
+
     def reset(self):
         self.issues = defaultdict(list)
         self.failed_run = set()
         self.failed_setup = set()
         self.suppressed_warnings = defaultdict(int)
 
     @property
     def returncode(self):
@@ -71,31 +78,40 @@ class Issue(object):
         'message',
         'lineno',
         'column',
         'hint',
         'source',
         'level',
         'rule',
         'lineoffset',
+        'relpath',
     )
 
     def __init__(self, linter, path, message, lineno, column=None, hint=None,
-                 source=None, level=None, rule=None, lineoffset=None):
-        self.path = path
+                 source=None, level=None, rule=None, lineoffset=None, relpath=None):
         self.message = message
         self.lineno = int(lineno) if lineno else 0
         self.column = int(column) if column else column
         self.hint = hint
         self.source = source
         self.level = level or 'error'
         self.linter = linter
         self.rule = rule
         self.lineoffset = lineoffset
 
+        root = ResultSummary.root
+        assert root is not None, 'Missing ResultSummary.root'
+        if os.path.isabs(path):
+            self.path = path
+            self.relpath = os.path.relpath(path, root)
+        else:
+            self.path = os.path.join(root, path)
+            self.relpath = path
+
     def __repr__(self):
         s = dumps(self, cls=IssueEncoder, indent=2)
         return "Issue({})".format(s)
 
 
 class IssueEncoder(JSONEncoder):
     """Class for encoding :class:`~result.Issue`s to json.
 
diff --git a/python/mozlint/mozlint/roller.py b/python/mozlint/mozlint/roller.py
--- a/python/mozlint/mozlint/roller.py
+++ b/python/mozlint/mozlint/roller.py
@@ -25,17 +25,17 @@ from .pathutils import findobject
 from .result import ResultSummary
 from .types import supported_types
 
 SHUTDOWN = False
 orig_sigint = signal.getsignal(signal.SIGINT)
 
 
 def _run_worker(config, paths, **lintargs):
-    result = ResultSummary()
+    result = ResultSummary(lintargs['root'])
 
     if SHUTDOWN:
         return result
 
     func = supported_types[config['type']]
     try:
         res = func(paths, config, **lintargs) or []
     except Exception:
@@ -102,17 +102,17 @@ class LintRoller(object):
         except InvalidRepoPath:
             self.vcs = None
 
         self.linters = []
         self.lintargs = lintargs
         self.lintargs['root'] = root
 
         # result state
-        self.result = ResultSummary()
+        self.result = ResultSummary(root)
 
         self.root = root
         self.exclude = exclude or []
 
     def read(self, paths):
         """Parse one or more linters and add them to the registry.
 
         :param paths: A path or iterable of paths to linter definitions.
diff --git a/python/mozlint/test/test_editor.py b/python/mozlint/test/test_editor.py
--- a/python/mozlint/test/test_editor.py
+++ b/python/mozlint/test/test_editor.py
@@ -26,17 +26,17 @@ def capture_commands(monkeypatch):
 
         monkeypatch.setattr(subprocess, 'call', fake_subprocess_call)
 
     return inner
 
 
 @pytest.fixture
 def result():
-    result = ResultSummary()
+    result = ResultSummary('/fake/root')
     result.issues['foo.py'].extend([
         Issue(
             linter='no-foobar',
             path='foo.py',
             lineno=1,
             message="Oh no!",
         ),
         Issue(
diff --git a/python/mozlint/test/test_formatters.py b/python/mozlint/test/test_formatters.py
--- a/python/mozlint/test/test_formatters.py
+++ b/python/mozlint/test/test_formatters.py
@@ -94,17 +94,17 @@ def result(scope='module'):
         Issue(
             linter='baz',
             path='a/b/c.txt',
             message="oh no baz",
             lineno=4,
             source="if baz:",
         ),
     )
-    result = ResultSummary()
+    result = ResultSummary('/fake/root')
     for c in containers:
         result.issues[c.path].append(c)
     return result
 
 
 @pytest.mark.parametrize("name", EXPECTED.keys())
 def test_formatters(result, name):
     opts = EXPECTED[name]
diff --git a/python/mozlint/test/test_roller.py b/python/mozlint/test/test_roller.py
--- a/python/mozlint/test/test_roller.py
+++ b/python/mozlint/test/test_roller.py
@@ -143,17 +143,17 @@ def test_roll_warnings(lint, linters, fi
     result = lint.roll(files)
     assert len(result.issues) == 1
     assert result.total_issues == 2
     assert len(result.suppressed_warnings) == 0
     assert result.total_suppressed_warnings == 0
 
 
 def fake_run_worker(config, paths, **lintargs):
-    result = ResultSummary()
+    result = ResultSummary(lintargs['root'])
     result.issues['count'].append(1)
     return result
 
 
 @pytest.mark.skipif(platform.system() == 'Windows',
                     reason="monkeypatch issues with multiprocessing on Windows")
 @pytest.mark.parametrize('num_procs', [1, 4, 8, 16])
 def test_number_of_jobs(monkeypatch, lint, linters, files, num_procs):
