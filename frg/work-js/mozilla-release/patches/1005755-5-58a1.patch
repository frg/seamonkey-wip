# HG changeset patch
# User Jan Odvarko <odvarko@gmail.com>
# Date 1508159593 -7200
# Node ID f22d51b93cf81f184d3d782429a5ca7bbb1925a4
# Parent  59886ff80264ddba7264c11a2c30b9d555793d4e
Bug 1005755 - Test for pause/stop button; r=nchevobbe

MozReview-Commit-ID: Hbg6hnmgNgF

diff --git a/devtools/client/netmonitor/src/components/toolbar.js b/devtools/client/netmonitor/src/components/toolbar.js
--- a/devtools/client/netmonitor/src/components/toolbar.js
+++ b/devtools/client/netmonitor/src/components/toolbar.js
@@ -153,16 +153,17 @@ const Toolbar = createClass({
         )
       );
     });
 
     // Calculate class-list for toggle recording button. The button
     // has two states: pause/play.
     let toggleButtonClassList = [
       "devtools-button",
+      "requests-list-pause-button",
       recording ? "devtools-pause-icon" : "devtools-play-icon",
     ];
 
     // Render the entire toolbar.
     return (
       span({ className: "devtools-toolbar devtools-toolbar-container" },
         span({ className: "devtools-toolbar-group" },
           button({
diff --git a/devtools/client/netmonitor/src/connector/firefox-data-provider.js b/devtools/client/netmonitor/src/connector/firefox-data-provider.js
--- a/devtools/client/netmonitor/src/connector/firefox-data-provider.js
+++ b/devtools/client/netmonitor/src/connector/firefox-data-provider.js
@@ -423,16 +423,18 @@ class FirefoxDataProvider {
             this.requestData(actor, updateType).then(response => {
               this.onEventTimings(response)
                 .then(() => this.onDataReceived(actor, updateType));
               emit(EVENTS.UPDATING_EVENT_TIMINGS, actor);
             });
           });
         break;
     }
+
+    emit(EVENTS.NETWORK_EVENT_UPDATED, actor);
   }
 
   /**
    * Wrapper method for requesting HTTP details data from the backend.
    *
    * It collects all RDP requests and monitors responses, so it's
    * possible to determine whether (and when) all requested data
    * has been fetched from the backend.
diff --git a/devtools/client/netmonitor/src/constants.js b/devtools/client/netmonitor/src/constants.js
--- a/devtools/client/netmonitor/src/constants.js
+++ b/devtools/client/netmonitor/src/constants.js
@@ -49,16 +49,17 @@ const ACTIVITY_TYPE = {
 };
 
 // The panel's window global is an EventEmitter firing the following events:
 const EVENTS = {
   // When a network or timeline event is received.
   // See https://developer.mozilla.org/docs/Tools/Web_Console/remoting for
   // more information about what each packet is supposed to deliver.
   NETWORK_EVENT: "NetMonitor:NetworkEvent",
+  NETWORK_EVENT_UPDATED: "NetMonitor:NetworkEventUpdated",
   TIMELINE_EVENT: "NetMonitor:TimelineEvent",
 
   // When a network event is added to the view
   REQUEST_ADDED: "NetMonitor:RequestAdded",
 
   // When request headers begin and finish receiving.
   UPDATING_REQUEST_HEADERS: "NetMonitor:NetworkEventUpdating:RequestHeaders",
   RECEIVED_REQUEST_HEADERS: "NetMonitor:NetworkEventUpdated:RequestHeaders",
diff --git a/devtools/client/netmonitor/test/browser.ini b/devtools/client/netmonitor/test/browser.ini
--- a/devtools/client/netmonitor/test/browser.ini
+++ b/devtools/client/netmonitor/test/browser.ini
@@ -20,16 +20,17 @@ support-files =
   html_json-custom-mime-test-page.html
   html_json-long-test-page.html
   html_json-malformed-test-page.html
   html_json-text-mime-test-page.html
   html_jsonp-test-page.html
   html_maps-test-page.html
   html_navigate-test-page.html
   html_params-test-page.html
+  html_pause-test-page.html
   html_post-data-test-page.html
   html_post-json-test-page.html
   html_post-raw-test-page.html
   html_post-raw-with-headers-test-page.html
   html_simple-test-page.html
   html_single-get-page.html
   html_send-beacon.html
   html_sorting-test-page.html
@@ -131,16 +132,17 @@ skip-if = (os == 'linux' && debug && bit
 [browser_net_jsonp.js]
 [browser_net_large-response.js]
 [browser_net_leak_on_tab_close.js]
 [browser_net_open_in_debugger.js]
 [browser_net_open_in_style_editor.js]
 [browser_net_open_request_in_tab.js]
 [browser_net_pane-collapse.js]
 [browser_net_pane-toggle.js]
+[browser_net_pause.js]
 [browser_net_params_sorted.js]
 [browser_net_persistent_logs.js]
 [browser_net_post-data-01.js]
 [browser_net_post-data-02.js]
 [browser_net_post-data-03.js]
 [browser_net_post-data-04.js]
 [browser_net_prefs-and-l10n.js]
 [browser_net_prefs-reload.js]
diff --git a/devtools/client/netmonitor/test/browser_net_pause.js b/devtools/client/netmonitor/test/browser_net_pause.js
new file mode 100644
--- /dev/null
+++ b/devtools/client/netmonitor/test/browser_net_pause.js
@@ -0,0 +1,90 @@
+/* Any copyright is dedicated to the Public Domain.
+   http://creativecommons.org/publicdomain/zero/1.0/ */
+
+"use strict";
+
+/**
+ * Tests if the pause/resume button works.
+ */
+add_task(function* () {
+  let { tab, monitor } = yield initNetMonitor(PAUSE_URL);
+  info("Starting test... ");
+
+  let { document, store, windowRequire, connector } = monitor.panelWin;
+  let Actions = windowRequire("devtools/client/netmonitor/src/actions/index");
+  let pauseButton = document.querySelector(".requests-list-pause-button");
+
+  store.dispatch(Actions.batchEnable(false));
+
+  // Make sure we start in a sane state.
+  assertRequestCount(store, 0);
+
+  // Load one request and assert it shows up in the list.
+  yield performRequestAndWait(tab, monitor);
+  assertRequestCount(store, 1);
+
+  let noRequest = true;
+  monitor.panelWin.once(EVENTS.NETWORK_EVENT, () => {
+    noRequest = false;
+  });
+
+  monitor.panelWin.once(EVENTS.NETWORK_EVENT_UPDATED, () => {
+    noRequest = false;
+  });
+
+  // Click pause, load second request and make sure they don't show up.
+  EventUtils.sendMouseEvent({ type: "click" }, pauseButton);
+  yield performPausedRequest(connector, tab, monitor);
+  ok(noRequest, "There should be no activity when paused.");
+  assertRequestCount(store, 1);
+
+  // Click pause again to resume monitoring. Load a third request
+  // and make sure they will show up.
+  EventUtils.sendMouseEvent({ type: "click" }, pauseButton);
+  yield performRequestAndWait(tab, monitor);
+  assertRequestCount(store, 2);
+
+  return teardown(monitor);
+});
+
+/**
+ * Asserts the number of requests in the network monitor.
+ */
+function assertRequestCount(store, count) {
+  is(store.getState().requests.requests.size, count,
+    "There should be correct number of requests");
+}
+
+/**
+ * Execute simple GET request and wait till it's done.
+ */
+function* performRequestAndWait(tab, monitor) {
+  let wait = waitForNetworkEvents(monitor, 1);
+  yield ContentTask.spawn(tab.linkedBrowser, SIMPLE_SJS, function* (url) {
+    yield content.wrappedJSObject.performRequests(url);
+  });
+  yield wait;
+}
+
+/**
+ * Execute simple GET request
+ */
+function* performPausedRequest(connector, tab, monitor) {
+  let wait = waitForWebConsoleNetworkEvent(connector);
+  yield ContentTask.spawn(tab.linkedBrowser, SIMPLE_SJS, function* (url) {
+    yield content.wrappedJSObject.performRequests(url);
+  });
+  yield wait;
+}
+
+/**
+ * Listen for events fired by the console client since the Firefox
+ * connector (data provider) is paused.
+ */
+function waitForWebConsoleNetworkEvent(connector) {
+  return new Promise(resolve => {
+    connector.connector.webConsoleClient.once("networkEvent", (type, networkInfo) => {
+      resolve();
+    });
+  });
+}
diff --git a/devtools/client/netmonitor/test/head.js b/devtools/client/netmonitor/test/head.js
--- a/devtools/client/netmonitor/test/head.js
+++ b/devtools/client/netmonitor/test/head.js
@@ -55,16 +55,17 @@ const FILTERING_URL = EXAMPLE_URL + "htm
 const INFINITE_GET_URL = EXAMPLE_URL + "html_infinite-get-page.html";
 const CUSTOM_GET_URL = EXAMPLE_URL + "html_custom-get-page.html";
 const SINGLE_GET_URL = EXAMPLE_URL + "html_single-get-page.html";
 const STATISTICS_URL = EXAMPLE_URL + "html_statistics-test-page.html";
 const CURL_URL = EXAMPLE_URL + "html_copy-as-curl.html";
 const CURL_UTILS_URL = EXAMPLE_URL + "html_curl-utils.html";
 const SEND_BEACON_URL = EXAMPLE_URL + "html_send-beacon.html";
 const CORS_URL = EXAMPLE_URL + "html_cors-test-page.html";
+const PAUSE_URL = EXAMPLE_URL + "html_pause-test-page.html";
 
 const SIMPLE_SJS = EXAMPLE_URL + "sjs_simple-test-server.sjs";
 const SIMPLE_UNSORTED_COOKIES_SJS = EXAMPLE_URL + "sjs_simple-unsorted-cookies-test-server.sjs";
 const CONTENT_TYPE_SJS = EXAMPLE_URL + "sjs_content-type-test-server.sjs";
 const HTTPS_CONTENT_TYPE_SJS = HTTPS_EXAMPLE_URL + "sjs_content-type-test-server.sjs";
 const STATUS_CODES_SJS = EXAMPLE_URL + "sjs_status-codes-test-server.sjs";
 const SORTING_SJS = EXAMPLE_URL + "sjs_sorting-test-server.sjs";
 const HTTPS_REDIRECT_SJS = EXAMPLE_URL + "sjs_https-redirect-test-server.sjs";
diff --git a/devtools/client/netmonitor/test/html_pause-test-page.html b/devtools/client/netmonitor/test/html_pause-test-page.html
new file mode 100644
--- /dev/null
+++ b/devtools/client/netmonitor/test/html_pause-test-page.html
@@ -0,0 +1,37 @@
+<!-- Any copyright is dedicated to the Public Domain.
+     http://creativecommons.org/publicdomain/zero/1.0/ -->
+<!doctype html>
+
+<html>
+  <head>
+    <meta charset="utf-8"/>
+    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
+    <meta http-equiv="Pragma" content="no-cache" />
+    <meta http-equiv="Expires" content="0" />
+    <title>Network Monitor test page</title>
+  </head>
+
+  <body>
+    <p>Performing a custom number of GETs</p>
+
+    <script type="text/javascript">
+      /* exported performRequests */
+      "use strict";
+
+      function performRequests(url) {
+        return new Promise((resolve, reject) => {
+          let xhr = new XMLHttpRequest();
+          xhr.open("GET", url, true);
+
+          xhr.onreadystatechange = function () {
+            if (this.readyState == this.DONE) {
+              resolve();
+            }
+          };
+          xhr.send(null);
+        });
+      }
+    </script>
+  </body>
+
+</html>
