# HG changeset patch
# User Jeff Walden <jwalden@mit.edu>
# Date 1520661548 28800
#      Fri Mar 09 21:59:08 2018 -0800
# Node ID b78439446b01302e362078324550bd630bd74a14
# Parent  c57f65888bfd39a7638c9768c01f1bdef0c75a7c
Bug 1442967 - Futz with a uintptr_t (as uint32_t) == uint64_t comparison to evade constantly-false compiler warnings in 32-bit builds.  r=jorendorff

diff --git a/mfbt/tests/TestEndian.cpp b/mfbt/tests/TestEndian.cpp
--- a/mfbt/tests/TestEndian.cpp
+++ b/mfbt/tests/TestEndian.cpp
@@ -1,22 +1,24 @@
 /* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "mozilla/Assertions.h"
 #include "mozilla/EndianUtils.h"
+#include "mozilla/MathAlgorithms.h"
 
 #include <stddef.h>
 
 using mozilla::BigEndian;
 using mozilla::LittleEndian;
 using mozilla::NativeEndian;
+using mozilla::RotateRight;
 
 template<typename T>
 void
 TestSingleSwap(T aValue, T aSwappedValue)
 {
 #if MOZ_LITTLE_ENDIAN
   MOZ_RELEASE_ASSERT(NativeEndian::swapToBigEndian(aValue) == aSwappedValue);
   MOZ_RELEASE_ASSERT(NativeEndian::swapFromBigEndian(aValue) == aSwappedValue);
@@ -363,20 +365,30 @@ main()
     BigEndian::readUint32(&unsigned_bytes[0]) == 0x01020304U);
 
   MOZ_RELEASE_ASSERT(
     LittleEndian::readUint64(&unsigned_bytes[0]) == 0x0807060504030201ULL);
   MOZ_RELEASE_ASSERT(
     BigEndian::readUint64(&unsigned_bytes[0]) == 0x0102030405060708ULL);
 
   if (sizeof(uintptr_t) == 8) {
+    // Comparing a uint32_t (in 32-bit builds) against a non-uint32_t uint64_t
+    // value causes constantly-false compiler warnings with some compilers.
+    // Hack around this by ensuring both sides are uint64_t, then rotating by
+    // identical amounts to fake out compiler range analysis.
+    //
+    // C++17 adds an |if constexpr(...)| statement construct that compilers use
+    // to not-compile things like invalid template uses, that may eventually
+    // allow a clean fix for this.
     MOZ_RELEASE_ASSERT(
-      LittleEndian::readUintptr(&unsigned_bytes[0]) == 0x0807060504030201ULL);
+      RotateRight(uint64_t(LittleEndian::readUintptr(&unsigned_bytes[0])), 5) ==
+      RotateRight(0x0807060504030201ULL, 5));
     MOZ_RELEASE_ASSERT(
-      BigEndian::readUintptr(&unsigned_bytes[0]) == 0x0102030405060708ULL);
+      RotateRight(uint64_t(BigEndian::readUintptr(&unsigned_bytes[0])), 5) ==
+      RotateRight(0x0102030405060708ULL, 5));
   } else {
     MOZ_RELEASE_ASSERT(
       LittleEndian::readUintptr(&unsigned_bytes[0]) == 0x04030201U);
     MOZ_RELEASE_ASSERT(
       BigEndian::readUintptr(&unsigned_bytes[0]) == 0x01020304U);
   }
 
   LittleEndian::writeUint16(&buffer[0], 0x0201);
