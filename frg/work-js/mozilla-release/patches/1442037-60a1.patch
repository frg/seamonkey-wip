# HG changeset patch
# User Andrew Osmond <aosmond@mozilla.com>
# Date 1519951138 18000
# Node ID 3a5462439ead52afdcee1d2633dcc5426e451d16
# Parent  618b05d074850d6d89a1bb078593e56af1300dce
Bug 1442037 - Fix an incorrect assert in DecoderFactory::CloneAnimationDecoder. r=tnikkel

When cloning an animated image decoder, we asserted that
Decoder::HasAnimation was true. This is incorrect because if the decoder
has yet to complete the metadata decoding, or it has but only finds out
the image is animated when it discovers the second frame, then we will
try to clone a valid animated image decoder, but fail the assertion.
Instead, this patch verifies the image type supports animations.

diff --git a/image/DecoderFactory.cpp b/image/DecoderFactory.cpp
--- a/image/DecoderFactory.cpp
+++ b/image/DecoderFactory.cpp
@@ -234,20 +234,26 @@ DecoderFactory::CreateAnimationDecoder(D
   task.forget(aOutTask);
   return NS_OK;
 }
 
 /* static */ already_AddRefed<Decoder>
 DecoderFactory::CloneAnimationDecoder(Decoder* aDecoder)
 {
   MOZ_ASSERT(aDecoder);
-  MOZ_ASSERT(aDecoder->HasAnimation());
 
-  RefPtr<Decoder> decoder = GetDecoder(aDecoder->GetType(), nullptr,
-                                       /* aIsRedecode = */ true);
+  // In an ideal world, we would assert aDecoder->HasAnimation() but we cannot.
+  // The decoder may not have detected it is animated yet (e.g. it did not even
+  // get scheduled yet, or it has only decoded the first frame and has yet to
+  // rediscover it is animated).
+  DecoderType type = aDecoder->GetType();
+  MOZ_ASSERT(type == DecoderType::GIF || type == DecoderType::PNG,
+             "Calling CloneAnimationDecoder for non-animating DecoderType");
+
+  RefPtr<Decoder> decoder = GetDecoder(type, nullptr, /* aIsRedecode = */ true);
   MOZ_ASSERT(decoder, "Should have a decoder now");
 
   // Initialize the decoder.
   decoder->SetMetadataDecode(false);
   decoder->SetIterator(aDecoder->GetSourceBuffer()->Iterator());
   decoder->SetDecoderFlags(aDecoder->GetDecoderFlags());
   decoder->SetSurfaceFlags(aDecoder->GetSurfaceFlags());
 
