# HG changeset patch
# User Brad Werth <bwerth@mozilla.com>
# Date 1517252138 18000
# Node ID b1868b7597e2682b8fb8501dd0a22a4230a57440
# Parent  e755446d475dc4105529a9c2f3b5b6af457b72fc
Bug 1418930 Part 3: Update css::ImageValue to carry a parameter that indicates anonymous CORS headers should be used during loading. r=emilio

MozReview-Commit-ID: JJ5lZRwS6Be

diff --git a/dom/base/nsAttrValue.cpp b/dom/base/nsAttrValue.cpp
--- a/dom/base/nsAttrValue.cpp
+++ b/dom/base/nsAttrValue.cpp
@@ -11,16 +11,17 @@
 
 #include "mozilla/DebugOnly.h"
 #include "mozilla/HashFunctions.h"
 
 #include "nsAttrValue.h"
 #include "nsAttrValueInlines.h"
 #include "nsIAtom.h"
 #include "nsUnicharUtils.h"
+#include "mozilla/CORSMode.h"
 #include "mozilla/MemoryReporting.h"
 #include "mozilla/ServoBindingTypes.h"
 #include "mozilla/ServoStyleSet.h"
 #include "mozilla/DeclarationBlockInlines.h"
 #include "nsContentUtils.h"
 #include "nsReadableUtils.h"
 #include "nsHTMLCSSStyleSheet.h"
 #include "nsCSSParser.h"
@@ -1710,17 +1711,19 @@ nsAttrValue::LoadImage(nsIDocument* aDoc
 
   MiscContainer* cont = GetMiscContainer();
   mozilla::css::URLValue* url = cont->mValue.mURL;
 
   NS_ASSERTION(!url->IsStringEmpty(),
                "How did we end up with an empty string for eURL");
 
   mozilla::css::ImageValue* image =
-      mozilla::css::ImageValue::CreateFromURLValue(url, aDocument);
+      mozilla::css::ImageValue::CreateFromURLValue(url,
+                                                   aDocument,
+                                                   mozilla::CORSMode::CORS_NONE);
 
   NS_ADDREF(image);
   cont->mValue.mImage = image;
   NS_RELEASE(url);
   cont->mType = eImage;
 }
 
 bool
diff --git a/layout/style/ServoBindings.cpp b/layout/style/ServoBindings.cpp
--- a/layout/style/ServoBindings.cpp
+++ b/layout/style/ServoBindings.cpp
@@ -47,16 +47,17 @@
 #include "nsNetUtil.h"
 #include "nsString.h"
 #include "nsStyleStruct.h"
 #include "nsStyleUtil.h"
 #include "nsSVGElement.h"
 #include "nsTArray.h"
 #include "nsTransitionManager.h"
 
+#include "mozilla/CORSMode.h"
 #include "mozilla/DeclarationBlockInlines.h"
 #include "mozilla/EffectCompositor.h"
 #include "mozilla/EffectSet.h"
 #include "mozilla/EventStates.h"
 #ifdef MOZ_OLD_STYLE
 #include "mozilla/GeckoStyleContext.h"
 #endif
 #include "mozilla/Keyframe.h"
@@ -1508,18 +1509,21 @@ CreateStyleImageRequest(nsStyleImageRequ
   RefPtr<nsStyleImageRequest> req =
     new nsStyleImageRequest(aModeFlags, aImageValue);
   return req.forget();
 }
 
 mozilla::css::ImageValue*
 Gecko_ImageValue_Create(ServoBundledURI aURI, ServoRawOffsetArc<RustString> aURIString)
 {
+  // Bug 1434963: Change this to accept a CORS mode from the caller.
   RefPtr<css::ImageValue> value(
-    new css::ImageValue(aURIString, do_AddRef(aURI.mExtraData)));
+    new css::ImageValue(aURIString,
+                        do_AddRef(aURI.mExtraData),
+                        mozilla::CORSMode::CORS_NONE));
   return value.forget().take();
 }
 
 MOZ_DEFINE_MALLOC_SIZE_OF(GeckoImageValueMallocSizeOf)
 
 size_t
 Gecko_ImageValue_SizeOfIncludingThis(mozilla::css::ImageValue* aImageValue)
 {
diff --git a/layout/style/nsCSSDataBlock.cpp b/layout/style/nsCSSDataBlock.cpp
--- a/layout/style/nsCSSDataBlock.cpp
+++ b/layout/style/nsCSSDataBlock.cpp
@@ -9,16 +9,17 @@
  * declaration, and the code for expanding and compacting it
  */
 
 #include "nsCSSDataBlock.h"
 
 #include "CSSVariableImageTable.h"
 #include "mozilla/css/Declaration.h"
 #include "mozilla/css/ImageLoader.h"
+#include "mozilla/CORSMode.h"
 #include "mozilla/MemoryReporting.h"
 #include "mozilla/WritingModes.h"
 #include "nsAutoPtr.h"
 #include "nsIDocument.h"
 #include "nsRuleData.h"
 #include "nsStyleContext.h"
 #include "nsStyleSet.h"
 
@@ -89,17 +90,17 @@ TryToStartImageLoadOnValue(const nsCSSVa
         bool isEqualExceptRef = false;
         nsresult  rv = imageURI->EqualsExceptRef(docURI, &isEqualExceptRef);
         if (NS_SUCCEEDED(rv) && isEqualExceptRef) {
           return;
         }
       }
     }
 
-    aValue.StartImageLoad(aDocument);
+    aValue.StartImageLoad(aDocument, CORSMode::CORS_NONE);
     if (aForTokenStream && aContext) {
       CSSVariableImageTable::Add(aContext, aProperty,
                                  aValue.GetImageStructValue());
     }
   }
   else if (aValue.GetUnit() == eCSSUnit_Image) {
     // If we already have a request, see if this document needs to clone it.
     imgIRequest* request = aValue.GetImageValue(nullptr);
diff --git a/layout/style/nsCSSValue.cpp b/layout/style/nsCSSValue.cpp
--- a/layout/style/nsCSSValue.cpp
+++ b/layout/style/nsCSSValue.cpp
@@ -889,21 +889,24 @@ nsCSSValue::GetCalcValue() const
     result.mLength = length.GetPixelLength();
     result.mPercent = percent.GetPercentValue();
     result.mHasPercent = true;
   }
 
   return result;
 }
 
-void nsCSSValue::StartImageLoad(nsIDocument* aDocument) const
+void nsCSSValue::StartImageLoad(nsIDocument* aDocument,
+                                mozilla::CORSMode aCORSMode) const
 {
   MOZ_ASSERT(eCSSUnit_URL == mUnit, "Not a URL value!");
   mozilla::css::ImageValue* image =
-      mozilla::css::ImageValue::CreateFromURLValue(mValue.mURL, aDocument);
+      mozilla::css::ImageValue::CreateFromURLValue(mValue.mURL,
+                                                   aDocument,
+                                                   aCORSMode);
 
   nsCSSValue* writable = const_cast<nsCSSValue*>(this);
   writable->SetImageValue(image);
 }
 
 nscolor nsCSSValue::GetColorValue() const
 {
   MOZ_ASSERT(IsNumericColorUnit(), "not a color value");
@@ -3106,54 +3109,69 @@ css::URLValue::SizeOfIncludingThis(mozil
     n += aMallocSizeOf(this);
     n += URLValueData::SizeOfExcludingThis(aMallocSizeOf);
   }
   return n;
 }
 
 css::ImageValue::ImageValue(nsIURI* aURI, const nsAString& aString,
                             already_AddRefed<URLExtraData> aExtraData,
-                            nsIDocument* aDocument)
+                            nsIDocument* aDocument,
+                            CORSMode aCORSMode)
   : URLValueData(do_AddRef(new PtrHolder<nsIURI>("URLValueData::mURI", aURI)),
                  aString, Move(aExtraData))
 {
+  mCORSMode = aCORSMode;
   Initialize(aDocument);
 }
 
 css::ImageValue::ImageValue(nsIURI* aURI, ServoRawOffsetArc<RustString> aString,
                             already_AddRefed<URLExtraData> aExtraData,
-                            nsIDocument* aDocument)
+                            nsIDocument* aDocument,
+                            CORSMode aCORSMode)
   : URLValueData(do_AddRef(new PtrHolder<nsIURI>("URLValueData::mURI", aURI)),
                  aString, Move(aExtraData))
 {
+  mCORSMode = aCORSMode;
   Initialize(aDocument);
 }
 
 css::ImageValue::ImageValue(const nsAString& aString,
-                            already_AddRefed<URLExtraData> aExtraData)
+                            already_AddRefed<URLExtraData> aExtraData,
+                            CORSMode aCORSMode)
   : URLValueData(aString, Move(aExtraData))
 {
+  mCORSMode = aCORSMode;
 }
 
 css::ImageValue::ImageValue(ServoRawOffsetArc<RustString> aString,
-                            already_AddRefed<URLExtraData> aExtraData)
+                            already_AddRefed<URLExtraData> aExtraData,
+                            CORSMode aCORSMode)
   : URLValueData(aString, Move(aExtraData))
 {
+  mCORSMode = aCORSMode;
 }
 
 /*static*/ css::ImageValue*
-css::ImageValue::CreateFromURLValue(URLValue* aUrl, nsIDocument* aDocument)
+css::ImageValue::CreateFromURLValue(URLValue* aUrl,
+                                    nsIDocument* aDocument,
+                                    CORSMode aCORSMode)
 {
   if (aUrl->mUsingRustString) {
     return new css::ImageValue(aUrl->GetURI(),
                                Servo_CloneArcStringData(&aUrl->mStrings.mRustString),
-                               do_AddRef(aUrl->mExtraData), aDocument);
+                               do_AddRef(aUrl->mExtraData),
+                               aDocument,
+                               aCORSMode);
   }
-  return new css::ImageValue(aUrl->GetURI(), aUrl->mStrings.mString,
-                             do_AddRef(aUrl->mExtraData), aDocument);
+  return new css::ImageValue(aUrl->GetURI(),
+                             aUrl->mStrings.mString,
+                             do_AddRef(aUrl->mExtraData),
+                             aDocument,
+                             aCORSMode);
 }
 
 void
 css::ImageValue::Initialize(nsIDocument* aDocument)
 {
   MOZ_ASSERT(NS_IsMainThread());
 
   // NB: If aDocument is not the original document, we may not be able to load
@@ -3164,19 +3182,19 @@ css::ImageValue::Initialize(nsIDocument*
     loadingDoc = aDocument;
   }
 
   if (!mLoadedImage) {
     loadingDoc->StyleImageLoader()->LoadImage(GetURI(),
                                               mExtraData->GetPrincipal(),
                                               mExtraData->GetReferrer(),
                                               this,
-                                              CORSMode::CORS_NONE);
-
-     mLoadedImage = true;
+                                              mCORSMode);
+
+    mLoadedImage = true;
   }
 
   aDocument->StyleImageLoader()->MaybeRegisterCSSImage(this);
 }
 
 css::ImageValue::~ImageValue()
 {
   MOZ_ASSERT(NS_IsMainThread() || mRequests.Count() == 0,
diff --git a/layout/style/nsCSSValue.h b/layout/style/nsCSSValue.h
--- a/layout/style/nsCSSValue.h
+++ b/layout/style/nsCSSValue.h
@@ -5,16 +5,17 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 /* representation of simple property values within CSS declarations */
 
 #ifndef nsCSSValue_h___
 #define nsCSSValue_h___
 
 #include "mozilla/Attributes.h"
+#include "mozilla/CORSMode.h"
 #include "mozilla/MemoryReporting.h"
 #include "mozilla/ServoTypes.h"
 #include "mozilla/SheetType.h"
 #include "mozilla/StyleComplexColor.h"
 #include "mozilla/URLExtraData.h"
 #include "mozilla/UniquePtr.h"
 
 #include "nsCSSKeywords.h"
@@ -199,21 +200,27 @@ private:
   } mStrings;
   mutable bool mUsingRustString;
 
 protected:
   // Only used by ImageValue.  Declared up here because otherwise bindgen gets
   // confused by the non-standard-layout packing of the variable up into
   // URLValueData.
   bool mLoadedImage = false;
+  CORSMode mCORSMode = CORSMode::CORS_NONE;
 
   virtual ~URLValueData();
 
   size_t SizeOfExcludingThis(mozilla::MallocSizeOf aMallocSizeOf) const;
 
+public:
+  void SetCORSMode(CORSMode aCORSMode) {
+    mCORSMode = aCORSMode;
+  }
+
 private:
   URLValueData(const URLValueData& aOther) = delete;
   URLValueData& operator=(const URLValueData& aOther) = delete;
 
   friend struct ImageValue;
 };
 
 struct URLValue final : public URLValueData
@@ -232,41 +239,47 @@ struct URLValue final : public URLValueD
   URLValue(const URLValue&) = delete;
   URLValue& operator=(const URLValue&) = delete;
 
   size_t SizeOfIncludingThis(mozilla::MallocSizeOf aMallocSizeOf) const;
 };
 
 struct ImageValue final : public URLValueData
 {
-  static ImageValue* CreateFromURLValue(URLValue* url, nsIDocument* aDocument);
+  static ImageValue* CreateFromURLValue(URLValue* url,
+                                        nsIDocument* aDocument,
+                                        CORSMode aCORSMode);
 
   // Not making the constructor and destructor inline because that would
   // force us to include imgIRequest.h, which leads to REQUIRES hell, since
   // this header is included all over.
   //
   // This constructor is only safe to call from the main thread.
   ImageValue(nsIURI* aURI, const nsAString& aString,
              already_AddRefed<URLExtraData> aExtraData,
-             nsIDocument* aDocument);
+             nsIDocument* aDocument,
+             CORSMode aCORSMode);
 
   // This constructor is only safe to call from the main thread.
   ImageValue(nsIURI* aURI, ServoRawOffsetArc<RustString> aString,
              already_AddRefed<URLExtraData> aExtraData,
-             nsIDocument* aDocument);
+             nsIDocument* aDocument,
+             CORSMode aCORSMode);
 
   // This constructor is safe to call from any thread, but Initialize
   // must be called later for the object to be useful.
   ImageValue(const nsAString& aString,
-             already_AddRefed<URLExtraData> aExtraData);
+             already_AddRefed<URLExtraData> aExtraData,
+             CORSMode aCORSMode);
 
   // This constructor is safe to call from any thread, but Initialize
   // must be called later for the object to be useful.
   ImageValue(ServoRawOffsetArc<RustString> aURIString,
-             already_AddRefed<URLExtraData> aExtraData);
+             already_AddRefed<URLExtraData> aExtraData,
+             CORSMode aCORSMode);
 
   ImageValue(const ImageValue&) = delete;
   ImageValue& operator=(const ImageValue&) = delete;
 
   void Initialize(nsIDocument* aDocument);
 
   size_t SizeOfIncludingThis(mozilla::MallocSizeOf aMallocSizeOf) const;
 
