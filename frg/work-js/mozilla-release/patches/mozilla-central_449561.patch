# HG changeset patch
# User Chris Manchester <cmanchester@mozilla.com>
# Date 1544136491 0
#      Thu Dec 06 22:48:11 2018 +0000
# Node ID 7f908c15daf69ea33ab6c71471f98a517e831f72
# Parent  c9ff07bf8ef3e76000e14e51ffbcd65dbafcd441
Bug 1510714 - Use mozbuild instead of path heuristics to prioritize rust in the compile traversal. r=firefox-build-system-reviewers,nalexander

Differential Revision: https://phabricator.services.mozilla.com/D13921

diff --git a/python/mozbuild/mozbuild/backend/recursivemake.py b/python/mozbuild/mozbuild/backend/recursivemake.py
--- a/python/mozbuild/mozbuild/backend/recursivemake.py
+++ b/python/mozbuild/mozbuild/backend/recursivemake.py
@@ -397,16 +397,17 @@ class RecursiveMakeBackend(CommonBackend
         # even if they are empty, because the directories are still filled
         # by the build system itself, and the install manifests are only
         # used for a "magic" rm -rf.
         self._install_manifests['dist_public']
         self._install_manifests['dist_private']
 
         self._traversal = RecursiveMakeTraversal()
         self._compile_graph = OrderedDefaultDict(set)
+        self._rust_dirs = set()
 
         self._no_skip = {
             'export': set(),
             'libs': set(),
             'misc': set(),
             'tools': set(),
             'check': set(),
             'syms': set(),
@@ -623,22 +624,24 @@ class RecursiveMakeBackend(CommonBackend
            method=obj.method))
 
         elif isinstance(obj, JARManifest):
             self._no_skip['libs'].add(backend_file.relobjdir)
             backend_file.write('JAR_MANIFEST := %s\n' % obj.path.full_path)
 
         elif isinstance(obj, RustProgram):
             self._process_rust_program(obj, backend_file)
+            self._rust_dirs.add(obj.relobjdir)
             # Hook the program into the compile graph.
             build_target = self._build_target_for_obj(obj)
             self._compile_graph[build_target]
 
         elif isinstance(obj, HostRustProgram):
             self._process_host_rust_program(obj, backend_file)
+            self._rust_dirs.add(obj.relobjdir)
             # Hook the program into the compile graph.
             build_target = self._build_target_for_obj(obj)
             self._compile_graph[build_target]
 
         elif isinstance(obj, RustTests):
             self._process_rust_tests(obj, backend_file)
 
         elif isinstance(obj, Program):
@@ -669,16 +672,17 @@ class RecursiveMakeBackend(CommonBackend
             self._process_computed_flags(obj, backend_file)
 
         elif isinstance(obj, InstallationTarget):
             self._process_installation_target(obj, backend_file)
 
         elif isinstance(obj, RustLibrary):
             self.backend_input_files.add(obj.cargo_file)
             self._process_rust_library(obj, backend_file)
+            self._rust_dirs.add(obj.relobjdir)
             # No need to call _process_linked_libraries, because Rust
             # libraries are self-contained objects at this point.
 
             # Hook the library into the compile graph.
             build_target = self._build_target_for_obj(obj)
             self._compile_graph[build_target]
 
         elif isinstance(obj, SharedLibrary):
@@ -809,18 +813,18 @@ class RecursiveMakeBackend(CommonBackend
         compile_roots = [t for t, deps in self._compile_graph.iteritems()
                          if not deps or t not in all_compile_deps]
 
         def add_category_rules(category, roots, graph):
             rule = root_deps_mk.create_rule(['recurse_%s' % category])
             # Directories containing rust compilations don't generally depend
             # on other directories in the tree, so putting them first here will
             # start them earlier in the build.
-            rule.add_dependencies(chain((r for r in roots if 'rust' in r),
-                                        (r for r in roots if 'rust' not in r)))
+            rule.add_dependencies(chain((r for r in roots if mozpath.dirname(r) in self._rust_dirs),
+                                        (r for r in roots if mozpath.dirname(r) not in self._rust_dirs)))
             for target, deps in sorted(graph.items()):
                 if deps:
                     rule = root_deps_mk.create_rule([target])
                     rule.add_dependencies(deps)
 
         non_default_roots = defaultdict(list)
         non_default_graphs = defaultdict(lambda: OrderedDefaultDict(set))
 
