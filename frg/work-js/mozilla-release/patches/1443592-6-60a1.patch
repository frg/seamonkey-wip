# HG changeset patch
# User Jim Blandy <jimb@mozilla.com>
# Date 1520373073 28800
#      Tue Mar 06 13:51:13 2018 -0800
# Node ID 797239d1e7059c6e5f6fd394e261ea089b2325a9
# Parent  c8eff45d0955decbbf82df66a67d1639e986396d
Bug 1443592: Part 6: Remove hasCachedSavedFrame accessors from AbstractFramePtr. r=fitzgen

Some variants of AbstractFramePtr have a cached saved frame flag, and others
don't, but the whole point of LiveSavedFrameCache::FramePtr is that it
represents a frame that does have a flag.

Rather than one variant of FramePtr that is an AbstractFramePtr (restricted to
certain variants), flatten out the two enums into one, and make FramePtr the
sole authority on which sorts of frames we have, and how to access their flags.

This means there is no need for hasCachedSavedFrame accessors on
AbstractFramePtr, just on the individual concrete frame types.

MozReview-Commit-ID: BRVdfqOqBsG

diff --git a/js/src/vm/Stack-inl.h b/js/src/vm/Stack-inl.h
--- a/js/src/vm/Stack-inl.h
+++ b/js/src/vm/Stack-inl.h
@@ -604,41 +604,16 @@ AbstractFramePtr::isDebuggerEvalFrame() 
         return asInterpreterFrame()->isDebuggerEvalFrame();
     if (isBaselineFrame())
         return asBaselineFrame()->isDebuggerEvalFrame();
     MOZ_ASSERT(isRematerializedFrame());
     return false;
 }
 
 inline bool
-AbstractFramePtr::hasCachedSavedFrame() const
-{
-    if (isInterpreterFrame())
-        return asInterpreterFrame()->hasCachedSavedFrame();
-    if (isBaselineFrame())
-        return asBaselineFrame()->hasCachedSavedFrame();
-    if (isWasmDebugFrame())
-        return asWasmDebugFrame()->hasCachedSavedFrame();
-    return asRematerializedFrame()->hasCachedSavedFrame();
-}
-
-inline void
-AbstractFramePtr::setHasCachedSavedFrame()
-{
-    if (isInterpreterFrame())
-        asInterpreterFrame()->setHasCachedSavedFrame();
-    else if (isBaselineFrame())
-        asBaselineFrame()->setHasCachedSavedFrame();
-    else if (isWasmDebugFrame())
-        asWasmDebugFrame()->setHasCachedSavedFrame();
-    else
-        asRematerializedFrame()->setHasCachedSavedFrame();
-}
-
-inline bool
 AbstractFramePtr::isDebuggee() const
 {
     if (isInterpreterFrame())
         return asInterpreterFrame()->isDebuggee();
     if (isBaselineFrame())
         return asBaselineFrame()->isDebuggee();
     if (isWasmDebugFrame())
         return asWasmDebugFrame()->isDebuggee();
@@ -999,41 +974,50 @@ InterpreterActivation::resumeGeneratorFr
 }
 
 /* static */ inline Maybe<LiveSavedFrameCache::FramePtr>
 LiveSavedFrameCache::FramePtr::create(const FrameIter& iter)
 {
     if (iter.done())
         return mozilla::Nothing();
 
-    if (iter.isWasm() && !iter.wasmDebugEnabled())
-        return mozilla::Nothing();
+    if (iter.hasUsableAbstractFramePtr()) {
+        auto afp = iter.abstractFramePtr();
 
-    if (iter.hasUsableAbstractFramePtr())
-        return mozilla::Some(FramePtr(iter.abstractFramePtr()));
+        if (afp.isInterpreterFrame())
+            return mozilla::Some(FramePtr(afp.asInterpreterFrame()));
+        if (afp.isBaselineFrame())
+            return mozilla::Some(FramePtr(afp.asBaselineFrame()));
+        if (afp.isWasmDebugFrame())
+            return mozilla::Some(FramePtr(afp.asWasmDebugFrame()));
+        if (afp.isRematerializedFrame())
+            return mozilla::Some(FramePtr(afp.asRematerializedFrame()));
+
+        return mozilla::Nothing();
+    }
 
     if (iter.isPhysicalIonFrame())
         return mozilla::Some(FramePtr(iter.physicalIonFrame()));
 
     return mozilla::Nothing();
 }
 
 struct LiveSavedFrameCache::FramePtr::HasCachedMatcher {
-    bool match(AbstractFramePtr f) const { return f.hasCachedSavedFrame(); }
-    bool match(jit::CommonFrameLayout* f) const { return f->hasCachedSavedFrame(); }
+    template<typename Frame>
+    bool match(Frame* f) const { return f->hasCachedSavedFrame(); }
 };
 
 inline bool
 LiveSavedFrameCache::FramePtr::hasCachedSavedFrame() const {
     return ptr.match(HasCachedMatcher());
 }
 
 struct LiveSavedFrameCache::FramePtr::SetHasCachedMatcher {
-    void match(AbstractFramePtr f) const { f.setHasCachedSavedFrame(); }
-    void match(jit::CommonFrameLayout* f) const { f->setHasCachedSavedFrame(); }
+    template<typename Frame>
+    void match(Frame* f) const { f->setHasCachedSavedFrame(); }
 };
 
 inline void
 LiveSavedFrameCache::FramePtr::setHasCachedSavedFrame() {
     ptr.match(SetHasCachedMatcher());
 }
 
 } /* namespace js */
diff --git a/js/src/vm/Stack.h b/js/src/vm/Stack.h
--- a/js/src/vm/Stack.h
+++ b/js/src/vm/Stack.h
@@ -236,18 +236,16 @@ class AbstractFramePtr
 
     inline JSCompartment* compartment() const;
 
     inline bool hasInitialEnvironment() const;
     inline bool isGlobalFrame() const;
     inline bool isModuleFrame() const;
     inline bool isEvalFrame() const;
     inline bool isDebuggerEvalFrame() const;
-    inline bool hasCachedSavedFrame() const;
-    inline void setHasCachedSavedFrame();
 
     inline bool hasScript() const;
     inline JSScript* script() const;
     inline wasm::Instance* wasmInstance() const;
     inline GlobalObject* global() const;
     inline JSFunction* callee() const;
     inline Value calleev() const;
     inline Value& thisArgument() const;
@@ -1169,17 +1167,21 @@ struct DefaultHasher<AbstractFramePtr> {
 // cache.
 class LiveSavedFrameCache
 {
   public:
     // The address of a live frame for which we can cache SavedFrames: it has a
     // 'hasCachedSavedFrame' bit we can examine and set, and can be converted to
     // a Key to index the cache.
     class FramePtr {
-        using Ptr = mozilla::Variant<AbstractFramePtr, jit::CommonFrameLayout*>;
+        using Ptr = mozilla::Variant<InterpreterFrame*,
+                                     jit::BaselineFrame*,
+                                     jit::RematerializedFrame*,
+                                     wasm::DebugFrame*,
+                                     jit::CommonFrameLayout*>;
 
         Ptr ptr;
 
         template<typename Frame>
         explicit FramePtr(Frame ptr) : ptr(ptr) { }
 
         struct HasCachedMatcher;
         struct SetHasCachedMatcher;
