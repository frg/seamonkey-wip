# HG changeset patch
# User Luke Wagner <luke@mozilla.com>
# Date 1518471062 21600
#      Mon Feb 12 15:31:02 2018 -0600
# Node ID 4ca928e3c74008885671e122dd05cee40432fc98
# Parent  6eca24657bebba94e5e66fcb1942356d5d8c764d
Bug 1428453 - Baldr: use new traps for float-to-int conversions (r=bbouvier)

diff --git a/js/src/jit/arm/MacroAssembler-arm.cpp b/js/src/jit/arm/MacroAssembler-arm.cpp
--- a/js/src/jit/arm/MacroAssembler-arm.cpp
+++ b/js/src/jit/arm/MacroAssembler-arm.cpp
@@ -5894,18 +5894,17 @@ MacroAssemblerARM::outOfLineWasmTruncate
     // We had an actual correct value, get back to where we were.
     ma_b(rejoin);
 
     // Handle errors.
     bind(&fail);
     asMasm().wasmTrap(wasm::Trap::IntegerOverflow, trapOffset);
 
     bind(&inputIsNaN);
-    asMasm().jump(wasm::OldTrapDesc(trapOffset, wasm::Trap::InvalidConversionToInteger,
-                                    asMasm().framePushed()));
+    asMasm().wasmTrap(wasm::Trap::InvalidConversionToInteger, trapOffset);
 }
 
 void
 MacroAssemblerARM::wasmLoadImpl(const wasm::MemoryAccessDesc& access, Register memoryBase,
                                 Register ptr, Register ptrScratch, AnyRegister output,
                                 Register64 out64)
 {
     MOZ_ASSERT(ptr == ptrScratch);
diff --git a/js/src/jit/mips-shared/CodeGenerator-mips-shared.cpp b/js/src/jit/mips-shared/CodeGenerator-mips-shared.cpp
--- a/js/src/jit/mips-shared/CodeGenerator-mips-shared.cpp
+++ b/js/src/jit/mips-shared/CodeGenerator-mips-shared.cpp
@@ -1566,17 +1566,17 @@ CodeGeneratorMIPSShared::visitOutOfLineW
 
     masm.jump(ool->rejoin());
 
     // Handle errors.
     masm.bind(&fail);
     masm.wasmTrap(wasm::Trap::IntegerOverflow, ool->bytecodeOffset());
 
     masm.bind(&inputIsNaN);
-    masm.jump(oldTrap(ool, wasm::Trap::InvalidConversionToInteger));
+    masm.wasmTrap(wasm::Trap::InvalidConversionToInteger, ool->bytecodeOffset());
 }
 
 void
 CodeGeneratorMIPSShared::visitCopySignF(LCopySignF* ins)
 {
     FloatRegister lhs = ToFloatRegister(ins->getOperand(0));
     FloatRegister rhs = ToFloatRegister(ins->getOperand(1));
     FloatRegister output = ToFloatRegister(ins->getDef(0));
diff --git a/js/src/jit/x86-shared/MacroAssembler-x86-shared.cpp b/js/src/jit/x86-shared/MacroAssembler-x86-shared.cpp
--- a/js/src/jit/x86-shared/MacroAssembler-x86-shared.cpp
+++ b/js/src/jit/x86-shared/MacroAssembler-x86-shared.cpp
@@ -692,17 +692,17 @@ struct MOZ_RAII AutoHandleWasmTruncateTo
 
     ~AutoHandleWasmTruncateToIntErrors() {
         // Handle errors.  These cases are not in arbitrary order: code will
         // fall through to intOverflow.
         masm.bind(&intOverflow);
         masm.wasmTrap(wasm::Trap::IntegerOverflow, off);
 
         masm.bind(&inputIsNaN);
-        masm.jump(wasm::OldTrapDesc(off, wasm::Trap::InvalidConversionToInteger, masm.framePushed()));
+        masm.wasmTrap(wasm::Trap::InvalidConversionToInteger, off);
     }
 };
 
 void
 MacroAssembler::wasmTruncateDoubleToInt32(FloatRegister input, Register output, Label* oolEntry)
 {
     vcvttsd2si(input, output);
     cmp32(output, Imm32(1));
diff --git a/js/src/wasm/WasmStubs.cpp b/js/src/wasm/WasmStubs.cpp
--- a/js/src/wasm/WasmStubs.cpp
+++ b/js/src/wasm/WasmStubs.cpp
@@ -1722,21 +1722,21 @@ wasm::GenerateStubs(const ModuleEnvironm
     }
 
     JitSpew(JitSpew_Codegen, "# Emitting wasm trap stubs");
 
     for (Trap trap : MakeEnumeratedRange(Trap::Limit)) {
         switch (trap) {
           case Trap::Unreachable:
           case Trap::IntegerOverflow:
+          case Trap::InvalidConversionToInteger:
           case Trap::IntegerDivideByZero:
           case Trap::StackOverflow:
             break;
           // The TODO list of "old" traps to convert to new traps:
-          case Trap::InvalidConversionToInteger:
           case Trap::OutOfBounds:
           case Trap::UnalignedAccess:
           case Trap::IndirectCallToNull:
           case Trap::IndirectCallBadSig:
           case Trap::ImpreciseSimdConversion:
           case Trap::ThrowReported: {
             CallableOffsets offsets;
             if (!GenerateOldTrapExit(masm, trap, &throwLabel, &offsets))
