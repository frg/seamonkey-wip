# HG changeset patch
# User Jean-Yves Avenard <jyavenard@mozilla.com>
# Date 1527176282 -7200
# Node ID 4688aa455ae38eb0d359d96a925c5344376c398b
# Parent  fc4b32a311dd865a92b8ecf138b3b6f268e8fe55
Bug 1450607 - P3. Remove unused argument. r=gerald

MozReview-Commit-ID: 6NU9BLJi6kl

diff --git a/dom/media/ChannelMediaResource.cpp b/dom/media/ChannelMediaResource.cpp
--- a/dom/media/ChannelMediaResource.cpp
+++ b/dom/media/ChannelMediaResource.cpp
@@ -417,17 +417,17 @@ ChannelMediaResource::OnStopRequest(nsIR
       }
       // Close the streams that failed due to error. This will cause all
       // client Read and Seek operations on those streams to fail. Blocked
       // Reads will also be woken up.
       Close();
     }
   }
 
-  mCacheStream.NotifyDataEnded(mLoadID, aStatus, true /*aReopenOnError*/);
+  mCacheStream.NotifyDataEnded(mLoadID, aStatus);
   return NS_OK;
 }
 
 nsresult
 ChannelMediaResource::OnChannelRedirect(nsIChannel* aOld,
                                         nsIChannel* aNew,
                                         uint32_t aFlags,
                                         int64_t aOffset)
diff --git a/dom/media/MediaCache.cpp b/dom/media/MediaCache.cpp
--- a/dom/media/MediaCache.cpp
+++ b/dom/media/MediaCache.cpp
@@ -2188,19 +2188,17 @@ MediaCacheStream::UpdateDownloadStatisti
   if (mChannelEnded || mClientSuspended) {
     mDownloadStatistics.Stop();
   } else {
     mDownloadStatistics.Start();
   }
 }
 
 void
-MediaCacheStream::NotifyDataEndedInternal(uint32_t aLoadID,
-                                          nsresult aStatus,
-                                          bool aReopenOnError)
+MediaCacheStream::NotifyDataEndedInternal(uint32_t aLoadID, nsresult aStatus)
 {
   MOZ_ASSERT(OwnerThread()->IsOnCurrentThread());
   AutoLock lock(mMediaCache->Monitor());
 
   if (mClosed || aLoadID != mLoadID) {
     // Nothing to do if the stream is closed or a new load has begun.
     return;
   }
@@ -2234,28 +2232,25 @@ MediaCacheStream::NotifyDataEndedInterna
       stream->mDidNotifyDataEnded = true;
       stream->mNotifyDataEndedStatus = aStatus;
       stream->mClient->CacheClientNotifyDataEnded(aStatus);
     }
   }
 }
 
 void
-MediaCacheStream::NotifyDataEnded(uint32_t aLoadID,
-                                  nsresult aStatus,
-                                  bool aReopenOnError)
+MediaCacheStream::NotifyDataEnded(uint32_t aLoadID, nsresult aStatus)
 {
   MOZ_ASSERT(NS_IsMainThread());
   MOZ_ASSERT(aLoadID > 0);
 
   RefPtr<ChannelMediaResource> client = mClient;
   nsCOMPtr<nsIRunnable> r = NS_NewRunnableFunction(
-    "MediaCacheStream::NotifyDataEnded",
-    [client, this, aLoadID, aStatus, aReopenOnError]() {
-      NotifyDataEndedInternal(aLoadID, aStatus, aReopenOnError);
+    "MediaCacheStream::NotifyDataEnded", [client, this, aLoadID, aStatus]() {
+      NotifyDataEndedInternal(aLoadID, aStatus);
     });
   OwnerThread()->Dispatch(r.forget());
 }
 
 void
 MediaCacheStream::NotifyClientSuspended(bool aSuspended)
 {
   MOZ_ASSERT(NS_IsMainThread());
diff --git a/dom/media/MediaCache.h b/dom/media/MediaCache.h
--- a/dom/media/MediaCache.h
+++ b/dom/media/MediaCache.h
@@ -275,19 +275,17 @@ public:
                           const uint8_t* aData);
 
   // Set the load ID so the following NotifyDataEnded() call can work properly.
   // Used in some rare cases where NotifyDataEnded() is called without the
   // preceding NotifyDataStarted().
   void NotifyLoadID(uint32_t aLoadID);
 
   // Notifies the cache that the channel has closed with the given status.
-  void NotifyDataEnded(uint32_t aLoadID,
-                       nsresult aStatus,
-                       bool aReopenOnError = false);
+  void NotifyDataEnded(uint32_t aLoadID, nsresult aStatus);
 
   // Notifies the stream that the suspend status of the client has changed.
   // Main thread only.
   void NotifyClientSuspended(bool aSuspended);
 
   // Notifies the stream to resume download at the current offset.
   void NotifyResume();
 
@@ -463,19 +461,17 @@ private:
   // waiting on the media cache monitor. Called on the main thread only.
   void FlushPartialBlockInternal(AutoLock&, bool aNotify);
 
   void NotifyDataStartedInternal(uint32_t aLoadID,
                                  int64_t aOffset,
                                  bool aSeekable,
                                  int64_t aLength);
 
-  void NotifyDataEndedInternal(uint32_t aLoadID,
-                               nsresult aStatus,
-                               bool aReopenOnError);
+  void NotifyDataEndedInternal(uint32_t aLoadID, nsresult aStatus);
 
   void UpdateDownloadStatistics(AutoLock&);
 
   void CloseInternal(AutoLock&);
   void InitAsCloneInternal(MediaCacheStream* aOriginal);
 
   // Instance of MediaCache to use with this MediaCacheStream.
   RefPtr<MediaCache> mMediaCache;
