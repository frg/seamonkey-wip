# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1518541562 18000
# Node ID 63f56f0215ef007a60ad3013e6a7e24948bb71f4
# Parent  7e5620aa962f1c110398b75751165e571d45dcd4
servo: Merge #20038 - style: Avoid an intermediate buffer in Servo_Shorthand_AnimationValues_Serialize (from emilio:no-need-for-strings); r=nox

Source-Repo: https://github.com/servo/servo
Source-Revision: 4c3f1756da1373e9eef33716c9a93b0bb0559241

diff --git a/servo/ports/geckolib/glue.rs b/servo/ports/geckolib/glue.rs
--- a/servo/ports/geckolib/glue.rs
+++ b/servo/ports/geckolib/glue.rs
@@ -704,39 +704,38 @@ pub extern "C" fn Servo_AnimationValue_S
     let buffer = unsafe { buffer.as_mut().unwrap() };
     let rv = PropertyDeclarationBlock::with_one(uncomputed_value, Importance::Normal)
         .single_value_to_css(&get_property_id_from_nscsspropertyid!(property, ()), buffer,
                              None, None /* No extra custom properties */);
     debug_assert!(rv.is_ok());
 }
 
 #[no_mangle]
-pub extern "C" fn Servo_Shorthand_AnimationValues_Serialize(shorthand_property: nsCSSPropertyID,
-                                                            values: RawGeckoServoAnimationValueListBorrowed,
-                                                            buffer: *mut nsAString)
-{
+pub unsafe extern "C" fn Servo_Shorthand_AnimationValues_Serialize(
+    shorthand_property: nsCSSPropertyID,
+    values: RawGeckoServoAnimationValueListBorrowed,
+    buffer: *mut nsAString,
+) {
     let property_id = get_property_id_from_nscsspropertyid!(shorthand_property, ());
     let shorthand = match property_id.as_shorthand() {
         Ok(shorthand) => shorthand,
         _ => return,
     };
 
     // Convert RawServoAnimationValue(s) into a vector of PropertyDeclaration
     // so that we can use reference of the PropertyDeclaration without worrying
     // about its lifetime. (longhands_to_css() expects &PropertyDeclaration
     // iterator.)
     let declarations: Vec<PropertyDeclaration> =
-        values.iter().map(|v| AnimationValue::as_arc(unsafe { &&*v.mRawPtr }).uncompute()).collect();
-
-    let mut string = String::new();
-    let rv = shorthand.longhands_to_css(declarations.iter(), &mut CssWriter::new(&mut string));
-    if rv.is_ok() {
-        let buffer = unsafe { buffer.as_mut().unwrap() };
-        buffer.assign_utf8(&string);
-    }
+        values.iter().map(|v| AnimationValue::as_arc(&&*v.mRawPtr).uncompute()).collect();
+
+    let _ = shorthand.longhands_to_css(
+        declarations.iter(),
+        &mut CssWriter::new(&mut *buffer),
+    );
 }
 
 #[no_mangle]
 pub extern "C" fn Servo_AnimationValue_GetOpacity(
     value: RawServoAnimationValueBorrowed,
 ) -> f32 {
     let value = AnimationValue::as_arc(&value);
     if let AnimationValue::Opacity(opacity) = **value {
