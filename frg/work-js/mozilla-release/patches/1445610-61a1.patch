# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1521106739 -3600
#      Thu Mar 15 10:38:59 2018 +0100
# Node ID bb94742032b51d15ad23f836316c91bb30ac89ed
# Parent  be9e41af931838a4c4dda017609a9b6d03bb9511
Bug 1445610 - Clean up some enums in Value.h. r=jwalden

diff --git a/js/public/Value.h b/js/public/Value.h
--- a/js/public/Value.h
+++ b/js/public/Value.h
@@ -6,16 +6,17 @@
 
 /* JS::Value implementation. */
 
 #ifndef js_Value_h
 #define js_Value_h
 
 #include "mozilla/Attributes.h"
 #include "mozilla/Casting.h"
+#include "mozilla/Compiler.h"
 #include "mozilla/EndianUtils.h"
 #include "mozilla/FloatingPoint.h"
 #include "mozilla/Likely.h"
 
 #include <limits> /* for std::numeric_limits */
 
 #include "js-config.h"
 #include "jstypes.h"
@@ -33,41 +34,42 @@ namespace JS { class Value; }
 
 #if defined(JS_PUNBOX64)
 # define JSVAL_TAG_SHIFT 47
 #endif
 
 // Use enums so that printing a JS::Value in the debugger shows nice
 // symbolic type tags.
 
-#if defined(_MSC_VER)
+// Work around a GCC bug. See comment above #undef JS_ENUM_HEADER.
+#if MOZ_IS_GCC
+# define JS_ENUM_HEADER(id, type)              enum id
+# define JS_ENUM_FOOTER(id)                    __attribute__((packed))
+#else
 # define JS_ENUM_HEADER(id, type)              enum id : type
 # define JS_ENUM_FOOTER(id)
-#else
-# define JS_ENUM_HEADER(id, type)              enum id
-# define JS_ENUM_FOOTER(id)                    __attribute__((packed))
 #endif
 
-JS_ENUM_HEADER(JSValueType, uint8_t)
+enum JSValueType : uint8_t
 {
     JSVAL_TYPE_DOUBLE              = 0x00,
     JSVAL_TYPE_INT32               = 0x01,
     JSVAL_TYPE_BOOLEAN             = 0x02,
     JSVAL_TYPE_UNDEFINED           = 0x03,
     JSVAL_TYPE_NULL                = 0x04,
     JSVAL_TYPE_MAGIC               = 0x05,
     JSVAL_TYPE_STRING              = 0x06,
     JSVAL_TYPE_SYMBOL              = 0x07,
     JSVAL_TYPE_PRIVATE_GCTHING     = 0x08,
     JSVAL_TYPE_OBJECT              = 0x0c,
 
     /* These never appear in a jsval; they are only provided as an out-of-band value. */
     JSVAL_TYPE_UNKNOWN             = 0x20,
     JSVAL_TYPE_MISSING             = 0x21
-} JS_ENUM_FOOTER(JSValueType);
+};
 
 static_assert(sizeof(JSValueType) == 1,
               "compiler typed enum support is apparently buggy");
 
 #if defined(JS_NUNBOX32)
 
 JS_ENUM_HEADER(JSValueTag, uint32_t)
 {
@@ -100,29 +102,29 @@ JS_ENUM_HEADER(JSValueTag, uint32_t)
     JSVAL_TAG_SYMBOL               = JSVAL_TAG_MAX_DOUBLE | JSVAL_TYPE_SYMBOL,
     JSVAL_TAG_PRIVATE_GCTHING      = JSVAL_TAG_MAX_DOUBLE | JSVAL_TYPE_PRIVATE_GCTHING,
     JSVAL_TAG_OBJECT               = JSVAL_TAG_MAX_DOUBLE | JSVAL_TYPE_OBJECT
 } JS_ENUM_FOOTER(JSValueTag);
 
 static_assert(sizeof(JSValueTag) == sizeof(uint32_t),
               "compiler typed enum support is apparently buggy");
 
-JS_ENUM_HEADER(JSValueShiftedTag, uint64_t)
+enum JSValueShiftedTag : uint64_t
 {
     JSVAL_SHIFTED_TAG_MAX_DOUBLE      = ((((uint64_t)JSVAL_TAG_MAX_DOUBLE)     << JSVAL_TAG_SHIFT) | 0xFFFFFFFF),
     JSVAL_SHIFTED_TAG_INT32           = (((uint64_t)JSVAL_TAG_INT32)           << JSVAL_TAG_SHIFT),
     JSVAL_SHIFTED_TAG_UNDEFINED       = (((uint64_t)JSVAL_TAG_UNDEFINED)       << JSVAL_TAG_SHIFT),
     JSVAL_SHIFTED_TAG_NULL            = (((uint64_t)JSVAL_TAG_NULL)            << JSVAL_TAG_SHIFT),
     JSVAL_SHIFTED_TAG_BOOLEAN         = (((uint64_t)JSVAL_TAG_BOOLEAN)         << JSVAL_TAG_SHIFT),
     JSVAL_SHIFTED_TAG_MAGIC           = (((uint64_t)JSVAL_TAG_MAGIC)           << JSVAL_TAG_SHIFT),
     JSVAL_SHIFTED_TAG_STRING          = (((uint64_t)JSVAL_TAG_STRING)          << JSVAL_TAG_SHIFT),
     JSVAL_SHIFTED_TAG_SYMBOL          = (((uint64_t)JSVAL_TAG_SYMBOL)          << JSVAL_TAG_SHIFT),
     JSVAL_SHIFTED_TAG_PRIVATE_GCTHING = (((uint64_t)JSVAL_TAG_PRIVATE_GCTHING) << JSVAL_TAG_SHIFT),
     JSVAL_SHIFTED_TAG_OBJECT          = (((uint64_t)JSVAL_TAG_OBJECT)          << JSVAL_TAG_SHIFT)
-} JS_ENUM_FOOTER(JSValueShiftedTag);
+};
 
 static_assert(sizeof(JSValueShiftedTag) == sizeof(uint64_t),
               "compiler typed enum support is apparently buggy");
 
 #endif
 
 /*
  * All our supported compilers implement C++11 |enum Foo : T| syntax, so don't
@@ -166,17 +168,17 @@ static_assert(sizeof(JSValueShiftedTag) 
 // JSVAL_TYPE_OBJECT and JSVAL_TYPE_NULL differ by one bit. We can use this to
 // implement toObjectOrNull more efficiently.
 #define JSVAL_OBJECT_OR_NULL_BIT   (uint64_t(0x8) << JSVAL_TAG_SHIFT)
 static_assert((JSVAL_SHIFTED_TAG_NULL ^ JSVAL_SHIFTED_TAG_OBJECT) == JSVAL_OBJECT_OR_NULL_BIT,
               "JSVAL_OBJECT_OR_NULL_BIT must be consistent with object and null tags");
 
 #endif /* JS_PUNBOX64 */
 
-typedef enum JSWhyMagic
+enum JSWhyMagic
 {
     /** a hole in a native object's elements */
     JS_ELEMENTS_HOLE,
 
     /** there is not a pending iterator value */
     JS_NO_ITER_VALUE,
 
     /** exception value thrown when closing a generator */
@@ -223,17 +225,17 @@ typedef enum JSWhyMagic
 
     /** standard constructors are not created for off-thread parsing. */
     JS_OFF_THREAD_CONSTRUCTOR,
 
     /** for local use */
     JS_GENERIC_MAGIC,
 
     JS_WHY_MAGIC_COUNT
-} JSWhyMagic;
+};
 
 namespace js {
 static inline JS::Value PoisonedObjectValue(uintptr_t poison);
 } // namespace js
 
 namespace JS {
 
 static inline constexpr JS::Value UndefinedValue();
