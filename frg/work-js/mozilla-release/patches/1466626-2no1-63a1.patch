# HG changeset patch
# User Andre Bargull <andre.bargull@gmail.com>
# Date 1529921688 25200
# Node ID fd737ab7af6f92b160409b66dcd3f85c68b552a9
# Parent  30b2272c8ed7eb3d25c7a665136e8abb8d14316a
Bug 1466626 - Part 2: Add missing OOM handling in various places. r=jonco

diff --git a/js/src/builtin/TypedObject.cpp b/js/src/builtin/TypedObject.cpp
--- a/js/src/builtin/TypedObject.cpp
+++ b/js/src/builtin/TypedObject.cpp
@@ -2166,19 +2166,24 @@ InlineTypedObject::obj_moved(JSObject* d
 }
 
 ArrayBufferObject*
 InlineTransparentTypedObject::getOrCreateBuffer(JSContext* cx)
 {
     ObjectRealm& realm = ObjectRealm::get(this);
     if (!realm.lazyArrayBuffers) {
         auto table = cx->make_unique<ObjectWeakMap>(cx);
-        if (!table || !table->init())
+        if (!table)
             return nullptr;
 
+        if (!table->init()) {
+            ReportOutOfMemory(cx);
+            return nullptr;
+        }
+
         realm.lazyArrayBuffers = std::move(table);
     }
 
     ObjectWeakMap* table = realm.lazyArrayBuffers.get();
 
     JSObject* obj = table->lookup(this);
     if (obj)
         return &obj->as<ArrayBufferObject>();
diff --git a/js/src/frontend/Parser.cpp b/js/src/frontend/Parser.cpp
--- a/js/src/frontend/Parser.cpp
+++ b/js/src/frontend/Parser.cpp
@@ -181,17 +181,17 @@ ParseContext::Scope::dump(ParseContext* 
 bool
 ParseContext::Scope::addPossibleAnnexBFunctionBox(ParseContext* pc, FunctionBox* funbox)
 {
     if (!possibleAnnexBFunctionBoxes_) {
         if (!possibleAnnexBFunctionBoxes_.acquire(pc->sc()->context))
             return false;
     }
 
-    return possibleAnnexBFunctionBoxes_->append(funbox);
+    return maybeReportOOM(pc, possibleAnnexBFunctionBoxes_->append(funbox));
 }
 
 bool
 ParseContext::Scope::propagateAndMarkAnnexBFunctionBoxes(ParseContext* pc)
 {
     // Strict mode doesn't have wack Annex B function semantics.
     if (pc->sc()->strict() ||
         !possibleAnnexBFunctionBoxes_ ||
diff --git a/js/src/jit-test/tests/auto-regress/bug1466626-1.js b/js/src/jit-test/tests/auto-regress/bug1466626-1.js
new file mode 100644
--- /dev/null
+++ b/js/src/jit-test/tests/auto-regress/bug1466626-1.js
@@ -0,0 +1,8 @@
+if (!("oomTest" in this))
+    quit();
+
+oomTest(function() {
+    for (var i = 0; i < 10; ++i) {
+        Promise.resolve().then();
+    }
+});
diff --git a/js/src/jit-test/tests/auto-regress/bug1466626-2.js b/js/src/jit-test/tests/auto-regress/bug1466626-2.js
new file mode 100644
--- /dev/null
+++ b/js/src/jit-test/tests/auto-regress/bug1466626-2.js
@@ -0,0 +1,19 @@
+if (!("oomTest" in this))
+    quit();
+
+var globals = [];
+for (var i = 0; i < 24; ++i) {
+    var g = newGlobal();
+    g.eval(`
+        function f(){}
+        var env = {};
+    `);
+    globals.push(g);
+}
+
+var i = 0;
+oomTest(function() {
+    globals[(i++) % globals.length].eval(`
+        this.clone(this.f, this.env);
+    `);
+});
diff --git a/js/src/jit-test/tests/auto-regress/bug1466626-3.js b/js/src/jit-test/tests/auto-regress/bug1466626-3.js
new file mode 100644
--- /dev/null
+++ b/js/src/jit-test/tests/auto-regress/bug1466626-3.js
@@ -0,0 +1,14 @@
+if (!("oomTest" in this))
+    quit();
+
+var g = newGlobal();
+
+var i = 0;
+oomTest(function() {
+    var s = String.fromCharCode((i++) & 0xffff);
+    var r = "";
+    for (var j = 0; j < 1000; ++j) {
+        r = s + r;
+    }
+    g.String.prototype.toString.call(r);
+});
diff --git a/js/src/jit-test/tests/auto-regress/bug1466626-4.js b/js/src/jit-test/tests/auto-regress/bug1466626-4.js
new file mode 100644
--- /dev/null
+++ b/js/src/jit-test/tests/auto-regress/bug1466626-4.js
@@ -0,0 +1,11 @@
+if (!("oomTest" in this))
+    quit();
+
+var source = "{";
+for (var i = 0; i < 120; ++i)
+    source += `function f${i}(){}`
+source += "}";
+
+oomTest(function() {
+    Function(source);
+});
diff --git a/js/src/jsapi.cpp b/js/src/jsapi.cpp
--- a/js/src/jsapi.cpp
+++ b/js/src/jsapi.cpp
@@ -7020,18 +7020,20 @@ JSErrorNotes::addNoteASCII(JSContext* cx
     va_list ap;
     va_start(ap, errorNumber);
     auto note = CreateErrorNoteVA(cx, filename, lineno, column, errorCallback, userRef,
                                   errorNumber, ArgumentsAreASCII, ap);
     va_end(ap);
 
     if (!note)
         return false;
-    if (!notes_.append(std::move(note)))
-        return false;
+    if (!notes_.append(std::move(note))) {
+        ReportOutOfMemory(cx);
+        return false;
+    }
     return true;
 }
 
 bool
 JSErrorNotes::addNoteLatin1(JSContext* cx,
                             const char* filename, unsigned lineno, unsigned column,
                             JSErrorCallback errorCallback, void* userRef,
                             const unsigned errorNumber, ...)
@@ -7039,18 +7041,20 @@ JSErrorNotes::addNoteLatin1(JSContext* c
     va_list ap;
     va_start(ap, errorNumber);
     auto note = CreateErrorNoteVA(cx, filename, lineno, column, errorCallback, userRef,
                                   errorNumber, ArgumentsAreLatin1, ap);
     va_end(ap);
 
     if (!note)
         return false;
-    if (!notes_.append(std::move(note)))
-        return false;
+    if (!notes_.append(std::move(note))) {
+        ReportOutOfMemory(cx);
+        return false;
+    }
     return true;
 }
 
 bool
 JSErrorNotes::addNoteUTF8(JSContext* cx,
                           const char* filename, unsigned lineno, unsigned column,
                           JSErrorCallback errorCallback, void* userRef,
                           const unsigned errorNumber, ...)
@@ -7058,18 +7062,20 @@ JSErrorNotes::addNoteUTF8(JSContext* cx,
     va_list ap;
     va_start(ap, errorNumber);
     auto note = CreateErrorNoteVA(cx, filename, lineno, column, errorCallback, userRef,
                                   errorNumber, ArgumentsAreUTF8, ap);
     va_end(ap);
 
     if (!note)
         return false;
-    if (!notes_.append(std::move(note)))
-        return false;
+    if (!notes_.append(std::move(note))) {
+        ReportOutOfMemory(cx);
+        return false;
+    }
     return true;
 }
 
 JS_PUBLIC_API(size_t)
 JSErrorNotes::length()
 {
     return notes_.length();
 }
diff --git a/js/src/shell/js.cpp b/js/src/shell/js.cpp
--- a/js/src/shell/js.cpp
+++ b/js/src/shell/js.cpp
@@ -5779,31 +5779,44 @@ ConsumeBufferSource(JSContext* cx, JS::H
     SharedMem<uint8_t*> dataPointer;
     size_t byteLength;
     if (!IsBufferSource(obj, &dataPointer, &byteLength)) {
         JS_ReportErrorASCII(cx, "shell streaming consumes a buffer source (buffer or view)");
         return false;
     }
 
     auto job = cx->make_unique<BufferStreamJob>(consumer);
-    if (!job || !job->bytes.resize(byteLength))
-        return false;
+    if (!job)
+        return false;
+
+    if (!job->bytes.resize(byteLength)) {
+        JS_ReportOutOfMemory(cx);
+        return false;
+    }
 
     memcpy(job->bytes.begin(), dataPointer.unwrap(), byteLength);
 
     BufferStreamJob* jobPtr = job.get();
 
     {
         auto state = bufferStreamState->lock();
         MOZ_ASSERT(!state->shutdown);
-        if (!state->jobs.append(std::move(job)))
-            return false;
-    }
-
-    return jobPtr->thread.init(BufferStreamMain, jobPtr);
+        if (!state->jobs.append(std::move(job))) {
+            JS_ReportOutOfMemory(cx);
+            return false;
+        }
+    }
+
+    {
+        AutoEnterOOMUnsafeRegion oomUnsafe;
+        if (!jobPtr->thread.init(BufferStreamMain, jobPtr))
+            oomUnsafe.crash("ConsumeBufferSource");
+    }
+
+    return true;
 }
 
 static bool
 SetBufferStreamParams(JSContext* cx, unsigned argc, Value* vp)
 {
     CallArgs args = CallArgsFromVp(argc, vp);
     if (!args.requireAtLeast(cx, "setBufferStreamParams", 2))
         return false;
diff --git a/js/src/vm/JSContext.cpp b/js/src/vm/JSContext.cpp
--- a/js/src/vm/JSContext.cpp
+++ b/js/src/vm/JSContext.cpp
@@ -1027,17 +1027,21 @@ JSContext::recoverFromOutOfMemory()
 }
 
 static bool
 InternalEnqueuePromiseJobCallback(JSContext* cx, JS::HandleObject job,
                                   JS::HandleObject allocationSite,
                                   JS::HandleObject incumbentGlobal, void* data)
 {
     MOZ_ASSERT(job);
-    return cx->jobQueue->append(job);
+    if (!cx->jobQueue->append(job)) {
+        ReportOutOfMemory(cx);
+        return false;
+    }
+    return true;
 }
 
 namespace {
 class MOZ_STACK_CLASS ReportExceptionClosure : public ScriptEnvironmentPreparer::Closure
 {
   public:
     explicit ReportExceptionClosure(HandleValue exn)
         : exn_(exn)
diff --git a/js/src/vm/JSScript.cpp b/js/src/vm/JSScript.cpp
--- a/js/src/vm/JSScript.cpp
+++ b/js/src/vm/JSScript.cpp
@@ -3748,30 +3748,39 @@ JSScript::destroyDebugScript(FreeOp* fop
 bool
 JSScript::ensureHasDebugScript(JSContext* cx)
 {
     if (bitFields_.hasDebugScript_)
         return true;
 
     size_t nbytes = offsetof(DebugScript, breakpoints) + length() * sizeof(BreakpointSite*);
     UniqueDebugScript debug(reinterpret_cast<DebugScript*>(zone()->pod_calloc<uint8_t>(nbytes)));
-    if (!debug)
+    if (!debug) {
+        ReportOutOfMemory(cx);
         return false;
+    }
 
     /* Create realm's debugScriptMap if necessary. */
     if (!realm()->debugScriptMap) {
         auto map = cx->make_unique<DebugScriptMap>();
-        if (!map || !map->init())
+        if (!map)
             return false;
 
+        if (!map->init()) {
+            ReportOutOfMemory(cx);
+            return false;
+        }
+
         realm()->debugScriptMap = std::move(map);
     }
 
-    if (!realm()->debugScriptMap->putNew(this, std::move(debug)))
+    if (!realm()->debugScriptMap->putNew(this, std::move(debug))) {
+        ReportOutOfMemory(cx);
         return false;
+    }
 
     bitFields_.hasDebugScript_ = true; // safe to set this;  we can't fail after this point
 
     /*
      * Ensure that any Interpret() instances running on this script have
      * interrupts enabled. The interrupts must stay enabled until the
      * debug state is destroyed.
      */
diff --git a/js/src/vm/Realm.cpp b/js/src/vm/Realm.cpp
--- a/js/src/vm/Realm.cpp
+++ b/js/src/vm/Realm.cpp
@@ -200,19 +200,24 @@ struct CheckGCThingAfterMovingGCFunctor 
 
 LexicalEnvironmentObject*
 ObjectRealm::getOrCreateNonSyntacticLexicalEnvironment(JSContext* cx, HandleObject enclosing)
 {
     MOZ_ASSERT(&ObjectRealm::get(enclosing) == this);
 
     if (!nonSyntacticLexicalEnvironments_) {
         auto map = cx->make_unique<ObjectWeakMap>(cx);
-        if (!map || !map->init())
+        if (!map)
             return nullptr;
 
+        if (!map->init()) {
+            ReportOutOfMemory(cx);
+            return nullptr;
+        }
+
         nonSyntacticLexicalEnvironments_ = std::move(map);
     }
 
     // If a wrapped WithEnvironmentObject was passed in, unwrap it, as we may
     // be creating different WithEnvironmentObject wrappers each time.
     RootedObject key(cx, enclosing);
     if (enclosing->is<WithEnvironmentObject>()) {
         MOZ_ASSERT(!enclosing->as<WithEnvironmentObject>().isSyntactic());
diff --git a/js/src/vm/StringType.cpp b/js/src/vm/StringType.cpp
--- a/js/src/vm/StringType.cpp
+++ b/js/src/vm/StringType.cpp
@@ -275,65 +275,68 @@ AllocChars(JSString* str, size_t length,
     *capacity = numChars - 1;
 
     JS_STATIC_ASSERT(JSString::MAX_LENGTH * sizeof(CharT) < UINT32_MAX);
     *chars = str->zone()->pod_malloc<CharT>(numChars);
     return *chars != nullptr;
 }
 
 UniquePtr<Latin1Char[], JS::FreePolicy>
-JSRope::copyLatin1CharsZ(JSContext* cx) const
+JSRope::copyLatin1CharsZ(JSContext* maybecx) const
 {
-    return copyCharsInternal<Latin1Char>(cx, true);
+    return copyCharsInternal<Latin1Char>(maybecx, true);
 }
 
 UniqueTwoByteChars
-JSRope::copyTwoByteCharsZ(JSContext* cx) const
+JSRope::copyTwoByteCharsZ(JSContext* maybecx) const
 {
-    return copyCharsInternal<char16_t>(cx, true);
+    return copyCharsInternal<char16_t>(maybecx, true);
 }
 
 UniquePtr<Latin1Char[], JS::FreePolicy>
-JSRope::copyLatin1Chars(JSContext* cx) const
+JSRope::copyLatin1Chars(JSContext* maybecx) const
 {
-    return copyCharsInternal<Latin1Char>(cx, false);
+    return copyCharsInternal<Latin1Char>(maybecx, false);
 }
 
 UniqueTwoByteChars
-JSRope::copyTwoByteChars(JSContext* cx) const
+JSRope::copyTwoByteChars(JSContext* maybecx) const
 {
-    return copyCharsInternal<char16_t>(cx, false);
+    return copyCharsInternal<char16_t>(maybecx, false);
 }
 
 template <typename CharT>
 UniquePtr<CharT[], JS::FreePolicy>
-JSRope::copyCharsInternal(JSContext* cx, bool nullTerminate) const
+JSRope::copyCharsInternal(JSContext* maybecx, bool nullTerminate) const
 {
     // Left-leaning ropes are far more common than right-leaning ropes, so
     // perform a non-destructive traversal of the rope, right node first,
     // splatting each node's characters into a contiguous buffer.
 
     size_t n = length();
 
     UniquePtr<CharT[], JS::FreePolicy> out;
-    if (cx)
-        out.reset(cx->pod_malloc<CharT>(n + 1));
+    if (maybecx)
+        out.reset(maybecx->pod_malloc<CharT>(n + 1));
     else
         out.reset(js_pod_malloc<CharT>(n + 1));
 
     if (!out)
         return nullptr;
 
     Vector<const JSString*, 8, SystemAllocPolicy> nodeStack;
     const JSString* str = this;
     CharT* end = out.get() + str->length();
     while (true) {
         if (str->isRope()) {
-            if (!nodeStack.append(str->asRope().leftChild()))
+            if (!nodeStack.append(str->asRope().leftChild())) {
+                if (maybecx)
+                    ReportOutOfMemory(maybecx);
                 return nullptr;
+            }
             str = str->asRope().rightChild();
         } else {
             end -= str->length();
             CopyChars(end, str->asLinear());
             if (nodeStack.empty())
                 break;
             str = nodeStack.popCopy();
         }
diff --git a/js/src/vm/StringType.h b/js/src/vm/StringType.h
--- a/js/src/vm/StringType.h
+++ b/js/src/vm/StringType.h
@@ -685,24 +685,24 @@ class JSRope : public JSString
 
   public:
     template <js::AllowGC allowGC>
     static inline JSRope* new_(JSContext* cx,
                                typename js::MaybeRooted<JSString*, allowGC>::HandleType left,
                                typename js::MaybeRooted<JSString*, allowGC>::HandleType right,
                                size_t length, js::gc::InitialHeap = js::gc::DefaultHeap);
 
-    js::UniquePtr<JS::Latin1Char[], JS::FreePolicy> copyLatin1Chars(JSContext* cx) const;
-    JS::UniqueTwoByteChars copyTwoByteChars(JSContext* cx) const;
+    js::UniquePtr<JS::Latin1Char[], JS::FreePolicy> copyLatin1Chars(JSContext* maybecx) const;
+    JS::UniqueTwoByteChars copyTwoByteChars(JSContext* maybecx) const;
 
-    js::UniquePtr<JS::Latin1Char[], JS::FreePolicy> copyLatin1CharsZ(JSContext* cx) const;
-    JS::UniqueTwoByteChars copyTwoByteCharsZ(JSContext* cx) const;
+    js::UniquePtr<JS::Latin1Char[], JS::FreePolicy> copyLatin1CharsZ(JSContext* maybecx) const;
+    JS::UniqueTwoByteChars copyTwoByteCharsZ(JSContext* maybecx) const;
 
     template <typename CharT>
-    js::UniquePtr<CharT[], JS::FreePolicy> copyChars(JSContext* cx) const;
+    js::UniquePtr<CharT[], JS::FreePolicy> copyChars(JSContext* maybecx) const;
 
     // Hash function specific for ropes that avoids allocating a temporary
     // string. There are still allocations internally so it's technically
     // fallible.
     //
     // Returns the same value as if this were a linear string being hashed.
     MOZ_MUST_USE bool hash(uint32_t* outhHash) const;
 
@@ -1734,26 +1734,26 @@ template<>
 MOZ_ALWAYS_INLINE const JS::Latin1Char*
 JSLinearString::chars(const JS::AutoRequireNoGC& nogc) const
 {
     return rawLatin1Chars();
 }
 
 template <>
 MOZ_ALWAYS_INLINE js::UniquePtr<JS::Latin1Char[], JS::FreePolicy>
-JSRope::copyChars<JS::Latin1Char>(JSContext* cx) const
+JSRope::copyChars<JS::Latin1Char>(JSContext* maybecx) const
 {
-    return copyLatin1Chars(cx);
+    return copyLatin1Chars(maybecx);
 }
 
 template <>
 MOZ_ALWAYS_INLINE JS::UniqueTwoByteChars
-JSRope::copyChars<char16_t>(JSContext* cx) const
+JSRope::copyChars<char16_t>(JSContext* maybecx) const
 {
-    return copyTwoByteChars(cx);
+    return copyTwoByteChars(maybecx);
 }
 
 template<>
 MOZ_ALWAYS_INLINE bool
 JSThinInlineString::lengthFits<JS::Latin1Char>(size_t length)
 {
     return length <= MAX_LENGTH_LATIN1;
 }
