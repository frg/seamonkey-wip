# HG changeset patch
# User Markus Stange <mstange@themasta.com>
# Date 1517948945 18000
# Node ID 8e6d2f2f7f30c9a6ad78cb33544f855c9dad6b18
# Parent  ec0b493bf1bf82d5ae82365611cbcb6d4ee8aadb
Bug 1431179 - Keep all exit profiles that overlap with the parent process's buffer time range. r=mconley

MozReview-Commit-ID: 1DIYQZ70ckR

diff --git a/tools/profiler/gecko/nsProfiler.cpp b/tools/profiler/gecko/nsProfiler.cpp
--- a/tools/profiler/gecko/nsProfiler.cpp
+++ b/tools/profiler/gecko/nsProfiler.cpp
@@ -534,43 +534,35 @@ nsProfiler::GatheredOOPProfile(const nsA
 
   if (mPendingProfiles == 0) {
     // We've got all of the async profiles now. Let's
     // finish off the profile and resolve the Promise.
     FinishGathering();
   }
 }
 
-// When a subprocess exits before we've gathered profiles, we'll store profiles
-// for those processes until gathering starts. We'll only store up to
-// MAX_SUBPROCESS_EXIT_PROFILES. The buffer is circular, so as soon as we
-// receive another exit profile, we'll bump the oldest one out of the buffer.
-static const uint32_t MAX_SUBPROCESS_EXIT_PROFILES = 5;
-
 void
 nsProfiler::ReceiveShutdownProfile(const nsCString& aProfile)
 {
   MOZ_RELEASE_ASSERT(NS_IsMainThread());
 
   Maybe<ProfilerBufferInfo> bufferInfo = profiler_get_buffer_info();
   if (!bufferInfo) {
     // The profiler is not running. Discard the profile.
     return;
   }
 
-  // Append the exit profile to mExitProfiles so that it can be picked up the
-  // next time a profile is requested. If we're currently gathering a profile,
-  // do not add this exit profile to it; chances are that we already have a
-  // profile from the exiting process and we don't want another one.
-  // We only keep around at most MAX_SUBPROCESS_EXIT_PROFILES exit profiles.
-  if (mExitProfiles.Length() >= MAX_SUBPROCESS_EXIT_PROFILES) {
-    mExitProfiles.RemoveElementAt(0);
-  }
+  // Append the exit profile to mExitProfiles so that it can be picked up when
+  // a profile is requested.
   uint64_t bufferPosition = bufferInfo->mRangeEnd;
   mExitProfiles.AppendElement(ExitProfile{ aProfile, bufferPosition });
+
+  // This is a good time to clear out exit profiles whose time ranges have no
+  // overlap with this process's profile buffer contents any more.
+  ClearExpiredExitProfiles();
 }
 
 RefPtr<nsProfiler::GatheringPromise>
 nsProfiler::StartGathering(double aSinceTime)
 {
   MOZ_RELEASE_ASSERT(NS_IsMainThread());
 
   if (mGathering) {
@@ -587,45 +579,37 @@ nsProfiler::StartGathering(double aSince
   // Do this before the call to profiler_stream_json_for_this_process() because
   // that call is slow and we want to let the other processes grab their
   // profiles as soon as possible.
   nsTArray<RefPtr<ProfilerParent::SingleProcessProfilePromise>> profiles =
     ProfilerParent::GatherProfiles();
 
   mWriter.emplace();
 
-  Maybe<ProfilerBufferInfo> bufferInfo = profiler_get_buffer_info();
-
   // Start building up the JSON result and grab the profile from this process.
   mWriter->Start();
   if (!profiler_stream_json_for_this_process(*mWriter, aSinceTime,
                                              /* aIsShuttingDown */ false)) {
     // The profiler is inactive. This either means that it was inactive even
     // at the time that ProfileGatherer::Start() was called, or that it was
     // stopped on a different thread since that call. Either way, we need to
     // reject the promise and stop gathering.
     return GatheringPromise::CreateAndReject(NS_ERROR_NOT_AVAILABLE, __func__);
   }
 
   mWriter->StartArrayProperty("processes");
 
-  // If we have any process exit profiles, add them immediately, and clear
-  // mExitProfiles.
+  ClearExpiredExitProfiles();
+
+  // If we have any process exit profiles, add them immediately.
   for (auto& exitProfile : mExitProfiles) {
-    if (bufferInfo &&
-        exitProfile.mBufferPositionAtGatherTime < bufferInfo->mRangeStart) {
-      // Don't include exit profiles that have no overlap with the profile
-      // from our own process.
-      continue;
-    }
     if (!exitProfile.mJSON.IsEmpty()) {
       mWriter->Splice(exitProfile.mJSON.get());
     }
   }
-  mExitProfiles.Clear();
 
   mPromiseHolder.emplace();
   RefPtr<GatheringPromise> promise = mPromiseHolder->Ensure(__func__);
 
   // Keep the array property "processes" and the root object in mWriter open
   // until FinishGathering() is called. As profiles from the other processes
   // come in, they will be inserted and end up in the right spot.
   // FinishGathering() will close the array and the root object.
@@ -671,8 +655,20 @@ nsProfiler::FinishGathering()
 void
 nsProfiler::ResetGathering()
 {
   mPromiseHolder.reset();
   mPendingProfiles = 0;
   mGathering = false;
   mWriter.reset();
 }
+
+void
+nsProfiler::ClearExpiredExitProfiles()
+{
+  Maybe<ProfilerBufferInfo> bufferInfo = profiler_get_buffer_info();
+  MOZ_RELEASE_ASSERT(bufferInfo, "the profiler should be running at the moment");
+  uint64_t bufferRangeStart = bufferInfo->mRangeStart;
+  // Discard any exit profiles that were gathered before bufferRangeStart.
+  mExitProfiles.RemoveElementsBy([bufferRangeStart](ExitProfile& aExitProfile){
+    return aExitProfile.mBufferPositionAtGatherTime < bufferRangeStart;
+  });
+}
\ No newline at end of file
diff --git a/tools/profiler/gecko/nsProfiler.h b/tools/profiler/gecko/nsProfiler.h
--- a/tools/profiler/gecko/nsProfiler.h
+++ b/tools/profiler/gecko/nsProfiler.h
@@ -7,16 +7,17 @@
 #ifndef nsProfiler_h
 #define nsProfiler_h
 
 #include "nsIProfiler.h"
 #include "nsIObserver.h"
 #include "mozilla/Attributes.h"
 #include "mozilla/Maybe.h"
 #include "mozilla/MozPromise.h"
+#include "mozilla/TimeStamp.h"
 #include "nsServiceManagerUtils.h"
 #include "ProfileJSONWriter.h"
 
 class nsProfiler final : public nsIProfiler, public nsIObserver
 {
 public:
   nsProfiler();
 
@@ -39,16 +40,18 @@ private:
   ~nsProfiler();
 
   typedef mozilla::MozPromise<nsCString, nsresult, false> GatheringPromise;
 
   RefPtr<GatheringPromise> StartGathering(double aSinceTime);
   void FinishGathering();
   void ResetGathering();
 
+  void ClearExpiredExitProfiles();
+
   bool mLockedForPrivateBrowsing;
 
   struct ExitProfile {
     nsCString mJSON;
     uint64_t mBufferPositionAtGatherTime;
   };
 
   // These fields are all related to profile gathering.

