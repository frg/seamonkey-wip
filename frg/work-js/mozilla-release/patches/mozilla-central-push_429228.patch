# HG changeset patch
# User Boris Zbarsky <bzbarsky@mit.edu>
# Date 1532965904 14400
#      Mon Jul 30 11:51:44 2018 -0400
# Node ID 3895fde69249ee14f4f3d55aafe090bcfd0d6cb0
# Parent  3db40ff608ebd9188a3e85c6e5f92c1305a752fb
Bug 1347999.  Annotate xpidl methods/attributes that can be implemented in script as JS_HAZ_CAN_RUN_SCRIPT. r=froydnj

diff --git a/js/public/GCAnnotations.h b/js/public/GCAnnotations.h
--- a/js/public/GCAnnotations.h
+++ b/js/public/GCAnnotations.h
@@ -37,21 +37,26 @@
 
 // Mark a function as something that runs a garbage collection, potentially
 // invalidating GC pointers.
 # define JS_HAZ_GC_CALL __attribute__((tag("GC Call")))
 
 // Mark an RAII class as suppressing GC within its scope.
 # define JS_HAZ_GC_SUPPRESSED __attribute__((tag("Suppress GC")))
 
+// Mark a function as one that can run script if called.  This obviously
+// subsumes JS_HAZ_GC_CALL, since anything that can run script can GC.`
+# define JS_HAZ_CAN_RUN_SCRIPT __attribute__((tag("Can run script")))
+
 #else
 
 # define JS_HAZ_GC_THING
 # define JS_HAZ_GC_POINTER
 # define JS_HAZ_ROOTED
 # define JS_HAZ_GC_INVALIDATED
 # define JS_HAZ_NON_GC_POINTER
 # define JS_HAZ_GC_CALL
 # define JS_HAZ_GC_SUPPRESSED
+# define JS_HAZ_CAN_RUN_SCRIPT
 
 #endif
 
 #endif /* js_GCAnnotations_h */
diff --git a/xpcom/idl-parser/xpidl/header.py b/xpcom/idl-parser/xpidl/header.py
--- a/xpcom/idl-parser/xpidl/header.py
+++ b/xpcom/idl-parser/xpidl/header.py
@@ -129,16 +129,25 @@ def paramlistAsNative(m, empty='void'):
             paramIter -= 1
 
     if len(l) == 0:
         return empty
 
     return ", ".join(l)
 
 
+def memberCanRunScript(member):
+    # This can only happen if the member is scriptable and its interface is not builtinclass.
+    return member.isScriptable() and not member.iface.attributes.builtinclass
+
+
+def runScriptAnnotation(member):
+    return "JS_HAZ_CAN_RUN_SCRIPT " if memberCanRunScript(member) else ""
+
+
 def paramAsNative(p):
     default_spec = ''
     if p.default_value:
         default_spec = " = " + p.default_value
     return "%s%s%s" % (p.nativeType(),
                        p.name,
                        default_spec)
 
@@ -179,16 +188,20 @@ jsvalue_include = """
 """
 
 infallible_includes = """
 #include "mozilla/AlreadyAddRefed.h"
 #include "mozilla/Assertions.h"
 #include "mozilla/DebugOnly.h"
 """
 
+can_run_script_includes = """
+#include "js/GCAnnotations.h"
+"""
+
 header_end = """/* For IDL files that don't want to include root IDL files. */
 #ifndef NS_NO_VTABLE
 #define NS_NO_VTABLE
 #endif
 """
 
 footer = """
 #endif /* __gen_%(basename)s_h__ */
@@ -214,22 +227,33 @@ def print_header(idl, fd, filename):
             foundinc = True
             fd.write('\n')
         fd.write(include % {'basename': idl_basename(inc.filename)})
 
     if idl.needsJSTypes():
         fd.write(jsvalue_include)
 
     # Include some extra files if any attributes are infallible.
-    for iface in [p for p in idl.productions if p.kind == 'interface']:
-        for attr in [m for m in iface.members if isinstance(m, xpidl.Attribute)]:
+    interfaces = [p for p in idl.productions if p.kind == 'interface']
+    wroteRunScriptIncludes = False
+    for iface in interfaces:
+        attrs = [m for m in iface.members if isinstance(m, xpidl.Attribute)]
+        for attr in attrs:
             if attr.infallible:
                 fd.write(infallible_includes)
                 break
 
+        if not wroteRunScriptIncludes:
+            methods = [m for m in iface.members if isinstance(m, xpidl.Method)]
+            for member in itertools.chain(attrs, methods):
+                if memberCanRunScript(member):
+                    fd.write(can_run_script_includes)
+                    wroteRunScriptIncludes = True
+                    break
+
     fd.write('\n')
     fd.write(header_end)
 
     for p in idl.productions:
         if p.kind == 'include':
             continue
         if p.kind == 'cdata':
             fd.write(p.data)
@@ -365,40 +389,43 @@ def write_interface(iface, fd):
                          'signed': (not basetype.signed) and 'U' or ''})
         fd.write(",\n".join(enums))
         fd.write("\n  };\n\n")
 
     def write_method_decl(m):
         printComments(fd, m.doccomments, '  ')
 
         fd.write("  /* %s */\n" % m.toIDL())
-        fd.write("  %s = 0;\n\n" % methodAsNative(m))
+        fd.write("  %s%s = 0;\n\n" % (runScriptAnnotation(m),
+                                      methodAsNative(m)))
 
     def write_attr_decl(a):
         printComments(fd, a.doccomments, '  ')
 
         fd.write("  /* %s */\n" % a.toIDL())
 
-        fd.write("  %s = 0;\n" % attributeAsNative(a, True))
+        fd.write("  %s%s = 0;\n" % (runScriptAnnotation(a),
+                                    attributeAsNative(a, True)))
         if a.infallible:
             realtype = a.realtype.nativeType('in')
             tmpl = attr_builtin_infallible_tmpl
 
             if a.realtype.kind != 'builtin':
                 assert realtype.endswith(' *'), "bad infallible type"
                 tmpl = attr_refcnt_infallible_tmpl
                 realtype = realtype[:-2]  # strip trailing pointer
 
             fd.write(tmpl % {'realtype': realtype,
                              'nativename': attributeNativeName(a, getter=True),
                              'args': '' if not a.implicit_jscontext else 'JSContext* cx',
                              'argnames': '' if not a.implicit_jscontext else 'cx, '})
 
         if not a.readonly:
-            fd.write("  %s = 0;\n" % attributeAsNative(a, False))
+            fd.write("  %s%s = 0;\n" % (runScriptAnnotation(a),
+                                        attributeAsNative(a, False)))
         fd.write("\n")
 
     defname = iface.name.upper()
     if iface.name[0:2] == 'ns':
         defname = 'NS_' + defname[2:]
 
     names = uuid_decoder.match(iface.attributes.uuid).groupdict()
     m3str = names['m3'] + names['m4']
diff --git a/xpcom/idl-parser/xpidl/runtests.py b/xpcom/idl-parser/xpidl/runtests.py
--- a/xpcom/idl-parser/xpidl/runtests.py
+++ b/xpcom/idl-parser/xpidl/runtests.py
@@ -97,16 +97,17 @@ attribute long bar;
         self.assertEqual(xpidl.TypeId("long"), a.type)
 
     def testOverloadedVirtual(self):
         i = self.p.parse("""[uuid(abc)] interface foo {
 attribute long bar;
 void getBar();
 };""", filename='f')
         self.assertTrue(isinstance(i, xpidl.IDL))
+        i.resolve([], self.p, {})
 
         class FdMock:
             def write(self, s):
                 pass
         try:
             header.print_header(i, FdMock(), filename='f')
         except Exception as e:
             self.assertEqual(
