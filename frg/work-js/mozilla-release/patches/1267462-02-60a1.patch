# HG changeset patch
# User Daniel Holbert <dholbert@cs.stanford.edu>
# Date 1518123683 28800
#      Thu Feb 08 13:01:23 2018 -0800
# Node ID 19aed0c8a84855b70af0c7fe74ac5af0cc07ab3b
# Parent  b370336ae1263515fe3c4401e7b8477de25d8ead
Bug 1267462 part 2: Add a reftest for flex items with text & orthogonal flows. r=mats

MozReview-Commit-ID: FrxV0JlhTRP

diff --git a/layout/reftests/w3c-css/submitted/flexbox/flexbox-writing-mode-010-ref.html b/layout/reftests/w3c-css/submitted/flexbox/flexbox-writing-mode-010-ref.html
new file mode 100644
--- /dev/null
+++ b/layout/reftests/w3c-css/submitted/flexbox/flexbox-writing-mode-010-ref.html
@@ -0,0 +1,94 @@
+<!DOCTYPE html>
+<!--
+     Any copyright is dedicated to the Public Domain.
+     http://creativecommons.org/publicdomain/zero/1.0/
+-->
+<html>
+<head>
+  <title>CSS Reftest Reference</title>
+  <meta charset="utf-8">
+  <link rel="author" title="Daniel Holbert" href="mailto:dholbert@mozilla.com">
+  <link rel="stylesheet" type="text/css" href="support/ahem.css">
+  <style>
+  .container {
+    display: block;
+    border: 2px solid purple;
+    padding: 2px;
+    margin-bottom: 2em;
+    height: 150px;
+    width: 500px;
+  }
+
+  span {
+    display: block;
+    background: lightgrey;
+    border: 2px solid black;
+    /* If browser supports it, signal the inline direction with border color: */
+    border-block-start-color: orange;
+    border-inline-start-color: lime;
+
+    margin: 11px 13px 17px 7px;
+    inline-size: 6px;
+  }
+
+  .small { font: 12px Ahem; }
+  .big   { font: 20px Ahem; }
+
+  .hl  { writing-mode: horizontal-tb;  direction: ltr; }
+  .hr  { writing-mode: horizontal-tb;  direction: rtl; }
+  .vl  { writing-mode: vertical-lr;    direction: ltr; }
+  .vr  { writing-mode: vertical-rl;    direction: ltr; }
+  .vl_rtl { writing-mode: vertical-lr; direction: rtl; }
+  .vr_rtl { writing-mode: vertical-rl; direction: rtl; }
+
+  .container > .hl, .container > .hr {
+    /* In the testcase, these items are stretched vertically
+       via the default "align-self:stretch" behavior, and because
+       they have a height of "auto".
+       (The rest of the items have a non-auto height from "inline-size"
+       and their vertical writing-mode, so those ones do not stretch.) */
+    height: 118px;
+  }
+
+  .container.hl > * { float: left; }
+  .container.hr > * { float: right; }
+
+  </style>
+</head>
+<body>
+
+<div class="container hl">
+  <span class="hl small">a b c</span>
+  <span class="hl big">d e</span>
+  <span class="hr small">a b c</span>
+  <span class="hr big">d e</span>
+  <span class="vl small">a b c</span>
+  <span class="vl big">d e</span>
+</div>
+<div class="container hl">
+  <span class="vr small">a b c</span>
+  <span class="vr big">d e</span>
+  <span class="vl_rtl small">a b c</span>
+  <span class="vl_rtl big">d e</span>
+  <span class="vr_rtl small">a b c</span>
+  <span class="vr_rtl big">d e</span>
+</div>
+<div class="container hr">
+  <span class="hl small">a b c</span>
+  <span class="hl big">d e</span>
+  <span class="hr small">a b c</span>
+  <span class="hr big">d e</span>
+  <span class="vl small">a b c</span>
+  <span class="vl big">d e</span>
+</div>
+<div class="container hr">
+  <span class="vr small">a b c</span>
+  <span class="vr big">d e</span>
+  <span class="vl_rtl small">a b c</span>
+  <span class="vl_rtl big">d e</span>
+  <span class="vr_rtl small">a b c</span>
+  <span class="vr_rtl big">d e</span>
+</div>
+
+</body>
+</html>
diff --git a/layout/reftests/w3c-css/submitted/flexbox/flexbox-writing-mode-010.html b/layout/reftests/w3c-css/submitted/flexbox/flexbox-writing-mode-010.html
new file mode 100644
--- /dev/null
+++ b/layout/reftests/w3c-css/submitted/flexbox/flexbox-writing-mode-010.html
@@ -0,0 +1,88 @@
+<!DOCTYPE html>
+<!--
+     Any copyright is dedicated to the Public Domain.
+     http://creativecommons.org/publicdomain/zero/1.0/
+-->
+<html>
+<head>
+  <title>
+    CSS Test: Testing a mix of flex items with various values for
+    'writing-mode' / 'direction' in a horizontal row-oriented flex container.
+  </title>
+  <meta charset="utf-8">
+  <link rel="author" title="Daniel Holbert" href="mailto:dholbert@mozilla.com">
+  <link rel="help" href="https://www.w3.org/TR/css-flexbox-1/#flex-direction-property">
+  <link rel="help" href="https://www.w3.org/TR/css-writing-modes-3/#propdef-writing-mode">
+  <link rel="match" href="flexbox-writing-mode-010-ref.html">
+  <link rel="stylesheet" type="text/css" href="support/ahem.css">
+  <style>
+  .container {
+    display: flex;
+    flex-direction: row;
+    border: 2px solid purple;
+    padding: 2px;
+    margin-bottom: 2em;
+    height: 150px;
+    width: 500px;
+  }
+
+  span {
+    display: block;
+    background: lightgrey;
+    border: 2px solid black;
+    /* If browser supports it, signal the inline direction with border color: */
+    border-block-start-color: orange;
+    border-inline-start-color: lime;
+
+    margin: 11px 13px 17px 7px;
+    inline-size: 6px;
+  }
+
+  .small { font: 12px Ahem; }
+  .big   { font: 20px Ahem; }
+
+  .hl  { writing-mode: horizontal-tb;  direction: ltr; }
+  .hr  { writing-mode: horizontal-tb;  direction: rtl; }
+  .vl  { writing-mode: vertical-lr;    direction: ltr; }
+  .vr  { writing-mode: vertical-rl;    direction: ltr; }
+  .vl_rtl { writing-mode: vertical-lr; direction: rtl; }
+  .vr_rtl { writing-mode: vertical-rl; direction: rtl; }
+  </style>
+</head>
+<body>
+
+<div class="container hl">
+  <span class="hl small">a b c</span>
+  <span class="hl big">d e</span>
+  <span class="hr small">a b c</span>
+  <span class="hr big">d e</span>
+  <span class="vl small">a b c</span>
+  <span class="vl big">d e</span>
+</div>
+<div class="container hl">
+  <span class="vr small">a b c</span>
+  <span class="vr big">d e</span>
+  <span class="vl_rtl small">a b c</span>
+  <span class="vl_rtl big">d e</span>
+  <span class="vr_rtl small">a b c</span>
+  <span class="vr_rtl big">d e</span>
+</div>
+<div class="container hr">
+  <span class="hl small">a b c</span>
+  <span class="hl big">d e</span>
+  <span class="hr small">a b c</span>
+  <span class="hr big">d e</span>
+  <span class="vl small">a b c</span>
+  <span class="vl big">d e</span>
+</div>
+<div class="container hr">
+  <span class="vr small">a b c</span>
+  <span class="vr big">d e</span>
+  <span class="vl_rtl small">a b c</span>
+  <span class="vl_rtl big">d e</span>
+  <span class="vr_rtl small">a b c</span>
+  <span class="vr_rtl big">d e</span>
+</div>
+
+</body>
+</html>
diff --git a/layout/reftests/w3c-css/submitted/flexbox/reftest.list b/layout/reftests/w3c-css/submitted/flexbox/reftest.list
--- a/layout/reftests/w3c-css/submitted/flexbox/reftest.list
+++ b/layout/reftests/w3c-css/submitted/flexbox/reftest.list
@@ -206,26 +206,28 @@ fails == flexbox-min-height-auto-002b.ht
 == flexbox-whitespace-handling-001b.xhtml flexbox-whitespace-handling-001-ref.xhtml
 == flexbox-whitespace-handling-002.xhtml  flexbox-whitespace-handling-002-ref.xhtml
 
 # Tests for flex containers with pseudo-elements
 == flexbox-with-pseudo-elements-001.html flexbox-with-pseudo-elements-001-ref.html
 == flexbox-with-pseudo-elements-002.html flexbox-with-pseudo-elements-002-ref.html
 == flexbox-with-pseudo-elements-003.html flexbox-with-pseudo-elements-003-ref.html
 
-# Tests for combined influence of 'writing-mode' & 'direction' on flex axes
+# Tests for combined influence of 'writing-mode' & 'direction'
+# on flex container axes & flex item placement
 == flexbox-writing-mode-001.html flexbox-writing-mode-001-ref.html
 == flexbox-writing-mode-002.html flexbox-writing-mode-002-ref.html
 == flexbox-writing-mode-003.html flexbox-writing-mode-003-ref.html
 == flexbox-writing-mode-004.html flexbox-writing-mode-004-ref.html
 == flexbox-writing-mode-005.html flexbox-writing-mode-005-ref.html
 == flexbox-writing-mode-006.html flexbox-writing-mode-006-ref.html
 == flexbox-writing-mode-007.html flexbox-writing-mode-007-ref.html
 == flexbox-writing-mode-008.html flexbox-writing-mode-008-ref.html
 == flexbox-writing-mode-009.html flexbox-writing-mode-009-ref.html
+fails == flexbox-writing-mode-010.html flexbox-writing-mode-010-ref.html # bug 1267462
 
 # Single-line size clamping
 == flexbox-single-line-clamp-1.html flexbox-single-line-clamp-1-ref.html
 == flexbox-single-line-clamp-2.html flexbox-single-line-clamp-2-ref.html
 == flexbox-single-line-clamp-3.html flexbox-single-line-clamp-3-ref.html
 
 # Flexbox as an absolute containing block.
 == position-absolute-containing-block-001.html position-absolute-containing-block-001-ref.html
