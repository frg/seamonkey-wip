# HG changeset patch
# User Tooru Fujisawa <arai_a@mac.com>
# Date 1528354741 -32400
#      Thu Jun 07 15:59:01 2018 +0900
# Node ID 23afb1420cc0ed72cd475b90db1b24a1b7fc4959
# Parent  233ac49a9f9afe6c0e1595eb367b0da618dcce09
Bug 1460154 - Part 2: Handle TDZCheckCache in IfEmitter. r=jwalden

diff --git a/js/src/frontend/BytecodeEmitter.cpp b/js/src/frontend/BytecodeEmitter.cpp
--- a/js/src/frontend/BytecodeEmitter.cpp
+++ b/js/src/frontend/BytecodeEmitter.cpp
@@ -71,20 +71,32 @@ ParseNodeRequiresSpecialLineNumberNotes(
     // Functions usually shouldn't have location information (bug 1431202).
 
     ParseNodeKind kind = pn->getKind();
     return kind == ParseNodeKind::While ||
            kind == ParseNodeKind::For ||
            kind == ParseNodeKind::Function;
 }
 
-// A cache that tracks superfluous TDZ checks.
+// A cache that tracks Temporal Dead Zone (TDZ) checks, so that any use of a
+// lexical variable that's dominated by an earlier use, or by evaluation of its
+// declaration (which will initialize it, perhaps to |undefined|), doesn't have
+// to redundantly check that the lexical variable has been initialized
 //
 // Each basic block should have a TDZCheckCache in scope. Some NestableControl
 // subclasses contain a TDZCheckCache.
+//
+// When a scope containing lexical variables is entered, all such variables are
+// marked as CheckTDZ.  When a lexical variable is accessed, its entry is
+// checked.  If it's CheckTDZ, a JSOP_CHECKLEXICAL is emitted and then the
+// entry is marked DontCheckTDZ.  If it's DontCheckTDZ, no check is emitted
+// because a prior check would have already failed.  Finally, because
+// evaluating a lexical variable declaration initializes it (after any
+// initializer is evaluated), evaluating a lexical declaration marks its entry
+// as DontCheckTDZ.
 class BytecodeEmitter::TDZCheckCache : public Nestable<BytecodeEmitter::TDZCheckCache>
 {
     PooledMapPtr<CheckTDZMap> cache_;
 
     MOZ_MUST_USE bool ensureCache(BytecodeEmitter* bce) {
         return cache_ || cache_.acquire(bce->cx);
     }
 
@@ -1974,30 +1986,57 @@ class MOZ_STACK_CLASS TryEmitter
 //     condElse.emitCond();
 //     emit(then_block);
 //     condElse.emitElse();
 //     emit(else_block);
 //     condElse.emitEnd();
 //
 class MOZ_STACK_CLASS IfEmitter
 {
+  public:
+    // Whether the then-clause, the else-clause, or else-if condition may
+    // contain declaration or access to lexical variables, which means they
+    // should have their own TDZCheckCache.  Basically TDZCheckCache should be
+    // created for each basic block, which then-clause, else-clause, and
+    // else-if condition are, but for internally used branches which are
+    // known not to touch lexical variables we can skip creating TDZCheckCache
+    // for them.
+    //
+    // See the comment for TDZCheckCache class for more details.
+    enum class Kind {
+        // For syntactic branches (if, if-else, and conditional expression),
+        // which basically may contain declaration or accesses to lexical
+        // variables inside then-clause, else-clause, and else-if condition.
+        MayContainLexicalAccessInBranch,
+
+        // For internally used branches which don't touch lexical variables
+        // inside then-clause, else-clause, nor else-if condition.
+        NoLexicalAccessInBranch
+    };
+
+  private:
+    using TDZCheckCache = BytecodeEmitter::TDZCheckCache;
+
     BytecodeEmitter* bce_;
 
     // Jump around the then clause, to the beginning of the else clause.
     JumpList jumpAroundThen_;
 
     // Jump around the else clause, to the end of the entire branch.
     JumpList jumpsAroundElse_;
 
     // The stack depth before emitting the then block.
     // Used for restoring stack depth before emitting the else block.
     // Also used for assertion to make sure then and else blocks pushed the
     // same number of values.
     int32_t thenDepth_;
 
+    Kind kind_;
+    Maybe<TDZCheckCache> tdzCache_;
+
 #ifdef DEBUG
     // The number of values pushed in the then and else blocks.
     int32_t pushed_;
     bool calculatedPushed_;
 
     // The state of this emitter.
     //
     // +-------+   emitCond +------+ emitElse +------+        emitEnd +-----+
@@ -2039,46 +2078,62 @@ class MOZ_STACK_CLASS IfEmitter
         ElseIf,
 
         // After calling emitEnd.
         End
     };
     State state_;
 #endif
 
-  public:
-    explicit IfEmitter(BytecodeEmitter* bce)
-      : bce_(bce),
-        thenDepth_(0)
+  protected:
+    // For InternalIfEmitter.
+    IfEmitter(BytecodeEmitter* bce, Kind kind)
+      : bce_(bce)
+      , thenDepth_(0)
+      , kind_(kind)
 #ifdef DEBUG
       , pushed_(0)
       , calculatedPushed_(false)
       , state_(State::Start)
 #endif
     {}
 
-    ~IfEmitter()
+  public:
+    explicit IfEmitter(BytecodeEmitter* bce)
+      : IfEmitter(bce, Kind::MayContainLexicalAccessInBranch)
     {}
 
   private:
     MOZ_MUST_USE bool emitIfInternal(SrcNoteType type) {
+        MOZ_ASSERT_IF(state_ == State::ElseIf, tdzCache_.isSome());
+        MOZ_ASSERT_IF(state_ != State::ElseIf, tdzCache_.isNothing());
+
+        // The end of TDZCheckCache for cond for else-if.
+        if (kind_ == Kind::MayContainLexicalAccessInBranch)
+            tdzCache_.reset();
+
         // Emit an annotated branch-if-false around the then part.
         if (!bce_->newSrcNote(type))
             return false;
         if (!bce_->emitJump(JSOP_IFEQ, &jumpAroundThen_))
             return false;
 
         // To restore stack depth in else part, save depth of the then part.
 #ifdef DEBUG
         // If DEBUG, this is also necessary to calculate |pushed_|.
         thenDepth_ = bce_->stackDepth;
 #else
         if (type == SRC_COND || type == SRC_IF_ELSE)
             thenDepth_ = bce_->stackDepth;
 #endif
+
+        // Enclose then-branch with TDZCheckCache.
+        if (kind_ == Kind::MayContainLexicalAccessInBranch)
+            tdzCache_.emplace(bce_);
+
         return true;
     }
 
     void calculateOrCheckPushed() {
 #ifdef DEBUG
         if (!calculatedPushed_) {
             pushed_ = bce_->stackDepth - thenDepth_;
             calculatedPushed_ = true;
@@ -2121,16 +2176,22 @@ class MOZ_STACK_CLASS IfEmitter
 #endif
         return true;
     }
 
   private:
     MOZ_MUST_USE bool emitElseInternal() {
         calculateOrCheckPushed();
 
+        // The end of TDZCheckCache for then-clause.
+        if (kind_ == Kind::MayContainLexicalAccessInBranch) {
+            MOZ_ASSERT(tdzCache_.isSome());
+            tdzCache_.reset();
+        }
+
         // Emit a jump from the end of our then part around the else part. The
         // patchJumpsToTarget call at the bottom of this function will fix up
         // the offset with jumpsAroundElse value.
         if (!bce_->emitJump(JSOP_GOTO, &jumpsAroundElse_))
             return false;
 
         // Ensure the branch-if-false comes here, then emit the else.
         if (!bce_->emitJumpTargetAndPatch(jumpAroundThen_))
@@ -2149,41 +2210,55 @@ class MOZ_STACK_CLASS IfEmitter
 
   public:
     MOZ_MUST_USE bool emitElse() {
         MOZ_ASSERT(state_ == State::ThenElse || state_ == State::Cond);
 
         if (!emitElseInternal())
             return false;
 
+        // Enclose else-branch with TDZCheckCache.
+        if (kind_ == Kind::MayContainLexicalAccessInBranch)
+            tdzCache_.emplace(bce_);
+
 #ifdef DEBUG
         state_ = State::Else;
 #endif
         return true;
     }
 
     MOZ_MUST_USE bool emitElseIf() {
         MOZ_ASSERT(state_ == State::ThenElse);
 
         if (!emitElseInternal())
             return false;
 
+        // Enclose cond for else-if with TDZCheckCache.
+        if (kind_ == Kind::MayContainLexicalAccessInBranch)
+            tdzCache_.emplace(bce_);
+
 #ifdef DEBUG
         state_ = State::ElseIf;
 #endif
         return true;
     }
 
     MOZ_MUST_USE bool emitEnd() {
         MOZ_ASSERT(state_ == State::Then || state_ == State::Else);
         // If there was an else part for the last branch, jumpAroundThen_ is
         // already fixed up when emitting the else part.
         MOZ_ASSERT_IF(state_ == State::Then, jumpAroundThen_.offset != -1);
         MOZ_ASSERT_IF(state_ == State::Else, jumpAroundThen_.offset == -1);
 
+        // The end of TDZCheckCache for then or else-clause.
+        if (kind_ == Kind::MayContainLexicalAccessInBranch) {
+            MOZ_ASSERT(tdzCache_.isSome());
+            tdzCache_.reset();
+        }
+
         calculateOrCheckPushed();
 
         if (jumpAroundThen_.offset != -1) {
             // No else part for the last branch, fixup the branch-if-false to
             // come here.
             if (!bce_->emitJumpTargetAndPatch(jumpAroundThen_))
                 return false;
         }
@@ -2210,16 +2285,29 @@ class MOZ_STACK_CLASS IfEmitter
     // `then_block` and `else_block`.
     // Can be used in assertion after emitting if-then-else.
     int32_t popped() const {
         return -pushed_;
     }
 #endif
 };
 
+// Class for emitting bytecode for blocks like if-then-else which doesn't touch
+// lexical variables.
+//
+// See the comments above NoLexicalAccessInBranch for more details when to use
+// this instead of IfEmitter.
+class MOZ_STACK_CLASS InternalIfEmitter : public IfEmitter
+{
+  public:
+    explicit InternalIfEmitter(BytecodeEmitter* bce)
+      : IfEmitter(bce, Kind::NoLexicalAccessInBranch)
+    {}
+};
+
 class ForOfLoopControl : public LoopControl
 {
     using EmitterScope = BytecodeEmitter::EmitterScope;
 
     // The stack depth of the iterator.
     int32_t iterDepth_;
 
     // for-of loops, when throwing from non-iterator code (i.e. from the body
@@ -2298,17 +2386,17 @@ class ForOfLoopControl : public LoopCont
         // If ITER is undefined, it means the exception is thrown by
         // IteratorClose for non-local jump, and we should't perform
         // IteratorClose again here.
         if (!bce->emit1(JSOP_UNDEFINED))          // ITER ... EXCEPTION ITER UNDEF
             return false;
         if (!bce->emit1(JSOP_STRICTNE))           // ITER ... EXCEPTION NE
             return false;
 
-        IfEmitter ifIteratorIsNotClosed(bce);
+        InternalIfEmitter ifIteratorIsNotClosed(bce);
         if (!ifIteratorIsNotClosed.emitThen())    // ITER ... EXCEPTION
             return false;
 
         MOZ_ASSERT(slotFromTop == unsigned(bce->stackDepth - iterDepth_));
         if (!bce->emitDupAt(slotFromTop))         // ITER ... EXCEPTION ITER
             return false;
         if (!emitIteratorCloseInInnermostScope(bce, CompletionKind::Throw))
             return false;                         // ITER ... EXCEPTION
@@ -2322,17 +2410,17 @@ class ForOfLoopControl : public LoopCont
         // If any yields were emitted, then this for-of loop is inside a star
         // generator and must handle the case of Generator.return. Like in
         // yield*, it is handled with a finally block.
         uint32_t numYieldsEmitted = bce->yieldAndAwaitOffsetList.numYields;
         if (numYieldsEmitted > numYieldsAtBeginCodeNeedingIterClose_) {
             if (!tryCatch_->emitFinally())
                 return false;
 
-            IfEmitter ifGeneratorClosing(bce);
+            InternalIfEmitter ifGeneratorClosing(bce);
             if (!bce->emit1(JSOP_ISGENCLOSING))   // ITER ... FTYPE FVALUE CLOSING
                 return false;
             if (!ifGeneratorClosing.emitThen())   // ITER ... FTYPE FVALUE
                 return false;
             if (!bce->emitDupAt(slotFromTop + 1)) // ITER ... FTYPE FVALUE ITER
                 return false;
             if (!emitIteratorCloseInInnermostScope(bce, CompletionKind::Normal))
                 return false;                     // ITER ... FTYPE FVALUE
@@ -5501,17 +5589,17 @@ BytecodeEmitter::emitIteratorCloseInScop
     //
     // Get the "return" method.
     if (!emitAtomOp(cx->names().return_, JSOP_CALLPROP))  // ... ITER RET
         return false;
 
     // Step 4.
     //
     // Do nothing if "return" is undefined or null.
-    IfEmitter ifReturnMethodIsDefined(this);
+    InternalIfEmitter ifReturnMethodIsDefined(this);
     if (!emitPushNotUndefinedOrNull())                    // ... ITER RET NOT-UNDEF-OR-NULL
         return false;
 
     if (!ifReturnMethodIsDefined.emitThenElse())          // ... ITER RET
         return false;
 
     if (completionKind == CompletionKind::Throw) {
         // 7.4.6 IteratorClose ( iterator, completion )
@@ -5880,17 +5968,17 @@ BytecodeEmitter::emitDestructuringOpsArr
             //
             // Non-first elements should emit if-else depending on the
             // member pattern, below.
             if (!emit1(JSOP_POP))                                 // ... OBJ NEXT ITER *LREF
                 return false;
         }
 
         if (member->isKind(ParseNodeKind::Spread)) {
-            IfEmitter ifThenElse(this);
+            InternalIfEmitter ifThenElse(this);
             if (!isFirst) {
                 // If spread is not the first element of the pattern,
                 // iterator can already be completed.
                                                                   // ... OBJ NEXT ITER *LREF DONE
                 if (!ifThenElse.emitThenElse())                   // ... OBJ NEXT ITER *LREF
                     return false;
 
                 if (!emitUint32Operand(JSOP_NEWARRAY, 0))         // ... OBJ NEXT ITER *LREF ARRAY
@@ -5937,17 +6025,17 @@ BytecodeEmitter::emitDestructuringOpsArr
         }
 
         ParseNode* pndefault = nullptr;
         if (member->isKind(ParseNodeKind::Assign))
             pndefault = member->pn_right;
 
         MOZ_ASSERT(!member->isKind(ParseNodeKind::Spread));
 
-        IfEmitter ifAlreadyDone(this);
+        InternalIfEmitter ifAlreadyDone(this);
         if (!isFirst) {
                                                                   // ... OBJ NEXT ITER *LREF DONE
             if (!ifAlreadyDone.emitThenElse())                    // ... OBJ NEXT ITER *LREF
                 return false;
 
             if (!emit1(JSOP_UNDEFINED))                           // ... OBJ NEXT ITER *LREF UNDEF
                 return false;
             if (!emit1(JSOP_NOP_DESTRUCTURING))                   // ... OBJ NEXT ITER *LREF UNDEF
@@ -5974,17 +6062,17 @@ BytecodeEmitter::emitDestructuringOpsArr
         if (!emitAtomOp(cx->names().done, JSOP_GETPROP))          // ... OBJ NEXT ITER *LREF RESULT DONE
             return false;
 
         if (!emit1(JSOP_DUP))                                     // ... OBJ NEXT ITER *LREF RESULT DONE DONE
             return false;
         if (!emit2(JSOP_UNPICK, emitted + 2))                     // ... OBJ NEXT ITER DONE *LREF RESULT DONE
             return false;
 
-        IfEmitter ifDone(this);
+        InternalIfEmitter ifDone(this);
         if (!ifDone.emitThenElse())                               // ... OBJ NEXT ITER DONE *LREF RESULT
             return false;
 
         if (!emit1(JSOP_POP))                                     // ... OBJ NEXT ITER DONE *LREF
             return false;
         if (!emit1(JSOP_UNDEFINED))                               // ... OBJ NEXT ITER DONE *LREF UNDEF
             return false;
         if (!emit1(JSOP_NOP_DESTRUCTURING))                       // ... OBJ NEXT ITER DONE *LREF UNDEF
@@ -6026,17 +6114,17 @@ BytecodeEmitter::emitDestructuringOpsArr
             if (!emit1(JSOP_POP))                                 // ... OBJ NEXT ITER DONE
                 return false;
         }
     }
 
     // The last DONE value is on top of the stack. If not DONE, call
     // IteratorClose.
                                                                   // ... OBJ NEXT ITER DONE
-    IfEmitter ifDone(this);
+    InternalIfEmitter ifDone(this);
     if (!ifDone.emitThenElse())                                   // ... OBJ NEXT ITER
         return false;
     if (!emitPopN(2))                                             // ... OBJ
         return false;
     if (!ifDone.emitElse())                                       // ... OBJ NEXT ITER
         return false;
     if (!emit1(JSOP_SWAP))                                        // ... OBJ ITER NEXT
         return false;
@@ -6928,47 +7016,47 @@ BytecodeEmitter::emitTry(ParseNode* pn)
 
 bool
 BytecodeEmitter::emitIf(ParseNode* pn)
 {
     IfEmitter ifThenElse(this);
 
   if_again:
     /* Emit code for the condition before pushing stmtInfo. */
-    if (!emitTreeInBranch(pn->pn_kid1))
+    if (!emitTree(pn->pn_kid1))
         return false;
 
     ParseNode* elseNode = pn->pn_kid3;
     if (elseNode) {
         if (!ifThenElse.emitThenElse())
             return false;
     } else {
         if (!ifThenElse.emitThen())
             return false;
     }
 
     /* Emit code for the then part. */
-    if (!emitTreeInBranch(pn->pn_kid2))
+    if (!emitTree(pn->pn_kid2))
         return false;
 
     if (elseNode) {
         if (elseNode->isKind(ParseNodeKind::If)) {
             pn = elseNode;
 
             if (!ifThenElse.emitElseIf())
                 return false;
 
             goto if_again;
         }
 
         if (!ifThenElse.emitElse())
             return false;
 
         /* Emit code for the else part. */
-        if (!emitTreeInBranch(elseNode))
+        if (!emitTree(elseNode))
             return false;
     }
 
     if (!ifThenElse.emitEnd())
         return false;
 
     return true;
 }
@@ -7154,17 +7242,17 @@ BytecodeEmitter::emitAsyncIterator()
     // Convert iterable to iterator.
     if (!emit1(JSOP_DUP))                                         // OBJ OBJ
         return false;
     if (!emit2(JSOP_SYMBOL, uint8_t(JS::SymbolCode::asyncIterator))) // OBJ OBJ @@ASYNCITERATOR
         return false;
     if (!emitElemOpBase(JSOP_CALLELEM))                           // OBJ ITERFN
         return false;
 
-    IfEmitter ifAsyncIterIsUndefined(this);
+    InternalIfEmitter ifAsyncIterIsUndefined(this);
     if (!emitPushNotUndefinedOrNull())                            // OBJ ITERFN !UNDEF-OR-NULL
         return false;
     if (!emit1(JSOP_NOT))                                         // OBJ ITERFN UNDEF-OR-NULL
         return false;
     if (!ifAsyncIterIsUndefined.emitThenElse())                   // OBJ ITERFN
         return false;
 
     if (!emit1(JSOP_POP))                                         // OBJ
@@ -7472,17 +7560,17 @@ BytecodeEmitter::emitForOf(ParseNode* fo
         if (!emitIteratorNext(forOfHead, iterKind, allowSelfHostedIter))
             return false;                                 // NEXT ITER RESULT
 
         if (!emit1(JSOP_DUP))                             // NEXT ITER RESULT RESULT
             return false;
         if (!emitAtomOp(cx->names().done, JSOP_GETPROP))  // NEXT ITER RESULT DONE
             return false;
 
-        IfEmitter ifDone(this);
+        InternalIfEmitter ifDone(this);
 
         if (!ifDone.emitThen())                           // NEXT ITER RESULT
             return false;
 
         // Remove RESULT from the stack to release it.
         if (!emit1(JSOP_POP))                             // NEXT ITER
             return false;
         if (!emit1(JSOP_UNDEFINED))                       // NEXT ITER UNDEF
@@ -8666,17 +8754,17 @@ BytecodeEmitter::emitYieldStar(ParseNode
         return false;
     if (!emit1(JSOP_DUP))                                 // NEXT ITER RESULT EXCEPTION ITER THROW THROW
         return false;
     if (!emit1(JSOP_UNDEFINED))                           // NEXT ITER RESULT EXCEPTION ITER THROW THROW UNDEFINED
         return false;
     if (!emit1(JSOP_EQ))                                  // NEXT ITER RESULT EXCEPTION ITER THROW ?EQL
         return false;
 
-    IfEmitter ifThrowMethodIsNotDefined(this);
+    InternalIfEmitter ifThrowMethodIsNotDefined(this);
     if (!ifThrowMethodIsNotDefined.emitThen())            // NEXT ITER RESULT EXCEPTION ITER THROW
         return false;
     savedDepthTemp = stackDepth;
     if (!emit1(JSOP_POP))                                 // NEXT ITER RESULT EXCEPTION ITER
         return false;
     // ES 14.4.13, YieldExpression : yield * AssignmentExpression, step 5.b.iii.2
     //
     // If the iterator does not have a "throw" method, it calls IteratorClose
@@ -8722,17 +8810,17 @@ BytecodeEmitter::emitYieldStar(ParseNode
     if (!tryCatch.emitFinally())
          return false;
 
     // ES 14.4.13, yield * AssignmentExpression, step 5.c
     //
     // Call iterator.return() for receiving a "forced return" completion from
     // the generator.
 
-    IfEmitter ifGeneratorClosing(this);
+    InternalIfEmitter ifGeneratorClosing(this);
     if (!emit1(JSOP_ISGENCLOSING))                        // NEXT ITER RESULT FTYPE FVALUE CLOSING
         return false;
     if (!ifGeneratorClosing.emitThen())                   // NEXT ITER RESULT FTYPE FVALUE
         return false;
 
     // Step ii.
     //
     // Get the "return" method.
@@ -8741,17 +8829,17 @@ BytecodeEmitter::emitYieldStar(ParseNode
     if (!emit1(JSOP_DUP))                                 // NEXT ITER RESULT FTYPE FVALUE ITER ITER
         return false;
     if (!emitAtomOp(cx->names().return_, JSOP_CALLPROP))  // NEXT ITER RESULT FTYPE FVALUE ITER RET
         return false;
 
     // Step iii.
     //
     // Do nothing if "return" is undefined or null.
-    IfEmitter ifReturnMethodIsDefined(this);
+    InternalIfEmitter ifReturnMethodIsDefined(this);
     if (!emitPushNotUndefinedOrNull())                    // NEXT ITER RESULT FTYPE FVALUE ITER RET NOT-UNDEF-OR-NULL
         return false;
 
     // Step iv.
     //
     // Call "return" with the argument passed to Generator.prototype.return,
     // which is currently in rval.value.
     if (!ifReturnMethodIsDefined.emitThenElse())          // NEXT ITER OLDRESULT FTYPE FVALUE ITER RET
@@ -8774,17 +8862,17 @@ BytecodeEmitter::emitYieldStar(ParseNode
     // Step v.
     if (!emitCheckIsObj(CheckIsObjectKind::IteratorReturn)) // NEXT ITER OLDRESULT FTYPE FVALUE RESULT
         return false;
 
     // Steps vi-viii.
     //
     // Check if the returned object from iterator.return() is done. If not,
     // continuing yielding.
-    IfEmitter ifReturnDone(this);
+    InternalIfEmitter ifReturnDone(this);
     if (!emit1(JSOP_DUP))                                 // NEXT ITER OLDRESULT FTYPE FVALUE RESULT RESULT
         return false;
     if (!emitAtomOp(cx->names().done, JSOP_GETPROP))      // NEXT ITER OLDRESULT FTYPE FVALUE RESULT DONE
         return false;
     if (!ifReturnDone.emitThenElse())                     // NEXT ITER OLDRESULT FTYPE FVALUE RESULT
         return false;
     if (!emitAtomOp(cx->names().value, JSOP_GETPROP))     // NEXT ITER OLDRESULT FTYPE FVALUE VALUE
         return false;
@@ -9514,17 +9602,17 @@ BytecodeEmitter::emitCallOrNew(ParseNode
                 // Repush the callee as new.target
                 if (!emitDupAt(argc + 1))
                     return false;
             }
         }
     } else {
         ParseNode* args = pn2->pn_next;
         bool emitOptCode = (argc == 1) && isRestParameter(args->pn_kid);
-        IfEmitter ifNotOptimizable(this);
+        InternalIfEmitter ifNotOptimizable(this);
 
         if (emitOptCode) {
             // Emit a preparation code to optimize the spread call with a rest
             // parameter:
             //
             //   function f(...args) {
             //     g(...args);
             //   }
@@ -9793,23 +9881,23 @@ BytecodeEmitter::emitConditionalExpressi
     /* Emit the condition, then branch if false to the else part. */
     if (!emitTree(&conditional.condition()))
         return false;
 
     IfEmitter ifThenElse(this);
     if (!ifThenElse.emitCond())
         return false;
 
-    if (!emitTreeInBranch(&conditional.thenExpression(), valueUsage))
+    if (!emitTree(&conditional.thenExpression(), valueUsage))
         return false;
 
     if (!ifThenElse.emitElse())
         return false;
 
-    if (!emitTreeInBranch(&conditional.elseExpression(), valueUsage))
+    if (!emitTree(&conditional.elseExpression(), valueUsage))
         return false;
 
     if (!ifThenElse.emitEnd())
         return false;
     MOZ_ASSERT(ifThenElse.pushed() == 1);
 
     return true;
 }
@@ -10648,17 +10736,17 @@ BytecodeEmitter::emitClass(ParseNode* pn
     //
     //   EmitPropertyList(...)
 
     // This is kind of silly. In order to the get the home object defined on
     // the constructor, we have to make it second, but we want the prototype
     // on top for EmitPropertyList, because we expect static properties to be
     // rarer. The result is a few more swaps than we would like. Such is life.
     if (heritageExpression) {
-        IfEmitter ifThenElse(this);
+        InternalIfEmitter ifThenElse(this);
 
         if (!emitTree(heritageExpression))                      // ... HERITAGE
             return false;
 
         // Heritage must be null or a non-generator constructor
         if (!emit1(JSOP_CHECKCLASSHERITAGE))                    // ... HERITAGE
             return false;
 
