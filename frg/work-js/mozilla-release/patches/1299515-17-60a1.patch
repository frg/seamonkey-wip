# HG changeset patch
# User Andreas Pehrson <pehrsons@mozilla.com>
# Date 1511864144 -3600
#      Tue Nov 28 11:15:44 2017 +0100
# Node ID aecf59ec97ff6a06d03a5d9f8e676869a676e9d8
# Parent  ddcf3f90fdb34c2447b4ed9630301d1e9a3c9c7a
Bug 1299515 - Modernize tests that disable tracks to prime for coming patches. r=jib

MozReview-Commit-ID: 9rzaZimjpm9

diff --git a/dom/media/tests/mochitest/test_getUserMedia_mediaStreamClone.html b/dom/media/tests/mochitest/test_getUserMedia_mediaStreamClone.html
--- a/dom/media/tests/mochitest/test_getUserMedia_mediaStreamClone.html
+++ b/dom/media/tests/mochitest/test_getUserMedia_mediaStreamClone.html
@@ -1,251 +1,250 @@
 <!DOCTYPE HTML>
 <html>
 <head>
   <script type="application/javascript" src="mediaStreamPlayback.js"></script>
 </head>
 <body>
 <pre id="test">
 <script type="application/javascript">
-  "use strict";
+"use strict";
 
-  createHTML({
-    title: "MediaStream.clone()",
-    bug: "1208371"
-  });
+createHTML({
+  title: "MediaStream.clone()",
+  bug: "1208371"
+});
 
-  runTest(() => Promise.resolve()
-    .then(() => getUserMedia({audio: true, video: true})).then(stream => {
-      info("Test clone()ing an audio/video gUM stream");
-      var clone = stream.clone();
+runTest(async () => {
+
+  let stream = await getUserMedia({audio: true, video: true});
+  info("Test clone()ing an audio/video gUM stream");
+  let clone = stream.clone();
 
-      checkMediaStreamCloneAgainstOriginal(clone, stream);
-      checkMediaStreamTrackCloneAgainstOriginal(clone.getAudioTracks()[0],
-                                                stream.getAudioTracks()[0]);
-      checkMediaStreamTrackCloneAgainstOriginal(clone.getVideoTracks()[0],
-                                                stream.getVideoTracks()[0]);
+  checkMediaStreamCloneAgainstOriginal(clone, stream);
+  checkMediaStreamTrackCloneAgainstOriginal(clone.getAudioTracks()[0],
+                                            stream.getAudioTracks()[0]);
+  checkMediaStreamTrackCloneAgainstOriginal(clone.getVideoTracks()[0],
+                                            stream.getVideoTracks()[0]);
 
-      isnot(clone.id.length, 0, "Stream clone should have an id string");
-      isnot(clone.getAudioTracks()[0].id.length, 0,
-            "Audio track clone should have an id string");
-      isnot(clone.getVideoTracks()[0].id.length, 0,
-            "Audio track clone should have an id string");
+  isnot(clone.id.length, 0, "Stream clone should have an id string");
+  isnot(clone.getAudioTracks()[0].id.length, 0,
+        "Audio track clone should have an id string");
+  isnot(clone.getVideoTracks()[0].id.length, 0,
+        "Audio track clone should have an id string");
 
-      info("Stopping original tracks");
-      stream.getTracks().forEach(t => t.stop());
+  info("Stopping original tracks");
+  stream.getTracks().forEach(t => t.stop());
 
-      info("Playing from track clones");
-      var test = createMediaElement('video', 'testClonePlayback');
-      var playback = new MediaStreamPlayback(test, clone);
-      return playback.playMedia(false);
-    })
-    .then(() => getUserMedia({video: true})).then(stream =>
-      getUserMedia({video: true}).then(otherStream => {
-        info("Test addTrack()ing a video track to a stream without affecting its clone");
-        var track = stream.getTracks()[0];
-        var otherTrack = otherStream.getTracks()[0];
+  info("Playing from track clones");
+  let test = createMediaElement('video', 'testClonePlayback');
+  let playback = new MediaStreamPlayback(test, clone);
+  await playback.playMedia(false);
+
+
+  stream = await getUserMedia({video: true});
+  let otherStream = await getUserMedia({video: true});
+  info("Test addTrack()ing a video track to a stream without affecting its clone");
+  let track = stream.getTracks()[0];
+  let otherTrack = otherStream.getTracks()[0];
 
-        var streamClone = stream.clone();
-        var trackClone = streamClone.getTracks()[0];
-        checkMediaStreamContains(streamClone, [trackClone], "Initial clone");
+  let streamClone = stream.clone();
+  let trackClone = streamClone.getTracks()[0];
+  checkMediaStreamContains(streamClone, [trackClone], "Initial clone");
 
-        stream.addTrack(otherTrack);
-        checkMediaStreamContains(stream, [track, otherTrack],
-                                 "Added video to original");
-        checkMediaStreamContains(streamClone, [trackClone],
-                                 "Clone not affected");
+  stream.addTrack(otherTrack);
+  checkMediaStreamContains(stream, [track, otherTrack],
+                           "Added video to original");
+  checkMediaStreamContains(streamClone, [trackClone],
+                           "Clone not affected");
 
-        stream.removeTrack(track);
-        streamClone.addTrack(track);
-        checkMediaStreamContains(streamClone, [trackClone, track],
-                                 "Added video to clone");
-        checkMediaStreamContains(stream, [otherTrack],
-                                 "Original not affected");
+  stream.removeTrack(track);
+  streamClone.addTrack(track);
+  checkMediaStreamContains(streamClone, [trackClone, track],
+                           "Added video to clone");
+  checkMediaStreamContains(stream, [otherTrack],
+                           "Original not affected");
 
-        // Not part of streamClone. Does not get stopped by the playback test.
-        otherTrack.stop();
-        otherStream.stop();
+  // Not part of streamClone. Does not get stopped by the playback test.
+  otherTrack.stop();
+  otherStream.stop();
+
+  test = createMediaElement('video', 'testClonePlayback');
+  playback = new MediaStreamPlayback(test, streamClone);
+  await playback.playMedia(false);
+  stream.getTracks().forEach(t => t.stop());
+  stream.stop();
+
 
-        var test = createMediaElement('video', 'testClonePlayback');
-        var playback = new MediaStreamPlayback(test, streamClone);
-        return playback.playMedia(false)
-          .then(() => stream.getTracks().forEach(t => t.stop()))
-          .then(() => stream.stop());
-    }))
-    .then(() => getUserMedia({audio: true, video: true})).then(stream => {
-      info("Test cloning a stream into inception");
-      var clone = stream;
-      var clones = Array(10).fill().map(() => clone = clone.clone());
-      var inceptionClone = clones.pop();
-      checkMediaStreamCloneAgainstOriginal(inceptionClone, stream);
-      stream.getTracks().forEach(t => (stream.removeTrack(t),
-                                       inceptionClone.addTrack(t)));
-      is(inceptionClone.getAudioTracks().length, 2,
-         "The inception clone should contain the original audio track and a track clone");
-      is(inceptionClone.getVideoTracks().length, 2,
-         "The inception clone should contain the original video track and a track clone");
+  stream = await getUserMedia({audio: true, video: true});
+  info("Test cloning a stream into inception");
+  clone = stream;
+  let clones = Array(10).fill().map(() => clone = clone.clone());
+  let inceptionClone = clones.pop();
+  checkMediaStreamCloneAgainstOriginal(inceptionClone, stream);
+  stream.getTracks().forEach(t => (stream.removeTrack(t),
+                                   inceptionClone.addTrack(t)));
+  is(inceptionClone.getAudioTracks().length, 2,
+     "The inception clone should contain the original audio track and a track clone");
+  is(inceptionClone.getVideoTracks().length, 2,
+     "The inception clone should contain the original video track and a track clone");
+
+  test = createMediaElement('video', 'testClonePlayback');
+  playback = new MediaStreamPlayback(test, inceptionClone);
+  await playback.playMedia(false);
+  clones.forEach(c => c.getTracks().forEach(t => t.stop()));
+
 
-      var test = createMediaElement('video', 'testClonePlayback');
-      var playback = new MediaStreamPlayback(test, inceptionClone);
-      return playback.playMedia(false)
-        .then(() => clones.forEach(c => c.getTracks().forEach(t => t.stop())));
-    })
-    .then(() => getUserMedia({audio: true, video: true})).then(stream => {
-      info("Test adding tracks from many stream clones to the original stream");
+  stream = await getUserMedia({audio: true, video: true});
+  info("Test adding tracks from many stream clones to the original stream");
+
+  const LOOPS = 3;
+  for (let i = 0; i < LOOPS; i++) {
+    stream.clone().getTracks().forEach(t => stream.addTrack(t));
+  }
+  is(stream.getAudioTracks().length, Math.pow(2, LOOPS),
+     "The original track should contain the original audio track and all the audio clones");
+  is(stream.getVideoTracks().length, Math.pow(2, LOOPS),
+     "The original track should contain the original video track and all the video clones");
+  stream.getTracks().forEach(t1 => is(stream.getTracks()
+                                            .filter(t2 => t1.id == t2.id)
+                                            .length,
+                                      1, "Each track should be unique"));
 
-      const LOOPS = 3;
-      for (var i = 0; i < LOOPS; i++) {
-        stream.clone().getTracks().forEach(t => stream.addTrack(t));
-      }
-      is(stream.getAudioTracks().length, Math.pow(2, LOOPS),
-         "The original track should contain the original audio track and all the audio clones");
-      is(stream.getVideoTracks().length, Math.pow(2, LOOPS),
-         "The original track should contain the original video track and all the video clones");
-      stream.getTracks().forEach(t1 => is(stream.getTracks()
-                                                .filter(t2 => t1.id == t2.id)
-                                                .length,
-                                          1, "Each track should be unique"));
+  test = createMediaElement('video', 'testClonePlayback');
+  playback = new MediaStreamPlayback(test, stream);
+  await playback.playMedia(false);
+
+
+  info("Testing audio content routing with MediaStream.clone()");
+  let ac = new AudioContext();
 
-      var test = createMediaElement('video', 'testClonePlayback');
-      var playback = new MediaStreamPlayback(test, stream);
-      return playback.playMedia(false);
-    })
-    .then(() => {
-      info("Testing audio content routing with MediaStream.clone()");
-      var ac = new AudioContext();
+  let osc1kOriginal = createOscillatorStream(ac, 1000);
+  let audioTrack1kOriginal = osc1kOriginal.getTracks()[0];
+  let audioTrack1kClone = osc1kOriginal.clone().getTracks()[0];
 
-      var osc1kOriginal = createOscillatorStream(ac, 1000);
-      var audioTrack1kOriginal = osc1kOriginal.getTracks()[0];
-      var audioTrack1kClone = osc1kOriginal.clone().getTracks()[0];
-
-      var osc5kOriginal = createOscillatorStream(ac, 5000);
-      var audioTrack5kOriginal = osc5kOriginal.getTracks()[0];
-      var audioTrack5kClone = osc5kOriginal.clone().getTracks()[0];
+  let osc5kOriginal = createOscillatorStream(ac, 5000);
+  let audioTrack5kOriginal = osc5kOriginal.getTracks()[0];
+  let audioTrack5kClone = osc5kOriginal.clone().getTracks()[0];
 
-      return Promise.resolve().then(() => {
-        info("Analysing audio output of original stream (1k + 5k)");
-        var stream = new MediaStream();
-        stream.addTrack(audioTrack1kOriginal);
-        stream.addTrack(audioTrack5kOriginal);
+  info("Analysing audio output of original stream (1k + 5k)");
+  stream = new MediaStream();
+  stream.addTrack(audioTrack1kOriginal);
+  stream.addTrack(audioTrack5kOriginal);
+
+  let analyser = new AudioStreamAnalyser(ac, stream);
+  await analyser.waitForAnalysisSuccess(array =>
+          array[analyser.binIndexForFrequency(50)] < 50 &&
+          array[analyser.binIndexForFrequency(1000)] > 200 &&
+          array[analyser.binIndexForFrequency(3000)] < 50 &&
+          array[analyser.binIndexForFrequency(5000)] > 200 &&
+          array[analyser.binIndexForFrequency(10000)] < 50);
+
+  info("Waiting for original tracks to stop");
+  stream.getTracks().forEach(t => t.stop());
+  await analyser.waitForAnalysisSuccess(array =>
+          array[analyser.binIndexForFrequency(50)] < 50 &&
+          // WebAudioDestination streams do not handle stop()
+          // XXX Should they? Plan to resolve that in bug 1208384.
+          // array[analyser.binIndexForFrequency(1000)] < 50 &&
+          array[analyser.binIndexForFrequency(3000)] < 50 &&
+          // array[analyser.binIndexForFrequency(5000)] < 50 &&
+          array[analyser.binIndexForFrequency(10000)] < 50);
+  analyser.disconnect();
 
-        var analyser = new AudioStreamAnalyser(ac, stream);
-        return analyser.waitForAnalysisSuccess(array =>
-                 array[analyser.binIndexForFrequency(50)] < 50 &&
-                 array[analyser.binIndexForFrequency(1000)] > 200 &&
-                 array[analyser.binIndexForFrequency(3000)] < 50 &&
-                 array[analyser.binIndexForFrequency(5000)] > 200 &&
-                 array[analyser.binIndexForFrequency(10000)] < 50)
-          .then(() => {
-            info("Waiting for original tracks to stop");
-            stream.getTracks().forEach(t => t.stop());
-            return analyser.waitForAnalysisSuccess(array =>
-                     array[analyser.binIndexForFrequency(50)] < 50 &&
-                     // WebAudioDestination streams do not handle stop()
-                     // XXX Should they? Plan to resolve that in bug 1208384.
-                     // array[analyser.binIndexForFrequency(1000)] < 50 &&
-                     array[analyser.binIndexForFrequency(3000)] < 50 &&
-                     // array[analyser.binIndexForFrequency(5000)] < 50 &&
-                     array[analyser.binIndexForFrequency(10000)] < 50);
-          })
-          .then(() => analyser.disconnect());
-      }).then(() => {
-        info("Analysing audio output of stream clone (1k + 5k)");
-        var stream = new MediaStream();
-        stream.addTrack(audioTrack1kClone);
-        stream.addTrack(audioTrack5kClone);
+  info("Analysing audio output of stream clone (1k + 5k)");
+  stream = new MediaStream();
+  stream.addTrack(audioTrack1kClone);
+  stream.addTrack(audioTrack5kClone);
+
+  analyser = new AudioStreamAnalyser(ac, stream);
+  await analyser.waitForAnalysisSuccess(array =>
+          array[analyser.binIndexForFrequency(50)] < 50 &&
+          array[analyser.binIndexForFrequency(1000)] > 200 &&
+          array[analyser.binIndexForFrequency(3000)] < 50 &&
+          array[analyser.binIndexForFrequency(5000)] > 200 &&
+          array[analyser.binIndexForFrequency(10000)] < 50);
+  analyser.disconnect();
 
-        var analyser = new AudioStreamAnalyser(ac, stream);
-        return analyser.waitForAnalysisSuccess(array =>
-                 array[analyser.binIndexForFrequency(50)] < 50 &&
-                 array[analyser.binIndexForFrequency(1000)] > 200 &&
-                 array[analyser.binIndexForFrequency(3000)] < 50 &&
-                 array[analyser.binIndexForFrequency(5000)] > 200 &&
-                 array[analyser.binIndexForFrequency(10000)] < 50)
-          .then(() => analyser.disconnect());
-      }).then(() => {
-        info("Analysing audio output of clone of clone (1k + 5k)");
-        var stream = new MediaStream([audioTrack1kClone, audioTrack5kClone]).clone();
+  info("Analysing audio output of clone of clone (1k + 5k)");
+  stream = new MediaStream([audioTrack1kClone, audioTrack5kClone]).clone();
 
-        var analyser = new AudioStreamAnalyser(ac, stream);
-        return analyser.waitForAnalysisSuccess(array =>
-                 array[analyser.binIndexForFrequency(50)] < 50 &&
-                 array[analyser.binIndexForFrequency(1000)] > 200 &&
-                 array[analyser.binIndexForFrequency(3000)] < 50 &&
-                 array[analyser.binIndexForFrequency(5000)] > 200 &&
-                 array[analyser.binIndexForFrequency(10000)] < 50)
-          .then(() => analyser.disconnect());
-      }).then(() => {
-        info("Analysing audio output of clone() + addTrack()ed tracks (1k + 5k)");
-        var stream =
-          new MediaStream(new MediaStream([ audioTrack1kClone
-                                          , audioTrack5kClone
-                                          ]).clone().getTracks());
+  analyser = new AudioStreamAnalyser(ac, stream);
+  await analyser.waitForAnalysisSuccess(array =>
+          array[analyser.binIndexForFrequency(50)] < 50 &&
+          array[analyser.binIndexForFrequency(1000)] > 200 &&
+          array[analyser.binIndexForFrequency(3000)] < 50 &&
+          array[analyser.binIndexForFrequency(5000)] > 200 &&
+          array[analyser.binIndexForFrequency(10000)] < 50);
+  analyser.disconnect();
+
+  info("Analysing audio output of clone() + addTrack()ed tracks (1k + 5k)");
+  stream = new MediaStream(new MediaStream([ audioTrack1kClone
+                                           , audioTrack5kClone
+                                           ]).clone().getTracks());
 
-        var analyser = new AudioStreamAnalyser(ac, stream);
-        return analyser.waitForAnalysisSuccess(array =>
-                 array[analyser.binIndexForFrequency(50)] < 50 &&
-                 array[analyser.binIndexForFrequency(1000)] > 200 &&
-                 array[analyser.binIndexForFrequency(3000)] < 50 &&
-                 array[analyser.binIndexForFrequency(5000)] > 200 &&
-                 array[analyser.binIndexForFrequency(10000)] < 50)
-          .then(() => analyser.disconnect());
-      }).then(() => {
-        info("Analysing audio output of clone()d tracks in original stream (1k) " +
-             "and clone()d tracks in stream clone (5k)");
-        var stream = new MediaStream([audioTrack1kClone, audioTrack5kClone]);
-        var streamClone = stream.clone();
+  analyser = new AudioStreamAnalyser(ac, stream);
+  await analyser.waitForAnalysisSuccess(array =>
+          array[analyser.binIndexForFrequency(50)] < 50 &&
+          array[analyser.binIndexForFrequency(1000)] > 200 &&
+          array[analyser.binIndexForFrequency(3000)] < 50 &&
+          array[analyser.binIndexForFrequency(5000)] > 200 &&
+          array[analyser.binIndexForFrequency(10000)] < 50);
+  analyser.disconnect();
 
-        stream.getTracks().forEach(t => stream.removeTrack(t));
-        stream.addTrack(streamClone.getTracks()[0]);
-        streamClone.removeTrack(streamClone.getTracks()[0]);
+  info("Analysing audio output of clone()d tracks in original stream (1k) " +
+       "and clone()d tracks in stream clone (5k)");
+  stream = new MediaStream([audioTrack1kClone, audioTrack5kClone]);
+  streamClone = stream.clone();
+
+  stream.getTracks().forEach(t => stream.removeTrack(t));
+  stream.addTrack(streamClone.getTracks()[0]);
+  streamClone.removeTrack(streamClone.getTracks()[0]);
+
+  analyser = new AudioStreamAnalyser(ac, stream);
+  await analyser.waitForAnalysisSuccess(array =>
+          array[analyser.binIndexForFrequency(50)] < 50 &&
+          array[analyser.binIndexForFrequency(1000)] > 200 &&
+          array[analyser.binIndexForFrequency(3000)] < 50 &&
+          array[analyser.binIndexForFrequency(5000)] < 50);
+  analyser.disconnect();
 
-        var analyser = new AudioStreamAnalyser(ac, stream);
-        return analyser.waitForAnalysisSuccess(array =>
-                 array[analyser.binIndexForFrequency(50)] < 50 &&
-                 array[analyser.binIndexForFrequency(1000)] > 200 &&
-                 array[analyser.binIndexForFrequency(3000)] < 50 &&
-                 array[analyser.binIndexForFrequency(5000)] < 50)
-          .then(() => {
-            analyser.disconnect();
-            var cloneAnalyser = new AudioStreamAnalyser(ac, streamClone);
-            return cloneAnalyser.waitForAnalysisSuccess(array =>
-                     array[cloneAnalyser.binIndexForFrequency(1000)] < 50 &&
-                     array[cloneAnalyser.binIndexForFrequency(3000)] < 50 &&
-                     array[cloneAnalyser.binIndexForFrequency(5000)] > 200 &&
-                     array[cloneAnalyser.binIndexForFrequency(10000)] < 50)
-              .then(() => cloneAnalyser.disconnect());
-          });
-      }).then(() => {
-        info("Analysing audio output enabled and disabled tracks that don't affect each other");
-        var stream = new MediaStream([audioTrack1kClone, audioTrack5kClone]);
-        var clone = stream.clone();
+  let cloneAnalyser = new AudioStreamAnalyser(ac, streamClone);
+  await cloneAnalyser.waitForAnalysisSuccess(array =>
+          array[cloneAnalyser.binIndexForFrequency(1000)] < 50 &&
+          array[cloneAnalyser.binIndexForFrequency(3000)] < 50 &&
+          array[cloneAnalyser.binIndexForFrequency(5000)] > 200 &&
+          array[cloneAnalyser.binIndexForFrequency(10000)] < 50);
+  cloneAnalyser.disconnect();
 
-        stream.getTracks()[0].enabled = true;
-        stream.getTracks()[1].enabled = false;
+  info("Analysing audio output enabled and disabled tracks that don't affect each other");
+  stream = new MediaStream([audioTrack1kClone, audioTrack5kClone]);
+  clone = stream.clone();
+
+  stream.getTracks()[0].enabled = true;
+  stream.getTracks()[1].enabled = false;
+
+  clone.getTracks()[0].enabled = false;
+  clone.getTracks()[1].enabled = true;
 
-        clone.getTracks()[0].enabled = false;
-        clone.getTracks()[1].enabled = true;
+  analyser = new AudioStreamAnalyser(ac, stream);
+  await analyser.waitForAnalysisSuccess(array =>
+          array[analyser.binIndexForFrequency(50)] < 50 &&
+          array[analyser.binIndexForFrequency(1000)] > 200 &&
+          array[analyser.binIndexForFrequency(3000)] < 50 &&
+          array[analyser.binIndexForFrequency(5000)] < 50);
+  analyser.disconnect();
 
-        var analyser = new AudioStreamAnalyser(ac, stream);
-        return analyser.waitForAnalysisSuccess(array =>
-                 array[analyser.binIndexForFrequency(50)] < 50 &&
-                 array[analyser.binIndexForFrequency(1000)] > 200 &&
-                 array[analyser.binIndexForFrequency(3000)] < 50 &&
-                 array[analyser.binIndexForFrequency(5000)] < 50)
-          .then(() => {
-            analyser.disconnect();
-            var cloneAnalyser = new AudioStreamAnalyser(ac, clone);
-            return cloneAnalyser.waitForAnalysisSuccess(array =>
-                     array[cloneAnalyser.binIndexForFrequency(1000)] < 50 &&
-                     array[cloneAnalyser.binIndexForFrequency(3000)] < 50 &&
-                     array[cloneAnalyser.binIndexForFrequency(5000)] > 200 &&
-                     array[cloneAnalyser.binIndexForFrequency(10000)] < 50)
-            .then(() => cloneAnalyser.disconnect());
-          })
-          // Restore original tracks
-          .then(() => stream.getTracks().forEach(t => t.enabled = true));
-      });
-    }));
+  cloneAnalyser = new AudioStreamAnalyser(ac, clone);
+  await cloneAnalyser.waitForAnalysisSuccess(array =>
+          array[cloneAnalyser.binIndexForFrequency(1000)] < 50 &&
+          array[cloneAnalyser.binIndexForFrequency(3000)] < 50 &&
+          array[cloneAnalyser.binIndexForFrequency(5000)] > 200 &&
+          array[cloneAnalyser.binIndexForFrequency(10000)] < 50);
+  cloneAnalyser.disconnect();
+
+  // Restore original tracks
+  stream.getTracks().forEach(t => t.enabled = true);
+});
 </script>
 </pre>
 </body>
 </html>
diff --git a/dom/media/tests/mochitest/test_peerConnection_trackDisabling.html b/dom/media/tests/mochitest/test_peerConnection_trackDisabling.html
--- a/dom/media/tests/mochitest/test_peerConnection_trackDisabling.html
+++ b/dom/media/tests/mochitest/test_peerConnection_trackDisabling.html
@@ -7,34 +7,34 @@
 <body>
 <pre id="test">
 <script type="application/javascript">
 createHTML({
   bug: "1219711",
   title: "Disabling locally should be reflected remotely"
 });
 
-runNetworkTest(() => {
+runNetworkTest(async () => {
   var test = new PeerConnectionTest();
 
   // Always use fake tracks since we depend on video to be somewhat green and
   // audio to have a large 1000Hz component (or 440Hz if using fake devices).
   test.setMediaConstraints([{audio: true, video: true, fake: true}], []);
   test.chain.append([
     function CHECK_ASSUMPTIONS() {
       is(test.pcLocal.localMediaElements.length, 2,
          "pcLocal should have one media element");
       is(test.pcRemote.remoteMediaElements.length, 2,
          "pcRemote should have one media element");
       is(test.pcLocal._pc.getLocalStreams().length, 1,
          "pcLocal should have one stream");
       is(test.pcRemote._pc.getRemoteStreams().length, 1,
          "pcRemote should have one stream");
     },
-    function CHECK_VIDEO() {
+    async function CHECK_VIDEO() {
       var h = new CaptureStreamTestHelper2D();
       var localVideo = test.pcLocal.localMediaElements
         .find(e => e instanceof HTMLVideoElement);
       var remoteVideo = test.pcRemote.remoteMediaElements
         .find(e => e instanceof HTMLVideoElement);
       // We check a pixel somewhere away from the top left corner since
       // MediaEngineDefault puts semi-transparent time indicators there.
       const offsetX = 50;
@@ -46,55 +46,54 @@ runNetworkTest(() => {
       var checkVideoEnabled = video => h.waitForPixel(video,
         px => (px[3] = 255, h.isPixelNot(px, h.black, threshold)),
         { offsetX, offsetY }
       );
       var checkVideoDisabled = video => h.waitForPixel(video,
         px => (px[3] = 255, h.isPixel(px, h.black, threshold)),
         { offsetX, offsetY }
       );
-      return Promise.resolve()
-        .then(() => info("Checking local video enabled"))
-        .then(() => checkVideoEnabled(localVideo))
-        .then(() => info("Checking remote video enabled"))
-        .then(() => checkVideoEnabled(remoteVideo))
+
+      info("Checking local video enabled");
+      await checkVideoEnabled(localVideo);
+      info("Checking remote video enabled");
+      await checkVideoEnabled(remoteVideo);
 
-        .then(() => info("Disabling original"))
-        .then(() => test.pcLocal._pc.getLocalStreams()[0].getVideoTracks()[0].enabled = false)
+      info("Disabling original");
+      test.pcLocal._pc.getLocalStreams()[0].getVideoTracks()[0].enabled = false;
 
-        .then(() => info("Checking local video disabled"))
-        .then(() => checkVideoDisabled(localVideo))
-        .then(() => info("Checking remote video disabled"))
-        .then(() => checkVideoDisabled(remoteVideo))
+      info("Checking local video disabled");
+      await checkVideoDisabled(localVideo);
+      info("Checking remote video disabled");
+      await checkVideoDisabled(remoteVideo);
     },
-    function CHECK_AUDIO() {
+    async function CHECK_AUDIO() {
       var ac = new AudioContext();
       var localAnalyser = new AudioStreamAnalyser(ac, test.pcLocal._pc.getLocalStreams()[0]);
       var remoteAnalyser = new AudioStreamAnalyser(ac, test.pcRemote._pc.getRemoteStreams()[0]);
 
       var checkAudio = (analyser, fun) => analyser.waitForAnalysisSuccess(fun);
 
       var freq1k = localAnalyser.binIndexForFrequency(1000);
       var checkAudioEnabled = analyser =>
         checkAudio(analyser, array => array[freq1k] > 200);
       var checkAudioDisabled = analyser =>
         checkAudio(analyser, array => array[freq1k] < 50);
 
-      return Promise.resolve()
-        .then(() => info("Checking local audio enabled"))
-        .then(() => checkAudioEnabled(localAnalyser))
-        .then(() => info("Checking remote audio enabled"))
-        .then(() => checkAudioEnabled(remoteAnalyser))
+      info("Checking local audio enabled");
+      await checkAudioEnabled(localAnalyser);
+      info("Checking remote audio enabled");
+      await checkAudioEnabled(remoteAnalyser);
 
-        .then(() => test.pcLocal._pc.getLocalStreams()[0].getAudioTracks()[0].enabled = false)
+      test.pcLocal._pc.getLocalStreams()[0].getAudioTracks()[0].enabled = false;
 
-        .then(() => info("Checking local audio disabled"))
-        .then(() => checkAudioDisabled(localAnalyser))
-        .then(() => info("Checking remote audio disabled"))
-        .then(() => checkAudioDisabled(remoteAnalyser))
-    }
+      info("Checking local audio disabled");
+      await checkAudioDisabled(localAnalyser);
+      info("Checking remote audio disabled");
+      await checkAudioDisabled(remoteAnalyser);
+    },
   ]);
   test.run();
 });
 </script>
 </pre>
 </body>
 </html>
diff --git a/dom/media/tests/mochitest/test_peerConnection_trackDisabling_clones.html b/dom/media/tests/mochitest/test_peerConnection_trackDisabling_clones.html
--- a/dom/media/tests/mochitest/test_peerConnection_trackDisabling_clones.html
+++ b/dom/media/tests/mochitest/test_peerConnection_trackDisabling_clones.html
@@ -7,17 +7,17 @@
 <body>
 <pre id="test">
 <script type="application/javascript">
 createHTML({
   bug: "1219711",
   title: "Disabling locally should be reflected remotely, individually for clones"
 });
 
-runNetworkTest(() => {
+runNetworkTest(async () => {
   var test = new PeerConnectionTest();
 
   var originalStream;
   var localVideoOriginal;
 
   // Always use fake tracks since we depend on audio to have a large 1000Hz
   // component.
   test.setMediaConstraints([{audio: true, video: true, fake: true}], []);
@@ -38,17 +38,17 @@ runNetworkTest(() => {
          "pcLocal should have one media element");
       is(test.pcRemote.remoteMediaElements.length, 2,
          "pcRemote should have one media element");
       is(test.pcLocal._pc.getLocalStreams().length, 1,
          "pcLocal should have one stream");
       is(test.pcRemote._pc.getRemoteStreams().length, 1,
          "pcRemote should have one stream");
     },
-    function CHECK_VIDEO() {
+    async function CHECK_VIDEO() {
       info("Checking video");
       var h = new CaptureStreamTestHelper2D();
       var localVideoClone = test.pcLocal.localMediaElements
         .find(e => e instanceof HTMLVideoElement);
       var remoteVideoClone = test.pcRemote.remoteMediaElements
         .find(e => e instanceof HTMLVideoElement);
 
       // We check a pixel somewhere away from the top left corner since
@@ -64,88 +64,86 @@ runNetworkTest(() => {
         px => (px[3] = 255, h.isPixelNot(px, h.black, threshold)),
         { offsetX, offsetY }
       );
       var checkVideoDisabled = video => h.waitForPixel(video,
         px => (px[3] = 255, h.isPixel(px, h.black, threshold)),
         { offsetX, offsetY }
       );
 
-      return Promise.resolve()
-        .then(() => info("Checking local original enabled"))
-        .then(() => checkVideoEnabled(localVideoOriginal))
-        .then(() => info("Checking local clone enabled"))
-        .then(() => checkVideoEnabled(localVideoClone))
-        .then(() => info("Checking remote clone enabled"))
-        .then(() => checkVideoEnabled(remoteVideoClone))
+      info("Checking local original enabled");
+      await checkVideoEnabled(localVideoOriginal);
+      info("Checking local clone enabled");
+      await checkVideoEnabled(localVideoClone);
+      info("Checking remote clone enabled");
+      await checkVideoEnabled(remoteVideoClone);
 
-        .then(() => info("Disabling original"))
-        .then(() => originalStream.getVideoTracks()[0].enabled = false)
+      info("Disabling original");
+      originalStream.getVideoTracks()[0].enabled = false;
 
-        .then(() => info("Checking local original disabled"))
-        .then(() => checkVideoDisabled(localVideoOriginal))
-        .then(() => info("Checking local clone enabled"))
-        .then(() => checkVideoEnabled(localVideoClone))
-        .then(() => info("Checking remote clone enabled"))
-        .then(() => checkVideoEnabled(remoteVideoClone))
+      info("Checking local original disabled");
+      await checkVideoDisabled(localVideoOriginal);
+      info("Checking local clone enabled");
+      await checkVideoEnabled(localVideoClone);
+      info("Checking remote clone enabled");
+      await checkVideoEnabled(remoteVideoClone);
 
-        .then(() => info("Re-enabling original; disabling clone"))
-        .then(() => originalStream.getVideoTracks()[0].enabled = true)
-        .then(() => test.pcLocal._pc.getLocalStreams()[0].getVideoTracks()[0].enabled = false)
+      info("Re-enabling original; disabling clone");
+      originalStream.getVideoTracks()[0].enabled = true;
+      test.pcLocal._pc.getLocalStreams()[0].getVideoTracks()[0].enabled = false;
 
-        .then(() => info("Checking local original enabled"))
-        .then(() => checkVideoEnabled(localVideoOriginal))
-        .then(() => info("Checking local clone disabled"))
-        .then(() => checkVideoDisabled(localVideoClone))
-        .then(() => info("Checking remote clone disabled"))
-        .then(() => checkVideoDisabled(remoteVideoClone))
+      info("Checking local original enabled");
+      await checkVideoEnabled(localVideoOriginal);
+      info("Checking local clone disabled");
+      await checkVideoDisabled(localVideoClone);
+      info("Checking remote clone disabled");
+      await checkVideoDisabled(remoteVideoClone);
     },
-    function CHECK_AUDIO() {
+    async function CHECK_AUDIO() {
       info("Checking audio");
       var ac = new AudioContext();
       var localAnalyserOriginal = new AudioStreamAnalyser(ac, originalStream);
       var localAnalyserClone =
         new AudioStreamAnalyser(ac, test.pcLocal._pc.getLocalStreams()[0]);
       var remoteAnalyserClone =
         new AudioStreamAnalyser(ac, test.pcRemote._pc.getRemoteStreams()[0]);
 
       var freq1k = localAnalyserOriginal.binIndexForFrequency(1000);
       var checkAudioEnabled = analyser =>
         analyser.waitForAnalysisSuccess(array => array[freq1k] > 200);
       var checkAudioDisabled = analyser =>
         analyser.waitForAnalysisSuccess(array => array[freq1k] < 50);
 
-      return Promise.resolve()
-        .then(() => info("Checking local original enabled"))
-        .then(() => checkAudioEnabled(localAnalyserOriginal))
-        .then(() => info("Checking local clone enabled"))
-        .then(() => checkAudioEnabled(localAnalyserClone))
-        .then(() => info("Checking remote clone enabled"))
-        .then(() => checkAudioEnabled(remoteAnalyserClone))
+      info("Checking local original enabled");
+      await checkAudioEnabled(localAnalyserOriginal);
+      info("Checking local clone enabled");
+      await checkAudioEnabled(localAnalyserClone);
+      info("Checking remote clone enabled");
+      await checkAudioEnabled(remoteAnalyserClone);
 
-        .then(() => info("Disabling original"))
-        .then(() => originalStream.getAudioTracks()[0].enabled = false)
+      info("Disabling original");
+      originalStream.getAudioTracks()[0].enabled = false;
 
-        .then(() => info("Checking local original disabled"))
-        .then(() => checkAudioDisabled(localAnalyserOriginal))
-        .then(() => info("Checking local clone enabled"))
-        .then(() => checkAudioEnabled(localAnalyserClone))
-        .then(() => info("Checking remote clone enabled"))
-        .then(() => checkAudioEnabled(remoteAnalyserClone))
+      info("Checking local original disabled");
+      await checkAudioDisabled(localAnalyserOriginal);
+      info("Checking local clone enabled");
+      await checkAudioEnabled(localAnalyserClone);
+      info("Checking remote clone enabled");
+      await checkAudioEnabled(remoteAnalyserClone);
 
-        .then(() => info("Re-enabling original; disabling clone"))
-        .then(() => originalStream.getAudioTracks()[0].enabled = true)
-        .then(() => test.pcLocal._pc.getLocalStreams()[0].getAudioTracks()[0].enabled = false)
+      info("Re-enabling original; disabling clone");
+      originalStream.getAudioTracks()[0].enabled = true;
+      test.pcLocal._pc.getLocalStreams()[0].getAudioTracks()[0].enabled = false;
 
-        .then(() => info("Checking local original enabled"))
-        .then(() => checkAudioEnabled(localAnalyserOriginal))
-        .then(() => info("Checking local clone disabled"))
-        .then(() => checkAudioDisabled(localAnalyserClone))
-        .then(() => info("Checking remote clone disabled"))
-        .then(() => checkAudioDisabled(remoteAnalyserClone))
-    }
+      info("Checking local original enabled");
+      await checkAudioEnabled(localAnalyserOriginal);
+      info("Checking local clone disabled");
+      await checkAudioDisabled(localAnalyserClone);
+      info("Checking remote clone disabled");
+      await checkAudioDisabled(remoteAnalyserClone);
+    },
   ]);
   test.run();
 });
 </script>
 </pre>
 </body>
 </html>
