# HG changeset patch
# User Daisuke Akatsuka <dakatsuka@mozilla.com>
# Date 1507600282 -32400
# Node ID c2d9bd9f5561914c84f584dab3d23ccf669f8674
# Parent  5509f6093b0e926d539e55151b8b3893102ee642
Bug 1405983 - Part 1: Display delay area in summary graph as 0 if fill is none or forwards. r=pbro

MozReview-Commit-ID: 6PRlPThxRw8

diff --git a/devtools/client/animationinspector/graph-helper.js b/devtools/client/animationinspector/graph-helper.js
--- a/devtools/client/animationinspector/graph-helper.js
+++ b/devtools/client/animationinspector/graph-helper.js
@@ -351,23 +351,30 @@ SummaryGraphHelper.prototype = {
    * Also, allows null value. In case of null, this graph helper shapes graph using
    * computed timing progress.
    * @param {Object} keyframes - Should have offset and easing, or null.
    */
   setKeyframes: function (keyframes) {
     let frames = null;
     if (keyframes) {
       // Create new keyframes for opacity as computed style.
+      // The reason why we use computed value instead of computed timing progress is to
+      // include the easing in keyframes as well. Although the computed timing progress
+      // is not affected by the easing in keyframes at all, computed value reflects that.
       frames = keyframes.map(keyframe => {
         return {
           opacity: keyframe.offset,
           offset: keyframe.offset,
           easing: keyframe.easing
         };
       });
+
+      // Set the underlying opacity to zero so that if we sample the animation's output
+      // during the delay phase and it is not filling backwards, we get zero.
+      this.targetEl.style.opacity = 0;
     }
     this.animation.effect.setKeyframes(frames);
     this.hasFrames = !!frames;
   },
 
   /*
    * Set animation behavior.
    * In Animation::SetCurrentTime spec, even if current time of animation is over
diff --git a/devtools/client/animationinspector/test/browser_animation_summarygraph_for_multiple_easings.js b/devtools/client/animationinspector/test/browser_animation_summarygraph_for_multiple_easings.js
--- a/devtools/client/animationinspector/test/browser_animation_summarygraph_for_multiple_easings.js
+++ b/devtools/client/animationinspector/test/browser_animation_summarygraph_for_multiple_easings.js
@@ -17,17 +17,16 @@ const TEST_CASES = {
   "no-easing": {
     expectedKeyframeEasingGraphs: [
       [
         { x: 0, y: 0 },
         { x: 25000, y: 0.25 },
         { x: 50000, y: 0.5 },
         { x: 75000, y: 0.75 },
         { x: 99000, y: 0.99 },
-        { x: 100000, y: 1 },
         { x: 100000, y: 0 },
       ]
     ]
   },
   "effect-easing": {
     expectedEffectEasingGraph: [
       { x: 0, y: 0 },
       { x: 19999, y: 0.0 },
@@ -59,17 +58,16 @@ const TEST_CASES = {
   },
   "keyframe-easing": {
     expectedKeyframeEasingGraphs: [
       [
         { x: 0, y: 0 },
         { x: 49999, y: 0.0 },
         { x: 50000, y: 0.5 },
         { x: 99999, y: 0.5 },
-        { x: 100000, y: 1 },
         { x: 100000, y: 0 },
       ]
     ]
   },
   "both-easing": {
     expectedEffectEasingGraph: [
       { x: 0, y: 0 },
       { x: 9999, y: 0.0 },
@@ -102,25 +100,23 @@ const TEST_CASES = {
         { x: 20000, y: 0.2 },
         { x: 39999, y: 0.2 },
         { x: 40000, y: 0.4 },
         { x: 59999, y: 0.4 },
         { x: 60000, y: 0.6 },
         { x: 79999, y: 0.6 },
         { x: 80000, y: 0.8 },
         { x: 99999, y: 0.8 },
-        { x: 100000, y: 1 },
         { x: 100000, y: 0 },
       ],
       [
         { x: 0, y: 0 },
         { x: 49999, y: 0.0 },
         { x: 50000, y: 0.5 },
         { x: 99999, y: 0.5 },
-        { x: 100000, y: 1 },
         { x: 100000, y: 0 },
       ]
     ]
   }
 };
 
 add_task(function* () {
   yield addTab(URL_ROOT + "doc_multiple_easings.html");
diff --git a/devtools/client/animationinspector/test/browser_animation_timeline_short_duration.js b/devtools/client/animationinspector/test/browser_animation_timeline_short_duration.js
--- a/devtools/client/animationinspector/test/browser_animation_timeline_short_duration.js
+++ b/devtools/client/animationinspector/test/browser_animation_timeline_short_duration.js
@@ -77,19 +77,18 @@ function checkSummaryGraph(el, state, is
       // The test animation is not 'forwards' fill-mode.
       // Therefore, the y of second last path segment will be 0.
       const secondLastPathSeg =
         pathSegList.getItem(pathSegList.numberOfItems - 3);
       is(secondLastPathSeg.x, endX,
          `The x of second last segment should be ${ endX }`);
       // We use computed style of 'opacity' to create summary graph.
       // So, if currentTime is same to the duration, although progress is null
-      // opacity is 1.
-      const expectedY =
-        state.iterationCount && expectedIterationCount === index + 1 ? 1 : 0;
+      // opacity is 0.
+      const expectedY = 0;
       is(secondLastPathSeg.y, expectedY,
          `The y of second last segment should be ${ expectedY }`);
 
       const lastPathSeg = pathSegList.getItem(pathSegList.numberOfItems - 2);
       is(lastPathSeg.x, endX, `The x of last segment should be ${ endX }`);
       is(lastPathSeg.y, 0, "The y of last segment should be 0");
 
       const closePathSeg = pathSegList.getItem(pathSegList.numberOfItems - 1);
