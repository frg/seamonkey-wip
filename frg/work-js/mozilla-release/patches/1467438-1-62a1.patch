# HG changeset patch
# User Andre Bargull <andre.bargull@gmail.com>
# Date 1529699379 25200
#      Fri Jun 22 13:29:39 2018 -0700
# Node ID d600ad533a95d45ae773c0888fb6093e711fe6b6
# Parent  cfd22ae5e9319bab9e266673bb6ac4e3511b3429
Bug 1467438 - Part 1: Replace ScopedJSFreePtr with UniqueChars/UniqueTwoByteChars. r=sfink

diff --git a/js/src/builtin/ReflectParse.cpp b/js/src/builtin/ReflectParse.cpp
--- a/js/src/builtin/ReflectParse.cpp
+++ b/js/src/builtin/ReflectParse.cpp
@@ -3346,17 +3346,17 @@ reflect_parse(JSContext* cx, uint32_t ar
                                   "Reflect.parse", "0", "s");
         return false;
     }
 
     RootedString src(cx, ToString<CanGC>(cx, args[0]));
     if (!src)
         return false;
 
-    ScopedJSFreePtr<char> filename;
+    UniqueChars filename;
     uint32_t lineno = 1;
     bool loc = true;
     RootedObject builder(cx);
     ParseGoal target = ParseGoal::Script;
 
     RootedValue arg(cx, args.get(1));
 
     if (!arg.isNullOrUndefined()) {
@@ -3385,17 +3385,17 @@ reflect_parse(JSContext* cx, uint32_t ar
             if (!GetPropertyDefault(cx, config, sourceId, nullVal, &prop))
                 return false;
 
             if (!prop.isNullOrUndefined()) {
                 RootedString str(cx, ToString<CanGC>(cx, prop));
                 if (!str)
                     return false;
 
-                filename = JS_EncodeString(cx, str);
+                filename.reset(JS_EncodeString(cx, str));
                 if (!filename)
                     return false;
             }
 
             /* config.line */
             RootedId lineId(cx, NameToId(cx->names().line));
             RootedValue oneValue(cx, Int32Value(1));
             if (!GetPropertyDefault(cx, config, lineId, oneValue, &prop) ||
@@ -3446,30 +3446,30 @@ reflect_parse(JSContext* cx, uint32_t ar
             target = ParseGoal::Module;
         } else {
             JS_ReportErrorASCII(cx, "Bad target value, expected 'script' or 'module'");
             return false;
         }
     }
 
     /* Extract the builder methods first to report errors before parsing. */
-    ASTSerializer serialize(cx, loc, filename, lineno);
+    ASTSerializer serialize(cx, loc, filename.get(), lineno);
     if (!serialize.init(builder))
         return false;
 
     JSLinearString* linear = src->ensureLinear(cx);
     if (!linear)
         return false;
 
     AutoStableStringChars linearChars(cx);
     if (!linearChars.initTwoByte(cx, linear))
         return false;
 
     CompileOptions options(cx);
-    options.setFileAndLine(filename, lineno);
+    options.setFileAndLine(filename.get(), lineno);
     options.setCanLazilyParse(false);
     options.allowHTMLComments = target == ParseGoal::Script;
     mozilla::Range<const char16_t> chars = linearChars.twoByteRange();
     UsedNameTracker usedNames(cx);
     if (!usedNames.init())
         return false;
 
     RootedScriptSourceObject sourceObject(cx, frontend::CreateScriptSourceObject(cx, options,
diff --git a/js/src/jsapi.cpp b/js/src/jsapi.cpp
--- a/js/src/jsapi.cpp
+++ b/js/src/jsapi.cpp
@@ -6639,17 +6639,17 @@ JS_ObjectIsDate(JSContext* cx, HandleObj
  * Regular Expressions.
  */
 JS_PUBLIC_API(JSObject*)
 JS_NewRegExpObject(JSContext* cx, const char* bytes, size_t length, unsigned flags)
 {
     AssertHeapIsIdle();
     CHECK_REQUEST(cx);
 
-    ScopedJSFreePtr<char16_t> chars(InflateString(cx, bytes, length));
+    UniqueTwoByteChars chars(InflateString(cx, bytes, length));
     if (!chars)
         return nullptr;
 
     return RegExpObject::create(cx, chars.get(), length, RegExpFlag(flags), cx->tempLifoAlloc(),
                                 GenericObject);
 }
 
 JS_PUBLIC_API(JSObject*)
diff --git a/js/src/jsnum.cpp b/js/src/jsnum.cpp
--- a/js/src/jsnum.cpp
+++ b/js/src/jsnum.cpp
@@ -81,32 +81,32 @@ EnsureDtoaState(JSContext* cx)
  * Call js_strtod_harder to get the correct answer.
  */
 template <typename CharT>
 static bool
 ComputeAccurateDecimalInteger(JSContext* cx, const CharT* start, const CharT* end,
                               double* dp)
 {
     size_t length = end - start;
-    ScopedJSFreePtr<char> cstr(cx->pod_malloc<char>(length + 1));
+    UniqueChars cstr(cx->pod_malloc<char>(length + 1));
     if (!cstr)
         return false;
 
     for (size_t i = 0; i < length; i++) {
         char c = char(start[i]);
         MOZ_ASSERT(IsAsciiAlphanumeric(c));
         cstr[i] = c;
     }
     cstr[length] = 0;
 
     if (!EnsureDtoaState(cx))
         return false;
 
     char* estr;
-    *dp = js_strtod_harder(cx->dtoaState, cstr, &estr);
+    *dp = js_strtod_harder(cx->dtoaState, cstr.get(), &estr);
 
     return true;
 }
 
 namespace {
 
 template <typename CharT>
 class BinaryDigitReader
diff --git a/js/src/shell/js.cpp b/js/src/shell/js.cpp
--- a/js/src/shell/js.cpp
+++ b/js/src/shell/js.cpp
@@ -854,33 +854,33 @@ RunFile(JSContext* cx, const char* filen
 static bool
 InitModuleLoader(JSContext* cx)
 {
 
     // Decompress and evaluate the embedded module loader source to initialize
     // the module loader for the current compartment.
 
     uint32_t srcLen = moduleloader::GetRawScriptsSize();
-    ScopedJSFreePtr<char> src(cx->pod_malloc<char>(srcLen));
+    UniqueChars src(cx->pod_malloc<char>(srcLen));
     if (!src || !DecompressString(moduleloader::compressedSources, moduleloader::GetCompressedSize(),
                                   reinterpret_cast<unsigned char*>(src.get()), srcLen))
     {
         return false;
     }
 
     CompileOptions options(cx);
     options.setIntroductionType("shell module loader");
     options.setFileAndLine("shell/ModuleLoader.js", 1);
     options.setSelfHostingMode(false);
     options.setCanLazilyParse(false);
     options.werrorOption = true;
     options.strictOption = true;
 
     RootedValue rv(cx);
-    return Evaluate(cx, options, src, srcLen, &rv);
+    return Evaluate(cx, options, src.get(), srcLen, &rv);
 }
 
 static bool
 GetLoaderObject(JSContext* cx, MutableHandleObject resultOut)
 {
     // Look up the |Reflect.Loader| object that has been defined by the module
     // loader.
 
diff --git a/js/src/util/StringBuffer.cpp b/js/src/util/StringBuffer.cpp
--- a/js/src/util/StringBuffer.cpp
+++ b/js/src/util/StringBuffer.cpp
@@ -2,16 +2,17 @@
  * vim: set ts=8 sts=4 et sw=4 tw=99:
  * This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "util/StringBuffer.h"
 
 #include "mozilla/Range.h"
+#include "mozilla/Unused.h"
 
 #include "vm/JSObject-inl.h"
 #include "vm/StringType-inl.h"
 
 using namespace js;
 
 template <typename CharT, class Buffer>
 static CharT*
@@ -74,31 +75,31 @@ StringBuffer::inflateChars()
 template <typename CharT, class Buffer>
 static JSFlatString*
 FinishStringFlat(JSContext* cx, StringBuffer& sb, Buffer& cb)
 {
     size_t len = sb.length();
     if (!sb.append('\0'))
         return nullptr;
 
-    ScopedJSFreePtr<CharT> buf(ExtractWellSized<CharT>(cx, cb));
+    UniquePtr<CharT[], JS::FreePolicy> buf(ExtractWellSized<CharT>(cx, cb));
     if (!buf)
         return nullptr;
 
     JSFlatString* str = NewStringDontDeflate<CanGC>(cx, buf.get(), len);
     if (!str)
         return nullptr;
 
     /*
      * The allocation was made on a TempAllocPolicy, so account for the string
      * data on the string's zone.
      */
     cx->updateMallocCounter(sizeof(CharT) * len);
 
-    buf.forget();
+    mozilla::Unused << buf.release();
     return str;
 }
 
 JSFlatString*
 StringBuffer::finishString()
 {
     size_t len = length();
     if (len == 0)
diff --git a/js/src/vm/Compartment.cpp b/js/src/vm/Compartment.cpp
--- a/js/src/vm/Compartment.cpp
+++ b/js/src/vm/Compartment.cpp
@@ -122,32 +122,32 @@ CopyStringPure(JSContext* cx, JSString* 
             return nullptr;
 
         return chars.isLatin1()
                ? NewStringCopyN<CanGC>(cx, chars.latin1Range().begin().get(), len)
                : NewStringCopyNDontDeflate<CanGC>(cx, chars.twoByteRange().begin().get(), len);
     }
 
     if (str->hasLatin1Chars()) {
-        ScopedJSFreePtr<Latin1Char> copiedChars;
-        if (!str->asRope().copyLatin1CharsZ(cx, copiedChars))
+        UniquePtr<Latin1Char[], JS::FreePolicy> copiedChars = str->asRope().copyLatin1CharsZ(cx);
+        if (!copiedChars)
             return nullptr;
 
-        auto* rawCopiedChars = copiedChars.forget();
+        auto* rawCopiedChars = copiedChars.release();
         auto* result = NewString<CanGC>(cx, rawCopiedChars, len);
         if (!result)
             js_free(rawCopiedChars);
         return result;
     }
 
-    ScopedJSFreePtr<char16_t> copiedChars;
-    if (!str->asRope().copyTwoByteCharsZ(cx, copiedChars))
+    UniqueTwoByteChars copiedChars = str->asRope().copyTwoByteCharsZ(cx);
+    if (!copiedChars)
         return nullptr;
 
-    auto* rawCopiedChars = copiedChars.forget();
+    auto* rawCopiedChars = copiedChars.release();
     auto* result = NewStringDontDeflate<CanGC>(cx, rawCopiedChars, len);
     if (!result)
         js_free(rawCopiedChars);
     return result;
 }
 
 bool
 Compartment::wrap(JSContext* cx, MutableHandleString strp)
diff --git a/js/src/vm/MemoryMetrics.cpp b/js/src/vm/MemoryMetrics.cpp
--- a/js/src/vm/MemoryMetrics.cpp
+++ b/js/src/vm/MemoryMetrics.cpp
@@ -76,34 +76,36 @@ InefficientNonFlatteningStringHashPolicy
 template <typename Char1, typename Char2>
 static bool
 EqualStringsPure(JSString* s1, JSString* s2)
 {
     if (s1->length() != s2->length())
         return false;
 
     const Char1* c1;
-    ScopedJSFreePtr<Char1> ownedChars1;
+    UniquePtr<Char1[], JS::FreePolicy> ownedChars1;
     JS::AutoCheckCannotGC nogc;
     if (s1->isLinear()) {
         c1 = s1->asLinear().chars<Char1>(nogc);
     } else {
-        if (!s1->asRope().copyChars<Char1>(/* tcx */ nullptr, ownedChars1))
+        ownedChars1 = s1->asRope().copyChars<Char1>(/* tcx */ nullptr);
+        if (!ownedChars1)
             MOZ_CRASH("oom");
-        c1 = ownedChars1;
+        c1 = ownedChars1.get();
     }
 
     const Char2* c2;
-    ScopedJSFreePtr<Char2> ownedChars2;
+    UniquePtr<Char2[], JS::FreePolicy> ownedChars2;
     if (s2->isLinear()) {
         c2 = s2->asLinear().chars<Char2>(nogc);
     } else {
-        if (!s2->asRope().copyChars<Char2>(/* tcx */ nullptr, ownedChars2))
+        ownedChars2 = s2->asRope().copyChars<Char2>(/* tcx */ nullptr);
+        if (!ownedChars2)
             MOZ_CRASH("oom");
-        c2 = ownedChars2;
+        c2 = ownedChars2.get();
     }
 
     return EqualChars(c1, c2, s1->length());
 }
 
 /* static */ bool
 InefficientNonFlatteningStringHashPolicy::match(const JSString* const& k, const Lookup& l)
 {
@@ -143,24 +145,25 @@ NotableStringInfo::NotableStringInfo()
 {
 }
 
 template <typename CharT>
 static void
 StoreStringChars(char* buffer, size_t bufferSize, JSString* str)
 {
     const CharT* chars;
-    ScopedJSFreePtr<CharT> ownedChars;
+    UniquePtr<CharT[], JS::FreePolicy> ownedChars;
     JS::AutoCheckCannotGC nogc;
     if (str->isLinear()) {
         chars = str->asLinear().chars<CharT>(nogc);
     } else {
-        if (!str->asRope().copyChars<CharT>(/* tcx */ nullptr, ownedChars))
+        ownedChars = str->asRope().copyChars<CharT>(/* tcx */ nullptr);
+        if (!ownedChars)
             MOZ_CRASH("oom");
-        chars = ownedChars;
+        chars = ownedChars.get();
     }
 
     // We might truncate |str| even if it's much shorter than 1024 chars, if
     // |str| contains unicode chars.  Since this is just for a memory reporter,
     // we don't care.
     PutEscapedString(buffer, bufferSize, chars, str->length(), /* quote */ 0);
 }
 
diff --git a/js/src/vm/SelfHosting.cpp b/js/src/vm/SelfHosting.cpp
--- a/js/src/vm/SelfHosting.cpp
+++ b/js/src/vm/SelfHosting.cpp
@@ -2966,24 +2966,24 @@ JSRuntime::initSelfHosting(JSContext* cx
     FillSelfHostingCompileOptions(options);
 
     RootedValue rv(cx);
 
     uint32_t srcLen = GetRawScriptsSize();
 
     const unsigned char* compressed = compressedSources;
     uint32_t compressedLen = GetCompressedSize();
-    ScopedJSFreePtr<char> src(selfHostingGlobal_->zone()->pod_malloc<char>(srcLen));
+    UniqueChars src(selfHostingGlobal_->zone()->pod_malloc<char>(srcLen));
     if (!src || !DecompressString(compressed, compressedLen,
                                   reinterpret_cast<unsigned char*>(src.get()), srcLen))
     {
         return false;
     }
 
-    if (!Evaluate(cx, options, src, srcLen, &rv))
+    if (!Evaluate(cx, options, src.get(), srcLen, &rv))
         return false;
 
     if (!VerifyGlobalNames(cx, shg))
         return false;
 
     return true;
 }
 
diff --git a/js/src/vm/StringType.cpp b/js/src/vm/StringType.cpp
--- a/js/src/vm/StringType.cpp
+++ b/js/src/vm/StringType.cpp
@@ -274,81 +274,82 @@ AllocChars(JSString* str, size_t length,
     /* Like length, capacity does not include the null char, so take it out. */
     *capacity = numChars - 1;
 
     JS_STATIC_ASSERT(JSString::MAX_LENGTH * sizeof(CharT) < UINT32_MAX);
     *chars = str->zone()->pod_malloc<CharT>(numChars);
     return *chars != nullptr;
 }
 
-bool
-JSRope::copyLatin1CharsZ(JSContext* cx, ScopedJSFreePtr<Latin1Char>& out) const
+UniquePtr<Latin1Char[], JS::FreePolicy>
+JSRope::copyLatin1CharsZ(JSContext* cx) const
 {
-    return copyCharsInternal<Latin1Char>(cx, out, true);
+    return copyCharsInternal<Latin1Char>(cx, true);
 }
 
-bool
-JSRope::copyTwoByteCharsZ(JSContext* cx, ScopedJSFreePtr<char16_t>& out) const
+UniqueTwoByteChars
+JSRope::copyTwoByteCharsZ(JSContext* cx) const
 {
-    return copyCharsInternal<char16_t>(cx, out, true);
+    return copyCharsInternal<char16_t>(cx, true);
 }
 
-bool
-JSRope::copyLatin1Chars(JSContext* cx, ScopedJSFreePtr<Latin1Char>& out) const
+UniquePtr<Latin1Char[], JS::FreePolicy>
+JSRope::copyLatin1Chars(JSContext* cx) const
 {
-    return copyCharsInternal<Latin1Char>(cx, out, false);
+    return copyCharsInternal<Latin1Char>(cx, false);
 }
 
-bool
-JSRope::copyTwoByteChars(JSContext* cx, ScopedJSFreePtr<char16_t>& out) const
+UniqueTwoByteChars
+JSRope::copyTwoByteChars(JSContext* cx) const
 {
-    return copyCharsInternal<char16_t>(cx, out, false);
+    return copyCharsInternal<char16_t>(cx, false);
 }
 
 template <typename CharT>
-bool
-JSRope::copyCharsInternal(JSContext* cx, ScopedJSFreePtr<CharT>& out,
-                          bool nullTerminate) const
+UniquePtr<CharT[], JS::FreePolicy>
+JSRope::copyCharsInternal(JSContext* cx, bool nullTerminate) const
 {
     // Left-leaning ropes are far more common than right-leaning ropes, so
     // perform a non-destructive traversal of the rope, right node first,
     // splatting each node's characters into a contiguous buffer.
 
     size_t n = length();
+
+    UniquePtr<CharT[], JS::FreePolicy> out;
     if (cx)
         out.reset(cx->pod_malloc<CharT>(n + 1));
     else
         out.reset(js_pod_malloc<CharT>(n + 1));
 
     if (!out)
-        return false;
+        return nullptr;
 
     Vector<const JSString*, 8, SystemAllocPolicy> nodeStack;
     const JSString* str = this;
-    CharT* end = out + str->length();
+    CharT* end = out.get() + str->length();
     while (true) {
         if (str->isRope()) {
             if (!nodeStack.append(str->asRope().leftChild()))
-                return false;
+                return nullptr;
             str = str->asRope().rightChild();
         } else {
             end -= str->length();
             CopyChars(end, str->asLinear());
             if (nodeStack.empty())
                 break;
             str = nodeStack.popCopy();
         }
     }
 
-    MOZ_ASSERT(end == out);
+    MOZ_ASSERT(end == out.get());
 
     if (nullTerminate)
         out[n] = 0;
 
-    return true;
+    return out;
 }
 
 template <typename CharT>
 void AddStringToHash(uint32_t* hash, const CharT* chars, size_t len)
 {
     // It's tempting to use |HashString| instead of this loop, but that's
     // slightly different than our existing implementation for non-ropes. We
     // want to pretend we have a contiguous set of chars so we need to
@@ -1497,31 +1498,31 @@ static JSFlatString*
 NewStringDeflated(JSContext* cx, const char16_t* s, size_t n)
 {
     if (JSFlatString* str = TryEmptyOrStaticString(cx, s, n))
         return str;
 
     if (JSInlineString::lengthFits<Latin1Char>(n))
         return NewInlineStringDeflated<allowGC>(cx, mozilla::Range<const char16_t>(s, n));
 
-    ScopedJSFreePtr<Latin1Char> news(cx->pod_malloc<Latin1Char>(n + 1));
+    UniquePtr<Latin1Char[], JS::FreePolicy> news(cx->pod_malloc<Latin1Char>(n + 1));
     if (!news)
         return nullptr;
 
     for (size_t i = 0; i < n; i++) {
         MOZ_ASSERT(s[i] <= JSString::MAX_LATIN1_CHAR);
-        news.get()[i] = Latin1Char(s[i]);
+        news[i] = Latin1Char(s[i]);
     }
     news[n] = '\0';
 
     JSFlatString* str = JSFlatString::new_<allowGC>(cx, news.get(), n);
     if (!str)
         return nullptr;
 
-    news.forget();
+    mozilla::Unused << news.release();
     return str;
 }
 
 template <AllowGC allowGC>
 static JSFlatString*
 NewStringDeflated(JSContext* cx, const Latin1Char* s, size_t n)
 {
     MOZ_CRASH("Shouldn't be called for Latin1 chars");
@@ -1599,31 +1600,31 @@ JSFlatString*
 NewStringCopyNDontDeflate(JSContext* cx, const CharT* s, size_t n)
 {
     if (JSFlatString* str = TryEmptyOrStaticString(cx, s, n))
         return str;
 
     if (JSInlineString::lengthFits<CharT>(n))
         return NewInlineString<allowGC>(cx, mozilla::Range<const CharT>(s, n));
 
-    ScopedJSFreePtr<CharT> news(cx->pod_malloc<CharT>(n + 1));
+    UniquePtr<CharT[], JS::FreePolicy> news(cx->pod_malloc<CharT>(n + 1));
     if (!news) {
         if (!allowGC)
             cx->recoverFromOutOfMemory();
         return nullptr;
     }
 
     PodCopy(news.get(), s, n);
     news[n] = 0;
 
     JSFlatString* str = JSFlatString::new_<allowGC>(cx, news.get(), n);
     if (!str)
         return nullptr;
 
-    news.forget();
+    mozilla::Unused << news.release();
     return str;
 }
 
 template JSFlatString*
 NewStringCopyNDontDeflate<CanGC>(JSContext* cx, const char16_t* s, size_t n);
 
 template JSFlatString*
 NewStringCopyNDontDeflate<NoGC>(JSContext* cx, const char16_t* s, size_t n);
diff --git a/js/src/vm/StringType.h b/js/src/vm/StringType.h
--- a/js/src/vm/StringType.h
+++ b/js/src/vm/StringType.h
@@ -17,16 +17,17 @@
 #include "builtin/String.h"
 #include "gc/Barrier.h"
 #include "gc/Cell.h"
 #include "gc/Heap.h"
 #include "gc/Nursery.h"
 #include "gc/Rooting.h"
 #include "js/CharacterEncoding.h"
 #include "js/RootingAPI.h"
+#include "js/UniquePtr.h"
 #include "util/Text.h"
 #include "vm/Printer.h"
 
 class JSDependentString;
 class JSExtensibleString;
 class JSExternalString;
 class JSInlineString;
 class JSRope;
@@ -661,18 +662,18 @@ class JSString : public js::gc::Cell
     JSString() = delete;
     JSString(const JSString& other) = delete;
     void operator=(const JSString& other) = delete;
 };
 
 class JSRope : public JSString
 {
     template <typename CharT>
-    bool copyCharsInternal(JSContext* cx, js::ScopedJSFreePtr<CharT>& out,
-                           bool nullTerminate) const;
+    js::UniquePtr<CharT[], JS::FreePolicy> copyCharsInternal(JSContext* cx,
+                                                             bool nullTerminate) const;
 
     enum UsingBarrier { WithIncrementalBarrier, NoBarrier };
 
     template<UsingBarrier b, typename CharT>
     JSFlatString* flattenInternal(JSContext* cx);
 
     template<UsingBarrier b>
     JSFlatString* flattenInternal(JSContext* cx);
@@ -684,26 +685,24 @@ class JSRope : public JSString
 
   public:
     template <js::AllowGC allowGC>
     static inline JSRope* new_(JSContext* cx,
                                typename js::MaybeRooted<JSString*, allowGC>::HandleType left,
                                typename js::MaybeRooted<JSString*, allowGC>::HandleType right,
                                size_t length, js::gc::InitialHeap = js::gc::DefaultHeap);
 
-    bool copyLatin1Chars(JSContext* cx,
-                         js::ScopedJSFreePtr<JS::Latin1Char>& out) const;
-    bool copyTwoByteChars(JSContext* cx, js::ScopedJSFreePtr<char16_t>& out) const;
+    js::UniquePtr<JS::Latin1Char[], JS::FreePolicy> copyLatin1Chars(JSContext* cx) const;
+    JS::UniqueTwoByteChars copyTwoByteChars(JSContext* cx) const;
 
-    bool copyLatin1CharsZ(JSContext* cx,
-                          js::ScopedJSFreePtr<JS::Latin1Char>& out) const;
-    bool copyTwoByteCharsZ(JSContext* cx, js::ScopedJSFreePtr<char16_t>& out) const;
+    js::UniquePtr<JS::Latin1Char[], JS::FreePolicy> copyLatin1CharsZ(JSContext* cx) const;
+    JS::UniqueTwoByteChars copyTwoByteCharsZ(JSContext* cx) const;
 
     template <typename CharT>
-    bool copyChars(JSContext* cx, js::ScopedJSFreePtr<CharT>& out) const;
+    js::UniquePtr<CharT[], JS::FreePolicy> copyChars(JSContext* cx) const;
 
     // Hash function specific for ropes that avoids allocating a temporary
     // string. There are still allocations internally so it's technically
     // fallible.
     //
     // Returns the same value as if this were a linear string being hashed.
     MOZ_MUST_USE bool hash(uint32_t* outhHash) const;
 
@@ -1734,28 +1733,27 @@ JSLinearString::chars(const JS::AutoRequ
 template<>
 MOZ_ALWAYS_INLINE const JS::Latin1Char*
 JSLinearString::chars(const JS::AutoRequireNoGC& nogc) const
 {
     return rawLatin1Chars();
 }
 
 template <>
-MOZ_ALWAYS_INLINE bool
-JSRope::copyChars<JS::Latin1Char>(JSContext* cx,
-                                  js::ScopedJSFreePtr<JS::Latin1Char>& out) const
+MOZ_ALWAYS_INLINE js::UniquePtr<JS::Latin1Char[], JS::FreePolicy>
+JSRope::copyChars<JS::Latin1Char>(JSContext* cx) const
 {
-    return copyLatin1Chars(cx, out);
+    return copyLatin1Chars(cx);
 }
 
 template <>
-MOZ_ALWAYS_INLINE bool
-JSRope::copyChars<char16_t>(JSContext* cx, js::ScopedJSFreePtr<char16_t>& out) const
+MOZ_ALWAYS_INLINE JS::UniqueTwoByteChars
+JSRope::copyChars<char16_t>(JSContext* cx) const
 {
-    return copyTwoByteChars(cx, out);
+    return copyTwoByteChars(cx);
 }
 
 template<>
 MOZ_ALWAYS_INLINE bool
 JSThinInlineString::lengthFits<JS::Latin1Char>(size_t length)
 {
     return length <= MAX_LENGTH_LATIN1;
 }
