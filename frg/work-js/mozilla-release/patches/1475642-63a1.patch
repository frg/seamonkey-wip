# HG changeset patch
# User Jean-Yves Avenard <jyavenard@mozilla.com>
# Date 1531553177 0
# Node ID 04dd259d71db60341016eccf53ced43742319631
# Parent  ff25d41b504f255587a0aa7a498337c39a666eb0
Bug 1475642 - Fix playback with some 3rd party audio cards. r=kinetik

Additional fix to bug 1469152

Differential Revision: https://phabricator.services.mozilla.com/D2131

diff --git a/media/libcubeb/0002-Always-upmix-mono-to-the-first-two-channels-if-enoug.patch b/media/libcubeb/0002-Always-upmix-mono-to-the-first-two-channels-if-enoug.patch
--- a/media/libcubeb/0002-Always-upmix-mono-to-the-first-two-channels-if-enoug.patch
+++ b/media/libcubeb/0002-Always-upmix-mono-to-the-first-two-channels-if-enoug.patch
@@ -1,22 +1,8 @@
-From dbd61924736fbe1a1caf1cbd544f7d197f1836f7 Mon Sep 17 00:00:00 2001
-From: Jean-Yves Avenard <jyavenard@mozilla.com>
-Date: Mon, 9 Jul 2018 20:10:13 +0200
-Subject: [PATCH 2/2] Always upmix mono to the first two channels if enough
- output channels are available
-
-This allows to output what people typically expect when playing mono audio: sound coming from both left and right channels.
-
-To force this conversion for happening on mac, we tag that layout are unknown as soon as a channel type is unknown
----
- src/cubeb_audiounit.cpp |  8 ++++++--
- src/cubeb_mixer.cpp     | 12 ++++++++++++
- 2 files changed, 18 insertions(+), 2 deletions(-)
-
 diff --git a/src/cubeb_audiounit.cpp b/src/cubeb_audiounit.cpp
 index 43120b3..5a83098 100644
 --- a/src/cubeb_audiounit.cpp
 +++ b/src/cubeb_audiounit.cpp
 @@ -291,7 +291,7 @@ cubeb_channel_to_channel_label(cubeb_channel channel)
      case CHANNEL_TOP_BACK_RIGHT:
        return kAudioChannelLabel_TopBackRight;
      default:
@@ -35,33 +21,31 @@ index 43120b3..5a83098 100644
 +    if (cc == CHANNEL_UNKNOWN) {
 +      return CUBEB_LAYOUT_UNDEFINED;
 +    }
 +    cl |= cc;
    }
  
    return cl;
 diff --git a/src/cubeb_mixer.cpp b/src/cubeb_mixer.cpp
-index 8995c55..9aa141d 100644
+index 8995c55..c95fef6 100644
 --- a/src/cubeb_mixer.cpp
 +++ b/src/cubeb_mixer.cpp
-@@ -533,6 +533,18 @@ struct cubeb_mixer
-         output_buffer += _context._out_ch_count - _context._in_ch_count;
-       }
-     } else {
+@@ -525,6 +525,19 @@ struct cubeb_mixer
+   {
+     if (_context._in_ch_count <= _context._out_ch_count) {
+       // Not enough channels to copy, fill the gaps with silence.
 +      if (_context._in_ch_count == 1 && _context._out_ch_count >= 2) {
 +        // Special case for upmixing mono input to stereo and more. We will
 +        // duplicate the mono channel to the first two channels. On most system,
 +        // the first two channels are for left and right. It is commonly
 +        // expected that mono will on both left+right channels
 +        for (uint32_t i = 0; i < frames; i++) {
 +          output_buffer[0] = output_buffer[1] = *input_buffer;
-+          output_buffer += _context._out_ch_count ;
++          PodZero(output_buffer + 2, _context._out_ch_count - 2);
++          output_buffer += _context._out_ch_count;
 +          input_buffer++;
 +        }
 +        return;
 +      }
        for (uint32_t i = 0; i < frames; i++) {
-         PodCopy(output_buffer, input_buffer, _context._out_ch_count);
-         output_buffer += _context._out_ch_count;
--- 
-2.17.0
-
+         PodCopy(output_buffer, input_buffer, _context._in_ch_count);
+         output_buffer += _context._in_ch_count;
diff --git a/media/libcubeb/src/cubeb_mixer.cpp b/media/libcubeb/src/cubeb_mixer.cpp
--- a/media/libcubeb/src/cubeb_mixer.cpp
+++ b/media/libcubeb/src/cubeb_mixer.cpp
@@ -520,36 +520,37 @@ struct cubeb_mixer
 
   template<typename T>
   void copy_and_trunc(size_t frames,
                       const T * input_buffer,
                       T * output_buffer) const
   {
     if (_context._in_ch_count <= _context._out_ch_count) {
       // Not enough channels to copy, fill the gaps with silence.
+      if (_context._in_ch_count == 1 && _context._out_ch_count >= 2) {
+        // Special case for upmixing mono input to stereo and more. We will
+        // duplicate the mono channel to the first two channels. On most system,
+        // the first two channels are for left and right. It is commonly
+        // expected that mono will on both left+right channels
+        for (uint32_t i = 0; i < frames; i++) {
+          output_buffer[0] = output_buffer[1] = *input_buffer;
+          PodZero(output_buffer + 2, _context._out_ch_count - 2);
+          output_buffer += _context._out_ch_count;
+          input_buffer++;
+        }
+        return;
+      }
       for (uint32_t i = 0; i < frames; i++) {
         PodCopy(output_buffer, input_buffer, _context._in_ch_count);
         output_buffer += _context._in_ch_count;
         input_buffer += _context._in_ch_count;
         PodZero(output_buffer, _context._out_ch_count - _context._in_ch_count);
         output_buffer += _context._out_ch_count - _context._in_ch_count;
       }
     } else {
-      if (_context._in_ch_count == 1 && _context._out_ch_count >= 2) {
-        // Special case for upmixing mono input to stereo and more. We will
-        // duplicate the mono channel to the first two channels. On most system,
-        // the first two channels are for left and right. It is commonly
-        // expected that mono will on both left+right channels
-        for (uint32_t i = 0; i < frames; i++) {
-          output_buffer[0] = output_buffer[1] = *input_buffer;
-          output_buffer += _context._out_ch_count ;
-          input_buffer++;
-        }
-        return;
-      }
       for (uint32_t i = 0; i < frames; i++) {
         PodCopy(output_buffer, input_buffer, _context._out_ch_count);
         output_buffer += _context._out_ch_count;
         input_buffer += _context._in_ch_count;
       }
     }
   }
 

