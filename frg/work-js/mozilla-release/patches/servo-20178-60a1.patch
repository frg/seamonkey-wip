# HG changeset patch
# User Anthony Ramine <n.oxyde@gmail.com>
# Date 1520090262 18000
# Node ID e3f41cf7ac017e66d36ecc5ca372303e9606a32d
# Parent  9fd956a82519eba435d74710123f3653b8812f3c
servo: Merge #20178 - Derive ToCss for some more stuff (from servo:derive-all-the-things); r=emilio

Source-Repo: https://github.com/servo/servo
Source-Revision: 0c9be9f77630c30120a72e50f0865b8b6e55db00

diff --git a/servo/components/style/counter_style/mod.rs b/servo/components/style/counter_style/mod.rs
--- a/servo/components/style/counter_style/mod.rs
+++ b/servo/components/style/counter_style/mod.rs
@@ -3,17 +3,17 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 //! The [`@counter-style`][counter-style] at-rule.
 //!
 //! [counter-style]: https://drafts.csswg.org/css-counter-styles/
 
 use Atom;
 use cssparser::{AtRuleParser, DeclarationListParser, DeclarationParser};
-use cssparser::{Parser, Token, serialize_identifier, CowRcStr};
+use cssparser::{Parser, Token, CowRcStr};
 use error_reporting::{ContextualParseError, ParseErrorReporter};
 #[cfg(feature = "gecko")] use gecko::rules::CounterStyleDescriptors;
 #[cfg(feature = "gecko")] use gecko_bindings::structs::{ nsCSSCounterDesc, nsCSSValue };
 use parser::{ParserContext, ParserErrorContext, Parse};
 use selectors::parser::SelectorParseErrorKind;
 use shared_lock::{SharedRwLockReadGuard, ToCssWithGuard};
 #[allow(unused_imports)] use std::ascii::AsciiExt;
 use std::borrow::Cow;
@@ -386,50 +386,42 @@ impl ToCss for System {
                 other.to_css(dest)
             }
         }
     }
 }
 
 /// <https://drafts.csswg.org/css-counter-styles/#typedef-symbol>
 #[cfg_attr(feature = "gecko", derive(MallocSizeOf))]
-#[derive(Clone, Debug, Eq, PartialEq, ToComputedValue)]
+#[derive(Clone, Debug, Eq, PartialEq, ToComputedValue, ToCss)]
 pub enum Symbol {
     /// <string>
     String(String),
-    /// <ident>
-    Ident(String),
+    /// <custom-ident>
+    Ident(CustomIdent),
     // Not implemented:
     // /// <image>
     // Image(Image),
 }
 
 impl Parse for Symbol {
     fn parse<'i, 't>(_context: &ParserContext, input: &mut Parser<'i, 't>) -> Result<Self, ParseError<'i>> {
         let location = input.current_source_location();
         match *input.next()? {
             Token::QuotedString(ref s) => Ok(Symbol::String(s.as_ref().to_owned())),
-            Token::Ident(ref s) => Ok(Symbol::Ident(s.as_ref().to_owned())),
+            Token::Ident(ref s) => {
+                Ok(Symbol::Ident(
+                    CustomIdent::from_ident(location, s, &[])?,
+                ))
+            }
             ref t => Err(location.new_unexpected_token_error(t.clone())),
         }
     }
 }
 
-impl ToCss for Symbol {
-    fn to_css<W>(&self, dest: &mut CssWriter<W>) -> fmt::Result
-    where
-        W: Write,
-    {
-        match *self {
-            Symbol::String(ref s) => s.to_css(dest),
-            Symbol::Ident(ref s) => serialize_identifier(s, dest),
-        }
-    }
-}
-
 impl Symbol {
     /// Returns whether this symbol is allowed in symbols() function.
     pub fn is_allowed_in_symbols(&self) -> bool {
         match self {
             // Identifier is not allowed.
             &Symbol::Ident(_) => false,
             _ => true,
         }
@@ -545,17 +537,18 @@ pub struct Fallback(pub CustomIdent);
 impl Parse for Fallback {
     fn parse<'i, 't>(_context: &ParserContext, input: &mut Parser<'i, 't>) -> Result<Self, ParseError<'i>> {
         parse_counter_style_name(input).map(Fallback)
     }
 }
 
 /// <https://drafts.csswg.org/css-counter-styles/#descdef-counter-style-symbols>
 #[cfg_attr(feature = "gecko", derive(MallocSizeOf))]
-#[derive(Clone, Debug, Eq, PartialEq, ToComputedValue)]
+#[css(iterable)]
+#[derive(Clone, Debug, Eq, PartialEq, ToComputedValue, ToCss)]
 pub struct Symbols(pub Vec<Symbol>);
 
 impl Parse for Symbols {
     fn parse<'i, 't>(context: &ParserContext, input: &mut Parser<'i, 't>) -> Result<Self, ParseError<'i>> {
         let mut symbols = Vec::new();
         loop {
             if let Ok(s) = input.try(|input| Symbol::parse(context, input)) {
                 symbols.push(s)
@@ -565,32 +558,16 @@ impl Parse for Symbols {
                 } else {
                     return Ok(Symbols(symbols))
                 }
             }
         }
     }
 }
 
-impl ToCss for Symbols {
-    fn to_css<W>(&self, dest: &mut CssWriter<W>) -> fmt::Result
-    where
-        W: Write,
-    {
-        let mut iter = self.0.iter();
-        let first = iter.next().expect("expected at least one symbol");
-        first.to_css(dest)?;
-        for item in iter {
-            dest.write_char(' ')?;
-            item.to_css(dest)?;
-        }
-        Ok(())
-    }
-}
-
 /// <https://drafts.csswg.org/css-counter-styles/#descdef-counter-style-additive-symbols>
 #[derive(Clone, Debug, ToCss)]
 pub struct AdditiveSymbols(pub Vec<AdditiveTuple>);
 
 impl Parse for AdditiveSymbols {
     fn parse<'i, 't>(context: &ParserContext, input: &mut Parser<'i, 't>) -> Result<Self, ParseError<'i>> {
         let tuples = Vec::<AdditiveTuple>::parse(context, input)?;
         // FIXME maybe? https://github.com/w3c/csswg-drafts/issues/1220
diff --git a/servo/components/style/gecko/rules.rs b/servo/components/style/gecko/rules.rs
--- a/servo/components/style/gecko/rules.rs
+++ b/servo/components/style/gecko/rules.rs
@@ -308,17 +308,17 @@ impl ToNsCssValue for counter_style::Neg
         }
     }
 }
 
 impl ToNsCssValue for counter_style::Symbol {
     fn convert(self, nscssvalue: &mut nsCSSValue) {
         match self {
             counter_style::Symbol::String(s) => nscssvalue.set_string(&s),
-            counter_style::Symbol::Ident(s) => nscssvalue.set_ident(&s),
+            counter_style::Symbol::Ident(s) => nscssvalue.set_ident_from_atom(&s.0),
         }
     }
 }
 
 impl ToNsCssValue for counter_style::Ranges {
     fn convert(self, nscssvalue: &mut nsCSSValue) {
         if self.0.is_empty() {
             nscssvalue.set_auto();
diff --git a/servo/components/style/properties/gecko.mako.rs b/servo/components/style/properties/gecko.mako.rs
--- a/servo/components/style/properties/gecko.mako.rs
+++ b/servo/components/style/properties/gecko.mako.rs
@@ -4787,26 +4787,25 @@ fn static_assert() {
             self.gecko.mTextEmphasisStyle = structs::NS_STYLE_TEXT_EMPHASIS_STYLE_NONE as u8;
         }
     }
 
     ${impl_simple_type_with_conversion("text_emphasis_position")}
 
     pub fn set_text_emphasis_style(&mut self, v: longhands::text_emphasis_style::computed_value::T) {
         use properties::longhands::text_emphasis_style::computed_value::T;
-        use properties::longhands::text_emphasis_style::ShapeKeyword;
+        use properties::longhands::text_emphasis_style::{FillMode, ShapeKeyword};
 
         self.clear_text_emphasis_style_if_string();
         let (te, s) = match v {
             T::None => (structs::NS_STYLE_TEXT_EMPHASIS_STYLE_NONE, ""),
             T::Keyword(ref keyword) => {
-                let fill = if keyword.fill {
-                    structs::NS_STYLE_TEXT_EMPHASIS_STYLE_FILLED
-                } else {
-                    structs::NS_STYLE_TEXT_EMPHASIS_STYLE_OPEN
+                let fill = match keyword.fill {
+                    FillMode::Filled => structs::NS_STYLE_TEXT_EMPHASIS_STYLE_FILLED,
+                    FillMode::Open => structs::NS_STYLE_TEXT_EMPHASIS_STYLE_OPEN,
                 };
                 let shape = match keyword.shape {
                     ShapeKeyword::Dot => structs::NS_STYLE_TEXT_EMPHASIS_STYLE_DOT,
                     ShapeKeyword::Circle => structs::NS_STYLE_TEXT_EMPHASIS_STYLE_CIRCLE,
                     ShapeKeyword::DoubleCircle => structs::NS_STYLE_TEXT_EMPHASIS_STYLE_DOUBLE_CIRCLE,
                     ShapeKeyword::Triangle => structs::NS_STYLE_TEXT_EMPHASIS_STYLE_TRIANGLE,
                     ShapeKeyword::Sesame => structs::NS_STYLE_TEXT_EMPHASIS_STYLE_SESAME,
                 };
@@ -4831,25 +4830,29 @@ fn static_assert() {
     }
 
     pub fn reset_text_emphasis_style(&mut self, other: &Self) {
         self.copy_text_emphasis_style_from(other)
     }
 
     pub fn clone_text_emphasis_style(&self) -> longhands::text_emphasis_style::computed_value::T {
         use properties::longhands::text_emphasis_style::computed_value::{T, KeywordValue};
-        use properties::longhands::text_emphasis_style::ShapeKeyword;
+        use properties::longhands::text_emphasis_style::{FillMode, ShapeKeyword};
 
         if self.gecko.mTextEmphasisStyle == structs::NS_STYLE_TEXT_EMPHASIS_STYLE_NONE as u8 {
             return T::None;
         } else if self.gecko.mTextEmphasisStyle == structs::NS_STYLE_TEXT_EMPHASIS_STYLE_STRING as u8 {
             return T::String(self.gecko.mTextEmphasisStyleString.to_string());
         }
 
-        let fill = self.gecko.mTextEmphasisStyle & structs::NS_STYLE_TEXT_EMPHASIS_STYLE_OPEN as u8 == 0;
+        let fill = match self.gecko.mTextEmphasisStyle as u32 {
+            structs::NS_STYLE_TEXT_EMPHASIS_STYLE_FILLED => FillMode::Filled,
+            structs::NS_STYLE_TEXT_EMPHASIS_STYLE_OPEN => FillMode::Open,
+            _ => panic!("Unexpected value in style struct for text-emphasis-style property"),
+        };
         let shape =
             match self.gecko.mTextEmphasisStyle as u32 & !structs::NS_STYLE_TEXT_EMPHASIS_STYLE_OPEN {
                 structs::NS_STYLE_TEXT_EMPHASIS_STYLE_DOT => ShapeKeyword::Dot,
                 structs::NS_STYLE_TEXT_EMPHASIS_STYLE_CIRCLE => ShapeKeyword::Circle,
                 structs::NS_STYLE_TEXT_EMPHASIS_STYLE_DOUBLE_CIRCLE => ShapeKeyword::DoubleCircle,
                 structs::NS_STYLE_TEXT_EMPHASIS_STYLE_TRIANGLE => ShapeKeyword::Triangle,
                 structs::NS_STYLE_TEXT_EMPHASIS_STYLE_SESAME => ShapeKeyword::Sesame,
                 _ => panic!("Unexpected value in style struct for text-emphasis-style property")
diff --git a/servo/components/style/properties/helpers/animated_properties.mako.rs b/servo/components/style/properties/helpers/animated_properties.mako.rs
--- a/servo/components/style/properties/helpers/animated_properties.mako.rs
+++ b/servo/components/style/properties/helpers/animated_properties.mako.rs
@@ -21,20 +21,19 @@ use properties::longhands;
 use properties::longhands::font_weight::computed_value::T as FontWeight;
 use properties::longhands::font_stretch::computed_value::T as FontStretch;
 use properties::longhands::visibility::computed_value::T as Visibility;
 use properties::PropertyId;
 use properties::{LonghandId, ShorthandId};
 use servo_arc::Arc;
 use smallvec::SmallVec;
 use std::{cmp, ptr};
-use std::fmt::{self, Write};
 use std::mem::{self, ManuallyDrop};
 #[cfg(feature = "gecko")] use hash::FnvHashMap;
-use style_traits::{CssWriter, ParseError, ToCss};
+use style_traits::ParseError;
 use super::ComputedValues;
 use values::{CSSFloat, CustomIdent, Either};
 use values::animated::{Animate, Procedure, ToAnimatedValue, ToAnimatedZero};
 use values::animated::color::RGBA as AnimatedRGBA;
 use values::animated::effects::Filter as AnimatedFilter;
 use values::animated::effects::FilterList as AnimatedFilterList;
 use values::computed::{Angle, CalcLengthOrPercentage};
 use values::computed::{ClipRect, Context, ComputedUrl};
@@ -74,40 +73,27 @@ pub fn nscsspropertyid_is_animatable(pro
         _ => false
     }
 }
 
 /// A given transition property, that is either `All`, a transitionable longhand property,
 /// a shorthand with at least one transitionable longhand component, or an unsupported property.
 // NB: This needs to be here because it needs all the longhands generated
 // beforehand.
-#[derive(Clone, Debug, Eq, Hash, MallocSizeOf, PartialEq)]
+#[derive(Clone, Debug, Eq, Hash, MallocSizeOf, PartialEq, ToCss)]
 pub enum TransitionProperty {
     /// A shorthand.
     Shorthand(ShorthandId),
     /// A longhand transitionable property.
     Longhand(LonghandId),
     /// Unrecognized property which could be any non-transitionable, custom property, or
     /// unknown property.
     Unsupported(CustomIdent),
 }
 
-impl ToCss for TransitionProperty {
-    fn to_css<W>(&self, dest: &mut CssWriter<W>) -> fmt::Result
-    where
-        W: Write,
-    {
-        match *self {
-            TransitionProperty::Shorthand(ref id) => dest.write_str(id.name()),
-            TransitionProperty::Longhand(ref id) => dest.write_str(id.name()),
-            TransitionProperty::Unsupported(ref id) => id.to_css(dest),
-        }
-    }
-}
-
 trivial_to_computed_value!(TransitionProperty);
 
 impl TransitionProperty {
     /// Returns `all`.
     #[inline]
     pub fn all() -> Self {
         TransitionProperty::Shorthand(ShorthandId::All)
     }
diff --git a/servo/components/style/properties/longhand/inherited_text.mako.rs b/servo/components/style/properties/longhand/inherited_text.mako.rs
--- a/servo/components/style/properties/longhand/inherited_text.mako.rs
+++ b/servo/components/style/properties/longhand/inherited_text.mako.rs
@@ -194,113 +194,84 @@
     flags="APPLIES_TO_FIRST_LETTER APPLIES_TO_FIRST_LINE APPLIES_TO_PLACEHOLDER",
     spec="https://drafts.csswg.org/css-text-decor-3/#text-shadow-property",
 )}
 
 <%helpers:longhand name="text-emphasis-style" products="gecko" boxed="True"
                    animation_value_type="discrete"
                    spec="https://drafts.csswg.org/css-text-decor/#propdef-text-emphasis-style">
     use computed_values::writing_mode::T as WritingMode;
-    use std::fmt::{self, Write};
-    use style_traits::{CssWriter, ToCss};
     use unicode_segmentation::UnicodeSegmentation;
 
-
     pub mod computed_value {
         #[derive(Clone, Debug, MallocSizeOf, PartialEq, ToCss)]
         #[cfg_attr(feature = "servo", derive(ToComputedValue))]
         pub enum T {
             Keyword(KeywordValue),
             None,
             String(String),
         }
 
-        #[derive(Clone, Debug, MallocSizeOf, PartialEq)]
+        #[derive(Clone, Debug, MallocSizeOf, PartialEq, ToCss)]
         pub struct KeywordValue {
-            pub fill: bool,
+            pub fill: super::FillMode,
             pub shape: super::ShapeKeyword,
         }
     }
 
     #[derive(Clone, Debug, MallocSizeOf, PartialEq, ToCss)]
     pub enum SpecifiedValue {
         Keyword(KeywordValue),
         None,
         String(String),
     }
 
-    #[derive(Clone, Debug, MallocSizeOf, PartialEq)]
+    #[derive(Clone, Debug, MallocSizeOf, PartialEq, ToCss)]
     pub enum KeywordValue {
-        Fill(bool),
+        Fill(FillMode),
         Shape(ShapeKeyword),
-        FillAndShape(bool, ShapeKeyword),
-    }
-
-    impl ToCss for KeywordValue {
-        fn to_css<W>(&self, dest: &mut CssWriter<W>) -> fmt::Result where W: fmt::Write {
-            if let Some(fill) = self.fill() {
-                if fill {
-                    dest.write_str("filled")?;
-                } else {
-                    dest.write_str("open")?;
-                }
-            }
-            if let Some(shape) = self.shape() {
-                if self.fill().is_some() {
-                    dest.write_str(" ")?;
-                }
-                shape.to_css(dest)?;
-            }
-            Ok(())
-        }
-    }
-
-    impl ToCss for computed_value::KeywordValue {
-        fn to_css<W>(&self, dest: &mut CssWriter<W>) -> fmt::Result
-        where
-            W: Write,
-        {
-            if self.fill {
-                dest.write_str("filled")?;
-            } else {
-                dest.write_str("open")?;
-            }
-            dest.write_str(" ")?;
-            self.shape.to_css(dest)
-        }
+        FillAndShape(FillMode, ShapeKeyword),
     }
 
     impl KeywordValue {
-        fn fill(&self) -> Option<bool> {
+        fn fill(&self) -> Option<FillMode> {
             match *self {
                 KeywordValue::Fill(fill) |
-                KeywordValue::FillAndShape(fill,_) => Some(fill),
+                KeywordValue::FillAndShape(fill, _) => Some(fill),
                 _ => None,
             }
         }
+
         fn shape(&self) -> Option<ShapeKeyword> {
             match *self {
                 KeywordValue::Shape(shape) |
                 KeywordValue::FillAndShape(_, shape) => Some(shape),
                 _ => None,
             }
         }
     }
 
+    #[derive(Clone, Copy, Debug, MallocSizeOf, Parse, PartialEq, ToCss)]
+    pub enum FillMode {
+        Filled,
+        Open,
+    }
+
     #[derive(Clone, Copy, Debug, Eq, MallocSizeOf, Parse, PartialEq, ToCss)]
     pub enum ShapeKeyword {
         Dot,
         Circle,
         DoubleCircle,
         Triangle,
         Sesame,
     }
 
     impl ShapeKeyword {
-        pub fn char(&self, fill: bool) -> &str {
+        pub fn char(&self, fill: FillMode) -> &str {
+            let fill = fill == FillMode::Filled;
             match *self {
                 ShapeKeyword::Dot => if fill { "\u{2022}" } else { "\u{25e6}" },
                 ShapeKeyword::Circle =>  if fill { "\u{25cf}" } else { "\u{25cb}" },
                 ShapeKeyword::DoubleCircle =>  if fill { "\u{25c9}" } else { "\u{25ce}" },
                 ShapeKeyword::Triangle =>  if fill { "\u{25b2}" } else { "\u{25b3}" },
                 ShapeKeyword::Sesame =>  if fill { "\u{fe45}" } else { "\u{fe46}" },
             }
         }
@@ -325,17 +296,17 @@
                 SpecifiedValue::Keyword(ref keyword) => {
                     let default_shape = if context.style().get_inheritedbox()
                                                   .clone_writing_mode() == WritingMode::HorizontalTb {
                         ShapeKeyword::Circle
                     } else {
                         ShapeKeyword::Sesame
                     };
                     computed_value::T::Keyword(computed_value::KeywordValue {
-                        fill: keyword.fill().unwrap_or(true),
+                        fill: keyword.fill().unwrap_or(FillMode::Filled),
                         shape: keyword.shape().unwrap_or(default_shape),
                     })
                 },
                 SpecifiedValue::None => computed_value::T::None,
                 SpecifiedValue::String(ref s) => {
                     // Passing `true` to iterate over extended grapheme clusters, following
                     // recommendation at http://www.unicode.org/reports/tr29/#Grapheme_Cluster_Boundaries
                     let string = s.graphemes(true).next().unwrap_or("").to_string();
@@ -349,43 +320,41 @@
                 computed_value::T::Keyword(ref keyword) =>
                     SpecifiedValue::Keyword(KeywordValue::FillAndShape(keyword.fill,keyword.shape)),
                 computed_value::T::None => SpecifiedValue::None,
                 computed_value::T::String(ref string) => SpecifiedValue::String(string.clone())
             }
         }
     }
 
-    pub fn parse<'i, 't>(_context: &ParserContext, input: &mut Parser<'i, 't>)
-                         -> Result<SpecifiedValue, ParseError<'i>> {
+    pub fn parse<'i, 't>(
+        _context: &ParserContext,
+        input: &mut Parser<'i, 't>,
+    ) -> Result<SpecifiedValue, ParseError<'i>> {
         if input.try(|input| input.expect_ident_matching("none")).is_ok() {
             return Ok(SpecifiedValue::None);
         }
 
         if let Ok(s) = input.try(|i| i.expect_string().map(|s| s.as_ref().to_owned())) {
             // Handle <string>
             return Ok(SpecifiedValue::String(s));
         }
 
         // Handle a pair of keywords
-        let mut shape = input.try(ShapeKeyword::parse);
-        let fill = if input.try(|input| input.expect_ident_matching("filled")).is_ok() {
-            Some(true)
-        } else if input.try(|input| input.expect_ident_matching("open")).is_ok() {
-            Some(false)
-        } else { None };
-        if shape.is_err() {
-            shape = input.try(ShapeKeyword::parse);
+        let mut shape = input.try(ShapeKeyword::parse).ok();
+        let fill = input.try(FillMode::parse).ok();
+        if shape.is_none() {
+            shape = input.try(ShapeKeyword::parse).ok();
         }
 
         // At least one of shape or fill must be handled
         let keyword_value = match (fill, shape) {
-            (Some(fill), Ok(shape)) => KeywordValue::FillAndShape(fill,shape),
-            (Some(fill), Err(_)) => KeywordValue::Fill(fill),
-            (None, Ok(shape)) => KeywordValue::Shape(shape),
+            (Some(fill), Some(shape)) => KeywordValue::FillAndShape(fill, shape),
+            (Some(fill), None) => KeywordValue::Fill(fill),
+            (None, Some(shape)) => KeywordValue::Shape(shape),
             _ => return Err(input.new_custom_error(StyleParseErrorKind::UnspecifiedError)),
         };
         Ok(SpecifiedValue::Keyword(keyword_value))
     }
 </%helpers:longhand>
 
 <%helpers:longhand name="text-emphasis-position" animation_value_type="discrete" products="gecko"
                    spec="https://drafts.csswg.org/css-text-decor/#propdef-text-emphasis-position">
diff --git a/servo/components/style/properties/properties.mako.rs b/servo/components/style/properties/properties.mako.rs
--- a/servo/components/style/properties/properties.mako.rs
+++ b/servo/components/style/properties/properties.mako.rs
@@ -795,16 +795,26 @@ bitflags! {
 #[repr(u16)]
 pub enum LonghandId {
     % for i, property in enumerate(data.longhands):
         /// ${property.name}
         ${property.camel_case} = ${i},
     % endfor
 }
 
+impl ToCss for LonghandId {
+    #[inline]
+    fn to_css<W>(&self, dest: &mut CssWriter<W>) -> fmt::Result
+    where
+        W: Write,
+    {
+        dest.write_str(self.name())
+    }
+}
+
 impl fmt::Debug for LonghandId {
     fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
         formatter.write_str(self.name())
     }
 }
 
 impl LonghandId {
     /// Get the name of this longhand property.
@@ -1129,24 +1139,34 @@ where
             if !self.filter || id.into().enabled_for_all_content() {
                 return Some(id)
             }
         }
     }
 }
 
 /// An identifier for a given shorthand property.
-#[derive(Clone, Copy, Debug, Eq, Hash, MallocSizeOf, PartialEq, ToCss)]
+#[derive(Clone, Copy, Debug, Eq, Hash, MallocSizeOf, PartialEq)]
 pub enum ShorthandId {
     % for property in data.shorthands:
         /// ${property.name}
         ${property.camel_case},
     % endfor
 }
 
+impl ToCss for ShorthandId {
+    #[inline]
+    fn to_css<W>(&self, dest: &mut CssWriter<W>) -> fmt::Result
+    where
+        W: Write,
+    {
+        dest.write_str(self.name())
+    }
+}
+
 impl ShorthandId {
     /// Get the name for this shorthand property.
     pub fn name(&self) -> &'static str {
         match *self {
             % for property in data.shorthands:
                 ShorthandId::${property.camel_case} => "${property.name}",
             % endfor
         }
diff --git a/servo/components/style/stylesheets/document_rule.rs b/servo/components/style/stylesheets/document_rule.rs
--- a/servo/components/style/stylesheets/document_rule.rs
+++ b/servo/components/style/stylesheets/document_rule.rs
@@ -66,38 +66,41 @@ impl DeepCloneWithLock for DocumentRule 
             condition: self.condition.clone(),
             rules: Arc::new(lock.wrap(rules.deep_clone_with_lock(lock, guard, params))),
             source_location: self.source_location.clone(),
         }
     }
 }
 
 /// A URL matching function for a `@document` rule's condition.
-#[derive(Clone, Debug)]
+#[derive(Clone, Debug, ToCss)]
 pub enum UrlMatchingFunction {
     /// Exact URL matching function. It evaluates to true whenever the
     /// URL of the document being styled is exactly the URL given.
     Url(SpecifiedUrl),
     /// URL prefix matching function. It evaluates to true whenever the
     /// URL of the document being styled has the argument to the
     /// function as an initial substring (which is true when the two
     /// strings are equal). When the argument is the empty string,
     /// it evaluates to true for all documents.
+    #[css(function)]
     UrlPrefix(String),
     /// Domain matching function. It evaluates to true whenever the URL
     /// of the document being styled has a host subcomponent and that
     /// host subcomponent is exactly the argument to the ‘domain()’
     /// function or a final substring of the host component is a
     /// period (U+002E) immediately followed by the argument to the
     /// ‘domain()’ function.
+    #[css(function)]
     Domain(String),
     /// Regular expression matching function. It evaluates to true
     /// whenever the regular expression matches the entirety of the URL
     /// of the document being styled.
-    RegExp(String),
+    #[css(function)]
+    Regexp(String),
 }
 
 macro_rules! parse_quoted_or_unquoted_string {
     ($input:ident, $url_matching_function:expr) => {
         $input.parse_nested_block(|input| {
             let start = input.position();
             input.parse_entirely(|input| {
                 let location = input.current_source_location();
@@ -120,17 +123,17 @@ impl UrlMatchingFunction {
     pub fn parse<'i, 't>(context: &ParserContext, input: &mut Parser<'i, 't>)
         -> Result<UrlMatchingFunction, ParseError<'i>> {
         if input.try(|input| input.expect_function_matching("url-prefix")).is_ok() {
             parse_quoted_or_unquoted_string!(input, UrlMatchingFunction::UrlPrefix)
         } else if input.try(|input| input.expect_function_matching("domain")).is_ok() {
             parse_quoted_or_unquoted_string!(input, UrlMatchingFunction::Domain)
         } else if input.try(|input| input.expect_function_matching("regexp")).is_ok() {
             input.parse_nested_block(|input| {
-                Ok(UrlMatchingFunction::RegExp(input.expect_string()?.as_ref().to_owned()))
+                Ok(UrlMatchingFunction::Regexp(input.expect_string()?.as_ref().to_owned()))
             })
         } else if let Ok(url) = input.try(|input| SpecifiedUrl::parse(context, input)) {
             Ok(UrlMatchingFunction::Url(url))
         } else {
             Err(input.new_custom_error(StyleParseErrorKind::UnspecifiedError))
         }
     }
 
@@ -140,73 +143,46 @@ impl UrlMatchingFunction {
         use gecko_bindings::bindings::Gecko_DocumentRule_UseForPresentation;
         use gecko_bindings::structs::URLMatchingFunction as GeckoUrlMatchingFunction;
         use nsstring::nsCStr;
 
         let func = match *self {
             UrlMatchingFunction::Url(_) => GeckoUrlMatchingFunction::eURL,
             UrlMatchingFunction::UrlPrefix(_) => GeckoUrlMatchingFunction::eURLPrefix,
             UrlMatchingFunction::Domain(_) => GeckoUrlMatchingFunction::eDomain,
-            UrlMatchingFunction::RegExp(_) => GeckoUrlMatchingFunction::eRegExp,
+            UrlMatchingFunction::Regexp(_) => GeckoUrlMatchingFunction::eRegExp,
         };
 
         let pattern = nsCStr::from(match *self {
             UrlMatchingFunction::Url(ref url) => url.as_str(),
             UrlMatchingFunction::UrlPrefix(ref pat) |
             UrlMatchingFunction::Domain(ref pat) |
-            UrlMatchingFunction::RegExp(ref pat) => pat,
+            UrlMatchingFunction::Regexp(ref pat) => pat,
         });
         unsafe {
             Gecko_DocumentRule_UseForPresentation(device.pres_context(), &*pattern, func)
         }
     }
 
     #[cfg(not(feature = "gecko"))]
     /// Evaluate a URL matching function.
     pub fn evaluate(&self, _: &Device) -> bool {
         false
     }
 }
 
-impl ToCss for UrlMatchingFunction {
-    fn to_css<W>(&self, dest: &mut CssWriter<W>) -> fmt::Result
-    where
-        W: Write,
-    {
-        match *self {
-            UrlMatchingFunction::Url(ref url) => {
-                url.to_css(dest)
-            },
-            UrlMatchingFunction::UrlPrefix(ref url_prefix) => {
-                dest.write_str("url-prefix(")?;
-                url_prefix.to_css(dest)?;
-                dest.write_str(")")
-            },
-            UrlMatchingFunction::Domain(ref domain) => {
-                dest.write_str("domain(")?;
-                domain.to_css(dest)?;
-                dest.write_str(")")
-            },
-            UrlMatchingFunction::RegExp(ref regex) => {
-                dest.write_str("regexp(")?;
-                regex.to_css(dest)?;
-                dest.write_str(")")
-            },
-        }
-    }
-}
-
 /// A `@document` rule's condition.
 ///
 /// <https://www.w3.org/TR/2012/WD-css3-conditional-20120911/#at-document>
 ///
 /// The `@document` rule's condition is written as a comma-separated list of
 /// URL matching functions, and the condition evaluates to true whenever any
 /// one of those functions evaluates to true.
-#[derive(Clone, Debug)]
+#[css(comma, iterable)]
+#[derive(Clone, Debug, ToCss)]
 pub struct DocumentCondition(Vec<UrlMatchingFunction>);
 
 impl DocumentCondition {
     /// Parse a document condition.
     pub fn parse<'i, 't>(context: &ParserContext, input: &mut Parser<'i, 't>)
         -> Result<Self, ParseError<'i>> {
         input.parse_comma_separated(|input| UrlMatchingFunction::parse(context, input))
              .map(DocumentCondition)
@@ -214,25 +190,8 @@ impl DocumentCondition {
 
     /// Evaluate a document condition.
     pub fn evaluate(&self, device: &Device) -> bool {
         self.0.iter().any(|ref url_matching_function|
             url_matching_function.evaluate(device)
         )
     }
 }
-
-impl ToCss for DocumentCondition {
-    fn to_css<W>(&self, dest: &mut CssWriter<W>) -> fmt::Result
-    where
-        W: Write,
-    {
-        let mut iter = self.0.iter();
-        let first = iter.next()
-            .expect("Empty DocumentCondition, should contain at least one URL matching function");
-        first.to_css(dest)?;
-        for url_matching_function in iter {
-            dest.write_str(", ")?;
-            url_matching_function.to_css(dest)?;
-        }
-        Ok(())
-    }
-}
diff --git a/servo/components/style/stylesheets/font_feature_values_rule.rs b/servo/components/style/stylesheets/font_feature_values_rule.rs
--- a/servo/components/style/stylesheets/font_feature_values_rule.rs
+++ b/servo/components/style/stylesheets/font_feature_values_rule.rs
@@ -75,17 +75,17 @@ impl Parse for SingleValue {
 impl ToGeckoFontFeatureValues for SingleValue {
     fn to_gecko_font_feature_values(&self, array: &mut nsTArray<u32>) {
         unsafe { array.set_len_pod(1); }
         array[0] = self.0 as u32;
     }
 }
 
 /// A @font-feature-values block declaration value that keeps one or two values.
-#[derive(Clone, Debug, PartialEq)]
+#[derive(Clone, Debug, PartialEq, ToCss)]
 pub struct PairValues(pub u32, pub Option<u32>);
 
 impl Parse for PairValues {
     fn parse<'i, 't>(_context: &ParserContext, input: &mut Parser<'i, 't>)
                      -> Result<PairValues, ParseError<'i>> {
         let location = input.current_source_location();
         let first = match *input.next()? {
             Token::Number { int_value: Some(a), .. } if a >= 0 => a as u32,
@@ -99,45 +99,32 @@ impl Parse for PairValues {
             // It can't be anything other than number.
             Ok(t) => Err(location.new_unexpected_token_error(t.clone())),
             // It can be just one value.
             Err(_) => Ok(PairValues(first, None))
         }
     }
 }
 
-impl ToCss for PairValues {
-    fn to_css<W>(&self, dest: &mut CssWriter<W>) -> fmt::Result
-    where
-        W: Write,
-    {
-        self.0.to_css(dest)?;
-        if let Some(second) = self.1 {
-            dest.write_char(' ')?;
-            second.to_css(dest)?;
-        }
-        Ok(())
-    }
-}
-
 #[cfg(feature = "gecko")]
 impl ToGeckoFontFeatureValues for PairValues {
     fn to_gecko_font_feature_values(&self, array: &mut nsTArray<u32>) {
         let len = if self.1.is_some() { 2 } else { 1 };
 
         unsafe { array.set_len_pod(len); }
         array[0] = self.0 as u32;
         if let Some(second) = self.1 {
             array[1] = second as u32;
         };
     }
 }
 
 /// A @font-feature-values block declaration value that keeps a list of values.
-#[derive(Clone, Debug, PartialEq)]
+#[css(iterable)]
+#[derive(Clone, Debug, PartialEq, ToCss)]
 pub struct VectorValues(pub Vec<u32>);
 
 impl Parse for VectorValues {
     fn parse<'i, 't>(_context: &ParserContext, input: &mut Parser<'i, 't>)
                      -> Result<VectorValues, ParseError<'i>> {
         let mut vec = vec![];
         loop {
             let location = input.current_source_location();
@@ -154,34 +141,16 @@ impl Parse for VectorValues {
         if vec.len() == 0 {
             return Err(input.new_error(BasicParseErrorKind::EndOfInput));
         }
 
         Ok(VectorValues(vec))
     }
 }
 
-impl ToCss for VectorValues {
-    fn to_css<W>(&self, dest: &mut CssWriter<W>) -> fmt::Result
-    where
-        W: Write,
-    {
-        let mut iter = self.0.iter();
-        let first = iter.next();
-        if let Some(first) = first {
-            first.to_css(dest)?;
-            for value in iter {
-                dest.write_char(' ')?;
-                value.to_css(dest)?;
-            }
-        }
-        Ok(())
-    }
-}
-
 #[cfg(feature = "gecko")]
 impl ToGeckoFontFeatureValues for VectorValues {
     fn to_gecko_font_feature_values(&self, array: &mut nsTArray<u32>) {
         unsafe { array.set_len_pod(self.0.len() as u32); }
         for (dest, value) in array.iter_mut().zip(self.0.iter()) {
             *dest = *value;
         }
     }
diff --git a/servo/components/style/stylesheets/keyframes_rule.rs b/servo/components/style/stylesheets/keyframes_rule.rs
--- a/servo/components/style/stylesheets/keyframes_rule.rs
+++ b/servo/components/style/stylesheets/keyframes_rule.rs
@@ -140,34 +140,20 @@ impl KeyframePercentage {
         };
 
         Ok(percentage)
     }
 }
 
 /// A keyframes selector is a list of percentages or from/to symbols, which are
 /// converted at parse time to percentages.
-#[derive(Clone, Debug, Eq, PartialEq)]
+#[css(comma, iterable)]
+#[derive(Clone, Debug, Eq, PartialEq, ToCss)]
 pub struct KeyframeSelector(Vec<KeyframePercentage>);
 
-impl ToCss for KeyframeSelector {
-    fn to_css<W>(&self, dest: &mut CssWriter<W>) -> fmt::Result
-    where
-        W: Write,
-    {
-        let mut iter = self.0.iter();
-        iter.next().unwrap().to_css(dest)?;
-        for percentage in iter {
-            dest.write_str(", ")?;
-            percentage.to_css(dest)?;
-        }
-        Ok(())
-    }
-}
-
 impl KeyframeSelector {
     /// Return the list of percentages this selector contains.
     #[inline]
     pub fn percentages(&self) -> &[KeyframePercentage] {
         &self.0
     }
 
     /// A dummy public function so we can write a unit test for this.
diff --git a/servo/components/style/stylesheets/viewport_rule.rs b/servo/components/style/stylesheets/viewport_rule.rs
--- a/servo/components/style/stylesheets/viewport_rule.rs
+++ b/servo/components/style/stylesheets/viewport_rule.rs
@@ -138,36 +138,24 @@ declare_viewport_descriptor! {
 trait FromMeta: Sized {
     fn from_meta(value: &str) -> Option<Self>;
 }
 
 /// ViewportLength is a length | percentage | auto | extend-to-zoom
 /// See:
 /// * http://dev.w3.org/csswg/css-device-adapt/#min-max-width-desc
 /// * http://dev.w3.org/csswg/css-device-adapt/#extend-to-zoom
-#[derive(Clone, Debug, PartialEq)]
+#[allow(missing_docs)]
 #[cfg_attr(feature = "servo", derive(MallocSizeOf))]
-#[allow(missing_docs)]
+#[derive(Clone, Debug, PartialEq, ToCss)]
 pub enum ViewportLength {
     Specified(LengthOrPercentageOrAuto),
     ExtendToZoom
 }
 
-impl ToCss for ViewportLength {
-    fn to_css<W>(&self, dest: &mut CssWriter<W>) -> fmt::Result
-    where
-        W: Write,
-    {
-        match *self {
-            ViewportLength::Specified(ref length) => length.to_css(dest),
-            ViewportLength::ExtendToZoom => dest.write_str("extend-to-zoom"),
-        }
-    }
-}
-
 impl FromMeta for ViewportLength {
     fn from_meta(value: &str) -> Option<ViewportLength> {
         macro_rules! specified {
             ($value:expr) => {
                 ViewportLength::Specified(LengthOrPercentageOrAuto::Length($value))
             }
         }
 
diff --git a/servo/components/style_derive/to_css.rs b/servo/components/style_derive/to_css.rs
--- a/servo/components/style_derive/to_css.rs
+++ b/servo/components/style_derive/to_css.rs
@@ -126,16 +126,18 @@ pub fn derive(input: syn::DeriveInput) -
 }
 
 #[darling(attributes(css), default)]
 #[derive(Default, FromDeriveInput)]
 struct CssInputAttrs {
     derive_debug: bool,
     function: Option<Function>,
     comma: bool,
+    // Here because structs variants are also their whole type definition.
+    iterable: bool,
 }
 
 #[darling(attributes(css), default)]
 #[derive(Default, FromVariant)]
 pub struct CssVariantAttrs {
     pub function: Option<Function>,
     pub iterable: bool,
     pub comma: bool,
