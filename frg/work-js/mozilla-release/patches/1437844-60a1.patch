# HG changeset patch
# User Michael Ratcliffe <mratcliffe@mozilla.com>
# Date 1519919036 0
# Node ID b7b3c610cbca88a282316af23e9e636bc2c98c73
# Parent  3f83ae5607ecebc2c52c30d5e9e61f0ba3a7380a
Bug 1437844 - Enable browser_console_consolejsm_output.js in new frontend r=nchevobbe

The old test was focused on log levels so I have converted this to use categories as discussed.

Of course, the test passes just fine with `verify`

MozReview-Commit-ID: 8SktBkz5ysk

diff --git a/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini b/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini
--- a/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini
+++ b/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini
@@ -166,17 +166,16 @@ support-files =
 
 [browser_console.js]
 skip-if = true # Bug 1437806
 [browser_console_addonsdk_loader_exception.js]
 skip-if = true # Bug 1437807
 [browser_console_clear_method.js]
 skip-if = true # Bug 1437843
 [browser_console_consolejsm_output.js]
-skip-if = true # Bug 1437844
 [browser_console_context_menu_entries.js]
 [browser_console_dead_objects.js]
 [browser_console_error_source_click.js]
 [browser_console_filters.js]
 [browser_console_nsiconsolemessage.js]
 [browser_console_open_or_focus.js]
 [browser_console_restore.js]
 [browser_console_webconsole_ctrlw_close_tab.js]
diff --git a/devtools/client/webconsole/new-console-output/test/mochitest/browser_console_consolejsm_output.js b/devtools/client/webconsole/new-console-output/test/mochitest/browser_console_consolejsm_output.js
--- a/devtools/client/webconsole/new-console-output/test/mochitest/browser_console_consolejsm_output.js
+++ b/devtools/client/webconsole/new-console-output/test/mochitest/browser_console_consolejsm_output.js
@@ -1,45 +1,30 @@
 /* -*- indent-tabs-mode: nil; js-indent-level: 2 -*- */
 /* vim: set ft=javascript ts=2 et sw=2 tw=80: */
 /* Any copyright is dedicated to the Public Domain.
  * http://creativecommons.org/publicdomain/zero/1.0/ */
 
-// Test that Console.jsm outputs messages to the Browser Console, bug 851231.
+/* import-globals-from head.js */
+
+// Test that Console.jsm outputs messages to the Browser Console.
 
 "use strict";
 
-function onNewMessage(aNewMessages) {
-  for (let msg of aNewMessages) {
-    // Messages that shouldn't be output contain the substring FAIL_TEST
-    if (msg.node.textContent.includes("FAIL_TEST")) {
-      ok(false, "Message shouldn't have been output: " + msg.node.textContent);
-    }
-  }
-}
-
-add_task(function* () {
+add_task(async function testCategoryLogs() {
   let consoleStorage = Cc["@mozilla.org/consoleAPI-storage;1"];
   let storage = consoleStorage.getService(Ci.nsIConsoleAPIStorage);
   storage.clearEvents();
 
   let {console} = ChromeUtils.import("resource://gre/modules/Console.jsm", {});
   console.log("bug861338-log-cached");
 
-  let hud = yield HUDService.toggleBrowserConsole();
+  let hud = await HUDService.toggleBrowserConsole();
 
-  yield waitForMessages({
-    webconsole: hud,
-    messages: [{
-      name: "cached console.log message",
-      text: "bug861338-log-cached",
-      category: CATEGORY_WEBDEV,
-      severity: SEVERITY_LOG,
-    }],
-  });
+  await checkMessageExists(hud, "bug861338-log-cached");
 
   hud.jsterm.clearOutput(true);
 
   function testTrace() {
     console.trace();
   }
 
   console.time("foobarTimer");
@@ -51,246 +36,62 @@ add_task(function* () {
   console.error("bug851231-error", foobar);
   console.debug("bug851231-debug");
   console.dir(document);
   testTrace();
   console.timeEnd("foobarTimer");
 
   info("wait for the Console.jsm messages");
 
-  let results = yield waitForMessages({
-    webconsole: hud,
-    messages: [
-      {
-        name: "console.log output",
-        text: "bug851231-log",
-        category: CATEGORY_WEBDEV,
-        severity: SEVERITY_LOG,
-      },
-      {
-        name: "console.info output",
-        text: "bug851231-info",
-        category: CATEGORY_WEBDEV,
-        severity: SEVERITY_INFO,
-      },
-      {
-        name: "console.warn output",
-        text: "bug851231-warn",
-        category: CATEGORY_WEBDEV,
-        severity: SEVERITY_WARNING,
-      },
-      {
-        name: "console.error output",
-        text: /\bbug851231-error\b.+\{\s*bug851231prop:\s"bug851231value"\s*\}/,
-        category: CATEGORY_WEBDEV,
-        severity: SEVERITY_ERROR,
-        objects: true,
-      },
-      {
-        name: "console.debug output",
-        text: "bug851231-debug",
-        category: CATEGORY_WEBDEV,
-        severity: SEVERITY_LOG,
-      },
-      {
-        name: "console.trace output",
-        consoleTrace: {
-          file: "browser_console_consolejsm_output.js",
-          fn: "testTrace",
-        },
-      },
-      {
-        name: "console.dir output",
-        consoleDir: /XULDocument\s+.+\s+chrome:\/\/.+\/browser\.xul/,
-      },
-      {
-        name: "console.time output",
-        consoleTime: "foobarTimer",
-      },
-      {
-        name: "console.timeEnd output",
-        consoleTimeEnd: "foobarTimer",
-      },
-    ],
-  });
+  await checkMessageExists(hud, "bug851231-log");
+  await checkMessageExists(hud, "bug851231-info");
+  await checkMessageExists(hud, "bug851231-warn");
+  await checkMessageExists(hud, "bug851231-error");
+  await checkMessageExists(hud, "bug851231-debug");
+  await checkMessageExists(hud, "XULDocument");
+  await checkMessageExists(hud, "console.trace()");
+  await checkMessageExists(hud, "foobarTimer");
 
-  let consoleErrorMsg = results[3];
-  ok(consoleErrorMsg, "console.error message element found");
-  let clickable = consoleErrorMsg.clickableElements[0];
-  ok(clickable, "clickable object found for console.error");
-
-  let deferred = defer();
-
-  let onFetch = (aVar) => {
-    // Skip the notification from console.dir variablesview-fetched.
-    if (aVar._variablesView != hud.jsterm._variablesView) {
-      return;
-    }
-    hud.jsterm.off("variablesview-fetched", onFetch);
-
-    deferred.resolve(aVar);
-  };
-
-  hud.jsterm.on("variablesview-fetched", onFetch);
-
-  clickable.scrollIntoView(false);
-
-  info("wait for variablesview-fetched");
-  executeSoon(() =>
-    EventUtils.synthesizeMouse(clickable, 2, 2, {}, hud.iframeWindow));
-
-  let varView = yield deferred.promise;
-  ok(varView, "object inspector opened on click");
-
-  yield findVariableViewProperties(varView, [{
-    name: "bug851231prop",
-    value: "bug851231value",
-  }], { webconsole: hud });
-
-  yield HUDService.toggleBrowserConsole();
+  hud.jsterm.clearOutput(true);
+  await HUDService.toggleBrowserConsole();
 });
 
-add_task(function* testPrefix() {
+add_task(async function testFilter() {
   let consoleStorage = Cc["@mozilla.org/consoleAPI-storage;1"];
   let storage = consoleStorage.getService(Ci.nsIConsoleAPIStorage);
   storage.clearEvents();
 
   let {ConsoleAPI} = ChromeUtils.import("resource://gre/modules/Console.jsm", {});
-  let consoleOptions = {
-    maxLogLevel: "error",
-    prefix: "Log Prefix",
-  };
-  let console2 = new ConsoleAPI(consoleOptions);
-  console2.error("Testing a prefix");
-  console2.log("FAIL_TEST: Below the maxLogLevel");
+  let console2 = new ConsoleAPI();
+  let hud = await HUDService.toggleBrowserConsole();
 
-  let hud = yield HUDService.toggleBrowserConsole();
-  hud.ui.on("new-messages", onNewMessage);
-  yield waitForMessages({
-    webconsole: hud,
-    messages: [{
-      name: "cached console.error message",
-      prefix: "Log Prefix:",
-      severity: SEVERITY_ERROR,
-      text: "Testing a prefix",
-    }],
+  // Enable the error category and disable the log category.
+  await setFilterState(hud, {
+    error: true,
+    log: false
   });
 
-  hud.jsterm.clearOutput(true);
-  hud.ui.off("new-messages", onNewMessage);
-  yield HUDService.toggleBrowserConsole();
-});
-
-add_task(function* testMaxLogLevelPrefMissing() {
-  let consoleStorage = Cc["@mozilla.org/consoleAPI-storage;1"];
-  let storage = consoleStorage.getService(Ci.nsIConsoleAPIStorage);
-  storage.clearEvents();
+  let shouldBeVisible = "Should be visible";
+  let shouldBeHidden = "Should be hidden";
 
-  let {ConsoleAPI} = ChromeUtils.import("resource://gre/modules/Console.jsm", {});
-  let consoleOptions = {
-    maxLogLevel: "error",
-    maxLogLevelPref: "testing.maxLogLevel",
-  };
-  let console = new ConsoleAPI(consoleOptions);
-
-  is(Services.prefs.getPrefType(consoleOptions.maxLogLevelPref),
-     Services.prefs.PREF_INVALID,
-     "Check log level pref is missing");
+  console2.log(shouldBeHidden);
+  console2.error(shouldBeVisible);
 
-  // Since the maxLogLevelPref doesn't exist, we should fallback to the passed
-  // maxLogLevel of "error".
-  console.warn("FAIL_TEST: Below the maxLogLevel");
-  console.error("Error should be shown");
-
-  let hud = yield HUDService.toggleBrowserConsole();
-
-  hud.ui.on("new-messages", onNewMessage);
+  await checkMessageExists(hud, shouldBeVisible);
+  // Here we can safely assert that the log message is not visible, since the
+  // error message was logged after and is visible.
+  await checkMessageHidden(hud, shouldBeHidden);
 
-  yield waitForMessages({
-    webconsole: hud,
-    messages: [{
-      name: "defaulting to error level",
-      severity: SEVERITY_ERROR,
-      text: "Error should be shown",
-    }],
-  });
-
+  await resetFilters(hud);
   hud.jsterm.clearOutput(true);
-  hud.ui.off("new-messages", onNewMessage);
-  yield HUDService.toggleBrowserConsole();
+  await HUDService.toggleBrowserConsole();
 });
 
-add_task(function* testMaxLogLevelPref() {
-  let consoleStorage = Cc["@mozilla.org/consoleAPI-storage;1"];
-  let storage = consoleStorage.getService(Ci.nsIConsoleAPIStorage);
-  storage.clearEvents();
-
-  let {ConsoleAPI} = ChromeUtils.import("resource://gre/modules/Console.jsm", {});
-  let consoleOptions = {
-    maxLogLevel: "error",
-    maxLogLevelPref: "testing.maxLogLevel",
-  };
-
-  info("Setting the pref to warn");
-  Services.prefs.setCharPref(consoleOptions.maxLogLevelPref, "Warn");
-
-  let console = new ConsoleAPI(consoleOptions);
-
-  is(console.maxLogLevel, "warn", "Check pref was read at initialization");
-
-  console.info("FAIL_TEST: info is below the maxLogLevel");
-  console.error("Error should be shown");
-  console.warn("Warn should be shown due to the initial pref value");
-
-  info("Setting the pref to info");
-  Services.prefs.setCharPref(consoleOptions.maxLogLevelPref, "INFO");
-  is(console.maxLogLevel, "info", "Check pref was lowercased");
-
-  console.info("info should be shown due to the pref change being observed");
-
-  info("Clearing the pref");
-  Services.prefs.clearUserPref(consoleOptions.maxLogLevelPref);
-
-  console.warn("FAIL_TEST: Shouldn't be shown due to defaulting to error");
-  console.error("Should be shown due to defaulting to error");
-
-  let hud = yield HUDService.toggleBrowserConsole();
-  hud.ui.on("new-messages", onNewMessage);
-
-  yield waitForMessages({
-    webconsole: hud,
-    messages: [{
-      name: "error > warn",
-      severity: SEVERITY_ERROR,
-      text: "Error should be shown",
-    },
-    {
-      name: "warn is the inital pref value",
-      severity: SEVERITY_WARNING,
-      text: "Warn should be shown due to the initial pref value",
-    },
-    {
-      name: "pref changed to info",
-      severity: SEVERITY_INFO,
-      text: "info should be shown due to the pref change being observed",
-    },
-    {
-      name: "default to intial maxLogLevel if pref is removed",
-      severity: SEVERITY_ERROR,
-      text: "Should be shown due to defaulting to error",
-    }],
-  });
-
-  hud.jsterm.clearOutput(true);
-  hud.ui.off("new-messages", onNewMessage);
-  yield HUDService.toggleBrowserConsole();
-});
-
-// Test that console.profile/profileEnd trigger the right events
-add_task(function* testProfile() {
+// Test that console.profile / profileEnd trigger the right events
+add_task(async function testProfile() {
   let consoleStorage = Cc["@mozilla.org/consoleAPI-storage;1"];
   let storage = consoleStorage.getService(Ci.nsIConsoleAPIStorage);
   let { console } = ChromeUtils.import("resource://gre/modules/Console.jsm", {});
 
   storage.clearEvents();
 
   let profilerEvents = [];
 
@@ -315,8 +116,20 @@ add_task(function* testProfile() {
 
   // Test that two profiler events were fired
   is(profilerEvents.length, 2, "Got two profiler events");
   is(profilerEvents[0].action, "profile", "First event has the right action");
   is(profilerEvents[0].name, "test", "First event has the right name");
   is(profilerEvents[1].action, "profileEnd", "Second event has the right action");
   is(profilerEvents[1].name, "test", "Second event has the right name");
 });
+
+async function checkMessageExists(hud, msg) {
+  info(`Checking "${msg}" was logged`);
+  let message = await waitFor(() => findMessage(hud, msg));
+  ok(message, `"${msg}" was logged`);
+}
+
+async function checkMessageHidden(hud, msg) {
+  info(`Checking "${msg}" was not logged`);
+  await waitFor(() => findMessage(hud, msg) == null);
+  ok(true, `"${msg}" was not logged`);
+}
