# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1532176477 -7200
#      Sat Jul 21 14:34:37 2018 +0200
# Node ID 161c8da1e1aafac35c916b43c59c9ada12aa4d6c
# Parent  e2646ddf248fbc64949ab182f0b4d9172629abab
Bug 1475559 part 5 - Pass a global object to nsScriptErrorWithStack. r=bz

The stack object might be a CCW and we want to make it impossible to get a CCW's global. Now the caller has to supply a same-compartment global object, so we no longer rely on getting the CCW's global.

diff --git a/dom/base/nsJSEnvironment.cpp b/dom/base/nsJSEnvironment.cpp
--- a/dom/base/nsJSEnvironment.cpp
+++ b/dom/base/nsJSEnvironment.cpp
@@ -215,60 +215,75 @@ ProcessNameForCollectorLog()
     "default" : "content";
 }
 
 namespace xpc {
 
 // This handles JS Exceptions (via ExceptionStackOrNull), as well as DOM and XPC
 // Exceptions.
 //
-// Note that the returned object is _not_ wrapped into the compartment of
-// exceptionValue.
-JSObject*
+// Note that the returned stackObj and stackGlobal are _not_ wrapped into the
+// compartment of exceptionValue.
+void
 FindExceptionStackForConsoleReport(nsPIDOMWindowInner* win,
-                                   JS::HandleValue exceptionValue)
+                                   JS::HandleValue exceptionValue,
+                                   JS::MutableHandleObject stackObj,
+                                   JS::MutableHandleObject stackGlobal)
 {
+  stackObj.set(nullptr);
+  stackGlobal.set(nullptr);
+
   if (!exceptionValue.isObject()) {
-    return nullptr;
+    return;
   }
 
   if (win && win->AsGlobal()->IsDying()) {
     // Pretend like we have no stack, so we don't end up keeping the global
     // alive via the stack.
-    return nullptr;
+    return;
   }
 
   JS::RootingContext* rcx = RootingCx();
   JS::RootedObject exceptionObject(rcx, &exceptionValue.toObject());
-  JSObject* stackObject = JS::ExceptionStackOrNull(exceptionObject);
-  if (stackObject) {
-    return stackObject;
+  if (JSObject* excStack = JS::ExceptionStackOrNull(exceptionObject)) {
+    // At this point we know exceptionObject is a possibly-wrapped
+    // js::ErrorObject that has excStack as stack. excStack might also be a CCW,
+    // but excStack must be same-compartment with the unwrapped ErrorObject.
+    // Return the ErrorObject's global as stackGlobal. This matches what we do
+    // in the ErrorObject's |.stack| getter and ensures stackObj and stackGlobal
+    // are same-compartment.
+    JSObject* unwrappedException = js::UncheckedUnwrap(exceptionObject);
+    stackObj.set(excStack);
+    stackGlobal.set(JS::GetNonCCWObjectGlobal(unwrappedException));
+    return;
   }
 
   // It is not a JS Exception, try DOM Exception.
   RefPtr<Exception> exception;
   UNWRAP_OBJECT(DOMException, exceptionObject, exception);
   if (!exception) {
     // Not a DOM Exception, try XPC Exception.
     UNWRAP_OBJECT(Exception, exceptionObject, exception);
     if (!exception) {
-      return nullptr;
+      return;
     }
   }
 
   nsCOMPtr<nsIStackFrame> stack = exception->GetLocation();
   if (!stack) {
-    return nullptr;
+    return;
   }
   JS::RootedValue value(rcx);
   stack->GetNativeSavedFrame(&value);
   if (value.isObject()) {
-    return &value.toObject();
+    stackObj.set(&value.toObject());
+    MOZ_ASSERT(JS::IsUnwrappedSavedFrame(stackObj));
+    stackGlobal.set(JS::GetNonCCWObjectGlobal(stackObj));
+    return;
   }
-  return nullptr;
 }
 
 } /* namespace xpc */
 
 static PRTime
 GetCollectionTimeDelta()
 {
   PRTime now = PR_Now();
@@ -461,19 +476,21 @@ public:
                                 NS_LITERAL_STRING("error"), init);
       event->SetTrusted(true);
 
       EventDispatcher::DispatchDOMEvent(win, nullptr, event, presContext,
                                         &status);
     }
 
     if (status != nsEventStatus_eConsumeNoDefault) {
-      JS::Rooted<JSObject*> stack(rootingCx,
-        xpc::FindExceptionStackForConsoleReport(win, mError));
-      mReport->LogToConsoleWithStack(stack);
+      JS::Rooted<JSObject*> stack(rootingCx);
+      JS::Rooted<JSObject*> stackGlobal(rootingCx);
+      xpc::FindExceptionStackForConsoleReport(win, mError,
+                                              &stack, &stackGlobal);
+      mReport->LogToConsoleWithStack(stack, stackGlobal);
     }
 
     return NS_OK;
   }
 
 private:
   nsCOMPtr<nsPIDOMWindowInner>  mWindow;
   RefPtr<xpc::ErrorReport>      mReport;
diff --git a/dom/bindings/nsScriptError.h b/dom/bindings/nsScriptError.h
--- a/dom/bindings/nsScriptError.h
+++ b/dom/bindings/nsScriptError.h
@@ -86,24 +86,26 @@ public:
   NS_DECL_THREADSAFE_ISUPPORTS
 
 private:
   virtual ~nsScriptError() {}
 };
 
 class nsScriptErrorWithStack : public nsScriptErrorBase {
 public:
-  explicit nsScriptErrorWithStack(JS::HandleObject);
+  nsScriptErrorWithStack(JS::HandleObject aStack, JS::HandleObject aStackGlobal);
 
   NS_DECL_CYCLE_COLLECTING_ISUPPORTS
   NS_DECL_CYCLE_COLLECTION_SCRIPT_HOLDER_CLASS(nsScriptErrorWithStack)
 
   NS_IMETHOD GetStack(JS::MutableHandleValue) override;
   NS_IMETHOD ToString(nsACString& aResult) override;
 
 private:
   virtual ~nsScriptErrorWithStack();
   // Complete stackframe where the error happened.
-  // Must be SavedFrame object.
-  JS::Heap<JSObject*>  mStack;
+  // Must be a (possibly wrapped) SavedFrame object.
+  JS::Heap<JSObject*> mStack;
+  // Global object that must be same-compartment with mStack.
+  JS::Heap<JSObject*> mStackGlobal;
 };
 
 #endif /* mozilla_dom_nsScriptError_h */
diff --git a/dom/bindings/nsScriptErrorWithStack.cpp b/dom/bindings/nsScriptErrorWithStack.cpp
--- a/dom/bindings/nsScriptErrorWithStack.cpp
+++ b/dom/bindings/nsScriptErrorWithStack.cpp
@@ -39,38 +39,46 @@ FormatStackString(JSContext* cx, JS::Han
 
 }
 
 
 NS_IMPL_CYCLE_COLLECTION_CLASS(nsScriptErrorWithStack)
 
 NS_IMPL_CYCLE_COLLECTION_UNLINK_BEGIN(nsScriptErrorWithStack)
   tmp->mStack = nullptr;
+  tmp->mStackGlobal = nullptr;
 NS_IMPL_CYCLE_COLLECTION_UNLINK_END
 
 NS_IMPL_CYCLE_COLLECTION_TRAVERSE_BEGIN(nsScriptErrorWithStack)
 NS_IMPL_CYCLE_COLLECTION_TRAVERSE_END
 
 NS_IMPL_CYCLE_COLLECTION_TRACE_BEGIN(nsScriptErrorWithStack)
   NS_IMPL_CYCLE_COLLECTION_TRACE_JS_MEMBER_CALLBACK(mStack)
+  NS_IMPL_CYCLE_COLLECTION_TRACE_JS_MEMBER_CALLBACK(mStackGlobal)
 NS_IMPL_CYCLE_COLLECTION_TRACE_END
 
 NS_IMPL_CYCLE_COLLECTING_ADDREF(nsScriptErrorWithStack)
 NS_IMPL_CYCLE_COLLECTING_RELEASE(nsScriptErrorWithStack)
 
 NS_INTERFACE_MAP_BEGIN_CYCLE_COLLECTION(nsScriptErrorWithStack)
   NS_INTERFACE_MAP_ENTRY(nsISupports)
   NS_INTERFACE_MAP_ENTRY(nsIConsoleMessage)
   NS_INTERFACE_MAP_ENTRY(nsIScriptError)
 NS_INTERFACE_MAP_END
 
-nsScriptErrorWithStack::nsScriptErrorWithStack(JS::HandleObject aStack)
+nsScriptErrorWithStack::nsScriptErrorWithStack(JS::HandleObject aStack,
+                                               JS::HandleObject aStackGlobal)
   : mStack(aStack)
+  , mStackGlobal(aStackGlobal)
 {
     MOZ_ASSERT(NS_IsMainThread(), "You can't use this class on workers.");
+
+    MOZ_ASSERT(JS_IsGlobalObject(mStackGlobal));
+    js::AssertSameCompartment(mStack, mStackGlobal);
+
     mozilla::HoldJSObjects(this);
 }
 
 nsScriptErrorWithStack::~nsScriptErrorWithStack() {
     mozilla::DropJSObjects(this);
 }
 
 NS_IMETHODIMP
@@ -89,17 +97,17 @@ nsScriptErrorWithStack::ToString(nsACStr
     NS_ENSURE_SUCCESS(rv, rv);
 
     if (!mStack) {
         aResult.Assign(message);
         return NS_OK;
     }
 
     AutoJSAPI jsapi;
-    if (!jsapi.Init(mStack)) {
+    if (!jsapi.Init(mStackGlobal)) {
         return NS_ERROR_FAILURE;
     }
 
     JSContext* cx = jsapi.cx();
     JS::RootedObject stack(cx, mStack);
     nsCString stackString = FormatStackString(cx, stack);
     nsCString combined = message + NS_LITERAL_CSTRING("\n") + stackString;
     aResult.Assign(combined);
diff --git a/dom/ipc/ContentParent.cpp b/dom/ipc/ContentParent.cpp
--- a/dom/ipc/ContentParent.cpp
+++ b/dom/ipc/ContentParent.cpp
@@ -4050,17 +4050,20 @@ ContentParent::RecvScriptErrorInternal(c
     ErrorResult rv;
     data.Read(cx, &stack, rv);
     if (rv.Failed() || !stack.isObject()) {
       rv.SuppressException();
       return IPC_OK();
     }
 
     JS::RootedObject stackObj(cx, &stack.toObject());
-    msg = new nsScriptErrorWithStack(stackObj);
+    MOZ_ASSERT(JS::IsUnwrappedSavedFrame(stackObj));
+
+    JS::RootedObject stackGlobal(cx, JS::GetNonCCWObjectGlobal(stackObj));
+    msg = new nsScriptErrorWithStack(stackObj, stackGlobal);
   } else {
     msg = new nsScriptError();
   }
 
   nsresult rv = msg->Init(aMessage, aSourceName, aSourceLine,
                           aLineNumber, aColNumber, aFlags,
                           aCategory.get(), aFromPrivateWindow);
   if (NS_FAILED(rv))
diff --git a/dom/script/ScriptSettings.cpp b/dom/script/ScriptSettings.cpp
--- a/dom/script/ScriptSettings.cpp
+++ b/dom/script/ScriptSettings.cpp
@@ -583,19 +583,20 @@ AutoJSAPI::ReportException()
         nsContentUtils::ObjectPrincipal(errorGlobal));
       xpcReport->Init(jsReport.report(), jsReport.toStringResult().c_str(),
                       isChrome,
                       inner ? inner->WindowID() : 0);
       if (inner && jsReport.report()->errorNumber != JSMSG_OUT_OF_MEMORY) {
         JS::RootingContext* rcx = JS::RootingContext::get(cx());
         DispatchScriptErrorEvent(inner, rcx, xpcReport, exn);
       } else {
-        JS::Rooted<JSObject*> stack(cx(),
-          xpc::FindExceptionStackForConsoleReport(inner, exn));
-        xpcReport->LogToConsoleWithStack(stack);
+        JS::Rooted<JSObject*> stack(cx());
+        JS::Rooted<JSObject*> stackGlobal(cx());
+        xpc::FindExceptionStackForConsoleReport(inner, exn, &stack, &stackGlobal);
+        xpcReport->LogToConsoleWithStack(stack, stackGlobal);
       }
     } else {
       // On a worker, we just use the worker error reporting mechanism and don't
       // bother with xpc::ErrorReport.  This will ensure that all the right
       // events (which are a lot more complicated than in the window case) get
       // fired.
       WorkerPrivate* worker = GetCurrentThreadWorkerPrivate();
       MOZ_ASSERT(worker);
diff --git a/js/xpconnect/src/XPCComponents.cpp b/js/xpconnect/src/XPCComponents.cpp
--- a/js/xpconnect/src/XPCComponents.cpp
+++ b/js/xpconnect/src/XPCComponents.cpp
@@ -2033,34 +2033,40 @@ nsXPCComponents_Utils::ReportError(Handl
     const uint64_t innerWindowID = win ? win->WindowID() : 0;
 
     RootedObject errorObj(cx, error.isObject() ? &error.toObject() : nullptr);
     JSErrorReport* err = errorObj ? JS_ErrorFromException(cx, errorObj) : nullptr;
 
     nsCOMPtr<nsIScriptError> scripterr;
 
     if (errorObj) {
-        JS::RootedObject stackVal(cx,
-          FindExceptionStackForConsoleReport(win, error));
+        JS::RootedObject stackVal(cx);
+        JS::RootedObject stackGlobal(cx);
+        FindExceptionStackForConsoleReport(win, error, &stackVal, &stackGlobal);
         if (stackVal) {
-            scripterr = new nsScriptErrorWithStack(stackVal);
+            scripterr = new nsScriptErrorWithStack(stackVal, stackGlobal);
         }
     }
 
     nsString fileName;
     uint32_t lineNo = 0;
 
     if (!scripterr) {
         RootedObject stackObj(cx);
+        RootedObject stackGlobal(cx);
         if (stack.isObject()) {
             if (!JS::IsMaybeWrappedSavedFrame(&stack.toObject())) {
                 return NS_ERROR_INVALID_ARG;
             }
 
+            // |stack| might be a wrapper, but it must be same-compartment with
+            // the current global.
             stackObj = &stack.toObject();
+            stackGlobal = JS::CurrentGlobalOrNull(cx);
+            js::AssertSameCompartment(stackObj, stackGlobal);
 
             if (GetSavedFrameLine(cx, stackObj, &lineNo) != SavedFrameResult::Ok) {
                 JS_ClearPendingException(cx);
             }
 
             RootedString source(cx);
             nsAutoJSString str;
             if (GetSavedFrameSource(cx, stackObj, &source) == SavedFrameResult::Ok &&
@@ -2073,22 +2079,24 @@ nsXPCComponents_Utils::ReportError(Handl
             nsCOMPtr<nsIStackFrame> frame = dom::GetCurrentJSStack();
             if (frame) {
                 frame->GetFilename(cx, fileName);
                 lineNo = frame->GetLineNumber(cx);
                 JS::Rooted<JS::Value> stack(cx);
                 nsresult rv = frame->GetNativeSavedFrame(&stack);
                 if (NS_SUCCEEDED(rv) && stack.isObject()) {
                   stackObj = &stack.toObject();
+                  MOZ_ASSERT(JS::IsUnwrappedSavedFrame(stackObj));
+                  stackGlobal = JS::GetNonCCWObjectGlobal(stackObj);
                 }
             }
         }
 
         if (stackObj) {
-            scripterr = new nsScriptErrorWithStack(stackObj);
+            scripterr = new nsScriptErrorWithStack(stackObj, stackGlobal);
         }
     }
 
     if (!scripterr) {
         scripterr = new nsScriptError();
     }
 
     if (err) {
diff --git a/js/xpconnect/src/nsXPConnect.cpp b/js/xpconnect/src/nsXPConnect.cpp
--- a/js/xpconnect/src/nsXPConnect.cpp
+++ b/js/xpconnect/src/nsXPConnect.cpp
@@ -314,21 +314,31 @@ xpc::ErrorReport::LogToStderr()
         ErrorNote& note = mNotes[i];
         note.LogToStderr();
     }
 }
 
 void
 xpc::ErrorReport::LogToConsole()
 {
-  LogToConsoleWithStack(nullptr);
+    LogToConsoleWithStack(nullptr, nullptr);
 }
+
 void
-xpc::ErrorReport::LogToConsoleWithStack(JS::HandleObject aStack)
+xpc::ErrorReport::LogToConsoleWithStack(JS::HandleObject aStack,
+                                        JS::HandleObject aStackGlobal)
 {
+    if (aStack) {
+        MOZ_ASSERT(aStackGlobal);
+        MOZ_ASSERT(JS_IsGlobalObject(aStackGlobal));
+        js::AssertSameCompartment(aStack, aStackGlobal);
+    } else {
+        MOZ_ASSERT(!aStackGlobal);
+    }
+
     LogToStderr();
 
     MOZ_LOG(gJSDiagnostics,
             JSREPORT_IS_WARNING(mFlags) ? LogLevel::Warning : LogLevel::Error,
             ("file %s, line %u\n%s", NS_LossyConvertUTF16toASCII(mFileName).get(),
              mLineNumber, NS_LossyConvertUTF16toASCII(mErrorMsg).get()));
 
     // Log to the console. We do this last so that we can simply return if
@@ -339,17 +349,17 @@ xpc::ErrorReport::LogToConsoleWithStack(
     NS_ENSURE_TRUE_VOID(consoleService);
 
     RefPtr<nsScriptErrorBase> errorObject;
     if (mWindowID && aStack) {
       // Only set stack on messages related to a document
       // As we cache messages in the console service,
       // we have to ensure not leaking them after the related
       // context is destroyed and we only track document lifecycle for now.
-      errorObject = new nsScriptErrorWithStack(aStack);
+      errorObject = new nsScriptErrorWithStack(aStack, aStackGlobal);
     } else {
       errorObject = new nsScriptError();
     }
     errorObject->SetErrorMessageName(mErrorMsgName);
 
     nsresult rv = errorObject->InitWithWindowID(mErrorMsg, mFileName, mSourceLine,
                                                 mLineNumber, mColumn, mFlags,
                                                 mCategory, mWindowID);
diff --git a/js/xpconnect/src/xpcpublic.h b/js/xpconnect/src/xpcpublic.h
--- a/js/xpconnect/src/xpcpublic.h
+++ b/js/xpconnect/src/xpcpublic.h
@@ -600,18 +600,20 @@ class ErrorReport : public ErrorBase {
     void Init(JSContext* aCx, mozilla::dom::Exception* aException,
               bool aIsChrome, uint64_t aWindowID);
 
     // Log the error report to the console.  Which console will depend on the
     // window id it was initialized with.
     void LogToConsole();
     // Log to console, using the given stack object (which should be a stack of
     // the sort that JS::CaptureCurrentStack produces).  aStack is allowed to be
-    // null.
-    void LogToConsoleWithStack(JS::HandleObject aStack);
+    // null. If aStack is non-null, aStackGlobal must be a non-null global
+    // object that's same-compartment with aStack. Note that aStack might be a
+    // CCW.
+    void LogToConsoleWithStack(JS::HandleObject aStack, JS::HandleObject aStackGlobal);
 
     // Produce an error event message string from the given JSErrorReport.  Note
     // that this may produce an empty string if aReport doesn't have a
     // message attached.
     static void ErrorReportToMessageString(JSErrorReport* aReport,
                                            nsAString& aString);
 
     // Log the error report to the stderr.
@@ -620,30 +622,37 @@ class ErrorReport : public ErrorBase {
   private:
     ~ErrorReport() {}
 };
 
 void
 DispatchScriptErrorEvent(nsPIDOMWindowInner* win, JS::RootingContext* rootingCx,
                          xpc::ErrorReport* xpcReport, JS::Handle<JS::Value> exception);
 
-// Get a stack of the sort that can be passed to
+// Get a stack (as stackObj outparam) of the sort that can be passed to
 // xpc::ErrorReport::LogToConsoleWithStack from the given exception value.  Can
-// return null if the exception value doesn't have an associated stack.  The
+// be nullptr if the exception value doesn't have an associated stack.  The
 // returned stack, if any, may also not be in the same compartment as
 // exceptionValue.
 //
 // The "win" argument passed in here should be the same as the window whose
 // WindowID() is used to initialize the xpc::ErrorReport.  This may be null, of
 // course.  If it's not null, this function may return a null stack object if
 // the window is far enough gone, because in those cases we don't want to have
 // the stack in the console message keeping the window alive.
-JSObject*
+//
+// If this function sets stackObj to a non-null value, stackGlobal is set to
+// either the JS exception object's global or the global of the SavedFrame we
+// got from a DOM or XPConnect exception. In all cases, stackGlobal is an
+// unwrapped global object and is same-compartment with stackObj.
+void
 FindExceptionStackForConsoleReport(nsPIDOMWindowInner* win,
-                                   JS::HandleValue exceptionValue);
+                                   JS::HandleValue exceptionValue,
+                                   JS::MutableHandleObject stackObj,
+                                   JS::MutableHandleObject stackGlobal);
 
 // Return a name for the realm.
 // This function makes reasonable efforts to make this name both mostly human-readable
 // and unique. However, there are no guarantees of either property.
 extern void
 GetCurrentRealmName(JSContext*, nsCString& name);
 
 void AddGCCallback(xpcGCCallback cb);
