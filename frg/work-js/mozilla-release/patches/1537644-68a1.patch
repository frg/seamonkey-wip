# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1554847039 0
# Node ID 730d38d7baf7a96e6cd9e633a01123d885a18a18
# Parent  fd68ef11f142db53d9e3fa97847ad40fa83753a1
Bug 1537644 - Avoid using link.exe during configure. r=chmanchester

Interestingly, the change makes one configure test have a different
result (localeconv ends up being found when it used not to be found),
but the result of that check is actually not used on Windows because we
set HAVE_LOCALECONV manually.

Differential Revision: https://phabricator.services.mozilla.com/D25902

diff --git a/build/autoconf/toolchain.m4 b/build/autoconf/toolchain.m4
--- a/build/autoconf/toolchain.m4
+++ b/build/autoconf/toolchain.m4
@@ -7,16 +7,39 @@ dnl or AC_HEADER_STDC, meaning they are 
 dnl them explicitly.
 dnl However, theses checks are not necessary and python configure sets
 dnl the corresponding variables already, so just skip those tests
 dnl entirely.
 define([AC_PROG_CPP],[])
 define([AC_PROG_CXXCPP],[])
 define([AC_HEADER_STDC], [])
 
+dnl AC_LANG_* set ac_link to the C/C++ compiler, which works fine with
+dnl gcc and clang, but not great with clang-cl, where the build system
+dnl currently expects to run the linker independently. So LDFLAGS are not
+dnl really adapted to be used with clang-cl, which then proceeds to
+dnl execute link.exe rather than lld-link.exe.
+dnl So when the compiler is clang-cl, we modify ac_link to use a separate
+dnl linker call.
+define([_MOZ_AC_LANG_C], defn([AC_LANG_C]))
+define([AC_LANG_C],
+[_MOZ_AC_LANG_C
+if test "$CC_TYPE" = "clang-cl"; then
+  ac_link="$ac_compile"' && ${LINKER} -OUT:conftest${ac_exeext} $LDFLAGS conftest.obj $LIBS 1>&AC_FD_CC'
+fi
+])
+
+define([_MOZ_AC_LANG_CPLUSPLUS], defn([AC_LANG_CPLUSPLUS]))
+define([AC_LANG_CPLUSPLUS],
+[_MOZ_AC_LANG_CPLUSPLUS
+if test "$CC_TYPE" = "clang-cl"; then
+  ac_link="$ac_compile"' && ${LINKER} -OUT:conftest${ac_exeext} $LDFLAGS conftest.obj $LIBS 1>&AC_FD_CC'
+fi
+])
+
 AC_DEFUN([MOZ_TOOL_VARIABLES],
 [
 GNU_CC=
 GNU_CXX=
 if test "$CC_TYPE" = "gcc"; then
     GNU_CC=1
     GNU_CXX=1
 fi

