# HG changeset patch
# User Kartikaya Gupta <kgupta@mozilla.com>
# Date 1517520532 18000
# Node ID d5aa68597329f998db17f1ee17e8236d30153eae
# Parent  19487b2572f83f62499aac2712c374c9ced7738d
Bug 1435022 - Make a couple of functions private. r=sotaro

MozReview-Commit-ID: G6qC9TbIChQ

diff --git a/gfx/layers/ipc/CompositorVsyncScheduler.cpp b/gfx/layers/ipc/CompositorVsyncScheduler.cpp
--- a/gfx/layers/ipc/CompositorVsyncScheduler.cpp
+++ b/gfx/layers/ipc/CompositorVsyncScheduler.cpp
@@ -117,17 +117,16 @@ CompositorVsyncScheduler::Destroy()
 
   CancelCurrentSetNeedsCompositeTask();
   CancelCurrentCompositeTask();
 }
 
 void
 CompositorVsyncScheduler::PostCompositeTask(TimeStamp aCompositeTimestamp)
 {
-  // can be called from the compositor or vsync thread
   MonitorAutoLock lock(mCurrentCompositeTaskMonitor);
   if (mCurrentCompositeTask == nullptr && CompositorThreadHolder::Loop()) {
     RefPtr<CancelableRunnable> task = NewCancelableRunnableMethod<TimeStamp>(
       "layers::CompositorVsyncScheduler::Composite",
       this,
       &CompositorVsyncScheduler::Composite,
       aCompositeTimestamp);
     mCurrentCompositeTask = task;
diff --git a/gfx/layers/ipc/CompositorVsyncScheduler.h b/gfx/layers/ipc/CompositorVsyncScheduler.h
--- a/gfx/layers/ipc/CompositorVsyncScheduler.h
+++ b/gfx/layers/ipc/CompositorVsyncScheduler.h
@@ -46,18 +46,16 @@ class CompositorVsyncScheduler
 
 public:
   explicit CompositorVsyncScheduler(CompositorVsyncSchedulerOwner* aVsyncSchedulerOwner,
                                     widget::CompositorWidget* aWidget);
 
   bool NotifyVsync(TimeStamp aVsyncTimestamp);
   void SetNeedsComposite();
 
-  void PostCompositeTask(TimeStamp aCompositeTimestamp);
-  void PostVRTask(TimeStamp aTimestamp);
   void Destroy();
   void ScheduleComposition();
   void CancelCurrentCompositeTask();
   bool NeedsComposite();
   void Composite(TimeStamp aVsyncTimestamp);
   void ForceComposeToTarget(gfx::DrawTarget* aTarget, const gfx::IntRect* aRect);
 
   const TimeStamp& GetLastComposeTime()
@@ -72,16 +70,25 @@ public:
   }
 #endif
 
 private:
   virtual ~CompositorVsyncScheduler();
 
   // Schedule a task to run on the compositor thread.
   void ScheduleTask(already_AddRefed<CancelableRunnable>);
+
+  // Post a task to run Composite() on the compositor thread, if there isn't
+  // such a task already queued. Can be called from any thread.
+  void PostCompositeTask(TimeStamp aCompositeTimestamp);
+
+  // Post a task to run DispatchVREvents() on the VR thread, if there isn't
+  // such a task already queued. Can be called from any thread.
+  void PostVRTask(TimeStamp aTimestamp);
+
   void NotifyCompositeTaskExecuted();
   void ObserveVsync();
   void UnobserveVsync();
   void DispatchTouchEvents(TimeStamp aVsyncTimestamp);
   void DispatchVREvents(TimeStamp aVsyncTimestamp);
   void CancelCurrentSetNeedsCompositeTask();
 
   class Observer final : public VsyncObserver
