# HG changeset patch
# User Botond Ballo <botond@mozilla.com>
# Date 1520291298 18000
# Node ID e3135a56a12503df3ab8e5b9ee1aaad002a1da2e
# Parent  6a55fb1c8cc64c13cac6ce7ecce447423be40a4a
Bug 1442767 - Rename Box to RectAbsolute (Gecko changes). r=kats

MozReview-Commit-ID: 1wrzwQw8bdx

diff --git a/gfx/layers/LayerAttributes.h b/gfx/layers/LayerAttributes.h
--- a/gfx/layers/LayerAttributes.h
+++ b/gfx/layers/LayerAttributes.h
@@ -187,18 +187,18 @@ public:
     if (!mFixedPositionData) {
       mFixedPositionData.emplace();
     }
     mFixedPositionData->mScrollId = aScrollId;
     mFixedPositionData->mAnchor = aAnchor;
     mFixedPositionData->mSides = aSides;
     return true;
   }
-  bool SetStickyPositionData(FrameMetrics::ViewID aScrollId, LayerBox aOuter,
-                             LayerBox aInner)
+  bool SetStickyPositionData(FrameMetrics::ViewID aScrollId,
+                             LayerRectAbsolute aOuter, LayerRectAbsolute aInner)
   {
     if (mStickyPositionData &&
         mStickyPositionData->mOuter.IsEqualEdges(aOuter) &&
         mStickyPositionData->mInner.IsEqualEdges(aInner)) {
       return false;
     }
     if (!mStickyPositionData) {
       mStickyPositionData.emplace();
@@ -285,20 +285,20 @@ public:
     return mFixedPositionData ? mFixedPositionData->mSides : eSideBitsNone;
   }
   bool IsStickyPosition() const {
     return !!mStickyPositionData;
   }
   FrameMetrics::ViewID StickyScrollContainerId() const {
     return mStickyPositionData->mScrollId;
   }
-  const LayerBox& StickyScrollRangeOuter() const {
+  const LayerRectAbsolute& StickyScrollRangeOuter() const {
     return mStickyPositionData->mOuter;
   }
-  const LayerBox& StickyScrollRangeInner() const {
+  const LayerRectAbsolute& StickyScrollRangeInner() const {
     return mStickyPositionData->mInner;
   }
 
   bool operator ==(const SimpleLayerAttributes& aOther) const {
     return mTransform == aOther.mTransform &&
            mTransformIsPerspective == aOther.mTransformIsPerspective &&
            mScrolledClip == aOther.mScrolledClip &&
            mPostXScale == aOther.mPostXScale &&
@@ -332,18 +332,18 @@ private:
     FrameMetrics::ViewID mScrollId;
     LayerPoint mAnchor;
     int32_t mSides;
   };
   Maybe<FixedPositionData> mFixedPositionData;
 
   struct StickyPositionData {
     FrameMetrics::ViewID mScrollId;
-    LayerBox mOuter;
-    LayerBox mInner;
+    LayerRectAbsolute mOuter;
+    LayerRectAbsolute mInner;
   };
   Maybe<StickyPositionData> mStickyPositionData;
 
   // This class may only contain plain-old-data members that can be safely
   // copied over IPC. Make sure to add new members to operator ==.
 };
 
 } // namespace layers
diff --git a/gfx/layers/Layers.h b/gfx/layers/Layers.h
--- a/gfx/layers/Layers.h
+++ b/gfx/layers/Layers.h
@@ -1273,18 +1273,18 @@ public:
    * CONSTRUCTION PHASE ONLY
    * If a layer is "sticky position", |aScrollId| holds the scroll identifier
    * of the scrollable content that contains it. The difference between the two
    * rectangles |aOuter| and |aInner| is treated as two intervals in each
    * dimension, with the current scroll position at the origin. For each
    * dimension, while that component of the scroll position lies within either
    * interval, the layer should not move relative to its scrolling container.
    */
-  void SetStickyPositionData(FrameMetrics::ViewID aScrollId, LayerBox aOuter,
-                             LayerBox aInner)
+  void SetStickyPositionData(FrameMetrics::ViewID aScrollId,
+                             LayerRectAbsolute aOuter, LayerRectAbsolute aInner)
   {
     if (mSimpleAttrs.SetStickyPositionData(aScrollId, aOuter, aInner)) {
       MOZ_LAYERS_LOG_IF_SHADOWABLE(this, ("Layer::Mutated(%p) StickyPositionData", this));
       MutatedSimple();
     }
   }
 
   /**
@@ -1362,18 +1362,18 @@ public:
   virtual float GetPostYScale() const { return mSimpleAttrs.PostYScale(); }
   bool GetIsFixedPosition() { return mSimpleAttrs.IsFixedPosition(); }
   bool GetTransformIsPerspective() const { return mSimpleAttrs.TransformIsPerspective(); }
   bool GetIsStickyPosition() { return mSimpleAttrs.IsStickyPosition(); }
   FrameMetrics::ViewID GetFixedPositionScrollContainerId() { return mSimpleAttrs.FixedPositionScrollContainerId(); }
   LayerPoint GetFixedPositionAnchor() { return mSimpleAttrs.FixedPositionAnchor(); }
   int32_t GetFixedPositionSides() { return mSimpleAttrs.FixedPositionSides(); }
   FrameMetrics::ViewID GetStickyScrollContainerId() { return mSimpleAttrs.StickyScrollContainerId(); }
-  const LayerBox& GetStickyScrollRangeOuter() { return mSimpleAttrs.StickyScrollRangeOuter(); }
-  const LayerBox& GetStickyScrollRangeInner() { return mSimpleAttrs.StickyScrollRangeInner(); }
+  const LayerRectAbsolute& GetStickyScrollRangeOuter() { return mSimpleAttrs.StickyScrollRangeOuter(); }
+  const LayerRectAbsolute& GetStickyScrollRangeInner() { return mSimpleAttrs.StickyScrollRangeInner(); }
   FrameMetrics::ViewID GetScrollbarTargetContainerId() { return mSimpleAttrs.ScrollbarTargetContainerId(); }
   const ScrollThumbData& GetScrollThumbData() const { return mSimpleAttrs.ThumbData(); }
   bool IsScrollbarContainer() { return mSimpleAttrs.GetScrollbarContainerDirection().isSome(); }
   Maybe<ScrollDirection> GetScrollbarContainerDirection() { return mSimpleAttrs.GetScrollbarContainerDirection(); }
   Layer* GetMaskLayer() const { return mMaskLayer; }
   bool HasPendingTransform() const { return mPendingTransform; }
 
   void CheckCanary() const { mCanary.Check(); }
diff --git a/gfx/layers/composite/AsyncCompositionManager.cpp b/gfx/layers/composite/AsyncCompositionManager.cpp
--- a/gfx/layers/composite/AsyncCompositionManager.cpp
+++ b/gfx/layers/composite/AsyncCompositionManager.cpp
@@ -504,18 +504,18 @@ AsyncCompositionManager::AlignFixedAndSt
   LayerPoint unconsumedTranslation;
 
   if (layer->GetIsStickyPosition()) {
     // For sticky positioned layers, the difference between the two rectangles
     // defines a pair of translation intervals in each dimension through which
     // the layer should not move relative to the scroll container. To
     // accomplish this, we limit each dimension of the |translation| to that
     // part of it which overlaps those intervals.
-    const LayerBox& stickyOuter = layer->GetStickyScrollRangeOuter();
-    const LayerBox& stickyInner = layer->GetStickyScrollRangeInner();
+    const LayerRectAbsolute& stickyOuter = layer->GetStickyScrollRangeOuter();
+    const LayerRectAbsolute& stickyInner = layer->GetStickyScrollRangeInner();
 
     LayerPoint originalTranslation = translation;
     translation.y = IntervalOverlap(translation.y, stickyOuter.Y(), stickyOuter.YMost()) -
                     IntervalOverlap(translation.y, stickyInner.Y(), stickyInner.YMost());
     translation.x = IntervalOverlap(translation.x, stickyOuter.X(), stickyOuter.XMost()) -
                     IntervalOverlap(translation.x, stickyInner.X(), stickyInner.XMost());
     unconsumedTranslation = translation - originalTranslation;
   }
diff --git a/gfx/src/moz.build b/gfx/src/moz.build
--- a/gfx/src/moz.build
+++ b/gfx/src/moz.build
@@ -18,25 +18,25 @@ EXPORTS += [
     'FilterSupport.h',
     'gfxCrashReporterUtils.h',
     'gfxTelemetry.h',
     'nsBoundingMetrics.h',
     'nsColor.h',
     'nsColorNameList.h',
     'nsColorNames.h',
     'nsCoord.h',
-    'nsCoordBox.h',
     'nsDeviceContext.h',
     'nsFont.h',
     'nsFontMetrics.h',
     'nsGfxCIID.h',
     'nsITheme.h',
     'nsMargin.h',
     'nsPoint.h',
     'nsRect.h',
+    'nsRectAbsolute.h',
     'nsRegion.h',
     'nsRegionFwd.h',
     'nsSize.h',
     'nsThemeConstants.h',
     'nsTransform2D.h',
     'PingPongRegion.h',
     'RegionBuilder.h',
     'X11UndefineNone.h'
diff --git a/gfx/src/nsCoordBox.h b/gfx/src/nsRectAbsolute.h
rename from gfx/src/nsCoordBox.h
rename to gfx/src/nsRectAbsolute.h
--- a/gfx/src/nsCoordBox.h
+++ b/gfx/src/nsRectAbsolute.h
@@ -1,24 +1,23 @@
 /* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
-#ifndef NSCOORDBOX_H
-#define NSCOORDBOX_H
+#ifndef NSRECTABSOLUTE_H
+#define NSRECTABSOLUTE_H
 
-#include "mozilla/gfx/Box.h"
+#include "mozilla/gfx/RectAbsolute.h"
 #include "nsCoord.h"
 #include "nsRect.h"
 
-// Would like to call this nsBox, but can't because nsBox is a frame type.
-struct nsCoordBox :
-  public mozilla::gfx::BaseBox<nscoord, nsCoordBox, nsRect> {
-  typedef mozilla::gfx::BaseBox<nscoord, nsCoordBox, nsRect> Super;
+struct nsRectAbsolute :
+  public mozilla::gfx::BaseRectAbsolute<nscoord, nsRectAbsolute, nsRect> {
+  typedef mozilla::gfx::BaseRectAbsolute<nscoord, nsRectAbsolute, nsRect> Super;
 
-  nsCoordBox() : Super() {}
-  nsCoordBox(nscoord aX1, nscoord aY1, nscoord aX2, nscoord aY2) :
+  nsRectAbsolute() : Super() {}
+  nsRectAbsolute(nscoord aX1, nscoord aY1, nscoord aX2, nscoord aY2) :
       Super(aX1, aY1, aX2, aY2) {}
 };
 
-#endif /* NSCOORDBOX_H */
+#endif /* NSRECTABSOLUTE_H */
diff --git a/layout/base/Units.h b/layout/base/Units.h
--- a/layout/base/Units.h
+++ b/layout/base/Units.h
@@ -2,20 +2,20 @@
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef MOZ_UNITS_H_
 #define MOZ_UNITS_H_
 
-#include "mozilla/gfx/Box.h"
 #include "mozilla/gfx/Coord.h"
 #include "mozilla/gfx/Point.h"
 #include "mozilla/gfx/Rect.h"
+#include "mozilla/gfx/RectAbsolute.h"
 #include "mozilla/gfx/ScaleFactor.h"
 #include "mozilla/gfx/ScaleFactors2D.h"
 #include "nsMargin.h"
 #include "nsRect.h"
 #include "nsRegion.h"
 #include "mozilla/AppUnits.h"
 #include "mozilla/TypeTraits.h"
 
@@ -70,18 +70,18 @@ typedef gfx::IntMarginTyped<LayoutDevice
 typedef gfx::IntRegionTyped<LayoutDevicePixel> LayoutDeviceIntRegion;
 
 typedef gfx::CoordTyped<LayerPixel> LayerCoord;
 typedef gfx::IntCoordTyped<LayerPixel> LayerIntCoord;
 typedef gfx::PointTyped<LayerPixel> LayerPoint;
 typedef gfx::IntPointTyped<LayerPixel> LayerIntPoint;
 typedef gfx::SizeTyped<LayerPixel> LayerSize;
 typedef gfx::IntSizeTyped<LayerPixel> LayerIntSize;
-typedef gfx::BoxTyped<LayerPixel> LayerBox;
 typedef gfx::RectTyped<LayerPixel> LayerRect;
+typedef gfx::RectAbsoluteTyped<LayerPixel> LayerRectAbsolute;
 typedef gfx::IntRectTyped<LayerPixel> LayerIntRect;
 typedef gfx::MarginTyped<LayerPixel> LayerMargin;
 typedef gfx::IntMarginTyped<LayerPixel> LayerIntMargin;
 typedef gfx::IntRegionTyped<LayerPixel> LayerIntRegion;
 
 typedef gfx::CoordTyped<CSSTransformedLayerPixel> CSSTransformedLayerCoord;
 typedef gfx::IntCoordTyped<CSSTransformedLayerPixel> CSSTransformedLayerIntCoord;
 typedef gfx::PointTyped<CSSTransformedLayerPixel> CSSTransformedLayerPoint;
diff --git a/layout/generic/StickyScrollContainer.cpp b/layout/generic/StickyScrollContainer.cpp
--- a/layout/generic/StickyScrollContainer.cpp
+++ b/layout/generic/StickyScrollContainer.cpp
@@ -274,31 +274,31 @@ StickyScrollContainer::ComputePosition(n
   position.y = std::min(position.y, std::max(stick.YMost(), contain.y));
   position.x = std::max(position.x, std::min(stick.x, contain.XMost()));
   position.x = std::min(position.x, std::max(stick.XMost(), contain.x));
 
   return position;
 }
 
 void
-StickyScrollContainer::GetScrollRanges(nsIFrame* aFrame, nsCoordBox* aOuter,
-                                       nsCoordBox* aInner) const
+StickyScrollContainer::GetScrollRanges(nsIFrame* aFrame, nsRectAbsolute* aOuter,
+                                       nsRectAbsolute* aInner) const
 {
   // We need to use the first in flow; continuation frames should not move
   // relative to each other and should get identical scroll ranges.
   // Also, ComputeStickyLimits requires this.
   nsIFrame *firstCont =
     nsLayoutUtils::FirstContinuationOrIBSplitSibling(aFrame);
 
   nsRect stickRect;
   nsRect containRect;
   ComputeStickyLimits(firstCont, &stickRect, &containRect);
 
-  nsCoordBox stick = nsCoordBox::FromRect(stickRect);
-  nsCoordBox contain = nsCoordBox::FromRect(containRect);
+  nsRectAbsolute stick = nsRectAbsolute::FromRect(stickRect);
+  nsRectAbsolute contain = nsRectAbsolute::FromRect(containRect);
 
   aOuter->SetBox(gUnboundedNegative, gUnboundedNegative, gUnboundedPositive, gUnboundedPositive);
   aInner->SetBox(gUnboundedNegative, gUnboundedNegative, gUnboundedPositive, gUnboundedPositive);
 
   const nsPoint normalPosition = firstCont->GetNormalPosition();
 
   // Bottom and top
   if (stick.YMost() != gUnboundedPositive) {
diff --git a/layout/generic/StickyScrollContainer.h b/layout/generic/StickyScrollContainer.h
--- a/layout/generic/StickyScrollContainer.h
+++ b/layout/generic/StickyScrollContainer.h
@@ -7,18 +7,18 @@
 /**
  * compute sticky positioning, both during reflow and when the scrolling
  * container scrolls
  */
 
 #ifndef StickyScrollContainer_h
 #define StickyScrollContainer_h
 
-#include "nsCoordBox.h"
 #include "nsPoint.h"
+#include "nsRectAbsolute.h"
 #include "nsTArray.h"
 #include "nsIScrollPositionListener.h"
 
 struct nsRect;
 class nsIFrame;
 class nsIScrollableFrame;
 
 namespace mozilla {
@@ -63,17 +63,17 @@ public:
    * stored in its properties along with our scroll frame and scroll position.
    */
   nsPoint ComputePosition(nsIFrame* aFrame) const;
 
   /**
    * Compute where a frame should not scroll with the page, represented by the
    * difference of two rectangles.
    */
-  void GetScrollRanges(nsIFrame* aFrame, nsCoordBox* aOuter, nsCoordBox* aInner) const;
+  void GetScrollRanges(nsIFrame* aFrame, nsRectAbsolute* aOuter, nsRectAbsolute* aInner) const;
 
   /**
    * Compute and set the position of a frame and its following continuations.
    */
   void PositionContinuations(nsIFrame* aFrame);
 
   /**
    * Compute and set the position of all sticky frames, given the current
diff --git a/layout/painting/nsDisplayList.cpp b/layout/painting/nsDisplayList.cpp
--- a/layout/painting/nsDisplayList.cpp
+++ b/layout/painting/nsDisplayList.cpp
@@ -7411,28 +7411,28 @@ nsDisplayStickyPosition::BuildLayer(nsDi
   nsLayoutUtils::SetFixedPositionLayerData(layer, scrollFrame,
     nsRect(scrollFrame->GetOffsetToCrossDoc(ReferenceFrame()), scrollFrameSize),
     mFrame, presContext, aContainerParameters);
 
   ViewID scrollId = nsLayoutUtils::FindOrCreateIDFor(
     stickyScrollContainer->ScrollFrame()->GetScrolledFrame()->GetContent());
 
   float factor = presContext->AppUnitsPerDevPixel();
-  nsCoordBox outer;
-  nsCoordBox inner;
+  nsRectAbsolute outer;
+  nsRectAbsolute inner;
   stickyScrollContainer->GetScrollRanges(mFrame, &outer, &inner);
-  LayerBox stickyOuter(NSAppUnitsToFloatPixels(outer.X(), factor) *
+  LayerRectAbsolute stickyOuter(NSAppUnitsToFloatPixels(outer.X(), factor) *
                           aContainerParameters.mXScale,
                         NSAppUnitsToFloatPixels(outer.Y(), factor) *
                           aContainerParameters.mYScale,
                         NSAppUnitsToFloatPixels(outer.XMost(), factor) *
                           aContainerParameters.mXScale,
                         NSAppUnitsToFloatPixels(outer.YMost(), factor) *
                           aContainerParameters.mYScale);
-  LayerBox stickyInner(NSAppUnitsToFloatPixels(inner.X(), factor) *
+  LayerRectAbsolute stickyInner(NSAppUnitsToFloatPixels(inner.X(), factor) *
                           aContainerParameters.mXScale,
                         NSAppUnitsToFloatPixels(inner.Y(), factor) *
                           aContainerParameters.mYScale,
                         NSAppUnitsToFloatPixels(inner.XMost(), factor) *
                           aContainerParameters.mXScale,
                         NSAppUnitsToFloatPixels(inner.YMost(), factor) *
                           aContainerParameters.mYScale);
   layer->SetStickyPositionData(scrollId, stickyOuter, stickyInner);
@@ -7488,18 +7488,18 @@ nsDisplayStickyPosition::CreateWebRender
     Maybe<float> topMargin;
     Maybe<float> rightMargin;
     Maybe<float> bottomMargin;
     Maybe<float> leftMargin;
     wr::StickyOffsetBounds vBounds = { 0.0, 0.0 };
     wr::StickyOffsetBounds hBounds = { 0.0, 0.0 };
     nsPoint appliedOffset;
 
-    nsCoordBox outer;
-    nsCoordBox inner;
+    nsRectAbsolute outer;
+    nsRectAbsolute inner;
     stickyScrollContainer->GetScrollRanges(mFrame, &outer, &inner);
 
     nsIFrame* scrollFrame = do_QueryFrame(stickyScrollContainer->ScrollFrame());
     nsPoint offset = scrollFrame->GetOffsetToCrossDoc(ReferenceFrame());
 
     // Adjust the scrollPort coordinates to be relative to the reference frame,
     // so that it is in the same space as everything else.
     nsRect scrollPort = stickyScrollContainer->ScrollFrame()->GetScrollPortRect();
