# HG changeset patch
# User Matt Howell <mhowell@mozilla.com>
# Date 1527112662 25200
# Node ID 1f5426580dec836eaae2dbb7512461ffccfaf4b6
# Parent  40f7d1046d5d1c4536d3683c5b0b2c71aee451f0
Bug 1052582 Part 2 - Create and use a separate malloc arena for ArrayBuffer contents. r=sfink

MozReview-Commit-ID: 7IlFvr3hoA8

diff --git a/dom/network/TCPSocketChild.cpp b/dom/network/TCPSocketChild.cpp
--- a/dom/network/TCPSocketChild.cpp
+++ b/dom/network/TCPSocketChild.cpp
@@ -21,17 +21,18 @@ using mozilla::net::gNeckoChild;
 
 namespace IPC {
 
 bool
 DeserializeArrayBuffer(JSContext* cx,
                        const InfallibleTArray<uint8_t>& aBuffer,
                        JS::MutableHandle<JS::Value> aVal)
 {
-  mozilla::UniquePtr<uint8_t[], JS::FreePolicy> data(js_pod_malloc<uint8_t>(aBuffer.Length()));
+  mozilla::UniquePtr<uint8_t[], JS::FreePolicy> data(
+    js_pod_arena_malloc<uint8_t>(js::ArrayBufferContentsArena, aBuffer.Length()));
   if (!data)
       return false;
   memcpy(data.get(), aBuffer.Elements(), aBuffer.Length());
 
   JSObject* obj = JS_NewArrayBufferWithContents(cx, aBuffer.Length(), data.get());
   if (!obj)
       return false;
   // If JS_NewArrayBufferWithContents returns non-null, the ownership of
diff --git a/dom/workers/FileReaderSync.cpp b/dom/workers/FileReaderSync.cpp
--- a/dom/workers/FileReaderSync.cpp
+++ b/dom/workers/FileReaderSync.cpp
@@ -60,17 +60,18 @@ FileReaderSync::ReadAsArrayBuffer(JSCont
                                   JS::MutableHandle<JSObject*> aRetval,
                                   ErrorResult& aRv)
 {
   uint64_t blobSize = aBlob.GetSize(aRv);
   if (NS_WARN_IF(aRv.Failed())) {
     return;
   }
 
-  UniquePtr<char[], JS::FreePolicy> bufferData(js_pod_malloc<char>(blobSize));
+  UniquePtr<char[], JS::FreePolicy> bufferData(
+    js_pod_arena_malloc<char>(js::ArrayBufferContentsArena, blobSize));
   if (!bufferData) {
     aRv.Throw(NS_ERROR_OUT_OF_MEMORY);
     return;
   }
 
   nsCOMPtr<nsIInputStream> stream;
   aBlob.CreateInputStream(getter_AddRefs(stream), aRv);
   if (NS_WARN_IF(aRv.Failed())) {
diff --git a/js/public/Utility.h b/js/public/Utility.h
--- a/js/public/Utility.h
+++ b/js/public/Utility.h
@@ -370,16 +370,17 @@ struct MOZ_RAII JS_PUBLIC_DATA(AutoEnter
 
 } /* namespace js */
 
 // Malloc allocation.
 
 namespace js {
 
 extern JS_PUBLIC_DATA(arena_id_t) MallocArena;
+extern JS_PUBLIC_DATA(arena_id_t) ArrayBufferContentsArena;
 
 extern void InitMallocAllocator();
 extern void ShutDownMallocAllocator();
 
 } /* namespace js */
 
 static inline void* js_arena_malloc(arena_id_t arena, size_t bytes)
 {
diff --git a/js/src/jsutil.cpp b/js/src/jsutil.cpp
--- a/js/src/jsutil.cpp
+++ b/js/src/jsutil.cpp
@@ -160,28 +160,31 @@ ResetSimulatedInterrupt()
     interruptCheckFailAlways = false;
 }
 
 } // namespace oom
 } // namespace js
 #endif // defined(DEBUG) || defined(JS_OOM_BREAKPOINT)
 
 JS_PUBLIC_DATA(arena_id_t) js::MallocArena;
+JS_PUBLIC_DATA(arena_id_t) js::ArrayBufferContentsArena;
 
 void
 js::InitMallocAllocator()
 {
     MallocArena = moz_create_arena();
+    ArrayBufferContentsArena = moz_create_arena();
 }
 
 void
 js::ShutDownMallocAllocator()
 {
     // Until Bug 1364359 is fixed it is unsafe to call moz_dispose_arena.
     // moz_dispose_arena(MallocArena);
+    // moz_dispose_arena(ArrayBufferContentsArena);
 }
 
 JS_PUBLIC_API(void)
 JS_Assert(const char* s, const char* file, int ln)
 {
     MOZ_ReportAssertionFailure(s, file, ln);
     MOZ_CRASH();
 }
diff --git a/js/src/vm/ArrayBufferObject.cpp b/js/src/vm/ArrayBufferObject.cpp
--- a/js/src/vm/ArrayBufferObject.cpp
+++ b/js/src/vm/ArrayBufferObject.cpp
@@ -454,17 +454,18 @@ ArrayBufferObject::class_constructor(JSC
         return false;
     args.rval().setObject(*bufobj);
     return true;
 }
 
 static ArrayBufferObject::BufferContents
 AllocateArrayBufferContents(JSContext* cx, uint32_t nbytes)
 {
-    uint8_t* p = cx->zone()->pod_callocCanGC<uint8_t>(nbytes);
+    uint8_t* p = cx->zone()->pod_callocCanGC<uint8_t>(nbytes,
+                                                      js::ArrayBufferContentsArena);
     if (!p)
         ReportOutOfMemory(cx);
 
     return ArrayBufferObject::BufferContents::create<ArrayBufferObject::PLAIN>(p);
 }
 
 static void
 NoteViewBufferWasDetached(ArrayBufferViewObject* view,
