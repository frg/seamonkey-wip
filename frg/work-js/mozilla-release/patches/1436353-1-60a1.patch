# HG changeset patch
# User Benjamin Bouvier <benj@benj.me>
# Date 1518011688 -3600
# Node ID 0a4f01f2bab716b21d343e1f197bb6031000ce64
# Parent  19d4cddb35a1a03f62b601c441322bd7842b4798
Bug 1436353: Fix and enhance perf support in the jits; r=campbell

MozReview-Commit-ID: IKyJf5jRIZu

diff --git a/js/src/jit/CodeGenerator.cpp b/js/src/jit/CodeGenerator.cpp
--- a/js/src/jit/CodeGenerator.cpp
+++ b/js/src/jit/CodeGenerator.cpp
@@ -5501,17 +5501,18 @@ CodeGenerator::generateBody()
         if (counts) {
             blockCounts.emplace(&counts->block(i), &masm);
             if (!blockCounts->init())
                 return false;
         }
         TrackedOptimizations* last = nullptr;
 
 #if defined(JS_ION_PERF)
-        perfSpewer->startBasicBlock(current->mir(), masm);
+        if (!perfSpewer->startBasicBlock(current->mir(), masm))
+            return false;
 #endif
 
         for (LInstructionIterator iter = current->begin(); iter != current->end(); iter++) {
             if (!alloc().ensureBallast())
                 return false;
 
 #ifdef JS_JITSPEW
             JitSpewStart(JitSpew_Codegen, "instruction %s", iter->opName());
diff --git a/js/src/jit/PerfSpewer.cpp b/js/src/jit/PerfSpewer.cpp
--- a/js/src/jit/PerfSpewer.cpp
+++ b/js/src/jit/PerfSpewer.cpp
@@ -124,34 +124,31 @@ js::jit::PerfBlockEnabled() {
 }
 
 bool
 js::jit::PerfFuncEnabled() {
     MOZ_ASSERT(PerfMode);
     return PerfMode == PERF_MODE_FUNC;
 }
 
-static bool
-lockPerfMap(void)
-{
-    if (!PerfEnabled())
-        return false;
-
-    PerfMutex->lock();
-
-    MOZ_ASSERT(PerfFilePtr);
-    return true;
-}
-
-static void
-unlockPerfMap()
-{
-    MOZ_ASSERT(PerfFilePtr);
-    fflush(PerfFilePtr);
-    PerfMutex->unlock();
+namespace {
+    struct MOZ_RAII AutoLockPerfMap
+    {
+        AutoLockPerfMap() {
+            if (!PerfEnabled())
+                return;
+            PerfMutex->lock();
+            MOZ_ASSERT(PerfFilePtr);
+        }
+        ~AutoLockPerfMap() {
+            MOZ_ASSERT(PerfFilePtr);
+            fflush(PerfFilePtr);
+            PerfMutex->unlock();
+        }
+    };
 }
 
 uint32_t PerfSpewer::nextFunctionIndex = 0;
 
 bool
 PerfSpewer::startBasicBlock(MBasicBlock* blk,
                             MacroAssembler& masm)
 {
@@ -168,172 +165,150 @@ PerfSpewer::startBasicBlock(MBasicBlock*
         lineNumber = 0;
         columnNumber = 0;
     }
     Record r(filename, lineNumber, columnNumber, blk->id());
     masm.bind(&r.start);
     return basicBlocks_.append(r);
 }
 
-bool
+void
 PerfSpewer::endBasicBlock(MacroAssembler& masm)
 {
     if (!PerfBlockEnabled())
-        return true;
-
+        return;
     masm.bind(&basicBlocks_.back().end);
-    return true;
 }
 
-bool
+void
 PerfSpewer::noteEndInlineCode(MacroAssembler& masm)
 {
     if (!PerfBlockEnabled())
-        return true;
+        return;
+    masm.bind(&endInlineCode);
+}
 
-    masm.bind(&endInlineCode);
-    return true;
+void
+PerfSpewer::WriteEntry(const AutoLockPerfMap&, uintptr_t address, size_t size,
+                       const char* fmt, ...)
+{
+    va_list ap;
+    va_start(ap, fmt);
+
+    auto result = mozilla::Vsmprintf<js::SystemAllocPolicy>(fmt, ap);
+    va_end(ap);
+
+    fprintf(PerfFilePtr, "%" PRIxPTR " %zx %s\n",
+            address,
+            size,
+            result.get());
 }
 
 void
 PerfSpewer::writeProfile(JSScript* script,
                          JitCode* code,
                          MacroAssembler& masm)
 {
+    AutoLockPerfMap lock;
+
     if (PerfFuncEnabled()) {
-        if (!lockPerfMap())
-            return;
-
         uint32_t thisFunctionIndex = nextFunctionIndex++;
-
         size_t size = code->instructionsSize();
         if (size > 0) {
-            fprintf(PerfFilePtr, "%p %zx %s:%zu: Func%02d\n",
-                    code->raw(),
-                    size,
-                    script->filename(),
-                    script->lineno(),
-                    thisFunctionIndex);
+            WriteEntry(lock, reinterpret_cast<uintptr_t>(code->raw()), size, "%s:%zu: Func%02" PRIu32,
+                       script->filename(), script->lineno(), thisFunctionIndex);
         }
-        unlockPerfMap();
         return;
     }
 
     if (PerfBlockEnabled() && basicBlocks_.length() > 0) {
-        if (!lockPerfMap())
-            return;
-
         uint32_t thisFunctionIndex = nextFunctionIndex++;
         uintptr_t funcStart = uintptr_t(code->raw());
         uintptr_t funcEndInlineCode = funcStart + endInlineCode.offset();
         uintptr_t funcEnd = funcStart + code->instructionsSize();
 
         // function begins with the prologue, which is located before the first basic block
         size_t prologueSize = basicBlocks_[0].start.offset();
 
         if (prologueSize > 0) {
-            fprintf(PerfFilePtr, "%zx %zx %s:%zu: Func%02d-Prologue\n",
-                    funcStart, prologueSize, script->filename(), script->lineno(), thisFunctionIndex);
+            WriteEntry(lock, funcStart, prologueSize, "%s:%zu: Func%02" PRIu32 "-Prologue",
+                       script->filename(), script->lineno(), thisFunctionIndex);
         }
 
         uintptr_t cur = funcStart + prologueSize;
         for (uint32_t i = 0; i < basicBlocks_.length(); i++) {
             Record& r = basicBlocks_[i];
 
             uintptr_t blockStart = funcStart + r.start.offset();
             uintptr_t blockEnd = funcStart + r.end.offset();
 
             MOZ_ASSERT(cur <= blockStart);
             if (cur < blockStart) {
-                fprintf(PerfFilePtr, "%" PRIxPTR " %" PRIxPTR " %s:%zu: Func%02d-Block?\n",
-                        cur, blockStart - cur,
-                        script->filename(), script->lineno(),
-                        thisFunctionIndex);
+                WriteEntry(lock, cur, blockStart - cur, "%s:%zu: Func%02" PRIu32 "-Block?",
+                           script->filename(), script->lineno(), thisFunctionIndex);
             }
             cur = blockEnd;
 
             size_t size = blockEnd - blockStart;
 
             if (size > 0) {
-                fprintf(PerfFilePtr, "%" PRIxPTR " %zx %s:%d:%d: Func%02d-Block%d\n",
-                        blockStart, size,
-                        r.filename, r.lineNumber, r.columnNumber,
-                        thisFunctionIndex, r.id);
+                WriteEntry(lock, blockStart, size, "%s:%u:%u: Func%02" PRIu32 "d-Block%" PRIu32,
+                           r.filename, r.lineNumber, r.columnNumber, thisFunctionIndex, r.id);
             }
         }
 
         MOZ_ASSERT(cur <= funcEndInlineCode);
         if (cur < funcEndInlineCode) {
-            fprintf(PerfFilePtr, "%" PRIxPTR " %" PRIxPTR " %s:%zu: Func%02d-Epilogue\n",
-                    cur, funcEndInlineCode - cur,
-                    script->filename(), script->lineno(),
-                    thisFunctionIndex);
+            WriteEntry(lock, cur, funcEndInlineCode - cur, "%s:%zu: Func%02" PRIu32 "-Epilogue",
+                       script->filename(), script->lineno(), thisFunctionIndex);
         }
 
         MOZ_ASSERT(funcEndInlineCode <= funcEnd);
         if (funcEndInlineCode < funcEnd) {
-            fprintf(PerfFilePtr, "%" PRIxPTR " %" PRIxPTR " %s:%zu: Func%02d-OOL\n",
-                    funcEndInlineCode, funcEnd - funcEndInlineCode,
-                    script->filename(), script->lineno(),
-                    thisFunctionIndex);
+            WriteEntry(lock, funcEndInlineCode, funcEnd - funcEndInlineCode,
+                       "%s:%zu: Func%02" PRIu32 "-OOL",
+                       script->filename(), script->lineno(), thisFunctionIndex);
         }
-
-        unlockPerfMap();
-        return;
     }
 }
 
 void
 js::jit::writePerfSpewerBaselineProfile(JSScript* script, JitCode* code)
 {
     if (!PerfEnabled())
         return;
 
-    if (!lockPerfMap())
-        return;
-
     size_t size = code->instructionsSize();
     if (size > 0) {
-        fprintf(PerfFilePtr, "%" PRIxPTR " %zx %s:%zu: Baseline\n",
-                reinterpret_cast<uintptr_t>(code->raw()),
-                size, script->filename(), script->lineno());
+        AutoLockPerfMap lock;
+        PerfSpewer::WriteEntry(lock, reinterpret_cast<uintptr_t>(code->raw()), size,
+                               "%s:%zu: Baseline", script->filename(), script->lineno());
     }
-
-    unlockPerfMap();
 }
 
 void
 js::jit::writePerfSpewerJitCodeProfile(JitCode* code, const char* msg)
 {
     if (!code || !PerfEnabled())
         return;
 
-    if (!lockPerfMap())
-        return;
-
     size_t size = code->instructionsSize();
     if (size > 0) {
-        fprintf(PerfFilePtr, "%" PRIxPTR " %zx %s (%p 0x%zx)\n",
-                reinterpret_cast<uintptr_t>(code->raw()),
-                size, msg, code->raw(), size);
+        AutoLockPerfMap lock;
+        PerfSpewer::WriteEntry(lock, reinterpret_cast<uintptr_t>(code->raw()), size,
+                               "%s (%p 0x%zx)", msg, code->raw(), size);
     }
 
-    unlockPerfMap();
 }
 
 void
 js::jit::writePerfSpewerWasmFunctionMap(uintptr_t base, uintptr_t size,
                                          const char* filename, unsigned lineno, unsigned colIndex,
                                          const char* funcName)
 {
     if (!PerfFuncEnabled() || size == 0U)
         return;
 
-    if (!lockPerfMap())
-        return;
-
-    fprintf(PerfFilePtr, "%" PRIxPTR " %" PRIxPTR " %s:%u:%u: Function %s\n",
-            base, size, filename, lineno, colIndex, funcName);
-
-    unlockPerfMap();
+    AutoLockPerfMap lock;
+    PerfSpewer::WriteEntry(lock, base, size, "%s:%u: Function %s", filename, lineno, funcName);
 }
 
 #endif // defined (JS_ION_PERF)
diff --git a/js/src/jit/PerfSpewer.h b/js/src/jit/PerfSpewer.h
--- a/js/src/jit/PerfSpewer.h
+++ b/js/src/jit/PerfSpewer.h
@@ -7,16 +7,20 @@
 #ifndef jit_PerfSpewer_h
 #define jit_PerfSpewer_h
 
 #ifdef JS_ION_PERF
 # include <stdio.h>
 # include "jit/MacroAssembler.h"
 #endif
 
+namespace {
+    struct MOZ_RAII AutoLockPerfMap;
+}
+
 namespace js {
 namespace jit {
 
 class MBasicBlock;
 class MacroAssembler;
 
 #ifdef JS_ION_PERF
 void CheckPerf();
@@ -62,31 +66,35 @@ class PerfSpewer
   public:
     Label endInlineCode;
 
   protected:
     BasicBlocksVector basicBlocks_;
 
   public:
     virtual MOZ_MUST_USE bool startBasicBlock(MBasicBlock* blk, MacroAssembler& masm);
-    virtual MOZ_MUST_USE bool endBasicBlock(MacroAssembler& masm);
-    MOZ_MUST_USE bool noteEndInlineCode(MacroAssembler& masm);
+    virtual void endBasicBlock(MacroAssembler& masm);
+    void noteEndInlineCode(MacroAssembler& masm);
 
     void writeProfile(JSScript* script, JitCode* code, MacroAssembler& masm);
+
+    static void WriteEntry(const AutoLockPerfMap&, uintptr_t address, size_t size,
+                           const char* fmt, ...)
+        MOZ_FORMAT_PRINTF(4, 5);
 };
 
 void writePerfSpewerBaselineProfile(JSScript* script, JitCode* code);
 void writePerfSpewerJitCodeProfile(JitCode* code, const char* msg);
 
 // wasm doesn't support block annotations.
 class WasmPerfSpewer : public PerfSpewer
 {
   public:
     MOZ_MUST_USE bool startBasicBlock(MBasicBlock* blk, MacroAssembler& masm) { return true; }
-    MOZ_MUST_USE bool endBasicBlock(MacroAssembler& masm) { return true; }
+    void endBasicBlock(MacroAssembler& masm) { }
 };
 
 void writePerfSpewerWasmFunctionMap(uintptr_t base, uintptr_t size, const char* filename,
                                     unsigned lineno, unsigned colIndex, const char* funcName);
 
 #endif // JS_ION_PERF
 
 } // namespace jit
