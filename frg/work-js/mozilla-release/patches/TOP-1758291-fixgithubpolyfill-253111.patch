# HG changeset patch
# User Dmitry Butskoy <buc@buc.me>
# Date 1645392882 -3600
# Parent  db1e724d16219a59da6c861efade1386f3b9e0aa
Bug 1758291 - Fix edge case when setting IntersectionObserver threshold. r=frg a=frg

See https://github.com/JustOff/github-wc-polyfill/issues/44#issuecomment-1043176073
and http://logbot.buc.me/seamonkey/20220220#c11121

diff --git a/dom/base/DOMIntersectionObserver.cpp b/dom/base/DOMIntersectionObserver.cpp
--- a/dom/base/DOMIntersectionObserver.cpp
+++ b/dom/base/DOMIntersectionObserver.cpp
@@ -441,28 +441,30 @@ DOMIntersectionObserver::Update(nsIDocum
     if (targetArea > 0.0) {
       intersectionRatio =
         std::min((double) intersectionArea / (double) targetArea, 1.0);
     } else {
       intersectionRatio = intersectionRect.isSome() ? 1.0 : 0.0;
     }
 
     int32_t threshold = -1;
-    if (intersectionRect.isSome()) {
+    if (intersectionRatio > 0.0) {
       // Spec: "Let thresholdIndex be the index of the first entry in
       // observer.thresholds whose value is greater than intersectionRatio."
       threshold = mThresholds.IndexOfFirstElementGt(intersectionRatio);
       if (threshold == 0) {
         // Per the spec, we should leave threshold at 0 and distinguish between
         // "less than all thresholds and intersecting" and "not intersecting"
         // (queuing observer entries as both cases come to pass). However,
         // neither Chrome nor the WPT tests expect this behavior, so treat these
         // two cases as one.
         threshold = -1;
       }
+    } else if (intersectionRect.isSome()) {
+      threshold = 0;
     }
 
     if (target->UpdateIntersectionObservation(this, threshold)) {
       QueueIntersectionObserverEntry(
         target, time,
         isInSimilarOriginBrowsingContext ==
           BrowsingContextInfo::DifferentOriginBrowsingContext ?
           Nothing() : Some(rootIntersectionRect),
