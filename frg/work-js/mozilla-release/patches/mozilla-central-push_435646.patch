# HG changeset patch
# User Jon Coppeard <jcoppeard@mozilla.com>
# Date 1536650317 -3600
#      Tue Sep 11 08:18:37 2018 +0100
# Node ID 20e30197e49021d2c3f09b37228062b756a5975f
# Parent  bef9c20b995274f5989c979b8eae6cc091a568a5
Bug 1490055 - Use SystemAllocPolicy for ParseTask which can be used by both main thread and helper thread contexts r=nbp

diff --git a/js/src/vm/HelperThreads.cpp b/js/src/vm/HelperThreads.cpp
--- a/js/src/vm/HelperThreads.cpp
+++ b/js/src/vm/HelperThreads.cpp
@@ -426,42 +426,49 @@ static const JSClass parseTaskGlobalClas
 
 ParseTask::ParseTask(ParseTaskKind kind, JSContext* cx,
                      JS::OffThreadCompileCallback callback, void* callbackData)
   : kind(kind),
     options(cx),
     alloc(JSContext::TEMP_LIFO_ALLOC_PRIMARY_CHUNK_SIZE),
     parseGlobal(nullptr),
     callback(callback), callbackData(callbackData),
-    scripts(cx), sourceObjects(cx),
     overRecursed(false), outOfMemory(false)
 {
+    // Note that |cx| is the main thread context here but the parse task will
+    // run with a different, helper thread, context.
+    MOZ_ASSERT(!cx->helperThread());
+
     MOZ_ALWAYS_TRUE(scripts.reserve(scripts.capacity()));
     MOZ_ALWAYS_TRUE(sourceObjects.reserve(sourceObjects.capacity()));
 }
 
 bool
 ParseTask::init(JSContext* cx, const ReadOnlyCompileOptions& options, JSObject* global)
 {
+    MOZ_ASSERT(!cx->helperThread());
+
     if (!this->options.copy(cx, options))
         return false;
 
     parseGlobal = global;
     return true;
 }
 
 void
 ParseTask::activate(JSRuntime* rt)
 {
     rt->setUsedByHelperThread(parseGlobal->zone());
 }
 
 bool
 ParseTask::finish(JSContext* cx)
 {
+    MOZ_ASSERT(!cx->helperThread());
+
     for (auto& sourceObject : sourceObjects) {
         RootedScriptSourceObject sso(cx, sourceObject);
         if (!ScriptSourceObject::initFromOptions(cx, sso, options))
             return false;
         if (!sso->source()->tryCompressOffThread(cx))
             return false;
     }
 
@@ -499,16 +506,18 @@ ScriptParseTask::ScriptParseTask(JSConte
                                  JS::OffThreadCompileCallback callback, void* callbackData)
   : ParseTask(ParseTaskKind::Script, cx, callback, callbackData),
     data(std::move(srcBuf))
 {}
 
 void
 ScriptParseTask::parse(JSContext* cx)
 {
+    MOZ_ASSERT(cx->helperThread());
+
     Rooted<ScriptSourceObject*> sourceObject(cx);
 
     ScopeKind scopeKind = options.nonSyntacticScope ? ScopeKind::NonSyntactic : ScopeKind::Global;
 
     JSScript* script = frontend::CompileGlobalScript(cx, alloc, scopeKind,
                                                      options, data,
                                                      /* sourceObjectOut = */ &sourceObject.get());
     if (script)
@@ -521,16 +530,18 @@ ModuleParseTask::ModuleParseTask(JSConte
                                  JS::OffThreadCompileCallback callback, void* callbackData)
   : ParseTask(ParseTaskKind::Module, cx, callback, callbackData),
     data(std::move(srcBuf))
 {}
 
 void
 ModuleParseTask::parse(JSContext* cx)
 {
+    MOZ_ASSERT(cx->helperThread());
+
     Rooted<ScriptSourceObject*> sourceObject(cx);
 
     JSScript* script = frontend::CompileModule(cx, options, data, alloc, &sourceObject.get());
     if (script) {
         scripts.infallibleAppend(script);
         if (sourceObject)
             sourceObjects.infallibleAppend(sourceObject);
     }
@@ -540,16 +551,18 @@ ScriptDecodeTask::ScriptDecodeTask(JSCon
                                    JS::OffThreadCompileCallback callback, void* callbackData)
   : ParseTask(ParseTaskKind::ScriptDecode, cx, callback, callbackData),
     range(range)
 {}
 
 void
 ScriptDecodeTask::parse(JSContext* cx)
 {
+    MOZ_ASSERT(cx->helperThread());
+
     RootedScript resultScript(cx);
     Rooted<ScriptSourceObject*> sourceObject(cx);
 
     XDROffThreadDecoder decoder(cx, alloc, &options, /* sourceObjectOut = */ &sourceObject.get(),
                                 range);
     XDRResult res = decoder.codeScript(&resultScript);
     MOZ_ASSERT(bool(resultScript) == res.isOk());
     if (res.isOk()) {
@@ -565,16 +578,18 @@ BinASTDecodeTask::BinASTDecodeTask(JSCon
                                    JS::OffThreadCompileCallback callback, void* callbackData)
   : ParseTask(ParseTaskKind::BinAST, cx, callback, callbackData),
     data(buf, length)
 {}
 
 void
 BinASTDecodeTask::parse(JSContext* cx)
 {
+    MOZ_ASSERT(cx->helperThread());
+
     RootedScriptSourceObject sourceObject(cx);
 
     JSScript* script = frontend::CompileGlobalBinASTScript(cx, alloc, options,
                                                            data.begin().get(), data.length(),
                                                            &sourceObject.get());
     if (script) {
         scripts.infallibleAppend(script);
         if (sourceObject)
@@ -589,19 +604,22 @@ MultiScriptsDecodeTask::MultiScriptsDeco
                                                void* callbackData)
   : ParseTask(ParseTaskKind::MultiScriptsDecode, cx, callback, callbackData),
     sources(&sources)
 {}
 
 void
 MultiScriptsDecodeTask::parse(JSContext* cx)
 {
+    MOZ_ASSERT(cx->helperThread());
+
     if (!scripts.reserve(sources->length()) ||
         !sourceObjects.reserve(sources->length()))
     {
+        ReportOutOfMemory(cx); // This sets |outOfMemory|.
         return;
     }
 
     for (auto& source : *sources) {
         CompileOptions opts(cx, options);
         opts.setFileAndLine(source.filename, source.lineno);
 
         RootedScript resultScript(cx);
@@ -1647,16 +1665,17 @@ GlobalHelperThreadState::removeFinishedP
     return task;
 }
 
 template <typename F, typename>
 bool
 GlobalHelperThreadState::finishParseTask(JSContext* cx, ParseTaskKind kind,
                                          JS::OffThreadToken* token, F&& finishCallback)
 {
+    MOZ_ASSERT(!cx->helperThread());
     MOZ_ASSERT(cx->realm());
 
     Rooted<UniquePtr<ParseTask>> parseTask(cx, removeFinishedParseTask(kind, token));
 
     // Make sure we have all the constructors we need for the prototype
     // remapping below, since we can't GC while that's happening.
     if (!EnsureParserCreatedClasses(cx, kind)) {
         LeaveParseTaskZone(cx->runtime(), parseTask.get().get());
@@ -1724,24 +1743,26 @@ GlobalHelperThreadState::finishParseTask
 
 bool
 GlobalHelperThreadState::finishParseTask(JSContext* cx, ParseTaskKind kind,
                                          JS::OffThreadToken* token,
                                          MutableHandle<ScriptVector> scripts)
 {
     size_t expectedLength = 0;
 
-    bool ok = finishParseTask(cx, kind, token, [&scripts, &expectedLength] (ParseTask* parseTask) {
+    bool ok = finishParseTask(cx, kind, token, [cx, &scripts, &expectedLength] (ParseTask* parseTask) {
         MOZ_ASSERT(parseTask->kind == ParseTaskKind::MultiScriptsDecode);
         auto task = static_cast<MultiScriptsDecodeTask*>(parseTask);
 
         expectedLength = task->sources->length();
 
-        if (!scripts.reserve(task->scripts.length()))
+        if (!scripts.reserve(task->scripts.length())) {
+            ReportOutOfMemory(cx);
             return false;
+        }
 
         for (auto& script : task->scripts)
             scripts.infallibleAppend(script);
         return true;
     });
 
     if (!ok)
         return false;
diff --git a/js/src/vm/HelperThreads.h b/js/src/vm/HelperThreads.h
--- a/js/src/vm/HelperThreads.h
+++ b/js/src/vm/HelperThreads.h
@@ -697,20 +697,20 @@ struct ParseTask : public mozilla::Linke
 
     // Callback invoked off thread when the parse finishes.
     JS::OffThreadCompileCallback callback;
     void* callbackData;
 
     // Holds the final scripts between the invocation of the callback and the
     // point where FinishOffThreadScript is called, which will destroy the
     // ParseTask.
-    GCVector<JSScript*, 1> scripts;
+    GCVector<JSScript*, 1, SystemAllocPolicy> scripts;
 
     // Holds the ScriptSourceObjects generated for the script compilation.
-    GCVector<ScriptSourceObject*, 1> sourceObjects;
+    GCVector<ScriptSourceObject*, 1, SystemAllocPolicy> sourceObjects;
 
     // Any errors or warnings produced during compilation. These are reported
     // when finishing the script.
     Vector<UniquePtr<CompileError>, 0, SystemAllocPolicy> errors;
     bool overRecursed;
     bool outOfMemory;
 
     ParseTask(ParseTaskKind kind, JSContext* cx,
