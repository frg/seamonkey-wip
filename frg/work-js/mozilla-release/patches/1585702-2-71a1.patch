# HG changeset patch
# User Andrew Halberstadt <ahalberstadt@mozilla.com>
# Date 1570211198 0
# Node ID 9ea7f7d1768681d657daf36134d7a7d681b2e879
# Parent  0505d94eaab49ddb68626cadffc0931538b8cbdb
Bug 1585702 - [mozprocess] Fix "Embedded null character" error in Windows with Python 3, r=gbrown

This works around a bug in Python:
https://bugs.python.org/issue32745

Null characters aren't allowed in 'c_wchar_p' types anymore, but we can get around
the issue by allocating a buffer in memory and casting it after the fact. This was
discovered via trial and error and I'm not really sure why it works.. But it does.

This also enables the tests under Python 3 on Windows (which thankfully all
seem to pass).

Differential Revision: https://phabricator.services.mozilla.com/D48113

diff --git a/testing/mozbase/mozprocess/mozprocess/processhandler.py b/testing/mozbase/mozprocess/mozprocess/processhandler.py
--- a/testing/mozbase/mozprocess/mozprocess/processhandler.py
+++ b/testing/mozbase/mozprocess/mozprocess/processhandler.py
@@ -725,19 +725,17 @@ falling back to not using job objects fo
         self.cmd = cmd
         self.args = args
         self.cwd = cwd
         self.didTimeout = False
         self._ignore_children = ignore_children
         self.keywordargs = kwargs
         self.read_buffer = ''
 
-        # XXX Bug 1585702 - Setting env is broken on Windows + Python 3. In the
-        # meantime let's at least make sure we don't set it in the default case.
-        if env is None and not (six.PY3 and isWin):
+        if env is None:
             env = os.environ.copy()
         self.env = env
 
         # handlers
         def to_callable_list(arg):
             if callable(arg):
                 arg = [arg]
             return CallableList(arg)
diff --git a/testing/mozbase/mozprocess/mozprocess/winprocess.py b/testing/mozbase/mozprocess/mozprocess/winprocess.py
--- a/testing/mozbase/mozprocess/mozprocess/winprocess.py
+++ b/testing/mozbase/mozprocess/mozprocess/winprocess.py
@@ -33,17 +33,28 @@
 # OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 # NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION
 # WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 
 from __future__ import absolute_import, unicode_literals, print_function
 
 import subprocess
 import sys
-from ctypes import c_void_p, POINTER, sizeof, Structure, windll, WinError, WINFUNCTYPE, c_ulong
+from ctypes import (
+    cast,
+    create_unicode_buffer,
+    c_ulong,
+    c_void_p,
+    POINTER,
+    sizeof,
+    Structure,
+    windll,
+    WinError,
+    WINFUNCTYPE,
+)
 from ctypes.wintypes import BOOL, BYTE, DWORD, HANDLE, LPCWSTR, LPWSTR, UINT, WORD
 
 from .qijo import QueryInformationJobObject
 
 LPVOID = c_void_p
 LPBYTE = POINTER(BYTE)
 LPDWORD = POINTER(DWORD)
 LPBOOL = POINTER(BOOL)
@@ -158,18 +169,23 @@ class EnvironmentBlock:
             values = []
             fs_encoding = sys.getfilesystemencoding() or 'mbcs'
             for k, v in env.items():
                 if isinstance(k, bytes):
                     k = k.decode(fs_encoding, 'replace')
                 if isinstance(v, bytes):
                     v = v.decode(fs_encoding, 'replace')
                 values.append("{}={}".format(k, v))
-            values.append("")
-            self._as_parameter_ = LPCWSTR("\0".join(values))
+
+            # The lpEnvironment parameter of the 'CreateProcess' function expects a series
+            # of null terminated strings followed by a final null terminator. We write this
+            # value to a buffer and then cast it to LPCWSTR to avoid a Python ctypes bug
+            # that probihits embedded null characters (https://bugs.python.org/issue32745).
+            values = create_unicode_buffer("\0".join(values) + "\0")
+            self._as_parameter_ = cast(values, LPCWSTR)
 
 
 # Error Messages we need to watch for go here
 
 # https://msdn.microsoft.com/en-us/library/windows/desktop/ms681382(v=vs.85).aspx (0 - 499)
 ERROR_ACCESS_DENIED = 5
 ERROR_INVALID_PARAMETER = 87
 
diff --git a/testing/mozbase/mozprocess/tests/manifest.ini b/testing/mozbase/mozprocess/tests/manifest.ini
--- a/testing/mozbase/mozprocess/tests/manifest.ini
+++ b/testing/mozbase/mozprocess/tests/manifest.ini
@@ -1,11 +1,9 @@
 [DEFAULT]
 subsuite = mozbase, os == "linux"
-# Python bug https://bugs.python.org/issue32745
-skip-if = python == 3 && os == "win"  # Bug 1428713 for more info
 [test_kill.py]
 [test_misc.py]
 [test_poll.py]
 [test_wait.py]
 [test_output.py]
 [test_params.py]
 [test_process_reader.py]
