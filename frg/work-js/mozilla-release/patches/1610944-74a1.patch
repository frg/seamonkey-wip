# HG changeset patch
# User Ricky Stewart <rstewart@mozilla.com>
# Date 1579799738 0
# Node ID 4a010069572f2266659189d8813f33b39dec53c2
# Parent  fbb3db53e279ae62709389155d62bfdcbe11b2c7
Bug 1610944 - configure/test_toolchain_configure.py and configure/test_toolchain_helpers.py support Python 3 r=firefox-build-system-reviewers,mshal CLOSED TREE

Differential Revision: https://phabricator.services.mozilla.com/D60743

diff --git a/build/moz.configure/rust.configure b/build/moz.configure/rust.configure
--- a/build/moz.configure/rust.configure
+++ b/build/moz.configure/rust.configure
@@ -147,16 +147,17 @@ def rust_triple_alias(host_or_target):
     host_or_target_str = {host: 'host', target: 'target'}[host_or_target]
 
     @depends(rustc, host_or_target, c_compiler, rust_supported_targets,
              arm_target, when=rust_compiler)
     @checking('for rust %s triplet' % host_or_target_str)
     @imports('os')
     @imports(_from='mozbuild.configure.util', _import='LineIO')
     @imports(_from='mozbuild.shellutil', _import='quote')
+    @imports(_from='six', _import='ensure_binary')
     @imports(_from='tempfile', _import='mkstemp')
     @imports(_from='textwrap', _import='dedent')
     def rust_target(rustc, host_or_target, compiler_info,
                     rust_supported_targets, arm_target):
         # Rust's --target options are similar to, but not exactly the same
         # as, the autoconf-derived targets we use.  An example would be that
         # Rust uses distinct target triples for targetting the GNU C++ ABI
         # and the MSVC C++ ABI on Win32, whereas autoconf has a single
@@ -273,26 +274,26 @@ def rust_triple_alias(host_or_target):
 
         if rustc_target is None:
             die("Don't know how to translate {} for rustc".format(
                 host_or_target.alias))
 
         # Check to see whether our rustc has a reasonably functional stdlib
         # for our chosen target.
         target_arg = '--target=' + rustc_target
-        in_fd, in_path = mkstemp(prefix='conftest', suffix='.rs')
+        in_fd, in_path = mkstemp(prefix='conftest', suffix='.rs', text=True)
         out_fd, out_path = mkstemp(prefix='conftest', suffix='.rlib')
         os.close(out_fd)
         try:
             source = 'pub extern fn hello() { println!("Hello world"); }'
             log.debug('Creating `%s` with content:', in_path)
             with LineIO(lambda l: log.debug('| %s', l)) as out:
                 out.write(source)
 
-            os.write(in_fd, source)
+            os.write(in_fd, ensure_binary(source))
             os.close(in_fd)
 
             cmd = [
                 rustc,
                 '--crate-type', 'staticlib',
                 target_arg,
                 '-o', out_path,
                 in_path,
diff --git a/build/moz.configure/toolchain.configure b/build/moz.configure/toolchain.configure
--- a/build/moz.configure/toolchain.configure
+++ b/build/moz.configure/toolchain.configure
@@ -348,16 +348,17 @@ def try_preprocess(compiler, language, s
     return try_invoke_compiler(compiler, language, source, ['-E'])
 
 
 @imports(_from='mozbuild.configure.constants', _import='CompilerType')
 @imports(_from='mozbuild.configure.constants',
          _import='CPU_preprocessor_checks')
 @imports(_from='mozbuild.configure.constants',
          _import='kernel_preprocessor_checks')
+@imports(_from='six', _import='iteritems')
 @imports(_from='textwrap', _import='dedent')
 @imports(_from='__builtin__', _import='Exception')
 def get_compiler_info(compiler, language):
     '''Returns information about the given `compiler` (command line in the
     form of a list or tuple), in the given `language`.
 
     The returned information includes:
     - the compiler type (msvc, clang-cl, clang or gcc)
@@ -406,17 +407,17 @@ def get_compiler_info(compiler, language
 
     # While we're doing some preprocessing, we might as well do some more
     # preprocessor-based tests at the same time, to check the toolchain
     # matches what we want.
     for name, preprocessor_checks in (
         ('CPU', CPU_preprocessor_checks),
         ('KERNEL', kernel_preprocessor_checks),
     ):
-        for n, (value, condition) in enumerate(preprocessor_checks.iteritems()):
+        for n, (value, condition) in enumerate(iteritems(preprocessor_checks)):
             check += dedent('''\
                 #%(if)s %(condition)s
                 %%%(name)s "%(value)s"
             ''' % {
                 'if': 'elif' if n else 'if',
                 'condition': condition,
                 'name': name,
                 'value': value,
@@ -439,17 +440,17 @@ def get_compiler_info(compiler, language
     if not result:
         raise FatalCheckError(
             'Unknown compiler or compiler not supported.')
 
     # Metadata emitted by preprocessors such as GCC with LANG=ja_JP.utf-8 may
     # have non-ASCII characters. Treat the output as bytearray.
     data = {}
     for line in result.splitlines():
-        if line.startswith(b'%'):
+        if line.startswith('%'):
             k, _, v = line.partition(' ')
             k = k.lstrip('%')
             data[k] = v.replace(' ', '').lstrip('"').rstrip('"')
             log.debug('%s = %s', k, data[k])
 
     try:
         type = CompilerType(data['COMPILER'])
     except Exception:
diff --git a/build/moz.configure/util.configure b/build/moz.configure/util.configure
--- a/build/moz.configure/util.configure
+++ b/build/moz.configure/util.configure
@@ -192,38 +192,39 @@ def find_program(file, paths=None):
         paths = pathsep.join(paths)
 
     path = which(file, path=paths, exts=exts)
     return normalize_path(path) if path else None
 
 
 @imports('os')
 @imports(_from='mozbuild.configure.util', _import='LineIO')
+@imports(_from='six', _import='ensure_binary')
 @imports(_from='tempfile', _import='mkstemp')
 def try_invoke_compiler(compiler, language, source, flags=None, onerror=None):
     flags = flags or []
 
     if not isinstance(flags, (list, tuple)):
         die("Flags provided to try_compile must be a list of strings, "
             "not %r", flags)
 
     suffix = {
         'C': '.c',
         'C++': '.cpp',
     }[language]
 
-    fd, path = mkstemp(prefix='conftest.', suffix=suffix)
+    fd, path = mkstemp(prefix='conftest.', suffix=suffix, text=True)
     try:
         source = source.encode('ascii', 'replace')
 
         log.debug('Creating `%s` with content:', path)
         with LineIO(lambda l: log.debug('| %s', l)) as out:
             out.write(source)
 
-        os.write(fd, source)
+        os.write(fd, ensure_binary(source))
         os.close(fd)
         cmd = compiler + [path] + list(flags)
         kwargs = {'onerror': onerror}
         return check_cmd_output(*cmd, **kwargs)
     finally:
         os.remove(path)
 
 
diff --git a/python/mozbuild/mozbuild/preprocessor.py b/python/mozbuild/mozbuild/preprocessor.py
--- a/python/mozbuild/mozbuild/preprocessor.py
+++ b/python/mozbuild/mozbuild/preprocessor.py
@@ -17,17 +17,17 @@ test:
 unary :
   '!'? value ;
 value :
   [0-9]+ # integer
   | 'defined(' \w+ ')'
   | \w+  # string identifier or value;
 """
 
-from __future__ import absolute_import, print_function
+from __future__ import absolute_import, print_function, unicode_literals
 
 import sys
 import os
 import re
 import six
 from optparse import OptionParser
 import errno
 from mozbuild.makeutil import Makefile
@@ -43,16 +43,25 @@ if sys.platform == "win32":
 __all__ = [
   'Context',
   'Expression',
   'Preprocessor',
   'preprocess'
 ]
 
 
+def _to_text(a):
+    # We end up converting a lot of different types (text_type, binary_type,
+    # int, etc.) to Unicode in this script. This function handles all of those
+    # possibilities.
+    if isinstance(a, (six.text_type, six.binary_type)):
+        return six.ensure_text(a)
+    return six.text_type(a)
+
+
 def path_starts_with(path, prefix):
     if os.altsep:
         prefix = prefix.replace(os.altsep, os.sep)
         path = path.replace(os.altsep, os.sep)
     prefix = [os.path.normcase(p) for p in prefix.split(os.sep)]
     path = [os.path.normcase(p) for p in path.split(os.sep)]
     return path[:len(prefix)] == prefix
 
@@ -623,60 +632,57 @@ class Preprocessor:
             return
         val = None
         try:
             e = Expression(args)
             val = e.evaluate(self.context)
         except Exception:
             # XXX do real error reporting
             raise Preprocessor.Error(self, 'SYNTAX_ERR', args)
-        if type(val) == str:
+        if isinstance(val, six.text_type) or isinstance(val, six.binary_type):
             # we're looking for a number value, strings are false
             val = False
         if not val:
             self.disableLevel = 1
         if replace:
             if val:
                 self.disableLevel = 0
             self.ifStates[-1] = self.disableLevel
         else:
             self.ifStates.append(self.disableLevel)
-        pass
 
     def do_ifdef(self, args, replace=False):
         if self.disableLevel and not replace:
             self.disableLevel += 1
             return
         if re.search('\W', args, re.U):
             raise Preprocessor.Error(self, 'INVALID_VAR', args)
         if args not in self.context:
             self.disableLevel = 1
         if replace:
             if args in self.context:
                 self.disableLevel = 0
             self.ifStates[-1] = self.disableLevel
         else:
             self.ifStates.append(self.disableLevel)
-        pass
 
     def do_ifndef(self, args, replace=False):
         if self.disableLevel and not replace:
             self.disableLevel += 1
             return
         if re.search('\W', args, re.U):
             raise Preprocessor.Error(self, 'INVALID_VAR', args)
         if args in self.context:
             self.disableLevel = 1
         if replace:
             if args not in self.context:
                 self.disableLevel = 0
             self.ifStates[-1] = self.disableLevel
         else:
             self.ifStates.append(self.disableLevel)
-        pass
 
     def do_else(self, args, ifState=2):
         self.ensure_not_else()
         hadTrue = self.ifStates[-1] == 0
         self.ifStates[-1] = ifState  # in-else
         if hadTrue:
             self.disableLevel = 1
             return
@@ -710,17 +716,17 @@ class Preprocessor:
             self.ifStates.pop()
 
     # output processing
     def do_expand(self, args):
         lst = re.split('__(\w+)__', args, re.U)
 
         def vsubst(v):
             if v in self.context:
-                return str(self.context[v])
+                return _to_text(self.context[v])
             return ''
         for i in range(1, len(lst), 2):
             lst[i] = vsubst(lst[i])
         lst.append('\n')  # add back the newline
         self.write(six.moves.reduce(lambda x, y: x+y, lst, ''))
 
     def do_literal(self, args):
         self.write(args + '\n')
@@ -765,17 +771,17 @@ class Preprocessor:
     def filter_spaces(self, aLine):
         return re.sub(' +', ' ', aLine).strip(' ')
 
     # substitution: variables wrapped in @ are replaced with their value.
     def filter_substitution(self, aLine, fatal=True):
         def repl(matchobj):
             varname = matchobj.group('VAR')
             if varname in self.context:
-                return str(self.context[varname])
+                return _to_text(self.context[varname])
             if fatal:
                 raise Preprocessor.Error(self, 'UNDEFINED_VAR', varname)
             return matchobj.group(0)
         return self.varsubst.sub(repl, aLine)
 
     # attemptSubstitution: variables wrapped in @ are replaced with their
     # value, or an empty string if the variable is not defined.
     def filter_attemptSubstitution(self, aLine):
@@ -788,26 +794,26 @@ class Preprocessor:
         args can either be a file name, or a file-like object.
         Files should be opened, and will be closed after processing.
         """
         isName = isinstance(args, six.string_types)
         oldCheckLineNumbers = self.checkLineNumbers
         self.checkLineNumbers = False
         if isName:
             try:
-                args = str(args)
+                args = _to_text(args)
                 if filters:
                     args = self.applyFilters(args)
                 if not os.path.isabs(args):
                     args = os.path.join(self.curdir, args)
                 args = open(args, 'rU')
             except Preprocessor.Error:
                 raise
             except Exception:
-                raise Preprocessor.Error(self, 'FILE_NOT_FOUND', str(args))
+                raise Preprocessor.Error(self, 'FILE_NOT_FOUND', _to_text(args))
         self.checkLineNumbers = bool(re.search('\.(js|jsm|java|webidl)(?:\.in)?$', args.name))
         oldFile = self.context['FILE']
         oldLine = self.context['LINE']
         oldDir = self.context['DIRECTORY']
         oldCurdir = self.curdir
         self.noteLineInfo()
 
         if args.isatty():
@@ -839,17 +845,17 @@ class Preprocessor:
         self.context['DIRECTORY'] = oldDir
         self.curdir = oldCurdir
 
     def do_includesubst(self, args):
         args = self.filter_substitution(args)
         self.do_include(args)
 
     def do_error(self, args):
-        raise Preprocessor.Error(self, 'Error: ', str(args))
+        raise Preprocessor.Error(self, 'Error: ', _to_text(args))
 
 
 def preprocess(includes=[sys.stdin], defines={},
                output=sys.stdout,
                marker='#'):
     pp = Preprocessor(defines=defines,
                       marker=marker)
     for f in includes:
diff --git a/python/mozbuild/mozbuild/test/configure/test_toolchain_configure.py b/python/mozbuild/mozbuild/test/configure/test_toolchain_configure.py
--- a/python/mozbuild/mozbuild/test/configure/test_toolchain_configure.py
+++ b/python/mozbuild/mozbuild/test/configure/test_toolchain_configure.py
@@ -1,18 +1,18 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import, print_function, unicode_literals
 
 import logging
 import os
-
-from StringIO import StringIO
+import six
+from six import StringIO
 
 from mozunit import main
 
 from common import BaseConfigureTest
 from mozbuild.configure.util import Version
 from mozbuild.util import (
     memoize,
     ReadOnlyNamespace,
@@ -215,28 +215,28 @@ def CLANG(version):
 def CLANGXX(version):
     return (GCC_BASE('4.2.1') + CLANG_BASE(version) + DEFAULT_CXX_97 +
             SUPPORTS_GNUXX11 + SUPPORTS_GNUXX14)
 
 
 CLANG_3_3 = CLANG('3.3.0') + DEFAULT_C99
 CLANGXX_3_3 = CLANGXX('3.3.0')
 CLANG_4_0 = CLANG('4.0.2') + DEFAULT_C11 + {
-    '__has_attribute(diagnose_if)': '1',
+    '__has_attribute(diagnose_if)': 1,
 }
 CLANGXX_4_0 = CLANGXX('4.0.2') + SUPPORTS_GNUXX1Z + {
-    '__has_attribute(diagnose_if)': '1',
+    '__has_attribute(diagnose_if)': 1,
 }
 CLANG_5_0 = CLANG('5.0.1') + DEFAULT_C11 + {
-    '__has_attribute(diagnose_if)': '1',
-    '__has_warning("-Wunguarded-availability")': '1',
+    '__has_attribute(diagnose_if)': 1,
+    '__has_warning("-Wunguarded-availability")': 1,
 }
 CLANGXX_5_0 = CLANGXX('5.0.1') + SUPPORTS_GNUXX17 + {
-    '__has_attribute(diagnose_if)': '1',
-    '__has_warning("-Wunguarded-availability")': '1',
+    '__has_attribute(diagnose_if)': 1,
+    '__has_warning("-Wunguarded-availability")': 1,
 }
 DEFAULT_CLANG = CLANG_5_0
 DEFAULT_CLANGXX = CLANGXX_5_0
 
 
 def CLANG_PLATFORM(gcc_platform):
     base = {
         '--target=x86_64-linux-gnu': GCC_PLATFORM_X86_64_LINUX[None],
@@ -414,16 +414,17 @@ class BaseToolchainTest(BaseConfigureTes
             if var in results:
                 result = results[var]
             elif var.startswith('host_'):
                 result = results.get(var[5:], {})
             else:
                 result = {}
             try:
                 self.out.truncate(0)
+                self.out.seek(0)
                 compiler = sandbox._value_for(sandbox[var])
                 # Add var on both ends to make it clear which of the
                 # variables is failing the test when that happens.
                 self.assertEquals((var, compiler), (var, result))
             except SystemExit:
                 self.assertEquals((var, result),
                                   (var, self.out.getvalue().strip()))
                 return
@@ -666,17 +667,17 @@ class LinuxToolchainTest(BaseToolchainTe
         }, environ={
             'CXX': 'clang',
         })
 
     def test_clang(self):
         # We'll try gcc and clang, but since there is no gcc (gcc-x.y doesn't
         # count), find clang.
         paths = {
-            k: v for k, v in self.PATHS.iteritems()
+            k: v for k, v in six.iteritems(self.PATHS)
             if os.path.basename(k) not in ('gcc', 'g++')
         }
         self.do_toolchain_test(paths, {
             'c_compiler': self.DEFAULT_CLANG_RESULT,
             'cxx_compiler': self.DEFAULT_CLANGXX_RESULT,
         })
 
     def test_guess_cxx_clang(self):
@@ -703,17 +704,17 @@ class LinuxToolchainTest(BaseToolchainTe
             'CC': 'clang-4.0',
             'CXX': 'clang++-4.0',
         })
 
     def test_no_supported_compiler(self):
         # Even if there are gcc-x.y or clang-x.y compilers available, we
         # don't try them. This could be considered something to improve.
         paths = {
-            k: v for k, v in self.PATHS.iteritems()
+            k: v for k, v in six.iteritems(self.PATHS)
             if os.path.basename(k) not in ('gcc', 'g++', 'clang', 'clang++')
         }
         self.do_toolchain_test(paths, {
             'c_compiler': 'Cannot find the target C compiler',
         })
 
     def test_absolute_path(self):
         paths = dict(self.PATHS)
@@ -908,17 +909,17 @@ class OSXToolchainTest(BaseToolchainTest
         self.do_toolchain_test(self.PATHS, {
             'c_compiler': self.DEFAULT_CLANG_RESULT + self.SYSROOT_FLAGS,
             'cxx_compiler': self.DEFAULT_CLANGXX_RESULT + self.SYSROOT_FLAGS,
         })
 
     def test_not_gcc(self):
         # We won't pick GCC if it's the only thing available.
         paths = {
-            k: v for k, v in self.PATHS.iteritems()
+            k: v for k, v in six.iteritems(self.PATHS)
             if os.path.basename(k) not in ('clang', 'clang++')
         }
         self.do_toolchain_test(paths, {
             'c_compiler': 'Cannot find the target C compiler',
         })
 
     def test_unsupported_clang(self):
         self.do_toolchain_test(self.PATHS, {
@@ -1051,17 +1052,17 @@ class WindowsToolchainTest(BaseToolchain
     GXX_7_RESULT = LinuxToolchainTest.GXX_7_RESULT
     DEFAULT_GCC_RESULT = LinuxToolchainTest.DEFAULT_GCC_RESULT
     DEFAULT_GXX_RESULT = LinuxToolchainTest.DEFAULT_GXX_RESULT
 
     # VS2017u8 or greater is required.
     def test_msvc(self):
         # We'll pick msvc if clang-cl can't be found.
         paths = {
-            k: v for k, v in self.PATHS.iteritems()
+            k: v for k, v in six.iteritems(self.PATHS)
             if os.path.basename(k) != 'clang-cl'
         }
         self.do_toolchain_test(paths, {
             'c_compiler': self.VS_2017u8_RESULT,
             'cxx_compiler': self.VSXX_2017u8_RESULT,
         })
 
     def test_unsupported_msvc(self):
@@ -1124,17 +1125,17 @@ class WindowsToolchainTest(BaseToolchain
         self.do_toolchain_test(self.PATHS, {
             'c_compiler': self.CLANG_CL_8_0_RESULT,
             'cxx_compiler': self.CLANGXX_CL_8_0_RESULT,
         })
 
     def test_gcc(self):
         # We'll pick GCC if msvc and clang-cl can't be found.
         paths = {
-            k: v for k, v in self.PATHS.iteritems()
+            k: v for k, v in six.iteritems(self.PATHS)
             if os.path.basename(k) not in ('cl', 'clang-cl')
         }
         self.do_toolchain_test(paths, {
             'c_compiler': self.DEFAULT_GCC_RESULT,
             'cxx_compiler': self.DEFAULT_GXX_RESULT,
         })
 
     def test_overridden_unsupported_gcc(self):
@@ -1143,17 +1144,17 @@ class WindowsToolchainTest(BaseToolchain
         }, environ={
             'CC': 'gcc-5',
             'CXX': 'g++-5',
         })
 
     def test_clang(self):
         # We'll pick clang if nothing else is found.
         paths = {
-            k: v for k, v in self.PATHS.iteritems()
+            k: v for k, v in six.iteritems(self.PATHS)
             if os.path.basename(k) not in ('cl', 'clang-cl', 'gcc')
         }
         self.do_toolchain_test(paths, {
             'c_compiler': self.DEFAULT_CLANG_RESULT,
             'cxx_compiler': self.DEFAULT_CLANGXX_RESULT,
         })
 
     def test_overridden_unsupported_clang(self):
diff --git a/python/mozbuild/mozbuild/test/configure/test_toolchain_helpers.py b/python/mozbuild/mozbuild/test/configure/test_toolchain_helpers.py
--- a/python/mozbuild/mozbuild/test/configure/test_toolchain_helpers.py
+++ b/python/mozbuild/mozbuild/test/configure/test_toolchain_helpers.py
@@ -66,17 +66,17 @@ class CompilerPreprocessor(Preprocessor)
     class Context(dict):
         def __missing__(self, key):
             return None
 
     def filter_c_substitution(self, line):
         def repl(matchobj):
             varname = matchobj.group('VAR')
             if varname in self.context:
-                result = str(self.context[varname])
+                result = six.text_type(self.context[varname])
                 # The C preprocessor inserts whitespaces around expanded
                 # symbols.
                 start, end = matchobj.span('VAR')
                 if self.NON_WHITESPACE.match(line[start-1:start]):
                     result = ' ' + result
                 if self.NON_WHITESPACE.match(line[end:end+1]):
                     result = result + ' '
                 return result
@@ -122,17 +122,17 @@ class TestCompilerPreprocessor(unittest.
             IF_NOT_C
             #else
             IF_C
             #endif
         '''))
         input.name = 'foo'
         pp.do_include(input)
 
-        self.assertEquals('IFDEF_A\nIF_A\nIF_B\nIF_NOT_C\n', pp.out.getvalue())
+        self.assertEquals('IFDEF_A\nIF_A\nIF_NOT_B\nIF_NOT_C\n', pp.out.getvalue())
 
 
 class FakeCompiler(dict):
     '''Defines a fake compiler for use in toolchain tests below.
 
     The definitions given when creating an instance can have one of two
     forms:
     - a dict giving preprocessor symbols and their respective value, e.g.
diff --git a/python/mozbuild/mozbuild/test/python.ini b/python/mozbuild/mozbuild/test/python.ini
--- a/python/mozbuild/mozbuild/test/python.ini
+++ b/python/mozbuild/mozbuild/test/python.ini
@@ -1,16 +1,18 @@
 [DEFAULT]
 subsuite = mozbuild
 
 [configure/test_checks_configure.py]
 [configure/test_compile_checks.py]
 [configure/test_configure.py]
 [configure/test_moz_configure.py]
 [configure/test_options.py]
+[configure/test_toolchain_configure.py]
+[configure/test_toolchain_helpers.py]
 [configure/test_util.py]
 [controller/test_ccachestats.py]
 [controller/test_clobber.py]
 [test_artifacts.py]
 [test_base.py]
 [test_containers.py]
 [test_dotproperties.py]
 [test_expression.py]
diff --git a/python/mozbuild/mozbuild/test/python2.ini b/python/mozbuild/mozbuild/test/python2.ini
--- a/python/mozbuild/mozbuild/test/python2.ini
+++ b/python/mozbuild/mozbuild/test/python2.ini
@@ -11,16 +11,14 @@ subsuite = mozbuild
 [backend/test_partialconfigenvironment.py]
 [backend/test_recursivemake.py]
 [backend/test_test_manifest.py]
 [backend/test_visualstudio.py]
 [codecoverage/test_lcov_rewrite.py]
 [compilation/test_warnings.py]
 [configure/lint.py]
 [configure/test_lint.py]
-[configure/test_toolchain_configure.py]
-[configure/test_toolchain_helpers.py]
 [configure/test_toolkit_moz_configure.py]
 [frontend/test_context.py]
 [frontend/test_emitter.py]
 [frontend/test_namespaces.py]
 [frontend/test_reader.py]
 [frontend/test_sandbox.py]
