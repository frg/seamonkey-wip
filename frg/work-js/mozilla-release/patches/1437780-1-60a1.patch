# HG changeset patch
# User Lars T Hansen <lhansen@mozilla.com>
# Date 1518505238 -3600
#      Tue Feb 13 08:00:38 2018 +0100
# Node ID 4661476ec0a08a55bfbfe93975a174fa7f31ae68
# Parent  d0d9d2c8d6661151cc4d4d778f1133ee73bb157a
Bug 1437780 - Wasm baseline, remove platform dependencies. r=bbouvier

diff --git a/js/src/jit/MacroAssembler.h b/js/src/jit/MacroAssembler.h
--- a/js/src/jit/MacroAssembler.h
+++ b/js/src/jit/MacroAssembler.h
@@ -1528,28 +1528,33 @@ class MacroAssembler : public MacroAssem
 
     // `ptr` will always be updated.
     void wasmUnalignedStoreI64(const wasm::MemoryAccessDesc& access, Register64 value,
                                Register memoryBase, Register ptr, Register ptrScratch,
                                Register tmp)
         DEFINED_ON(arm);
 
     // wasm specific methods, used in both the wasm baseline compiler and ion.
+
+    // The truncate-to-int32 methods do not bind the rejoin label; clients must
+    // do so if oolWasmTruncateCheckF64ToI32() can jump to it.
     void wasmTruncateDoubleToUInt32(FloatRegister input, Register output, Label* oolEntry) PER_ARCH;
     void wasmTruncateDoubleToInt32(FloatRegister input, Register output, Label* oolEntry) PER_SHARED_ARCH;
     void oolWasmTruncateCheckF64ToI32(FloatRegister input, bool isUnsigned,
                                       wasm::BytecodeOffset off, Label* rejoin)
         DEFINED_ON(arm, arm64, x86_shared);
 
     void wasmTruncateFloat32ToUInt32(FloatRegister input, Register output, Label* oolEntry) PER_ARCH;
     void wasmTruncateFloat32ToInt32(FloatRegister input, Register output, Label* oolEntry) PER_SHARED_ARCH;
     void oolWasmTruncateCheckF32ToI32(FloatRegister input, bool isUnsigned,
                                       wasm::BytecodeOffset off, Label* rejoin)
         DEFINED_ON(arm, arm64, x86_shared);
 
+    // The truncate-to-int64 methods will always bind the `oolRejoin` label
+    // after the last emitted instruction.
     void wasmTruncateDoubleToInt64(FloatRegister input, Register64 output, Label* oolEntry,
                                    Label* oolRejoin, FloatRegister tempDouble)
         DEFINED_ON(arm64, x86, x64);
     void wasmTruncateDoubleToUInt64(FloatRegister input, Register64 output, Label* oolEntry,
                                     Label* oolRejoin, FloatRegister tempDouble)
         DEFINED_ON(arm64, x86, x64);
     void oolWasmTruncateCheckF64ToI64(FloatRegister input, bool isUnsigned,
                                       wasm::BytecodeOffset off, Label* rejoin)
diff --git a/js/src/wasm/WasmBaselineCompile.cpp b/js/src/wasm/WasmBaselineCompile.cpp
--- a/js/src/wasm/WasmBaselineCompile.cpp
+++ b/js/src/wasm/WasmBaselineCompile.cpp
@@ -3586,30 +3586,22 @@ class BaseCompiler final : public BaseCo
       public:
         OutOfLineTruncateCheckF32OrF64ToI32(AnyReg src, bool isUnsigned, BytecodeOffset off)
           : src(src),
             isUnsigned(isUnsigned),
             off(off)
         {}
 
         virtual void generate(MacroAssembler* masm) override {
-#if defined(JS_CODEGEN_X86) || defined(JS_CODEGEN_X64) || defined(JS_CODEGEN_ARM) || \
-    defined(JS_CODEGEN_ARM64)
             if (src.tag == AnyReg::F32)
                 masm->oolWasmTruncateCheckF32ToI32(src.f32(), isUnsigned, off, rejoin());
             else if (src.tag == AnyReg::F64)
                 masm->oolWasmTruncateCheckF64ToI32(src.f64(), isUnsigned, off, rejoin());
             else
                 MOZ_CRASH("unexpected type");
-#else
-            (void)src;
-            (void)isUnsigned;
-            (void)off;
-            MOZ_CRASH("BaseCompiler platform hook: OutOfLineTruncateCheckF32OrF64ToI32 wasm");
-#endif
         }
     };
 
     MOZ_MUST_USE bool truncateF32ToI32(RegF32 src, RegI32 dest, bool isUnsigned) {
         BytecodeOffset off = bytecodeOffset();
         OutOfLineCode* ool =
             addOutOfLineCode(new(alloc_) OutOfLineTruncateCheckF32OrF64ToI32(AnyReg(src),
                                                                              isUnsigned,
@@ -3649,79 +3641,59 @@ class BaseCompiler final : public BaseCo
       public:
         OutOfLineTruncateCheckF32OrF64ToI64(AnyReg src, bool isUnsigned, BytecodeOffset off)
           : src(src),
             isUnsigned(isUnsigned),
             off(off)
         {}
 
         virtual void generate(MacroAssembler* masm) override {
-#if defined(JS_CODEGEN_X86) || defined(JS_CODEGEN_X64) || defined(JS_CODEGEN_ARM) || \
-    defined(JS_CODEGEN_ARM64)
             if (src.tag == AnyReg::F32)
                 masm->oolWasmTruncateCheckF32ToI64(src.f32(), isUnsigned, off, rejoin());
             else if (src.tag == AnyReg::F64)
                 masm->oolWasmTruncateCheckF64ToI64(src.f64(), isUnsigned, off, rejoin());
             else
                 MOZ_CRASH("unexpected type");
-#else
-            (void)src;
-            (void)isUnsigned;
-            (void)off;
-            MOZ_CRASH("BaseCompiler platform hook: OutOfLineTruncateCheckF32OrF64ToI64");
-#endif
         }
     };
 
 #ifndef RABALDR_FLOAT_TO_I64_CALLOUT
     MOZ_MUST_USE RegF64 needTempForFloatingToI64(bool isUnsigned) {
 # if defined(JS_CODEGEN_X86) || defined(JS_CODEGEN_X64)
         if (isUnsigned)
             return needF64();
 # endif
         return RegF64::Invalid();
     }
 
     MOZ_MUST_USE bool truncateF32ToI64(RegF32 src, RegI64 dest, bool isUnsigned, RegF64 temp) {
-# if defined(JS_CODEGEN_X64) || defined(JS_CODEGEN_X86)
-        OutOfLineCode* ool =
-            addOutOfLineCode(new (alloc_) OutOfLineTruncateCheckF32OrF64ToI64(AnyReg(src),
-                                                                              isUnsigned,
-                                                                              bytecodeOffset()));
+        OutOfLineCode* ool = addOutOfLineCode(
+            new (alloc_) OutOfLineTruncateCheckF32OrF64ToI64(AnyReg(src),
+                                                             isUnsigned,
+                                                             bytecodeOffset()));
         if (!ool)
             return false;
         if (isUnsigned)
-            masm.wasmTruncateFloat32ToUInt64(src, dest, ool->entry(),
-                                             ool->rejoin(), temp);
+            masm.wasmTruncateFloat32ToUInt64(src, dest, ool->entry(), ool->rejoin(), temp);
         else
-            masm.wasmTruncateFloat32ToInt64(src, dest, ool->entry(),
-                                            ool->rejoin(), temp);
-# else
-        MOZ_CRASH("BaseCompiler platform hook: truncateF32ToI64");
-# endif
+            masm.wasmTruncateFloat32ToInt64(src, dest, ool->entry(), ool->rejoin(), temp);
         return true;
     }
 
     MOZ_MUST_USE bool truncateF64ToI64(RegF64 src, RegI64 dest, bool isUnsigned, RegF64 temp) {
-# if defined(JS_CODEGEN_X64) || defined(JS_CODEGEN_X86)
-        OutOfLineCode* ool =
-            addOutOfLineCode(new (alloc_) OutOfLineTruncateCheckF32OrF64ToI64(AnyReg(src),
-                                                                              isUnsigned,
-                                                                              bytecodeOffset()));
+        OutOfLineCode* ool = addOutOfLineCode(
+            new (alloc_) OutOfLineTruncateCheckF32OrF64ToI64(AnyReg(src),
+                                                             isUnsigned,
+                                                             bytecodeOffset()));
         if (!ool)
             return false;
         if (isUnsigned)
-            masm.wasmTruncateDoubleToUInt64(src, dest, ool->entry(),
-                                            ool->rejoin(), temp);
+            masm.wasmTruncateDoubleToUInt64(src, dest, ool->entry(), ool->rejoin(), temp);
         else
-            masm.wasmTruncateDoubleToInt64(src, dest, ool->entry(),
-                                           ool->rejoin(), temp);
-# else
-        MOZ_CRASH("BaseCompiler platform hook: truncateF64ToI64");
-# endif
+            masm.wasmTruncateDoubleToInt64(src, dest, ool->entry(), ool->rejoin(), temp);
         return true;
     }
 #endif // RABALDR_FLOAT_TO_I64_CALLOUT
 
 #ifndef RABALDR_I64_TO_FLOAT_CALLOUT
     RegI32 needConvertI64ToFloatTemp(ValType to, bool isUnsigned) {
         bool needs = false;
         if (to == ValType::F64) {
@@ -3765,18 +3737,17 @@ class BaseCompiler final : public BaseCo
 #endif
     }
 
     void eqz64(RegI64 src, RegI32 dest) {
 #ifdef JS_PUNBOX64
         masm.cmpPtrSet(Assembler::Equal, src.reg, ImmWord(0), dest);
 #else
         masm.or32(src.high, src.low);
-        masm.cmp32(src.low, Imm32(0));
-        masm.emitSet(Assembler::Equal, dest);
+        masm.cmp32Set(Assembler::Equal, src.low, Imm32(0), dest);
 #endif
     }
 
     MOZ_MUST_USE bool
     supportsRoundInstruction(RoundingMode mode)
     {
 #if defined(JS_CODEGEN_X64) || defined(JS_CODEGEN_X86)
         return Assembler::HasRoundInstruction(mode);
