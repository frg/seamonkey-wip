# HG changeset patch
# User Jon Coppeard <jcoppeard@mozilla.com>
# Date 1535647913 -3600
#      Thu Aug 30 17:51:53 2018 +0100
# Node ID 8a2005a771b3683c6457df492cde2f90d2ed82c8
# Parent  6615e70815bbfe82c6fb07dab484c7a3cf6f52c6
Bug 1487384 - Remove unnecessary references to Runtime::gc in GCRuntime methods r=sfink

diff --git a/js/src/gc/GC.cpp b/js/src/gc/GC.cpp
--- a/js/src/gc/GC.cpp
+++ b/js/src/gc/GC.cpp
@@ -2527,17 +2527,17 @@ Zone::prepareForCompacting()
 {
     FreeOp* fop = runtimeFromMainThread()->defaultFreeOp();
     discardJitCode(fop);
 }
 
 void
 GCRuntime::sweepTypesAfterCompacting(Zone* zone)
 {
-    zone->beginSweepTypes(rt->gc.releaseObservedTypes && !zone->isPreservingCode());
+    zone->beginSweepTypes(releaseObservedTypes && !zone->isPreservingCode());
 
     AutoClearTypeInferenceStateOnOOM oom(zone);
 
     for (auto script = zone->cellIter<JSScript>(); !script.done(); script.next())
         AutoSweepTypeScript sweep(script, &oom);
     for (auto group = zone->cellIter<ObjectGroup>(); !group.done(); group.next())
         AutoSweepObjectGroup sweep(group, &oom);
 
@@ -2943,17 +2943,17 @@ GCRuntime::updateZonePointersToRelocated
     // Mark roots to update them.
     {
         gcstats::AutoPhase ap(stats(), gcstats::PhaseKind::MARK_ROOTS);
 
         WeakMapBase::traceZone(zone, &trc);
     }
 
     // Sweep everything to fix up weak pointers.
-    rt->gc.sweepZoneAfterCompacting(zone);
+    sweepZoneAfterCompacting(zone);
 
     // Call callbacks to get the rest of the system to fixup other untraced pointers.
     for (CompartmentsInZoneIter comp(zone); !comp.done(); comp.next())
         callWeakPointerCompartmentCallbacks(comp);
 }
 
 /*
  * Update runtime-wide pointers to relocated cells.
@@ -3637,17 +3637,17 @@ GCRuntime::sweepBackgroundThings(ZoneLis
         Arena* next;
         for (Arena* arena = emptyArenas; arena; arena = next) {
             next = arena->next;
 
             // We already calculated the zone's GC trigger after foreground
             // sweeping finished. Now we must update this value.
             arena->zone->threshold.updateForRemovedArena(tunables);
 
-            rt->gc.releaseArena(arena, lock);
+            releaseArena(arena, lock);
             releaseCount++;
             if (releaseCount % LockReleasePeriod == 0) {
                 lock.unlock();
                 lock.lock();
             }
         }
     }
 }
@@ -3758,43 +3758,49 @@ BackgroundSweepTask::run()
 {
     AutoTraceLog logSweeping(TraceLoggerForCurrentThread(), TraceLogger_GCSweeping);
 
     AutoLockHelperThreadState lock;
     AutoSetThreadIsSweeping threadIsSweeping;
 
     MOZ_ASSERT(!done);
 
-    JSRuntime* rt = runtime();
-
-    // The main thread may call queueZonesForBackgroundSweep() while this is
-    // running so we must check there is no more work after releasing the lock.
-    do {
-        ZoneList zones;
-        zones.transferFrom(rt->gc.backgroundSweepZones.ref());
-        LifoAlloc freeLifoAlloc(JSContext::TEMP_LIFO_ALLOC_PRIMARY_CHUNK_SIZE);
-        freeLifoAlloc.transferFrom(&rt->gc.blocksToFreeAfterSweeping.ref());
-
-        AutoUnlockHelperThreadState unlock(lock);
-        rt->gc.sweepBackgroundThings(zones, freeLifoAlloc);
-    } while (!rt->gc.backgroundSweepZones.ref().isEmpty());
+    runtime()->gc.sweepFromBackgroundThread(lock);
 
     // Signal to the main thread that we're finished, because we release the
     // lock again before GCParallelTask's state is changed to finished.
     done = true;
 }
 
 void
+GCRuntime::sweepFromBackgroundThread(AutoLockHelperThreadState& lock)
+{
+    do {
+        ZoneList zones;
+        zones.transferFrom(backgroundSweepZones.ref());
+        LifoAlloc freeLifoAlloc(JSContext::TEMP_LIFO_ALLOC_PRIMARY_CHUNK_SIZE);
+        freeLifoAlloc.transferFrom(&blocksToFreeAfterSweeping.ref());
+
+        AutoUnlockHelperThreadState unlock(lock);
+        sweepBackgroundThings(zones, freeLifoAlloc);
+
+        // The main thread may call queueZonesForBackgroundSweep() while this is
+        // running so we must check there is no more work after releasing the
+        // lock.
+    } while (!backgroundSweepZones.ref().isEmpty());
+}
+
+void
 GCRuntime::waitBackgroundSweepEnd()
 {
     sweepTask.join();
 
     // TODO: Improve assertion to work in incremental GC?
-    if (!rt->gc.isIncrementalGCInProgress())
-        rt->gc.assertBackgroundSweepingFinished();
+    if (!isIncrementalGCInProgress())
+        assertBackgroundSweepingFinished();
 }
 
 bool
 GCRuntime::shouldReleaseObservedTypes()
 {
     bool releaseTypes = false;
 
 #ifdef JS_GC_ZEAL
@@ -3949,17 +3955,17 @@ GCRuntime::deleteEmptyZone(Zone* zone)
 }
 
 void
 GCRuntime::sweepZones(FreeOp* fop, bool destroyingRuntime)
 {
     MOZ_ASSERT_IF(destroyingRuntime, numActiveZoneIters == 0);
     MOZ_ASSERT_IF(destroyingRuntime, arenasEmptyAtShutdown);
 
-    if (rt->gc.numActiveZoneIters)
+    if (numActiveZoneIters)
         return;
 
     assertBackgroundSweepingFinished();
 
     Zone** read = zones().begin();
     Zone** end = zones().end();
     Zone** write = read;
 
@@ -5157,17 +5163,17 @@ GCRuntime::getNextSweepGroup()
  * extra slot of the cross compartment wrapper.
  *
  * The list is created during gray marking when one of the
  * MarkCrossCompartmentXXX functions is called for a pointer that leaves the
  * current compartent group.  This calls DelayCrossCompartmentGrayMarking to
  * push the referring object onto the list.
  *
  * The list is traversed and then unlinked in
- * MarkIncomingCrossCompartmentPointers.
+ * GCRuntime::markIncomingCrossCompartmentPointers.
  */
 
 static bool
 IsGrayListObject(JSObject* obj)
 {
     MOZ_ASSERT(obj);
     return obj->is<CrossCompartmentWrapperObject>() && !IsDeadProxyObject(obj);
 }
@@ -5251,26 +5257,26 @@ js::gc::DelayCrossCompartmentGrayMarking
         if (obj == src)
             found = true;
         obj = NextIncomingCrossCompartmentPointer(obj, false);
     }
     MOZ_ASSERT(found);
 #endif
 }
 
-static void
-MarkIncomingCrossCompartmentPointers(JSRuntime* rt, MarkColor color)
+void
+GCRuntime::markIncomingCrossCompartmentPointers(MarkColor color)
 {
     MOZ_ASSERT(color == MarkColor::Black || color == MarkColor::Gray);
 
     static const gcstats::PhaseKind statsPhases[] = {
         gcstats::PhaseKind::SWEEP_MARK_INCOMING_BLACK,
         gcstats::PhaseKind::SWEEP_MARK_INCOMING_GRAY
     };
-    gcstats::AutoPhase ap1(rt->gc.stats(), statsPhases[unsigned(color)]);
+    gcstats::AutoPhase ap1(stats(), statsPhases[unsigned(color)]);
 
     bool unlinkList = color == MarkColor::Gray;
 
     for (SweepGroupCompartmentsIter c(rt); !c.done(); c.next()) {
         MOZ_ASSERT_IF(color == MarkColor::Gray, c->zone()->isGCMarkingGray());
         MOZ_ASSERT_IF(color == MarkColor::Black, c->zone()->isGCMarkingBlack());
         MOZ_ASSERT_IF(c->gcIncomingGrayPointers, IsGrayListObject(c->gcIncomingGrayPointers));
 
@@ -5278,31 +5284,31 @@ MarkIncomingCrossCompartmentPointers(JSR
              src;
              src = NextIncomingCrossCompartmentPointer(src, unlinkList))
         {
             JSObject* dst = CrossCompartmentPointerReferent(src);
             MOZ_ASSERT(dst->compartment() == c);
 
             if (color == MarkColor::Gray) {
                 if (IsMarkedUnbarriered(rt, &src) && src->asTenured().isMarkedGray())
-                    TraceManuallyBarrieredEdge(&rt->gc.marker, &dst,
+                    TraceManuallyBarrieredEdge(&marker, &dst,
                                                "cross-compartment gray pointer");
             } else {
                 if (IsMarkedUnbarriered(rt, &src) && !src->asTenured().isMarkedGray())
-                    TraceManuallyBarrieredEdge(&rt->gc.marker, &dst,
+                    TraceManuallyBarrieredEdge(&marker, &dst,
                                                "cross-compartment black pointer");
             }
         }
 
         if (unlinkList)
             c->gcIncomingGrayPointers = nullptr;
     }
 
     auto unlimited = SliceBudget::unlimited();
-    MOZ_RELEASE_ASSERT(rt->gc.marker.drainMarkStack(unlimited));
+    MOZ_RELEASE_ASSERT(marker.drainMarkStack(unlimited));
 }
 
 static bool
 RemoveFromGrayList(JSObject* wrapper)
 {
     AutoTouchingGrayThings tgt;
 
     if (!IsGrayListObject(wrapper))
@@ -5389,31 +5395,31 @@ GCRuntime::endMarkingSweepGroup(FreeOp* 
 {
     gcstats::AutoPhase ap(stats(), gcstats::PhaseKind::SWEEP_MARK);
 
     /*
      * Mark any incoming black pointers from previously swept compartments
      * whose referents are not marked. This can occur when gray cells become
      * black by the action of UnmarkGray.
      */
-    MarkIncomingCrossCompartmentPointers(rt, MarkColor::Black);
+    markIncomingCrossCompartmentPointers(MarkColor::Black);
     markWeakReferencesInCurrentGroup(gcstats::PhaseKind::SWEEP_MARK_WEAK);
 
     /*
      * Change state of current group to MarkGray to restrict marking to this
      * group.  Note that there may be pointers to the atoms zone, and
      * these will be marked through, as they are not marked with
      * TraceCrossCompartmentEdge.
      */
     for (SweepGroupZonesIter zone(rt); !zone.done(); zone.next())
         zone->changeGCState(Zone::Mark, Zone::MarkGray);
     marker.setMarkColorGray();
 
     /* Mark incoming gray pointers from previously swept compartments. */
-    MarkIncomingCrossCompartmentPointers(rt, MarkColor::Gray);
+    markIncomingCrossCompartmentPointers(MarkColor::Gray);
 
     /* Mark gray roots and mark transitively inside the current compartment group. */
     markGrayReferencesInCurrentGroup(gcstats::PhaseKind::SWEEP_MARK_GRAY);
     markWeakReferencesInCurrentGroup(gcstats::PhaseKind::SWEEP_MARK_GRAY_WEAK);
 
     /* Restore marking state. */
     for (SweepGroupZonesIter zone(rt); !zone.done(); zone.next())
         zone->changeGCState(Zone::MarkGray, Zone::Mark);
@@ -6955,25 +6961,25 @@ GCRuntime::resetIncrementalGC(gc::AbortR
 
         auto unlimited = SliceBudget::unlimited();
         incrementalCollectSlice(unlimited, JS::gcreason::RESET, session);
 
         isCompacting = wasCompacting;
 
         {
             gcstats::AutoPhase ap(stats(), gcstats::PhaseKind::WAIT_BACKGROUND_THREAD);
-            rt->gc.waitBackgroundSweepOrAllocEnd();
+            waitBackgroundSweepOrAllocEnd();
         }
         break;
       }
 
       case State::Finalize: {
         {
             gcstats::AutoPhase ap(stats(), gcstats::PhaseKind::WAIT_BACKGROUND_THREAD);
-            rt->gc.waitBackgroundSweepOrAllocEnd();
+            waitBackgroundSweepOrAllocEnd();
         }
 
         bool wasCompacting = isCompacting;
         isCompacting = false;
 
         auto unlimited = SliceBudget::unlimited();
         incrementalCollectSlice(unlimited, JS::gcreason::RESET, session);
 
@@ -7427,31 +7433,31 @@ GCRuntime::budgetIncrementalGC(bool noni
 
 namespace {
 
 class AutoScheduleZonesForGC
 {
     JSRuntime* rt_;
 
   public:
-    explicit AutoScheduleZonesForGC(JSRuntime* rt) : rt_(rt) {
-        for (ZonesIter zone(rt, WithAtoms); !zone.done(); zone.next()) {
+    explicit AutoScheduleZonesForGC(GCRuntime* gc) : rt_(gc->rt) {
+        for (ZonesIter zone(rt_, WithAtoms); !zone.done(); zone.next()) {
             if (!zone->canCollect())
                 continue;
 
-            if (rt->gc.gcMode() == JSGC_MODE_GLOBAL)
+            if (gc->gcMode() == JSGC_MODE_GLOBAL)
                 zone->scheduleGC();
 
             // To avoid resets, continue to collect any zones that were being
             // collected in a previous slice.
-            if (rt->gc.isIncrementalGCInProgress() && zone->wasGCStarted())
+            if (gc->isIncrementalGCInProgress() && zone->wasGCStarted())
                 zone->scheduleGC();
 
             // This is a heuristic to reduce the total number of collections.
-            bool inHighFrequencyMode = rt->gc.schedulingState.inHighFrequencyGCMode();
+            bool inHighFrequencyMode = gc->schedulingState.inHighFrequencyGCMode();
             if (zone->usage.gcBytes() >= zone->threshold.eagerAllocTrigger(inHighFrequencyMode))
                 zone->scheduleGC();
 
             // This ensures we collect zones that have reached the malloc limit.
             if (zone->shouldTriggerGCForTooMuchMalloc())
                 zone->scheduleGC();
         }
     }
@@ -7734,17 +7740,17 @@ GCRuntime::collect(bool nonincrementalBy
         return;
 
     stats().writeLogMessage("GC starting in state %s",
         StateName(incrementalState));
 
     AutoTraceLog logGC(TraceLoggerForCurrentThread(), TraceLogger_GC);
     AutoStopVerifyingBarriers av(rt, IsShutdownGC(reason));
     AutoEnqueuePendingParseTasksAfterGC aept(*this);
-    AutoScheduleZonesForGC asz(rt);
+    AutoScheduleZonesForGC asz(this);
 
     bool repeat;
     do {
         IncrementalResult cycleResult = gcCycle(nonincrementalByAPI, budget, reason);
 
         if (reason == JS::gcreason::ABORT_GC) {
             MOZ_ASSERT(!isIncrementalGCInProgress());
             stats().writeLogMessage("GC aborted by request");
@@ -7777,21 +7783,21 @@ GCRuntime::collect(bool nonincrementalBy
             repeat = true;
         }
     } while (repeat);
 
     if (reason == JS::gcreason::COMPARTMENT_REVIVED)
         maybeDoCycleCollection();
 
 #ifdef JS_GC_ZEAL
-    if (rt->hasZealMode(ZealMode::CheckHeapAfterGC)) {
-        gcstats::AutoPhase ap(rt->gc.stats(), gcstats::PhaseKind::TRACE_HEAP);
+    if (hasZealMode(ZealMode::CheckHeapAfterGC)) {
+        gcstats::AutoPhase ap(stats(), gcstats::PhaseKind::TRACE_HEAP);
         CheckHeapAfterGC(rt);
     }
-    if (rt->hasZealMode(ZealMode::CheckGrayMarking) && !isIncrementalGCInProgress()) {
+    if (hasZealMode(ZealMode::CheckGrayMarking) && !isIncrementalGCInProgress()) {
         MOZ_RELEASE_ASSERT(CheckGrayMarkingState(rt));
     }
 #endif
     stats().writeLogMessage("GC ending");
 }
 
 js::AutoEnqueuePendingParseTasksAfterGC::~AutoEnqueuePendingParseTasksAfterGC()
 {
@@ -7954,30 +7960,30 @@ GCRuntime::minorGC(JS::gcreason::Reason 
         return;
 
     // Note that we aren't collecting the updated alloc counts from any helper
     // threads.  We should be but I'm not sure where to add that
     // synchronisation.
     uint32_t numAllocs = rt->mainContextFromOwnThread()->getAndResetAllocsThisZoneSinceMinorGC();
     for (ZonesIter zone(rt, WithAtoms); !zone.done(); zone.next())
         numAllocs += zone->getAndResetTenuredAllocsSinceMinorGC();
-    rt->gc.stats().setAllocsSinceMinorGCTenured(numAllocs);
-
-    gcstats::AutoPhase ap(rt->gc.stats(), phase);
+    stats().setAllocsSinceMinorGCTenured(numAllocs);
+
+    gcstats::AutoPhase ap(stats(), phase);
 
     nursery().clearMinorGCRequest();
     TraceLoggerThread* logger = TraceLoggerForCurrentThread();
     AutoTraceLog logMinorGC(logger, TraceLogger_MinorGC);
     nursery().collect(reason);
     MOZ_ASSERT(nursery().isEmpty());
 
     blocksToFreeAfterMinorGC.ref().freeAll();
 
 #ifdef JS_GC_ZEAL
-    if (rt->hasZealMode(ZealMode::CheckHeapAfterGC))
+    if (hasZealMode(ZealMode::CheckHeapAfterGC))
         CheckHeapAfterGC(rt);
 #endif
 
     {
         AutoLockGC lock(rt);
         for (ZonesIter zone(rt, WithAtoms); !zone.done(); zone.next())
             maybeAllocTriggerZoneGC(zone, lock);
     }
diff --git a/js/src/gc/GCRuntime.h b/js/src/gc/GCRuntime.h
--- a/js/src/gc/GCRuntime.h
+++ b/js/src/gc/GCRuntime.h
@@ -619,16 +619,17 @@ class GCRuntime
     void markAllWeakReferences(gcstats::PhaseKind phase);
     void markAllGrayReferences(gcstats::PhaseKind phase);
 
     void beginSweepPhase(JS::gcreason::Reason reason, AutoGCSession& session);
     void groupZonesForSweeping(JS::gcreason::Reason reason);
     MOZ_MUST_USE bool findInterZoneEdges();
     void getNextSweepGroup();
     IncrementalProgress endMarkingSweepGroup(FreeOp* fop, SliceBudget& budget);
+    void markIncomingCrossCompartmentPointers(MarkColor color);
     IncrementalProgress beginSweepingSweepGroup(FreeOp* fop, SliceBudget& budget);
 #ifdef JS_GC_ZEAL
     IncrementalProgress maybeYieldForSweepingZeal(FreeOp* fop, SliceBudget& budget);
 #endif
     bool shouldReleaseObservedTypes();
     void sweepDebuggerOnMainThread(FreeOp* fop);
     void sweepJitDataOnMainThread(FreeOp* fop);
     IncrementalProgress endSweepingSweepGroup(FreeOp* fop, SliceBudget& budget);
@@ -643,16 +644,17 @@ class GCRuntime
     IncrementalProgress sweepShapeTree(FreeOp* fop, SliceBudget& budget, Zone* zone);
     void endSweepPhase(bool lastGC);
     bool allCCVisibleZonesWereCollected() const;
     void sweepZones(FreeOp* fop, bool destroyingRuntime);
     void decommitAllWithoutUnlocking(const AutoLockGC& lock);
     void startDecommit();
     void queueZonesForBackgroundSweep(ZoneList& zones);
     void maybeStartBackgroundSweep(AutoLockHelperThreadState& lock);
+    void sweepFromBackgroundThread(AutoLockHelperThreadState& lock);
     void sweepBackgroundThings(ZoneList& zones, LifoAlloc& freeBlocks);
     void assertBackgroundSweepingFinished();
     bool shouldCompact();
     void beginCompactPhase();
     IncrementalProgress compactPhase(JS::gcreason::Reason reason, SliceBudget& sliceBudget,
                                      AutoGCSession& session);
     void endCompactPhase();
     void sweepTypesAfterCompacting(Zone* zone);
