# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1526572960 -7200
#      Thu May 17 18:02:40 2018 +0200
# Node ID 77be093ecde94975498c9f5cf51cf97c733dc0a8
# Parent  3e87fd4dacab7e16514087845266d32676a400d2
Bug 1461938 part 1 - Store JS::Realm* instead of JSCompartment* in JSContext. r=luke

diff --git a/dom/base/nsJSUtils.cpp b/dom/base/nsJSUtils.cpp
--- a/dom/base/nsJSUtils.cpp
+++ b/dom/base/nsJSUtils.cpp
@@ -95,17 +95,17 @@ nsJSUtils::CompileFunction(AutoJSAPI& js
                            JS::CompileOptions& aOptions,
                            const nsACString& aName,
                            uint32_t aArgCount,
                            const char** aArgArray,
                            const nsAString& aBody,
                            JSObject** aFunctionObject)
 {
   JSContext* cx = jsapi.cx();
-  MOZ_ASSERT(js::GetEnterCompartmentDepth(cx) > 0);
+  MOZ_ASSERT(js::GetEnterRealmDepth(cx) > 0);
   MOZ_ASSERT_IF(aScopeChain.length() != 0,
                 js::IsObjectInContextCompartment(aScopeChain[0], cx));
 
   // Do the junk Gecko is supposed to do before calling into JSAPI.
   for (size_t i = 0; i < aScopeChain.length(); ++i) {
     JS::ExposeObjectToActiveJS(aScopeChain[i]);
   }
 
diff --git a/js/public/RootingAPI.h b/js/public/RootingAPI.h
--- a/js/public/RootingAPI.h
+++ b/js/public/RootingAPI.h
@@ -17,16 +17,17 @@
 #include <type_traits>
 
 #include "jspubtd.h"
 
 #include "js/GCAnnotations.h"
 #include "js/GCPolicyAPI.h"
 #include "js/HeapAPI.h"
 #include "js/ProfilingStack.h"
+#include "js/Realm.h"
 #include "js/TypeDecls.h"
 #include "js/UniquePtr.h"
 #include "js/Utility.h"
 
 /*
  * Moving GC Stack Rooting
  *
  * A moving GC may change the physical location of GC allocated things, even
@@ -822,18 +823,18 @@ class RootingContext
 
     js::GeckoProfilerThread& geckoProfiler() { return geckoProfiler_; }
 
   protected:
     // The remaining members in this class should only be accessed through
     // JSContext pointers. They are unrelated to rooting and are in place so
     // that inlined API functions can directly access the data.
 
-    /* The current compartment. */
-    JSCompartment*      compartment_;
+    /* The current realm. */
+    JS::Realm*          realm_;
 
     /* The current zone. */
     JS::Zone*           zone_;
 
   public:
     /* Limit pointer for checking native stack consumption. */
     uintptr_t nativeStackLimit[StackKindCount];
 
@@ -1010,17 +1011,17 @@ namespace js {
  *
  * - They're perfectly ordinary JSContext functionality, so ought to be
  *   usable without resorting to jsfriendapi.h, and when JSContext is an
  *   incomplete type.
  */
 inline JSCompartment*
 GetContextCompartment(const JSContext* cx)
 {
-    return JS::RootingContext::get(cx)->compartment_;
+    return GetCompartmentForRealm(JS::RootingContext::get(cx)->realm_);
 }
 
 inline JS::Zone*
 GetContextZone(const JSContext* cx)
 {
     return JS::RootingContext::get(cx)->zone_;
 }
 
diff --git a/js/rust/src/rust.rs b/js/rust/src/rust.rs
--- a/js/rust/src/rust.rs
+++ b/js/rust/src/rust.rs
@@ -606,17 +606,17 @@ impl GCMethods for JS::Value {
     }
 }
 
 // ___________________________________________________________________________
 // Implementations for various things in jsapi.rs
 
 impl Drop for JSAutoRealm {
     fn drop(&mut self) {
-        unsafe { JS::LeaveRealm(self.cx_, self.oldCompartment_); }
+        unsafe { JS::LeaveRealm(self.cx_, self.oldRealm_); }
     }
 }
 
 impl JSJitMethodCallArgs {
     #[inline]
     pub fn get(&self, i: u32) -> JS::HandleValue {
         unsafe {
             if i < self._base.argc_ {
diff --git a/js/src/NamespaceImports.h b/js/src/NamespaceImports.h
--- a/js/src/NamespaceImports.h
+++ b/js/src/NamespaceImports.h
@@ -146,16 +146,17 @@ using JS::UndefinedHandleValue;
 using JS::TrueHandleValue;
 using JS::FalseHandleValue;
 
 using JS::HandleValueArray;
 
 using JS::ObjectOpResult;
 using JS::PropertyResult;
 
+using JS::Realm;
 using JS::Zone;
 
 using JS::Symbol;
 using JS::SymbolCode;
 
 } /* namespace js */
 
 #endif /* NamespaceImports_h */
diff --git a/js/src/fuzz-tests/tests.cpp b/js/src/fuzz-tests/tests.cpp
--- a/js/src/fuzz-tests/tests.cpp
+++ b/js/src/fuzz-tests/tests.cpp
@@ -85,17 +85,17 @@ jsfuzz_init(JSContext** cx, JS::Persiste
     JS::EnterRealm(*cx, *global);
     return true;
 }
 
 static void
 jsfuzz_uninit(JSContext* cx, JSCompartment* oldCompartment)
 {
     if (oldCompartment) {
-        JS::LeaveRealm(cx, oldCompartment);
+        JS::LeaveRealm(cx, JS::GetRealmForCompartment(oldCompartment));
         oldCompartment = nullptr;
     }
     if (cx) {
         JS_EndRequest(cx);
         JS_DestroyContext(cx);
         cx = nullptr;
     }
 }
diff --git a/js/src/jit/BaselineJIT.cpp b/js/src/jit/BaselineJIT.cpp
--- a/js/src/jit/BaselineJIT.cpp
+++ b/js/src/jit/BaselineJIT.cpp
@@ -131,17 +131,17 @@ EnterBaseline(JSContext* cx, EnterJitDat
     EnterJitCode enter = cx->runtime()->jitRuntime()->enterJit();
 
     // Caller must construct |this| before invoking the function.
     MOZ_ASSERT_IF(data.constructing,
                   data.maxArgv[0].isObject() || data.maxArgv[0].isMagic(JS_UNINITIALIZED_LEXICAL));
 
     data.result.setInt32(data.numActualArgs);
     {
-        AssertCompartmentUnchanged pcc(cx);
+        AssertRealmUnchanged aru(cx);
         ActivationEntryMonitor entryMonitor(cx, data.calleeToken);
         JitActivation activation(cx);
 
         data.osrFrame->setRunningInJit();
 
 #ifdef DEBUG
         nogc.reset();
 #endif
diff --git a/js/src/jit/CodeGenerator.cpp b/js/src/jit/CodeGenerator.cpp
--- a/js/src/jit/CodeGenerator.cpp
+++ b/js/src/jit/CodeGenerator.cpp
@@ -2525,18 +2525,18 @@ CodeGenerator::visitRegExpPrototypeOptim
     Register object = ToRegister(ins->object());
     Register output = ToRegister(ins->output());
     Register temp = ToRegister(ins->temp());
 
     OutOfLineRegExpPrototypeOptimizable* ool = new(alloc()) OutOfLineRegExpPrototypeOptimizable(ins);
     addOutOfLineCode(ool, ins->mir());
 
     masm.loadJSContext(temp);
-    masm.loadPtr(Address(temp, JSContext::offsetOfCompartment()), temp);
-    size_t offset = JSCompartment::offsetOfRegExps() +
+    masm.loadPtr(Address(temp, JSContext::offsetOfRealm()), temp);
+    size_t offset = Realm::offsetOfRegExps() +
                     RegExpCompartment::offsetOfOptimizableRegExpPrototypeShape();
     masm.loadPtr(Address(temp, offset), temp);
 
     masm.branchTestObjShapeUnsafe(Assembler::NotEqual, object, temp, ool->entry());
     masm.move32(Imm32(0x1), output);
 
     masm.bind(ool->rejoin());
 }
@@ -2585,18 +2585,18 @@ CodeGenerator::visitRegExpInstanceOptimi
     Register object = ToRegister(ins->object());
     Register output = ToRegister(ins->output());
     Register temp = ToRegister(ins->temp());
 
     OutOfLineRegExpInstanceOptimizable* ool = new(alloc()) OutOfLineRegExpInstanceOptimizable(ins);
     addOutOfLineCode(ool, ins->mir());
 
     masm.loadJSContext(temp);
-    masm.loadPtr(Address(temp, JSContext::offsetOfCompartment()), temp);
-    size_t offset = JSCompartment::offsetOfRegExps() +
+    masm.loadPtr(Address(temp, JSContext::offsetOfRealm()), temp);
+    size_t offset = Realm::offsetOfRegExps() +
                     RegExpCompartment::offsetOfOptimizableRegExpInstanceShape();
     masm.loadPtr(Address(temp, offset), temp);
 
     masm.branchTestObjShapeUnsafe(Assembler::NotEqual, object, temp, ool->entry());
     masm.move32(Imm32(0x1), output);
 
     masm.bind(ool->rejoin());
 }
diff --git a/js/src/jit/Jit.cpp b/js/src/jit/Jit.cpp
--- a/js/src/jit/Jit.cpp
+++ b/js/src/jit/Jit.cpp
@@ -83,17 +83,17 @@ EnterJit(JSContext* cx, RunState& state,
     }
 
     // Caller must construct |this| before invoking the function.
     MOZ_ASSERT_IF(constructing,
                   maxArgv[0].isObject() || maxArgv[0].isMagic(JS_UNINITIALIZED_LEXICAL));
 
     RootedValue result(cx, Int32Value(numActualArgs));
     {
-        AssertCompartmentUnchanged pcc(cx);
+        AssertRealmUnchanged aru(cx);
         ActivationEntryMonitor entryMonitor(cx, calleeToken);
         JitActivation activation(cx);
         EnterJitCode enter = cx->runtime()->jitRuntime()->enterJit();
 
 #ifdef DEBUG
         nogc.reset();
 #endif
         CALL_GENERATED_CODE(enter, code, maxArgc, maxArgv, /* osrFrame = */ nullptr,
diff --git a/js/src/jsapi-tests/tests.cpp b/js/src/jsapi-tests/tests.cpp
--- a/js/src/jsapi-tests/tests.cpp
+++ b/js/src/jsapi-tests/tests.cpp
@@ -28,17 +28,17 @@ bool JSAPITest::init()
         return false;
     JS::EnterRealm(cx, global);
     return true;
 }
 
 void JSAPITest::uninit()
 {
     if (oldCompartment) {
-        JS::LeaveRealm(cx, oldCompartment);
+        JS::LeaveRealm(cx, JS::GetRealmForCompartment(oldCompartment));
         oldCompartment = nullptr;
     }
     if (global) {
         JS::LeaveRealm(cx, nullptr);
         global = nullptr;
     }
     if (cx) {
         JS_EndRequest(cx);
diff --git a/js/src/jsapi.cpp b/js/src/jsapi.cpp
--- a/js/src/jsapi.cpp
+++ b/js/src/jsapi.cpp
@@ -641,71 +641,71 @@ JS_SetExternalStringSizeofCallback(JSCon
 }
 
 JS_PUBLIC_API(JSCompartment*)
 JS::EnterRealm(JSContext* cx, JSObject* target)
 {
     AssertHeapIsIdle();
     CHECK_REQUEST(cx);
 
-    JSCompartment* oldCompartment = cx->compartment();
-    cx->enterCompartmentOf(target);
-    return oldCompartment;
+    Realm* oldRealm = cx->realm();
+    cx->enterRealmOf(target);
+    return JS::GetRealmForCompartment(oldRealm);
 }
 
 JS_PUBLIC_API(void)
-JS::LeaveRealm(JSContext* cx, JSCompartment* oldRealm)
-{
-    AssertHeapIsIdle();
-    CHECK_REQUEST(cx);
-    cx->leaveCompartment(oldRealm);
+JS::LeaveRealm(JSContext* cx, JS::Realm* oldRealm)
+{
+    AssertHeapIsIdle();
+    CHECK_REQUEST(cx);
+    cx->leaveRealm(oldRealm);
 }
 
 JSAutoRealm::JSAutoRealm(JSContext* cx, JSObject* target
                          MOZ_GUARD_OBJECT_NOTIFIER_PARAM_IN_IMPL)
   : cx_(cx),
-    oldCompartment_(cx->compartment())
+    oldRealm_(cx->realm())
 {
     AssertHeapIsIdleOrIterating();
     MOZ_GUARD_OBJECT_NOTIFIER_INIT;
-    cx_->enterCompartmentOf(target);
+    cx_->enterRealmOf(target);
 }
 
 JSAutoRealm::JSAutoRealm(JSContext* cx, JSScript* target
                          MOZ_GUARD_OBJECT_NOTIFIER_PARAM_IN_IMPL)
   : cx_(cx),
-    oldCompartment_(cx->compartment())
+    oldRealm_(cx->realm())
 {
     AssertHeapIsIdleOrIterating();
     MOZ_GUARD_OBJECT_NOTIFIER_INIT;
-    cx_->enterCompartmentOf(target);
+    cx_->enterRealmOf(target);
 }
 
 JSAutoRealm::~JSAutoRealm()
 {
-    cx_->leaveCompartment(oldCompartment_);
+    cx_->leaveRealm(oldRealm_);
 }
 
 JSAutoNullableRealm::JSAutoNullableRealm(JSContext* cx,
                                          JSObject* targetOrNull
                                          MOZ_GUARD_OBJECT_NOTIFIER_PARAM_IN_IMPL)
   : cx_(cx),
-    oldCompartment_(cx->compartment())
+    oldRealm_(cx->realm())
 {
     AssertHeapIsIdleOrIterating();
     MOZ_GUARD_OBJECT_NOTIFIER_INIT;
     if (targetOrNull)
-        cx_->enterCompartmentOf(targetOrNull);
+        cx_->enterRealmOf(targetOrNull);
     else
-        cx_->enterNullCompartment();
+        cx_->enterNullRealm();
 }
 
 JSAutoNullableRealm::~JSAutoNullableRealm()
 {
-    cx_->leaveCompartment(oldCompartment_);
+    cx_->leaveRealm(oldRealm_);
 }
 
 JS_PUBLIC_API(void)
 JS_SetCompartmentPrivate(JSCompartment* compartment, void* data)
 {
     compartment->data = data;
 }
 
diff --git a/js/src/jsapi.h b/js/src/jsapi.h
--- a/js/src/jsapi.h
+++ b/js/src/jsapi.h
@@ -1011,29 +1011,29 @@ JS_RefreshCrossCompartmentWrappers(JSCon
  * the corresponding JS::LeaveRealm call.
  *
  * Entering a realm roots the realm and its global object for the lifetime of
  * the JSAutoRealm.
  */
 class MOZ_RAII JS_PUBLIC_API(JSAutoRealm)
 {
     JSContext* cx_;
-    JSCompartment* oldCompartment_;
+    JS::Realm* oldRealm_;
   public:
     JSAutoRealm(JSContext* cx, JSObject* target MOZ_GUARD_OBJECT_NOTIFIER_PARAM);
     JSAutoRealm(JSContext* cx, JSScript* target MOZ_GUARD_OBJECT_NOTIFIER_PARAM);
     ~JSAutoRealm();
 
     MOZ_DECL_USE_GUARD_OBJECT_NOTIFIER
 };
 
 class MOZ_RAII JS_PUBLIC_API(JSAutoNullableRealm)
 {
     JSContext* cx_;
-    JSCompartment* oldCompartment_;
+    JS::Realm* oldRealm_;
   public:
     explicit JSAutoNullableRealm(JSContext* cx, JSObject* targetOrNull
                                  MOZ_GUARD_OBJECT_NOTIFIER_PARAM);
     ~JSAutoNullableRealm();
 
     MOZ_DECL_USE_GUARD_OBJECT_NOTIFIER
 };
 
@@ -1043,17 +1043,17 @@ namespace JS {
  *
  * Entering a compartment roots the compartment and its global object until the
  * matching JS::LeaveRealm() call.
  */
 extern JS_PUBLIC_API(JSCompartment*)
 EnterRealm(JSContext* cx, JSObject* target);
 
 extern JS_PUBLIC_API(void)
-LeaveRealm(JSContext* cx, JSCompartment* oldRealm);
+LeaveRealm(JSContext* cx, JS::Realm* oldRealm);
 
 } // namespace JS
 
 typedef void (*JSIterateCompartmentCallback)(JSContext* cx, void* data, JSCompartment* compartment);
 
 /**
  * This function calls |compartmentCallback| on every compartment. Beware that
  * there is no guarantee that the compartment will survive after the callback
diff --git a/js/src/jsfriendapi.cpp b/js/src/jsfriendapi.cpp
--- a/js/src/jsfriendapi.cpp
+++ b/js/src/jsfriendapi.cpp
@@ -34,17 +34,17 @@
 #include "vm/NativeObject-inl.h"
 
 using namespace js;
 
 using mozilla::Move;
 using mozilla::PodArrayZero;
 
 JS::RootingContext::RootingContext()
-  : autoGCRooters_(nullptr), compartment_(nullptr), zone_(nullptr)
+  : autoGCRooters_(nullptr), realm_(nullptr), zone_(nullptr)
 {
     for (auto& stackRootPtr : stackRoots_)
         stackRootPtr = nullptr;
 
     PodArrayZero(nativeStackLimit);
 #if JS_STACK_GROWTH_DIRECTION > 0
     for (int i=0; i<StackKindCount; i++)
         nativeStackLimit[i] = UINTPTR_MAX;
@@ -1313,19 +1313,19 @@ js::GetTestingFunctions(JSContext* cx)
     if (!DefineTestingFunctions(cx, obj, false, false))
         return nullptr;
 
     return obj;
 }
 
 #ifdef DEBUG
 JS_FRIEND_API(unsigned)
-js::GetEnterCompartmentDepth(JSContext* cx)
+js::GetEnterRealmDepth(JSContext* cx)
 {
-  return cx->getEnterCompartmentDepth();
+  return cx->getEnterRealmDepth();
 }
 #endif
 
 JS_FRIEND_API(void)
 js::SetDOMCallbacks(JSContext* cx, const DOMCallbacks* callbacks)
 {
     cx->runtime()->DOMcallbacks = callbacks;
 }
diff --git a/js/src/jsfriendapi.h b/js/src/jsfriendapi.h
--- a/js/src/jsfriendapi.h
+++ b/js/src/jsfriendapi.h
@@ -1199,17 +1199,17 @@ CastToJSFreeOp(FreeOp* fop)
  * Get an error type name from a JSExnType constant.
  * Returns nullptr for invalid arguments and JSEXN_INTERNALERR
  */
 extern JS_FRIEND_API(JSFlatString*)
 GetErrorTypeName(JSContext* cx, int16_t exnType);
 
 #ifdef JS_DEBUG
 extern JS_FRIEND_API(unsigned)
-GetEnterCompartmentDepth(JSContext* cx);
+GetEnterRealmDepth(JSContext* cx);
 #endif
 
 extern JS_FRIEND_API(RegExpShared*)
 RegExpToSharedNonInline(JSContext* cx, JS::HandleObject regexp);
 
 /* Implemented in CrossCompartmentWrapper.cpp. */
 typedef enum NukeReferencesToWindow {
     NukeWindowReferences,
diff --git a/js/src/vm/JSCompartment-inl.h b/js/src/vm/JSCompartment-inl.h
--- a/js/src/vm/JSCompartment-inl.h
+++ b/js/src/vm/JSCompartment-inl.h
@@ -41,57 +41,57 @@ JSCompartment::globalIsAboutToBeFinalize
 {
     MOZ_ASSERT(zone_->isGCSweeping());
     return global_ && js::gc::IsAboutToBeFinalizedUnbarriered(global_.unsafeGet());
 }
 
 template <typename T>
 js::AutoRealm::AutoRealm(JSContext* cx, const T& target)
   : cx_(cx),
-    origin_(cx->compartment()),
+    origin_(cx->realm()),
     maybeLock_(nullptr)
 {
-    cx_->enterCompartmentOf(target);
+    cx_->enterRealmOf(target);
 }
 
 // Protected constructor that bypasses assertions in enterCompartmentOf. Used
 // only for entering the atoms compartment.
-js::AutoRealm::AutoRealm(JSContext* cx, JSCompartment* target,
+js::AutoRealm::AutoRealm(JSContext* cx, JS::Realm* target,
                          js::AutoLockForExclusiveAccess& lock)
   : cx_(cx),
-    origin_(cx->compartment()),
+    origin_(cx->realm()),
     maybeLock_(&lock)
 {
     MOZ_ASSERT(target->isAtomsCompartment());
-    cx_->enterAtomsCompartment(target, lock);
+    cx_->enterAtomsRealm(target, lock);
 }
 
 // Protected constructor that bypasses assertions in enterCompartmentOf. Should
 // not be used to enter the atoms compartment.
-js::AutoRealm::AutoRealm(JSContext* cx, JSCompartment* target)
+js::AutoRealm::AutoRealm(JSContext* cx, JS::Realm* target)
   : cx_(cx),
-    origin_(cx->compartment()),
+    origin_(cx->realm()),
     maybeLock_(nullptr)
 {
     MOZ_ASSERT(!target->isAtomsCompartment());
-    cx_->enterNonAtomsCompartment(target);
+    cx_->enterNonAtomsRealm(target);
 }
 
 js::AutoRealm::~AutoRealm()
 {
-    cx_->leaveCompartment(origin_, maybeLock_);
+    cx_->leaveRealm(origin_, maybeLock_);
 }
 
 js::AutoAtomsRealm::AutoAtomsRealm(JSContext* cx,
                                    js::AutoLockForExclusiveAccess& lock)
-  : AutoRealm(cx, cx->atomsCompartment(lock), lock)
+  : AutoRealm(cx, JS::GetRealmForCompartment(cx->atomsCompartment(lock)), lock)
 {}
 
 js::AutoRealmUnchecked::AutoRealmUnchecked(JSContext* cx, JSCompartment* target)
-  : AutoRealm(cx, target)
+  : AutoRealm(cx, JS::GetRealmForCompartment(target))
 {}
 
 inline bool
 JSCompartment::wrap(JSContext* cx, JS::MutableHandleValue vp)
 {
     /* Only GC things have to be wrapped or copied. */
     if (!vp.isGCThing())
         return true;
diff --git a/js/src/vm/JSCompartment.h b/js/src/vm/JSCompartment.h
--- a/js/src/vm/JSCompartment.h
+++ b/js/src/vm/JSCompartment.h
@@ -1206,16 +1206,20 @@ struct JSCompartment
     /*
      * Lists of map and set objects allocated in the nursery or with iterators
      * allocated there. Such objects need to be swept after minor GC.
      */
     js::Vector<js::MapObject*, 0, js::SystemAllocPolicy> mapsWithNurseryMemory;
     js::Vector<js::SetObject*, 0, js::SystemAllocPolicy> setsWithNurseryMemory;
 };
 
+class JS::Realm : public JSCompartment
+{
+};
+
 namespace js {
 
 // We only set the maybeAlive flag for objects and scripts. It's assumed that,
 // if a compartment is alive, then it will have at least some live object or
 // script it in. Even if we get this wrong, the worst that will happen is that
 // scheduledForDestruction will be set on the compartment, which will cause
 // some extra GC activity to try to free the compartment.
 template<typename T> inline void SetMaybeAliveFlag(T* thing) {}
@@ -1223,66 +1227,67 @@ template<> inline void SetMaybeAliveFlag
 template<> inline void SetMaybeAliveFlag(JSScript* thing) {thing->compartment()->maybeAlive = true;}
 
 } // namespace js
 
 inline js::Handle<js::GlobalObject*>
 JSContext::global() const
 {
     /*
-     * It's safe to use |unsafeGet()| here because any compartment that is
+     * It's safe to use |unsafeGet()| here because any realm that is
      * on-stack will be marked automatically, so there's no need for a read
-     * barrier on it. Once the compartment is popped, the handle is no longer
+     * barrier on it. Once the realm is popped, the handle is no longer
      * safe to use.
      */
-    MOZ_ASSERT(compartment_, "Caller needs to enter a compartment first");
-    return js::Handle<js::GlobalObject*>::fromMarkedLocation(compartment_->global_.unsafeGet());
+    MOZ_ASSERT(realm_, "Caller needs to enter a realm first");
+    JSCompartment* comp = GetCompartmentForRealm(realm_);
+    return js::Handle<js::GlobalObject*>::fromMarkedLocation(comp->global_.unsafeGet());
 }
 
 namespace js {
 
-class MOZ_RAII AssertCompartmentUnchanged
+class MOZ_RAII AssertRealmUnchanged
 {
   public:
-    explicit AssertCompartmentUnchanged(JSContext* cx
-                                        MOZ_GUARD_OBJECT_NOTIFIER_PARAM)
-      : cx(cx), oldCompartment(cx->compartment())
+    explicit AssertRealmUnchanged(JSContext* cx
+                                  MOZ_GUARD_OBJECT_NOTIFIER_PARAM)
+      : cx(cx), oldRealm(cx->realm())
     {
         MOZ_GUARD_OBJECT_NOTIFIER_INIT;
     }
 
-    ~AssertCompartmentUnchanged() {
-        MOZ_ASSERT(cx->compartment() == oldCompartment);
+    ~AssertRealmUnchanged() {
+        MOZ_ASSERT(cx->realm() == oldRealm);
     }
 
   protected:
-    JSContext * const cx;
-    JSCompartment * const oldCompartment;
+    JSContext* const cx;
+    JS::Realm* const oldRealm;
     MOZ_DECL_USE_GUARD_OBJECT_NOTIFIER
 };
 
 class AutoRealm
 {
     JSContext* const cx_;
-    JSCompartment* const origin_;
+    JS::Realm* const origin_;
     const AutoLockForExclusiveAccess* maybeLock_;
 
   public:
     template <typename T>
     inline AutoRealm(JSContext* cx, const T& target);
     inline ~AutoRealm();
 
     JSContext* context() const { return cx_; }
-    JSCompartment* origin() const { return origin_; }
+    JS::Realm* origin() const { return origin_; }
 
   protected:
-    inline AutoRealm(JSContext* cx, JSCompartment* target);
+    inline AutoRealm(JSContext* cx, JS::Realm* target);
 
     // Used only for entering the atoms compartment.
-    inline AutoRealm(JSContext* cx, JSCompartment* target,
+    inline AutoRealm(JSContext* cx, JS::Realm* target,
                      AutoLockForExclusiveAccess& lock);
 
   private:
     AutoRealm(const AutoRealm&) = delete;
     AutoRealm& operator=(const AutoRealm&) = delete;
 };
 
 class AutoAtomsRealm : protected AutoRealm
@@ -1399,17 +1404,17 @@ class MOZ_RAII AutoWrapperRooter : priva
 };
 
 class MOZ_RAII AutoSuppressAllocationMetadataBuilder {
     JS::Zone* zone;
     bool saved;
 
   public:
     explicit AutoSuppressAllocationMetadataBuilder(JSContext* cx)
-      : AutoSuppressAllocationMetadataBuilder(cx->compartment()->zone())
+      : AutoSuppressAllocationMetadataBuilder(cx->realm()->zone())
     { }
 
     explicit AutoSuppressAllocationMetadataBuilder(JS::Zone* zone)
       : zone(zone),
         saved(zone->suppressAllocationMetadataBuilder)
     {
         zone->suppressAllocationMetadataBuilder = true;
     }
diff --git a/js/src/vm/JSContext-inl.h b/js/src/vm/JSContext-inl.h
--- a/js/src/vm/JSContext-inl.h
+++ b/js/src/vm/JSContext-inl.h
@@ -434,93 +434,91 @@ JSContext::setPendingException(const js:
 
 inline bool
 JSContext::runningWithTrustedPrincipals()
 {
     return !compartment() || compartment()->principals() == runtime()->trustedPrincipals();
 }
 
 inline void
-JSContext::enterNonAtomsCompartment(JSCompartment* c)
+JSContext::enterNonAtomsRealm(JS::Realm* realm)
 {
-    enterCompartmentDepth_++;
+    enterRealmDepth_++;
 
-    MOZ_ASSERT(!c->zone()->isAtomsZone());
+    MOZ_ASSERT(!realm->zone()->isAtomsZone());
 
-    c->enter();
-    setCompartment(c, nullptr);
+    realm->enter();
+    setRealm(realm, nullptr);
 }
 
 inline void
-JSContext::enterAtomsCompartment(JSCompartment* c,
-                                 const js::AutoLockForExclusiveAccess& lock)
+JSContext::enterAtomsRealm(JS::Realm* realm,
+                           const js::AutoLockForExclusiveAccess& lock)
 {
-    enterCompartmentDepth_++;
+    enterRealmDepth_++;
 
-    MOZ_ASSERT(c->zone()->isAtomsZone());
+    MOZ_ASSERT(realm->zone()->isAtomsZone());
 
-    c->enter();
-    setCompartment(c, &lock);
+    realm->enter();
+    setRealm(realm, &lock);
 }
 
 template <typename T>
 inline void
-JSContext::enterCompartmentOf(const T& target)
+JSContext::enterRealmOf(const T& target)
 {
     MOZ_ASSERT(JS::CellIsNotGray(target));
-    enterNonAtomsCompartment(target->compartment());
+    enterNonAtomsRealm(JS::GetRealmForCompartment(target->compartment()));
 }
 
 inline void
-JSContext::enterNullCompartment()
+JSContext::enterNullRealm()
 {
-    enterCompartmentDepth_++;
-    setCompartment(nullptr);
+    enterRealmDepth_++;
+    setRealm(nullptr);
 }
 
 inline void
-JSContext::leaveCompartment(
-    JSCompartment* oldCompartment,
-    const js::AutoLockForExclusiveAccess* maybeLock /* = nullptr */)
+JSContext::leaveRealm(JS::Realm* oldRealm,
+                      const js::AutoLockForExclusiveAccess* maybeLock /* = nullptr */)
 {
-    MOZ_ASSERT(hasEnteredCompartment());
-    enterCompartmentDepth_--;
+    MOZ_ASSERT(hasEnteredRealm());
+    enterRealmDepth_--;
 
-    // Only call leave() after we've setCompartment()-ed away from the current
-    // compartment.
-    JSCompartment* startingCompartment = compartment_;
-    setCompartment(oldCompartment, maybeLock);
-    if (startingCompartment)
-        startingCompartment->leave();
+    // Only call leave() after we've setRealm()-ed away from the current realm.
+    JS::Realm* startingRealm = realm_;
+    setRealm(oldRealm, maybeLock);
+    if (startingRealm)
+        startingRealm->leave();
 }
 
 inline void
-JSContext::setCompartment(JSCompartment* comp,
-                          const js::AutoLockForExclusiveAccess* maybeLock /* = nullptr */)
+JSContext::setRealm(JS::Realm* realm,
+                    const js::AutoLockForExclusiveAccess* maybeLock /* = nullptr */)
 {
-    // Only one thread can be in the atoms compartment at a time.
-    MOZ_ASSERT_IF(runtime_->isAtomsCompartment(comp), maybeLock != nullptr);
-    MOZ_ASSERT_IF(runtime_->isAtomsCompartment(comp) || runtime_->isAtomsCompartment(compartment_),
+    // Only one thread can be in the atoms realm at a time.
+    MOZ_ASSERT_IF(runtime_->isAtomsCompartment(realm), maybeLock != nullptr);
+    MOZ_ASSERT_IF(runtime_->isAtomsCompartment(realm) || runtime_->isAtomsCompartment(realm_),
                   runtime_->currentThreadHasExclusiveAccess());
 
-    // Make sure that the atoms compartment has its own zone.
-    MOZ_ASSERT_IF(comp && !runtime_->isAtomsCompartment(comp),
-                  !comp->zone()->isAtomsZone());
+    // Make sure that the atoms realm has its own zone.
+    MOZ_ASSERT_IF(realm && !runtime_->isAtomsCompartment(realm),
+                  !realm->zone()->isAtomsZone());
 
-    // Both the current and the new compartment should be properly marked as
+    // Both the current and the new realm should be properly marked as
     // entered at this point.
-    MOZ_ASSERT_IF(compartment_, compartment_->hasBeenEntered());
-    MOZ_ASSERT_IF(comp, comp->hasBeenEntered());
+    MOZ_ASSERT_IF(realm_, realm_->hasBeenEntered());
+    MOZ_ASSERT_IF(realm, realm->hasBeenEntered());
 
     // This thread must have exclusive access to the zone.
-    MOZ_ASSERT_IF(comp && !comp->zone()->isAtomsZone(),
-                  CurrentThreadCanAccessZone(comp->zone()));
+    MOZ_ASSERT_IF(realm && !realm->zone()->isAtomsZone(),
+                  CurrentThreadCanAccessZone(realm->zone()));
 
-    compartment_ = comp;
-    zone_ = comp ? comp->zone() : nullptr;
+    realm_ = realm;
+    zone_ = realm ? realm->zone() : nullptr;
     arenas_ = zone_ ? &zone_->arenas : nullptr;
 }
 
 inline JSScript*
 JSContext::currentScript(jsbytecode** ppc,
                          MaybeAllowCrossCompartment allowCrossCompartment) const
 {
     if (ppc)
diff --git a/js/src/vm/JSContext.cpp b/js/src/vm/JSContext.cpp
--- a/js/src/vm/JSContext.cpp
+++ b/js/src/vm/JSContext.cpp
@@ -1214,17 +1214,17 @@ JSContext::alreadyReportedError()
 }
 
 JSContext::JSContext(JSRuntime* runtime, const JS::ContextOptions& options)
   : runtime_(runtime),
     kind_(ContextKind::HelperThread),
     helperThread_(nullptr),
     options_(options),
     arenas_(nullptr),
-    enterCompartmentDepth_(0),
+    enterRealmDepth_(0),
     jitActivation(nullptr),
     activation_(nullptr),
     profilingActivation_(nullptr),
     nativeStackBase(GetNativeStackBase()),
     entryMonitor(nullptr),
     noExecuteDebuggerTop(nullptr),
     activityCallback(nullptr),
     activityCallbackArg(nullptr),
@@ -1477,18 +1477,18 @@ JSContext::sizeOfExcludingThis(mozilla::
 }
 
 void
 JSContext::trace(JSTracer* trc)
 {
     cycleDetectorVector().trace(trc);
     geckoProfiler().trace(trc);
 
-    if (trc->isMarkingTracer() && compartment_)
-        compartment_->mark();
+    if (trc->isMarkingTracer() && realm_)
+        realm_->mark();
 }
 
 void*
 JSContext::stackLimitAddressForJitCode(JS::StackKind kind)
 {
 #ifdef JS_SIMULATOR
     return addressOfSimulatorStackLimit();
 #else
diff --git a/js/src/vm/JSContext.h b/js/src/vm/JSContext.h
--- a/js/src/vm/JSContext.h
+++ b/js/src/vm/JSContext.h
@@ -139,17 +139,17 @@ struct JSContext : public JS::RootingCon
 
     template <typename T>
     bool isInsideCurrentZone(T thing) const {
         return thing->zoneFromAnyThread() == zone_;
     }
 
     template <typename T>
     inline bool isInsideCurrentCompartment(T thing) const {
-        return thing->compartment() == compartment_;
+        return thing->compartment() == GetCompartmentForRealm(realm_);
     }
 
     void* onOutOfMemory(js::AllocFunction allocFunc, size_t nbytes, void* reallocPtr = nullptr) {
         if (helperThread()) {
             addPendingOutOfMemory();
             return nullptr;
         }
         return runtime_->onOutOfMemory(allocFunc, nbytes, reallocPtr, this);
@@ -183,75 +183,78 @@ struct JSContext : public JS::RootingCon
     uintptr_t stackLimitForJitCode(JS::StackKind kind);
     size_t gcSystemPageSize() { return js::gc::SystemPageSize(); }
     bool jitSupportsFloatingPoint() const { return runtime_->jitSupportsFloatingPoint; }
     bool jitSupportsUnalignedAccesses() const { return runtime_->jitSupportsUnalignedAccesses; }
     bool jitSupportsSimd() const { return runtime_->jitSupportsSimd; }
     bool lcovEnabled() const { return runtime_->lcovOutput().isEnabled(); }
 
     /*
-     * "Entering" a compartment changes cx->compartment (which changes
-     * cx->global). Note that this does not push any InterpreterFrame which means
-     * that it is possible for cx->fp()->compartment() != cx->compartment.
-     * This is not a problem since, in general, most places in the VM cannot
-     * know that they were called from script (e.g., they may have been called
-     * through the JSAPI via JS_CallFunction) and thus cannot expect fp.
+     * "Entering" a realm changes cx->realm (which changes cx->global). Note
+     * that this does not push an Activation so it's possible for the caller's
+     * realm to be != cx->realm(). This is not a problem since, in general, most
+     * places in the VM cannot know that they were called from script (e.g.,
+     * they may have been called through the JSAPI via JS_CallFunction) and thus
+     * cannot expect there is a scripted caller.
      *
-     * Compartments should be entered/left in a LIFO fasion. The depth of this
-     * enter/leave stack is maintained by enterCompartmentDepth_ and queried by
-     * hasEnteredCompartment.
+     * Realms should be entered/left in a LIFO fasion. The depth of this
+     * enter/leave stack is maintained by enterRealmDepth_ and queried by
+     * hasEnteredRealm.
      *
      * To enter a compartment, code should prefer using AutoRealm over
-     * manually calling cx->enterCompartment/leaveCompartment.
+     * manually calling cx->enterRealm/leaveRealm.
      */
   protected:
-    js::ThreadData<unsigned> enterCompartmentDepth_;
+    js::ThreadData<unsigned> enterRealmDepth_;
 
-    inline void setCompartment(JSCompartment* comp,
-                               const js::AutoLockForExclusiveAccess* maybeLock = nullptr);
+    inline void setRealm(JS::Realm* realm,
+                         const js::AutoLockForExclusiveAccess* maybeLock = nullptr);
   public:
-    bool hasEnteredCompartment() const {
-        return enterCompartmentDepth_ > 0;
+    bool hasEnteredRealm() const {
+        return enterRealmDepth_ > 0;
     }
 #ifdef DEBUG
-    unsigned getEnterCompartmentDepth() const {
-        return enterCompartmentDepth_;
+    unsigned getEnterRealmDepth() const {
+        return enterRealmDepth_;
     }
 #endif
 
   private:
-    // We distinguish between entering the atoms compartment and all other
-    // compartments. Entering the atoms compartment requires a lock.
-    inline void enterNonAtomsCompartment(JSCompartment* c);
-    inline void enterAtomsCompartment(JSCompartment* c,
-                                      const js::AutoLockForExclusiveAccess& lock);
+    // We distinguish between entering the atoms realm and all other realms.
+    // Entering the atoms realm requires a lock.
+    inline void enterNonAtomsRealm(JS::Realm* realm);
+    inline void enterAtomsRealm(JS::Realm* realm,
+                                const js::AutoLockForExclusiveAccess& lock);
 
     friend class js::AutoRealm;
 
   public:
     template <typename T>
-    inline void enterCompartmentOf(const T& target);
-    inline void enterNullCompartment();
-    inline void leaveCompartment(JSCompartment* oldCompartment,
-                                 const js::AutoLockForExclusiveAccess* maybeLock = nullptr);
+    inline void enterRealmOf(const T& target);
+    inline void enterNullRealm();
+    inline void leaveRealm(JS::Realm* oldRealm,
+                           const js::AutoLockForExclusiveAccess* maybeLock = nullptr);
 
     void setHelperThread(js::HelperThread* helperThread);
     js::HelperThread* helperThread() const { return helperThread_; }
 
     bool isNurseryAllocSuppressed() const {
         return nurserySuppressions_;
     }
 
     // Threads may freely access any data in their compartment and zone.
     JSCompartment* compartment() const {
-        return compartment_;
+        return JS::GetCompartmentForRealm(realm_);
+    }
+    JS::Realm* realm() const {
+        return realm_;
     }
     JS::Zone* zone() const {
-        MOZ_ASSERT_IF(!compartment(), !zone_);
-        MOZ_ASSERT_IF(compartment(), js::GetCompartmentZone(compartment()) == zone_);
+        MOZ_ASSERT_IF(!realm(), !zone_);
+        MOZ_ASSERT_IF(realm(), js::GetCompartmentZone(GetCompartmentForRealm(realm())) == zone_);
         return zoneRaw();
     }
 
     // For use when the context's zone is being read by another thread and the
     // compartment and zone pointers might not be in sync.
     JS::Zone* zoneRaw() const {
         return zone_;
     }
@@ -302,18 +305,18 @@ struct JSContext : public JS::RootingCon
     // Methods specific to any HelperThread for the context.
     bool addPendingCompileError(js::CompileError** err);
     void addPendingOverRecursed();
     void addPendingOutOfMemory();
 
     JSRuntime* runtime() { return runtime_; }
     const JSRuntime* runtime() const { return runtime_; }
 
-    static size_t offsetOfCompartment() {
-        return offsetof(JSContext, compartment_);
+    static size_t offsetOfRealm() {
+        return offsetof(JSContext, realm_);
     }
 
     friend class JS::AutoSaveExceptionState;
     friend class js::jit::DebugModeOSRVolatileJitFrameIter;
     friend void js::ReportOverRecursed(JSContext*, unsigned errorNumber);
 
   private:
     static JS::Error reportedError;
diff --git a/xpcom/tests/gtest/TestGCPostBarriers.cpp b/xpcom/tests/gtest/TestGCPostBarriers.cpp
--- a/xpcom/tests/gtest/TestGCPostBarriers.cpp
+++ b/xpcom/tests/gtest/TestGCPostBarriers.cpp
@@ -95,17 +95,17 @@ CreateGlobalAndRunTest(JSContext* cx)
     &GlobalClassOps
   };
 
   JS::RealmOptions options;
   JS::PersistentRootedObject global(cx);
   global = JS_NewGlobalObject(cx, &GlobalClass, nullptr, JS::FireOnNewGlobalHook, options);
   ASSERT_TRUE(global != nullptr);
 
-  JSCompartment* oldRealm = JS::EnterRealm(cx, global);
+  JS::Realm* oldRealm = JS::GetRealmForCompartment(JS::EnterRealm(cx, global));
 
   typedef Heap<JSObject*> ElementT;
 
   {
     nsTArray<ElementT>* array = new nsTArray<ElementT>(InitialElements);
     RunTest(cx, array);
     delete array;
   }
