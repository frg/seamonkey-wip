# HG changeset patch
# User Anthony Ramine <n.oxyde@gmail.com>
# Date 1519814238 18000
# Node ID ec4702fa1fc1325eeb5f8db72ae1d47b57233c6e
# Parent  4fb71931ebd3c943c3aae20f7ffba6dd7b8b5c1c
servo: Merge #20141 - Replace ColorOrAuto by CaretColor (from servo:rm-color-or-auto); r=emilio

This is its only use.

Source-Repo: https://github.com/servo/servo
Source-Revision: 8471011e6b6bd72db68ec9ae21df58ab09f2f6d8

diff --git a/servo/components/style/gecko_bindings/sugar/style_complex_color.rs b/servo/components/style/gecko_bindings/sugar/style_complex_color.rs
--- a/servo/components/style/gecko_bindings/sugar/style_complex_color.rs
+++ b/servo/components/style/gecko_bindings/sugar/style_complex_color.rs
@@ -1,18 +1,18 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 //! Rust helpers to interact with Gecko's StyleComplexColor.
 
 use gecko::values::{convert_nscolor_to_rgba, convert_rgba_to_nscolor};
 use gecko_bindings::structs::{nscolor, StyleComplexColor};
-use values::{Auto, Either};
 use values::computed::Color as ComputedColor;
+use values::generics::pointing::CaretColor;
 
 impl From<nscolor> for StyleComplexColor {
     fn from(other: nscolor) -> Self {
         StyleComplexColor {
             mColor: other,
             mForegroundRatio: 0,
             mIsAuto: false,
         }
@@ -54,26 +54,32 @@ impl From<StyleComplexColor> for Compute
         debug_assert!(!other.mIsAuto);
         ComputedColor {
             color: convert_nscolor_to_rgba(other.mColor),
             foreground_ratio: other.mForegroundRatio,
         }
     }
 }
 
-impl From<Either<ComputedColor, Auto>> for StyleComplexColor {
-    fn from(other: Either<ComputedColor, Auto>) -> Self {
+impl<Color> From<CaretColor<Color>> for StyleComplexColor
+where
+    Color: Into<StyleComplexColor>,
+{
+    fn from(other: CaretColor<Color>) -> Self {
         match other {
-            Either::First(color) => color.into(),
-            Either::Second(_auto) => StyleComplexColor::auto(),
+            CaretColor::Color(color) => color.into(),
+            CaretColor::Auto => StyleComplexColor::auto(),
         }
     }
 }
 
-impl From<StyleComplexColor> for Either<ComputedColor, Auto> {
+impl<Color> From<StyleComplexColor> for CaretColor<Color>
+where
+    StyleComplexColor: Into<Color>,
+{
     fn from(other: StyleComplexColor) -> Self {
         if !other.mIsAuto {
-            Either::First(other.into())
+            CaretColor::Color(other.into())
         } else {
-            Either::Second(Auto)
+            CaretColor::Auto
         }
     }
 }
diff --git a/servo/components/style/properties/longhand/pointing.mako.rs b/servo/components/style/properties/longhand/pointing.mako.rs
--- a/servo/components/style/properties/longhand/pointing.mako.rs
+++ b/servo/components/style/properties/longhand/pointing.mako.rs
@@ -38,16 +38,16 @@
                          "none ignore normal select-after select-before select-menu select-same select-all",
                          products="gecko", gecko_ffi_name="mUserFocus",
                          gecko_enum_prefix="StyleUserFocus",
                          animation_value_type="discrete",
                          spec="Nonstandard (https://developer.mozilla.org/en-US/docs/Web/CSS/-moz-user-focus)")}
 
 ${helpers.predefined_type(
     "caret-color",
-    "ColorOrAuto",
-    "Either::Second(Auto)",
+    "CaretColor",
+    "generics::pointing::CaretColor::Auto",
     spec="https://drafts.csswg.org/css-ui/#caret-color",
-    animation_value_type="Either<AnimatedColor, Auto>",
+    animation_value_type="AnimatedCaretColor",
     boxed=not RUSTC_HAS_PR45225,
     ignored_when_colors_disabled=True,
     products="gecko",
 )}
diff --git a/servo/components/style/values/computed/mod.rs b/servo/components/style/values/computed/mod.rs
--- a/servo/components/style/values/computed/mod.rs
+++ b/servo/components/style/values/computed/mod.rs
@@ -65,20 +65,20 @@ pub use super::specified::{BorderStyle, 
 pub use self::length::{CalcLengthOrPercentage, Length, LengthOrNumber, LengthOrPercentage};
 pub use self::length::{LengthOrPercentageOrAuto, LengthOrPercentageOrNone, MaxLength, MozLength};
 pub use self::length::{CSSPixelLength, ExtremumLength, NonNegativeLength, NonNegativeLengthOrPercentage};
 pub use self::list::{ListStyleImage, Quotes};
 #[cfg(feature = "gecko")]
 pub use self::list::ListStyleType;
 pub use self::outline::OutlineStyle;
 pub use self::percentage::Percentage;
-pub use self::position::{GridAutoFlow, GridTemplateAreas, Position, ZIndex};
-pub use self::pointing::Cursor;
+pub use self::pointing::{CaretColor, Cursor};
 #[cfg(feature = "gecko")]
 pub use self::pointing::CursorImage;
+pub use self::position::{GridAutoFlow, GridTemplateAreas, Position, ZIndex};
 pub use self::svg::{SVGLength, SVGOpacity, SVGPaint, SVGPaintKind};
 pub use self::svg::{SVGPaintOrder, SVGStrokeDashArray, SVGWidth};
 pub use self::svg::MozContextProperties;
 pub use self::table::XSpan;
 pub use self::text::{InitialLetter, LetterSpacing, LineHeight, MozTabSize};
 pub use self::text::{TextAlign, TextOverflow, WordSpacing};
 pub use self::time::Time;
 pub use self::transform::{Rotate, Scale, TimingFunction, Transform, TransformOperation};
@@ -628,19 +628,16 @@ impl ClipRectOrAuto {
     pub fn is_auto(&self) -> bool {
         match *self {
             Either::Second(_) => true,
             _ => false
         }
     }
 }
 
-/// <color> | auto
-pub type ColorOrAuto = Either<Color, Auto>;
-
 /// The computed value of a CSS `url()`, resolved relative to the stylesheet URL.
 #[cfg(feature = "servo")]
 #[derive(Clone, Debug, Deserialize, MallocSizeOf, PartialEq, Serialize)]
 pub enum ComputedUrl {
     /// The `url()` was invalid or it wasn't specified by the user.
     Invalid(#[ignore_malloc_size_of = "Arc"] Arc<String>),
     /// The resolved `url()` relative to the stylesheet URL.
     Valid(ServoUrl),
diff --git a/servo/components/style/values/computed/pointing.rs b/servo/components/style/values/computed/pointing.rs
--- a/servo/components/style/values/computed/pointing.rs
+++ b/servo/components/style/values/computed/pointing.rs
@@ -10,25 +10,27 @@ use cssparser::Parser;
 use parser::{Parse, ParserContext};
 use selectors::parser::SelectorParseErrorKind;
 #[cfg(feature = "gecko")]
 use std::fmt::{self, Write};
 #[cfg(feature = "gecko")]
 use style_traits::{CssWriter, ToCss};
 use style_traits::ParseError;
 use style_traits::cursor::CursorKind;
+use values::computed::color::Color;
+use values::generics::pointing::CaretColor as GenericCaretColor;
+#[cfg(feature = "gecko")]
+use values::specified::url::SpecifiedUrl;
 
 /// The computed value for the `cursor` property.
 ///
 /// https://drafts.csswg.org/css-ui/#cursor
 pub use values::specified::pointing::Cursor;
 #[cfg(feature = "gecko")]
 pub use values::specified::pointing::CursorImage;
-#[cfg(feature = "gecko")]
-use values::specified::url::SpecifiedUrl;
 
 impl Cursor {
     /// Set `cursor` to `auto`
     #[cfg(feature = "servo")]
     #[inline]
     pub fn auto() -> Self {
         Cursor(CursorKind::Auto)
     }
@@ -133,8 +135,11 @@ impl ToCss for CursorImage {
             dest.write_str(" ")?;
             x.to_css(dest)?;
             dest.write_str(" ")?;
             y.to_css(dest)?;
         }
         Ok(())
     }
 }
+
+/// A computed value for the `caret-color` property.
+pub type CaretColor = GenericCaretColor<Color>;
diff --git a/servo/components/style/values/generics/mod.rs b/servo/components/style/values/generics/mod.rs
--- a/servo/components/style/values/generics/mod.rs
+++ b/servo/components/style/values/generics/mod.rs
@@ -20,16 +20,17 @@ pub mod column;
 pub mod counters;
 pub mod effects;
 pub mod flex;
 pub mod font;
 #[cfg(feature = "gecko")]
 pub mod gecko;
 pub mod grid;
 pub mod image;
+pub mod pointing;
 pub mod position;
 pub mod rect;
 pub mod size;
 pub mod svg;
 pub mod text;
 pub mod transform;
 
 // https://drafts.csswg.org/css-counter-styles/#typedef-symbols-type
diff --git a/servo/components/style/values/generics/pointing.rs b/servo/components/style/values/generics/pointing.rs
new file mode 100644
--- /dev/null
+++ b/servo/components/style/values/generics/pointing.rs
@@ -0,0 +1,15 @@
+/* This Source Code Form is subject to the terms of the Mozilla Public
+ * License, v. 2.0. If a copy of the MPL was not distributed with this
+ * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
+
+//! Generic values for pointing properties.
+
+/// A generic value for the `caret-color` property.
+#[derive(Animate, Clone, ComputeSquaredDistance, Copy, Debug, MallocSizeOf)]
+#[derive(PartialEq, ToAnimatedValue, ToAnimatedZero, ToComputedValue, ToCss)]
+pub enum CaretColor<Color> {
+    /// An explicit color.
+    Color(Color),
+    /// The keyword `auto`.
+    Auto,
+}
diff --git a/servo/components/style/values/specified/mod.rs b/servo/components/style/values/specified/mod.rs
--- a/servo/components/style/values/specified/mod.rs
+++ b/servo/components/style/values/specified/mod.rs
@@ -58,21 +58,21 @@ pub use self::length::{LengthOrPercentag
 pub use self::length::{NoCalcLength, ViewportPercentageLength};
 pub use self::length::NonNegativeLengthOrPercentage;
 pub use self::list::{ListStyleImage, Quotes};
 #[cfg(feature = "gecko")]
 pub use self::list::ListStyleType;
 pub use self::outline::OutlineStyle;
 pub use self::rect::LengthOrNumberRect;
 pub use self::percentage::Percentage;
+pub use self::pointing::{CaretColor, Cursor};
+#[cfg(feature = "gecko")]
+pub use self::pointing::CursorImage;
 pub use self::position::{GridAutoFlow, GridTemplateAreas, Position};
 pub use self::position::{PositionComponent, ZIndex};
-pub use self::pointing::Cursor;
-#[cfg(feature = "gecko")]
-pub use self::pointing::CursorImage;
 pub use self::svg::{SVGLength, SVGOpacity, SVGPaint, SVGPaintKind};
 pub use self::svg::{SVGPaintOrder, SVGStrokeDashArray, SVGWidth};
 pub use self::svg::MozContextProperties;
 pub use self::table::XSpan;
 pub use self::text::{InitialLetter, LetterSpacing, LineHeight, MozTabSize, TextAlign};
 pub use self::text::{TextAlignKeyword, TextDecorationLine, TextOverflow, WordSpacing};
 pub use self::time::Time;
 pub use self::transform::{Rotate, Scale, TimingFunction, Transform};
@@ -688,19 +688,16 @@ impl ClipRectOrAuto {
         if let Ok(v) = input.try(|i| ClipRect::parse_quirky(context, i, allow_quirks)) {
             Ok(Either::First(v))
         } else {
             Auto::parse(context, input).map(Either::Second)
         }
     }
 }
 
-/// <color> | auto
-pub type ColorOrAuto = Either<Color, Auto>;
-
 /// Whether quirks are allowed in this context.
 #[derive(Clone, Copy, PartialEq)]
 pub enum AllowQuirks {
     /// Quirks are allowed.
     Yes,
     /// Quirks are not allowed.
     No,
 }
diff --git a/servo/components/style/values/specified/pointing.rs b/servo/components/style/values/specified/pointing.rs
--- a/servo/components/style/values/specified/pointing.rs
+++ b/servo/components/style/values/specified/pointing.rs
@@ -1,17 +1,22 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 //! Specified values for Pointing properties.
 //!
 //! https://drafts.csswg.org/css-ui/#pointing-keyboard
 
+use cssparser::Parser;
+use parser::{Parse, ParserContext};
+use style_traits::ParseError;
 use style_traits::cursor::CursorKind;
+use values::generics::pointing::CaretColor as GenericCaretColor;
+use values::specified::color::Color;
 #[cfg(feature = "gecko")]
 use values::specified::url::SpecifiedUrl;
 
 /// The specified value for the `cursor` property.
 ///
 /// https://drafts.csswg.org/css-ui/#cursor
 #[cfg(feature = "servo")]
 #[derive(Clone, Copy, Debug, MallocSizeOf, PartialEq, ToComputedValue, ToCss)]
@@ -33,8 +38,23 @@ pub struct Cursor {
 #[cfg(feature = "gecko")]
 #[derive(Clone, Debug, MallocSizeOf, PartialEq, ToComputedValue)]
 pub struct CursorImage {
     /// The url to parse images from.
     pub url: SpecifiedUrl,
     /// The <x> and <y> coordinates.
     pub hotspot: Option<(f32, f32)>,
 }
+
+/// A specified value for the `caret-color` property.
+pub type CaretColor = GenericCaretColor<Color>;
+
+impl Parse for CaretColor {
+    fn parse<'i, 't>(
+        context: &ParserContext,
+        input: &mut Parser<'i, 't>,
+    ) -> Result<Self, ParseError<'i>> {
+        if input.try(|i| i.expect_ident_matching("auto")).is_ok() {
+            return Ok(GenericCaretColor::Auto);
+        }
+        Ok(GenericCaretColor::Color(Color::parse(context, input)?))
+    }
+}
