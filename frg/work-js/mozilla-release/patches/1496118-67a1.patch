# HG changeset patch
# User Makoto Kato <m_kato@ga2.so-net.ne.jp>
# Date 1549448795 -32400
# Node ID 1b4b115e52529cbd771bbecb681e6eb374c62f8f
# Parent  15dcb1e28d473903b0019456f2e13df946d22982
Bug 1496118 - Clean up caret when destroying editor. r=masayuki

Summary:
Editor changes caret visibility during drag and drop.  But when destroying
editor, we don't restore caret state.  So we should restore it when destroying
editor.

Tags: #secure-revision

Bug #: 1496118

Differential Revision: https://phabricator.services.mozilla.com/D18923

diff --git a/editor/libeditor/EditorEventListener.cpp b/editor/libeditor/EditorEventListener.cpp
--- a/editor/libeditor/EditorEventListener.cpp
+++ b/editor/libeditor/EditorEventListener.cpp
@@ -233,18 +233,19 @@ EditorEventListener::Disconnect()
       mEditorBase->FinalizeSelection();
     }
   }
 
   mEditorBase = nullptr;
 }
 
 void
-EditorEventListener::UninstallFromEditor()
-{
+EditorEventListener::UninstallFromEditor() {
+  CleanupDragDropCaret();
+
   nsCOMPtr<EventTarget> piTarget = mEditorBase->GetDOMEventTarget();
   if (!piTarget) {
     return;
   }
 
   EventListenerManager* elmP = piTarget->GetOrCreateListenerManager();
   if (!elmP) {
     return;
diff --git a/layout/base/tests/bug1496118-ref.html b/layout/base/tests/bug1496118-ref.html
new file mode 100644
--- /dev/null
+++ b/layout/base/tests/bug1496118-ref.html
@@ -0,0 +1,24 @@
+<!DOCTYPE HTML>
+<html class="reftest-wait">
+<head>
+  <script type="text/javascript" src="/tests/SimpleTest/EventUtils.js"></script>
+  <script type="text/javascript" src="/tests/SimpleTest/SimpleTest.js"></script>
+  <style>
+    input, input:active { border: none; }
+  </style>
+</head>
+<body>
+<input id="input1">
+<div id=div1 style="height: 100px;">
+  <textarea id="textarea1" style="display: none;"></textarea>
+</div>
+<script>
+SimpleTest.waitForFocus(() => {
+  let input1 = document.getElementById('input1');
+  input1.focus();
+  synthesizeKey("A");
+  document.documentElement.removeAttribute("class");
+});
+</script>
+</body>
+</html>
diff --git a/layout/base/tests/bug1496118.html b/layout/base/tests/bug1496118.html
new file mode 100644
--- /dev/null
+++ b/layout/base/tests/bug1496118.html
@@ -0,0 +1,37 @@
+<!DOCTYPE HTML>
+<html class="reftest-wait">
+<head>
+  <script type="text/javascript" src="/tests/SimpleTest/EventUtils.js"></script>
+  <script type="text/javascript" src="/tests/SimpleTest/SimpleTest.js"></script>
+  <style>
+    input, input:active { border: none; }
+  </style>
+</head>
+<body>
+<input id="input1" value="aaaaaaaaaaaaaaaaaaaa">
+<div id=div1 style="height: 100px;">
+  <textarea id="textarea1"></textarea>
+</div>
+<script>
+SimpleTest.waitForFocus(() => {
+  let div1 = document.getElementById('div1');
+  let textarea1 = document.getElementById('textarea1');
+  div1.addEventListener("drop", e => {
+    e.preventDefault();
+
+    textarea1.style = "display: none;";
+    SimpleTest.executeSoon(() => {
+      synthesizeKey("A");
+      document.documentElement.removeAttribute("class");
+    });
+  });
+
+  let input1 = document.getElementById('input1');
+  input1.focus();
+  input1.setSelectionRange(0, input1.value.length);
+
+  synthesizeDrop(input1, textarea1, [[{type: "text/plain", data: "foo"}]]);
+});
+</script>
+</body>
+</html>
diff --git a/layout/base/tests/mochitest.ini b/layout/base/tests/mochitest.ini
--- a/layout/base/tests/mochitest.ini
+++ b/layout/base/tests/mochitest.ini
@@ -302,16 +302,18 @@ support-files =
   bug1354478-4.html
   bug1354478-4-ref.html
   bug1354478-5.html
   bug1354478-5-ref.html
   bug1354478-6.html
   bug1354478-6-ref.html
   bug1359411.html
   bug1359411-ref.html
+  bug1496118.html
+  bug1496118-ref.html
   image_rgrg-256x256.png
   input-invalid-ref.html
   input-maxlength-invalid-change.html
   input-maxlength-ui-invalid-change.html
   input-maxlength-ui-valid-change.html
   input-maxlength-valid-before-change.html
   input-maxlength-valid-change.html
   input-minlength-invalid-change.html
diff --git a/layout/base/tests/test_reftests_with_caret.html b/layout/base/tests/test_reftests_with_caret.html
--- a/layout/base/tests/test_reftests_with_caret.html
+++ b/layout/base/tests/test_reftests_with_caret.html
@@ -188,16 +188,17 @@ var tests = [
     [ 'bug1263357-5.html' , 'bug1263357-5-ref.html'] ,
     [ 'bug1354478-1.html' , 'bug1354478-1-ref.html'] ,
     [ 'bug1354478-2.html' , 'bug1354478-2-ref.html'] ,
     [ 'bug1354478-3.html' , 'bug1354478-3-ref.html'] ,
     [ 'bug1354478-4.html' , 'bug1354478-4-ref.html'] ,
     [ 'bug1354478-5.html' , 'bug1354478-5-ref.html'] ,
     [ 'bug1354478-6.html' , 'bug1354478-6-ref.html'] ,
     [ 'bug1359411.html'   , 'bug1359411-ref.html' ] ,
+    [ 'bug1496118.html'   , 'bug1496118-ref.html' ] ,
     function() {SpecialPowers.pushPrefEnv({'clear': [['layout.accessiblecaret.enabled']]}, nextTest);} ,
 ];
 
 if (!navigator.appVersion.includes("Android")) {
   tests.push([ 'bug512295-1.html' , 'bug512295-1-ref.html' ]);
   tests.push([ 'bug512295-2.html' , 'bug512295-2-ref.html' ]);
   tests.push([ 'bug923376.html'   , 'bug923376-ref.html'   ]);
   tests.push(function() {SpecialPowers.pushPrefEnv({'set': [['layout.css.overflow-clip-box.enabled', true]]}, nextTest);});
