# HG changeset patch
# User Kartikaya Gupta <kgupta@mozilla.com>
# Date 1517520529 18000
# Node ID b3e3d4a0fd588b7cd4c2783a36fd6104824b1d7b
# Parent  70ac3be15a7d634381558410669f9a7f0cdd2556
Bug 1435022 - Inline OnForceComposeToTarget and clarify the comment. r=sotaro

This function has only one call site and there's no need to make it
public.

MozReview-Commit-ID: 5o7B9KMyVqy

diff --git a/gfx/layers/ipc/CompositorVsyncScheduler.cpp b/gfx/layers/ipc/CompositorVsyncScheduler.cpp
--- a/gfx/layers/ipc/CompositorVsyncScheduler.cpp
+++ b/gfx/layers/ipc/CompositorVsyncScheduler.cpp
@@ -284,37 +284,37 @@ CompositorVsyncScheduler::Composite(Time
     mozilla::Telemetry::Accumulate(mozilla::Telemetry::COMPOSITE_FRAME_ROUNDTRIP_TIME,
                                    compositeFrameTotal.ToMilliseconds());
   } else if (mVsyncNotificationsSkipped++ > gfxPrefs::CompositorUnobserveCount()) {
     UnobserveVsync();
   }
 }
 
 void
-CompositorVsyncScheduler::OnForceComposeToTarget()
-{
-  /**
-   * bug 1138502 - There are cases such as during long-running window resizing events
-   * where we receive many sync RecvFlushComposites. We also get vsync notifications which
-   * will increment mVsyncNotificationsSkipped because a composite just occurred. After
-   * enough vsyncs and RecvFlushComposites occurred, we will disable vsync. Then at the next
-   * ScheduleComposite, we will enable vsync, then get a RecvFlushComposite, which will
-   * force us to unobserve vsync again. On some platforms, enabling/disabling vsync is not
-   * free and this oscillating behavior causes a performance hit. In order to avoid this problem,
-   * we reset the mVsyncNotificationsSkipped counter to keep vsync enabled.
-   */
-  MOZ_ASSERT(CompositorThreadHolder::IsInCompositorThread());
-  mVsyncNotificationsSkipped = 0;
-}
-
-void
 CompositorVsyncScheduler::ForceComposeToTarget(gfx::DrawTarget* aTarget, const IntRect* aRect)
 {
   MOZ_ASSERT(CompositorThreadHolder::IsInCompositorThread());
-  OnForceComposeToTarget();
+
+  /**
+   * bug 1138502 - There are cases such as during long-running window resizing
+   * events where we receive many force-composites. We also continue to get
+   * vsync notifications. Because the force-composites trigger compositing and
+   * clear the mNeedsComposite counter, the vsync notifications will not need
+   * to do anything and so will increment the mVsyncNotificationsSkipped counter
+   * to indicate the vsync was ignored. If this happens enough times, we will
+   * disable listening for vsync entirely. On the next force-composite we will
+   * enable listening for vsync again, and continued force-composites and vsyncs
+   * will cause oscillation between observing vsync and not.
+   * On some platforms, enabling/disabling vsync is not free and this
+   * oscillating behavior causes a performance hit. In order to avoid this
+   * problem, we reset the mVsyncNotificationsSkipped counter to keep vsync
+   * enabled.
+   */
+  mVsyncNotificationsSkipped = 0;
+
   mLastCompose = TimeStamp::Now();
   ComposeToTarget(aTarget, aRect);
 }
 
 bool
 CompositorVsyncScheduler::NeedsComposite()
 {
   MOZ_ASSERT(CompositorThreadHolder::IsInCompositorThread());
diff --git a/gfx/layers/ipc/CompositorVsyncScheduler.h b/gfx/layers/ipc/CompositorVsyncScheduler.h
--- a/gfx/layers/ipc/CompositorVsyncScheduler.h
+++ b/gfx/layers/ipc/CompositorVsyncScheduler.h
@@ -45,17 +45,16 @@ class CompositorVsyncScheduler
   NS_INLINE_DECL_THREADSAFE_REFCOUNTING(CompositorVsyncScheduler)
 
 public:
   explicit CompositorVsyncScheduler(CompositorVsyncSchedulerOwner* aVsyncSchedulerOwner,
                                     widget::CompositorWidget* aWidget);
 
   bool NotifyVsync(TimeStamp aVsyncTimestamp);
   void SetNeedsComposite();
-  void OnForceComposeToTarget();
 
   void ScheduleTask(already_AddRefed<CancelableRunnable>, int);
   void ResumeComposition();
   void ComposeToTarget(gfx::DrawTarget* aTarget, const gfx::IntRect* aRect = nullptr);
   void PostCompositeTask(TimeStamp aCompositeTimestamp);
   void PostVRTask(TimeStamp aTimestamp);
   void Destroy();
   void ScheduleComposition();
