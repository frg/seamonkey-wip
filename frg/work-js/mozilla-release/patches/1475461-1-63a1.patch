# HG changeset patch
# User Masayuki Nakano <masayuki@d-toybox.com>
# Date 1531468589 -32400
# Node ID 9f8a3c2f65288f91b94223b69c63da6612ebee55
# Parent  7ce64da43ceb50ea7ea358126332cb86765eeb25
Bug 1475461 - part 1: Mark PLDHashTable::Search() and called by it as const r=Ehsan

PLDHashTable::Search() does not modify any members.  So, this method and
methods called by it should be marked as const.

MozReview-Commit-ID: 6g4jrYK1j9E

diff --git a/xpcom/ds/PLDHashTable.cpp b/xpcom/ds/PLDHashTable.cpp
--- a/xpcom/ds/PLDHashTable.cpp
+++ b/xpcom/ds/PLDHashTable.cpp
@@ -251,24 +251,24 @@ PLDHashTable::operator=(PLDHashTable&& a
 #endif
     aOther.mEntryStore.Set(nullptr, &aOther.mGeneration);
   }
 
   return *this;
 }
 
 PLDHashNumber
-PLDHashTable::Hash1(PLDHashNumber aHash0)
+PLDHashTable::Hash1(PLDHashNumber aHash0) const
 {
   return aHash0 >> mHashShift;
 }
 
 void
 PLDHashTable::Hash2(PLDHashNumber aHash0,
-                    uint32_t& aHash2Out, uint32_t& aSizeMaskOut)
+                    uint32_t& aHash2Out, uint32_t& aSizeMaskOut) const
 {
   uint32_t sizeLog2 = kHashBits - mHashShift;
   uint32_t sizeMask = (PLDHashNumber(1) << sizeLog2) - 1;
   aSizeMaskOut = sizeMask;
 
   // The incoming aHash0 always has the low bit unset (since we leave it
   // free for the collision flag), and should have reasonably random
   // data in the other 31 bits.  We used the high bits of aHash0 for
@@ -288,27 +288,29 @@ PLDHashTable::Hash2(PLDHashNumber aHash0
 // that a removed-entry sentinel need be stored only if the removed entry had
 // a colliding entry added after it. Therefore we can use 1 as the collision
 // flag in addition to the removed-entry sentinel value. Multiplicative hash
 // uses the high order bits of mKeyHash, so this least-significant reservation
 // should not hurt the hash function's effectiveness much.
 
 // Match an entry's mKeyHash against an unstored one computed from a key.
 /* static */ bool
-PLDHashTable::MatchEntryKeyhash(PLDHashEntryHdr* aEntry, PLDHashNumber aKeyHash)
+PLDHashTable::MatchEntryKeyhash(const PLDHashEntryHdr* aEntry,
+                                const PLDHashNumber aKeyHash)
 {
   return (aEntry->mKeyHash & ~kCollisionFlag) == aKeyHash;
 }
 
 // Compute the address of the indexed entry in table.
 PLDHashEntryHdr*
-PLDHashTable::AddressEntry(uint32_t aIndex)
+PLDHashTable::AddressEntry(uint32_t aIndex) const
 {
-  return reinterpret_cast<PLDHashEntryHdr*>(
-    mEntryStore.Get() + aIndex * mEntrySize);
+  return const_cast<PLDHashEntryHdr*>(
+    reinterpret_cast<const PLDHashEntryHdr*>(
+      mEntryStore.Get() + aIndex * mEntrySize));
 }
 
 PLDHashTable::~PLDHashTable()
 {
 #ifdef DEBUG
   AutoDestructorOp op(mChecker);
 #endif
 
@@ -349,17 +351,17 @@ PLDHashTable::Clear()
 
 // If |Reason| is |ForAdd|, the return value is always non-null and it may be
 // a previously-removed entry. If |Reason| is |ForSearchOrRemove|, the return
 // value is null on a miss, and will never be a previously-removed entry on a
 // hit. This distinction is a bit grotty but this function is hot enough that
 // these differences are worthwhile.
 template <PLDHashTable::SearchReason Reason>
 PLDHashEntryHdr* NS_FASTCALL
-PLDHashTable::SearchTable(const void* aKey, PLDHashNumber aKeyHash)
+PLDHashTable::SearchTable(const void* aKey, PLDHashNumber aKeyHash) const
 {
   MOZ_ASSERT(mEntryStore.Get());
   NS_ASSERTION(!(aKeyHash & kCollisionFlag),
                "!(aKeyHash & kCollisionFlag)");
 
   // Compute the primary hash address.
   PLDHashNumber hash1 = Hash1(aKeyHash);
   PLDHashEntryHdr* entry = AddressEntry(hash1);
@@ -416,17 +418,17 @@ PLDHashTable::SearchTable(const void* aK
 // This is a copy of SearchTable(), used by ChangeTable(), hardcoded to
 //   1. assume |Reason| is |ForAdd|,
 //   2. assume that |aKey| will never match an existing entry, and
 //   3. assume that no entries have been removed from the current table
 //      structure.
 // Avoiding the need for |aKey| means we can avoid needing a way to map entries
 // to keys, which means callers can use complex key types more easily.
 MOZ_ALWAYS_INLINE PLDHashEntryHdr*
-PLDHashTable::FindFreeEntry(PLDHashNumber aKeyHash)
+PLDHashTable::FindFreeEntry(PLDHashNumber aKeyHash) const
 {
   MOZ_ASSERT(mEntryStore.Get());
   NS_ASSERTION(!(aKeyHash & kCollisionFlag),
                "!(aKeyHash & kCollisionFlag)");
 
   // Compute the primary hash address.
   PLDHashNumber hash1 = Hash1(aKeyHash);
   PLDHashEntryHdr* entry = AddressEntry(hash1);
@@ -506,34 +508,34 @@ PLDHashTable::ChangeTable(int32_t aDelta
     oldEntryAddr += mEntrySize;
   }
 
   free(oldEntryStore);
   return true;
 }
 
 MOZ_ALWAYS_INLINE PLDHashNumber
-PLDHashTable::ComputeKeyHash(const void* aKey)
+PLDHashTable::ComputeKeyHash(const void* aKey) const
 {
   MOZ_ASSERT(mEntryStore.Get());
 
   PLDHashNumber keyHash = mOps->hashKey(aKey);
   keyHash *= kGoldenRatio;
 
   // Avoid 0 and 1 hash codes, they indicate free and removed entries.
   if (keyHash < 2) {
     keyHash -= 2;
   }
   keyHash &= ~kCollisionFlag;
 
   return keyHash;
 }
 
 PLDHashEntryHdr*
-PLDHashTable::Search(const void* aKey)
+PLDHashTable::Search(const void* aKey) const
 {
 #ifdef DEBUG
   AutoReadOp op(mChecker);
 #endif
 
   PLDHashEntryHdr* entry = mEntryStore.Get()
                          ? SearchTable<ForSearchOrRemove>(aKey,
                                                           ComputeKeyHash(aKey))
diff --git a/xpcom/ds/PLDHashTable.h b/xpcom/ds/PLDHashTable.h
--- a/xpcom/ds/PLDHashTable.h
+++ b/xpcom/ds/PLDHashTable.h
@@ -320,17 +320,17 @@ public:
   uint32_t Generation() const { return mGeneration; }
 
   // To search for a |key| in |table|, call:
   //
   //   entry = table.Search(key);
   //
   // If |entry| is non-null, |key| was found. If |entry| is null, key was not
   // found.
-  PLDHashEntryHdr* Search(const void* aKey);
+  PLDHashEntryHdr* Search(const void* aKey) const;
 
   // To add an entry identified by |key| to table, call:
   //
   //   entry = table.Add(key, mozilla::fallible);
   //
   // If |entry| is null upon return, then the table is severely overloaded and
   // memory can't be allocated for entry storage.
   //
@@ -502,60 +502,62 @@ private:
   // expressed as a fixed-point 32-bit fraction.
   static const uint32_t kHashBits = 32;
   static const uint32_t kGoldenRatio = 0x9E3779B9U;
 
   static uint32_t HashShift(uint32_t aEntrySize, uint32_t aLength);
 
   static const PLDHashNumber kCollisionFlag = 1;
 
-  static bool EntryIsFree(PLDHashEntryHdr* aEntry)
+  static bool EntryIsFree(const PLDHashEntryHdr* aEntry)
   {
     return aEntry->mKeyHash == 0;
   }
-  static bool EntryIsRemoved(PLDHashEntryHdr* aEntry)
+  static bool EntryIsRemoved(const PLDHashEntryHdr* aEntry)
   {
     return aEntry->mKeyHash == 1;
   }
-  static bool EntryIsLive(PLDHashEntryHdr* aEntry)
+  static bool EntryIsLive(const PLDHashEntryHdr* aEntry)
   {
     return aEntry->mKeyHash >= 2;
   }
 
   static void MarkEntryFree(PLDHashEntryHdr* aEntry)
   {
     aEntry->mKeyHash = 0;
   }
   static void MarkEntryRemoved(PLDHashEntryHdr* aEntry)
   {
     aEntry->mKeyHash = 1;
   }
 
-  PLDHashNumber Hash1(PLDHashNumber aHash0);
-  void Hash2(PLDHashNumber aHash, uint32_t& aHash2Out, uint32_t& aSizeMaskOut);
+  PLDHashNumber Hash1(PLDHashNumber aHash0) const;
+  void Hash2(PLDHashNumber aHash,
+             uint32_t& aHash2Out, uint32_t& aSizeMaskOut) const;
 
-  static bool MatchEntryKeyhash(PLDHashEntryHdr* aEntry, PLDHashNumber aHash);
-  PLDHashEntryHdr* AddressEntry(uint32_t aIndex);
+  static bool MatchEntryKeyhash(const PLDHashEntryHdr* aEntry,
+                                const PLDHashNumber aHash);
+  PLDHashEntryHdr* AddressEntry(uint32_t aIndex) const;
 
   // We store mHashShift rather than sizeLog2 to optimize the collision-free
   // case in SearchTable.
   uint32_t CapacityFromHashShift() const
   {
     return ((uint32_t)1 << (kHashBits - mHashShift));
   }
 
-  PLDHashNumber ComputeKeyHash(const void* aKey);
+  PLDHashNumber ComputeKeyHash(const void* aKey) const;
 
   enum SearchReason { ForSearchOrRemove, ForAdd };
 
   template <SearchReason Reason>
   PLDHashEntryHdr* NS_FASTCALL
-    SearchTable(const void* aKey, PLDHashNumber aKeyHash);
+    SearchTable(const void* aKey, PLDHashNumber aKeyHash) const;
 
-  PLDHashEntryHdr* FindFreeEntry(PLDHashNumber aKeyHash);
+  PLDHashEntryHdr* FindFreeEntry(PLDHashNumber aKeyHash) const;
 
   bool ChangeTable(int aDeltaLog2);
 
   void ShrinkIfAppropriate();
 
   PLDHashTable(const PLDHashTable& aOther) = delete;
   PLDHashTable& operator=(const PLDHashTable& aOther) = delete;
 };

