# HG changeset patch
# User Matthew Gregan <kinetik@flim.org>
# Date 1545149274 -7200
# Node ID 3cf80bcaa78cf6186312d27811fa876d5b2b1b8f
# Parent  6f67ea02ace5b8e93cd33df8d01164d8a84ccfbb
Bug 1512450 - Clean up reslicing of audio buffers in AudioIPC's server data callback. r=chunmin

diff --git a/media/audioipc/server/src/server.rs b/media/audioipc/server/src/server.rs
--- a/media/audioipc/server/src/server.rs
+++ b/media/audioipc/server/src/server.rs
@@ -80,39 +80,41 @@ struct ServerStreamCallbacks {
     /// RPC interface to callback server running in client
     rpc: rpc::ClientProxy<CallbackReq, CallbackResp>,
 }
 
 impl ServerStreamCallbacks {
     fn data_callback(&mut self, input: &[u8], output: &mut [u8]) -> isize {
         trace!("Stream data callback: {} {}", input.len(), output.len());
 
-        // len is of input and output is frame len. Turn these into the real lengths.
+        // FFI wrapper (data_cb_c) converted buffers to [u8] slices but len is frames *not* bytes.
+        // Convert slices to correct length now we have {input,output}_frame_size available.
         let real_input = unsafe {
             let nbytes = input.len() * self.input_frame_size as usize;
             slice::from_raw_parts(input.as_ptr(), nbytes)
         };
+        let real_output = unsafe {
+            let nbytes = output.len() * self.output_frame_size as usize;
+            slice::from_raw_parts_mut(output.as_mut_ptr(), nbytes)
+        };
 
         self.input_shm.write(real_input).unwrap();
 
         let r = self
             .rpc
             .call(CallbackReq::Data(
                 output.len() as isize,
                 self.output_frame_size as usize,
             )).wait();
 
         match r {
             Ok(CallbackResp::Data(frames)) => {
                 if frames >= 0 {
                     let nbytes = frames as usize * self.output_frame_size as usize;
-                    let real_output = unsafe {
-                        trace!("Resize output to {}", nbytes);
-                        slice::from_raw_parts_mut(output.as_mut_ptr(), nbytes)
-                    };
+                    trace!("Reslice output to {}", nbytes);
                     self.output_shm.read(&mut real_output[..nbytes]).unwrap();
                 }
                 frames
             }
             _ => {
                 debug!("Unexpected message {:?} during data_callback", r);
                 -1
             }

