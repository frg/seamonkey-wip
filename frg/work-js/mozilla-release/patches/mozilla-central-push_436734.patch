# HG changeset patch
# User Jeff Walden <jwalden@mit.edu>
# Date 1536961703 25200
#      Fri Sep 14 14:48:23 2018 -0700
# Node ID 48dd64a6036c69b78e1fd710d33a55b2c835f8de
# Parent  34cc4a8c46c078108736af48dcc767bb9b6f994f
Bug 1491137 - Rename the JS::Evaluate that accepts a filename and is always used to process UTF-8 source text to JS::EvaluateUtf8Path.  r=jandem

diff --git a/js/public/CompilationAndEvaluation.h b/js/public/CompilationAndEvaluation.h
--- a/js/public/CompilationAndEvaluation.h
+++ b/js/public/CompilationAndEvaluation.h
@@ -121,21 +121,27 @@ Evaluate(JSContext* cx, AutoVector<JSObj
 /**
  * Evaluate the given byte buffer in the scope of the current global of cx.
  */
 extern JS_PUBLIC_API(bool)
 Evaluate(JSContext* cx, const ReadOnlyCompileOptions& options,
          const char* bytes, size_t length, MutableHandle<Value> rval);
 
 /**
- * Evaluate the given file in the scope of the current global of cx.
+ * Evaluate the UTF-8 contents of the file at the given path, and return the
+ * completion value in |rval|.  (The path itself is in the system encoding, not
+ * [necessarily] UTF-8.)  If the contents contain any malformed UTF-8, an error
+ * is reported.
+ *
+ * The |options.utf8| flag is asserted to be true.  At a future time this flag
+ * will be removed.
  */
 extern JS_PUBLIC_API(bool)
-Evaluate(JSContext* cx, const ReadOnlyCompileOptions& options,
-         const char* filename, MutableHandle<Value> rval);
+EvaluateUtf8Path(JSContext* cx, const ReadOnlyCompileOptions& options,
+                 const char* filename, MutableHandle<Value> rval);
 
 /**
  * |script| will always be set. On failure, it will be set to nullptr.
  */
 extern JS_PUBLIC_API(bool)
 Compile(JSContext* cx, const ReadOnlyCompileOptions& options,
         SourceBufferHolder& srcBuf, MutableHandle<JSScript*> script);
 
diff --git a/js/src/shell/js.cpp b/js/src/shell/js.cpp
--- a/js/src/shell/js.cpp
+++ b/js/src/shell/js.cpp
@@ -1614,35 +1614,41 @@ LoadScript(JSContext* cx, unsigned argc,
     RootedString str(cx);
     for (unsigned i = 0; i < args.length(); i++) {
         str = JS::ToString(cx, args[i]);
         if (!str) {
             JS_ReportErrorNumberASCII(cx, my_GetErrorMessage, nullptr, JSSMSG_INVALID_ARGS,
                                       "load");
             return false;
         }
+
         str = ResolvePath(cx, str, scriptRelative ? ScriptRelative : RootRelative);
         if (!str) {
             JS_ReportErrorASCII(cx, "unable to resolve path");
             return false;
         }
+
         UniqueChars filename = JS_EncodeStringToLatin1(cx, str);
         if (!filename) {
             return false;
         }
+
         errno = 0;
+
         CompileOptions opts(cx);
         opts.setIntroductionType("js shell load")
             .setUTF8(true)
             .setIsRunOnce(true)
             .setNoScriptRval(true);
+
         RootedScript script(cx);
         RootedValue unused(cx);
-        if ((compileOnly && !CompileUtf8Path(cx, opts, filename.get(), &script)) ||
-            !Evaluate(cx, opts, filename.get(), &unused))
+        if (!(compileOnly
+              ? JS::CompileUtf8Path(cx, opts, filename.get(), &script)
+              : JS::EvaluateUtf8Path(cx, opts, filename.get(), &unused)))
         {
             return false;
         }
     }
 
     args.rval().setUndefined();
     return true;
 }
diff --git a/js/src/vm/CompilationAndEvaluation.cpp b/js/src/vm/CompilationAndEvaluation.cpp
--- a/js/src/vm/CompilationAndEvaluation.cpp
+++ b/js/src/vm/CompilationAndEvaluation.cpp
@@ -557,46 +557,45 @@ JS::Evaluate(JSContext* cx, const ReadOn
     }
 
     SourceBufferHolder srcBuf(chars, length, SourceBufferHolder::GiveOwnership);
     RootedObject globalLexical(cx, &cx->global()->lexicalEnvironment());
     bool ok = ::Evaluate(cx, ScopeKind::Global, globalLexical, options, srcBuf, rval);
     return ok;
 }
 
-static bool
-Evaluate(JSContext* cx, const ReadOnlyCompileOptions& optionsArg,
-         const char* filename, MutableHandleValue rval)
-{
-    FileContents buffer(cx);
-    {
-        AutoFile file;
-        if (!file.open(cx, filename) || !file.readAll(cx, buffer)) {
-            return false;
-        }
-    }
-
-    CompileOptions options(cx, optionsArg);
-    options.setFileAndLine(filename, 1);
-    return Evaluate(cx, options, reinterpret_cast<const char*>(buffer.begin()), buffer.length(), rval);
-}
-
 JS_PUBLIC_API(bool)
 JS::Evaluate(JSContext* cx, const ReadOnlyCompileOptions& optionsArg,
              SourceBufferHolder& srcBuf, MutableHandleValue rval)
 {
     RootedObject globalLexical(cx, &cx->global()->lexicalEnvironment());
     return ::Evaluate(cx, ScopeKind::Global, globalLexical, optionsArg, srcBuf, rval);
 }
 
 JS_PUBLIC_API(bool)
 JS::Evaluate(JSContext* cx, AutoObjectVector& envChain, const ReadOnlyCompileOptions& optionsArg,
              SourceBufferHolder& srcBuf, MutableHandleValue rval)
 {
     return ::Evaluate(cx, envChain, optionsArg, srcBuf, rval);
 }
 
 JS_PUBLIC_API(bool)
-JS::Evaluate(JSContext* cx, const ReadOnlyCompileOptions& optionsArg,
-             const char* filename, MutableHandleValue rval)
+JS::EvaluateUtf8Path(JSContext* cx, const ReadOnlyCompileOptions& optionsArg,
+                     const char* filename, MutableHandleValue rval)
 {
-    return ::Evaluate(cx, optionsArg, filename, rval);
+    MOZ_ASSERT(optionsArg.utf8,
+               "this function only evaluates UTF-8 source text in the file at "
+               "the given path");
+
+    FileContents buffer(cx);
+    {
+        AutoFile file;
+        if (!file.open(cx, filename) || !file.readAll(cx, buffer)) {
+            return false;
+        }
+    }
+
+    CompileOptions options(cx, optionsArg);
+    options.setFileAndLine(filename, 1);
+
+    return Evaluate(cx, options,
+                    reinterpret_cast<const char*>(buffer.begin()), buffer.length(), rval);
 }
