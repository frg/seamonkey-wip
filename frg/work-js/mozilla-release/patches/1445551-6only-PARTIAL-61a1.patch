# HG changeset patch
# User Kris Maglione <maglione.k@gmail.com>
# Date 1521000154 25200
# Node ID 7585e7acb34b63d21504aefb24a4ba43f90fa169
# Parent  c12c1148a09601a48cf6621c64fc7b26cba2ac1e
Bug 1445551: Part 6 - Remove JSAddonId type and addonId compartment flag. r=mccr8

Note: For SeaMonkey 2.53 and up only remove the telemetry reporting for now.

This compartment flag was only ever needed in order to track system-privileged
add-on code running under the compartment-per-addon system. That system, and
the legacy add-ons it supported, are gone.

WebExtension compartments have their add-on ID stored on their principal, and
are tracked in less obtrusive ways, so this code is no longer useful.

MozReview-Commit-ID: NVEd3Oawak

diff --git a/js/src/jsexn.cpp b/js/src/jsexn.cpp
--- a/js/src/jsexn.cpp
+++ b/js/src/jsexn.cpp
@@ -773,77 +773,16 @@ ErrorReport::ErrorReport(JSContext* cx)
     exnObject(cx)
 {
 }
 
 ErrorReport::~ErrorReport()
 {
 }
 
-void
-ErrorReport::ReportAddonExceptionToTelemetry(JSContext* cx)
-{
-    MOZ_ASSERT(exnObject);
-    RootedObject unwrapped(cx, UncheckedUnwrap(exnObject));
-    MOZ_ASSERT(unwrapped, "UncheckedUnwrap failed?");
-
-    // There is not much we can report if the exception is not an ErrorObject, let's ignore those.
-    if (!unwrapped->is<ErrorObject>())
-        return;
-
-    Rooted<ErrorObject*> errObj(cx, &unwrapped->as<ErrorObject>());
-    RootedObject stack(cx, errObj->stack());
-
-    // Let's ignore TOP level exceptions. For regular add-ons those will not be reported anyway,
-    // for SDK based once it should not be a valid case either.
-    // At this point the frame stack is unwound but the exception object stored the stack so let's
-    // use that for getting the function name.
-    if (!stack)
-        return;
-
-    JSCompartment* comp = stack->compartment();
-    JSAddonId* addonId = comp->creationOptions().addonIdOrNull();
-
-    // We only want to send the report if the scope that just have thrown belongs to an add-on.
-    // Let's check the compartment of the youngest function on the stack, to determine that.
-    if (!addonId)
-        return;
-
-    RootedString funnameString(cx);
-    JS::SavedFrameResult result = GetSavedFrameFunctionDisplayName(cx, stack, &funnameString);
-    // AccessDenied should never be the case here for add-ons but let's not risk it.
-    JSAutoByteString bytes;
-    const char* funname = nullptr;
-    bool denied = result == JS::SavedFrameResult::AccessDenied;
-    funname = denied ? "unknown"
-                     : funnameString ? AtomToPrintableString(cx,
-                                                             &funnameString->asAtom(),
-                                                             &bytes)
-                                     : "anonymous";
-
-    UniqueChars addonIdChars(JS_EncodeString(cx, addonId));
-
-    const char* filename = nullptr;
-    if (reportp && reportp->filename) {
-        filename = strrchr(reportp->filename, '/');
-        if (filename)
-            filename++;
-    }
-    if (!filename) {
-        filename = "FILE_NOT_FOUND";
-    }
-    char histogramKey[64];
-    SprintfLiteral(histogramKey, "%s %s %s %u",
-                   addonIdChars.get(),
-                   funname,
-                   filename,
-                   (reportp ? reportp->lineno : 0) );
-    cx->runtime()->addTelemetry(JS_TELEMETRY_ADDON_EXCEPTIONS, 1, histogramKey);
-}
-
 bool
 ErrorReport::init(JSContext* cx, HandleValue exn,
                   SniffingBehavior sniffingBehavior)
 {
     MOZ_ASSERT(!cx->isExceptionPending());
     MOZ_ASSERT(!reportp);
 
     if (exn.isObject()) {
@@ -852,20 +791,16 @@ ErrorReport::init(JSContext* cx, HandleV
         exnObject = &exn.toObject();
         reportp = ErrorFromException(cx, exnObject);
 
         if (!reportp && sniffingBehavior == NoSideEffects) {
             JS_ReportErrorNumberASCII(cx, GetErrorMessage, nullptr,
                                       JSMSG_ERR_DURING_THROW);
             return false;
         }
-
-        // Let's see if the exception is from add-on code, if so, it should be reported
-        // to telemetry.
-        ReportAddonExceptionToTelemetry(cx);
     }
 
 
     // Be careful not to invoke ToString if we've already successfully extracted
     // an error report, since the exception might be wrapped in a security
     // wrapper, and ToString-ing it might throw.
     if (reportp) {
         str = ErrorReportToString(cx, reportp);
diff --git a/js/src/jsfriendapi.h b/js/src/jsfriendapi.h
--- a/js/src/jsfriendapi.h
+++ b/js/src/jsfriendapi.h
@@ -155,17 +155,16 @@ enum {
     JS_TELEMETRY_GC_SCC_SWEEP_MAX_PAUSE_MS,
     JS_TELEMETRY_GC_MINOR_REASON,
     JS_TELEMETRY_GC_MINOR_REASON_LONG,
     JS_TELEMETRY_GC_MINOR_US,
     JS_TELEMETRY_GC_NURSERY_BYTES,
     JS_TELEMETRY_GC_PRETENURE_COUNT,
     JS_TELEMETRY_DEPRECATED_LANGUAGE_EXTENSIONS_IN_CONTENT,
     JS_TELEMETRY_DEPRECATED_LANGUAGE_EXTENSIONS_IN_ADDONS,
-    JS_TELEMETRY_ADDON_EXCEPTIONS,
     JS_TELEMETRY_PRIVILEGED_PARSER_COMPILE_LAZY_AFTER_MS,
     JS_TELEMETRY_WEB_PARSER_COMPILE_LAZY_AFTER_MS,
     JS_TELEMETRY_END
 };
 
 typedef void
 (*JSAccumulateTelemetryDataCallback)(int id, uint32_t sample, const char* key);
 
@@ -1567,19 +1566,16 @@ struct MOZ_STACK_CLASS JS_FRIEND_API(Err
     // but fills in an ErrorReport instead of reporting it.  Uses varargs to
     // make it simpler to call js::ExpandErrorArgumentsVA.
     //
     // Returns false if we fail to actually populate the ErrorReport
     // for some reason (probably out of memory).
     bool populateUncaughtExceptionReportUTF8(JSContext* cx, ...);
     bool populateUncaughtExceptionReportUTF8VA(JSContext* cx, va_list ap);
 
-    // Reports exceptions from add-on scopes to telemetry.
-    void ReportAddonExceptionToTelemetry(JSContext* cx);
-
     // We may have a provided JSErrorReport, so need a way to represent that.
     JSErrorReport* reportp;
 
     // Or we may need to synthesize a JSErrorReport one of our own.
     JSErrorReport ownedReport;
 
     // And we have a string to maybe keep alive that has pointers into
     // it from ownedReport.
diff --git a/js/src/vm/JSCompartment.cpp b/js/src/vm/JSCompartment.cpp
--- a/js/src/vm/JSCompartment.cpp
+++ b/js/src/vm/JSCompartment.cpp
@@ -1361,17 +1361,17 @@ JSCompartment::reportTelemetry()
 }
 
 void
 JSCompartment::addTelemetry(const char* filename, DeprecatedLanguageExtension e)
 {
     // Only report telemetry for web content and add-ons, not chrome JS.
     if (isSystem_)
         return;
-    if (!creationOptions_.addonIdOrNull() && (!filename || strncmp(filename, "http", 4) != 0))
+    if (!filename || strncmp(filename, "http", 4) != 0)
         return;
 
     sawDeprecatedLanguageExtension[size_t(e)] = true;
 }
 
 HashNumber
 JSCompartment::randomHashCode()
 {
diff --git a/js/xpconnect/src/XPCJSRuntime.cpp b/js/xpconnect/src/XPCJSRuntime.cpp
--- a/js/xpconnect/src/XPCJSRuntime.cpp
+++ b/js/xpconnect/src/XPCJSRuntime.cpp
@@ -2655,19 +2655,16 @@ AccumulateTelemetryCallback(int id, uint
         Telemetry::Accumulate(Telemetry::GC_PRETENURE_COUNT, sample);
         break;
       case JS_TELEMETRY_DEPRECATED_LANGUAGE_EXTENSIONS_IN_CONTENT:
         Telemetry::Accumulate(Telemetry::JS_DEPRECATED_LANGUAGE_EXTENSIONS_IN_CONTENT, sample);
         break;
       case JS_TELEMETRY_DEPRECATED_LANGUAGE_EXTENSIONS_IN_ADDONS:
         Telemetry::Accumulate(Telemetry::JS_DEPRECATED_LANGUAGE_EXTENSIONS_IN_ADDONS, sample);
         break;
-      case JS_TELEMETRY_ADDON_EXCEPTIONS:
-        Telemetry::Accumulate(Telemetry::JS_TELEMETRY_ADDON_EXCEPTIONS, nsDependentCString(key), sample);
-        break;
       case JS_TELEMETRY_PRIVILEGED_PARSER_COMPILE_LAZY_AFTER_MS:
         Telemetry::Accumulate(Telemetry::JS_PRIVILEGED_PARSER_COMPILE_LAZY_AFTER_MS, sample);
         break;
       case JS_TELEMETRY_WEB_PARSER_COMPILE_LAZY_AFTER_MS:
         Telemetry::Accumulate(Telemetry::JS_WEB_PARSER_COMPILE_LAZY_AFTER_MS, sample);
         break;
       default:
         MOZ_ASSERT_UNREACHABLE("Unexpected JS_TELEMETRY id");
diff --git a/toolkit/components/telemetry/Histograms.json b/toolkit/components/telemetry/Histograms.json
--- a/toolkit/components/telemetry/Histograms.json
+++ b/toolkit/components/telemetry/Histograms.json
@@ -5239,23 +5239,16 @@
     "record_in_processes": ["main", "content"],
     "alert_emails": ["seceng-telemetry@mozilla.com", "fxprivacyandsecurity@mozilla.com"],
     "bug_numbers": [767676],
     "expires_in_version": "never",
     "kind": "enumerated",
     "n_values": 100,
     "description": "Security-related UI events (addons, form submission, TLS certs, Safe Browsing, updates and geolocation). See /security/manager/ssl/nsISecurityUITelemetry.idl for the specific values."
   },
-  "JS_TELEMETRY_ADDON_EXCEPTIONS" : {
-    "record_in_processes": ["main", "content"],
-    "expires_in_version" : "never",
-    "kind": "count",
-    "keyed" : true,
-    "description" : "Exceptions thrown by add-ons"
-  },
   "IPC_TRANSACTION_CANCEL": {
     "record_in_processes": ["main", "content"],
     "alert_emails": ["billm@mozilla.com"],
     "expires_in_version": "never",
     "kind": "boolean",
     "description": "True when an IPC transaction is canceled"
   },
   "IPC_SAME_PROCESS_MESSAGE_COPY_OOM_KB": {
diff --git a/toolkit/components/telemetry/histogram-whitelists.json b/toolkit/components/telemetry/histogram-whitelists.json
--- a/toolkit/components/telemetry/histogram-whitelists.json
+++ b/toolkit/components/telemetry/histogram-whitelists.json
@@ -182,17 +182,16 @@
     "GDI_INITFONTLIST_TOTAL",
     "GEOLOCATION_ACCURACY_EXPONENTIAL",
     "GEOLOCATION_ERROR",
     "GEOLOCATION_OSX_SOURCE_IS_MLS",
     "GEOLOCATION_WIN8_SOURCE_IS_MLS",
     "GFX_CRASH",
     "INNERWINDOWS_WITH_MUTATION_LISTENERS",
     "IPC_SAME_PROCESS_MESSAGE_COPY_OOM_KB",
-    "JS_TELEMETRY_ADDON_EXCEPTIONS",
     "LINK_ICON_SIZES_ATTR_DIMENSION",
     "LINK_ICON_SIZES_ATTR_USAGE",
     "LOCALDOMSTORAGE_CLEAR_BLOCKING_MS",
     "LOCALDOMSTORAGE_GETALLKEYS_BLOCKING_MS",
     "LOCALDOMSTORAGE_GETKEY_BLOCKING_MS",
     "LOCALDOMSTORAGE_GETLENGTH_BLOCKING_MS",
     "LOCALDOMSTORAGE_GETVALUE_BLOCKING_MS",
     "LOCALDOMSTORAGE_PRELOAD_PENDING_ON_FIRST_ACCESS",
@@ -645,17 +644,16 @@
     "IMAGE_DECODE_SPEED_JPEG",
     "IMAGE_DECODE_SPEED_PNG",
     "IMAGE_DECODE_TIME",
     "INNERWINDOWS_WITH_MUTATION_LISTENERS",
     "IPC_SAME_PROCESS_MESSAGE_COPY_OOM_KB",
     "IPC_TRANSACTION_CANCEL",
     "JS_DEPRECATED_LANGUAGE_EXTENSIONS_IN_ADDONS",
     "JS_DEPRECATED_LANGUAGE_EXTENSIONS_IN_CONTENT",
-    "JS_TELEMETRY_ADDON_EXCEPTIONS",
     "LINK_ICON_SIZES_ATTR_DIMENSION",
     "LINK_ICON_SIZES_ATTR_USAGE",
     "LOCALDOMSTORAGE_CLEAR_BLOCKING_MS",
     "LOCALDOMSTORAGE_GETALLKEYS_BLOCKING_MS",
     "LOCALDOMSTORAGE_GETKEY_BLOCKING_MS",
     "LOCALDOMSTORAGE_GETLENGTH_BLOCKING_MS",
     "LOCALDOMSTORAGE_GETVALUE_BLOCKING_MS",
     "LOCALDOMSTORAGE_PRELOAD_PENDING_ON_FIRST_ACCESS",
@@ -1336,17 +1334,16 @@
     "FXA_CONFIGURED",
     "FX_BROWSER_FULLSCREEN_USED",
     "FX_CONTENT_CRASH_DUMP_UNAVAILABLE",
     "FX_CONTENT_CRASH_NOT_SUBMITTED",
     "FX_CONTENT_CRASH_PRESENTED",
     "FX_SESSION_RESTORE_SEND_UPDATE_CAUSED_OOM",
     "FX_TOUCH_USED",
     "GEOLOCATION_ERROR",
-    "JS_TELEMETRY_ADDON_EXCEPTIONS",
     "MASTER_PASSWORD_ENABLED",
     "MEDIA_CODEC_USED",
     "NUMBER_OF_PROFILES",
     "ONBEFOREUNLOAD_PROMPT_COUNT",
     "OPENGL_COMPOSITING_FAILURE_ID",
     "PERMISSIONS_SQL_CORRUPTED",
     "PLUGIN_HANG_NOTICE_COUNT",
     "PRINT_COUNT",
