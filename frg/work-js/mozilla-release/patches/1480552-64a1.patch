# HG changeset patch
# User Nathan Froyd <froydnj@mozilla.com>
# Date 1536093668 14400
# Node ID 9be13bbe1877d736a656f682b25b49f2b75b69d0
# Parent  b976a6348dfc5890eb1db14ce663d4a2079b4ac4
Bug 1480552 - handle aarch64 windows when determining MOZ_D3DCOMPILER_VISTA_DLL; r=ted.mielczarek

AArch64 Windows includes the necessary DLL in its default configuration,
so we don't need to bother locating it in the SDK.  We made need to
distribute an updated version that won't by on the system by default,
though, so we need some extra checking.

diff --git a/old-configure.in b/old-configure.in
--- a/old-configure.in
+++ b/old-configure.in
@@ -2534,16 +2534,19 @@ esac
 # The DirectX SDK libraries are split into x86 and x64 sub-directories
 case "${target_cpu}" in
 i*86)
   MOZ_D3D_CPU_SUFFIX=x86
   ;;
 x86_64)
   MOZ_D3D_CPU_SUFFIX=x64
   ;;
+aarch64)
+  MOZ_D3D_CPU_SUFFIX=arm
+  ;;
 esac
 
 dnl ========================================================
 dnl D3D compiler DLL
 dnl ========================================================
 MOZ_FOUND_D3D_COMPILERS=
 
 if test -n "$MOZ_ANGLE_RENDERER"; then
@@ -2565,16 +2568,19 @@ if test -n "$MOZ_ANGLE_RENDERER"; then
 
   if test -n "$MOZ_D3DCOMPILER_VISTA_DLL"; then
     # We have a name, now track down the path.
     if test -n "$WINDOWSSDKDIR"; then
       MOZ_D3DCOMPILER_VISTA_DLL_PATH="$WINDOWSSDKDIR/Redist/D3D/$MOZ_D3D_CPU_SUFFIX/$MOZ_D3DCOMPILER_VISTA_DLL"
       if test -f "$MOZ_D3DCOMPILER_VISTA_DLL_PATH"; then
         AC_MSG_RESULT([Found MOZ_D3DCOMPILER_VISTA_DLL_PATH: $MOZ_D3DCOMPILER_VISTA_DLL_PATH])
         MOZ_HAS_WINSDK_WITH_D3D=1
+      elif test "$target_cpu" = "aarch64" -a "$MOZ_D3DCOMPILER_VISTA_DLL" = "d3dcompiler_47.dll"; then
+        AC_MSG_RESULT([AArch64 Windows includes d3dcompiler DLLs])
+        MOZ_D3DCOMPILER_VISTA_DLL_PATH=
       else
         AC_MSG_RESULT([MOZ_D3DCOMPILER_VISTA_DLL_PATH doesn't exist: $MOZ_D3DCOMPILER_VISTA_DLL_PATH])
         AC_MSG_ERROR([Windows SDK at "$WINDOWSSDKDIR" appears broken. Try updating to MozillaBuild 1.9 final or higher.])
         MOZ_D3DCOMPILER_VISTA_DLL_PATH=
       fi
     else
       AC_MSG_RESULT([Windows SDK not found.])
     fi
