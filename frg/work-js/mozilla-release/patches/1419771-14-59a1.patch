# HG changeset patch
# User Andrea Marchesini <amarchesini@mozilla.com>
# Date 1515416705 -3600
# Node ID cf6aadebae4faf35298a9b41cd105ae9ad49e608
# Parent  9c2b3d7f1bbc2c65d742d5bee7c6ed9f154f2788
Bug 1419771 - Introduce DOMPrefs, a thread-safe access to preferences for DOM - part 14 - Request Context enabled, r=asuth

diff --git a/dom/base/DOMPrefs.cpp b/dom/base/DOMPrefs.cpp
--- a/dom/base/DOMPrefs.cpp
+++ b/dom/base/DOMPrefs.cpp
@@ -42,16 +42,17 @@ PREF(NotificationEnabled, "dom.webnotifi
 PREF(NotificationEnabledInServiceWorkers, "dom.webnotifications.serviceworker.enabled")
 PREF(NotificationRIEnabled, "dom.webnotifications.requireinteraction.enabled")
 PREF(ServiceWorkersEnabled, "dom.serviceWorkers.enabled")
 PREF(ServiceWorkersTestingEnabled, "dom.serviceWorkers.testing.enabled")
 PREF(StorageManagerEnabled, "dom.storageManager.enabled")
 PREF(PromiseRejectionEventsEnabled, "dom.promise_rejection_events.enabled")
 PREF(PushEnabled, "dom.push.enabled")
 PREF(StreamsEnabled, "dom.streams.enabled")
+PREF(RequestContextEnabled, "dom.requestcontext.enabled")
 
 #undef PREF
 
 #define PREF_WEBIDL(name)                        \
   /* static */ bool                              \
   DOMPrefs::name(JSContext* aCx, JSObject* aObj) \
   {                                              \
     return DOMPrefs::name();                     \
@@ -61,13 +62,14 @@ PREF_WEBIDL(ImageBitmapExtensionsEnabled
 PREF_WEBIDL(DOMCachesEnabled)
 PREF_WEBIDL(NotificationEnabledInServiceWorkers)
 PREF_WEBIDL(NotificationRIEnabled)
 PREF_WEBIDL(ServiceWorkersEnabled)
 PREF_WEBIDL(StorageManagerEnabled)
 PREF_WEBIDL(PromiseRejectionEventsEnabled)
 PREF_WEBIDL(PushEnabled)
 PREF_WEBIDL(StreamsEnabled)
+PREF_WEBIDL(RequestContextEnabled)
 
 #undef PREF_WEBIDL
 
 } // dom namespace
 } // mozilla namespace
diff --git a/dom/base/DOMPrefs.h b/dom/base/DOMPrefs.h
--- a/dom/base/DOMPrefs.h
+++ b/dom/base/DOMPrefs.h
@@ -62,14 +62,18 @@ public:
 
   // Returns true if the dom.push.enabled pref is set.
   static bool PushEnabled();
   static bool PushEnabled(JSContext* aCx, JSObject* aObj);
 
   // Returns true if the dom.streams.enabled pref is set.
   static bool StreamsEnabled();
   static bool StreamsEnabled(JSContext* aCx, JSObject* aObj);
+
+  // Returns true if the dom.requestcontext.enabled pref is set.
+  static bool RequestContextEnabled();
+  static bool RequestContextEnabled(JSContext* aCx, JSObject* aObj);
 };
 
 } // dom namespace
 } // mozilla namespace
 
 #endif // mozilla_dom_DOMPrefs_h
diff --git a/dom/fetch/Request.cpp b/dom/fetch/Request.cpp
--- a/dom/fetch/Request.cpp
+++ b/dom/fetch/Request.cpp
@@ -72,35 +72,16 @@ Request::Request(nsIGlobalObject* aOwner
     }
   }
 }
 
 Request::~Request()
 {
 }
 
-// static
-bool
-Request::RequestContextEnabled(JSContext* aCx, JSObject* aObj)
-{
-  if (NS_IsMainThread()) {
-    return Preferences::GetBool("dom.requestcontext.enabled", false);
-  }
-
-  using namespace workers;
-
-  // Otherwise, check the pref via the WorkerPrivate
-  WorkerPrivate* workerPrivate = GetWorkerPrivateFromContext(aCx);
-  if (!workerPrivate) {
-    return false;
-  }
-
-  return workerPrivate->RequestContextEnabled();
-}
-
 already_AddRefed<InternalRequest>
 Request::GetInternalRequest()
 {
   RefPtr<InternalRequest> r = mRequest;
   return r.forget();
 }
 
 namespace {
diff --git a/dom/fetch/Request.h b/dom/fetch/Request.h
--- a/dom/fetch/Request.h
+++ b/dom/fetch/Request.h
@@ -30,19 +30,16 @@ class Request final : public nsISupports
 {
   NS_DECL_CYCLE_COLLECTING_ISUPPORTS
   NS_DECL_CYCLE_COLLECTION_SCRIPT_HOLDER_CLASS(Request)
 
 public:
   Request(nsIGlobalObject* aOwner, InternalRequest* aRequest,
           AbortSignal* aSignal);
 
-  static bool
-  RequestContextEnabled(JSContext* aCx, JSObject* aObj);
-
   JSObject*
   WrapObject(JSContext* aCx, JS::Handle<JSObject*> aGivenProto) override
   {
     return RequestBinding::Wrap(aCx, this, aGivenProto);
   }
 
   void
   GetUrl(nsAString& aUrl) const
diff --git a/dom/webidl/Request.webidl b/dom/webidl/Request.webidl
--- a/dom/webidl/Request.webidl
+++ b/dom/webidl/Request.webidl
@@ -12,17 +12,17 @@ typedef unsigned long nsContentPolicyTyp
 
 [Constructor(RequestInfo input, optional RequestInit init),
  Exposed=(Window,Worker)]
 interface Request {
   readonly attribute ByteString method;
   readonly attribute USVString url;
   [SameObject] readonly attribute Headers headers;
 
-  [Func="mozilla::dom::Request::RequestContextEnabled"]
+  [Func="mozilla::dom::DOMPrefs::RequestContextEnabled"]
   readonly attribute RequestContext context;
   readonly attribute USVString referrer;
   readonly attribute ReferrerPolicy referrerPolicy;
   readonly attribute RequestMode mode;
   readonly attribute RequestCredentials credentials;
   readonly attribute RequestCache cache;
   readonly attribute RequestRedirect redirect;
   readonly attribute DOMString integrity;
diff --git a/dom/workers/WorkerPrefs.h b/dom/workers/WorkerPrefs.h
--- a/dom/workers/WorkerPrefs.h
+++ b/dom/workers/WorkerPrefs.h
@@ -16,17 +16,16 @@
 //     macro in Workers.h.
 //   * The name of the function that updates the new value of a pref.
 //
 //   WORKER_PREF("foo.bar", UpdaterFunction)
 //
 //   * First argument is the name of the pref.
 //   * The name of the function that updates the new value of a pref.
 
-WORKER_SIMPLE_PREF("dom.requestcontext.enabled", RequestContextEnabled, REQUESTCONTEXT_ENABLED)
 WORKER_SIMPLE_PREF("gfx.offscreencanvas.enabled", OffscreenCanvasEnabled, OFFSCREENCANVAS_ENABLED)
 WORKER_SIMPLE_PREF("dom.webkitBlink.dirPicker.enabled", WebkitBlinkDirectoryPickerEnabled, DOM_WEBKITBLINK_DIRPICKER_WEBKITBLINK)
 WORKER_SIMPLE_PREF("dom.netinfo.enabled", NetworkInformationEnabled, NETWORKINFORMATION_ENABLED)
 WORKER_SIMPLE_PREF("dom.fetchObserver.enabled", FetchObserverEnabled, FETCHOBSERVER_ENABLED)
 WORKER_SIMPLE_PREF("privacy.resistFingerprinting", ResistFingerprintingEnabled, RESISTFINGERPRINTING_ENABLED)
 WORKER_SIMPLE_PREF("devtools.enabled", DevToolsEnabled, DEVTOOLS_ENABLED)
 WORKER_PREF("intl.accept_languages", PrefLanguagesChanged)
 WORKER_PREF("general.appname.override", AppNameOverrideChanged)
