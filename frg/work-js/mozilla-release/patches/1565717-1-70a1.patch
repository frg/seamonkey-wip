# HG changeset patch
# User Markus Stange <mstange@themasta.com>
# Date 1565778227 0
#      Wed Aug 14 10:23:47 2019 +0000
# Node ID 69ae93abded3a9af88621d7acdf8bdd93206ac4d
# Parent  e049f243b7f03ec9c14495beefc93fd0e7ac64a1
Bug 1565717 - Remove unused MacIOSurface functions. r=mattwoodrow

These were used for creating accelerated CGContexts that render to an IOSurface.
We only used those experimentally on Nightly for a while but turned that option
off because of crashes. And in the meantime, DrawTargetCG has been removed
entirely. So we'll probably never use these functions again.

Differential Revision: https://phabricator.services.mozilla.com/D41809

diff --git a/gfx/2d/MacIOSurface.cpp b/gfx/2d/MacIOSurface.cpp
--- a/gfx/2d/MacIOSurface.cpp
+++ b/gfx/2d/MacIOSurface.cpp
@@ -14,60 +14,50 @@
 #include "GLContextCGL.h"
 
 using namespace mozilla;
 // IOSurface signatures
 #define IOSURFACE_FRAMEWORK_PATH \
   "/System/Library/Frameworks/IOSurface.framework/IOSurface"
 #define OPENGL_FRAMEWORK_PATH \
   "/System/Library/Frameworks/OpenGL.framework/OpenGL"
-#define COREGRAPHICS_FRAMEWORK_PATH \
-  "/System/Library/Frameworks/ApplicationServices.framework/Frameworks/" \
-  "CoreGraphics.framework/CoreGraphics"
 #define COREVIDEO_FRAMEWORK_PATH \
   "/System/Library/Frameworks/ApplicationServices.framework/Frameworks/" \
   "CoreVideo.framework/CoreVideo"
 
 #define GET_CONST(const_name) \
   ((CFStringRef*) dlsym(sIOSurfaceFramework, const_name))
 #define GET_IOSYM(dest,sym_name) \
   (typeof(dest)) dlsym(sIOSurfaceFramework, sym_name)
 #define GET_CGLSYM(dest,sym_name) \
   (typeof(dest)) dlsym(sOpenGLFramework, sym_name)
-#define GET_CGSYM(dest,sym_name) \
-  (typeof(dest)) dlsym(sCoreGraphicsFramework, sym_name)
 #define GET_CVSYM(dest, sym_name) \
   (typeof(dest)) dlsym(sCoreVideoFramework, sym_name)
 
 MacIOSurfaceLib::LibraryUnloader MacIOSurfaceLib::sLibraryUnloader;
 bool                          MacIOSurfaceLib::isLoaded = false;
 void*                         MacIOSurfaceLib::sIOSurfaceFramework;
 void*                         MacIOSurfaceLib::sOpenGLFramework;
-void*                         MacIOSurfaceLib::sCoreGraphicsFramework;
 void*                         MacIOSurfaceLib::sCoreVideoFramework;
 IOSurfaceCreateFunc           MacIOSurfaceLib::sCreate;
 IOSurfaceGetIDFunc            MacIOSurfaceLib::sGetID;
 IOSurfaceLookupFunc           MacIOSurfaceLib::sLookup;
 IOSurfaceGetBaseAddressFunc   MacIOSurfaceLib::sGetBaseAddress;
 IOSurfaceGetBaseAddressOfPlaneFunc  MacIOSurfaceLib::sGetBaseAddressOfPlane;
 IOSurfaceSizePlaneTFunc       MacIOSurfaceLib::sWidth;
 IOSurfaceSizePlaneTFunc       MacIOSurfaceLib::sHeight;
 IOSurfaceSizeTFunc            MacIOSurfaceLib::sPlaneCount;
 IOSurfaceSizePlaneTFunc       MacIOSurfaceLib::sBytesPerRow;
 IOSurfaceGetPropertyMaximumFunc   MacIOSurfaceLib::sGetPropertyMaximum;
 IOSurfaceVoidFunc             MacIOSurfaceLib::sIncrementUseCount;
 IOSurfaceVoidFunc             MacIOSurfaceLib::sDecrementUseCount;
 IOSurfaceLockFunc             MacIOSurfaceLib::sLock;
 IOSurfaceUnlockFunc           MacIOSurfaceLib::sUnlock;
 CGLTexImageIOSurface2DFunc    MacIOSurfaceLib::sTexImage;
-IOSurfaceContextCreateFunc    MacIOSurfaceLib::sIOSurfaceContextCreate;
-IOSurfaceContextCreateImageFunc   MacIOSurfaceLib::sIOSurfaceContextCreateImage;
-IOSurfaceContextGetSurfaceFunc    MacIOSurfaceLib::sIOSurfaceContextGetSurface;
 CVPixelBufferGetIOSurfaceFunc MacIOSurfaceLib::sCVPixelBufferGetIOSurface;
-unsigned int                  (*MacIOSurfaceLib::sCGContextGetTypePtr) (CGContextRef) = nullptr;
 IOSurfacePixelFormatFunc      MacIOSurfaceLib::sPixelFormat;
 
 CFStringRef                   MacIOSurfaceLib::kPropWidth;
 CFStringRef                   MacIOSurfaceLib::kPropHeight;
 CFStringRef                   MacIOSurfaceLib::kPropBytesPerElem;
 CFStringRef                   MacIOSurfaceLib::kPropBytesPerRow;
 CFStringRef                   MacIOSurfaceLib::kPropIsGlobal;
 
@@ -151,37 +141,16 @@ CGLError MacIOSurfaceLib::CGLTexImageIOS
   return sTexImage(ctxt, target, internalFormat, width, height,
                    format, type, ioSurface, plane);
 }
 
 IOSurfacePtr MacIOSurfaceLib::CVPixelBufferGetIOSurface(CVPixelBufferRef aPixelBuffer) {
   return sCVPixelBufferGetIOSurface(aPixelBuffer);
 }
 
-CGContextRef MacIOSurfaceLib::IOSurfaceContextCreate(IOSurfacePtr aIOSurfacePtr,
-                             unsigned aWidth, unsigned aHeight,
-                             unsigned aBitsPerComponent, unsigned aBytes,
-                             CGColorSpaceRef aColorSpace, CGBitmapInfo bitmapInfo) {
-  if (!sIOSurfaceContextCreate)
-    return nullptr;
-  return sIOSurfaceContextCreate(aIOSurfacePtr, aWidth, aHeight, aBitsPerComponent, aBytes, aColorSpace, bitmapInfo);
-}
-
-CGImageRef MacIOSurfaceLib::IOSurfaceContextCreateImage(CGContextRef aContext) {
-  if (!sIOSurfaceContextCreateImage)
-    return nullptr;
-  return sIOSurfaceContextCreateImage(aContext);
-}
-
-IOSurfacePtr MacIOSurfaceLib::IOSurfaceContextGetSurface(CGContextRef aContext) {
-  if (!sIOSurfaceContextGetSurface)
-    return nullptr;
-  return sIOSurfaceContextGetSurface(aContext);
-}
-
 CFStringRef MacIOSurfaceLib::GetIOConst(const char* symbole) {
   CFStringRef *address = (CFStringRef*)dlsym(sIOSurfaceFramework, symbole);
   if (!address)
     return nullptr;
 
   return *address;
 }
 
@@ -190,35 +159,28 @@ void MacIOSurfaceLib::LoadLibrary() {
     return;
   }
   isLoaded = true;
   sIOSurfaceFramework = dlopen(IOSURFACE_FRAMEWORK_PATH,
                             RTLD_LAZY | RTLD_LOCAL);
   sOpenGLFramework = dlopen(OPENGL_FRAMEWORK_PATH,
                             RTLD_LAZY | RTLD_LOCAL);
 
-  sCoreGraphicsFramework = dlopen(COREGRAPHICS_FRAMEWORK_PATH,
-                            RTLD_LAZY | RTLD_LOCAL);
-
   sCoreVideoFramework = dlopen(COREVIDEO_FRAMEWORK_PATH,
                             RTLD_LAZY | RTLD_LOCAL);
 
-  if (!sIOSurfaceFramework || !sOpenGLFramework || !sCoreGraphicsFramework ||
-      !sCoreVideoFramework) {
+  if (!sIOSurfaceFramework || !sOpenGLFramework || !sCoreVideoFramework) {
     if (sIOSurfaceFramework)
       dlclose(sIOSurfaceFramework);
     if (sOpenGLFramework)
       dlclose(sOpenGLFramework);
-    if (sCoreGraphicsFramework)
-      dlclose(sCoreGraphicsFramework);
     if (sCoreVideoFramework)
       dlclose(sCoreVideoFramework);
     sIOSurfaceFramework = nullptr;
     sOpenGLFramework = nullptr;
-    sCoreGraphicsFramework = nullptr;
     sCoreVideoFramework = nullptr;
     return;
   }
 
   kPropWidth = GetIOConst("kIOSurfaceWidth");
   kPropHeight = GetIOConst("kIOSurfaceHeight");
   kPropBytesPerElem = GetIOConst("kIOSurfaceBytesPerElement");
   kPropBytesPerRow = GetIOConst("kIOSurfaceBytesPerRow");
@@ -238,26 +200,20 @@ void MacIOSurfaceLib::LoadLibrary() {
     GET_IOSYM(sDecrementUseCount, "IOSurfaceDecrementUseCount");
   sGetBaseAddress = GET_IOSYM(sGetBaseAddress, "IOSurfaceGetBaseAddress");
   sGetBaseAddressOfPlane =
     GET_IOSYM(sGetBaseAddressOfPlane, "IOSurfaceGetBaseAddressOfPlane");
   sPlaneCount = GET_IOSYM(sPlaneCount, "IOSurfaceGetPlaneCount");
   sPixelFormat = GET_IOSYM(sPixelFormat, "IOSurfaceGetPixelFormat");
 
   sTexImage = GET_CGLSYM(sTexImage, "CGLTexImageIOSurface2D");
-  sCGContextGetTypePtr = (unsigned int (*)(CGContext*))dlsym(RTLD_DEFAULT, "CGContextGetType");
 
   sCVPixelBufferGetIOSurface =
     GET_CVSYM(sCVPixelBufferGetIOSurface, "CVPixelBufferGetIOSurface");
 
-  // Optional symbols
-  sIOSurfaceContextCreate = GET_CGSYM(sIOSurfaceContextCreate, "CGIOSurfaceContextCreate");
-  sIOSurfaceContextCreateImage = GET_CGSYM(sIOSurfaceContextCreateImage, "CGIOSurfaceContextCreateImage");
-  sIOSurfaceContextGetSurface = GET_CGSYM(sIOSurfaceContextGetSurface, "CGIOSurfaceContextGetSurface");
-
   if (!sCreate || !sGetID || !sLookup || !sTexImage || !sGetBaseAddress ||
       !sGetBaseAddressOfPlane || !sPlaneCount ||
       !kPropWidth || !kPropHeight || !kPropBytesPerElem || !kPropIsGlobal ||
       !sLock || !sUnlock || !sIncrementUseCount || !sDecrementUseCount ||
       !sWidth || !sHeight || !kPropBytesPerRow ||
       !sBytesPerRow || !sGetPropertyMaximum || !sCVPixelBufferGetIOSurface) {
     CloseLibrary();
   }
@@ -588,67 +544,8 @@ MacIOSurface::CGLTexImageIOSurface2D(moz
                                   : mozilla::gfx::SurfaceFormat::R8G8B8X8;
     }
   }
 
   return CGLTexImageIOSurface2D(ctx, LOCAL_GL_TEXTURE_RECTANGLE_ARB, internalFormat,
                                 GetDevicePixelWidth(plane), GetDevicePixelHeight(plane),
                                 format, type, plane);
 }
-
-static
-CGColorSpaceRef CreateSystemColorSpace() {
-  CGColorSpaceRef cspace = ::CGDisplayCopyColorSpace(::CGMainDisplayID());
-  if (!cspace) {
-    cspace = ::CGColorSpaceCreateDeviceRGB();
-  }
-  return cspace;
-}
-
-CGContextRef MacIOSurface::CreateIOSurfaceContext() {
-  CGColorSpaceRef cspace = CreateSystemColorSpace();
-  CGContextRef ref = MacIOSurfaceLib::IOSurfaceContextCreate(mIOSurfacePtr,
-                                                GetDevicePixelWidth(),
-                                                GetDevicePixelHeight(),
-                                                8, 32, cspace, 0x2002);
-  ::CGColorSpaceRelease(cspace);
-  return ref;
-}
-
-CGImageRef MacIOSurface::CreateImageFromIOSurfaceContext(CGContextRef aContext) {
-  if (!MacIOSurfaceLib::isInit())
-    return nullptr;
-
-  return MacIOSurfaceLib::IOSurfaceContextCreateImage(aContext);
-}
-
-already_AddRefed<MacIOSurface> MacIOSurface::IOSurfaceContextGetSurface(CGContextRef aContext,
-                                                                    double aContentsScaleFactor,
-                                                                    bool aHasAlpha) {
-  if (!MacIOSurfaceLib::isInit() || aContentsScaleFactor <= 0)
-    return nullptr;
-
-  IOSurfacePtr surfaceRef = MacIOSurfaceLib::IOSurfaceContextGetSurface(aContext);
-  if (!surfaceRef)
-    return nullptr;
-
-  RefPtr<MacIOSurface> ioSurface = new MacIOSurface(surfaceRef, aContentsScaleFactor, aHasAlpha);
-  if (!ioSurface) {
-    ::CFRelease(surfaceRef);
-    return nullptr;
-  }
-  return ioSurface.forget();
-}
-
-CGContextType GetContextType(CGContextRef ref)
-{
-  if (!MacIOSurfaceLib::isInit() || !MacIOSurfaceLib::sCGContextGetTypePtr)
-    return CG_CONTEXT_TYPE_UNKNOWN;
-
-  unsigned int type = MacIOSurfaceLib::sCGContextGetTypePtr(ref);
-  if (type == CG_CONTEXT_TYPE_BITMAP) {
-    return CG_CONTEXT_TYPE_BITMAP;
-  } else if (type == CG_CONTEXT_TYPE_IOSURFACE) {
-    return CG_CONTEXT_TYPE_IOSURFACE;
-  } else {
-    return CG_CONTEXT_TYPE_UNKNOWN;
-  }
-}
diff --git a/gfx/2d/MacIOSurface.h b/gfx/2d/MacIOSurface.h
--- a/gfx/2d/MacIOSurface.h
+++ b/gfx/2d/MacIOSurface.h
@@ -15,18 +15,16 @@ namespace mozilla {
 namespace gl {
 class GLContext;
 }
 }
 
 struct _CGLContextObject;
 
 typedef _CGLContextObject* CGLContextObj;
-typedef struct CGContext* CGContextRef;
-typedef struct CGImage* CGImageRef;
 typedef uint32_t IOSurfaceID;
 
 #ifdef XP_IOS
 typedef kern_return_t IOReturn;
 typedef int CGLError;
 #endif
 
 typedef CFTypeRef IOSurfacePtr;
@@ -44,47 +42,32 @@ typedef void* (*IOSurfaceGetBaseAddressO
 typedef size_t (*IOSurfaceSizeTFunc)(IOSurfacePtr io_surface);
 typedef size_t (*IOSurfaceSizePlaneTFunc)(IOSurfacePtr io_surface, size_t plane);
 typedef size_t (*IOSurfaceGetPropertyMaximumFunc) (CFStringRef property);
 typedef CGLError (*CGLTexImageIOSurface2DFunc) (CGLContextObj ctxt,
                              GLenum target, GLenum internalFormat,
                              GLsizei width, GLsizei height,
                              GLenum format, GLenum type,
                              IOSurfacePtr ioSurface, GLuint plane);
-typedef CGContextRef (*IOSurfaceContextCreateFunc)(CFTypeRef io_surface,
-                             unsigned width, unsigned height,
-                             unsigned bitsPerComponent, unsigned bytes,
-                             CGColorSpaceRef colorSpace, CGBitmapInfo bitmapInfo);
-typedef CGImageRef (*IOSurfaceContextCreateImageFunc)(CGContextRef ref);
-typedef IOSurfacePtr (*IOSurfaceContextGetSurfaceFunc)(CGContextRef ref);
 
 typedef IOSurfacePtr (*CVPixelBufferGetIOSurfaceFunc)(
   CVPixelBufferRef pixelBuffer);
 
 typedef OSType (*IOSurfacePixelFormatFunc)(IOSurfacePtr io_surface);
 
 #ifdef XP_MACOSX
 #import <OpenGL/OpenGL.h>
 #else
 #import <OpenGLES/ES2/gl.h>
 #endif
 
 #include "2D.h"
 #include "mozilla/RefPtr.h"
 #include "mozilla/RefCounted.h"
 
-enum CGContextType {
-  CG_CONTEXT_TYPE_UNKNOWN = 0,
-  // These are found by inspection, it's possible they could be changed
-  CG_CONTEXT_TYPE_BITMAP = 4,
-  CG_CONTEXT_TYPE_IOSURFACE = 8
-};
-
-CGContextType GetContextType(CGContextRef ref);
-
 class MacIOSurface final : public mozilla::external::AtomicRefCounted<MacIOSurface> {
 public:
   MOZ_DECLARE_REFCOUNTED_VIRTUAL_TYPENAME(MacIOSurface)
   typedef mozilla::gfx::SourceSurface SourceSurface;
 
   // The usage count of the IOSurface is increased by 1 during the lifetime
   // of the MacIOSurface instance.
   // MacIOSurface holds a reference to the corresponding IOSurface.
@@ -131,40 +114,33 @@ public:
                                   size_t plane,
                                   mozilla::gfx::SurfaceFormat* aOutReadFormat = nullptr);
   CGLError CGLTexImageIOSurface2D(CGLContextObj ctxt,
                                   GLenum target, GLenum internalFormat,
                                   GLsizei width, GLsizei height,
                                   GLenum format, GLenum type,
                                   GLuint plane) const;
   already_AddRefed<SourceSurface> GetAsSurface();
-  CGContextRef CreateIOSurfaceContext();
 
-  // FIXME This doesn't really belong here
-  static CGImageRef CreateImageFromIOSurfaceContext(CGContextRef aContext);
-  static already_AddRefed<MacIOSurface> IOSurfaceContextGetSurface(CGContextRef aContext,
-                                                                        double aContentsScaleFactor = 1.0,
-                                                                        bool aHasAlpha = true);
   static size_t GetMaxWidth();
   static size_t GetMaxHeight();
   const void* GetIOSurfacePtr() { return mIOSurfacePtr; }
 
 private:
   friend class nsCARenderer;
   const IOSurfacePtr mIOSurfacePtr;
   double mContentsScaleFactor;
   bool mHasAlpha;
 };
 
 class MacIOSurfaceLib {
 public:
   MacIOSurfaceLib() = delete;
   static void                        *sIOSurfaceFramework;
   static void                        *sOpenGLFramework;
-  static void                        *sCoreGraphicsFramework;
   static void                        *sCoreVideoFramework;
   static bool                         isLoaded;
   static IOSurfaceCreateFunc          sCreate;
   static IOSurfaceGetIDFunc           sGetID;
   static IOSurfaceLookupFunc          sLookup;
   static IOSurfaceGetBaseAddressFunc  sGetBaseAddress;
   static IOSurfaceGetBaseAddressOfPlaneFunc sGetBaseAddressOfPlane;
   static IOSurfaceSizeTFunc           sPlaneCount;
@@ -172,19 +148,16 @@ public:
   static IOSurfaceUnlockFunc          sUnlock;
   static IOSurfaceVoidFunc            sIncrementUseCount;
   static IOSurfaceVoidFunc            sDecrementUseCount;
   static IOSurfaceSizePlaneTFunc      sWidth;
   static IOSurfaceSizePlaneTFunc      sHeight;
   static IOSurfaceSizePlaneTFunc      sBytesPerRow;
   static IOSurfaceGetPropertyMaximumFunc  sGetPropertyMaximum;
   static CGLTexImageIOSurface2DFunc   sTexImage;
-  static IOSurfaceContextCreateFunc   sIOSurfaceContextCreate;
-  static IOSurfaceContextCreateImageFunc  sIOSurfaceContextCreateImage;
-  static IOSurfaceContextGetSurfaceFunc   sIOSurfaceContextGetSurface;
   static CVPixelBufferGetIOSurfaceFunc    sCVPixelBufferGetIOSurface;
   static IOSurfacePixelFormatFunc     sPixelFormat;
   static CFStringRef                  kPropWidth;
   static CFStringRef                  kPropHeight;
   static CFStringRef                  kPropBytesPerElem;
   static CFStringRef                  kPropBytesPerRow;
   static CFStringRef                  kPropIsGlobal;
 
@@ -207,25 +180,18 @@ public:
                                       uint32_t options, uint32_t *seed);
   static void         IOSurfaceIncrementUseCount(IOSurfacePtr aIOSurfacePtr);
   static void         IOSurfaceDecrementUseCount(IOSurfacePtr aIOSurfacePtr);
   static CGLError     CGLTexImageIOSurface2D(CGLContextObj ctxt,
                              GLenum target, GLenum internalFormat,
                              GLsizei width, GLsizei height,
                              GLenum format, GLenum type,
                              IOSurfacePtr ioSurface, GLuint plane);
-  static CGContextRef IOSurfaceContextCreate(IOSurfacePtr aIOSurfacePtr,
-                             unsigned aWidth, unsigned aHeight,
-                             unsigned aBitsPerCompoent, unsigned aBytes,
-                             CGColorSpaceRef aColorSpace, CGBitmapInfo bitmapInfo);
-  static CGImageRef   IOSurfaceContextCreateImage(CGContextRef ref);
-  static IOSurfacePtr IOSurfaceContextGetSurface(CGContextRef ref);
   static IOSurfacePtr CVPixelBufferGetIOSurface(CVPixelBufferRef apixelBuffer);
   static OSType       IOSurfaceGetPixelFormat(IOSurfacePtr aIOSurfacePtr);
-  static unsigned int (*sCGContextGetTypePtr) (CGContextRef);
   static void LoadLibrary();
   static void CloseLibrary();
 
   // Static deconstructor
   static class LibraryUnloader {
   public:
     ~LibraryUnloader() {
       CloseLibrary();
