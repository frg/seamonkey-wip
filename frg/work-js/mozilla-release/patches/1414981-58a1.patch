# HG changeset patch
# User Fred Lin <gasolin@gmail.com>
# Date 1510033617 -28800
# Node ID ffd79ce93abd8199f4c98540b9cd32c8dc4dfe38
# Parent  520c6482218ab0c6b5b4d7ab807aad3c8b29ec4f
Bug 1414981 - Request URL should not show #hash;r=Honza

MozReview-Commit-ID: G3FqltGcaTC

diff --git a/devtools/client/netmonitor/src/components/HeadersPanel.js b/devtools/client/netmonitor/src/components/HeadersPanel.js
--- a/devtools/client/netmonitor/src/components/HeadersPanel.js
+++ b/devtools/client/netmonitor/src/components/HeadersPanel.js
@@ -158,18 +158,19 @@ const HeadersPanel = createClass({
     }
 
     let object = Object.assign({},
       this.getProperties(responseHeaders, RESPONSE_HEADERS),
       this.getProperties(requestHeaders, REQUEST_HEADERS),
       this.getProperties(uploadHeaders, REQUEST_HEADERS_FROM_UPLOAD),
     );
 
+    // not showing #hash in url
     let summaryUrl = urlDetails.unicodeUrl ?
-      this.renderSummary(SUMMARY_URL, new URL(urlDetails.unicodeUrl).origin) : null;
+      this.renderSummary(SUMMARY_URL, urlDetails.unicodeUrl.split("#")[0]) : null;
 
     let summaryMethod = method ?
       this.renderSummary(SUMMARY_METHOD, method) : null;
 
     let summaryAddress = remoteAddress ?
       this.renderSummary(SUMMARY_ADDRESS,
         getFormattedIPAndPort(remoteAddress, remotePort)) : null;
 
diff --git a/devtools/client/netmonitor/test/browser_net_status-codes.js b/devtools/client/netmonitor/test/browser_net_status-codes.js
--- a/devtools/client/netmonitor/test/browser_net_status-codes.js
+++ b/devtools/client/netmonitor/test/browser_net_status-codes.js
@@ -25,68 +25,73 @@ add_task(function* () {
 
   let requestItems = [];
 
   const REQUEST_DATA = [
     {
       // request #0
       method: "GET",
       uri: STATUS_CODES_SJS + "?sts=100",
+      correctUri: STATUS_CODES_SJS + "?sts=100",
       details: {
         status: 101,
         statusText: "Switching Protocols",
         type: "plain",
         fullMimeType: "text/plain; charset=utf-8",
         size: L10N.getFormatStrWithNumbers("networkMenu.sizeB", 0),
         time: true
       }
     },
     {
       // request #1
       method: "GET",
-      uri: STATUS_CODES_SJS + "?sts=200",
+      uri: STATUS_CODES_SJS + "?sts=200#doh",
+      correctUri: STATUS_CODES_SJS + "?sts=200",
       details: {
         status: 202,
         statusText: "Created",
         type: "plain",
         fullMimeType: "text/plain; charset=utf-8",
         size: L10N.getFormatStrWithNumbers("networkMenu.sizeB", 22),
         time: true
       }
     },
     {
       // request #2
       method: "GET",
       uri: STATUS_CODES_SJS + "?sts=300",
+      correctUri: STATUS_CODES_SJS + "?sts=300",
       details: {
         status: 303,
         statusText: "See Other",
         type: "plain",
         fullMimeType: "text/plain; charset=utf-8",
         size: L10N.getFormatStrWithNumbers("networkMenu.sizeB", 22),
         time: true
       }
     },
     {
       // request #3
       method: "GET",
       uri: STATUS_CODES_SJS + "?sts=400",
+      correctUri: STATUS_CODES_SJS + "?sts=400",
       details: {
         status: 404,
         statusText: "Not Found",
         type: "plain",
         fullMimeType: "text/plain; charset=utf-8",
         size: L10N.getFormatStrWithNumbers("networkMenu.sizeB", 22),
         time: true
       }
     },
     {
       // request #4
       method: "GET",
       uri: STATUS_CODES_SJS + "?sts=500",
+      correctUri: STATUS_CODES_SJS + "?sts=500",
       details: {
         status: 501,
         statusText: "Not Implemented",
         type: "plain",
         fullMimeType: "text/plain; charset=utf-8",
         size: L10N.getFormatStrWithNumbers("networkMenu.sizeB", 22),
         time: true
       }
@@ -164,19 +169,19 @@ add_task(function* () {
    * A function that tests "Headers" panel contains correct information.
    */
   function* testHeaders(data, index) {
     EventUtils.sendMouseEvent({ type: "mousedown" },
       document.querySelectorAll(".request-list-item")[index]);
 
     let panel = document.querySelector("#headers-panel");
     let summaryValues = panel.querySelectorAll(".tabpanel-summary-value.textbox-input");
-    let { method, uri, details: { status, statusText } } = data;
+    let { method, correctUri, details: { status, statusText } } = data;
 
-    is(summaryValues[0].value, new URL(uri).origin,
+    is(summaryValues[0].value, correctUri,
       "The url summary value is incorrect.");
     is(summaryValues[1].value, method, "The method summary value is incorrect.");
     is(panel.querySelector(".requests-list-status-icon").dataset.code, status,
       "The status summary code is incorrect.");
     is(summaryValues[3].value, status + " " + statusText,
       "The status summary value is incorrect.");
   }
 
@@ -185,17 +190,18 @@ add_task(function* () {
    */
   function* testParams(data, index) {
     EventUtils.sendMouseEvent({ type: "mousedown" },
       document.querySelectorAll(".request-list-item")[index]);
     EventUtils.sendMouseEvent({ type: "click" },
       document.querySelector("#params-tab"));
 
     let panel = document.querySelector("#params-panel");
-    let statusParamValue = data.uri.split("=").pop();
+    // Bug 1414981 - Request URL should not show #hash
+    let statusParamValue = data.uri.split("=").pop().split("#")[0];
     let treeSections = panel.querySelectorAll(".tree-section");
 
     is(treeSections.length, 1,
       "There should be 1 param section displayed in this panel.");
     is(panel.querySelectorAll("tr:not(.tree-section).treeRow").length, 1,
       "There should be 1 param row displayed in this panel.");
     is(panel.querySelectorAll(".empty-notice").length, 0,
       "The empty notice should not be displayed in this panel.");
diff --git a/devtools/client/netmonitor/test/head.js b/devtools/client/netmonitor/test/head.js
--- a/devtools/client/netmonitor/test/head.js
+++ b/devtools/client/netmonitor/test/head.js
@@ -398,17 +398,18 @@ function verifyRequestItemTarget(documen
   let visibleIndex = requestList.indexOf(requestItem);
 
   info("Visible index of item: " + visibleIndex);
 
   let { fuzzyUrl, status, statusText, cause, type, fullMimeType,
         transferred, size, time, displayedStatus } = data;
 
   let target = document.querySelectorAll(".request-list-item")[visibleIndex];
-  let unicodeUrl = decodeUnicodeUrl(url);
+  // Bug 1414981 - Request URL should not show #hash
+  let unicodeUrl = decodeUnicodeUrl(url).split("#")[0];
   let name = getUrlBaseName(url);
   let query = getUrlQuery(url);
   let host = getUrlHost(url);
   let scheme = getUrlScheme(url);
   let {
     remoteAddress,
     remotePort,
     totalTime,
@@ -420,17 +421,17 @@ function verifyRequestItemTarget(documen
   let latency = getFormattedTime(eventTimings.timings.wait);
   let protocol = getFormattedProtocol(requestItem);
 
   if (fuzzyUrl) {
     ok(requestItem.method.startsWith(method), "The attached method is correct.");
     ok(requestItem.url.startsWith(url), "The attached url is correct.");
   } else {
     is(requestItem.method, method, "The attached method is correct.");
-    is(requestItem.url, url, "The attached url is correct.");
+    is(requestItem.url, url.split("#")[0], "The attached url is correct.");
   }
 
   is(target.querySelector(".requests-list-method").textContent,
     method, "The displayed method is correct.");
 
   if (fuzzyUrl) {
     ok(target.querySelector(".requests-list-file").textContent.startsWith(
       name + (query ? "?" + query : "")), "The displayed file is correct.");
