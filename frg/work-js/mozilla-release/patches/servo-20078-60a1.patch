# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1519126843 18000
# Node ID bfca7256e3f4f7d641be0768d6335d1c5998351b
# Parent  eabf98d3a216bdadb7560e154345c788df9951ea
servo: Merge #20078 - style: Rejigger serialization of a declaration block to not look at the shorthands length (from emilio:decl-block-serialization); r=nox

This is because I'm going to make shorthands() and longhands() return an
iterator, so that we account for prefs properly
(https://bugzilla.mozilla.org/show_bug.cgi?id=1438234).

Source-Repo: https://github.com/servo/servo
Source-Revision: 4d7383f4f3acabfdc76c285c8cf7fb849abd11bd

diff --git a/servo/components/style/properties/declaration_block.rs b/servo/components/style/properties/declaration_block.rs
--- a/servo/components/style/properties/declaration_block.rs
+++ b/servo/components/style/properties/declaration_block.rs
@@ -202,21 +202,23 @@ impl<'a, 'cx, 'cx_a:'cx> Iterator for An
 impl fmt::Debug for PropertyDeclarationBlock {
     fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
         self.declarations.fmt(f)
     }
 }
 
 impl PropertyDeclarationBlock {
     /// Returns the number of declarations in the block.
+    #[inline]
     pub fn len(&self) -> usize {
         self.declarations.len()
     }
 
     /// Create an empty block
+    #[inline]
     pub fn new() -> Self {
         PropertyDeclarationBlock {
             declarations: Vec::new(),
             declarations_importance: SmallBitVec::new(),
             longhands: LonghandIdSet::new(),
         }
     }
 
@@ -224,81 +226,97 @@ impl PropertyDeclarationBlock {
     pub fn with_one(declaration: PropertyDeclaration, importance: Importance) -> Self {
         let mut longhands = LonghandIdSet::new();
         if let PropertyDeclarationId::Longhand(id) = declaration.id() {
             longhands.insert(id);
         }
         PropertyDeclarationBlock {
             declarations: vec![declaration],
             declarations_importance: SmallBitVec::from_elem(1, importance.important()),
-            longhands: longhands,
+            longhands,
         }
     }
 
     /// The declarations in this block
+    #[inline]
     pub fn declarations(&self) -> &[PropertyDeclaration] {
         &self.declarations
     }
 
     /// The `important` flags for declarations in this block
+    #[inline]
     pub fn declarations_importance(&self) -> &SmallBitVec {
         &self.declarations_importance
     }
 
     /// Iterate over `(PropertyDeclaration, Importance)` pairs
+    #[inline]
     pub fn declaration_importance_iter(&self) -> DeclarationImportanceIterator {
         DeclarationImportanceIterator::new(&self.declarations, &self.declarations_importance)
     }
 
     /// Iterate over `PropertyDeclaration` for Importance::Normal
+    #[inline]
     pub fn normal_declaration_iter(&self) -> NormalDeclarationIterator {
         NormalDeclarationIterator::new(&self.declarations, &self.declarations_importance)
     }
 
     /// Return an iterator of (AnimatableLonghand, AnimationValue).
+    #[inline]
     pub fn to_animation_value_iter<'a, 'cx, 'cx_a:'cx>(
         &'a self,
         context: &'cx mut Context<'cx_a>,
         default_values: &'a ComputedValues,
         extra_custom_properties: Option<&'a Arc<::custom_properties::CustomPropertiesMap>>,
     ) -> AnimationValueIterator<'a, 'cx, 'cx_a> {
         AnimationValueIterator::new(self, context, default_values, extra_custom_properties)
     }
 
     /// Returns whether this block contains any declaration with `!important`.
     ///
     /// This is based on the `declarations_importance` bit-vector,
     /// which should be maintained whenever `declarations` is changed.
+    #[inline]
     pub fn any_important(&self) -> bool {
         !self.declarations_importance.all_false()
     }
 
     /// Returns whether this block contains any declaration without `!important`.
     ///
     /// This is based on the `declarations_importance` bit-vector,
     /// which should be maintained whenever `declarations` is changed.
+    #[inline]
     pub fn any_normal(&self) -> bool {
         !self.declarations_importance.all_true()
     }
 
     /// Returns whether this block contains a declaration of a given longhand.
+    #[inline]
     pub fn contains(&self, id: LonghandId) -> bool {
         self.longhands.contains(id)
     }
 
     /// Returns whether this block contains any reset longhand.
     #[inline]
     pub fn contains_any_reset(&self) -> bool {
         self.longhands.contains_any_reset()
     }
 
     /// Get a declaration for a given property.
     ///
-    /// NOTE: This is linear time.
+    /// NOTE: This is linear time in the case of custom properties or in the
+    /// case the longhand is actually in the declaration block.
+    #[inline]
     pub fn get(&self, property: PropertyDeclarationId) -> Option<(&PropertyDeclaration, Importance)> {
+        if let PropertyDeclarationId::Longhand(id) = property {
+            if !self.contains(id) {
+                return None;
+            }
+        }
+
         self.declarations.iter().enumerate().find(|&(_, decl)| decl.id() == property).map(|(i, decl)| {
             let importance = if self.declarations_importance.get(i as u32) {
                 Importance::Important
             } else {
                 Importance::Normal
             };
             (decl, importance)
         })
@@ -403,17 +421,17 @@ impl PropertyDeclarationBlock {
         importance: Importance,
         source: DeclarationSource,
     ) -> bool {
         match source {
             DeclarationSource::Parsing => {
                 let all_shorthand_len = match drain.all_shorthand {
                     AllShorthand::NotSet => 0,
                     AllShorthand::CSSWideKeyword(_) |
-                    AllShorthand::WithVariables(_) => ShorthandId::All.longhands().len()
+                    AllShorthand::WithVariables(_) => shorthands::ALL_SHORTHAND_MAX_LEN,
                 };
                 let push_calls_count =
                     drain.declarations.len() + all_shorthand_len;
 
                 // With deduplication the actual length increase may be less than this.
                 self.declarations.reserve(push_calls_count);
             }
             DeclarationSource::CssOm => {
@@ -761,18 +779,16 @@ impl PropertyDeclarationBlock {
             }
 
             // Step 3.3
             // Step 3.3.1 is done by checking already_serialized while
             // iterating below.
 
             // Step 3.3.2
             for &shorthand in declaration.shorthands() {
-                let properties = shorthand.longhands();
-
                 // Substep 2 & 3
                 let mut current_longhands = SmallVec::<[_; 10]>::new();
                 let mut important_count = 0;
                 let mut found_system = None;
 
                 let is_system_font =
                     shorthand == ShorthandId::Font &&
                     self.declarations.iter().any(|l| {
@@ -788,31 +804,34 @@ impl PropertyDeclarationBlock {
                                found_system = longhand.get_system();
                             }
                             if importance.important() {
                                 important_count += 1;
                             }
                         }
                     }
                 } else {
-                    for (longhand, importance) in self.declaration_importance_iter() {
-                        if longhand.id().is_longhand_of(shorthand) {
-                            current_longhands.push(longhand);
-                            if importance.important() {
-                                important_count += 1;
+                    let mut contains_all_longhands = true;
+                    for &longhand in shorthand.longhands() {
+                        match self.get(PropertyDeclarationId::Longhand(longhand)) {
+                            Some((declaration, importance)) => {
+                                current_longhands.push(declaration);
+                                if importance.important() {
+                                    important_count += 1;
+                                }
+                            }
+                            None => {
+                                contains_all_longhands = false;
+                                break;
                             }
                         }
                     }
+
                     // Substep 1:
-                    //
-                    // Assuming that the PropertyDeclarationBlock contains no
-                    // duplicate entries, if the current_longhands length is
-                    // equal to the properties length, it means that the
-                    // properties that map to shorthand are present in longhands
-                    if current_longhands.len() != properties.len() {
+                    if !contains_all_longhands {
                         continue;
                     }
                 }
 
                 // Substep 4
                 let is_important = important_count > 0;
                 if is_important && important_count != current_longhands.len() {
                     continue;
diff --git a/servo/components/style/properties/properties.mako.rs b/servo/components/style/properties/properties.mako.rs
--- a/servo/components/style/properties/properties.mako.rs
+++ b/servo/components/style/properties/properties.mako.rs
@@ -188,16 +188,19 @@ pub mod shorthands {
 
         data.declare_shorthand(
             "all",
             logical_longhands + other_longhands,
             gecko_pref="layout.css.all-shorthand.enabled",
             spec="https://drafts.csswg.org/css-cascade-3/#all-shorthand"
         )
     %>
+
+    /// The max amount of longhands that the `all` shorthand will ever contain.
+    pub const ALL_SHORTHAND_MAX_LEN: usize = ${len(logical_longhands + other_longhands)};
 }
 
 <%
     from itertools import groupby
 
     # After this code, `data.longhands` is sorted in the following order:
     # - first all keyword variants and all variants known to be Copy,
     # - second all the other variants, such as all variants with the same field
