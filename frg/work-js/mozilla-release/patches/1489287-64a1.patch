# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1536670776 -7200
# Node ID a1afefe5b1f0a8e0e3b097e3836e4a58e476fb04
# Parent  82f227bae1465427f17e85764d01cecff90d9d09
Bug 1489287 - Always try to eagerly-place a floating first-letter frame. r=mats

To avoid its deferred reflow potentially killing continuations that are parented
to the parent of the letter frame.

This rolls back to the behavior previous to bug 488725 just for this case, until
we fix it properly in bug 1490281. This also fixes bug 1489863.

Differential Revision: https://phabricator.services.mozilla.com/D5521

diff --git a/layout/generic/crashtests/1489287.html b/layout/generic/crashtests/1489287.html
new file mode 100644
--- /dev/null
+++ b/layout/generic/crashtests/1489287.html
@@ -0,0 +1,17 @@
+<html>
+<head>
+  <script>
+    function start() {
+      o1 = document.createElement('u')
+      o2 = document.createElement('li')
+      o3 = document.createElement('style')
+      o2.textContent = '\nr'
+      o1.appendChild(o2)
+      document.documentElement.appendChild(o1)
+      o3.textContent = 'html { white-space: pre } :first-letter { float: left }'
+      document.documentElement.appendChild(o3)
+    }
+    document.addEventListener('DOMContentLoaded', start)
+  </script>
+</head>
+</html>
diff --git a/layout/generic/crashtests/crashtests.list b/layout/generic/crashtests/crashtests.list
--- a/layout/generic/crashtests/crashtests.list
+++ b/layout/generic/crashtests/crashtests.list
@@ -672,8 +672,9 @@ load 1401420-1.html
 load 1401709.html
 load 1401807.html
 load 1405443.html
 load 1415185.html
 load 1416544.html
 load 1427824.html
 load 1431781.html
 load 1431781-2.html
+load 1489287.html
diff --git a/layout/generic/nsLineLayout.cpp b/layout/generic/nsLineLayout.cpp
--- a/layout/generic/nsLineLayout.cpp
+++ b/layout/generic/nsLineLayout.cpp
@@ -948,17 +948,28 @@ nsLineLayout::ReflowFrame(nsIFrame* aFra
   } else {
     if (LayoutFrameType::Placeholder == frameType) {
       isEmpty = true;
       pfd->mIsPlaceholder = true;
       pfd->mSkipWhenTrimmingWhitespace = true;
       nsIFrame* outOfFlowFrame = nsLayoutUtils::GetFloatFromPlaceholder(aFrame);
       if (outOfFlowFrame) {
         if (psd->mNoWrap &&
-            !LineIsEmpty() && // We can always place floats in an empty line.
+            // We can always place floats in an empty line.
+            !LineIsEmpty() &&
+            // We always place floating letter frames. This kinda sucks. They'd
+            // usually fall into the LineIsEmpty() check anyway, except when
+            // there's something like a bullet before or what not. We actually
+            // need to place them now, because they're pretty nasty and they
+            // create continuations that are in flow and not a kid of the
+            // previous continuation's parent. We don't want the deferred reflow
+            // of the letter frame to kill a continuation after we've stored it
+            // in the line layout data structures. See bug 1490281 to fix the
+            // underlying issue. When that's fixed this check should be removed.
+            !outOfFlowFrame->IsLetterFrame() &&
             !GetOutermostLineLayout()->mBlockRI->mFlags.mCanHaveTextOverflow) {
           // We'll do this at the next break opportunity.
           RecordNoWrapFloat(outOfFlowFrame);
         } else {
           placedFloat = TryToPlaceFloat(outOfFlowFrame);
         }
       }
     }
