# HG changeset patch
# User Robin Templeton <robin@igalia.com>
# Date 1537242945 0
# Node ID b74cf577c4015db47ad19fca177beb7f9221c8aa
# Parent  4a02e23b3352d588d34977162dbc48d8645d85cb
bug 1490387 - Part 2: Implement ToInt32OrBigInt operation. r=jandem

Differential Revision: https://phabricator.services.mozilla.com/D5556

diff --git a/js/src/jsnum.cpp b/js/src/jsnum.cpp
--- a/js/src/jsnum.cpp
+++ b/js/src/jsnum.cpp
@@ -1749,16 +1749,39 @@ js::ToInt32Slow(JSContext* cx, const Han
     } else {
         if (!ToNumberSlow(cx, v, &d))
             return false;
     }
     *out = ToInt32(d);
     return true;
 }
 
+bool
+js::ToInt32OrBigIntSlow(JSContext* cx, MutableHandleValue vp)
+{
+    MOZ_ASSERT(!vp.isInt32());
+    if (vp.isDouble()) {
+        vp.setInt32(ToInt32(vp.toDouble()));
+        return true;
+    }
+
+    if (!ToNumeric(cx, vp)) {
+        return false;
+    }
+
+#ifdef ENABLE_BIGINT
+    if (vp.isBigInt()) {
+        return true;
+    }
+#endif
+
+    vp.setInt32(ToInt32(vp.toNumber()));
+    return true;
+}
+
 JS_PUBLIC_API(bool)
 js::ToUint32Slow(JSContext* cx, const HandleValue v, uint32_t* out)
 {
     MOZ_ASSERT(!v.isInt32());
     double d;
     if (v.isDouble()) {
         d = v.toDouble();
     } else {
diff --git a/js/src/jsnum.h b/js/src/jsnum.h
--- a/js/src/jsnum.h
+++ b/js/src/jsnum.h
@@ -182,16 +182,28 @@ ToNumeric(JSContext* cx, JS::MutableHand
         return true;
 #ifdef ENABLE_BIGINT
     if (vp.isBigInt())
         return true;
 #endif
     return ToNumericSlow(cx, vp);
 }
 
+bool
+ToInt32OrBigIntSlow(JSContext* cx, JS::MutableHandleValue vp);
+
+MOZ_ALWAYS_INLINE MOZ_MUST_USE bool
+ToInt32OrBigInt(JSContext* cx, JS::MutableHandleValue vp)
+{
+    if (vp.isInt32()) {
+        return true;
+    }
+    return ToInt32OrBigIntSlow(cx, vp);
+}
+
 MOZ_MUST_USE bool
 num_parseInt(JSContext* cx, unsigned argc, Value* vp);
 
 }  /* namespace js */
 
 /*
  * Similar to strtod except that it replaces overflows with infinities of the
  * correct sign, and underflows with zeros of the correct sign.  Guaranteed to
