# HG changeset patch
# User Jeff Walden <jwalden@mit.edu>
# Date 1530131174 25200
# Node ID 26ded9d5d3321a20235036cbc45a118aea6942cf
# Parent  f3b134e1e8f154a9df09490a459b8cf6fc218c34
Bug 1471465 - Fix some issues with ungetting LineTerminatorSequence in certain places.  r=arai

diff --git a/js/src/frontend/TokenStream.cpp b/js/src/frontend/TokenStream.cpp
--- a/js/src/frontend/TokenStream.cpp
+++ b/js/src/frontend/TokenStream.cpp
@@ -1368,44 +1368,40 @@ TokenStreamSpecific<CharT, AnyCharsAcces
     charBuffer.clear();
     do {
         int32_t unit = getCodeUnit();
         if (unit == EOF)
             break;
 
         uint32_t codePoint;
         if (MOZ_LIKELY(isAsciiCodePoint(unit))) {
-            if (MOZ_LIKELY(unicode::IsIdentifierPart(char16_t(unit)))) {
+            if (unicode::IsIdentifierPart(char16_t(unit))) {
                 if (!charBuffer.append(unit))
                     return false;
 
                 continue;
             }
 
             if (unit != '\\' || !matchUnicodeEscapeIdent(&codePoint))
                 break;
         } else {
             int32_t cp;
             if (!getNonAsciiCodePoint(unit, &cp))
                 return false;
 
             codePoint = AssertedCast<uint32_t>(cp);
-        }
-
-        if (!unicode::IsIdentifierPart(codePoint)) {
-            if (MOZ_UNLIKELY(codePoint == unicode::LINE_SEPARATOR ||
-                             codePoint == unicode::PARA_SEPARATOR))
-            {
-                // |restoreNextRawCharAddress| undoes all gets, but it doesn't
-                // revert line/column updates.  The ASCII code path never
-                // updates line/column state, so only Unicode separators gotten
-                // by |getNonAsciiCodePoint| require this.
-                anyCharsAccess().undoInternalUpdateLineInfoForEOL();
+
+            if (!unicode::IsIdentifierPart(codePoint)) {
+                if (MOZ_UNLIKELY(codePoint == '\n')) {
+                    // |restoreNextRawCharAddress| will undo all gets, but we
+                    // have to revert a line/column update manually.
+                    anyCharsAccess().undoInternalUpdateLineInfoForEOL();
+                }
+                break;
             }
-            break;
         }
 
         if (!appendCodePointToCharBuffer(codePoint))
             return false;
     } while (true);
 
     return true;
 }
@@ -1829,22 +1825,18 @@ TokenStreamSpecific<CharT, AnyCharsAcces
             TokenStart start(sourceUnits, -1);
             const CharT* identStart = sourceUnits.addressOfNextCodeUnit() - 1;
 
             int32_t codePoint;
             if (!getNonAsciiCodePoint(unit, &codePoint))
                 return badToken();
 
             if (unicode::IsSpaceOrBOM2(codePoint)) {
-                if (codePoint == unicode::LINE_SEPARATOR || codePoint == unicode::PARA_SEPARATOR) {
-                    if (!updateLineInfoForEOL())
-                        return badToken();
-
+                if (codePoint == '\n')
                     anyCharsAccess().updateFlagsForEOL();
-                }
 
                 continue;
             }
 
             static_assert(isAsciiCodePoint('$'),
                           "IdentifierStart contains '$', but as "
                           "!IsUnicodeIDStart('$'), ensure that '$' is never "
                           "handled here");
@@ -2034,19 +2026,17 @@ TokenStreamSpecific<CharT, AnyCharsAcces
                     error(JSMSG_IDSTART_AFTER_NUMBER);
                     return badToken();
                 }
             } else {
                 int32_t codePoint;
                 if (!getNonAsciiCodePoint(unit, &codePoint))
                     return badToken();
 
-                ungetCodePointIgnoreEOL(codePoint);
-                if (codePoint == unicode::LINE_SEPARATOR || codePoint == unicode::PARA_SEPARATOR)
-                    anyCharsAccess().undoInternalUpdateLineInfoForEOL();
+                ungetNonAsciiNormalizedCodePoint(codePoint);
 
                 if (unicode::IsIdentifierStart(uint32_t(codePoint))) {
                     error(JSMSG_IDSTART_AFTER_NUMBER);
                     return badToken();
                 }
             }
 
             double dval;
diff --git a/js/src/tests/non262/syntax/line-number-maintenance-for-identifier-containing-escape-terminated-by-unicode-separator.js b/js/src/tests/non262/syntax/line-number-maintenance-for-identifier-containing-escape-terminated-by-unicode-separator.js
new file mode 100644
--- /dev/null
+++ b/js/src/tests/non262/syntax/line-number-maintenance-for-identifier-containing-escape-terminated-by-unicode-separator.js
@@ -0,0 +1,41 @@
+/*
+ * Any copyright is dedicated to the Public Domain.
+ * http://creativecommons.org/licenses/publicdomain/
+ */
+
+//-----------------------------------------------------------------------------
+var BUGNUMBER = 9999999;
+var summary =
+  "Properly maintain the line number when tokenizing identifiers that " +
+  "contain Unicode escapes *and* are terminated by U+2028 LINE SEPARATOR " +
+  "or U+2029 PARAGRAPH SEPARATOR";
+
+print(BUGNUMBER + ": " + summary);
+
+/**************
+ * BEGIN TEST *
+ **************/
+
+var code = "var a\u0062c = [];\n"; // line 1
+
+for (var i = 0; i < 100; i++)
+  code += "a\\u0062c\u2028a\\u0062c\u2029"; // lines 2..2+200-1
+
+code += "@"; // line 2+200
+
+try
+{
+  eval(code);
+  throw new Error("didn't throw");
+}
+catch (e)
+{
+  assertEq(e.lineNumber, 2 + 200);
+}
+
+/******************************************************************************/
+
+if (typeof reportCompare === "function")
+  reportCompare(true, true);
+
+print("Tests complete");

