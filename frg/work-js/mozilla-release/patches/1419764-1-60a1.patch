# HG changeset patch
# User Robert Longson <longsonr@gmail.com>
# Date 1516990894 28800
# Node ID 96cf099ea7c1afa6e1656e40d2bad5a2e06ce794
# Parent  71dd0060a613163c0bc393819e8c4f1dea298735
Bug 1419764 - Improve performance where a transform is set by direct matrix manipulation. r=dholbert

MozReview-Commit-ID: zzDTSXtRiU

diff --git a/dom/svg/SVGFragmentIdentifier.cpp b/dom/svg/SVGFragmentIdentifier.cpp
--- a/dom/svg/SVGFragmentIdentifier.cpp
+++ b/dom/svg/SVGFragmentIdentifier.cpp
@@ -91,17 +91,17 @@ public:
                       aParams, mRoot, false))) {
         return false;
       }
     } else if (IsMatchingParameter(aToken, NS_LITERAL_STRING("transform"))) {
       if (mSVGView->mTransforms) {
         return false;
       }
       mSVGView->mTransforms = new nsSVGAnimatedTransformList();
-      if (NS_FAILED(mSVGView->mTransforms->SetBaseValueString(aParams))) {
+      if (NS_FAILED(mSVGView->mTransforms->SetBaseValueString(aParams, mRoot))) {
         return false;
       }
     } else if (IsMatchingParameter(aToken, NS_LITERAL_STRING("zoomAndPan"))) {
       if (mSVGView->mZoomAndPan.IsExplicitlySet()) {
         return false;
       }
       nsIAtom* valAtom = NS_GetStaticAtom(aParams);
       if (!valAtom ||
diff --git a/dom/svg/nsSVGAnimatedTransformList.cpp b/dom/svg/nsSVGAnimatedTransformList.cpp
--- a/dom/svg/nsSVGAnimatedTransformList.cpp
+++ b/dom/svg/nsSVGAnimatedTransformList.cpp
@@ -16,29 +16,31 @@
 #include "SVGTransformListSMILType.h"
 #include "nsIDOMMutationEvent.h"
 
 namespace mozilla {
 
 using namespace dom;
 
 nsresult
-nsSVGAnimatedTransformList::SetBaseValueString(const nsAString& aValue)
+nsSVGAnimatedTransformList::SetBaseValueString(const nsAString& aValue,
+                                               nsSVGElement* aSVGElement)
 {
   SVGTransformList newBaseValue;
   nsresult rv = newBaseValue.SetValueFromString(aValue);
   if (NS_FAILED(rv)) {
     return rv;
   }
 
-  return SetBaseValue(newBaseValue);
+  return SetBaseValue(newBaseValue, aSVGElement);
 }
 
 nsresult
-nsSVGAnimatedTransformList::SetBaseValue(const SVGTransformList& aValue)
+nsSVGAnimatedTransformList::SetBaseValue(const SVGTransformList& aValue,
+                                         nsSVGElement* aSVGElement)
 {
   SVGAnimatedTransformList *domWrapper =
     SVGAnimatedTransformList::GetDOMWrapperIfExists(this);
   if (domWrapper) {
     // We must send this notification *before* changing mBaseVal! If the length
     // of our baseVal is being reduced, our baseVal's DOM wrapper list may have
     // to remove DOM items from itself, and any removed DOM items need to copy
     // their internal counterpart values *before* we change them.
@@ -55,17 +57,22 @@ nsSVGAnimatedTransformList::SetBaseValue
 
   nsresult rv = mBaseVal.CopyFrom(aValue);
   if (NS_FAILED(rv) && domWrapper) {
     // Attempting to increase mBaseVal's length failed - reduce domWrapper
     // back to the same length:
     domWrapper->InternalBaseValListWillChangeLengthTo(mBaseVal.Length());
   } else {
     mIsAttrSet = true;
-    mHadTransformBeforeLastBaseValChange = hadTransform;
+    // If we set this flag to false, we're indicating that aSVGElement's frames
+    // will need reconstructing to account for stacking context changes.
+    // If aSVGElement doesn't have any frames, then that's clearly unnecessary,
+    // so in that case we set the flag to true.
+    mHadTransformBeforeLastBaseValChange =
+      !aSVGElement->GetPrimaryFrame() || hadTransform;
   }
   return rv;
 }
 
 void
 nsSVGAnimatedTransformList::ClearBaseValue()
 {
   mHadTransformBeforeLastBaseValChange = HasTransform();
diff --git a/dom/svg/nsSVGAnimatedTransformList.h b/dom/svg/nsSVGAnimatedTransformList.h
--- a/dom/svg/nsSVGAnimatedTransformList.h
+++ b/dom/svg/nsSVGAnimatedTransformList.h
@@ -55,19 +55,21 @@ public:
    * SVGAnimatedTransformList::InternalBaseValListWillChangeTo), this method
    * returns a const reference. Only our friend classes may get mutable
    * references to mBaseVal.
    */
   const SVGTransformList& GetBaseValue() const {
     return mBaseVal;
   }
 
-  nsresult SetBaseValue(const SVGTransformList& aValue);
+  nsresult SetBaseValue(const SVGTransformList& aValue,
+                        nsSVGElement* aSVGElement);
 
-  nsresult SetBaseValueString(const nsAString& aValue);
+  nsresult SetBaseValueString(const nsAString& aValue,
+                              nsSVGElement* aSVGElement);
 
   void ClearBaseValue();
 
   const SVGTransformList& GetAnimValue() const {
     return mAnimVal ? *mAnimVal : mBaseVal;
   }
 
   nsresult SetAnimValue(const SVGTransformList& aNewAnimValue,
diff --git a/dom/svg/nsSVGElement.cpp b/dom/svg/nsSVGElement.cpp
--- a/dom/svg/nsSVGElement.cpp
+++ b/dom/svg/nsSVGElement.cpp
@@ -625,17 +625,17 @@ nsSVGElement::ParseAttribute(int32_t aNa
           foundMatch = true;
         }
       // Check for SVGAnimatedTransformList attribute
       } else if (GetTransformListAttrName() == aAttribute) {
         // The transform attribute is being set, so we must ensure that the
         // nsSVGAnimatedTransformList is/has been allocated:
         nsSVGAnimatedTransformList *transformList =
           GetAnimatedTransformList(DO_ALLOCATE);
-        rv = transformList->SetBaseValueString(aValue);
+        rv = transformList->SetBaseValueString(aValue, this);
         if (NS_FAILED(rv)) {
           transformList->ClearBaseValue();
         } else {
           aResult.SetTo(transformList->GetBaseValue(), &aValue);
           didSetResult = true;
         }
         foundMatch = true;
       } else if (aAttribute == nsGkAtoms::tabindex) {
diff --git a/layout/base/tests/mochitest.ini b/layout/base/tests/mochitest.ini
--- a/layout/base/tests/mochitest.ini
+++ b/layout/base/tests/mochitest.ini
@@ -145,16 +145,17 @@ support-files = bug1226904.html
 [test_bug1278021.html]
 [test_emulateMedium.html]
 [test_event_target_iframe_oop.html]
 skip-if = e10s # bug 1020135, nested oop iframes not supported
 support-files = bug921928_event_target_iframe_apps_oop.html
 [test_event_target_radius.html]
 skip-if = toolkit == 'android' # Bug 1355836
 [test_frame_reconstruction_for_pseudo_elements.html]
+[test_frame_reconstruction_for_svg_transforms.html]
 [test_frame_reconstruction_scroll_restore.html]
 [test_getBoxQuads_convertPointRectQuad.html]
 support-files =
   file_getBoxQuads_convertPointRectQuad_frame1.html
   file_getBoxQuads_convertPointRectQuad_frame2.html
 [test_getClientRects_emptytext.html]
 [test_mozPaintCount.html]
 skip-if = toolkit == 'android' # Requires plugin support
diff --git a/layout/base/tests/test_frame_reconstruction_for_svg_transforms.html b/layout/base/tests/test_frame_reconstruction_for_svg_transforms.html
new file mode 100644
--- /dev/null
+++ b/layout/base/tests/test_frame_reconstruction_for_svg_transforms.html
@@ -0,0 +1,46 @@
+<!DOCTYPE HTML>
+<html>
+<!--
+https://bugzilla.mozilla.org/show_bug.cgi?id=1419764
+-->
+<head>
+  <meta charset="utf-8">
+  <title>Test for Bug 1419764</title>
+  <script type="application/javascript" src="/tests/SimpleTest/SimpleTest.js"></script>
+  <link rel="stylesheet" type="text/css" href="/tests/SimpleTest/test.css"/>
+  <script type="application/javascript">
+
+  /** Test for Bug 1419764 **/
+
+  SimpleTest.waitForExplicitFinish();
+
+  function run() {
+    var utils = SpecialPowers.getDOMWindowUtils(window);
+    var rect = document.querySelector("rect");
+
+    var matrix = rect.transform.baseVal[0].matrix;
+
+    matrix.e = 100;
+    document.documentElement.offsetTop; // flush layout
+
+    var startcount = utils.framesConstructed;
+
+    matrix.e = 200;
+    document.documentElement.offsetTop; // flush layout
+
+    var endcount = utils.framesConstructed;
+    is(endcount, startcount, "should not do frame construction");
+    SimpleTest.finish();
+  }
+
+  </script>
+</head>
+<body onload="run()">
+<a target="_blank" href="https://bugzilla.mozilla.org/show_bug.cgi?id=1419764">Mozilla Bug 1419764</a>
+<svg>
+  <rect transform="translate(1 1)" width="20" height="20" fill="yellow"/>
+</svg>
+<pre id="test">
+</pre>
+</body>
+</html>
diff --git a/layout/reftests/svg/smil/transform/reftest.list b/layout/reftests/svg/smil/transform/reftest.list
--- a/layout/reftests/svg/smil/transform/reftest.list
+++ b/layout/reftests/svg/smil/transform/reftest.list
@@ -3,16 +3,16 @@
 
 fuzzy(111,1802) fuzzy-if(skiaContent,130,1000) == additive-1.svg additive-1-ref.svg # bug 981344, bug 1239766
 == animate-width-1.svg lime.svg
 fuzzy-if(cocoaWidget,1,32) fuzzy-if(winWidget,15,30) fuzzy-if(gtkWidget,1,30) == paced-1.svg paced-1-ref.svg # bug 981640, Bug 1293550
 fuzzy-if(skiaContent,7,90) == rotate-angle-1.svg rotate-angle-ref.svg
 fuzzy-if(skiaContent,7,90) == rotate-angle-2.svg rotate-angle-ref.svg
 fuzzy-if(skiaContent,7,130) == rotate-angle-3.svg rotate-angle-ref.svg
 fuzzy-if(skiaContent,7,90) == rotate-angle-4.svg rotate-angle-ref.svg
-fuzzy-if(skiaContent,1,130) == rotate-angle-5.svg rotate-angle-ref.svg
+fuzzy-if(skiaContent,7,60) == rotate-angle-5.svg rotate-angle-ref.svg
 fuzzy(12,27) fuzzy-if(skiaContent,1,180) fuzzy-if(Android,16,3) fuzzy-if(webrender,7-7,306-306) == scale-1.svg scale-1-ref.svg  # bug 981004
 == set-transform-1.svg lime.svg
 fuzzy-if(winWidget||gtkWidget||OSX,1,27) fuzzy-if(skiaContent,7,1548) == skew-1.svg skew-1-ref.svg # bug 983671, Bug 1260629
 == translate-clipPath-1.svg lime.svg
 == translate-gradient-1.svg lime.svg
 == translate-pattern-1.svg lime.svg
 == use-1.svg lime.svg
