# HG changeset patch
# User Tom Ritter <tom@mozilla.com>
# Date 1591800443 0
# Node ID 5877126012cdfc91d8cdcc374d48bb52c9641c77
# Parent  d22bc1280d8fdb6730efdec49fa8df7d45d2a881
Bug 1637845 - Add support for github and googlesource as hosting repositories r=glob

Differential Revision: https://phabricator.services.mozilla.com/D76430

diff --git a/python/mozbuild/mozbuild/vendor/host_github.py b/python/mozbuild/mozbuild/vendor/host_github.py
new file mode 100644
--- /dev/null
+++ b/python/mozbuild/mozbuild/vendor/host_github.py
@@ -0,0 +1,29 @@
+# This Source Code Form is subject to the terms of the Mozilla Public
+# License, v. 2.0. If a copy of the MPL was not distributed with this
+# file, # You can obtain one at http://mozilla.org/MPL/2.0/.
+
+from __future__ import absolute_import, print_function, unicode_literals
+
+import urllib
+import requests
+
+
+class GitHubHost:
+    def __init__(self, manifest):
+        self.manifest = manifest
+
+    def upstream_commit(self, revision):
+        """Query the github api for a git commit id and timestamp."""
+        github_api = "https://api.github.com/"
+        repo_url = urllib.parse.urlparse(self.manifest["origin"]["url"])
+        repo = repo_url.path[1:]
+        url = "/".join([github_api, "repos", repo, "commits", revision])
+        req = requests.get(url)
+        req.raise_for_status()
+        info = req.json()
+        return (info["sha"], info["commit"]["committer"]["date"])
+
+    def upstream_snapshot(self, revision):
+        return "/".join(
+            [self.manifest["origin"]["url"], "archive", revision + ".tar.gz"]
+        )
diff --git a/python/mozbuild/mozbuild/vendor/host_googlesource.py b/python/mozbuild/mozbuild/vendor/host_googlesource.py
new file mode 100644
--- /dev/null
+++ b/python/mozbuild/mozbuild/vendor/host_googlesource.py
@@ -0,0 +1,33 @@
+# This Source Code Form is subject to the terms of the Mozilla Public
+# License, v. 2.0. If a copy of the MPL was not distributed with this
+# file, # You can obtain one at http://mozilla.org/MPL/2.0/.
+
+from __future__ import absolute_import, print_function, unicode_literals
+
+import requests
+
+
+class GoogleSourceHost:
+    def __init__(self, manifest):
+        self.manifest = manifest
+
+    def upstream_commit(self, revision):
+        """Query for a git commit and timestamp."""
+        url = "/".join([self.manifest["origin"]["url"], "+", revision + "?format=JSON"])
+        req = requests.get(url)
+        req.raise_for_status()
+        try:
+            info = req.json()
+        except ValueError:
+            # As of 2017 May, googlesource sends 4 garbage characters
+            # at the beginning of the json response. Work around this.
+            # https://bugs.chromium.org/p/chromium/issues/detail?id=718550
+            import json
+
+            info = json.loads(req.text[4:])
+        return (info["commit"], info["committer"]["time"])
+
+    def upstream_snapshot(self, revision):
+        return "/".join(
+            [self.manifest["origin"]["url"], "+archive", revision + ".tar.gz"]
+        )
diff --git a/python/mozbuild/mozbuild/vendor/moz_yaml.py b/python/mozbuild/mozbuild/vendor/moz_yaml.py
--- a/python/mozbuild/mozbuild/vendor/moz_yaml.py
+++ b/python/mozbuild/mozbuild/vendor/moz_yaml.py
@@ -52,17 +52,17 @@ VALID_LICENSES = [
     # Unique Licenses
     "ACE",  # http://www.cs.wustl.edu/~schmidt/ACE-copying.html
     "Anti-Grain-Geometry",  # http://www.antigrain.com/license/index.html
     "JPNIC",  # https://www.nic.ad.jp/ja/idn/idnkit/download/index.html
     "Khronos",  # https://www.khronos.org/openmaxdl
     "Unicode",  # http://www.unicode.org/copyright.html
 ]
 
-VALID_SOURCE_HOSTS = ["gitlab"]
+VALID_SOURCE_HOSTS = ["gitlab", "googlesource", "github"]
 
 """
 ---
 # Third-Party Library Template
 # All fields are mandatory unless otherwise noted
 
 # Version of this schema
 schema: 1
diff --git a/python/mozbuild/mozbuild/vendor/vendor_manifest.py b/python/mozbuild/mozbuild/vendor/vendor_manifest.py
--- a/python/mozbuild/mozbuild/vendor/vendor_manifest.py
+++ b/python/mozbuild/mozbuild/vendor/vendor_manifest.py
@@ -75,18 +75,25 @@ class VendorManifest(MozbuildObject):
             "done",
             {"revision": revision},
             "Update to version '{revision}' ready to commit.",
         )
 
     def get_source_host(self):
         if self.manifest["vendoring"]["source-hosting"] == "gitlab":
             from mozbuild.vendor.host_gitlab import GitLabHost
+            return GitLabHost(self.manifest)
+        elif self.manifest["vendoring"]["source-hosting"] == "github":
+            from mozbuild.vendor.host_github import GitHubHost
 
-            return GitLabHost(self.manifest)
+            return GitHubHost(self.manifest)
+        elif self.manifest["vendoring"]["source-hosting"] == "googlesource":
+            from mozbuild.vendor.host_googlesource import GoogleSourceHost
+
+            return GoogleSourceHost(self.manifest)
         else:
             raise Exception(
                 "Unknown source host: " + self.manifest["vendoring"]["source-hosting"]
             )
 
     def fetch_and_unpack(self, revision):
         """Fetch and unpack upstream source"""
         url = self.source_host.upstream_snapshot(revision)

