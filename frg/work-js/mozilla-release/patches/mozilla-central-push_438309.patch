# HG changeset patch
# User Nazim Can Altinova <canaltinova@gmail.com>
# Date 1537972818 0
#      Wed Sep 26 14:40:18 2018 +0000
# Node ID c9e9863b9e202a7eaeaa1ca48b163a833eab2767
# Parent  753cec0ea42c454a27c17381ef22a542b9f747d6
Bug 1490794 - Add profiling stack frame for script compilation r=jandem

Differential Revision: https://phabricator.services.mozilla.com/D5861

diff --git a/js/src/jit/BaselineJIT.cpp b/js/src/jit/BaselineJIT.cpp
--- a/js/src/jit/BaselineJIT.cpp
+++ b/js/src/jit/BaselineJIT.cpp
@@ -21,16 +21,17 @@
 #include "vm/Interpreter.h"
 #include "vm/TraceLogging.h"
 #include "wasm/WasmInstance.h"
 
 #include "gc/PrivateIterators-inl.h"
 #include "jit/JitFrames-inl.h"
 #include "jit/MacroAssembler-inl.h"
 #include "vm/BytecodeUtil-inl.h"
+#include "vm/GeckoProfiler-inl.h"
 #include "vm/JSObject-inl.h"
 #include "vm/JSScript-inl.h"
 #include "vm/Stack-inl.h"
 
 using mozilla::BinarySearchIf;
 using mozilla::DebugOnly;
 
 using namespace js;
@@ -247,16 +248,17 @@ jit::EnterBaselineAtBranch(JSContext* cx
 
 MethodStatus
 jit::BaselineCompile(JSContext* cx, JSScript* script, bool forceDebugInstrumentation)
 {
     cx->check(script);
     MOZ_ASSERT(!script->hasBaselineScript());
     MOZ_ASSERT(script->canBaselineCompile());
     MOZ_ASSERT(IsBaselineEnabled(cx));
+    AutoGeckoProfilerEntry pseudoFrame(cx, "Baseline script compilation");
 
     script->ensureNonLazyCanonicalFunction();
 
     TempAllocator temp(&cx->tempLifoAlloc());
     JitContext jctx(cx, nullptr);
 
     BaselineCompiler compiler(cx, temp, script);
     if (!compiler.init()) {
diff --git a/js/src/jit/Ion.cpp b/js/src/jit/Ion.cpp
--- a/js/src/jit/Ion.cpp
+++ b/js/src/jit/Ion.cpp
@@ -55,16 +55,17 @@
 #include "vtune/VTuneWrapper.h"
 
 #include "gc/PrivateIterators-inl.h"
 #include "jit/JitFrames-inl.h"
 #include "jit/MacroAssembler-inl.h"
 #include "jit/shared/Lowering-shared-inl.h"
 #include "vm/Debugger-inl.h"
 #include "vm/EnvironmentObject-inl.h"
+#include "vm/GeckoProfiler-inl.h"
 #include "vm/JSObject-inl.h"
 #include "vm/JSScript-inl.h"
 #include "vm/Realm-inl.h"
 #include "vm/Stack-inl.h"
 
 #if defined(ANDROID)
 # include <sys/system_properties.h>
 #endif
@@ -2369,16 +2370,17 @@ GetOptimizationLevel(HandleScript script
 
 static MethodStatus
 Compile(JSContext* cx, HandleScript script, BaselineFrame* osrFrame, jsbytecode* osrPc,
         bool forceRecompile = false)
 {
     MOZ_ASSERT(jit::IsIonEnabled(cx));
     MOZ_ASSERT(jit::IsBaselineEnabled(cx));
     MOZ_ASSERT_IF(osrPc != nullptr, LoopEntryCanIonOsr(osrPc));
+    AutoGeckoProfilerEntry pseudoFrame(cx, "Ion script compilation");
 
     if (!script->hasBaselineScript()) {
         return Method_Skipped;
     }
 
     if (script->isDebuggee() || (osrFrame && osrFrame->isDebuggee())) {
         TrackAndSpewIonAbort(cx, script, "debugging");
         return Method_Skipped;
