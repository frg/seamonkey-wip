# HG changeset patch
# User Matthew Gaudet <mgaudet@mozilla.com>
# Date 1519327761 18000
#      Thu Feb 22 14:29:21 2018 -0500
# Node ID 591a2c93a43e59ef7cf6d5fe86268a1e8315f1f1
# Parent  d791b48e93d3c6bea4b77944f769ded17eb329f9
Bug 1341261: [Part 3] Inline cache for comparing strictly different types r=tcampbell

diff --git a/js/src/jit-test/tests/cacheir/compare.js b/js/src/jit-test/tests/cacheir/compare.js
new file mode 100644
--- /dev/null
+++ b/js/src/jit-test/tests/cacheir/compare.js
@@ -0,0 +1,16 @@
+function warmup(fun, input, expected) {
+    assertEq(input.length, expected.length);
+    for (var i = 0; i < 30; i++) {
+        for (var j = 0; j < input.length; j++) {
+            lhs = input[j][0];
+            rhs = input[j][1];
+            assertEq(fun(lhs,rhs), expected[j]);
+        }
+    }
+}
+
+var strictCompare = function(a,b) { return a === b; }
+warmup(strictCompare, [[1,1], [3,3], [3,strictCompare],[strictCompare, {}], [3.2, 1],
+                       [0, -0]],
+                       [true, true,  false, false, false,
+                       true]);
\ No newline at end of file
diff --git a/js/src/jit/CacheIR.cpp b/js/src/jit/CacheIR.cpp
--- a/js/src/jit/CacheIR.cpp
+++ b/js/src/jit/CacheIR.cpp
@@ -4696,16 +4696,41 @@ CompareIRGenerator::tryAttachSymbol(ValO
     writer.compareSymbolResult(op_, lhsSymId, rhsSymId);
     writer.returnFromIC();
 
     trackAttached("Symbol");
     return true;
 }
 
 bool
+CompareIRGenerator::tryAttachStrictDifferentTypes(ValOperandId lhsId, ValOperandId rhsId)
+{
+    MOZ_ASSERT(IsEqualityOp(op_));
+
+    if (op_ != JSOP_STRICTEQ && op_ != JSOP_STRICTNE)
+        return false;
+
+    // Probably can't hit some of these.
+    if (SameType(lhsVal_, rhsVal_) || (lhsVal_.isNumber() && rhsVal_.isNumber()))
+        return false;
+
+    // Compare tags
+    ValueTagOperandId lhsTypeId = writer.loadValueTag(lhsId);
+    ValueTagOperandId rhsTypeId = writer.loadValueTag(rhsId);
+    writer.guardTagNotEqual(lhsTypeId, rhsTypeId);
+
+    // Now that we've passed the guard, we know differing types, so return the bool result.
+    writer.loadBooleanResult(op_ == JSOP_STRICTNE ? true : false);
+    writer.returnFromIC();
+
+    trackAttached("StrictDifferentTypes");
+    return true;
+}
+
+bool
 CompareIRGenerator::tryAttachStub()
 {
     MOZ_ASSERT(cacheKind_ == CacheKind::Compare);
     MOZ_ASSERT(IsEqualityOp(op_) ||
                op_ == JSOP_LE || op_ == JSOP_LT ||
                op_ == JSOP_GE || op_ == JSOP_GT);
 
     AutoAssertNoPendingException aanpe(cx_);
@@ -4715,16 +4740,18 @@ CompareIRGenerator::tryAttachStub()
 
     if (IsEqualityOp(op_)) {
         if (tryAttachString(lhsId, rhsId))
             return true;
         if (tryAttachObject(lhsId, rhsId))
             return true;
         if (tryAttachSymbol(lhsId, rhsId))
             return true;
+        if (tryAttachStrictDifferentTypes(lhsId, rhsId))
+            return true;
 
         trackAttached(IRGenerator::NotAttached);
         return false;
     }
 
     trackAttached(IRGenerator::NotAttached);
     return false;
 }
diff --git a/js/src/jit/CacheIR.h b/js/src/jit/CacheIR.h
--- a/js/src/jit/CacheIR.h
+++ b/js/src/jit/CacheIR.h
@@ -1673,16 +1673,17 @@ class MOZ_RAII CompareIRGenerator : publ
 {
     JSOp op_;
     HandleValue lhsVal_;
     HandleValue rhsVal_;
 
     bool tryAttachString(ValOperandId lhsId, ValOperandId rhsId);
     bool tryAttachObject(ValOperandId lhsId, ValOperandId rhsId);
     bool tryAttachSymbol(ValOperandId lhsId, ValOperandId rhsId);
+    bool tryAttachStrictDifferentTypes(ValOperandId lhsId, ValOperandId rhsId);
 
     void trackAttached(const char* name);
 
   public:
     CompareIRGenerator(JSContext* cx, HandleScript, jsbytecode* pc, ICState::Mode mode,
                        JSOp op, HandleValue lhsVal, HandleValue rhsVal);
 
     bool tryAttachStub();
