# HG changeset patch
# User Ted Campbell <tcampbell@mozilla.com>
# Date 1511041999 18000
#      Sat Nov 18 16:53:19 2017 -0500
# Node ID 384537aaeeb8cebfc1a2db299d55445a9e17cc68
# Parent  cd72109e796052ed22255267c3cb97372db44662
Bug 1419094 - Cleanup some XDR apis. r=nbp

Avoid declaring XDR core types in vm/JSAtoms.h. Remove codeConstValue
from XDR API.

MozReview-Commit-ID: GQbLocJbW6H

diff --git a/js/src/vm/JSAtom.h b/js/src/vm/JSAtom.h
--- a/js/src/vm/JSAtom.h
+++ b/js/src/vm/JSAtom.h
@@ -5,17 +5,16 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef vm_JSAtom_h
 #define vm_JSAtom_h
 
 #include "mozilla/Maybe.h"
 
 #include "gc/Rooting.h"
-#include "js/Result.h"
 #include "js/TypeDecls.h"
 #include "vm/CommonPropertyNames.h"
 
 class JSAutoByteString;
 
 namespace js {
 
 /*
@@ -38,20 +37,16 @@ JS_FOR_EACH_PROTOTYPE(DECLARE_PROTO_STR)
 FOR_EACH_COMMON_PROPERTYNAME(DECLARE_CONST_CHAR_STR)
 #undef DECLARE_CONST_CHAR_STR
 
 /* Constant strings that are not atomized. */
 extern const char js_getter_str[];
 extern const char js_send_str[];
 extern const char js_setter_str[];
 
-namespace JS {
-enum TranscodeResult : uint8_t;
-}
-
 namespace js {
 
 class AutoLockForExclusiveAccess;
 
 /*
  * Atom tracing and garbage collection hooks.
  */
 void
@@ -85,29 +80,21 @@ AtomizeUTF8Chars(JSContext* cx, const ch
 
 extern JSAtom*
 AtomizeString(JSContext* cx, JSString* str, js::PinningBehavior pin = js::DoNotPinAtom);
 
 template <AllowGC allowGC>
 extern JSAtom*
 ToAtom(JSContext* cx, typename MaybeRooted<JS::Value, allowGC>::HandleType v);
 
-enum XDRMode {
-    XDR_ENCODE,
-    XDR_DECODE
-};
-
-template <XDRMode mode>
-class XDRState;
-
-using XDRResult = mozilla::Result<mozilla::Ok, JS::TranscodeResult>;
-
-template<XDRMode mode>
-XDRResult
-XDRAtom(XDRState<mode>* xdr, js::MutableHandleAtom atomp);
+// These functions are declared in vm/Xdr.h
+//
+// template<XDRMode mode>
+// XDRResult
+// XDRAtom(XDRState<mode>* xdr, js::MutableHandleAtom atomp);
 
 extern JS::Handle<PropertyName*>
 ClassName(JSProtoKey key, JSContext* cx);
 
 namespace gc {
 void MergeAtomsAddedWhileSweeping(JSRuntime* rt);
 } // namespace gc
 
diff --git a/js/src/vm/JSObject.cpp b/js/src/vm/JSObject.cpp
--- a/js/src/vm/JSObject.cpp
+++ b/js/src/vm/JSObject.cpp
@@ -1433,17 +1433,17 @@ js::XDRObjectLiteral(XDRState<mode>* xdr
         if (mode == XDR_ENCODE)
             initialized = values.length();
         MOZ_TRY(xdr->codeUint32(&initialized));
         if (mode == XDR_DECODE && !values.appendN(MagicValue(JS_ELEMENTS_HOLE), initialized))
             return xdr->fail(JS::TranscodeResult_Throw);
 
         // Recursively copy dense elements.
         for (unsigned i = 0; i < initialized; i++)
-            MOZ_TRY(xdr->codeConstValue(values[i]));
+            MOZ_TRY(XDRScriptConst(xdr, values[i]));
 
         uint32_t copyOnWrite;
         if (mode == XDR_ENCODE) {
             copyOnWrite = obj->is<ArrayObject>() &&
                           obj->as<ArrayObject>().denseElementsAreCopyOnWrite();
         }
         MOZ_TRY(xdr->codeUint32(&copyOnWrite));
 
@@ -1472,18 +1472,18 @@ js::XDRObjectLiteral(XDRState<mode>* xdr
         return xdr->fail(JS::TranscodeResult_Throw);
 
     for (size_t i = 0; i < nproperties; i++) {
         if (mode == XDR_ENCODE) {
             tmpIdValue = IdToValue(properties[i].get().id);
             tmpValue = properties[i].get().value;
         }
 
-        MOZ_TRY(xdr->codeConstValue(&tmpIdValue));
-        MOZ_TRY(xdr->codeConstValue(&tmpValue));
+        MOZ_TRY(XDRScriptConst(xdr, &tmpIdValue));
+        MOZ_TRY(XDRScriptConst(xdr, &tmpValue));
 
         if (mode == XDR_DECODE) {
             if (!ValueToId<CanGC>(cx, tmpIdValue, &tmpId))
                 return xdr->fail(JS::TranscodeResult_Throw);
             properties[i].get().id = tmpId;
             properties[i].get().value = tmpValue;
         }
     }
diff --git a/js/src/vm/Scope.cpp b/js/src/vm/Scope.cpp
--- a/js/src/vm/Scope.cpp
+++ b/js/src/vm/Scope.cpp
@@ -219,17 +219,17 @@ XDRBindingName(XDRState<XDR_ENCODE>* xdr
     JSContext* cx = xdr->cx();
 
     RootedAtom atom(cx, bindingName->name());
     bool hasAtom = !!atom;
 
     uint8_t u8 = uint8_t(hasAtom << 1) | uint8_t(bindingName->closedOver());
     MOZ_TRY(xdr->codeUint8(&u8));
 
-    if (atom)
+    if (hasAtom)
         MOZ_TRY(XDRAtom(xdr, &atom));
 
     return Ok();
 }
 
 static XDRResult
 XDRBindingName(XDRState<XDR_DECODE>* xdr, BindingName* bindingName)
 {
@@ -277,27 +277,27 @@ Scope::XDRSizedBindingNames(XDRState<mod
         data.set(&scope->data());
     } else {
         data.set(NewEmptyScopeData<ConcreteScope>(cx, length).release());
         if (!data)
             return xdr->fail(JS::TranscodeResult_Throw);
         data->length = length;
     }
 
-    for (uint32_t i = 0; i < length; i++) {
-        auto guard = mozilla::MakeScopeExit([&] {
-            if (mode == XDR_DECODE) {
-                DeleteScopeData(data.get());
-                data.set(nullptr);
-            }
-        });
+    auto dataGuard = mozilla::MakeScopeExit([&] () {
+        if (mode == XDR_DECODE) {
+            DeleteScopeData(data.get());
+            data.set(nullptr);
+        }
+    });
+
+    for (uint32_t i = 0; i < length; i++)
         MOZ_TRY(XDRBindingName(xdr, &data->names[i]));
-        guard.release();
-    }
 
+    dataGuard.release();
     return Ok();
 }
 
 /* static */ Scope*
 Scope::create(JSContext* cx, ScopeKind kind, HandleScope enclosing, HandleShape envShape)
 {
     Scope* scope = Allocate<Scope>(cx);
     if (scope)
diff --git a/js/src/vm/Xdr.cpp b/js/src/vm/Xdr.cpp
--- a/js/src/vm/Xdr.cpp
+++ b/js/src/vm/Xdr.cpp
@@ -188,23 +188,16 @@ XDRState<mode>::codeScript(MutableHandle
     MOZ_TRY(VersionCheck(this));
     MOZ_TRY(XDRScript(this, nullptr, nullptr, nullptr, scriptp));
     MOZ_TRY(codeAlign(sizeof(js::XDRAlignment)));
 
     guard.release();
     return Ok();
 }
 
-template<XDRMode mode>
-XDRResult
-XDRState<mode>::codeConstValue(MutableHandleValue vp)
-{
-    return XDRScriptConst(this, vp);
-}
-
 template class js::XDRState<XDR_ENCODE>;
 template class js::XDRState<XDR_DECODE>;
 
 AutoXDRTree::AutoXDRTree(XDRCoderBase* xdr, AutoXDRTree::Key key)
   : key_(key),
     parent_(this),
     xdr_(xdr)
 {
diff --git a/js/src/vm/Xdr.h b/js/src/vm/Xdr.h
--- a/js/src/vm/Xdr.h
+++ b/js/src/vm/Xdr.h
@@ -15,16 +15,23 @@
 
 #include "js/TypeDecls.h"
 #include "vm/JSAtom.h"
 
 namespace js {
 
 class LifoAlloc;
 
+enum XDRMode {
+    XDR_ENCODE,
+    XDR_DECODE
+};
+
+using XDRResult = mozilla::Result<mozilla::Ok, JS::TranscodeResult>;
+
 class XDRBufferBase
 {
   public:
     explicit XDRBufferBase(JSContext* cx, size_t cursor = 0)
       : context_(cx), cursor_(cursor)
 #ifdef DEBUG
         // Note, when decoding the buffer can be set to a range, which does not
         // have any alignment requirement as opposed to allocations.
@@ -455,17 +462,16 @@ class XDRState : public XDRCoderBase
         return Ok();
     }
 
     XDRResult codeChars(const JS::Latin1Char* chars, size_t nchars);
     XDRResult codeChars(char16_t* chars, size_t nchars);
 
     XDRResult codeFunction(JS::MutableHandleFunction objp, HandleScriptSource sourceObject = nullptr);
     XDRResult codeScript(MutableHandleScript scriptp);
-    XDRResult codeConstValue(MutableHandleValue vp);
 };
 
 using XDREncoder = XDRState<XDR_ENCODE>;
 using XDRDecoder = XDRState<XDR_DECODE>;
 
 class XDROffThreadDecoder : public XDRDecoder
 {
     const ReadOnlyCompileOptions* options_;
@@ -593,11 +599,15 @@ class XDRIncrementalEncoder : public XDR
     void createOrReplaceSubTree(AutoXDRTree* child) override;
     void endSubTree() override;
 
     // Append the content collected during the incremental encoding into the
     // buffer given as argument.
     XDRResult linearize(JS::TranscodeBuffer& buffer);
 };
 
+template<XDRMode mode>
+XDRResult
+XDRAtom(XDRState<mode>* xdr, js::MutableHandleAtom atomp);
+
 } /* namespace js */
 
 #endif /* vm_Xdr_h */
