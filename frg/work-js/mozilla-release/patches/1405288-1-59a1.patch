# HG changeset patch
# User Patrick Brosset <pbrosset@mozilla.com>
# Date 1508403450 -7200
# Node ID 16c50fd548b688ab00ed16eb7bd09997a845ee2c
# Parent  0950cdf91fafd28a2e9e1f2667eba751994f2401
Bug 1405288 - Remove resolveRelativeURL actor method check from the inspector r=ochameau

The resolveRelativeURL actor method was added in bug 921102 which shipped
with Firefox 40.
This was 3 years ago, and well older than the latest current ESR (52), which
is the latest version we support.

MozReview-Commit-ID: 5X5czLP5v2E

diff --git a/devtools/client/inspector/inspector.js b/devtools/client/inspector/inspector.js
--- a/devtools/client/inspector/inspector.js
+++ b/devtools/client/inspector/inspector.js
@@ -216,31 +216,27 @@ Inspector.prototype = {
   },
 
   /**
    * Figure out what features the backend supports
    */
   _detectActorFeatures: function () {
     this._supportsDuplicateNode = false;
     this._supportsScrollIntoView = false;
-    this._supportsResolveRelativeURL = false;
 
     // Use getActorDescription first so that all actorHasMethod calls use
     // a cached response from the server.
     return this._target.getActorDescription("domwalker").then(desc => {
       return promise.all([
         this._target.actorHasMethod("domwalker", "duplicateNode").then(value => {
           this._supportsDuplicateNode = value;
         }).catch(console.error),
         this._target.actorHasMethod("domnode", "scrollIntoView").then(value => {
           this._supportsScrollIntoView = value;
-        }).catch(console.error),
-        this._target.actorHasMethod("inspector", "resolveRelativeURL").then(value => {
-          this._supportsResolveRelativeURL = value;
-        }).catch(console.error),
+        }).catch(console.error)
       ]);
     });
   },
 
   _deferredOpen: async function (defaultSelection) {
     this.breadcrumbs = new HTMLBreadcrumbs(this);
 
     this.walker.on("new-root", this.onNewRoot);
@@ -1580,18 +1576,17 @@ Inspector.prototype = {
 
     // Get information about the right-clicked node.
     let popupNode = this.contextMenuTarget;
     if (!popupNode || !popupNode.classList.contains("link")) {
       return [linkFollow, linkCopy];
     }
 
     let type = popupNode.dataset.type;
-    if (this._supportsResolveRelativeURL &&
-        (type === "uri" || type === "cssresource" || type === "jsresource")) {
+    if ((type === "uri" || type === "cssresource" || type === "jsresource")) {
       // Links can't be opened in new tabs in the browser toolbox.
       if (type === "uri" && !this.target.chrome) {
         linkFollow.visible = true;
         linkFollow.label = INSPECTOR_L10N.getStr(
           "inspector.menu.openUrlInNewTab.label");
       } else if (type === "cssresource") {
         linkFollow.visible = true;
         linkFollow.label = TOOLBOX_L10N.getStr(
@@ -2107,18 +2102,16 @@ Inspector.prototype = {
    */
   followAttributeLink: function (type, link) {
     if (!type || !link) {
       return;
     }
 
     if (type === "uri" || type === "cssresource" || type === "jsresource") {
       // Open link in a new tab.
-      // When the inspector menu was setup on click (see _getNodeLinkMenuItems), we
-      // already checked that resolveRelativeURL existed.
       this.inspector.resolveRelativeURL(
         link, this.selection.nodeFront).then(url => {
           if (type === "uri") {
             let browserWin = this.target.tab.ownerDocument.defaultView;
             browserWin.openUILinkIn(url, "tab");
           } else if (type === "cssresource") {
             return this.toolbox.viewSourceInStyleEditor(url);
           } else if (type === "jsresource") {
@@ -2149,18 +2142,16 @@ Inspector.prototype = {
 
     this.copyAttributeLink(link);
   },
 
   /**
    * This method is here for the benefit of copying links.
    */
   copyAttributeLink: function (link) {
-    // When the inspector menu was setup on click (see _getNodeLinkMenuItems), we
-    // already checked that resolveRelativeURL existed.
     this.inspector.resolveRelativeURL(link, this.selection.nodeFront).then(url => {
       clipboardHelper.copyString(url);
     }, console.error);
   },
 
   /**
    * Returns an object containing the shared handler functions used in the box
    * model and grid React components.
