# HG changeset patch
# User Edwin Takahashi <egao@mozilla.com>
# Date 1574889138 0
# Node ID 17386ce6dd6f18f081d9915ab45618e471b8637c
# Parent  62e75880bc8f148d2e32fb153a6c5ca0edadaf97
Bug 1428718 - make moztest syntax python3 compatible r=gbrown,mozbase

Changes:

Replace `<list>.items()` calls with `six.iteritems()`.

Remove `try/except` handling of `unittest` import as we have standardized on python2.7 in CI.

Use `six` to handle metaclass changes while python2/3 intercompatibility is required.

Differential Revision: https://phabricator.services.mozilla.com/D54376

diff --git a/testing/mozbase/moztest/moztest/adapters/unit.py b/testing/mozbase/moztest/moztest/adapters/unit.py
--- a/testing/mozbase/moztest/moztest/adapters/unit.py
+++ b/testing/mozbase/moztest/moztest/adapters/unit.py
@@ -4,21 +4,17 @@
 
 from __future__ import absolute_import
 
 import unittest
 import sys
 import time
 import traceback
 
-try:
-    from unittest import TextTestResult
-except ImportError:
-    # bug 971243 - python 2.6 compatibilty
-    from unittest import _TextTestResult as TextTestResult
+from unittest import TextTestResult
 
 """Adapter used to output structuredlog messages from unittest
 testsuites"""
 
 
 def get_test_class_name(test):
     """
     This method is used to return the full class name from a
diff --git a/testing/mozbase/moztest/moztest/resolve.py b/testing/mozbase/moztest/moztest/resolve.py
--- a/testing/mozbase/moztest/moztest/resolve.py
+++ b/testing/mozbase/moztest/moztest/resolve.py
@@ -3,16 +3,19 @@
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import, print_function, unicode_literals
 
 import fnmatch
 import os
 import pickle
 import sys
+
+import six
+
 from abc import ABCMeta, abstractmethod
 from collections import defaultdict
 
 import mozpack.path as mozpath
 from manifestparser import combine_fields, TestManifest
 from mozbuild.base import MozbuildObject
 from mozbuild.testing import TEST_MANIFESTS, REFTEST_FLAVORS
 from mozbuild.util import OrderedDefaultDict
@@ -345,18 +348,18 @@ def rewrite_test_base(test, new_base, ho
         test['path'] = mozpath.join(new_base, test['dir_relpath'],
                                     test['install-to-subdir'], manifest_relpath)
     else:
         test['path'] = mozpath.join(new_base, test['file_relpath'])
 
     return test
 
 
+@six.add_metaclass(ABCMeta)
 class TestLoader(MozbuildObject):
-    __metaclass__ = ABCMeta
 
     @abstractmethod
     def __call__(self):
         """Generate test metadata."""
 
 
 class BuildBackendLoader(TestLoader):
     def __call__(self):
@@ -386,17 +389,17 @@ class BuildBackendLoader(TestLoader):
         test_defaults = os.path.join(self.topobjdir, 'test-defaults.pkl')
 
         with open(all_tests, 'rb') as fh:
             test_data = pickle.load(fh)
 
         with open(test_defaults, 'rb') as fh:
             defaults = pickle.load(fh)
 
-        for path, tests in test_data.items():
+        for path, tests in six.iteritems(test_data):
             for metadata in tests:
                 defaults_manifests = [metadata['manifest']]
 
                 ancestor_manifest = metadata.get('ancestor-manifest')
                 if ancestor_manifest:
                     # The (ancestor manifest, included manifest) tuple
                     # contains the defaults of the included manifest, so
                     # use it instead of [metadata['manifest']].
@@ -411,18 +414,22 @@ class BuildBackendLoader(TestLoader):
                 yield metadata
 
 
 class TestManifestLoader(TestLoader):
     def __init__(self, *args, **kwargs):
         super(TestManifestLoader, self).__init__(*args, **kwargs)
         self.finder = FileFinder(self.topsrcdir)
         self.reader = self.mozbuild_reader(config_mode="empty")
-        self.variables = {'{}_MANIFESTS'.format(k): v[0] for k, v in TEST_MANIFESTS.items()}
-        self.variables.update({'{}_MANIFESTS'.format(f.upper()): f for f in REFTEST_FLAVORS})
+        self.variables = {
+            '{}_MANIFESTS'.format(k): v[0] for k, v in six.iteritems(TEST_MANIFESTS)
+        }
+        self.variables.update({
+            '{}_MANIFESTS'.format(f.upper()): f for f in REFTEST_FLAVORS
+        })
 
     def _load_manifestparser_manifest(self, mpath):
         mp = TestManifest(manifests=[mpath], strict=True, rootdir=self.topsrcdir,
                           finder=self.finder, handle_defaults=True)
         return (test for test in mp.tests)
 
     def _load_reftest_manifest(self, mpath):
         import reftest
@@ -643,17 +650,17 @@ class TestResolver(MozbuildObject):
 
         manifests = manifestupdate.run(self.topsrcdir, self.topobjdir, rebuild=False,
                                        download=True, config_path=None, rewrite_config=True,
                                        update=True, logger=logger)
         if not manifests:
             print("Loading wpt manifest failed")
             return
 
-        for manifest, data in manifests.iteritems():
+        for manifest, data in six.iteritems(manifests):
             tests_root = data["tests_path"]
             for test_type, path, tests in manifest:
                 full_path = os.path.join(tests_root, path)
                 src_path = os.path.relpath(full_path, self.topsrcdir)
                 if test_type not in ["testharness", "reftest", "wdspec"]:
                     continue
                 for test in tests:
                     self._tests.append({
@@ -742,17 +749,17 @@ class TestResolver(MozbuildObject):
 
     def get_outgoing_metadata(self):
         paths, tags, flavors = set(), set(), set()
         changed_files = self.repository.get_outgoing_files('AM')
         if changed_files:
             reader = self.mozbuild_reader(config_mode='empty')
             files_info = reader.files_info(changed_files)
 
-            for path, info in files_info.items():
+            for path, info in six.iteritems(files_info):
                 paths |= info.test_files
                 tags |= info.test_tags
                 flavors |= info.test_flavors
 
         return {
             'paths': paths,
             'tags': tags,
             'flavors': flavors,
@@ -768,17 +775,17 @@ class TestResolver(MozbuildObject):
 
         for entry in what:
             # If the path matches the name or alias of an entire suite, run
             # the entire suite.
             if entry in TEST_SUITES:
                 run_suites.add(entry)
                 continue
             suitefound = False
-            for suite, v in TEST_SUITES.items():
+            for suite, v in six.iteritems(TEST_SUITES):
                 if entry.lower() in v.get('aliases', []):
                     run_suites.add(suite)
                     suitefound = True
             if suitefound:
                 continue
 
             # Now look for file/directory matches in the TestResolver.
             relpath = self._wrap_path_argument(entry).relpath()
diff --git a/testing/mozbase/moztest/moztest/results.py b/testing/mozbase/moztest/moztest/results.py
--- a/testing/mozbase/moztest/moztest/results.py
+++ b/testing/mozbase/moztest/moztest/results.py
@@ -1,18 +1,18 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this file,
 # You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import
 
+import os
 import time
-import os
 
-from six import string_types
+import six
 
 import mozinfo
 
 
 class TestContext(object):
     """ Stores context data about the test """
 
     attrs = ['hostname', 'arch', 'env', 'os', 'os_version', 'tree', 'revision',
@@ -45,17 +45,17 @@ class TestContext(object):
             return False
         diffs = [a for a in self.attrs if getattr(self, a) != getattr(other, a)]
         return len(diffs) == 0
 
     def __hash__(self):
         def get(attr):
             value = getattr(self, attr)
             if isinstance(value, dict):
-                value = frozenset(value.items())
+                value = frozenset(six.iteritems(value))
             return value
         return hash(frozenset([get(a) for a in self.attrs]))
 
 
 class TestResult(object):
     """ Stores test result data """
 
     FAIL_RESULTS = [
@@ -83,17 +83,17 @@ class TestResult(object):
         time_start = timestamp (seconds since UNIX epoch) of when the test started
                      running; if not provided, defaults to the current time
                      ! Provide 0 if you only have the duration
         context = TestContext instance; can be None
         result_expected = string representing the expected outcome of the test"""
 
         msg = "Result '%s' not in possible results: %s" %\
               (result_expected, ', '.join(self.POSSIBLE_RESULTS))
-        assert isinstance(name, string_types), "name has to be a string"
+        assert isinstance(name, six.string_types), "name has to be a string"
         assert result_expected in self.POSSIBLE_RESULTS, msg
 
         self.name = name
         self.test_class = test_class
         self.context = context
         self.time_start = time_start if time_start is not None else time.time()
         self.time_end = None
         self._result_expected = result_expected
@@ -169,17 +169,17 @@ class TestResult(object):
             self.result = result
         else:
             valid = self.POSSIBLE_RESULTS + self.COMPUTED_RESULTS
             msg = "Result '%s' not valid. Need one of: %s" %\
                   (result, ', '.join(valid))
             raise ValueError(msg)
 
         # use lists instead of multiline strings
-        if isinstance(output, string_types):
+        if isinstance(output, six.string_types):
             output = output.splitlines()
 
         self.time_end = time_end if time_end is not None else time.time()
         self.output = output or self.output
         self.reason = reason
 
     @property
     def finished(self):
diff --git a/testing/mozbase/moztest/setup.py b/testing/mozbase/moztest/setup.py
--- a/testing/mozbase/moztest/setup.py
+++ b/testing/mozbase/moztest/setup.py
@@ -1,27 +1,31 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this file,
 # You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import
 
 from setuptools import setup, find_packages
 
-PACKAGE_VERSION = '0.8'
+PACKAGE_VERSION = '1.0.0'
 
 # dependencies
 deps = ['mozinfo']
 
 setup(name='moztest',
       version=PACKAGE_VERSION,
       description="Package for storing and outputting Mozilla test results",
       long_description="see https://firefox-source-docs.mozilla.org/mozbase/index.html",
-      classifiers=['Programming Language :: Python :: 2.7',
-                   'Programming Language :: Python :: 2 :: Only'],
+      classifiers=[
+            "Programming Language :: Python :: 2.7",
+            "Programming Language :: Python :: 3",
+            "Programming Language :: Python :: 3.5",
+            "Development Status :: 5 - Production/Stable",
+      ],
       # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
       keywords='mozilla',
       author='Mozilla Automation and Tools team',
       author_email='tools@lists.mozilla.org',
       url='https://wiki.mozilla.org/Auto-tools/Projects/Mozbase',
       license='MPL',
       packages=find_packages(),
       include_package_data=True,
diff --git a/testing/mozbase/moztest/tests/manifest.ini b/testing/mozbase/moztest/tests/manifest.ini
--- a/testing/mozbase/moztest/tests/manifest.ini
+++ b/testing/mozbase/moztest/tests/manifest.ini
@@ -1,5 +1,5 @@
 [DEFAULT]
 subsuite = mozbase
-skip-if = python == 3
 [test.py]
 [test_resolve.py]
+skip-if = python == 3 # bug 1599261
\ No newline at end of file
diff --git a/testing/mozbase/moztest/tests/test_resolve.py b/testing/mozbase/moztest/tests/test_resolve.py
--- a/testing/mozbase/moztest/tests/test_resolve.py
+++ b/testing/mozbase/moztest/tests/test_resolve.py
@@ -1,16 +1,20 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 # flake8: noqa: E501
 
 from __future__ import absolute_import, print_function, unicode_literals
 
-import cPickle as pickle
+try:
+    import cPickle as pickle
+except ImportError:
+    import pickle
+
 import json
 import os
 import re
 import shutil
 import tempfile
 from collections import defaultdict
 
 import pytest

