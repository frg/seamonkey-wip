# HG changeset patch
# User Dana Keeler <dkeeler@mozilla.com>
# Date 1675208946 0
#      Tue Jan 31 23:49:06 2023 +0000
# Node ID e31f59bc107f3704b7f1762f2cb8a53b4b0e27c5
# Parent  cdbd82c395d4aab10ac42c071413da453662d174
Bug 1813424 - webcrypto: check decoded key type before using r=jschanck a=pascalc

Differential Revision: https://phabricator.services.mozilla.com/D168472

diff --git a/dom/crypto/WebCryptoTask.cpp b/dom/crypto/WebCryptoTask.cpp
--- a/dom/crypto/WebCryptoTask.cpp
+++ b/dom/crypto/WebCryptoTask.cpp
@@ -1804,16 +1804,19 @@ private:
       if (!pubKey) {
         return NS_ERROR_DOM_UNKNOWN_ERR;
       }
     } else {
       // Invalid key format
       return NS_ERROR_DOM_SYNTAX_ERR;
     }
 
+    if (pubKey->keyType != rsaKey) {
+      return NS_ERROR_DOM_DATA_ERR;
+    }
     // Extract relevant information from the public key
     mModulusLength = 8 * pubKey->u.rsa.modulus.len;
     if (!mPublicExponent.Assign(&pubKey->u.rsa.publicExponent)) {
       return NS_ERROR_DOM_OPERATION_ERR;
     }
 
     return NS_OK;
   }
@@ -1936,16 +1939,19 @@ private:
         MOZ_ASSERT(false);
       }
 
       if (!pubKey) {
         return NS_ERROR_DOM_DATA_ERR;
       }
 
       if (mFormat.EqualsLiteral(WEBCRYPTO_KEY_FORMAT_SPKI)) {
+        if (pubKey->keyType != ecKey) {
+          return NS_ERROR_DOM_DATA_ERR;
+        }
         if (!CheckEncodedECParameters(&pubKey->u.ec.DEREncodedParams)) {
           return NS_ERROR_DOM_OPERATION_ERR;
         }
 
         // Construct the OID tag.
         SECItem oid = { siBuffer, nullptr, 0 };
         oid.len = pubKey->u.ec.DEREncodedParams.data[1];
         oid.data = pubKey->u.ec.DEREncodedParams.data + 2;
diff --git a/dom/crypto/test/test_WebCrypto.html b/dom/crypto/test/test_WebCrypto.html
--- a/dom/crypto/test/test_WebCrypto.html
+++ b/dom/crypto/test/test_WebCrypto.html
@@ -218,16 +218,31 @@ TestArray.addTest(
 
     crypto.subtle.importKey("spki", tv.negative_spki, alg, true, ["encrypt"])
       .then(error(that), complete(that));
   }
 );
 
 // -----------------------------------------------------------------------------
 TestArray.addTest(
+  "Importing an ECDSA key as an RSA key should fail",
+  function() {
+    var that = this;
+    var alg = {
+      name: "RSASSA-PKCS1-v1_5",
+      hash: "SHA-256",
+    };
+
+    crypto.subtle.importKey("spki", tv.ecdh_p256.spki, alg, true, ["verify"])
+      .then(error(that), complete(that));
+  }
+);
+
+// -----------------------------------------------------------------------------
+TestArray.addTest(
   "Refuse to export non-extractable key",
   function() {
     var that = this;
     var alg = "AES-GCM";
 
     function doExport(x) {
       return crypto.subtle.exportKey("raw", x);
     }
diff --git a/dom/crypto/test/test_WebCrypto_ECDSA.html b/dom/crypto/test/test_WebCrypto_ECDSA.html
--- a/dom/crypto/test/test_WebCrypto_ECDSA.html
+++ b/dom/crypto/test/test_WebCrypto_ECDSA.html
@@ -173,16 +173,29 @@ TestArray.addTest(
     }
 
     crypto.subtle.importKey("raw", tv.ecdsa_verify.raw, alg, true, ["verify"])
       .then(doVerify)
       .then(complete(that, x => x), error(that))
   }
 );
 
+// -----------------------------------------------------------------------------
+TestArray.addTest(
+  "Importing an RSA key as an ECDSA key should fail",
+  function() {
+    var that = this;
+    var alg = { name: "ECDSA", namedCurve: "P-256" };
+
+    // tv.spki is the SPKI for an RSA key, not an ECDSA key
+    crypto.subtle.importKey("spki", tv.spki, alg, false, ["verify"])
+      .then(error(that), complete(that));
+  }
+);
+
 /*]]>*/</script>
 </head>
 
 <body>
 
 <div id="content">
         <div id="head">
                 <b>Web</b>Crypto<br>
