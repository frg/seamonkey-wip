# HG changeset patch
# User Mats Palmgren <mats@mozilla.com>
# Date 1518716283 -3600
# Node ID 7d47e64db8a8f5bd63608f0e2bf443b6152a6b2c
# Parent  2de04e1a46bd141011f6bd390214c34623f25a5f
Bug 1432853 - Disable 'page-break-inside:avoid' on abs.pos. frames for now.  r=dholbert

diff --git a/layout/generic/nsAbsoluteContainingBlock.cpp b/layout/generic/nsAbsoluteContainingBlock.cpp
--- a/layout/generic/nsAbsoluteContainingBlock.cpp
+++ b/layout/generic/nsAbsoluteContainingBlock.cpp
@@ -162,27 +162,21 @@ nsAbsoluteContainingBlock::Reflow(nsCont
         }
       }
     }
     if (kidNeedsReflow && !aPresContext->HasPendingInterrupt()) {
       // Reflow the frame
       nsReflowStatus kidStatus;
       ReflowAbsoluteFrame(aDelegatingFrame, aPresContext, aReflowInput, cb,
                           aFlags, kidFrame, kidStatus, aOverflowAreas);
+      MOZ_ASSERT(!kidStatus.IsInlineBreakBefore(),
+                 "ShouldAvoidBreakInside should prevent this from happening");
       nsIFrame* nextFrame = kidFrame->GetNextInFlow();
-      if ((kidStatus.IsInlineBreakBefore() ||
-           !kidStatus.IsFullyComplete()) &&
+      if (!kidStatus.IsFullyComplete() &&
           aDelegatingFrame->IsFrameOfType(nsIFrame::eCanContainOverflowContainers)) {
-        // XXX it's unclear how we should handle 'page-break-inside:avoid' on
-        // abs.pos. boxes -- ignore it for now by setting the status to
-        // Incomplete (which will probably fragment it).
-        if (kidStatus.IsInlineBreakBefore()) {
-          kidStatus.Reset();
-          kidStatus.SetIncomplete();
-        }
         // Need a continuation
         if (!nextFrame) {
           nextFrame =
             aPresContext->PresShell()->FrameConstructor()->
               CreateContinuingFrame(aPresContext, kidFrame, aDelegatingFrame);
         }
         // Add it as an overflow container.
         //XXXfr This is a hack to fix some of our printing dataloss.
diff --git a/layout/generic/nsFrame.cpp b/layout/generic/nsFrame.cpp
--- a/layout/generic/nsFrame.cpp
+++ b/layout/generic/nsFrame.cpp
@@ -9699,16 +9699,26 @@ nsIFrame::GetDepthInFrameTree() const
 void
 nsFrame::ConsiderChildOverflow(nsOverflowAreas& aOverflowAreas,
                                nsIFrame* aChildFrame)
 {
   aOverflowAreas.UnionWith(aChildFrame->GetOverflowAreas() +
                            aChildFrame->GetPosition());
 }
 
+bool
+nsFrame::ShouldAvoidBreakInside(const ReflowInput& aReflowInput) const
+{
+  const auto* disp = StyleDisplay();
+  return !aReflowInput.mFlags.mIsTopOfPage &&
+    NS_STYLE_PAGE_BREAK_AVOID == disp->mBreakInside &&
+    !(HasAnyStateBits(NS_FRAME_OUT_OF_FLOW) && IsAbsolutelyPositioned(disp)) &&
+    !GetPrevInFlow();
+}
+
 /**
  * This function takes a frame that is part of a block-in-inline split,
  * and _if_ that frame is an anonymous block created by an ib split it
  * returns the block's preceding inline.  This is needed because the
  * split inline's style context is the parent of the anonymous block's
  * style context.
  *
  * If aFrame is not an anonymous block, null is returned.
diff --git a/layout/generic/nsFrame.h b/layout/generic/nsFrame.h
--- a/layout/generic/nsFrame.h
+++ b/layout/generic/nsFrame.h
@@ -468,21 +468,17 @@ public:
   // Incorporate the child overflow areas into aOverflowAreas.
   // If the child does not have a overflow, use the child area.
   void ConsiderChildOverflow(nsOverflowAreas& aOverflowAreas,
                              nsIFrame* aChildFrame);
 
   /**
    * @return true if we should avoid a page/column break in this frame.
    */
-  bool ShouldAvoidBreakInside(const ReflowInput& aReflowInput) const {
-    return !aReflowInput.mFlags.mIsTopOfPage &&
-           NS_STYLE_PAGE_BREAK_AVOID == StyleDisplay()->mBreakInside &&
-           !GetPrevInFlow();
-  }
+  bool ShouldAvoidBreakInside(const ReflowInput& aReflowInput) const;
 
 #ifdef DEBUG
   /**
    * Tracing method that writes a method enter/exit routine to the
    * nspr log using the nsIFrame log module. The tracing is only
    * done when the NS_FRAME_TRACE_CALLS bit is set in the log module's
    * level field.
    */

