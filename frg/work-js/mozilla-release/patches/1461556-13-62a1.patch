# HG changeset patch
# User Jeff Walden <jwalden@mit.edu>
# Date 1526669140 25200
#      Fri May 18 11:45:40 2018 -0700
# Node ID 7658d2d1e0d74ef1b887f871c67502b90bfd2f36
# Parent  441f59473bfa21d92affe6de99392f0975ac0311
Bug 1461556 - Don't PodZero ObjectGroupCompartment -- just add member initializers inside the class definition.  r=jandem

diff --git a/js/src/vm/ObjectGroup.cpp b/js/src/vm/ObjectGroup.cpp
--- a/js/src/vm/ObjectGroup.cpp
+++ b/js/src/vm/ObjectGroup.cpp
@@ -23,18 +23,16 @@
 #include "vm/Shape.h"
 #include "vm/TaggedProto.h"
 
 #include "gc/Marking-inl.h"
 #include "vm/UnboxedObject-inl.h"
 
 using namespace js;
 
-using mozilla::PodZero;
-
 /////////////////////////////////////////////////////////////////////
 // ObjectGroup
 /////////////////////////////////////////////////////////////////////
 
 ObjectGroup::ObjectGroup(const Class* clasp, TaggedProto proto, JS::Realm* realm,
                          ObjectGroupFlags initialFlags)
   : clasp_(clasp),
     proto_(proto),
@@ -1611,21 +1609,16 @@ ObjectGroup::findAllocationSite(JSContex
 
     return false;
 }
 
 /////////////////////////////////////////////////////////////////////
 // ObjectGroupCompartment
 /////////////////////////////////////////////////////////////////////
 
-ObjectGroupCompartment::ObjectGroupCompartment()
-{
-    PodZero(this);
-}
-
 ObjectGroupCompartment::~ObjectGroupCompartment()
 {
     js_delete(defaultNewTable);
     js_delete(lazyTable);
     js_delete(arrayObjectTable);
     js_delete(plainObjectTable);
     js_delete(allocationSiteTable);
     stringSplitStringGroup = nullptr;
diff --git a/js/src/vm/ObjectGroup.h b/js/src/vm/ObjectGroup.h
--- a/js/src/vm/ObjectGroup.h
+++ b/js/src/vm/ObjectGroup.h
@@ -587,25 +587,44 @@ class ObjectGroup : public gc::TenuredCe
 
   private:
     static ObjectGroup* defaultNewGroup(JSContext* cx, JSProtoKey key);
 };
 
 // Structure used to manage the groups in a compartment.
 class ObjectGroupCompartment
 {
-    friend class ObjectGroup;
-
+  private:
     class NewTable;
 
+    struct ArrayObjectKey;
+    using ArrayObjectTable = js::GCRekeyableHashMap<ArrayObjectKey,
+                                                    ReadBarrieredObjectGroup,
+                                                    ArrayObjectKey,
+                                                    SystemAllocPolicy>;
+
+    struct PlainObjectKey;
+    struct PlainObjectEntry;
+    struct PlainObjectTableSweepPolicy {
+        static bool needsSweep(PlainObjectKey* key, PlainObjectEntry* entry);
+    };
+    using PlainObjectTable = JS::GCHashMap<PlainObjectKey,
+                                           PlainObjectEntry,
+                                           PlainObjectKey,
+                                           SystemAllocPolicy,
+                                           PlainObjectTableSweepPolicy>;
+
+    class AllocationSiteTable;
+
+  private:
     // Set of default 'new' or lazy groups in the compartment.
-    NewTable* defaultNewTable;
-    NewTable* lazyTable;
+    NewTable* defaultNewTable = nullptr;
+    NewTable* lazyTable = nullptr;
 
-    // Cache for defaultNewGroup. Purged on GC.
+    // This cache is purged on GC.
     class DefaultNewGroupCache
     {
         ObjectGroup* group_;
         JSObject* associated_;
 
       public:
         DefaultNewGroupCache() { purge(); }
 
@@ -614,66 +633,52 @@ class ObjectGroupCompartment
         }
         void put(ObjectGroup* group, JSObject* associated) {
             group_ = group;
             associated_ = associated;
         }
 
         MOZ_ALWAYS_INLINE ObjectGroup* lookup(const Class* clasp, TaggedProto proto,
                                               JSObject* associated);
-    };
-    DefaultNewGroupCache defaultNewGroupCache;
-
-    struct ArrayObjectKey;
-    using ArrayObjectTable = js::GCRekeyableHashMap<ArrayObjectKey,
-                                                    ReadBarrieredObjectGroup,
-                                                    ArrayObjectKey,
-                                                    SystemAllocPolicy>;
-
-    struct PlainObjectKey;
-    struct PlainObjectEntry;
-    struct PlainObjectTableSweepPolicy {
-        static bool needsSweep(PlainObjectKey* key, PlainObjectEntry* entry);
-    };
-    using PlainObjectTable = JS::GCHashMap<PlainObjectKey,
-                                           PlainObjectEntry,
-                                           PlainObjectKey,
-                                           SystemAllocPolicy,
-                                           PlainObjectTableSweepPolicy>;
+    } defaultNewGroupCache = {};
 
     // Tables for managing groups common to the contents of large script
     // singleton objects and JSON objects. These are vanilla ArrayObjects and
     // PlainObjects, so we distinguish the groups of different ones by looking
     // at the types of their properties.
     //
     // All singleton/JSON arrays which have the same prototype, are homogenous
     // and of the same element type will share a group. All singleton/JSON
     // objects which have the same shape and property types will also share a
     // group. We don't try to collate arrays or objects with type mismatches.
-    ArrayObjectTable* arrayObjectTable;
-    PlainObjectTable* plainObjectTable;
-
-    struct AllocationSiteKey;
-    class AllocationSiteTable;
+    ArrayObjectTable* arrayObjectTable = nullptr;
+    PlainObjectTable* plainObjectTable = nullptr;
 
     // Table for referencing types of objects keyed to an allocation site.
-    AllocationSiteTable* allocationSiteTable;
+    AllocationSiteTable* allocationSiteTable = nullptr;
 
     // A single per-compartment ObjectGroup for all calls to StringSplitString.
     // StringSplitString is always called from self-hosted code, and conceptually
     // the return object for a string.split(string) operation should have a
     // unified type.  Having a global group for this also allows us to remove
     // the hash-table lookup that would be required if we allocated this group
     // on the basis of call-site pc.
-    ReadBarrieredObjectGroup stringSplitStringGroup;
+    ReadBarrieredObjectGroup stringSplitStringGroup = {};
+
+    // END OF PROPERTIES
+
+  private:
+    friend class ObjectGroup;
+
+    struct AllocationSiteKey;
 
   public:
     struct NewEntry;
 
-    ObjectGroupCompartment();
+    ObjectGroupCompartment() = default;
     ~ObjectGroupCompartment();
 
     void replaceAllocationSiteGroup(JSScript* script, jsbytecode* pc,
                                     JSProtoKey kind, ObjectGroup* group);
 
     void removeDefaultNewGroup(const Class* clasp, TaggedProto proto, JSObject* associated);
     void replaceDefaultNewGroup(const Class* clasp, TaggedProto proto, JSObject* associated,
                                 ObjectGroup* group);
