# HG changeset patch
# User Jason Orendorff <jorendorff@mozilla.com>
# Date 1530918290 18000
#      Fri Jul 06 18:04:50 2018 -0500
# Node ID 4fa3219f714254fcbbb920e7aa5aae4b0535bbaf
# Parent  71a6537e84fe2314ed64eddb8a91ccd97b480c98
Bug 1474385 - Part 1: Rename and comment a Debugger method involved in error handling. r=jimb

We will in fact delete this method later in the stack, but to understand those
coming patches, it helps to understand what's going on here.

diff --git a/js/src/vm/Debugger.cpp b/js/src/vm/Debugger.cpp
--- a/js/src/vm/Debugger.cpp
+++ b/js/src/vm/Debugger.cpp
@@ -1417,18 +1417,21 @@ Debugger::handleUncaughtExceptionHelper(
                 return ResumeMode::Terminate;
             cx->clearPendingException();
 
             RootedValue fval(cx, ObjectValue(*uncaughtExceptionHook));
             RootedValue rv(cx);
             if (js::Call(cx, fval, object, exc, &rv)) {
                 if (vp) {
                     ResumeMode resumeMode = ResumeMode::Continue;
-                    if (processResumptionValue(ar, frame, thisVForCheck, rv, resumeMode, *vp))
+                    if (processResumptionValueNoUncaughtExceptionHook(ar, frame, thisVForCheck, rv,
+                                                                      resumeMode, *vp))
+                    {
                         return resumeMode;
+                    }
                 } else {
                     return ResumeMode::Continue;
                 }
             }
         }
 
         return reportUncaughtException(ar);
     }
@@ -1629,19 +1632,20 @@ GetThisValueForCheck(JSContext* cx, Abst
         MOZ_ASSERT_IF(thisv.isMagic(), thisv.isMagic(JS_UNINITIALIZED_LEXICAL));
         maybeThisv.emplace(HandleValue(thisv));
     }
 
     return true;
 }
 
 bool
-Debugger::processResumptionValue(Maybe<AutoRealm>& ar, AbstractFramePtr frame,
-                                 const Maybe<HandleValue>& maybeThisv, HandleValue rval,
-                                 ResumeMode& resumeMode, MutableHandleValue vp)
+Debugger::processResumptionValueNoUncaughtExceptionHook(
+    Maybe<AutoRealm>& ar, AbstractFramePtr frame,
+    const Maybe<HandleValue>& maybeThisv, HandleValue rval,
+    ResumeMode& resumeMode, MutableHandleValue vp)
 {
     JSContext* cx = ar->context();
 
     if (!ParseResumptionValue(cx, rval, resumeMode, vp) ||
         !unwrapDebuggeeValue(cx, vp) ||
         !CheckResumptionValue(cx, frame, maybeThisv, resumeMode, vp))
     {
         return false;
diff --git a/js/src/vm/Debugger.h b/js/src/vm/Debugger.h
--- a/js/src/vm/Debugger.h
+++ b/js/src/vm/Debugger.h
@@ -602,17 +602,17 @@ class Debugger : private mozilla::Linked
     /*
      * Handle the result of a hook that is expected to return a resumption
      * value <https://wiki.mozilla.org/Debugger#Resumption_Values>. This is
      * called when we return from a debugging hook to debuggee code. The
      * interpreter wants a (ResumeMode, Value) pair telling it how to proceed.
      *
      * Precondition: ar is entered. We are in the debugger compartment.
      *
-     * Postcondition: This called ar.leave(). See handleUncaughtException.
+     * Postcondition: This called ar.reset(). See handleUncaughtException.
      *
      * If `success` is false, the hook failed. If an exception is pending in
      * ar.context(), return handleUncaughtException(ar, vp, callhook).
      * Otherwise just return ResumeMode::Terminate.
      *
      * If `success` is true, there must be no exception pending in ar.context().
      * `rv` may be:
      *
@@ -639,19 +639,29 @@ class Debugger : private mozilla::Linked
                                           MutableHandleValue vp);
 
     ResumeMode processParsedHandlerResultHelper(mozilla::Maybe<AutoRealm>& ar,
                                                 AbstractFramePtr frame,
                                                 const mozilla::Maybe<HandleValue>& maybeThisv,
                                                 bool success, ResumeMode resumeMode,
                                                 MutableHandleValue vp);
 
-    bool processResumptionValue(mozilla::Maybe<AutoRealm>& ar, AbstractFramePtr frame,
-                                const mozilla::Maybe<HandleValue>& maybeThis, HandleValue rval,
-                                ResumeMode& resumeMode, MutableHandleValue vp);
+    /*
+     * Like processHandlerResult, but if `rval` is not a valid resumption value,
+     * don't call the uncaughtExceptionHook. Just return false.
+     *
+     * This is used to process the return value of the uncaughtExceptionHook
+     * itself, to avoid the possibility of triggering it again.
+     */
+    bool processResumptionValueNoUncaughtExceptionHook(mozilla::Maybe<AutoRealm>& ar,
+                                                       AbstractFramePtr frame,
+                                                       const mozilla::Maybe<HandleValue>& maybeThis,
+                                                       HandleValue rval,
+                                                       ResumeMode& resumeMode,
+                                                       MutableHandleValue vp);
 
     GlobalObject* unwrapDebuggeeArgument(JSContext* cx, const Value& v);
 
     static void traceObject(JSTracer* trc, JSObject* obj);
 
     void trace(JSTracer* trc);
     friend struct js::GCManagedDeletePolicy<Debugger>;
 
