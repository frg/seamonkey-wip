# HG changeset patch
# User Aaron Klotz <aklotz@mozilla.com>
# Date 1530922308 21600
# Node ID de4058a2b443997db87908e24c807197ee8bbbfa
# Parent  b56104478d7560eeaa8b20ff28d8770dc7751633
Bug 1475067: Faster handling of UNICODE_STRINGs in bootstrap blocklist; r=mhowell

diff --git a/browser/app/winlauncher/DllBlocklistWin.cpp.1475067.later b/browser/app/winlauncher/DllBlocklistWin.cpp.1475067.later
new file mode 100644
--- /dev/null
+++ b/browser/app/winlauncher/DllBlocklistWin.cpp.1475067.later
@@ -0,0 +1,173 @@
+--- DllBlocklistWin.cpp
++++ DllBlocklistWin.cpp
+@@ -6,19 +6,29 @@
+ 
+ #include "NativeNt.h"
+ #include "nsWindowsDllInterceptor.h"
+ #include "mozilla/ArrayUtils.h"
+ #include "mozilla/Attributes.h"
+ #include "mozilla/Types.h"
+ #include "mozilla/WindowsDllBlocklist.h"
+ 
++#define MOZ_LITERAL_UNICODE_STRING(s) \
++  { \
++    /* Length of the string in bytes, less the null terminator */ \
++    sizeof(s) - sizeof(wchar_t), \
++    /* Length of the string in bytes, including the null terminator */ \
++    sizeof(s), \
++    /* Pointer to the buffer */ \
++    const_cast<wchar_t*>(s) \
++  }
++
+ #define DLL_BLOCKLIST_ENTRY(name, ...) \
+-  { L##name, __VA_ARGS__ },
+-#define DLL_BLOCKLIST_CHAR_TYPE wchar_t
++  { MOZ_LITERAL_UNICODE_STRING(L##name), __VA_ARGS__ },
++#define DLL_BLOCKLIST_STRING_TYPE UNICODE_STRING
+ 
+ // Restrict the blocklist definitions to Nightly-only for now
+ #if defined(NIGHTLY_BUILD)
+ #include "mozilla/WindowsDllBlocklistDefs.h"
+ #else
+ #include "mozilla/WindowsDllBlocklistCommon.h"
+ DLL_BLOCKLIST_DEFINITIONS_BEGIN
+ DLL_BLOCKLIST_DEFINITIONS_END
+@@ -29,72 +39,72 @@ extern uint32_t gBlocklistInitFlags;
+ static const HANDLE kCurrentProcess = reinterpret_cast<HANDLE>(-1);
+ 
+ class MOZ_STATIC_CLASS MOZ_TRIVIAL_CTOR_DTOR NativeNtBlockSet final
+ {
+   struct NativeNtBlockSetEntry
+   {
+     NativeNtBlockSetEntry() = default;
+     ~NativeNtBlockSetEntry() = default;
+-    NativeNtBlockSetEntry(const wchar_t* aName, uint64_t aVersion,
++    NativeNtBlockSetEntry(const UNICODE_STRING& aName, uint64_t aVersion,
+                           NativeNtBlockSetEntry* aNext)
+       : mName(aName)
+       , mVersion(aVersion)
+       , mNext(aNext)
+     {}
+-    const wchar_t*          mName;
++    UNICODE_STRING          mName;
+     uint64_t                mVersion;
+     NativeNtBlockSetEntry*  mNext;
+   };
+ 
+ public:
+   // Constructor and destructor MUST be trivial
+   NativeNtBlockSet() = default;
+   ~NativeNtBlockSet() = default;
+ 
+-  void Add(const wchar_t* aName, uint64_t aVersion);
++  void Add(const UNICODE_STRING& aName, uint64_t aVersion);
+   void Write(HANDLE aFile);
+ 
+ private:
+-  static NativeNtBlockSetEntry* NewEntry(const wchar_t* aName, uint64_t aVersion,
++  static NativeNtBlockSetEntry* NewEntry(const UNICODE_STRING& aName,
++                                         uint64_t aVersion,
+                                          NativeNtBlockSetEntry* aNextEntry);
+ 
+ private:
+   NativeNtBlockSetEntry* mFirstEntry;
+   // SRWLOCK_INIT == 0, so this is okay to use without any additional work as
+   // long as NativeNtBlockSet is instantiated statically
+   SRWLOCK                mLock;
+ };
+ 
+ NativeNtBlockSet::NativeNtBlockSetEntry*
+-NativeNtBlockSet::NewEntry(const wchar_t* aName, uint64_t aVersion,
++NativeNtBlockSet::NewEntry(const UNICODE_STRING& aName, uint64_t aVersion,
+                            NativeNtBlockSet::NativeNtBlockSetEntry* aNextEntry)
+ {
+   HANDLE processHeap = mozilla::nt::RtlGetProcessHeap();
+   if (!processHeap) {
+     return nullptr;
+   }
+ 
+   PVOID memory = ::RtlAllocateHeap(processHeap, 0, sizeof(NativeNtBlockSetEntry));
+   if (!memory) {
+     return nullptr;
+   }
+ 
+   return new (memory) NativeNtBlockSetEntry(aName, aVersion, aNextEntry);
+ }
+ 
+ void
+-NativeNtBlockSet::Add(const wchar_t* aName, uint64_t aVersion)
++NativeNtBlockSet::Add(const UNICODE_STRING& aName, uint64_t aVersion)
+ {
+   ::RtlAcquireSRWLockExclusive(&mLock);
+ 
+   for (NativeNtBlockSetEntry* entry = mFirstEntry; entry; entry = entry->mNext) {
+-    // We just need to compare the string pointers, not the strings themselves,
+-    // as we always pass in the strings statically defined in the blocklist.
+-    if (aName == entry->mName && aVersion == entry->mVersion) {
++    if (::RtlEqualUnicodeString(&entry->mName, &aName, TRUE) &&
++        aVersion == entry->mVersion) {
+       ::RtlReleaseSRWLockExclusive(&mLock);
+       return;
+     }
+   }
+ 
+   // Not present, add it
+   NativeNtBlockSetEntry* newEntry = NewEntry(aName, aVersion, mFirstEntry);
+   mFirstEntry = newEntry;
+@@ -113,18 +123,19 @@ NativeNtBlockSet::Write(HANDLE aFile)
+   // It would be nicer to use RAII here. However, its destructor
+   // might not run if an exception occurs, in which case we would never release
+   // the lock (MSVC warns about this possibility). So we acquire and release
+   // manually.
+   ::AcquireSRWLockExclusive(&mLock);
+ 
+   MOZ_SEH_TRY {
+     for (auto entry = mFirstEntry; entry; entry = entry->mNext) {
+-      int convOk = ::WideCharToMultiByte(CP_UTF8, 0, entry->mName, -1, buf,
+-                                         sizeof(buf), nullptr, nullptr);
++      int convOk = ::WideCharToMultiByte(CP_UTF8, 0, entry->mName.Buffer,
++                                         entry->mName.Length / sizeof(wchar_t),
++                                         buf, sizeof(buf), nullptr, nullptr);
+       if (!convOk) {
+         continue;
+       }
+ 
+       // write name[,v.v.v.v];
+       if (!WriteFile(aFile, buf, convOk, &nBytes, nullptr)) {
+         continue;
+       }
+@@ -220,29 +231,29 @@ CheckBlockInfo(const DllBlockInfo* aInfo
+ static bool
+ IsDllAllowed(const UNICODE_STRING& aLeafName, void* aBaseAddress)
+ {
+   if (mozilla::nt::Contains12DigitHexString(aLeafName) ||
+       mozilla::nt::IsFileNameAtLeast16HexDigits(aLeafName)) {
+     return false;
+   }
+ 
+-  UNICODE_STRING testStr;
+   DECLARE_POINTER_TO_FIRST_DLL_BLOCKLIST_ENTRY(info);
+-  while (info->name) {
+-    ::RtlInitUnicodeString(&testStr, info->name);
+-    if (::RtlEqualUnicodeString(&aLeafName, &testStr, TRUE)) {
++  DECLARE_POINTER_TO_LAST_DLL_BLOCKLIST_ENTRY(end);
++
++  while (info < end) {
++    if (::RtlEqualUnicodeString(&aLeafName, &info->name, TRUE)) {
+       break;
+     }
+ 
+     ++info;
+   }
+ 
+   uint64_t version;
+-  if (info->name && !CheckBlockInfo(info, aBaseAddress, version)) {
++  if (info->name.Length && !CheckBlockInfo(info, aBaseAddress, version)) {
+     gBlockSet.Add(info->name, version);
+     return false;
+   }
+ 
+   return true;
+ }
+ 
+ typedef decltype(&NtMapViewOfSection) NtMapViewOfSection_func;
diff --git a/browser/app/winlauncher/NativeNt.h.1475067.later b/browser/app/winlauncher/NativeNt.h.1475067.later
new file mode 100644
--- /dev/null
+++ b/browser/app/winlauncher/NativeNt.h.1475067.later
@@ -0,0 +1,50 @@
+--- NativeNt.h
++++ NativeNt.h
+@@ -143,16 +143,23 @@ MatchUnicodeString(const UNICODE_STRING&
+   }
+ 
+   return true;
+ }
+ 
+ inline bool
+ Contains12DigitHexString(const UNICODE_STRING& aLeafName)
+ {
++  // Quick check: If the string is too short, don't bother
++  // (We need at least 12 hex digits, one char for '.', and 3 for extension)
++  const USHORT kMinLen = (12 + 1 + 3) * sizeof(wchar_t);
++  if (aLeafName.Length < kMinLen) {
++    return false;
++  }
++
+   uint16_t start, end;
+   if (!FindCharInUnicodeString(aLeafName, L'.', start)) {
+     return false;
+   }
+ 
+   ++start;
+   if (!FindCharInUnicodeString(aLeafName, L'.', end, start)) {
+     return false;
+@@ -168,16 +175,23 @@ Contains12DigitHexString(const UNICODE_S
+   test.MaximumLength = test.Length;
+ 
+   return MatchUnicodeString(test, &IsHexDigit);
+ }
+ 
+ inline bool
+ IsFileNameAtLeast16HexDigits(const UNICODE_STRING& aLeafName)
+ {
++  // Quick check: If the string is too short, don't bother
++  // (We need 16 hex digits, one char for '.', and 3 for extension)
++  const USHORT kMinLen = (16 + 1 + 3) * sizeof(wchar_t);
++  if (aLeafName.Length < kMinLen) {
++    return false;
++  }
++
+   uint16_t dotIndex;
+   if (!FindCharInUnicodeString(aLeafName, L'.', dotIndex)) {
+     return false;
+   }
+ 
+   if (dotIndex < 16) {
+     return false;
+   }
diff --git a/mozglue/build/WindowsDllBlocklist.cpp b/mozglue/build/WindowsDllBlocklist.cpp
--- a/mozglue/build/WindowsDllBlocklist.cpp
+++ b/mozglue/build/WindowsDllBlocklist.cpp
@@ -35,17 +35,17 @@
 #include "WindowsDllBlocklist.h"
 #include "mozilla/AutoProfilerLabel.h"
 #include "mozilla/glue/WindowsDllServices.h"
 
 using namespace mozilla;
 
 #define DLL_BLOCKLIST_ENTRY(name, ...) \
   { name, __VA_ARGS__ },
-#define DLL_BLOCKLIST_CHAR_TYPE char
+#define DLL_BLOCKLIST_STRING_TYPE const char*
 #include "mozilla/WindowsDllBlocklistDefs.h"
 
 // define this for very verbose dll load debug spew
 #undef DEBUG_very_verbose
 
 static const char kBlockedDllsParameter[] = "BlockedDllList=";
 static const int kBlockedDllsParameterLen =
   sizeof(kBlockedDllsParameter) - 1;
diff --git a/mozglue/build/WindowsDllBlocklistCommon.h b/mozglue/build/WindowsDllBlocklistCommon.h
--- a/mozglue/build/WindowsDllBlocklistCommon.h
+++ b/mozglue/build/WindowsDllBlocklistCommon.h
@@ -4,23 +4,25 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
 #ifndef mozilla_WindowsDllBlocklistCommon_h
 #define mozilla_WindowsDllBlocklistCommon_h
 
 #include <stdint.h>
 
+#include "mozilla/ArrayUtils.h"
+
 namespace mozilla {
 
-template <typename CharType>
+template <typename StrType>
 struct DllBlockInfoT {
   // The name of the DLL -- in LOWERCASE!  It will be compared to
   // a lowercase version of the DLL name only.
-  const CharType* name;
+  StrType name;
 
   // If maxVersion is ALL_VERSIONS, we'll block all versions of this
   // dll.  Otherwise, we'll block all versions less than or equal to
   // the given version, as queried by GetFileVersionInfo and
   // VS_FIXEDFILEINFO's dwFileVersionMS and dwFileVersionLS fields.
   //
   // Note that the version is usually 4 components, which is A.B.C.D
   // encoded as 0x AAAA BBBB CCCC DDDD ULL (spaces added for clarity),
@@ -68,27 +70,30 @@ MAKE_VERSION(uint16_t a, uint16_t b, uin
   return static_cast<uint64_t>(a) << 48 |
          static_cast<uint64_t>(b) << 32 |
          static_cast<uint64_t>(c) << 16 |
          static_cast<uint64_t>(d);
 }
 
 #endif
 
-#if !defined(DLL_BLOCKLIST_CHAR_TYPE)
-#error "You must define DLL_BLOCKLIST_CHAR_TYPE"
-#endif // !defined(DLL_BLOCKLIST_CHAR_TYPE)
+#if !defined(DLL_BLOCKLIST_STRING_TYPE)
+#error "You must define DLL_BLOCKLIST_STRING_TYPE"
+#endif // !defined(DLL_BLOCKLIST_STRING_TYPE)
 
 #define DLL_BLOCKLIST_DEFINITIONS_BEGIN \
-  using DllBlockInfo = mozilla::DllBlockInfoT<DLL_BLOCKLIST_CHAR_TYPE>; \
+  using DllBlockInfo = mozilla::DllBlockInfoT<DLL_BLOCKLIST_STRING_TYPE>; \
   static const DllBlockInfo gWindowsDllBlocklist[] = {
 
 #define ALL_VERSIONS DllBlockInfo::ALL_VERSIONS
 #define UNVERSIONED DllBlockInfo::UNVERSIONED
 
 #define DLL_BLOCKLIST_DEFINITIONS_END \
-    {nullptr, 0} \
+    {} \
   };
 
 #define DECLARE_POINTER_TO_FIRST_DLL_BLOCKLIST_ENTRY(name) \
   const DllBlockInfo* name = &gWindowsDllBlocklist[0]
 
+#define DECLARE_POINTER_TO_LAST_DLL_BLOCKLIST_ENTRY(name) \
+  const DllBlockInfo* name = &gWindowsDllBlocklist[mozilla::ArrayLength(gWindowsDllBlocklist) - 1]
+
 #endif // mozilla_WindowsDllBlocklistCommon_h
