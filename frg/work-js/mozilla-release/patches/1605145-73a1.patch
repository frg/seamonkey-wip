# HG changeset patch
# User Jonathan Watt <jwatt@jwatt.org>
# Date 1576861143 0
# Node ID b39ace10dbc20bb052681b2d284a0fa0c6ab0b4c
# Parent  aa338228d6c89b0e2537e1d4856f33fa621eb0f8
Bug 1605145. Fix the 'line too long' E501 flake8 errors in python/lldbutils. r=ahal

Depends on D57817

Differential Revision: https://phabricator.services.mozilla.com/D57850

diff --git a/third_party/python/lldbutils/lldbutils/content.py b/third_party/python/lldbutils/lldbutils/content.py
--- a/third_party/python/lldbutils/lldbutils/content.py
+++ b/third_party/python/lldbutils/lldbutils/content.py
@@ -18,10 +18,12 @@ def summarize_text_fragment(valobj, inte
 
 
 def ptag(debugger, command, result, dict):
     """Displays the tag name of a content node."""
     debugger.HandleCommand("expr (" + command + ")->mNodeInfo.mRawPtr->mInner.mName")
 
 
 def init(debugger):
-    debugger.HandleCommand("type summary add nsTextFragment -F lldbutils.content.summarize_text_fragment")
+    debugger.HandleCommand(
+        "type summary add nsTextFragment -F lldbutils.content.summarize_text_fragment"
+    )
     debugger.HandleCommand("command script add -f lldbutils.content.ptag ptag")
diff --git a/third_party/python/lldbutils/lldbutils/general.py b/third_party/python/lldbutils/lldbutils/general.py
--- a/third_party/python/lldbutils/lldbutils/general.py
+++ b/third_party/python/lldbutils/lldbutils/general.py
@@ -69,29 +69,33 @@ def prefcnt(debugger, command, result, d
         print("not a refcounted object")
         return
     refcnt_type = field.GetType().GetCanonicalType().GetName()
     if refcnt_type == "nsAutoRefCnt":
         print(field.GetChildMemberWithName("mValue").GetValueAsUnsigned(0))
     elif refcnt_type == "nsCycleCollectingAutoRefCnt":
         print(field.GetChildMemberWithName("mRefCntAndFlags").GetValueAsUnsigned(0) >> 2)
     elif refcnt_type == "mozilla::ThreadSafeAutoRefCnt":
-        print(field.GetChildMemberWithName("mValue").GetChildMemberWithName("mValue").GetValueAsUnsigned(0))
+        print(
+            field.GetChildMemberWithName("mValue")
+            .GetChildMemberWithName("mValue")
+            .GetValueAsUnsigned(0)
+        )
     elif refcnt_type == "int":  # non-atomic mozilla::RefCounted object
         print(field.GetValueAsUnsigned(0))
     elif refcnt_type == "mozilla::Atomic<int>":  # atomic mozilla::RefCounted object
         print(field.GetChildMemberWithName("mValue").GetValueAsUnsigned(0))
     else:
         print("unknown mRefCnt type " + refcnt_type)
 
 
 # Used to work around http://llvm.org/bugs/show_bug.cgi?id=22211
 def callfunc(debugger, command, result, dict):
-    """Calls a function for which debugger information is unavailable by getting its address from the symbol table.
-       The function is assumed to return void."""
+    """Calls a function for which debugger information is unavailable by getting its address
+       from the symbol table.  The function is assumed to return void."""
 
     if '(' not in command:
         print('Usage: callfunc your_function(args)')
         return
 
     command_parts = command.split('(')
     funcname = command_parts[0].strip()
     args = command_parts[1]
@@ -101,24 +105,36 @@ def callfunc(debugger, command, result, 
     if not symbols:
         print('Could not find a function symbol for a function called "%s"' % funcname)
         return
 
     sym = symbols[0]
     arg_types = '()'
     if sym.name and sym.name.startswith(funcname + '('):
         arg_types = sym.name[len(funcname):]
-    debugger.HandleCommand('print ((void(*)%s)0x%0x)(%s' % (arg_types, sym.addr.GetLoadAddress(target), args))
+    debugger.HandleCommand(
+        "print ((void(*)%s)0x%0x)(%s" % (arg_types, sym.addr.GetLoadAddress(target), args)
+    )
 
 
 def init(debugger):
     debugger.HandleCommand("type summary add nsAString -F lldbutils.general.summarize_string")
     debugger.HandleCommand("type summary add nsACString -F lldbutils.general.summarize_string")
     debugger.HandleCommand("type summary add nsFixedString -F lldbutils.general.summarize_string")
     debugger.HandleCommand("type summary add nsFixedCString -F lldbutils.general.summarize_string")
     debugger.HandleCommand("type summary add nsAutoString -F lldbutils.general.summarize_string")
     debugger.HandleCommand("type summary add nsAutoCString -F lldbutils.general.summarize_string")
     debugger.HandleCommand("type summary add nsIAtom -F lldbutils.general.summarize_atom")
-    debugger.HandleCommand("type synthetic add -x \"nsTArray<\" -l lldbutils.general.TArraySyntheticChildrenProvider")
-    debugger.HandleCommand("type synthetic add -x \"AutoTArray<\" -l lldbutils.general.TArraySyntheticChildrenProvider")
-    debugger.HandleCommand("type synthetic add -x \"FallibleTArray<\" -l lldbutils.general.TArraySyntheticChildrenProvider")
-    debugger.HandleCommand("command script add -f lldbutils.general.prefcnt -f lldbutils.general.prefcnt prefcnt")
-    debugger.HandleCommand("command script add -f lldbutils.general.callfunc -f lldbutils.general.callfunc callfunc")
+    debugger.HandleCommand(
+        "type synthetic add -x \"nsTArray<\" -l lldbutils.general.TArraySyntheticChildrenProvider"
+    )
+    debugger.HandleCommand(
+        "type synthetic add -x \"AutoTArray<\" -l lldbutils.general.TArraySyntheticChildrenProvider"  # NOQA: E501
+    )
+    debugger.HandleCommand(
+        "type synthetic add -x \"FallibleTArray<\" -l lldbutils.general.TArraySyntheticChildrenProvider"  # NOQA: E501
+    )
+    debugger.HandleCommand(
+        "command script add -f lldbutils.general.prefcnt -f lldbutils.general.prefcnt prefcnt"
+    )
+    debugger.HandleCommand(
+        "command script add -f lldbutils.general.callfunc -f lldbutils.general.callfunc callfunc"
+    )
diff --git a/third_party/python/lldbutils/lldbutils/gfx.py b/third_party/python/lldbutils/lldbutils/gfx.py
--- a/third_party/python/lldbutils/lldbutils/gfx.py
+++ b/third_party/python/lldbutils/lldbutils/gfx.py
@@ -23,17 +23,22 @@ def summarize_nscolor(valobj, internal_d
         "#000000": "black",
         "#c0c0c0": "silver",
         "#808080": "gray"
     }
     value = valobj.GetValueAsUnsigned(0)
     if value == 0:
         return "transparent"
     if value & 0xff000000 != 0xff000000:
-        return "rgba(%d, %d, %d, %f)" % (value & 0xff, (value >> 8) & 0xff, (value >> 16) & 0xff, ((value >> 24) & 0xff) / 255.0)
+        return "rgba(%d, %d, %d, %f)" % (
+            value & 0xff,
+            (value >> 8) & 0xff,
+            (value >> 16) & 0xff,
+            ((value >> 24) & 0xff) / 255.0,
+        )
     color = "#%02x%02x%02x" % (value & 0xff, (value >> 8) & 0xff, (value >> 16) & 0xff)
     if color in colors:
         return colors[color]
     return color
 
 
 class RegionSyntheticChildrenProvider:
 
@@ -46,17 +51,21 @@ class RegionSyntheticChildrenProvider:
         self.num_rects = self.pixman_region_num_rects()
         self.box_type = self.pixman_extents.GetType()
         self.box_type_size = self.box_type.GetByteSize()
         self.box_list_base_ptr = self.pixman_data.GetValueAsUnsigned(0) \
             + self.pixman_data.GetType().GetPointeeType().GetByteSize()
 
     def pixman_region_num_rects(self):
         if self.pixman_data.GetValueAsUnsigned(0):
-            return self.pixman_data.Dereference().GetChildMemberWithName("numRects").GetValueAsUnsigned(0)
+            return (
+                self.pixman_data.Dereference()
+                .GetChildMemberWithName("numRects")
+                .GetValueAsUnsigned(0)
+            )
         return 1
 
     def num_children(self):
         return 2 + self.num_rects
 
     def get_child_index(self, name):
         if name == "numRects":
             return 0
@@ -64,39 +73,44 @@ class RegionSyntheticChildrenProvider:
             return 1
         return None
 
     def convert_pixman_box_to_rect(self, valobj, name):
         x1 = valobj.GetChildMemberWithName("x1").GetValueAsSigned()
         x2 = valobj.GetChildMemberWithName("x2").GetValueAsSigned()
         y1 = valobj.GetChildMemberWithName("y1").GetValueAsSigned()
         y2 = valobj.GetChildMemberWithName("y2").GetValueAsSigned()
-        return valobj.CreateValueFromExpression(name,
-            '%s(%d, %d, %d, %d)' % (self.rect_type, x1, y1, x2 - x1, y2 - y1))
+        return valobj.CreateValueFromExpression(
+            name, '%s(%d, %d, %d, %d)' % (self.rect_type, x1, y1, x2 - x1, y2 - y1)
+        )
 
     def get_child_at_index(self, index):
         if index == 0:
-            return self.pixman_data.CreateValueFromExpression('numRects', '(uint32_t)%d' % self.num_rects)
+            return self.pixman_data.CreateValueFromExpression(
+                "numRects", "(uint32_t)%d" % self.num_rects
+            )
         if index == 1:
             return self.convert_pixman_box_to_rect(self.pixman_extents, 'bounds')
 
         rect_index = index - 2
         if rect_index >= self.num_rects:
             return None
         if self.num_rects == 1:
             return self.convert_pixman_box_to_rect(self.pixman_extents, 'bounds')
         box_address = self.box_list_base_ptr + rect_index * self.box_type_size
         box = self.pixman_data.CreateValueFromAddress('', box_address, self.box_type)
         return self.convert_pixman_box_to_rect(box, "[%d]" % rect_index)
 
 
 class IntRegionSyntheticChildrenProvider:
     def __init__(self, valobj, internal_dict):
         wrapped_region = valobj.GetChildMemberWithName("mImpl")
-        self.wrapped_provider = RegionSyntheticChildrenProvider(wrapped_region, internal_dict, "nsIntRect")
+        self.wrapped_provider = RegionSyntheticChildrenProvider(
+            wrapped_region, internal_dict, "nsIntRect"
+        )
 
     def num_children(self):
         return self.wrapped_provider.num_children()
 
     def get_child_index(self, name):
         return self.wrapped_provider.get_child_index(name)
 
     def get_child_at_index(self, index):
@@ -130,12 +144,16 @@ def summarize_region(valobj, internal_di
     return str(num_rects) + " rects, bounds: " + bounds_summary
 
 
 def init(debugger):
     debugger.HandleCommand("type summary add nscolor -v -F lldbutils.gfx.summarize_nscolor")
     debugger.HandleCommand("type summary add nsRect -v -F lldbutils.gfx.summarize_rect")
     debugger.HandleCommand("type summary add nsIntRect -v -F lldbutils.gfx.summarize_rect")
     debugger.HandleCommand("type summary add gfxRect -v -F lldbutils.gfx.summarize_rect")
-    debugger.HandleCommand("type synthetic add nsRegion -l lldbutils.gfx.RegionSyntheticChildrenProvider")
-    debugger.HandleCommand("type synthetic add nsIntRegion -l lldbutils.gfx.IntRegionSyntheticChildrenProvider")
+    debugger.HandleCommand(
+        "type synthetic add nsRegion -l lldbutils.gfx.RegionSyntheticChildrenProvider"
+    )
+    debugger.HandleCommand(
+        "type synthetic add nsIntRegion -l lldbutils.gfx.IntRegionSyntheticChildrenProvider"
+    )
     debugger.HandleCommand("type summary add nsRegion -v -F lldbutils.gfx.summarize_region")
     debugger.HandleCommand("type summary add nsIntRegion -v -F lldbutils.gfx.summarize_region")
diff --git a/third_party/python/lldbutils/lldbutils/layout.py b/third_party/python/lldbutils/lldbutils/layout.py
--- a/third_party/python/lldbutils/lldbutils/layout.py
+++ b/third_party/python/lldbutils/lldbutils/layout.py
@@ -16,12 +16,14 @@ def frametreelimited(debugger, command, 
 
 def pstate(debugger, command, result, dict):
     """Displays a frame's state bits symbolically."""
     debugger.HandleCommand('expr mozilla::PrintFrameState(' + command + ')')
 
 
 def init(debugger):
     debugger.HandleCommand('command script add -f lldbutils.layout.frametree frametree')
-    debugger.HandleCommand('command script add -f lldbutils.layout.frametreelimited frametreelimited')
+    debugger.HandleCommand(
+        "command script add -f lldbutils.layout.frametreelimited frametreelimited"
+    )
     debugger.HandleCommand('command alias ft frametree')
     debugger.HandleCommand('command alias ftl frametreelimited')
     debugger.HandleCommand('command script add -f lldbutils.layout.pstate pstate')
diff --git a/third_party/python/lldbutils/lldbutils/utils.py b/third_party/python/lldbutils/lldbutils/utils.py
--- a/third_party/python/lldbutils/lldbutils/utils.py
+++ b/third_party/python/lldbutils/lldbutils/utils.py
@@ -46,17 +46,20 @@ def format_string(lldb_value, length=100
         s = "u\""
         size = 2
         mask = 0xffff
     else:
         return "(cannot format string with char type %s)" % char_type.GetName()
     i = 0
     terminated = False
     while i < length:
-        c = lldb_value.CreateValueFromAddress("x", ptr + i * size, char_type).GetValueAsUnsigned(0) & mask
+        c = (
+            lldb_value.CreateValueFromAddress("x", ptr + i * size, char_type).GetValueAsUnsigned(0)
+            & mask
+        )
         if c == 0:
             terminated = True
             break
         s += format_char(c)
         i = i + 1
     s += "\""
     if not terminated and i != length:
         s += "..."
