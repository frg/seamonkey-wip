# HG changeset patch
# User Nicolas Chevobbe <nchevobbe@mozilla.com>
# Date 1507715805 -7200
# Node ID 3df81d9d83379b1fc7b45151ac33e35ea36c9bc7
# Parent  135adbeb2a35fa882568f0786b3406a6a48020c8
Bug 1405070 - Add a DAMP test for console shutdown after expanding a large object; r=bgrins.

MozReview-Commit-ID: CQp3V4GIJzP

diff --git a/testing/talos/talos/tests/devtools/addon/content/damp.html b/testing/talos/talos/tests/devtools/addon/content/damp.html
--- a/testing/talos/talos/tests/devtools/addon/content/damp.html
+++ b/testing/talos/talos/tests/devtools/addon/content/damp.html
@@ -14,29 +14,31 @@ var defaultConfig = {
     inspectorOpen: true,
     debuggerOpen: true,
     styleEditorOpen: true,
     performanceOpen: true,
     netmonitorOpen: true,
     saveAndReadHeapSnapshot: true,
     consoleBulkLogging: true,
     consoleStreamLogging: true,
+    consoleObjectExpansion: true,
   }
 };
 
 var testsInfo = {
   webconsoleOpen: "Measure open/close toolbox on webconsole panel",
   inspectorOpen: "Measure open/close toolbox on inspector panel",
   debuggerOpen: "Measure open/close toolbox on debugger panel",
   styleEditorOpen: "Measure open/close toolbox on style editor panel",
   performanceOpen: "Measure open/close toolbox on performance panel",
   netmonitorOpen: "Measure open/close toolbox on network monitor panel",
   saveAndReadHeapSnapshot: "Measure open/close toolbox on memory panel and save/read heap snapshot",
   consoleBulkLogging: "Measure time for a bunch of sync console.log statements to appear",
   consoleStreamLogging: "Measure rAF on page during a stream of console.log statements",
+  consoleObjectExpansion: "Measure time to expand a large object and close the console",
 };
 
 function updateConfig() {
   config = {subtests: []};
   for (var test in defaultConfig.subtests) {
     if ($("subtest-" + test).checked) { // eslint-disable-line no-undef
       config.subtests.push(test);
     }
diff --git a/testing/talos/talos/tests/devtools/addon/content/damp.js b/testing/talos/talos/tests/devtools/addon/content/damp.js
--- a/testing/talos/talos/tests/devtools/addon/content/damp.js
+++ b/testing/talos/talos/tests/devtools/addon/content/damp.js
@@ -257,16 +257,74 @@ Damp.prototype = {
       name: "console.streamlog",
       value: avgTime
     });
 
     yield this.closeToolbox(null);
     yield this.testTeardown();
   }),
 
+  _consoleObjectExpansionTest: Task.async(function* () {
+    let tab = yield this.testSetup(SIMPLE_URL);
+    let messageManager = tab.linkedBrowser.messageManager;
+    let {toolbox} = yield this.openToolbox("webconsole");
+    let webconsole = toolbox.getPanel("webconsole");
+
+    // Resolve once the first message is received.
+    let onMessageReceived = new Promise(resolve => {
+      function receiveMessages(e, messages) {
+        for (let m of messages) {
+          resolve(m);
+        }
+      }
+      webconsole.hud.ui.once("new-messages", receiveMessages);
+    });
+
+    // Load a frame script using a data URI so we can do logs
+    // from the page.
+    messageManager.loadFrameScript("data:,(" + encodeURIComponent(
+      `function () {
+        addMessageListener("do-dir", function () {
+          content.console.dir(Array.from({length:1000}).reduce((res, _, i)=> {
+            res["item_" + i] = i;
+            return res;
+          }, {}));
+        });
+      }`
+    ) + ")()", true);
+
+    // Kick off the logging
+    messageManager.sendAsyncMessage("do-dir");
+
+    let start = performance.now();
+    yield onMessageReceived;
+    const tree = webconsole.hud.ui.outputNode.querySelector(".dir.message .tree");
+    // The tree can be collapsed since the properties are fetched asynchronously.
+    if (tree.querySelectorAll(".node").length === 1) {
+      // If this is the case, we wait for the properties to be fetched and displayed.
+      yield new Promise(resolve => {
+        const observer = new MutationObserver(mutations => {
+          resolve(mutations);
+          observer.disconnect();
+        });
+        observer.observe(tree, {
+          childList: true
+        });
+      });
+    }
+
+    this._results.push({
+      name: "console.objectexpand",
+      value: performance.now() - start,
+    });
+
+    yield this.closeToolboxAndLog("console.objectexpanded");
+    yield this.testTeardown();
+  }),
+
   takeCensus(label) {
     let start = performance.now();
 
     this._snapshot.takeCensus({
       breakdown: {
         by: "coarseType",
         objects: {
           by: "objectClass",
@@ -634,12 +692,15 @@ Damp.prototype = {
     }));
 
     if (config.subtests.indexOf("consoleBulkLogging") > -1) {
       tests = tests.concat(this._consoleBulkLoggingTest);
     }
     if (config.subtests.indexOf("consoleStreamLogging") > -1) {
       tests = tests.concat(this._consoleStreamLoggingTest);
     }
+    if (config.subtests.indexOf("consoleObjectExpansion") > -1) {
+      tests = tests.concat(this._consoleObjectExpansionTest);
+    }
 
     this._doSequence(tests, this._doneInternal);
   }
 }
