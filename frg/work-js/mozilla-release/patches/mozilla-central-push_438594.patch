# HG changeset patch
# User Iain Ireland <iireland@mozilla.com>
# Date 1538075826 0
#      Thu Sep 27 19:17:06 2018 +0000
# Node ID dfdc496fab45d4331d624cf16dc9afd48d511ae8
# Parent  414785cda2587dc4eb02df856374d4285f50e1a1
Bug 1492574: Rewrite GetDynamicName to return false if lookup can't be completed r=tcampbell

Depends on D7122

Differential Revision: https://phabricator.services.mozilla.com/D7123

diff --git a/js/src/jit-test/tests/ion/bug1492574.js b/js/src/jit-test/tests/ion/bug1492574.js
--- a/js/src/jit-test/tests/ion/bug1492574.js
+++ b/js/src/jit-test/tests/ion/bug1492574.js
@@ -1,10 +1,8 @@
-// |jit-test| --fuzzing-safe
-
 if (!('oomTest' in this)) {
     quit();
 }
 
 function foo() {}
 function foooooooooooooooooooooooooooooooo() {}
 function fn(s) {
     var o = {a:1}
diff --git a/js/src/jit/CodeGenerator.cpp b/js/src/jit/CodeGenerator.cpp
--- a/js/src/jit/CodeGenerator.cpp
+++ b/js/src/jit/CodeGenerator.cpp
@@ -5310,26 +5310,24 @@ CodeGenerator::visitGetDynamicName(LGetD
     masm.adjustStack(-int32_t(sizeof(Value)));
     masm.moveStackPtrTo(temp2);
 
     masm.setupUnalignedABICall(temp1);
     masm.passABIArg(temp3);
     masm.passABIArg(envChain);
     masm.passABIArg(name);
     masm.passABIArg(temp2);
-    masm.callWithABI(JS_FUNC_TO_DATA_PTR(void*, GetDynamicName));
+    masm.callWithABI(JS_FUNC_TO_DATA_PTR(void*, GetDynamicNamePure));
 
     const ValueOperand out = ToOutValue(lir);
 
     masm.loadValue(Address(masm.getStackPointer(), 0), out);
     masm.adjustStack(sizeof(Value));
 
-    Label undefined;
-    masm.branchTestUndefined(Assembler::Equal, out, &undefined);
-    bailoutFrom(&undefined, lir->snapshot());
+    bailoutIfFalseBool(ReturnReg, lir->snapshot());
 }
 
 typedef bool (*DirectEvalSFn)(JSContext*, HandleObject, HandleScript, HandleValue,
                               HandleString, jsbytecode*, MutableHandleValue);
 static const VMFunction DirectEvalStringInfo =
     FunctionInfo<DirectEvalSFn>(DirectEvalStringFromIon, "DirectEvalStringFromIon");
 
 void
diff --git a/js/src/jit/VMFunctions.cpp b/js/src/jit/VMFunctions.cpp
--- a/js/src/jit/VMFunctions.cpp
+++ b/js/src/jit/VMFunctions.cpp
@@ -705,52 +705,47 @@ CreateThis(JSContext* cx, HandleObject c
                 return false;
             }
         }
     }
 
     return true;
 }
 
-void
-GetDynamicName(JSContext* cx, JSObject* envChain, JSString* str, Value* vp)
+bool
+GetDynamicNamePure(JSContext* cx, JSObject* envChain, JSString* str, Value* vp)
 {
-    // Lookup a string on the env chain, returning either the value found or
-    // undefined through rval. This function is infallible, and cannot GC or
-    // invalidate.
+    // Lookup a string on the env chain, returning the value found through rval.
+    // This function is infallible, and cannot GC or invalidate.
+    // Returns false if the lookup could not be completed without GC.
 
     AutoUnsafeCallWithABI unsafe;
 
     JSAtom* atom;
     if (str->isAtom()) {
         atom = &str->asAtom();
     } else {
         atom = AtomizeString(cx, str);
         if (!atom) {
-            vp->setUndefined();
             cx->recoverFromOutOfMemory();
-            return;
+            return false;
         }
     }
 
     if (!frontend::IsIdentifier(atom) || frontend::IsKeyword(atom)) {
-        vp->setUndefined();
-        return;
+        return false;
     }
 
     PropertyResult prop;
     JSObject* scope = nullptr;
     JSObject* pobj = nullptr;
     if (LookupNameNoGC(cx, atom->asPropertyName(), envChain, &scope, &pobj, &prop)) {
-        if (FetchNameNoGC(pobj, prop, MutableHandleValue::fromMarkedLocation(vp))) {
-            return;
-        }
+        return FetchNameNoGC(pobj, prop, MutableHandleValue::fromMarkedLocation(vp));
     }
-
-    vp->setUndefined();
+    return false;
 }
 
 void
 PostWriteBarrier(JSRuntime* rt, js::gc::Cell* cell)
 {
     AutoUnsafeCallWithABI unsafe;
     MOZ_ASSERT(!IsInsideNursery(cell));
     rt->gc.storeBuffer().putWholeCell(cell);
diff --git a/js/src/jit/VMFunctions.h b/js/src/jit/VMFunctions.h
--- a/js/src/jit/VMFunctions.h
+++ b/js/src/jit/VMFunctions.h
@@ -746,17 +746,17 @@ bool OperatorIn(JSContext* cx, HandleVal
 bool OperatorInI(JSContext* cx, uint32_t index, HandleObject obj, bool* out);
 
 MOZ_MUST_USE bool
 GetIntrinsicValue(JSContext* cx, HandlePropertyName name, MutableHandleValue rval);
 
 MOZ_MUST_USE bool
 CreateThis(JSContext* cx, HandleObject callee, HandleObject newTarget, MutableHandleValue rval);
 
-void GetDynamicName(JSContext* cx, JSObject* scopeChain, JSString* str, Value* vp);
+bool GetDynamicNamePure(JSContext* cx, JSObject* scopeChain, JSString* str, Value* vp);
 
 void PostWriteBarrier(JSRuntime* rt, js::gc::Cell* cell);
 void PostGlobalWriteBarrier(JSRuntime* rt, GlobalObject* obj);
 
 enum class IndexInBounds { Yes, Maybe };
 
 template <IndexInBounds InBounds>
 void PostWriteElementBarrier(JSRuntime* rt, JSObject* obj, int32_t index);
