# HG changeset patch
# User Jonathan Kew <jkew@mozilla.com>
# Date 1526392766 -3600
# Node ID 711990fd66aa72f2f42793c278666990d5b4a566
# Parent  7a875278bca9b33d147343cc377a205a71b39af2
Bug 1457417 - Work around Core Text mishandling of 'opsz' axis when set to the font's default, by adjusting to a fractionally-different setting. r=jwatt

diff --git a/gfx/thebes/gfxMacFont.cpp b/gfx/thebes/gfxMacFont.cpp
--- a/gfx/thebes/gfxMacFont.cpp
+++ b/gfx/thebes/gfxMacFont.cpp
@@ -18,16 +18,23 @@
 #include "gfxTextRun.h"
 #include "nsCocoaFeatures.h"
 
 #include "cairo-quartz.h"
 
 using namespace mozilla;
 using namespace mozilla::gfx;
 
+template<class T>
+struct TagEquals {
+    bool Equals(const T& aIter, uint32_t aTag) const {
+        return aIter.mTag == aTag;
+    }
+};
+
 gfxMacFont::gfxMacFont(const RefPtr<UnscaledFontMac>& aUnscaledFont,
                        MacOSFontEntry *aFontEntry,
                        const gfxFontStyle *aFontStyle)
     : gfxFont(aUnscaledFont, aFontEntry, aFontStyle),
       mCGFont(nullptr),
       mCTFont(nullptr),
       mFontFace(nullptr),
       mFontSmoothingBackgroundColor(aFontStyle->fontSmoothingBackgroundColor),
@@ -42,16 +49,65 @@ gfxMacFont::gfxMacFont(const RefPtr<Unsc
             return;
         }
 
         // Get the variation settings needed to instantiate the fontEntry
         // for a particular fontStyle.
         AutoTArray<gfxFontVariation,4> vars;
         aFontEntry->GetVariationsForStyle(vars, *aFontStyle);
 
+        // Because of a Core Text bug, we need to ensure that if the font has
+        // an 'opsz' axis, it is always explicitly set, and NOT to the font's
+        // default value. (See bug 1457417.)
+        // We record the result of searching the font's axes in the font entry,
+        // so that this only has to be done by the first instance created for
+        // a given font resource.
+        const uint32_t kOpszTag = HB_TAG('o','p','s','z');
+
+        if (!aFontEntry->mCheckedForOpszAxis) {
+            aFontEntry->mCheckedForOpszAxis = true;
+            AutoTArray<gfxFontVariationAxis,4> axes;
+            aFontEntry->GetVariationAxes(axes);
+            auto index =
+                axes.IndexOf(kOpszTag, 0, TagEquals<gfxFontVariationAxis>());
+            if (index == axes.NoIndex) {
+                aFontEntry->mHasOpszAxis = false;
+            } else {
+                const auto& axis = axes[index];
+                aFontEntry->mHasOpszAxis = true;
+                aFontEntry->mOpszAxis = axis;
+                // Pick a slightly-adjusted version of the default that we'll
+                // use to work around Core Text's habit of ignoring any attempt
+                // to explicitly set the default value.
+                aFontEntry->mAdjustedDefaultOpsz =
+                    axis.mDefaultValue == axis.mMinValue
+                        ? axis.mDefaultValue + 0.001f
+                        : axis.mDefaultValue - 0.001f;
+            }
+        }
+
+        // Add 'opsz' if not present, or tweak its value if it looks too close
+        // to the default (after clamping to the font's available range).
+        if (aFontEntry->mHasOpszAxis) {
+            auto index =
+                vars.IndexOf(kOpszTag, 0, TagEquals<gfxFontVariation>());
+            if (index == vars.NoIndex) {
+                gfxFontVariation opsz{kOpszTag, aFontEntry->mAdjustedDefaultOpsz};
+                vars.AppendElement(opsz);
+            } else {
+                // Figure out a "safe" value that Core Text won't ignore.
+                auto& value = vars[index].mValue;
+                auto& axis = aFontEntry->mOpszAxis;
+                value = fmin(fmax(value, axis.mMinValue), axis.mMaxValue);
+                if (abs(value - axis.mDefaultValue) < 0.001f) {
+                    value = aFontEntry->mAdjustedDefaultOpsz;
+                }
+            }
+        }
+
         mCGFont =
             UnscaledFontMac::CreateCGFontWithVariations(baseFont,
                                                         vars.Length(),
                                                         vars.Elements());
         if (!mCGFont) {
           ::CFRetain(baseFont);
           mCGFont = baseFont;
         }
diff --git a/gfx/thebes/gfxMacPlatformFontList.h b/gfx/thebes/gfxMacPlatformFontList.h
--- a/gfx/thebes/gfxMacPlatformFontList.h
+++ b/gfx/thebes/gfxMacPlatformFontList.h
@@ -25,16 +25,17 @@
 
 class gfxMacPlatformFontList;
 
 // a single member of a font family (i.e. a single face, such as Times Italic)
 class MacOSFontEntry : public gfxFontEntry
 {
 public:
     friend class gfxMacPlatformFontList;
+    friend class gfxMacFont;
 
     MacOSFontEntry(const nsAString& aPostscriptName, WeightRange aWeight,
                    bool aIsStandardFace = false,
                    double aSizeHint = 0.0);
 
     // for use with data fonts
     MacOSFontEntry(const nsAString& aPostscriptName, CGFontRef aFontRef,
                    WeightRange aWeight, StretchRange aStretch, SlantStyleRange aStyle,
@@ -99,16 +100,29 @@ protected:
     bool mRequiresAAT;
     bool mIsCFF;
     bool mIsCFFInitialized;
     bool mHasVariations;
     bool mHasVariationsInitialized;
     bool mHasAATSmallCaps;
     bool mHasAATSmallCapsInitialized;
     bool mCheckedForTracking;
+
+    // To work around Core Text's mishandling of the default value for 'opsz',
+    // we need to record whether the font has an a optical size axis, what its
+    // range and default values are, and a usable close-to-default alternative.
+    // (See bug 1457417 for details.)
+    // These fields are used by gfxMacFont, but stored in the font entry so
+    // that only a single font instance needs to inspect the available
+    // variations.
+    bool mCheckedForOpszAxis;
+    bool mHasOpszAxis;
+    gfxFontVariationAxis mOpszAxis;
+    float mAdjustedDefaultOpsz;
+
     nsTHashtable<nsUint32HashKey> mAvailableTables;
 
     mozilla::ThreadSafeWeakPtr<mozilla::gfx::UnscaledFontMac> mUnscaledFont;
 
     // For AAT font being shaped by Core Text, a strong reference to the 'trak'
     // table (if present).
     hb_blob_t* mTrakTable;
     // Cached pointers to tables within 'trak', initialized by ParseTrakTable.
diff --git a/gfx/thebes/gfxMacPlatformFontList.mm b/gfx/thebes/gfxMacPlatformFontList.mm
--- a/gfx/thebes/gfxMacPlatformFontList.mm
+++ b/gfx/thebes/gfxMacPlatformFontList.mm
@@ -376,16 +376,17 @@ MacOSFontEntry::MacOSFontEntry(const nsA
       mRequiresAAT(false),
       mIsCFF(false),
       mIsCFFInitialized(false),
       mHasVariations(false),
       mHasVariationsInitialized(false),
       mHasAATSmallCaps(false),
       mHasAATSmallCapsInitialized(false),
       mCheckedForTracking(false),
+      mCheckedForOpszAxis(false),
       mTrakTable(nullptr),
       mTrakValues(nullptr),
       mTrakSizeTable(nullptr)
 {
     mWeightRange = aWeight;
 }
 
 MacOSFontEntry::MacOSFontEntry(const nsAString& aPostscriptName,
@@ -402,16 +403,17 @@ MacOSFontEntry::MacOSFontEntry(const nsA
       mRequiresAAT(false),
       mIsCFF(false),
       mIsCFFInitialized(false),
       mHasVariations(false),
       mHasVariationsInitialized(false),
       mHasAATSmallCaps(false),
       mHasAATSmallCapsInitialized(false),
       mCheckedForTracking(false),
+      mCheckedForOpszAxis(false),
       mTrakTable(nullptr),
       mTrakValues(nullptr),
       mTrakSizeTable(nullptr)
 {
     mFontRef = aFontRef;
     mFontRefInitialized = true;
     ::CFRetain(mFontRef);
 

