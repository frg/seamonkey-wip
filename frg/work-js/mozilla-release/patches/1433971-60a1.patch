# HG changeset patch
# User Tom Ritter <tom@mozilla.com>
# Date 1517247379 21600
# Node ID fe06b53fc1500e671dd152108155cc71374350e4
# Parent  97777abedc873537a8aa0aa21d5efc62f181368e
Bug 1433971 Clean up Assertions.h with respect to Unused Attributes r=froydnj

In Bug 1393538 I renamed MOZ_STATIC_ASSERT_UNUSED_ATTRIBUTE to MOZ_UNUSED_ATTRIBUTE,
moved it out of it's #define depth, and used it in toolkit. I also orphaned a
comment.

This was wrong. MOZ_UNUSED_ATTRIBUTE was basically identical to MOZ_MAYBE_UNUSED
which exists in Attributes.h (because it is an attribute, not an assertion.)

Undo that wrong thing: restore MOZ_STATIC_ASSERT_UNUSED_ATTRIBUTE to the correct
place, have toolkit use the correct macro, and remove MOZ_UNUSED_ATTRIBUTE.

MozReview-Commit-ID: 5BWWsXgbm9i

diff --git a/mfbt/Assertions.h b/mfbt/Assertions.h
--- a/mfbt/Assertions.h
+++ b/mfbt/Assertions.h
@@ -61,22 +61,16 @@ TerminateProcess(void* hProcess, unsigne
 MOZ_END_EXTERN_C
 #else
 #  include <signal.h>
 #endif
 #ifdef ANDROID
 #  include <android/log.h>
 #endif
 
-#if defined(__GNUC__)
-#  define MOZ_UNUSED_ATTRIBUTE __attribute__((unused))
-#else
-#  define MOZ_UNUSED_ATTRIBUTE /* nothing */
-#endif
-
 /*
  * MOZ_STATIC_ASSERT may be used to assert a condition *at compile time* in C.
  * In C++11, static_assert is provided by the compiler to the same effect.
  * This can be useful when you make certain assumptions about what must hold for
  * optimal, or even correct, behavior.  For example, you might assert that the
  * size of a struct is a multiple of the target architecture's word size:
  *
  *   struct S { ... };
@@ -91,16 +85,21 @@ MOZ_END_EXTERN_C
  * typedef could be used.
  */
 #ifndef __cplusplus
    /*
     * Some of the definitions below create an otherwise-unused typedef.  This
     * triggers compiler warnings with some versions of gcc, so mark the typedefs
     * as permissibly-unused to disable the warnings.
     */
+#  if defined(__GNUC__)
+#    define MOZ_STATIC_ASSERT_UNUSED_ATTRIBUTE __attribute__((unused))
+#  else
+#    define MOZ_STATIC_ASSERT_UNUSED_ATTRIBUTE /* nothing */
+#  endif
 #  define MOZ_STATIC_ASSERT_GLUE1(x, y)          x##y
 #  define MOZ_STATIC_ASSERT_GLUE(x, y)           MOZ_STATIC_ASSERT_GLUE1(x, y)
 #  if defined(__SUNPRO_CC)
      /*
       * The Sun Studio C++ compiler is buggy when declaring, inside a function,
       * another extern'd function with an array argument whose length contains a
       * sizeof, triggering the error message "sizeof expression not accepted as
       * size of array parameter".  This bug (6688515, not public yet) would hit
@@ -123,20 +122,20 @@ MOZ_END_EXTERN_C
       * we include the line number in the function name to do the best we can to
       * avoid conflicts.  These should be rare: a conflict would require use of
       * MOZ_STATIC_ASSERT on the same line in separate files in the same
       * translation unit, *and* the uses would have to be in code with
       * different linkage, *and* the first observed use must be in C++-linkage
       * code.
       */
 #    define MOZ_STATIC_ASSERT(cond, reason) \
-       typedef int MOZ_STATIC_ASSERT_GLUE(moz_static_assert, __COUNTER__)[(cond) ? 1 : -1] MOZ_UNUSED_ATTRIBUTE
+       typedef int MOZ_STATIC_ASSERT_GLUE(moz_static_assert, __COUNTER__)[(cond) ? 1 : -1] MOZ_STATIC_ASSERT_UNUSED_ATTRIBUTE
 #  else
 #    define MOZ_STATIC_ASSERT(cond, reason) \
-       extern void MOZ_STATIC_ASSERT_GLUE(moz_static_assert, __LINE__)(int arg[(cond) ? 1 : -1]) MOZ_UNUSED_ATTRIBUTE
+       extern void MOZ_STATIC_ASSERT_GLUE(moz_static_assert, __LINE__)(int arg[(cond) ? 1 : -1]) MOZ_STATIC_ASSERT_UNUSED_ATTRIBUTE
 #  endif
 
 #define MOZ_STATIC_ASSERT_IF(cond, expr, reason)  MOZ_STATIC_ASSERT(!(cond) || (expr), reason)
 #else
 #define MOZ_STATIC_ASSERT_IF(cond, expr, reason)  static_assert(!(cond) || (expr), reason)
 #endif
 
 MOZ_BEGIN_EXTERN_C
diff --git a/toolkit/xre/nsNativeAppSupportWin.cpp b/toolkit/xre/nsNativeAppSupportWin.cpp
--- a/toolkit/xre/nsNativeAppSupportWin.cpp
+++ b/toolkit/xre/nsNativeAppSupportWin.cpp
@@ -105,17 +105,17 @@ struct Win32Mutex {
         printf( "CreateMutex error = 0x%08X\n", (int)GetLastError() );
 #endif
     }
     ~Win32Mutex() {
         if ( mHandle ) {
             // Make sure we release it if we own it.
             Unlock();
 
-            BOOL rc MOZ_UNUSED_ATTRIBUTE = CloseHandle( mHandle );
+            BOOL rc MOZ_MAYBE_UNUSED = CloseHandle( mHandle );
 #if MOZ_DEBUG_DDE
             if ( !rc ) {
                 printf( "CloseHandle error = 0x%08X\n", (int)GetLastError() );
             }
 #endif
         }
     }
     BOOL Lock( DWORD timeout ) {
