# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1586391891 0
# Node ID b6bd082527123a713240e85ecc94062fd437a895
# Parent  5152b5be7eadac4533bbb902ebb0f9633699072c
Bug 1628205 - Convert nsinstall.py to python 3. r=rstewart

Also enable all config/tests with python3. unit-nsinstall.py was the
last one that didn't pass with python 3.

Switch the test to using @unittest.skipIf and disable the subprocess
test because we purposely broke running nsinstall.py independently with
python 2.

Differential Revision: https://phabricator.services.mozilla.com/D70160

diff --git a/config/config.mk b/config/config.mk
--- a/config/config.mk
+++ b/config/config.mk
@@ -324,17 +324,17 @@ GARBAGE		+= $(DEPENDENCIES) core $(wildc
 
 ifeq ($(OS_ARCH),Darwin)
 ifndef NSDISTMODE
 NSDISTMODE=absolute_symlink
 endif
 PWD := $(CURDIR)
 endif
 
-NSINSTALL_PY := $(PYTHON) $(abspath $(MOZILLA_DIR)/config/nsinstall.py)
+NSINSTALL_PY := $(PYTHON3) $(abspath $(MOZILLA_DIR)/config/nsinstall.py)
 ifneq (,$(or $(filter WINNT,$(HOST_OS_ARCH)),$(if $(COMPILE_ENVIRONMENT),,1)))
 NSINSTALL = $(NSINSTALL_PY)
 else
 NSINSTALL = $(DEPTH)/config/nsinstall$(HOST_BIN_SUFFIX)
 endif # WINNT
 
 
 ifeq (,$(CROSS_COMPILE)$(filter-out WINNT, $(OS_ARCH)))
diff --git a/config/nsinstall.py b/config/nsinstall.py
--- a/config/nsinstall.py
+++ b/config/nsinstall.py
@@ -10,16 +10,17 @@
 # The basic limitation is, it doesn't even try to link and ignores
 # all related options.
 from __future__ import absolute_import
 from __future__ import print_function
 from optparse import OptionParser
 import mozfile
 import os
 import os.path
+import six
 import sys
 import shutil
 
 
 def _nsinstall_internal(argv):
     usage = "usage: %prog [options] arg1 [arg2 ...] target-directory"
     p = OptionParser(usage=usage)
 
@@ -149,41 +150,13 @@ def _nsinstall_internal(argv):
 
     copy_all_entries(args, target)
     return 0
 
 # nsinstall as a native command is always UTF-8
 
 
 def nsinstall(argv):
-    return _nsinstall_internal([unicode(arg, "utf-8") for arg in argv])
+    return _nsinstall_internal([six.ensure_text(arg, "utf-8") for arg in argv])
 
 
 if __name__ == '__main__':
-    # sys.argv corrupts characters outside the system code page on Windows
-    # <http://bugs.python.org/issue2128>. Use ctypes instead. This is also
-    # useful because switching to Unicode strings makes python use the wide
-    # Windows APIs, which is what we want here since the wide APIs normally do a
-    # better job at handling long paths and such.
-    if sys.platform == "win32":
-        import ctypes
-        from ctypes import wintypes
-        GetCommandLine = ctypes.windll.kernel32.GetCommandLineW
-        GetCommandLine.argtypes = []
-        GetCommandLine.restype = wintypes.LPWSTR
-
-        CommandLineToArgv = ctypes.windll.shell32.CommandLineToArgvW
-        CommandLineToArgv.argtypes = [
-            wintypes.LPWSTR, ctypes.POINTER(ctypes.c_int)]
-        CommandLineToArgv.restype = ctypes.POINTER(wintypes.LPWSTR)
-
-        argc = ctypes.c_int(0)
-        argv_arr = CommandLineToArgv(GetCommandLine(), ctypes.byref(argc))
-        # The first argv will be "python", the second will be the .py file
-        argv = argv_arr[1:argc.value]
-    else:
-        # For consistency, do it on Unix as well
-        if sys.stdin.encoding is not None:
-            argv = [unicode(arg, sys.stdin.encoding) for arg in sys.argv]
-        else:
-            argv = [unicode(arg) for arg in sys.argv]
-
-    sys.exit(_nsinstall_internal(argv[1:]))
+    sys.exit(_nsinstall_internal(sys.argv[1:]))
diff --git a/config/tests/python.ini b/config/tests/python.ini
--- a/config/tests/python.ini
+++ b/config/tests/python.ini
@@ -1,8 +1,7 @@
 [DEFAULT]
 subsuite = mozbuild
-skip-if = python == 3
 
 [test_mozbuild_reading.py]
 [unit-mozunit.py]
 [unit-nsinstall.py]
 [unit-printprereleasesuffix.py]
diff --git a/config/tests/unit-nsinstall.py b/config/tests/unit-nsinstall.py
--- a/config/tests/unit-nsinstall.py
+++ b/config/tests/unit-nsinstall.py
@@ -1,12 +1,13 @@
 from __future__ import absolute_import
 import unittest
 
 import os
+import six
 import sys
 import os.path
 import time
 from tempfile import mkdtemp
 from shutil import rmtree
 import mozunit
 from mozprocess import processhandler
 
@@ -29,19 +30,19 @@ class TestNsinstall(unittest.TestCase):
 
     def setUp(self):
         self.tmpdir = mkdtemp()
 
     def tearDown(self):
         # Unicode strings means non-ASCII children can be deleted properly on
         # Windows
         if sys.stdin.encoding is None:
-            tmpdir = unicode(self.tmpdir)
+            tmpdir = six.ensure_text(self.tmpdir)
         else:
-            tmpdir = unicode(self.tmpdir, sys.stdin.encoding)
+            tmpdir = six.ensure_text(self.tmpdir, sys.stdin.encoding)
         rmtree(tmpdir)
 
     # utility methods for tests
     def touch(self, file, dir=None):
         if dir is None:
             dir = self.tmpdir
         f = os.path.join(dir, file)
         open(f, 'w').close()
@@ -116,67 +117,69 @@ class TestNsinstall(unittest.TestCase):
         t = int(time.time()) - 30
         os.utime(testfile, (t, t))
         self.assertEqual(nsinstall(["-t", testfile, testdir]), 0)
         destfile = os.path.join(testdir, "testfile")
         self.assert_(os.path.isfile(destfile))
         self.assertEqual(os.stat(testfile).st_mtime,
                          os.stat(destfile).st_mtime)
 
-    if sys.platform != "win32":
-        # can't run this test on windows, don't have real file modes there
-        def test_nsinstall_m(self):
-            "Test that nsinstall -m works (set mode)"
-            testfile = self.touch("testfile")
-            mode = 0o600
-            os.chmod(testfile, mode)
-            testdir = self.mkdirs("testdir")
-            self.assertEqual(nsinstall(["-m", "{0:04o}"
-                                        .format(mode), testfile, testdir]), 0)
-            destfile = os.path.join(testdir, "testfile")
-            self.assert_(os.path.isfile(destfile))
-            self.assertEqual(os.stat(testfile).st_mode,
-                             os.stat(destfile).st_mode)
+    @unittest.skipIf(sys.platform == "win32", "Windows doesn't have real file modes")
+    def test_nsinstall_m(self):
+        "Test that nsinstall -m works (set mode)"
+        testfile = self.touch("testfile")
+        mode = 0o600
+        os.chmod(testfile, mode)
+        testdir = self.mkdirs("testdir")
+        self.assertEqual(nsinstall(["-m", "{0:04o}"
+                                    .format(mode), testfile, testdir]), 0)
+        destfile = os.path.join(testdir, "testfile")
+        self.assert_(os.path.isfile(destfile))
+        self.assertEqual(os.stat(testfile).st_mode,
+                         os.stat(destfile).st_mode)
 
     def test_nsinstall_d(self):
         "Test that nsinstall -d works (create directories in target)"
         # -d makes no sense to me, but ok!
         testfile = self.touch("testfile")
         testdir = self.mkdirs("testdir")
         destdir = os.path.join(testdir, "subdir")
         self.assertEqual(nsinstall(["-d", testfile, destdir]), 0)
         self.assert_(os.path.isdir(os.path.join(destdir, "testfile")))
 
-    if RUN_NON_ASCII_TESTS:
-        def test_nsinstall_non_ascii(self):
-            "Test that nsinstall handles non-ASCII files"
-            filename = u"\u2325\u3452\u2415\u5081"
-            testfile = self.touch(filename)
-            testdir = self.mkdirs(u"\u4241\u1D04\u1414")
-            self.assertEqual(nsinstall([testfile.encode("utf-8"),
-                                        testdir.encode("utf-8")]), 0)
+    @unittest.skipIf(not RUN_NON_ASCII_TESTS, "Skipping non ascii tests")
+    def test_nsinstall_non_ascii(self):
+        "Test that nsinstall handles non-ASCII files"
+        filename = u"\u2325\u3452\u2415\u5081"
+        testfile = self.touch(filename)
+        testdir = self.mkdirs(u"\u4241\u1D04\u1414")
+        self.assertEqual(nsinstall([testfile.encode("utf-8"),
+                                    testdir.encode("utf-8")]), 0)
 
-            destfile = os.path.join(testdir, filename)
-            self.assert_(os.path.isfile(destfile))
+        destfile = os.path.join(testdir, filename)
+        self.assert_(os.path.isfile(destfile))
 
-        def test_nsinstall_non_ascii_subprocess(self):
-            "Test that nsinstall as a subprocess handles non-ASCII files"
-            filename = u"\u2325\u3452\u2415\u5081"
-            testfile = self.touch(filename)
-            testdir = self.mkdirs(u"\u4241\u1D04\u1414")
-            # We don't use subprocess because it can't handle Unicode on
-            # Windows <http://bugs.python.org/issue1759845>. mozprocess calls
-            # CreateProcessW directly so it's perfect.
-            p = processhandler.ProcessHandlerMixin([sys.executable,
-                                                    NSINSTALL_PATH,
-                                                    testfile, testdir])
-            p.run()
-            rv = p.wait()
+    # Executing nsinstall.py with python 2 is not supported.
+    @unittest.skipIf(not RUN_NON_ASCII_TESTS or sys.version_info[0] == 2,
+                     "Skipping non ascii tests")
+    def test_nsinstall_non_ascii_subprocess(self):
+        "Test that nsinstall as a subprocess handles non-ASCII files"
+        filename = u"\u2325\u3452\u2415\u5081"
+        testfile = self.touch(filename)
+        testdir = self.mkdirs(u"\u4241\u1D04\u1414")
+        # We don't use subprocess because it can't handle Unicode on
+        # Windows <http://bugs.python.org/issue1759845>. mozprocess calls
+        # CreateProcessW directly so it's perfect.
+        p = processhandler.ProcessHandlerMixin([sys.executable,
+                                                NSINSTALL_PATH,
+                                                testfile, testdir])
+        p.run()
+        rv = p.wait()
 
-            self.assertEqual(rv, 0)
-            destfile = os.path.join(testdir, filename)
-            self.assert_(os.path.isfile(destfile))
+        self.assertEqual(rv, 0)
+        destfile = os.path.join(testdir, filename)
+        self.assert_(os.path.isfile(destfile))
 
     # TODO: implement -R, -l, -L and test them!
 
 
 if __name__ == '__main__':
     mozunit.main()
