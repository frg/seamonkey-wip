# HG changeset patch
# User Ben Kelly <ben@wanderview.com>
# Date 1513695887 18000
# Node ID 56b3aab1d763d41b4672b06cc1034f6f5895357d
# Parent  b8380e17e90aa8c45d3c0cc76429aa1d0a585d8b
Bug 1425316 P1 Expose a chrome-only Window.shouldReportForServiceWorkerScope() method that initially forwards to ServiceWorkerManager. r=asuth

diff --git a/devtools/server/actors/webconsole/listeners.js b/devtools/server/actors/webconsole/listeners.js
--- a/devtools/server/actors/webconsole/listeners.js
+++ b/devtools/server/actors/webconsole/listeners.js
@@ -278,17 +278,17 @@ ConsoleAPIListener.prototype =
     let workerType = WebConsoleUtils.getWorkerType(message);
 
     if (this.window && workerType === "ServiceWorker") {
       // For messages from Service Workers, message.ID is the
       // scope, which can be used to determine whether it's controlling
       // a window.
       let scope = message.ID;
 
-      if (!swm.shouldReportToWindow(this.window, scope)) {
+      if (!this.window.shouldReportForServiceWorkerScope(scope)) {
         return false;
       }
     }
 
     if (this.window && !workerType) {
       let msgWindow = Services.wm.getCurrentInnerWindowWithId(message.innerID);
       if (!msgWindow || !isWindowIncluded(this.window, msgWindow)) {
         // Not the same window!
diff --git a/dom/base/nsGlobalWindow.cpp b/dom/base/nsGlobalWindow.cpp
--- a/dom/base/nsGlobalWindow.cpp
+++ b/dom/base/nsGlobalWindow.cpp
@@ -78,16 +78,17 @@
 #include "mozilla/Sprintf.h"
 #include "mozilla/Unused.h"
 
 // Other Classes
 #include "mozilla/dom/BarProps.h"
 #include "nsContentCID.h"
 #include "nsLayoutStatics.h"
 #include "nsCCUncollectableMarker.h"
+#include "mozilla/dom/workers/ServiceWorkerManager.h"
 #include "mozilla/dom/workers/Workers.h"
 #include "mozilla/dom/ToJSValue.h"
 #include "nsJSPrincipals.h"
 #include "mozilla/Attributes.h"
 #include "mozilla/Debug.h"
 #include "mozilla/EventListenerManager.h"
 #include "mozilla/EventStates.h"
 #include "mozilla/MouseEvents.h"
@@ -4435,16 +4436,31 @@ nsPIDOMWindowInner::GetClientState() con
 }
 
 Maybe<ServiceWorkerDescriptor>
 nsPIDOMWindowInner::GetController() const
 {
   return Move(nsGlobalWindow::Cast(this)->GetController());
 }
 
+bool
+nsGlobalWindow::ShouldReportForServiceWorkerScope(const nsAString& aScope)
+{
+  RefPtr<workers::ServiceWorkerManager> swm =
+    workers::ServiceWorkerManager::GetInstance();
+  NS_ENSURE_TRUE(swm, false);
+
+  bool aResult = false;
+  nsresult rv = swm->ShouldReportToWindow(GetOuterWindowInternal(),
+                                          NS_ConvertUTF16toUTF8(aScope), &aResult);
+  NS_ENSURE_SUCCESS(rv, false);
+
+  return aResult;
+}
+
 void
 nsGlobalWindow::UpdateTopInnerWindow()
 {
   if (!IsInnerWindow() || AsInner()->IsTopInnerWindow()) {
     return;
   }
 
   AsInner()->UpdateWebSocketCount(-(int32_t)mNumOfOpenWebSockets);
diff --git a/dom/base/nsGlobalWindow.h b/dom/base/nsGlobalWindow.h
--- a/dom/base/nsGlobalWindow.h
+++ b/dom/base/nsGlobalWindow.h
@@ -1314,16 +1314,18 @@ public:
                     JS::MutableHandle<JS::Value> aRetval,
                     mozilla::ErrorResult& aError);
 
   already_AddRefed<nsWindowRoot> GetWindowRootOuter();
   already_AddRefed<nsWindowRoot> GetWindowRoot(mozilla::ErrorResult& aError);
 
   mozilla::dom::Performance* GetPerformance();
 
+  bool ShouldReportForServiceWorkerScope(const nsAString& aScope);
+
   void UpdateTopInnerWindow();
 
   virtual bool IsInSyncOperation() override
   {
     return GetExtantDoc() && GetExtantDoc()->IsInSyncOperation();
   }
 
 protected:
diff --git a/dom/interfaces/base/nsIServiceWorkerManager.idl b/dom/interfaces/base/nsIServiceWorkerManager.idl
--- a/dom/interfaces/base/nsIServiceWorkerManager.idl
+++ b/dom/interfaces/base/nsIServiceWorkerManager.idl
@@ -229,15 +229,13 @@ interface nsIServiceWorkerManager : nsIS
                                      [optional] in uint32_t aDataLength,
                                      [optional, array, size_is(aDataLength)] in uint8_t aDataBytes);
   void sendPushSubscriptionChangeEvent(in ACString aOriginAttributes,
                                        in ACString scope);
 
   void addListener(in nsIServiceWorkerManagerListener aListener);
 
   void removeListener(in nsIServiceWorkerManagerListener aListener);
-
-  bool shouldReportToWindow(in mozIDOMWindowProxy aWindow, in ACString aScope);
 };
 
 %{ C++
 #define SERVICEWORKERMANAGER_CONTRACTID "@mozilla.org/serviceworkers/manager;1"
 %}
diff --git a/dom/webidl/Window.webidl b/dom/webidl/Window.webidl
--- a/dom/webidl/Window.webidl
+++ b/dom/webidl/Window.webidl
@@ -336,16 +336,23 @@ partial interface Window {
 
   [Throws, ChromeOnly] any getInterface(IID iid);
 
   /**
    * Same as nsIDOMWindow.windowRoot, useful for event listener targeting.
    */
   [ChromeOnly, Throws]
   readonly attribute WindowRoot? windowRoot;
+
+  /**
+   * ChromeOnly method to determine if a particular window should see console
+   * reports from service workers of the given scope.
+   */
+  [ChromeOnly]
+  boolean shouldReportForServiceWorkerScope(USVString aScope);
 };
 
 Window implements TouchEventHandlers;
 
 Window implements OnErrorEventHandlerForWindow;
 
 #if defined(MOZ_WIDGET_ANDROID)
 // https://compat.spec.whatwg.org/#windoworientation-interface
diff --git a/dom/workers/ServiceWorkerManager.cpp b/dom/workers/ServiceWorkerManager.cpp
--- a/dom/workers/ServiceWorkerManager.cpp
+++ b/dom/workers/ServiceWorkerManager.cpp
@@ -3626,17 +3626,17 @@ ServiceWorkerManager::RemoveListener(nsI
     return NS_ERROR_INVALID_ARG;
   }
 
   mListeners.RemoveElement(aListener);
 
   return NS_OK;
 }
 
-NS_IMETHODIMP
+nsresult
 ServiceWorkerManager::ShouldReportToWindow(mozIDOMWindowProxy* aWindow,
                                            const nsACString& aScope,
                                            bool* aResult)
 {
   AssertIsOnMainThread();
   MOZ_ASSERT(aResult);
 
   *aResult = false;
diff --git a/dom/workers/ServiceWorkerManager.h b/dom/workers/ServiceWorkerManager.h
--- a/dom/workers/ServiceWorkerManager.h
+++ b/dom/workers/ServiceWorkerManager.h
@@ -319,16 +319,20 @@ public:
   NotifyUnregister(nsIPrincipal* aPrincipal, const nsAString& aScope);
 
   void
   WorkerIsIdle(ServiceWorkerInfo* aWorker);
 
   void
   CheckPendingReadyPromises();
 
+  nsresult
+  ShouldReportToWindow(mozIDOMWindowProxy* aWindow, const nsACString& aScope,
+                       bool* aResult);
+
 private:
   ServiceWorkerManager();
   ~ServiceWorkerManager();
 
   void
   Init(ServiceWorkerRegistrar* aRegistrar);
 
   void
