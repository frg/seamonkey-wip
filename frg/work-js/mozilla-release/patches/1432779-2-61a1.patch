# HG changeset patch
# User Dan Glastonbury <dan.glastonbury@gmail.com>
# Date 1521711759 -36000
# Node ID b98af64fa15c8e3ab4b5a7c13d25cb41723e098b
# Parent  bca1660fc65cf338bcad03bb8e02911519bab1ec
Bug 1432779 - Update cubeb-pulse-rs to commit 247b01d. r=jya

MozReview-Commit-ID: ACsPdk19RoA


diff --git a/media/libcubeb/cubeb-pulse-rs/Cargo.toml b/media/libcubeb/cubeb-pulse-rs/Cargo.toml
--- a/media/libcubeb/cubeb-pulse-rs/Cargo.toml
+++ b/media/libcubeb/cubeb-pulse-rs/Cargo.toml
@@ -1,17 +1,17 @@
 [package]
 name = "cubeb-pulse"
-version = "0.1.1"
+version = "0.2.0"
 authors = ["Dan Glastonbury <dglastonbury@mozilla.com>"]
 description = "Cubeb backed for PulseAudio written in Rust"
 
 [features]
 pulse-dlopen = ["pulse-ffi/dlopen"]
 
 [lib]
 crate-type = ["staticlib", "rlib"]
 
 [dependencies]
-cubeb-backend = "0.4"
+cubeb-backend = "0.5"
 pulse-ffi = { path = "pulse-ffi" }
 pulse = { path = "pulse-rs" }
 semver = "^0.6"
diff --git a/media/libcubeb/cubeb-pulse-rs/README_MOZILLA b/media/libcubeb/cubeb-pulse-rs/README_MOZILLA
--- a/media/libcubeb/cubeb-pulse-rs/README_MOZILLA
+++ b/media/libcubeb/cubeb-pulse-rs/README_MOZILLA
@@ -1,8 +1,8 @@
 The source from this directory was copied from the cubeb-pulse-rs
 git repository using the update.sh script.  The only changes
 made were those applied by update.sh and the addition of
 Makefile.in build files for the Mozilla build system.
 
 The cubeb-pulse-rs git repository is: https://github.com/djg/cubeb-pulse-rs.git
 
-The git commit ID used was 22cdde3e573303649a77e48a19f7ca2c4d308047 (2018-03-06 10:39:46 +1000)
+The git commit ID used was 247b01d8e971d0680fa1cc39e63d17e0fc030556 (2018-03-23 08:27:05 +1000)
diff --git a/media/libcubeb/cubeb-pulse-rs/src/backend/context.rs b/media/libcubeb/cubeb-pulse-rs/src/backend/context.rs
--- a/media/libcubeb/cubeb-pulse-rs/src/backend/context.rs
+++ b/media/libcubeb/cubeb-pulse-rs/src/backend/context.rs
@@ -1,59 +1,26 @@
 // Copyright © 2017-2018 Mozilla Foundation
 //
 // This program is made available under an ISC-style license.  See the
 // accompanying file LICENSE for details.
 
 use backend::*;
-use cubeb_backend::{ffi, log_enabled, ChannelLayout, Context, ContextOps, DeviceCollectionRef,
-                    DeviceId, DeviceType, Error, Ops, Result, Stream, StreamParams,
-                    StreamParamsRef};
+use cubeb_backend::{ffi, log_enabled, Context, ContextOps, DeviceCollectionRef, DeviceId,
+                    DeviceType, Error, Ops, Result, Stream, StreamParams, StreamParamsRef};
 use pulse::{self, ProplistExt};
 use pulse_ffi::*;
 use semver;
 use std::cell::RefCell;
 use std::default::Default;
 use std::ffi::{CStr, CString};
 use std::mem;
 use std::os::raw::c_void;
 use std::ptr;
 
-fn pa_channel_to_cubeb_channel(channel: pulse::ChannelPosition) -> ffi::cubeb_channel {
-    use cubeb_backend::ffi::*;
-    use pulse::ChannelPosition;
-    assert_ne!(channel, ChannelPosition::Invalid);
-    match channel {
-        ChannelPosition::Mono => CHANNEL_MONO,
-        ChannelPosition::FrontLeft => CHANNEL_LEFT,
-        ChannelPosition::FrontRight => CHANNEL_RIGHT,
-        ChannelPosition::FrontCenter => CHANNEL_CENTER,
-        ChannelPosition::SideLeft => CHANNEL_LS,
-        ChannelPosition::SideRight => CHANNEL_RS,
-        ChannelPosition::RearLeft => CHANNEL_RLS,
-        ChannelPosition::RearCenter => CHANNEL_RCENTER,
-        ChannelPosition::RearRight => CHANNEL_RRS,
-        ChannelPosition::LowFreqEffects => CHANNEL_LFE,
-        _ => CHANNEL_INVALID,
-    }
-}
-
-fn channel_map_to_layout(cm: &pulse::ChannelMap) -> ChannelLayout {
-    use cubeb_backend::ffi::{cubeb_channel_map, cubeb_channel_map_to_layout};
-    use pulse::ChannelPosition;
-    let mut cubeb_map: cubeb_channel_map = unsafe { mem::zeroed() };
-    cubeb_map.channels = u32::from(cm.channels);
-    for i in 0usize..cm.channels as usize {
-        cubeb_map.map[i] = pa_channel_to_cubeb_channel(
-            ChannelPosition::try_from(cm.map[i]).unwrap_or(ChannelPosition::Invalid),
-        );
-    }
-    ChannelLayout::from(unsafe { cubeb_channel_map_to_layout(&cubeb_map) })
-}
-
 #[derive(Debug)]
 pub struct DefaultInfo {
     pub sample_spec: pulse::SampleSpec,
     pub channel_map: pulse::ChannelMap,
     pub flags: pulse::SinkFlags,
 }
 
 pub const PULSE_OPS: Ops = capi_new!(PulseContext, PulseStream);
@@ -65,17 +32,18 @@ pub struct PulseContext {
     pub context: Option<pulse::Context>,
     pub default_sink_info: Option<DefaultInfo>,
     pub context_name: Option<CString>,
     pub collection_changed_callback: ffi::cubeb_device_collection_changed_callback,
     pub collection_changed_user_ptr: *mut c_void,
     pub error: bool,
     pub version_2_0_0: bool,
     pub version_0_9_8: bool,
-    #[cfg(feature = "pulse-dlopen")] pub libpulse: LibLoader,
+    #[cfg(feature = "pulse-dlopen")]
+    pub libpulse: LibLoader,
     devids: RefCell<Intern>,
 }
 
 impl PulseContext {
     #[cfg(feature = "pulse-dlopen")]
     fn _new(name: Option<CString>) -> Result<Box<Self>> {
         let libpulse = unsafe { open() };
         if libpulse.is_none() {
@@ -113,17 +81,21 @@ impl PulseContext {
             error: true,
             version_0_9_8: false,
             version_2_0_0: false,
             devids: RefCell::new(Intern::new()),
         }))
     }
 
     fn new(name: Option<&CStr>) -> Result<Box<Self>> {
-        fn server_info_cb(context: &pulse::Context, info: Option<&pulse::ServerInfo>, u: *mut c_void) {
+        fn server_info_cb(
+            context: &pulse::Context,
+            info: Option<&pulse::ServerInfo>,
+            u: *mut c_void,
+        ) {
             fn sink_info_cb(
                 _: &pulse::Context,
                 i: *const pulse::SinkInfo,
                 eol: i32,
                 u: *mut c_void,
             ) {
                 let ctx = unsafe { &mut *(u as *mut PulseContext) };
                 if eol == 0 {
@@ -213,23 +185,16 @@ impl ContextOps for PulseContext {
 
     fn preferred_sample_rate(&mut self) -> Result<u32> {
         match self.default_sink_info {
             Some(ref info) => Ok(info.sample_spec.rate),
             None => Err(Error::error()),
         }
     }
 
-    fn preferred_channel_layout(&mut self) -> Result<ChannelLayout> {
-        match self.default_sink_info {
-            Some(ref info) => Ok(channel_map_to_layout(&info.channel_map)),
-            None => Err(Error::error()),
-        }
-    }
-
     fn enumerate_devices(
         &mut self,
         devtype: DeviceType,
         collection: &DeviceCollectionRef,
     ) -> Result<()> {
         fn add_output_device(
             _: &pulse::Context,
             i: *const pulse::SinkInfo,
@@ -591,22 +556,26 @@ impl PulseContext {
         };
 
         let context_ptr: *mut c_void = self as *mut _ as *mut _;
         if self.context.is_none() {
             return Err(Error::error());
         }
 
         self.mainloop.lock();
-        if let Some(ref context) = self.context {
+        let connected = if let Some(ref context) = self.context {
             context.set_state_callback(error_state, context_ptr);
-            let _ = context.connect(None, pulse::ContextFlags::empty(), ptr::null());
-        }
+            context
+                .connect(None, pulse::ContextFlags::empty(), ptr::null())
+                .is_ok()
+        } else {
+            false
+        };
 
-        if !self.wait_until_context_ready() {
+        if !connected || !self.wait_until_context_ready() {
             self.mainloop.unlock();
             self.context_destroy();
             return Err(Error::error());
         }
 
         self.mainloop.unlock();
 
         let version_str = unsafe { CStr::from_ptr(pulse::library_version()) };
diff --git a/media/libcubeb/cubeb-pulse-rs/src/backend/mod.rs b/media/libcubeb/cubeb-pulse-rs/src/backend/mod.rs
--- a/media/libcubeb/cubeb-pulse-rs/src/backend/mod.rs
+++ b/media/libcubeb/cubeb-pulse-rs/src/backend/mod.rs
@@ -1,16 +1,15 @@
 // Copyright © 2017-2018 Mozilla Foundation
 //
 // This program is made available under an ISC-style license.  See the
 // accompanying file LICENSE for details.
 
 mod context;
 mod cork_state;
-mod mixer;
 mod stream;
 mod intern;
 
 pub use self::context::PulseContext;
 use self::intern::Intern;
 pub use self::stream::Device;
 pub use self::stream::PulseStream;
 use std::ffi::CStr;
diff --git a/media/libcubeb/cubeb-pulse-rs/src/backend/stream.rs b/media/libcubeb/cubeb-pulse-rs/src/backend/stream.rs
--- a/media/libcubeb/cubeb-pulse-rs/src/backend/stream.rs
+++ b/media/libcubeb/cubeb-pulse-rs/src/backend/stream.rs
@@ -11,44 +11,56 @@ use pulse::{self, CVolumeExt, ChannelMap
 use pulse_ffi::*;
 use std::{mem, ptr};
 use std::ffi::{CStr, CString};
 use std::os::raw::{c_long, c_void};
 
 const PULSE_NO_GAIN: f32 = -1.0;
 
 fn cubeb_channel_to_pa_channel(channel: ffi::cubeb_channel) -> pa_channel_position_t {
-    use cubeb_backend::ffi::*;
-    assert_ne!(channel, CHANNEL_INVALID);
-
     match channel {
-        CHANNEL_LEFT => PA_CHANNEL_POSITION_FRONT_LEFT,
-        CHANNEL_RIGHT => PA_CHANNEL_POSITION_FRONT_RIGHT,
-        CHANNEL_CENTER => PA_CHANNEL_POSITION_FRONT_CENTER,
-        CHANNEL_LS => PA_CHANNEL_POSITION_SIDE_LEFT,
-        CHANNEL_RS => PA_CHANNEL_POSITION_SIDE_RIGHT,
-        CHANNEL_RLS => PA_CHANNEL_POSITION_REAR_LEFT,
-        CHANNEL_RCENTER => PA_CHANNEL_POSITION_REAR_CENTER,
-        CHANNEL_RRS => PA_CHANNEL_POSITION_REAR_RIGHT,
-        CHANNEL_LFE => PA_CHANNEL_POSITION_LFE,
-        // Also handles CHANNEL_MONO case
-        _ => PA_CHANNEL_POSITION_MONO,
+        ffi::CHANNEL_FRONT_LEFT => PA_CHANNEL_POSITION_FRONT_LEFT,
+        ffi::CHANNEL_FRONT_RIGHT => PA_CHANNEL_POSITION_FRONT_RIGHT,
+        ffi::CHANNEL_FRONT_CENTER => PA_CHANNEL_POSITION_FRONT_CENTER,
+        ffi::CHANNEL_LOW_FREQUENCY => PA_CHANNEL_POSITION_LFE,
+        ffi::CHANNEL_BACK_LEFT => PA_CHANNEL_POSITION_REAR_LEFT,
+        ffi::CHANNEL_BACK_RIGHT => PA_CHANNEL_POSITION_REAR_RIGHT,
+        ffi::CHANNEL_FRONT_LEFT_OF_CENTER => PA_CHANNEL_POSITION_FRONT_LEFT_OF_CENTER,
+        ffi::CHANNEL_FRONT_RIGHT_OF_CENTER => PA_CHANNEL_POSITION_FRONT_RIGHT_OF_CENTER,
+        ffi::CHANNEL_BACK_CENTER => PA_CHANNEL_POSITION_REAR_CENTER,
+        ffi::CHANNEL_SIDE_LEFT => PA_CHANNEL_POSITION_SIDE_LEFT,
+        ffi::CHANNEL_SIDE_RIGHT => PA_CHANNEL_POSITION_SIDE_RIGHT,
+        ffi::CHANNEL_TOP_CENTER => PA_CHANNEL_POSITION_TOP_CENTER,
+        ffi::CHANNEL_TOP_FRONT_LEFT => PA_CHANNEL_POSITION_TOP_FRONT_LEFT,
+        ffi::CHANNEL_TOP_FRONT_CENTER => PA_CHANNEL_POSITION_TOP_FRONT_CENTER,
+        ffi::CHANNEL_TOP_FRONT_RIGHT => PA_CHANNEL_POSITION_TOP_FRONT_RIGHT,
+        ffi::CHANNEL_TOP_BACK_LEFT => PA_CHANNEL_POSITION_TOP_REAR_LEFT,
+        ffi::CHANNEL_TOP_BACK_CENTER => PA_CHANNEL_POSITION_TOP_REAR_CENTER,
+        ffi::CHANNEL_TOP_BACK_RIGHT => PA_CHANNEL_POSITION_TOP_REAR_RIGHT,
+        _ => PA_CHANNEL_POSITION_INVALID,
     }
 }
 
 fn layout_to_channel_map(layout: ChannelLayout) -> pulse::ChannelMap {
-    assert_ne!(layout, ChannelLayout::Undefined);
-
-    let order = mixer::channel_index_to_order(layout.into());
+    assert_ne!(layout, ChannelLayout::UNDEFINED);
 
     let mut cm = pulse::ChannelMap::init();
-    cm.channels = order.len() as u8;
-    for (s, d) in order.iter().zip(cm.map.iter_mut()) {
-        *d = cubeb_channel_to_pa_channel(*s);
+
+    let mut channel_map = layout.bits();
+    let mut i = 0;
+    while channel_map != 0 {
+        let channel = (channel_map & 1) << i;
+        if channel != 0 {
+            cm.map[i] = cubeb_channel_to_pa_channel(channel);
+            i += 1;
+        }
+        channel_map = channel_map >> 1;
     }
+
+    cm.channels = layout.num_channels() as _;
     cm
 }
 
 pub struct Device(ffi::cubeb_device);
 
 impl Drop for Device {
     fn drop(&mut self) {
         unsafe {
@@ -621,17 +633,17 @@ impl<'ctx> PulseStream<'ctx> {
 
         let ss = pulse::SampleSpec {
             channels: stream_params.channels() as u8,
             format: fmt.into(),
             rate: stream_params.rate(),
         };
 
         let cm: Option<pa_channel_map> = match stream_params.layout() {
-            ChannelLayout::Undefined => None,
+            ChannelLayout::UNDEFINED => None,
             _ => Some(layout_to_channel_map(stream_params.layout())),
         };
 
         let stream = pulse::Stream::new(context, stream_name.unwrap(), &ss, cm.as_ref());
 
         match stream {
             None => Err(Error::error()),
             Some(stm) => Ok(stm),

