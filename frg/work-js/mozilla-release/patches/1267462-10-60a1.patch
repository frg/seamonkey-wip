# HG changeset patch
# User Daniel Holbert <dholbert@cs.stanford.edu>
# Date 1518131844 28800
#      Thu Feb 08 15:17:24 2018 -0800
# Node ID 3a096d205f992df9cf9f71f664a063d49fef6584
# Parent  8d77170b8a70a22267f609d9b860ecf4b8bb7812
Bug 1267462 part 10: Fix flexbox code in nsFrame::ComputeSize[WithIntrinsicDimensions] to use logical axes & coords. r=mats

This patch makes a bunch of tests start passing, so it removes some 'fails' annotations.

MozReview-Commit-ID: DCxkXFnztLc

diff --git a/layout/generic/nsFrame.cpp b/layout/generic/nsFrame.cpp
--- a/layout/generic/nsFrame.cpp
+++ b/layout/generic/nsFrame.cpp
@@ -5569,51 +5569,66 @@ nsFrame::ComputeSize(gfxContext*        
       // When resolving justify/align-self below, we want to use the grid
       // container's justify/align-items value and WritingMode.
       alignCB = grandParent;
     }
   }
   bool isFlexItem = parentFrame && parentFrame->IsFlexContainerFrame() &&
     !parentFrame->HasAnyStateBits(NS_STATE_FLEX_IS_LEGACY_WEBKIT_BOX) &&
     !HasAnyStateBits(NS_FRAME_OUT_OF_FLOW);
-  bool isInlineFlexItem = false;
+  // This bool only gets set (and used) if isFlexItem is true.
+  // It indicates (for flex items) whether we're using their flex-basis for the
+  // item's own ISize property vs. for its BSize property.
+  bool usingFlexBasisForISize;
   if (isFlexItem) {
     // Flex items use their "flex-basis" property in place of their main-size
     // property (e.g. "width") for sizing purposes, *unless* they have
     // "flex-basis:auto", in which case they use their main-size property after
     // all.
     uint32_t flexDirection = GetParent()->StylePosition()->mFlexDirection;
-    isInlineFlexItem =
+    const bool flexContainerIsRowOriented =
       flexDirection == NS_STYLE_FLEX_DIRECTION_ROW ||
       flexDirection == NS_STYLE_FLEX_DIRECTION_ROW_REVERSE;
+    const bool inlineAxisSameAsParent =
+      !aWM.IsOrthogonalTo(parentFrame->GetWritingMode());
+
+    // If parent is row-oriented, its main axis (i.e. the flex-basis axis) is
+    // its own inline axis. So if it's row oriented and our own inline axis
+    // is the same as our parent's, then we'll be using flex-basis in place
+    // of our _ISize_ sizing property.
+    // Otherwise, we'll be using flex-basis for our _BSize_ sizing property
+    // (unless both conditions are false, in which case we flip back around to
+    // using our ISize sizing property).
+    usingFlexBasisForISize =
+      (flexContainerIsRowOriented == inlineAxisSameAsParent);
 
     // NOTE: The logic here should match the similar chunk for determining
     // inlineStyleCoord and blockStyleCoord in
     // nsFrame::ComputeSizeWithIntrinsicDimensions().
     const nsStyleCoord* flexBasis = &(stylePos->mFlexBasis);
     if (flexBasis->GetUnit() != eStyleUnit_Auto) {
-      if (isInlineFlexItem) {
-        inlineStyleCoord = flexBasis;
-      } else {
-        // One caveat for vertical flex items: We don't support enumerated
-        // values (e.g. "max-content") for height properties yet. So, if our
-        // computed flex-basis is an enumerated value, we'll just behave as if
-        // it were "auto", which means "use the main-size property after all"
-        // (which is "height", in this case).
-        // NOTE: Once we support intrinsic sizing keywords for "height",
-        // we should remove this check.
-        if (flexBasis->GetUnit() != eStyleUnit_Enumerated) {
-          blockStyleCoord = flexBasis;
-        }
+      // One caveat for when flex-basis is stomping on 'height': We don't
+      // support enumerated values (e.g. "max-content") for height yet (that's
+      // bug 567039). So, if our computed flex-basis is an enumerated value,
+      // we'll just behave as if it were "auto", which means "use the main-size
+      // property after all" (which is "height", in this case).  NOTE: Once we
+      // support intrinsic sizing keywords for "height", we should remove this
+      // check.
+      bool usingFlexBasisForHeight =
+        (usingFlexBasisForISize != aWM.IsVertical());
+      if (!usingFlexBasisForHeight ||
+          flexBasis->GetUnit() != eStyleUnit_Enumerated) {
+        // Override whichever coord we're overriding:
+        (usingFlexBasisForISize ? inlineStyleCoord : blockStyleCoord) =
+          flexBasis;
       }
     }
   }
 
   // Compute inline-axis size
-
   if (inlineStyleCoord->GetUnit() != eStyleUnit_Auto) {
     result.ISize(aWM) =
       ComputeISizeValue(aRenderingContext, aCBSize.ISize(aWM),
                         boxSizingAdjust.ISize(aWM), boxSizingToMarginEdgeISize,
                         *inlineStyleCoord, aFlags);
   } else if (MOZ_UNLIKELY(isGridItem) &&
              !IS_TRUE_OVERFLOW_CONTAINER(this)) {
     // 'auto' inline-size for grid-level box - fill the CB for 'stretch' /
@@ -5640,28 +5655,28 @@ nsFrame::ComputeSize(gfxContext*        
   }
 
   // Flex items ignore their min & max sizing properties in their
   // flex container's main-axis.  (Those properties get applied later in
   // the flexbox algorithm.)
   const nsStyleCoord& maxISizeCoord = stylePos->MaxISize(aWM);
   nscoord maxISize = NS_UNCONSTRAINEDSIZE;
   if (maxISizeCoord.GetUnit() != eStyleUnit_None &&
-      !(isFlexItem && isInlineFlexItem)) {
+      !(isFlexItem && usingFlexBasisForISize)) {
     maxISize =
       ComputeISizeValue(aRenderingContext, aCBSize.ISize(aWM),
                         boxSizingAdjust.ISize(aWM), boxSizingToMarginEdgeISize,
                         maxISizeCoord, aFlags);
     result.ISize(aWM) = std::min(maxISize, result.ISize(aWM));
   }
 
   const nsStyleCoord& minISizeCoord = stylePos->MinISize(aWM);
   nscoord minISize;
   if (minISizeCoord.GetUnit() != eStyleUnit_Auto &&
-      !(isFlexItem && isInlineFlexItem)) {
+      !(isFlexItem && usingFlexBasisForISize)) {
     minISize =
       ComputeISizeValue(aRenderingContext, aCBSize.ISize(aWM),
                         boxSizingAdjust.ISize(aWM), boxSizingToMarginEdgeISize,
                         minISizeCoord, aFlags);
   } else if (MOZ_UNLIKELY(aFlags & eIApplyAutoMinSize)) {
     // This implements "Implied Minimum Size of Grid Items".
     // https://drafts.csswg.org/css-grid/#min-size-auto
     minISize = std::min(maxISize, GetMinISize(aRenderingContext));
@@ -5728,28 +5743,28 @@ nsFrame::ComputeSize(gfxContext*        
       }
     }
   }
 
   const nsStyleCoord& maxBSizeCoord = stylePos->MaxBSize(aWM);
 
   if (result.BSize(aWM) != NS_UNCONSTRAINEDSIZE) {
     if (!nsLayoutUtils::IsAutoBSize(maxBSizeCoord, aCBSize.BSize(aWM)) &&
-        !(isFlexItem && !isInlineFlexItem)) {
+        !(isFlexItem && !usingFlexBasisForISize)) {
       nscoord maxBSize =
         nsLayoutUtils::ComputeBSizeValue(aCBSize.BSize(aWM),
                                          boxSizingAdjust.BSize(aWM),
                                          maxBSizeCoord);
       result.BSize(aWM) = std::min(maxBSize, result.BSize(aWM));
     }
 
     const nsStyleCoord& minBSizeCoord = stylePos->MinBSize(aWM);
 
     if (!nsLayoutUtils::IsAutoBSize(minBSizeCoord, aCBSize.BSize(aWM)) &&
-        !(isFlexItem && !isInlineFlexItem)) {
+        !(isFlexItem && !usingFlexBasisForISize)) {
       nscoord minBSize =
         nsLayoutUtils::ComputeBSizeValue(aCBSize.BSize(aWM),
                                          boxSizingAdjust.BSize(aWM),
                                          minBSizeCoord);
       result.BSize(aWM) = std::max(minBSize, result.BSize(aWM));
     }
   }
 
@@ -5800,68 +5815,78 @@ nsFrame::ComputeSizeWithIntrinsicDimensi
   const nsStyleCoord* inlineStyleCoord = &stylePos->ISize(aWM);
   const nsStyleCoord* blockStyleCoord = &stylePos->BSize(aWM);
   auto* parentFrame = GetParent();
   const bool isGridItem = parentFrame && parentFrame->IsGridContainerFrame() &&
     !HasAnyStateBits(NS_FRAME_OUT_OF_FLOW);
   const bool isFlexItem = parentFrame && parentFrame->IsFlexContainerFrame() &&
     !parentFrame->HasAnyStateBits(NS_STATE_FLEX_IS_LEGACY_WEBKIT_BOX) &&
     !HasAnyStateBits(NS_FRAME_OUT_OF_FLOW);
-  bool isInlineFlexItem = false;
+  // This bool only gets set (and used) if isFlexItem is true.
+  // It indicates (for flex items) whether we're using their flex-basis for the
+  // item's own ISize property vs. for its BSize property.
+  bool usingFlexBasisForISize;
   Maybe<nsStyleCoord> imposedMainSizeStyleCoord;
 
   // If this is a flex item, and we're measuring its cross size after flexing
   // to resolve its main size, then we need to use the resolved main size
   // that the container provides to us *instead of* the main-size coordinate
   // from our style struct. (Otherwise, we'll be using an irrelevant value in
   // the aspect-ratio calculations below.)
   if (isFlexItem) {
     uint32_t flexDirection =
       GetParent()->StylePosition()->mFlexDirection;
-    isInlineFlexItem =
+    const bool flexContainerIsRowOriented =
       flexDirection == NS_STYLE_FLEX_DIRECTION_ROW ||
       flexDirection == NS_STYLE_FLEX_DIRECTION_ROW_REVERSE;
+    const bool inlineAxisSameAsParent =
+      !aWM.IsOrthogonalTo(parentFrame->GetWritingMode());
+
+    // (See explanatory comment in similar code within ComputeSize.)
+    usingFlexBasisForISize =
+      (flexContainerIsRowOriented == inlineAxisSameAsParent);
 
     // If FlexItemMainSizeOverride frame-property is set, then that means the
     // flex container is imposing a main-size on this flex item for it to use
     // as its size in the container's main axis.
     bool didImposeMainSize;
     nscoord imposedMainSize =
       GetProperty(nsIFrame::FlexItemMainSizeOverride(), &didImposeMainSize);
     if (didImposeMainSize) {
       imposedMainSizeStyleCoord.emplace(imposedMainSize,
                                         nsStyleCoord::CoordConstructor);
-      if (isInlineFlexItem) {
+      if (usingFlexBasisForISize) {
         inlineStyleCoord = imposedMainSizeStyleCoord.ptr();
       } else {
         blockStyleCoord = imposedMainSizeStyleCoord.ptr();
       }
 
     } else {
       // Flex items use their "flex-basis" property in place of their main-size
       // property (e.g. "width") for sizing purposes, *unless* they have
       // "flex-basis:auto", in which case they use their main-size property
       // after all.
       // NOTE: The logic here should match the similar chunk for determining
       // inlineStyleCoord and blockStyleCoord in nsFrame::ComputeSize().
       const nsStyleCoord* flexBasis = &(stylePos->mFlexBasis);
       if (flexBasis->GetUnit() != eStyleUnit_Auto) {
-        if (isInlineFlexItem) {
-          inlineStyleCoord = flexBasis;
-        } else {
-          // One caveat for vertical flex items: We don't support enumerated
-          // values (e.g. "max-content") for height properties yet. So, if our
-          // computed flex-basis is an enumerated value, we'll just behave as if
-          // it were "auto", which means "use the main-size property after all"
-          // (which is "height", in this case).
-          // NOTE: Once we support intrinsic sizing keywords for "height",
-          // we should remove this check.
-          if (flexBasis->GetUnit() != eStyleUnit_Enumerated) {
-            blockStyleCoord = flexBasis;
-          }
+        // One caveat for when flex-basis is stomping on 'height': We don't
+        // support enumerated values (e.g. "max-content") for height yet
+        // (that's bug 567039). So, if our computed flex-basis is an enumerated
+        // value, we'll just behave as if it were "auto", which means "use the
+        // main-size property after all" (which is "height", in this case).
+        // NOTE: Once we support intrinsic sizing keywords for "height", we
+        // should remove this check.
+        bool usingFlexBasisForHeight =
+          (usingFlexBasisForISize != aWM.IsVertical());
+        if (!usingFlexBasisForHeight ||
+            flexBasis->GetUnit() != eStyleUnit_Enumerated) {
+          // Override whichever coord we're overriding:
+          (usingFlexBasisForISize ? inlineStyleCoord : blockStyleCoord) =
+            flexBasis;
         }
       }
     }
   }
 
   // Handle intrinsic sizes and their interaction with
   // {min-,max-,}{width,height} according to the rules in
   // http://www.w3.org/TR/CSS21/visudet.html#min-max-widths
@@ -5964,32 +5989,32 @@ nsFrame::ComputeSizeWithIntrinsicDimensi
       aFlags = ComputeSizeFlags(aFlags &
                                 ~ComputeSizeFlags::eIClampMarginBoxMinSize);
     }
   }
 
   const nsStyleCoord& maxISizeCoord = stylePos->MaxISize(aWM);
 
   if (maxISizeCoord.GetUnit() != eStyleUnit_None &&
-      !(isFlexItem && isInlineFlexItem)) {
+      !(isFlexItem && usingFlexBasisForISize)) {
     maxISize = ComputeISizeValue(aRenderingContext,
                  aCBSize.ISize(aWM), boxSizingAdjust.ISize(aWM),
                  boxSizingToMarginEdgeISize, maxISizeCoord, aFlags);
   } else {
     maxISize = nscoord_MAX;
   }
 
   // NOTE: Flex items ignore their min & max sizing properties in their
   // flex container's main-axis.  (Those properties get applied later in
   // the flexbox algorithm.)
 
   const nsStyleCoord& minISizeCoord = stylePos->MinISize(aWM);
 
   if (minISizeCoord.GetUnit() != eStyleUnit_Auto &&
-      !(isFlexItem && isInlineFlexItem)) {
+      !(isFlexItem && usingFlexBasisForISize)) {
     minISize = ComputeISizeValue(aRenderingContext,
                  aCBSize.ISize(aWM), boxSizingAdjust.ISize(aWM),
                  boxSizingToMarginEdgeISize, minISizeCoord, aFlags);
   } else {
     // Treat "min-width: auto" as 0.
     // NOTE: Technically, "auto" is supposed to behave like "min-content" on
     // flex items. However, we don't need to worry about that here, because
     // flex items' min-sizes are intentionally ignored until the flex
@@ -6033,27 +6058,27 @@ nsFrame::ComputeSizeWithIntrinsicDimensi
       aFlags = ComputeSizeFlags(aFlags &
                                 ~ComputeSizeFlags::eBClampMarginBoxMinSize);
     }
   }
 
   const nsStyleCoord& maxBSizeCoord = stylePos->MaxBSize(aWM);
 
   if (!nsLayoutUtils::IsAutoBSize(maxBSizeCoord, aCBSize.BSize(aWM)) &&
-      !(isFlexItem && !isInlineFlexItem)) {
+      !(isFlexItem && !usingFlexBasisForISize)) {
     maxBSize = nsLayoutUtils::ComputeBSizeValue(aCBSize.BSize(aWM),
                   boxSizingAdjust.BSize(aWM), maxBSizeCoord);
   } else {
     maxBSize = nscoord_MAX;
   }
 
   const nsStyleCoord& minBSizeCoord = stylePos->MinBSize(aWM);
 
   if (!nsLayoutUtils::IsAutoBSize(minBSizeCoord, aCBSize.BSize(aWM)) &&
-      !(isFlexItem && !isInlineFlexItem)) {
+      !(isFlexItem && !usingFlexBasisForISize)) {
     minBSize = nsLayoutUtils::ComputeBSizeValue(aCBSize.BSize(aWM),
                   boxSizingAdjust.BSize(aWM), minBSizeCoord);
   } else {
     minBSize = 0;
   }
 
   NS_ASSERTION(aCBSize.ISize(aWM) != NS_UNCONSTRAINEDSIZE,
                "Our containing block must not have unconstrained inline-size!");
diff --git a/layout/reftests/w3c-css/submitted/flexbox/reftest.list b/layout/reftests/w3c-css/submitted/flexbox/reftest.list
--- a/layout/reftests/w3c-css/submitted/flexbox/reftest.list
+++ b/layout/reftests/w3c-css/submitted/flexbox/reftest.list
@@ -56,25 +56,25 @@ fuzzy-if(Android,158,32) == flexbox-alig
 == flexbox-baseline-multi-line-horiz-004.html flexbox-baseline-multi-line-horiz-004-ref.html
 == flexbox-baseline-multi-line-vert-001.html flexbox-baseline-multi-line-vert-001-ref.html
 == flexbox-baseline-multi-line-vert-002.html flexbox-baseline-multi-line-vert-002-ref.html
 == flexbox-baseline-single-item-001a.html flexbox-baseline-single-item-001-ref.html
 == flexbox-baseline-single-item-001b.html flexbox-baseline-single-item-001-ref.html
 
 # Basic tests with with blocks as flex items
 == flexbox-basic-block-horiz-001.xhtml flexbox-basic-block-horiz-001-ref.xhtml
-fails == flexbox-basic-block-horiz-001v.xhtml flexbox-basic-block-horiz-001-ref.xhtml # bug 1267462
+== flexbox-basic-block-horiz-001v.xhtml flexbox-basic-block-horiz-001-ref.xhtml
 == flexbox-basic-block-vert-001.xhtml flexbox-basic-block-vert-001-ref.xhtml
-fails == flexbox-basic-block-vert-001v.xhtml flexbox-basic-block-vert-001-ref.xhtml # bug 1267462
+== flexbox-basic-block-vert-001v.xhtml flexbox-basic-block-vert-001-ref.xhtml
 
 # Tests for basic handling of <canvas>/<img>/etc as a flex item
 == flexbox-basic-canvas-horiz-001.xhtml   flexbox-basic-canvas-horiz-001-ref.xhtml
-fails == flexbox-basic-canvas-horiz-001v.xhtml  flexbox-basic-canvas-horiz-001-ref.xhtml # bug 1267462
+== flexbox-basic-canvas-horiz-001v.xhtml  flexbox-basic-canvas-horiz-001-ref.xhtml
 == flexbox-basic-canvas-vert-001.xhtml    flexbox-basic-canvas-vert-001-ref.xhtml
-fails == flexbox-basic-canvas-vert-001v.xhtml   flexbox-basic-canvas-vert-001-ref.xhtml # bug 1267462
+== flexbox-basic-canvas-vert-001v.xhtml   flexbox-basic-canvas-vert-001-ref.xhtml
 == flexbox-basic-fieldset-horiz-001.xhtml flexbox-basic-fieldset-horiz-001-ref.xhtml
 == flexbox-basic-fieldset-vert-001.xhtml  flexbox-basic-fieldset-vert-001-ref.xhtml
 == flexbox-basic-iframe-horiz-001.xhtml   flexbox-basic-iframe-horiz-001-ref.xhtml
 == flexbox-basic-iframe-vert-001.xhtml    flexbox-basic-iframe-vert-001-ref.xhtml
 == flexbox-basic-img-horiz-001.xhtml      flexbox-basic-img-horiz-001-ref.xhtml
 == flexbox-basic-img-vert-001.xhtml       flexbox-basic-img-vert-001-ref.xhtml
 == flexbox-basic-textarea-horiz-001.xhtml flexbox-basic-textarea-horiz-001-ref.xhtml
 == flexbox-basic-textarea-vert-001.xhtml  flexbox-basic-textarea-vert-001-ref.xhtml
@@ -106,27 +106,27 @@ fails == flexbox-basic-canvas-vert-001v.
 == flexbox-flex-wrap-horiz-002.html flexbox-flex-wrap-horiz-002-ref.html
 == flexbox-flex-wrap-vert-001.html  flexbox-flex-wrap-vert-001-ref.html
 == flexbox-flex-wrap-vert-002.html  flexbox-flex-wrap-vert-002-ref.html
 
 # Tests for intrinsic ratio interactions on flex items.
 # (Note that tests 001 and 002 share a reference case; they render the same,
 # because they don't do any direction-specific stretching/flexing.)
 == flexbox-intrinsic-ratio-001.html flexbox-intrinsic-ratio-001-ref.html
-fails == flexbox-intrinsic-ratio-001v.html flexbox-intrinsic-ratio-001-ref.html # bug 1267462
+== flexbox-intrinsic-ratio-001v.html flexbox-intrinsic-ratio-001-ref.html
 == flexbox-intrinsic-ratio-002.html flexbox-intrinsic-ratio-001-ref.html
-fails == flexbox-intrinsic-ratio-002v.html flexbox-intrinsic-ratio-001-ref.html # bug 1267462
+== flexbox-intrinsic-ratio-002v.html flexbox-intrinsic-ratio-001-ref.html
 == flexbox-intrinsic-ratio-003.html flexbox-intrinsic-ratio-003-ref.html
-fails == flexbox-intrinsic-ratio-003v.html flexbox-intrinsic-ratio-003-ref.html # bug 1267462
+== flexbox-intrinsic-ratio-003v.html flexbox-intrinsic-ratio-003-ref.html
 == flexbox-intrinsic-ratio-004.html flexbox-intrinsic-ratio-004-ref.html
-fails == flexbox-intrinsic-ratio-004v.html flexbox-intrinsic-ratio-004-ref.html # bug 1267462
+== flexbox-intrinsic-ratio-004v.html flexbox-intrinsic-ratio-004-ref.html
 == flexbox-intrinsic-ratio-005.html flexbox-intrinsic-ratio-005-ref.html
-fails == flexbox-intrinsic-ratio-005v.html flexbox-intrinsic-ratio-005-ref.html # bug 1267462
+== flexbox-intrinsic-ratio-005v.html flexbox-intrinsic-ratio-005-ref.html
 == flexbox-intrinsic-ratio-006.html flexbox-intrinsic-ratio-006-ref.html
-fails == flexbox-intrinsic-ratio-006v.html flexbox-intrinsic-ratio-006-ref.html # bug 1267462
+== flexbox-intrinsic-ratio-006v.html flexbox-intrinsic-ratio-006-ref.html
 
 # Tests for flex items as (pseudo) stacking contexts
 == flexbox-items-as-stacking-contexts-001.xhtml flexbox-items-as-stacking-contexts-001-ref.xhtml
 == flexbox-items-as-stacking-contexts-002.html flexbox-items-as-stacking-contexts-002-ref.html
 == flexbox-items-as-stacking-contexts-003.html flexbox-items-as-stacking-contexts-003-ref.html
 
 # Tests for main-axis alignment (jusify-content property)
 == flexbox-justify-content-horiz-001a.xhtml flexbox-justify-content-horiz-001-ref.xhtml
@@ -146,19 +146,19 @@ fails == flexbox-intrinsic-ratio-006v.ht
 == flexbox-margin-auto-horiz-001.xhtml     flexbox-margin-auto-horiz-001-ref.xhtml
 == flexbox-margin-auto-horiz-002.xhtml     flexbox-margin-auto-horiz-002-ref.xhtml
 == flexbox-mbp-horiz-001.xhtml             flexbox-mbp-horiz-001-ref.xhtml
 == flexbox-mbp-horiz-001-reverse.xhtml     flexbox-mbp-horiz-001-reverse-ref.xhtml
 == flexbox-mbp-horiz-001-rtl.xhtml         flexbox-mbp-horiz-001-reverse-ref.xhtml
 == flexbox-mbp-horiz-001-rtl-reverse.xhtml flexbox-mbp-horiz-001-ref.xhtml
 == flexbox-mbp-horiz-002a.xhtml            flexbox-mbp-horiz-002-ref.xhtml
 == flexbox-mbp-horiz-002b.xhtml            flexbox-mbp-horiz-002-ref.xhtml
-asserts(16) fails == flexbox-mbp-horiz-002v.xhtml      flexbox-mbp-horiz-002-ref.xhtml # bug 1267462
+== flexbox-mbp-horiz-002v.xhtml            flexbox-mbp-horiz-002-ref.xhtml
 == flexbox-mbp-horiz-003.xhtml             flexbox-mbp-horiz-003-ref.xhtml
-asserts(32) fails == flexbox-mbp-horiz-003v.xhtml      flexbox-mbp-horiz-003-ref.xhtml # bug 1267462
+== flexbox-mbp-horiz-003v.xhtml            flexbox-mbp-horiz-003-ref.xhtml
 == flexbox-mbp-horiz-003-reverse.xhtml     flexbox-mbp-horiz-003-reverse-ref.xhtml
 == flexbox-mbp-horiz-004.xhtml             flexbox-mbp-horiz-004-ref.xhtml
 
 # Tests for min-height:auto / min-width:auto on flex items
 == flexbox-min-height-auto-001.html flexbox-min-height-auto-001-ref.html
 == flexbox-min-height-auto-002a.html flexbox-min-height-auto-002-ref.html
 fails == flexbox-min-height-auto-002b.html flexbox-min-height-auto-002-ref.html # bug 1055354
 == flexbox-min-height-auto-002c.html flexbox-min-height-auto-002-ref.html
