# HG changeset patch
# User Andrew Osmond <aosmond@mozilla.com>
# Date 1560424363 14400
#      Thu Jun 13 07:12:43 2019 -0400
# Node ID 00bc27f8de034bf74d90901bd6628e805dcd87ad
# Parent  db5e799c3bba030e18e63a3197300277a809f6c7
Bug 1551844 - Implement NEON variations of QCMS ICCv2 transforms. r=lsalzman

Differential Revision: https://phabricator.services.mozilla.com/D34582

diff --git a/gfx/qcms/moz.build b/gfx/qcms/moz.build
--- a/gfx/qcms/moz.build
+++ b/gfx/qcms/moz.build
@@ -19,30 +19,37 @@ SOURCES += [
 
 FINAL_LIBRARY = 'xul'
 
 if CONFIG['CC_TYPE'] in ('clang', 'gcc'):
     CFLAGS += ['-Wno-missing-field-initializers']
 
 use_sse1 = False
 use_sse2 = False
+use_neon = False
 use_altivec = False
 if CONFIG['INTEL_ARCHITECTURE']:
     use_sse2 = True
     if CONFIG['CC_TYPE'] in ('msvc', 'clang-cl'):
         if CONFIG['OS_ARCH'] != 'WINNT' or CONFIG['CPU_ARCH'] != 'x86_64':
             use_sse1 = True
     else:
         use_sse1 = True
+elif CONFIG['CPU_ARCH'] == 'aarch64' or CONFIG['BUILD_ARM_NEON']:
+    use_neon = True
 elif CONFIG['HAVE_ALTIVEC']:
     use_altivec = True
 
 if use_sse1:
     SOURCES += ['transform-sse1.cpp']
     SOURCES['transform-sse1.cpp'].flags += CONFIG['SSE_FLAGS']
 
 if use_sse2:
     SOURCES += ['transform-sse2.cpp']
     SOURCES['transform-sse2.cpp'].flags += CONFIG['SSE2_FLAGS']
 
+if use_neon:
+    SOURCES += ['transform-neon.cpp']
+    SOURCES['transform-neon.cpp'].flags += CONFIG['NEON_FLAGS']
+
 if use_altivec:
     SOURCES += ['transform-altivec.cpp']
     SOURCES['transform-altivec.cpp'].flags += ['-maltivec']
diff --git a/gfx/qcms/qcms.h b/gfx/qcms/qcms.h
--- a/gfx/qcms/qcms.h
+++ b/gfx/qcms/qcms.h
@@ -166,14 +166,15 @@ qcms_transform* qcms_transform_create(
 		qcms_profile* out, qcms_data_type out_type,
 		qcms_intent intent);
 
 void qcms_transform_release(qcms_transform *);
 
 void qcms_transform_data(qcms_transform *transform, const void *src, void *dest, size_t length);
 
 void qcms_enable_iccv4();
+void qcms_enable_neon();
 
 #ifdef  __cplusplus
 }
 #endif
 
 #endif
diff --git a/gfx/qcms/qcmsint.h b/gfx/qcms/qcmsint.h
--- a/gfx/qcms/qcmsint.h
+++ b/gfx/qcms/qcmsint.h
@@ -297,17 +297,31 @@ void qcms_transform_data_rgba_out_lut_al
                                               const unsigned char *src,
                                               unsigned char *dest,
                                               size_t length);
 void qcms_transform_data_bgra_out_lut_altivec(const qcms_transform *transform,
                                               const unsigned char *src,
                                               unsigned char *dest,
                                               size_t length);
 
+void qcms_transform_data_rgb_out_lut_neon(const qcms_transform *transform,
+                                          const unsigned char *src,
+                                          unsigned char *dest,
+                                          size_t length);
+void qcms_transform_data_rgba_out_lut_neon(const qcms_transform *transform,
+                                           const unsigned char *src,
+                                           unsigned char *dest,
+                                           size_t length);
+void qcms_transform_data_bgra_out_lut_neon(const qcms_transform *transform,
+                                           const unsigned char *src,
+                                           unsigned char *dest,
+                                           size_t length);
+
 extern bool qcms_supports_iccv4;
+extern bool qcms_supports_neon;
 
 #ifdef _MSC_VER
 
 long __cdecl _InterlockedIncrement(long volatile *);
 long __cdecl _InterlockedDecrement(long volatile *);
 #pragma intrinsic(_InterlockedIncrement)
 #pragma intrinsic(_InterlockedDecrement)
 
diff --git a/gfx/qcms/transform-neon.cpp b/gfx/qcms/transform-neon.cpp
new file mode 100644
--- /dev/null
+++ b/gfx/qcms/transform-neon.cpp
@@ -0,0 +1,138 @@
+#include <arm_neon.h>
+
+#include "qcmsint.h"
+
+#define FLOATSCALE  (float)(PRECACHE_OUTPUT_SIZE)
+#define CLAMPMAXVAL ( ((float) (PRECACHE_OUTPUT_SIZE - 1)) / PRECACHE_OUTPUT_SIZE )
+static const ALIGN float floatScale = FLOATSCALE;
+static const ALIGN float clampMaxValue = CLAMPMAXVAL;
+
+template <size_t kRIndex, size_t kGIndex, size_t kBIndex, size_t kAIndex = NO_A_INDEX>
+static void qcms_transform_data_template_lut_neon(const qcms_transform *transform,
+                                                  const unsigned char *src,
+                                                  unsigned char *dest,
+                                                  size_t length)
+{
+    unsigned int i;
+    const float (*mat)[4] = transform->matrix;
+
+    /* deref *transform now to avoid it in loop */
+    const float *igtbl_r = transform->input_gamma_table_r;
+    const float *igtbl_g = transform->input_gamma_table_g;
+    const float *igtbl_b = transform->input_gamma_table_b;
+
+    /* deref *transform now to avoid it in loop */
+    const uint8_t *otdata_r = &transform->output_table_r->data[0];
+    const uint8_t *otdata_g = &transform->output_table_g->data[0];
+    const uint8_t *otdata_b = &transform->output_table_b->data[0];
+
+    /* input matrix values never change */
+    const float32x4_t mat0  = vld1q_f32(mat[0]);
+    const float32x4_t mat1  = vld1q_f32(mat[1]);
+    const float32x4_t mat2  = vld1q_f32(mat[2]);
+
+    /* these values don't change, either */
+    const float32x4_t max   = vld1q_dup_f32(&clampMaxValue);
+    const float32x4_t min   = { 0.0f, 0.0f, 0.0f, 0.0f };
+    const float32x4_t scale = vld1q_dup_f32(&floatScale);
+    const unsigned int components = A_INDEX_COMPONENTS(kAIndex);
+
+    /* working variables */
+    float32x4_t vec_r, vec_g, vec_b;
+    int32x4_t result;
+    unsigned char alpha;
+
+    /* CYA */
+    if (!length)
+        return;
+
+    /* one pixel is handled outside of the loop */
+    length--;
+
+    /* setup for transforming 1st pixel */
+    vec_r = vld1q_dup_f32(&igtbl_r[src[kRIndex]]);
+    vec_g = vld1q_dup_f32(&igtbl_g[src[kGIndex]]);
+    vec_b = vld1q_dup_f32(&igtbl_b[src[kBIndex]]);
+    if (kAIndex != NO_A_INDEX) {
+        alpha = src[kAIndex];
+    }
+    src += components;
+
+    /* transform all but final pixel */
+
+    for (i=0; i<length; i++)
+    {
+        /* gamma * matrix */
+        vec_r = vmulq_f32(vec_r, mat0);
+        vec_g = vmulq_f32(vec_g, mat1);
+        vec_b = vmulq_f32(vec_b, mat2);
+
+        /* store alpha for this pixel; load alpha for next */
+        if (kAIndex != NO_A_INDEX) {
+            dest[kAIndex] = alpha;
+            alpha = src[kAIndex];
+        }
+
+        /* crunch, crunch, crunch */
+        vec_r  = vaddq_f32(vec_r, vaddq_f32(vec_g, vec_b));
+        vec_r  = vmaxq_f32(min, vec_r);
+        vec_r  = vminq_f32(max, vec_r);
+        result = vcvtq_s32_f32(vmulq_f32(vec_r, scale));
+
+        /* use calc'd indices to output RGB values */
+        dest[kRIndex] = otdata_r[vgetq_lane_s32(result, 0)];
+        dest[kGIndex] = otdata_g[vgetq_lane_s32(result, 1)];
+        dest[kBIndex] = otdata_b[vgetq_lane_s32(result, 2)];
+
+        /* load for next loop while store completes */
+        vec_r = vld1q_dup_f32(&igtbl_r[src[kRIndex]]);
+        vec_g = vld1q_dup_f32(&igtbl_g[src[kGIndex]]);
+        vec_b = vld1q_dup_f32(&igtbl_b[src[kBIndex]]);
+
+        dest += components;
+        src += components;
+    }
+
+    /* handle final (maybe only) pixel */
+
+    vec_r = vmulq_f32(vec_r, mat0);
+    vec_g = vmulq_f32(vec_g, mat1);
+    vec_b = vmulq_f32(vec_b, mat2);
+
+    if (kAIndex != NO_A_INDEX) {
+        dest[kAIndex] = alpha;
+    }
+
+    vec_r  = vaddq_f32(vec_r, vaddq_f32(vec_g, vec_b));
+    vec_r  = vmaxq_f32(min, vec_r);
+    vec_r  = vminq_f32(max, vec_r);
+    result = vcvtq_s32_f32(vmulq_f32(vec_r, scale));
+
+    dest[kRIndex] = otdata_r[vgetq_lane_s32(result, 0)];
+    dest[kGIndex] = otdata_g[vgetq_lane_s32(result, 1)];
+    dest[kBIndex] = otdata_b[vgetq_lane_s32(result, 2)];
+}
+
+void qcms_transform_data_rgb_out_lut_neon(const qcms_transform *transform,
+                                          const unsigned char *src,
+                                          unsigned char *dest,
+                                          size_t length)
+{
+  qcms_transform_data_template_lut_neon<RGBA_R_INDEX, RGBA_G_INDEX, RGBA_B_INDEX>(transform, src, dest, length);
+}
+
+void qcms_transform_data_rgba_out_lut_neon(const qcms_transform *transform,
+                                           const unsigned char *src,
+                                           unsigned char *dest,
+                                           size_t length)
+{
+  qcms_transform_data_template_lut_neon<RGBA_R_INDEX, RGBA_G_INDEX, RGBA_B_INDEX, RGBA_A_INDEX>(transform, src, dest, length);
+}
+
+void qcms_transform_data_bgra_out_lut_neon(const qcms_transform *transform,
+                                           const unsigned char *src,
+                                           unsigned char *dest,
+                                           size_t length)
+{
+  qcms_transform_data_template_lut_neon<BGRA_R_INDEX, BGRA_G_INDEX, BGRA_B_INDEX, BGRA_A_INDEX>(transform, src, dest, length);
+}
diff --git a/gfx/qcms/transform.cpp b/gfx/qcms/transform.cpp
--- a/gfx/qcms/transform.cpp
+++ b/gfx/qcms/transform.cpp
@@ -1227,16 +1227,27 @@ qcms_transform* qcms_transform_create(
 			    } else if (in_type == QCMS_DATA_RGBA_8) {
 				    transform->transform_fn = qcms_transform_data_rgba_out_lut_sse1;
 			    } else if (in_type == QCMS_DATA_BGRA_8) {
 				    transform->transform_fn = qcms_transform_data_bgra_out_lut_sse1;
 			    }
 #endif
 		    } else
 #endif
+#if defined(__arm__) || defined(__aarch64__)
+                    if (qcms_supports_neon) {
+			    if (in_type == QCMS_DATA_RGB_8) {
+				    transform->transform_fn = qcms_transform_data_rgb_out_lut_neon;
+			    } else if (in_type == QCMS_DATA_RGBA_8) {
+				    transform->transform_fn = qcms_transform_data_rgba_out_lut_neon;
+			    } else if (in_type == QCMS_DATA_BGRA_8) {
+				    transform->transform_fn = qcms_transform_data_bgra_out_lut_neon;
+			    }
+                    } else
+#endif
 #if (defined(__POWERPC__) || defined(__powerpc__) && !defined(__NO_FPRS__))
 		    if (have_altivec()) {
 			    if (in_type == QCMS_DATA_RGB_8) {
 				    transform->transform_fn = qcms_transform_data_rgb_out_lut_altivec;
 			    } else if (in_type == QCMS_DATA_RGBA_8) {
 				    transform->transform_fn = qcms_transform_data_rgba_out_lut_altivec;
 			    } else if (in_type == QCMS_DATA_BGRA_8) {
 				    transform->transform_fn = qcms_transform_data_bgra_out_lut_altivec;
@@ -1362,8 +1373,18 @@ void qcms_transform_data(qcms_transform 
 	transform->transform_fn(transform, (const unsigned char*)src, (unsigned char*)dest, length);
 }
 
 bool qcms_supports_iccv4;
 void qcms_enable_iccv4()
 {
 	qcms_supports_iccv4 = true;
 }
+
+#if defined(__arm__) || defined(__aarch64__)
+bool qcms_supports_neon;
+#endif
+void qcms_enable_neon()
+{
+#if defined(__arm__) || defined(__aarch64__)
+	qcms_supports_neon = true;
+#endif
+}
diff --git a/gfx/thebes/gfxPlatform.cpp b/gfx/thebes/gfxPlatform.cpp
--- a/gfx/thebes/gfxPlatform.cpp
+++ b/gfx/thebes/gfxPlatform.cpp
@@ -25,16 +25,18 @@
 #include "gfxPrefs.h"
 #include "gfxEnv.h"
 #include "gfxTextRun.h"
 #include "gfxUserFontSet.h"
 #include "gfxConfig.h"
 #include "MediaPrefs.h"
 #include "VRThread.h"
 
+#include "mozilla/arm.h"
+
 #ifdef XP_WIN
 #include <process.h>
 #define getpid _getpid
 #else
 #include <unistd.h>
 #endif
 
 #include "nsXULAppAPI.h"
@@ -1917,16 +1919,21 @@ gfxPlatform::GetCMSMode()
         if (mode >= 0 && mode < eCMSMode_AllCount) {
             gCMSMode = static_cast<eCMSMode>(mode);
         }
 
         bool enableV4 = gfxPrefs::CMSEnableV4();
         if (enableV4) {
             qcms_enable_iccv4();
         }
+#ifdef MOZILLA_MAY_SUPPORT_NEON
+    if (mozilla::supports_neon()) {
+      qcms_enable_neon();
+    }
+#endif
         gCMSInitialized = true;
     }
     return gCMSMode;
 }
 
 int
 gfxPlatform::GetRenderingIntent()
 {
diff --git a/image/test/reftest/pngsuite-ancillary/reftest.list b/image/test/reftest/pngsuite-ancillary/reftest.list
--- a/image/test/reftest/pngsuite-ancillary/reftest.list
+++ b/image/test/reftest/pngsuite-ancillary/reftest.list
@@ -1,16 +1,16 @@
 # PngSuite - Ancillary chunks
 
 # cHRM chunks
 #
 # ccwn2c08 - gamma 1.0000 chunk, chroma chunk w:0.3127,0.3290 r:0.64,0.33 g:0.30,0.60 b:0.15,0.06
-fails-if(prefs.getIntPref("gfx.color_management.mode")!=2) fuzzy-if(winWidget,8,569) == ccwn2c08.png ccwn2c08.html
+fails-if(prefs.getIntPref("gfx.color_management.mode")!=2) fuzzy-if(winWidget,8,569) fuzzy-if(Android,1,6) == ccwn2c08.png ccwn2c08.html
 # ccwn3p08 - gamma 1.0000 chunk, chroma chunk w:0.3127,0.3290 r:0.64,0.33 g:0.30,0.60 b:0.15,0.06
-fails-if(prefs.getIntPref("gfx.color_management.mode")!=2) fuzzy-if(winWidget,8,577) == ccwn3p08.png ccwn3p08.html
+fails-if(prefs.getIntPref("gfx.color_management.mode")!=2) fuzzy-if(winWidget,8,577) fuzzy-if(Android,1,19) == ccwn3p08.png ccwn3p08.html
 
 # pHYs chunks
 #
 # PngSuite implies these first 3 should end up as 32x32 bitmaps, but
 # per discussion in bug 408622 that's not actually true.
 #
 # cdfn2c08 - physical pixel dimensions, 8x32 flat pixels
 == cdfn2c08.png cdfn2c08.html
