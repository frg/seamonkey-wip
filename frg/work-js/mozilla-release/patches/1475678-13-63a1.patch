# HG changeset patch
# User Andre Bargull <andre.bargull@gmail.com>
# Date 1531516039 25200
#      Fri Jul 13 14:07:19 2018 -0700
# Node ID 1d65a2e8d3828bd4edd8e9e1b3a6a4df8d486d6c
# Parent  e064d391ad242d85ca5ed537f80a03d20c8bfc58
Bug 1475678 - Part 13: Use the Call() helper from Interpreter.h to reduce code duplication. r=arai

diff --git a/js/src/builtin/Promise.cpp b/js/src/builtin/Promise.cpp
--- a/js/src/builtin/Promise.cpp
+++ b/js/src/builtin/Promise.cpp
@@ -1515,19 +1515,17 @@ PromiseReactionJob(JSContext* cx, unsign
 
             handlerResult = ObjectValue(*resultObj);
         }
     } else {
         MOZ_ASSERT(handlerVal.isObject());
         MOZ_ASSERT(IsCallable(handlerVal));
 
         // Step 6.
-        FixedInvokeArgs<1> args2(cx);
-        args2[0].set(argument);
-        if (!Call(cx, handlerVal, UndefinedHandleValue, args2, &handlerResult)) {
+        if (!Call(cx, handlerVal, UndefinedHandleValue, argument, &handlerResult)) {
             resolutionMode = RejectMode;
             if (!MaybeGetAndClearException(cx, &handlerResult))
                 return false;
         }
     }
 
     // Steps 7-9.
     uint32_t hookSlot = resolutionMode == RejectMode
@@ -1586,21 +1584,18 @@ PromiseResolveThenableJob(JSContext* cx,
     RootedValue rval(cx);
     if (Call(cx, then, thenable, args2, &rval))
         return true;
 
     // Steps 3-4.
     if (!MaybeGetAndClearException(cx, &rval))
         return false;
 
-    FixedInvokeArgs<1> rejectArgs(cx);
-    rejectArgs[0].set(rval);
-
     RootedValue rejectVal(cx, ObjectValue(*rejectFn));
-    return Call(cx, rejectVal, UndefinedHandleValue, rejectArgs, &rval);
+    return Call(cx, rejectVal, UndefinedHandleValue, rval, &rval);
 }
 
 static MOZ_MUST_USE bool
 OriginalPromiseThenWithoutSettleHandlers(JSContext* cx, Handle<PromiseObject*> promise,
                                          Handle<PromiseObject*> promiseToResolve);
 
 /**
  * Specialization of PromiseResolveThenableJob when the `thenable` is a
@@ -2014,36 +2009,31 @@ PromiseObject::create(JSContext* cx, Han
     } else {
         promise->initFixedSlot(PromiseSlot_RejectFunction, ObjectValue(*rejectFn));
     }
 
     // Step 9.
     bool success;
     {
         FixedInvokeArgs<2> args(cx);
-
         args[0].setObject(*resolveFn);
         args[1].setObject(*rejectFn);
 
         RootedValue calleeOrRval(cx, ObjectValue(*executor));
         success = Call(cx, calleeOrRval, UndefinedHandleValue, args, &calleeOrRval);
     }
 
     // Step 10.
     if (!success) {
         RootedValue exceptionVal(cx);
         if (!MaybeGetAndClearException(cx, &exceptionVal))
             return nullptr;
 
-        FixedInvokeArgs<1> args(cx);
-
-        args[0].set(exceptionVal);
-
         RootedValue calleeOrRval(cx, ObjectValue(*rejectFn));
-        if (!Call(cx, calleeOrRval, UndefinedHandleValue, args, &calleeOrRval))
+        if (!Call(cx, calleeOrRval, UndefinedHandleValue, exceptionVal, &calleeOrRval))
             return nullptr;
     }
 
     // Let the Debugger know about this Promise.
     Debugger::onNewPromise(cx, promise);
 
     // Step 11.
     return promise;
@@ -2273,19 +2263,17 @@ RunResolutionFunction(JSContext *cx, Han
     // subclass constructor passes null/undefined to `super()`.)
     // There are also reactions where the Promise itself is missing. For
     // those, there's nothing left to do here.
     assertSameCompartment(cx, resolutionFun);
     assertSameCompartment(cx, result);
     assertSameCompartment(cx, promiseObj);
     if (resolutionFun) {
         RootedValue calleeOrRval(cx, ObjectValue(*resolutionFun));
-        FixedInvokeArgs<1> resolveArgs(cx);
-        resolveArgs[0].set(result);
-        return Call(cx, calleeOrRval, UndefinedHandleValue, resolveArgs, &calleeOrRval);
+        return Call(cx, calleeOrRval, UndefinedHandleValue, result, &calleeOrRval);
     }
 
     if (!promiseObj)
         return true;
 
     Handle<PromiseObject*> promise = promiseObj.as<PromiseObject>();
     if (promise->state() != JS::PromiseState::Pending)
         return true;
@@ -3373,21 +3361,18 @@ js::AsyncFromSyncIteratorMethod(JSContex
             args.rval().setObject(*resultPromise);
             return true;
         }
     }
 
     // 11.1.3.2.1 steps 5-6 (partially).
     // 11.1.3.2.2, 11.1.3.2.3 steps 8-9.
     RootedValue iterVal(cx, ObjectValue(*iter));
-    FixedInvokeArgs<1> args2(cx);
-    args2[0].set(args.get(0));
-
     RootedValue resultVal(cx);
-    if (!js::Call(cx, func, iterVal, args2, &resultVal))
+    if (!Call(cx, func, iterVal, args.get(0), &resultVal))
         return AbruptRejectPromise(cx, args, resultPromise, nullptr);
 
     // 11.1.3.2.1 steps 5-6 (partially).
     // 11.1.3.2.2, 11.1.3.2.3 steps 10.
     if (!resultVal.isObject()) {
         CheckIsObjectKind kind;
         switch (completionKind) {
           case CompletionKind::Normal:
@@ -4115,41 +4100,35 @@ PromiseObject::resolve(JSContext* cx, Ha
     RootedValue funVal(cx, ObjectValue(*resolveFun));
 
     // For xray'd Promises, the resolve fun may have been created in another
     // compartment. For the call below to work in that case, wrap the
     // function into the current compartment.
     if (!cx->compartment()->wrap(cx, &funVal))
         return false;
 
-    FixedInvokeArgs<1> args(cx);
-    args[0].set(resolutionValue);
-
     RootedValue dummy(cx);
-    return Call(cx, funVal, UndefinedHandleValue, args, &dummy);
+    return Call(cx, funVal, UndefinedHandleValue, resolutionValue, &dummy);
 }
 
 /* static */ bool
 PromiseObject::reject(JSContext* cx, Handle<PromiseObject*> promise, HandleValue rejectionValue)
 {
     MOZ_ASSERT(!PromiseHasAnyFlag(*promise, PROMISE_FLAG_ASYNC));
     if (promise->state() != JS::PromiseState::Pending)
         return true;
 
     if (PromiseHasAnyFlag(*promise, PROMISE_FLAG_DEFAULT_RESOLVING_FUNCTIONS))
         return ResolvePromise(cx, promise, rejectionValue, JS::PromiseState::Rejected);
 
     RootedValue funVal(cx, promise->getFixedSlot(PromiseSlot_RejectFunction));
     MOZ_ASSERT(IsCallable(funVal));
 
-    FixedInvokeArgs<1> args(cx);
-    args[0].set(rejectionValue);
-
     RootedValue dummy(cx);
-    return Call(cx, funVal, UndefinedHandleValue, args, &dummy);
+    return Call(cx, funVal, UndefinedHandleValue, rejectionValue, &dummy);
 }
 
 /* static */ void
 PromiseObject::onSettled(JSContext* cx, Handle<PromiseObject*> promise)
 {
     PromiseDebugInfo::setResolutionInfo(cx, promise);
 
     if (promise->state() == JS::PromiseState::Rejected && promise->isUnhandled())
