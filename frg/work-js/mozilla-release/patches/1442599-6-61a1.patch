# HG changeset patch
# User Andre Bargull <andre.bargull@gmail.com>
# Date 1521110591 25200
# Node ID 1ff23ca2f9fd605abd9a427604880dfbdc6770e9
# Parent  37eaf777fbb714594c62f0acca1904e497cf1649
Bug 1442599 - Part 6: Remove macros from jstypes.h when unused or replacements exist. r=jorendorff

diff --git a/js/src/gc/Heap.h b/js/src/gc/Heap.h
--- a/js/src/gc/Heap.h
+++ b/js/src/gc/Heap.h
@@ -573,17 +573,17 @@ struct ChunkInfo
  * few to usefully allocate.
  *
  * To actually compute the number of arenas we can allocate in a chunk, we
  * divide the amount of available space less the header info (not including
  * the mark bitmap which is distributed into the arena size) by the size of
  * the arena (with the mark bitmap bytes it uses).
  */
 const size_t BytesPerArenaWithHeader = ArenaSize + ArenaBitmapBytes;
-const size_t ChunkDecommitBitmapBytes = ChunkSize / ArenaSize / JS_BITS_PER_BYTE;
+const size_t ChunkDecommitBitmapBytes = ChunkSize / ArenaSize / CHAR_BIT;
 const size_t ChunkBytesAvailable = ChunkSize - sizeof(ChunkTrailer) - sizeof(ChunkInfo) - ChunkDecommitBitmapBytes;
 const size_t ArenasPerChunk = ChunkBytesAvailable / BytesPerArenaWithHeader;
 
 #ifdef JS_GC_SMALL_CHUNK_SIZE
 static_assert(ArenasPerChunk == 62, "Do not accidentally change our heap's density.");
 #else
 static_assert(ArenasPerChunk == 252, "Do not accidentally change our heap's density.");
 #endif
diff --git a/js/src/jstypes.h b/js/src/jstypes.h
--- a/js/src/jstypes.h
+++ b/js/src/jstypes.h
@@ -27,43 +27,16 @@
 
 // jstypes.h is (or should be!) included by every file in SpiderMonkey.
 // js-config.h also should be included by every file. So include it here.
 // XXX: including it in js/RequiredDefines.h should be a better option, since
 // that is by definition the header file that should be included in all
 // SpiderMonkey code.  However, Gecko doesn't do this!  See bug 909576.
 #include "js-config.h"
 
-/***********************************************************************
-** MACROS:      JS_EXTERN_API
-**              JS_EXPORT_API
-** DESCRIPTION:
-**      These are only for externally visible routines and globals.  For
-**      internal routines, just use "extern" for type checking and that
-**      will not export internal cross-file or forward-declared symbols.
-**      Define a macro for declaring procedures return types. We use this to
-**      deal with windoze specific type hackery for DLL definitions. Use
-**      JS_EXTERN_API when the prototype for the method is declared. Use
-**      JS_EXPORT_API for the implementation of the method.
-**
-** Example:
-**   in dowhim.h
-**     JS_EXTERN_API( void ) DoWhatIMean( void );
-**   in dowhim.c
-**     JS_EXPORT_API( void ) DoWhatIMean( void ) { return; }
-**
-**
-***********************************************************************/
-
-#define JS_EXTERN_API(type)  extern MOZ_EXPORT type
-#define JS_EXPORT_API(type)  MOZ_EXPORT type
-#define JS_EXPORT_DATA(type) MOZ_EXPORT type
-#define JS_IMPORT_API(type)  MOZ_IMPORT_API type
-#define JS_IMPORT_DATA(type) MOZ_IMPORT_DATA type
-
 /*
  * The linkage of JS API functions differs depending on whether the file is
  * used within the JS library or not. Any source file within the JS
  * interpreter should define EXPORT_JS_API whereas any client of the library
  * should not. STATIC_JS_API is used to build JS as a static library.
  */
 #if defined(STATIC_JS_API)
 #  define JS_PUBLIC_API(t)   t
@@ -131,19 +104,16 @@
 ** MACROS:      JS_HOWMANY
 **              JS_ROUNDUP
 ** DESCRIPTION:
 **      Commonly used macros for operations on compatible types.
 ***********************************************************************/
 #define JS_HOWMANY(x,y) (((x)+(y)-1)/(y))
 #define JS_ROUNDUP(x,y) (JS_HOWMANY(x,y)*(y))
 
-#define JS_BITS_PER_BYTE 8
-#define JS_BITS_PER_BYTE_LOG2 3
-
 #if defined(JS_64BIT)
 # define JS_BITS_PER_WORD 64
 #else
 # define JS_BITS_PER_WORD 32
 #endif
 
 /***********************************************************************
 ** MACROS:      JS_FUNC_TO_DATA_PTR
@@ -159,17 +129,9 @@
 **      ...
 **      nativeGetter = JS_DATA_TO_FUNC_PTR(JSGetterOp, scriptedGetter);
 **
 ***********************************************************************/
 
 #define JS_FUNC_TO_DATA_PTR(type, fun)  (mozilla::BitwiseCast<type>(fun))
 #define JS_DATA_TO_FUNC_PTR(type, ptr)  (mozilla::BitwiseCast<type>(ptr))
 
-#ifdef __GNUC__
-# define JS_EXTENSION __extension__
-# define JS_EXTENSION_(s) __extension__ ({ s; })
-#else
-# define JS_EXTENSION
-# define JS_EXTENSION_(s) s
-#endif
-
 #endif /* jstypes_h */
diff --git a/js/src/shell/js.cpp b/js/src/shell/js.cpp
--- a/js/src/shell/js.cpp
+++ b/js/src/shell/js.cpp
@@ -578,18 +578,18 @@ JSSecurityCallbacks ShellPrincipals::sec
     subsumes
 };
 
 // The fully-trusted principal subsumes all other principals.
 ShellPrincipals ShellPrincipals::fullyTrusted(-1, 1);
 
 #ifdef EDITLINE
 extern "C" {
-extern JS_EXPORT_API(char*) readline(const char* prompt);
-extern JS_EXPORT_API(void)   add_history(char* line);
+extern MOZ_EXPORT char* readline(const char* prompt);
+extern MOZ_EXPORT void add_history(char* line);
 } // extern "C"
 #endif
 
 ShellContext::ShellContext(JSContext* cx)
   : isWorker(false),
     timeoutInterval(-1.0),
     startTime(PRMJ_Now()),
     serviceInterrupt(false),
diff --git a/js/src/vm/BytecodeUtil.cpp b/js/src/vm/BytecodeUtil.cpp
--- a/js/src/vm/BytecodeUtil.cpp
+++ b/js/src/vm/BytecodeUtil.cpp
@@ -56,17 +56,17 @@
 using namespace js;
 using namespace js::gc;
 
 using js::frontend::IsIdentifier;
 
 /*
  * Index limit must stay within 32 bits.
  */
-JS_STATIC_ASSERT(sizeof(uint32_t) * JS_BITS_PER_BYTE >= INDEX_LIMIT_LOG2 + 1);
+JS_STATIC_ASSERT(sizeof(uint32_t) * CHAR_BIT >= INDEX_LIMIT_LOG2 + 1);
 
 const JSCodeSpec js::CodeSpec[] = {
 #define MAKE_CODESPEC(op,val,name,token,length,nuses,ndefs,format)  {length,nuses,ndefs,format},
     FOR_EACH_OPCODE(MAKE_CODESPEC)
 #undef MAKE_CODESPEC
 };
 
 const unsigned js::NumCodeSpecs = mozilla::ArrayLength(CodeSpec);
diff --git a/js/xpconnect/src/nsXPConnect.cpp b/js/xpconnect/src/nsXPConnect.cpp
--- a/js/xpconnect/src/nsXPConnect.cpp
+++ b/js/xpconnect/src/nsXPConnect.cpp
@@ -1205,22 +1205,25 @@ nsXPConnect::WriteFunction(nsIObjectOutp
 NS_IMETHODIMP
 nsXPConnect::ReadFunction(nsIObjectInputStream* stream, JSContext* cx, JSObject** functionObjp)
 {
     return ReadScriptOrFunction(stream, cx, nullptr, functionObjp);
 }
 
 /* These are here to be callable from a debugger */
 extern "C" {
-JS_EXPORT_API(void) DumpJSStack()
+
+MOZ_EXPORT void
+DumpJSStack()
 {
     xpc_DumpJSStack(true, true, false);
 }
 
-JS_EXPORT_API(void) DumpCompleteHeap()
+MOZ_EXPORT void
+DumpCompleteHeap()
 {
     nsCOMPtr<nsICycleCollectorListener> listener =
       do_CreateInstance("@mozilla.org/cycle-collector-logger;1");
     if (!listener) {
       NS_WARNING("Failed to create CC logger");
       return;
     }
 
