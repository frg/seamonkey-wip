# HG changeset patch
# User Andrea Marchesini <amarchesini@mozilla.com>
# Date 1515416705 -3600
# Node ID c5c4104210b2315a8ed4de66cc7e16522a43deb5
# Parent  ca65c2674345abeb070fb93fdab10049712b4378
Bug 1419771 - Introduce DOMPrefs, a thread-safe access to preferences for DOM - part 12 - Push enabled, r=asuth

diff --git a/dom/base/DOMPrefs.cpp b/dom/base/DOMPrefs.cpp
--- a/dom/base/DOMPrefs.cpp
+++ b/dom/base/DOMPrefs.cpp
@@ -40,16 +40,17 @@ PREF(DOMCachesTestingEnabled, "dom.cache
 PREF(PerformanceLoggingEnabled, "dom.performance.enable_user_timing_logging")
 PREF(NotificationEnabled, "dom.webnotifications.enabled")
 PREF(NotificationEnabledInServiceWorkers, "dom.webnotifications.serviceworker.enabled")
 PREF(NotificationRIEnabled, "dom.webnotifications.requireinteraction.enabled")
 PREF(ServiceWorkersEnabled, "dom.serviceWorkers.enabled")
 PREF(ServiceWorkersTestingEnabled, "dom.serviceWorkers.testing.enabled")
 PREF(StorageManagerEnabled, "dom.storageManager.enabled")
 PREF(PromiseRejectionEventsEnabled, "dom.promise_rejection_events.enabled")
+PREF(PushEnabled, "dom.push.enabled")
 
 #undef PREF
 
 #define PREF_WEBIDL(name)                        \
   /* static */ bool                              \
   DOMPrefs::name(JSContext* aCx, JSObject* aObj) \
   {                                              \
     return DOMPrefs::name();                     \
@@ -57,13 +58,14 @@ PREF(PromiseRejectionEventsEnabled, "dom
 
 PREF_WEBIDL(ImageBitmapExtensionsEnabled)
 PREF_WEBIDL(DOMCachesEnabled)
 PREF_WEBIDL(NotificationEnabledInServiceWorkers)
 PREF_WEBIDL(NotificationRIEnabled)
 PREF_WEBIDL(ServiceWorkersEnabled)
 PREF_WEBIDL(StorageManagerEnabled)
 PREF_WEBIDL(PromiseRejectionEventsEnabled)
+PREF_WEBIDL(PushEnabled)
 
 #undef PREF_WEBIDL
 
 } // dom namespace
 } // mozilla namespace
diff --git a/dom/base/DOMPrefs.h b/dom/base/DOMPrefs.h
--- a/dom/base/DOMPrefs.h
+++ b/dom/base/DOMPrefs.h
@@ -54,14 +54,18 @@ public:
 
   // Returns true if the dom.storageManager.enabled pref is set.
   static bool StorageManagerEnabled();
   static bool StorageManagerEnabled(JSContext* aCx, JSObject* aObj);
 
   // Returns true if the dom.promise_rejection_events.enabled pref is set.
   static bool PromiseRejectionEventsEnabled();
   static bool PromiseRejectionEventsEnabled(JSContext* aCx, JSObject* aObj);
+
+  // Returns true if the dom.push.enabled pref is set.
+  static bool PushEnabled();
+  static bool PushEnabled(JSContext* aCx, JSObject* aObj);
 };
 
 } // dom namespace
 } // mozilla namespace
 
 #endif // mozilla_dom_DOMPrefs_h
diff --git a/dom/base/nsContentUtils.cpp b/dom/base/nsContentUtils.cpp
--- a/dom/base/nsContentUtils.cpp
+++ b/dom/base/nsContentUtils.cpp
@@ -9071,35 +9071,16 @@ nsContentUtils::GetReferrerPolicyFromHea
       referrerPolicy = policy;
     }
   }
   return referrerPolicy;
 }
 
 // static
 bool
-nsContentUtils::PushEnabled(JSContext* aCx, JSObject* aObj)
-{
-  if (NS_IsMainThread()) {
-    return Preferences::GetBool("dom.push.enabled", false);
-  }
-
-  using namespace workers;
-
-  // Otherwise, check the pref via the WorkerPrivate
-  WorkerPrivate* workerPrivate = GetWorkerPrivateFromContext(aCx);
-  if (!workerPrivate) {
-    return false;
-  }
-
-  return workerPrivate->PushEnabled();
-}
-
-// static
-bool
 nsContentUtils::StreamsEnabled(JSContext* aCx, JSObject* aObj)
 {
   if (NS_IsMainThread()) {
     return Preferences::GetBool("dom.streams.enabled", false);
   }
 
   using namespace workers;
 
diff --git a/dom/base/nsContentUtils.h b/dom/base/nsContentUtils.h
--- a/dom/base/nsContentUtils.h
+++ b/dom/base/nsContentUtils.h
@@ -2952,18 +2952,16 @@ public:
    * Parse a referrer policy from a Referrer-Policy header
    * https://www.w3.org/TR/referrer-policy/#parse-referrer-policy-from-header
    *
    * @param aHeader the response's Referrer-Policy header to parse
    * @return referrer policy from the response header.
    */
   static mozilla::net::ReferrerPolicy GetReferrerPolicyFromHeader(const nsAString& aHeader);
 
-  static bool PushEnabled(JSContext* aCx, JSObject* aObj);
-
   static bool StreamsEnabled(JSContext* aCx, JSObject* aObj);
 
   static bool IsNonSubresourceRequest(nsIChannel* aChannel);
 
   static uint32_t CookiesBehavior()
   {
     return sCookiesBehavior;
   }
diff --git a/dom/push/PushManager.h b/dom/push/PushManager.h
--- a/dom/push/PushManager.h
+++ b/dom/push/PushManager.h
@@ -27,19 +27,19 @@
 #define mozilla_dom_PushManager_h
 
 #include "nsWrapperCache.h"
 
 #include "mozilla/AlreadyAddRefed.h"
 #include "mozilla/ErrorResult.h"
 #include "mozilla/dom/BindingDeclarations.h"
 #include "mozilla/dom/TypedArray.h"
+#include "mozilla/dom/DOMPrefs.h"
 
 #include "nsCOMPtr.h"
-#include "nsContentUtils.h" // Required for nsContentUtils::PushEnabled
 #include "mozilla/RefPtr.h"
 
 class nsIGlobalObject;
 class nsIPrincipal;
 
 namespace mozilla {
 namespace dom {
 
diff --git a/dom/push/PushSubscription.h b/dom/push/PushSubscription.h
--- a/dom/push/PushSubscription.h
+++ b/dom/push/PushSubscription.h
@@ -5,23 +5,23 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef mozilla_dom_PushSubscription_h
 #define mozilla_dom_PushSubscription_h
 
 #include "jsapi.h"
 #include "nsCOMPtr.h"
 #include "nsWrapperCache.h"
-#include "nsContentUtils.h" // Required for nsContentUtils::PushEnabled
 
 #include "mozilla/AlreadyAddRefed.h"
 #include "mozilla/ErrorResult.h"
 #include "mozilla/RefPtr.h"
 
 #include "mozilla/dom/BindingDeclarations.h"
+#include "mozilla/dom/DOMPrefs.h"
 #include "mozilla/dom/PushSubscriptionBinding.h"
 #include "mozilla/dom/PushSubscriptionOptionsBinding.h"
 #include "mozilla/dom/TypedArray.h"
 
 class nsIGlobalObject;
 
 namespace mozilla {
 namespace dom {
diff --git a/dom/push/PushSubscriptionOptions.h b/dom/push/PushSubscriptionOptions.h
--- a/dom/push/PushSubscriptionOptions.h
+++ b/dom/push/PushSubscriptionOptions.h
@@ -3,19 +3,19 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef mozilla_dom_PushSubscriptionOptions_h
 #define mozilla_dom_PushSubscriptionOptions_h
 
 #include "nsCycleCollectionParticipant.h"
-#include "nsContentUtils.h" // Required for nsContentUtils::PushEnabled
 #include "nsTArray.h"
 #include "nsWrapperCache.h"
+#include "mozilla/dom/DOMPrefs.h"
 
 class nsIGlobalObject;
 
 namespace mozilla {
 
 class ErrorResult;
 
 namespace dom {
diff --git a/dom/webidl/PushEvent.webidl b/dom/webidl/PushEvent.webidl
--- a/dom/webidl/PushEvent.webidl
+++ b/dom/webidl/PushEvent.webidl
@@ -3,17 +3,17 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this file,
  * You can obtain one at http://mozilla.org/MPL/2.0/.
  *
  * The origin of this IDL file is
  * https://w3c.github.io/push-api/
  */
 
 [Constructor(DOMString type, optional PushEventInit eventInitDict),
- Func="nsContentUtils::PushEnabled",
+ Func="mozilla::dom::DOMPrefs::PushEnabled",
  Exposed=ServiceWorker]
 interface PushEvent : ExtendableEvent {
   readonly attribute PushMessageData? data;
 };
 
 typedef (BufferSource or USVString) PushMessageDataInit;
 
 dictionary PushEventInit : ExtendableEventInit {
diff --git a/dom/webidl/PushManager.webidl b/dom/webidl/PushManager.webidl
--- a/dom/webidl/PushManager.webidl
+++ b/dom/webidl/PushManager.webidl
@@ -17,17 +17,17 @@ dictionary PushSubscriptionOptionsInit {
 [JSImplementation="@mozilla.org/push/PushManager;1",
  ChromeOnly, Constructor(DOMString scope)]
 interface PushManagerImpl {
   Promise<PushSubscription>    subscribe(optional PushSubscriptionOptionsInit options);
   Promise<PushSubscription?>   getSubscription();
   Promise<PushPermissionState> permissionState(optional PushSubscriptionOptionsInit options);
 };
 
-[Exposed=(Window,Worker), Func="nsContentUtils::PushEnabled",
+[Exposed=(Window,Worker), Func="mozilla::dom::DOMPrefs::PushEnabled",
  ChromeConstructor(DOMString scope)]
 interface PushManager {
   [Throws, UseCounter]
   Promise<PushSubscription>    subscribe(optional PushSubscriptionOptionsInit options);
   [Throws]
   Promise<PushSubscription?>   getSubscription();
   [Throws]
   Promise<PushPermissionState> permissionState(optional PushSubscriptionOptionsInit options);
diff --git a/dom/webidl/PushMessageData.webidl b/dom/webidl/PushMessageData.webidl
--- a/dom/webidl/PushMessageData.webidl
+++ b/dom/webidl/PushMessageData.webidl
@@ -2,17 +2,17 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this file,
  * You can obtain one at http://mozilla.org/MPL/2.0/.
  *
  * The origin of this IDL file is
  * https://w3c.github.io/push-api/
  */
 
-[Func="nsContentUtils::PushEnabled",
+[Func="mozilla::dom::DOMPrefs::PushEnabled",
  Exposed=ServiceWorker]
 interface PushMessageData
 {
     [Throws]
     ArrayBuffer arrayBuffer();
     [Throws]
     Blob        blob();
     [Throws]
diff --git a/dom/webidl/PushSubscription.webidl b/dom/webidl/PushSubscription.webidl
--- a/dom/webidl/PushSubscription.webidl
+++ b/dom/webidl/PushSubscription.webidl
@@ -31,17 +31,17 @@ dictionary PushSubscriptionInit
 {
   required USVString endpoint;
   required USVString scope;
   ArrayBuffer? p256dhKey;
   ArrayBuffer? authSecret;
   BufferSource? appServerKey;
 };
 
-[Exposed=(Window,Worker), Func="nsContentUtils::PushEnabled",
+[Exposed=(Window,Worker), Func="mozilla::dom::DOMPrefs::PushEnabled",
  ChromeConstructor(PushSubscriptionInit initDict)]
 interface PushSubscription
 {
   readonly attribute USVString endpoint;
   readonly attribute PushSubscriptionOptions options;
   [Throws]
   ArrayBuffer? getKey(PushEncryptionKeyName name);
   [Throws, UseCounter]
diff --git a/dom/webidl/PushSubscriptionOptions.webidl b/dom/webidl/PushSubscriptionOptions.webidl
--- a/dom/webidl/PushSubscriptionOptions.webidl
+++ b/dom/webidl/PushSubscriptionOptions.webidl
@@ -2,14 +2,14 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * The origin of this IDL file is
 * https://w3c.github.io/push-api/
 */
 
-[Exposed=(Window,Worker), Func="nsContentUtils::PushEnabled"]
+[Exposed=(Window,Worker), Func="mozilla::dom::DOMPrefs::PushEnabled"]
 interface PushSubscriptionOptions
 {
   [SameObject, Throws]
   readonly attribute ArrayBuffer? applicationServerKey;
 };
diff --git a/dom/webidl/ServiceWorkerRegistration.webidl b/dom/webidl/ServiceWorkerRegistration.webidl
--- a/dom/webidl/ServiceWorkerRegistration.webidl
+++ b/dom/webidl/ServiceWorkerRegistration.webidl
@@ -33,17 +33,17 @@ interface ServiceWorkerRegistration : Ev
 enum ServiceWorkerUpdateViaCache {
   "imports",
   "all",
   "none"
 };
 
 // https://w3c.github.io/push-api/
 partial interface ServiceWorkerRegistration {
-  [Throws, Exposed=(Window,Worker), Func="nsContentUtils::PushEnabled"]
+  [Throws, Exposed=(Window,Worker), Func="mozilla::dom::DOMPrefs::PushEnabled"]
   readonly attribute PushManager pushManager;
 };
 
 // https://notifications.spec.whatwg.org/
 partial interface ServiceWorkerRegistration {
   [Throws, Func="mozilla::dom::DOMPrefs::NotificationEnabledInServiceWorkers"]
   Promise<void> showNotification(DOMString title, optional NotificationOptions options);
   [Throws, Func="mozilla::dom::DOMPrefs::NotificationEnabledInServiceWorkers"]
diff --git a/dom/workers/ServiceWorkerRegistration.h b/dom/workers/ServiceWorkerRegistration.h
--- a/dom/workers/ServiceWorkerRegistration.h
+++ b/dom/workers/ServiceWorkerRegistration.h
@@ -3,21 +3,21 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef mozilla_dom_ServiceWorkerRegistration_h
 #define mozilla_dom_ServiceWorkerRegistration_h
 
 #include "mozilla/DOMEventTargetHelper.h"
+#include "mozilla/dom/DOMPrefs.h"
 #include "mozilla/dom/ServiceWorkerBinding.h"
 #include "mozilla/dom/ServiceWorkerCommon.h"
 #include "mozilla/dom/ServiceWorkerRegistrationBinding.h"
 #include "mozilla/dom/workers/bindings/WorkerHolder.h"
-#include "nsContentUtils.h" // Required for nsContentUtils::PushEnabled
 
 // Support for Notification API extension.
 #include "mozilla/dom/NotificationBinding.h"
 
 class nsPIDOMWindowInner;
 
 namespace mozilla {
 namespace dom {
diff --git a/dom/workers/WorkerPrefs.h b/dom/workers/WorkerPrefs.h
--- a/dom/workers/WorkerPrefs.h
+++ b/dom/workers/WorkerPrefs.h
@@ -16,17 +16,16 @@
 //     macro in Workers.h.
 //   * The name of the function that updates the new value of a pref.
 //
 //   WORKER_PREF("foo.bar", UpdaterFunction)
 //
 //   * First argument is the name of the pref.
 //   * The name of the function that updates the new value of a pref.
 
-WORKER_SIMPLE_PREF("dom.push.enabled", PushEnabled, PUSH_ENABLED)
 WORKER_SIMPLE_PREF("dom.streams.enabled", StreamsEnabled, STREAMS_ENABLED)
 WORKER_SIMPLE_PREF("dom.requestcontext.enabled", RequestContextEnabled, REQUESTCONTEXT_ENABLED)
 WORKER_SIMPLE_PREF("gfx.offscreencanvas.enabled", OffscreenCanvasEnabled, OFFSCREENCANVAS_ENABLED)
 WORKER_SIMPLE_PREF("dom.webkitBlink.dirPicker.enabled", WebkitBlinkDirectoryPickerEnabled, DOM_WEBKITBLINK_DIRPICKER_WEBKITBLINK)
 WORKER_SIMPLE_PREF("dom.netinfo.enabled", NetworkInformationEnabled, NETWORKINFORMATION_ENABLED)
 WORKER_SIMPLE_PREF("dom.fetchObserver.enabled", FetchObserverEnabled, FETCHOBSERVER_ENABLED)
 WORKER_SIMPLE_PREF("privacy.resistFingerprinting", ResistFingerprintingEnabled, RESISTFINGERPRINTING_ENABLED)
 WORKER_SIMPLE_PREF("devtools.enabled", DevToolsEnabled, DEVTOOLS_ENABLED)
