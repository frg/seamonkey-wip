# HG changeset patch
# User Jon Coppeard <jcoppeard@mozilla.com>
# Date 1526466183 -3600
#      Wed May 16 11:23:03 2018 +0100
# Node ID 4b6c733447b8f9dd693c0698e3613169170d21a9
# Parent  8f2d120bdcb94a3e8c58015524a34bb571db53ea
Bug 1461319 - Fix assertion failure accessing shape of associated object when tracing debugger object in a moving GC r=sfink

diff --git a/js/src/jit-test/tests/gc/bug-1461319.js b/js/src/jit-test/tests/gc/bug-1461319.js
new file mode 100644
--- /dev/null
+++ b/js/src/jit-test/tests/gc/bug-1461319.js
@@ -0,0 +1,7 @@
+// |jit-test| error: InternalError
+gczeal(14);
+var g = newGlobal();
+g.eval('function f(a) { evaluate("f(" + " - 1);", {newContext: true}); }');
+var dbg = new Debugger(g);
+dbg.onEnterFrame = function(frame) {};
+g.f();
diff --git a/js/src/vm/Debugger.cpp b/js/src/vm/Debugger.cpp
--- a/js/src/vm/Debugger.cpp
+++ b/js/src/vm/Debugger.cpp
@@ -3192,18 +3192,18 @@ Debugger::trace(JSTracer* trc)
      *
      * (Once we support generator frames properly, we will need
      * weakly-referenced Debugger.Frame objects as well, for suspended generator
      * frames.)
      */
     if (frames.initialized()) {
         for (FrameMap::Range r = frames.all(); !r.empty(); r.popFront()) {
             HeapPtr<DebuggerFrame*>& frameobj = r.front().value();
-            MOZ_ASSERT(MaybeForwarded(frameobj.get())->getPrivate());
             TraceEdge(trc, &frameobj, "live Debugger.Frame");
+            MOZ_ASSERT(frameobj->getPrivate(frameobj->numFixedSlotsMaybeForwarded()));
         }
     }
 
     allocationsLog.trace(trc);
 
     /* Trace the weak map from JSScript instances to Debugger.Script objects. */
     scripts.trace(trc);
 
diff --git a/js/src/vm/NativeObject.cpp b/js/src/vm/NativeObject.cpp
--- a/js/src/vm/NativeObject.cpp
+++ b/js/src/vm/NativeObject.cpp
@@ -305,16 +305,24 @@ js::NativeObject::slotInRange(uint32_t s
 bool
 js::NativeObject::slotIsFixed(uint32_t slot) const
 {
     // We call numFixedSlotsMaybeForwarded() to allow reading slots of
     // associated objects in trace hooks that may be called during a moving GC.
     return slot < numFixedSlotsMaybeForwarded();
 }
 
+bool
+js::NativeObject::isNumFixedSlots(uint32_t nfixed) const
+{
+    // We call numFixedSlotsMaybeForwarded() to allow reading slots of
+    // associated objects in trace hooks that may be called during a moving GC.
+    return nfixed == numFixedSlotsMaybeForwarded();
+}
+
 #endif /* DEBUG */
 
 Shape*
 js::NativeObject::lookup(JSContext* cx, jsid id)
 {
     MOZ_ASSERT(isNative());
     return Shape::search(cx, lastProperty(), id);
 }
diff --git a/js/src/vm/NativeObject.h b/js/src/vm/NativeObject.h
--- a/js/src/vm/NativeObject.h
+++ b/js/src/vm/NativeObject.h
@@ -687,16 +687,21 @@ class NativeObject : public ShapedObject
      * If sentinelAllowed then slot may equal the slot capacity.
      */
     bool slotInRange(uint32_t slot, SentinelAllowed sentinel = SENTINEL_NOT_ALLOWED) const;
 
     /*
      * Check whether a slot is a fixed slot.
      */
     bool slotIsFixed(uint32_t slot) const;
+
+    /*
+     * Check whether the supplied number of fixed slots is correct.
+     */
+    bool isNumFixedSlots(uint32_t nfixed) const;
 #endif
 
     /*
      * Minimum size for dynamically allocated slots in normal Objects.
      * ArrayObjects don't use this limit and can have a lower slot capacity,
      * since they normally don't have a lot of slots.
      */
     static const uint32_t SLOT_CAPACITY_MIN = 8;
@@ -735,18 +740,20 @@ class NativeObject : public ShapedObject
                       "int32_t, too)");
     }
 
     uint32_t numFixedSlots() const {
         return reinterpret_cast<const shadow::Object*>(this)->numFixedSlots();
     }
 
     // Get the number of fixed slots when the shape pointer may have been
-    // forwarded by a moving GC.
-    uint32_t numFixedSlotsMaybeForwarded() const;
+    // forwarded by a moving GC. You need to use this rather that
+    // numFixedSlots() in a trace hook if you access an object that is not the
+    // object being traced, since it may have a stale shape pointer.
+    inline uint32_t numFixedSlotsMaybeForwarded() const;
 
     uint32_t numUsedFixedSlots() const {
         uint32_t nslots = lastProperty()->slotSpan(getClass());
         return Min(nslots, numFixedSlots());
     }
 
     uint32_t slotSpan() const {
         if (inDictionaryMode())
@@ -1406,17 +1413,17 @@ class NativeObject : public ShapedObject
     /* Private data accessors. */
 
     inline void*& privateRef(uint32_t nfixed) const { /* XXX should be private, not protected! */
         /*
          * The private pointer of an object can hold any word sized value.
          * Private pointers are stored immediately after the last fixed slot of
          * the object.
          */
-        MOZ_ASSERT(nfixed == numFixedSlots());
+        MOZ_ASSERT(isNumFixedSlots(nfixed));
         MOZ_ASSERT(hasPrivate());
         HeapSlot* end = &fixedSlots()[nfixed];
         return *reinterpret_cast<void**>(end);
     }
 
     bool hasPrivate() const {
         return getClass()->hasPrivate();
     }
