# HG changeset patch
# User Jason Orendorff <jorendorff@mozilla.com>
# Date 1535660912 18000
#      Thu Aug 30 15:28:32 2018 -0500
# Node ID eb2019c9a30eb0b32dff94bce7fd62645c886036
# Parent  f88bf0327d46c9e5fc9835d77aba06fee1a7ec8e
Bug 1442587 - Part 3: Avoid Vector<Value> in the front end. r=Waldo

diff --git a/js/src/frontend/BytecodeEmitter.cpp b/js/src/frontend/BytecodeEmitter.cpp
--- a/js/src/frontend/BytecodeEmitter.cpp
+++ b/js/src/frontend/BytecodeEmitter.cpp
@@ -100,17 +100,17 @@ BytecodeEmitter::BytecodeEmitter(Bytecod
     bodyScopeIndex(UINT32_MAX),
     varEmitterScope(nullptr),
     innermostNestableControl(nullptr),
     innermostEmitterScope_(nullptr),
     innermostTDZCheckCache(nullptr),
 #ifdef DEBUG
     unstableEmitterScope(false),
 #endif
-    constList(cx),
+    numberList(cx),
     scopeList(cx),
     tryNoteList(cx),
     scopeNoteList(cx),
     yieldAndAwaitOffsetList(cx),
     typesetCount(0),
     hasSingletons(false),
     hasTryFinally(false),
     emittingRunOnceLambda(false),
@@ -2290,20 +2290,20 @@ BytecodeEmitter::emitNumberOp(double dva
             ptrdiff_t off;
             if (!emitN(JSOP_INT32, 4, &off))
                 return false;
             SET_INT32(code(off), ival);
         }
         return true;
     }
 
-    if (!constList.append(DoubleValue(dval)))
-        return false;
-
-    return emitIndex32(JSOP_DOUBLE, constList.length() - 1);
+    if (!numberList.append(dval))
+        return false;
+
+    return emitIndex32(JSOP_DOUBLE, numberList.length() - 1);
 }
 
 /*
  * Using MOZ_NEVER_INLINE in here is a workaround for llvm.org/pr14047.
  * LLVM is deciding to inline this function which uses a lot of stack space
  * into emitTree which is recursive and uses relatively little stack space.
  */
 MOZ_NEVER_INLINE bool
@@ -8543,22 +8543,22 @@ BytecodeEmitter::copySrcNotes(jssrcnote*
     unsigned mainCount = main.notes.length();
     // nsrcnotes includes SN_MAKE_TERMINATOR in addition to main.notes.
     MOZ_ASSERT(mainCount == nsrcnotes - 1);
     PodCopy(destination, main.notes.begin(), mainCount);
     SN_MAKE_TERMINATOR(&destination[mainCount]);
 }
 
 void
-CGConstList::finish(ConstArray* array)
+CGNumberList::finish(ConstArray* array)
 {
     MOZ_ASSERT(length() == array->length);
 
     for (unsigned i = 0; i < length(); i++)
-        array->vector[i] = list[i];
+        array->vector[i] = DoubleValue(list[i]);
 }
 
 /*
  * Find the index of the given object for code generator.
  *
  * Since the emitter refers to each parsed object only once, for the index we
  * use the number of already indexed objects. We also add the object to a list
  * to convert the list to a fixed-size array when we complete code generation,
diff --git a/js/src/frontend/BytecodeEmitter.h b/js/src/frontend/BytecodeEmitter.h
--- a/js/src/frontend/BytecodeEmitter.h
+++ b/js/src/frontend/BytecodeEmitter.h
@@ -20,22 +20,22 @@
 #include "frontend/SourceNotes.h"
 #include "vm/BytecodeUtil.h"
 #include "vm/Interpreter.h"
 #include "vm/Iteration.h"
 
 namespace js {
 namespace frontend {
 
-class CGConstList {
-    Vector<Value> list;
+class CGNumberList {
+    Vector<double> list;
+
   public:
-    explicit CGConstList(JSContext* cx) : list(cx) {}
-    MOZ_MUST_USE bool append(const Value& v) {
-        MOZ_ASSERT_IF(v.isString(), v.toString()->isAtom());
+    explicit CGNumberList(JSContext* cx) : list(cx) {}
+    MOZ_MUST_USE bool append(double v) {
         return list.append(v);
     }
     size_t length() const { return list.length(); }
     void finish(ConstArray* array);
 };
 
 struct CGObjectList {
     uint32_t            length;     /* number of emitted so far objects */
@@ -195,17 +195,17 @@ struct MOZ_STACK_CLASS BytecodeEmitter
     EmitterScope* innermostEmitterScope() const {
         MOZ_ASSERT(!unstableEmitterScope);
         return innermostEmitterScopeNoCheck();
     }
     EmitterScope* innermostEmitterScopeNoCheck() const {
         return innermostEmitterScope_;
     }
 
-    CGConstList      constList;      /* constants to be included with the script */
+    CGNumberList     numberList;     /* number constants to be included with the script */
     CGObjectList     objectList;     /* list of emitted objects */
     CGScopeList      scopeList;      /* list of emitted scopes */
     CGTryNoteList    tryNoteList;    /* list of emitted try notes */
     CGScopeNoteList  scopeNoteList;  /* list of emitted block scope notes */
 
     /*
      * For each yield or await op, map the yield and await index (stored as
      * bytecode operand) to the offset of the next op.
diff --git a/js/src/vm/JSScript.cpp b/js/src/vm/JSScript.cpp
--- a/js/src/vm/JSScript.cpp
+++ b/js/src/vm/JSScript.cpp
@@ -2949,17 +2949,17 @@ JSScript::fullyInitFromEmitter(JSContext
 
     uint32_t mainLength = bce->offset();
     uint32_t prologueLength = bce->prologueOffset();
     uint32_t nsrcnotes;
     if (!bce->finishTakingSrcNotes(&nsrcnotes))
         return false;
     uint32_t natoms = bce->atomIndices->count();
     if (!partiallyInit(cx, script,
-                       bce->scopeList.length(), bce->constList.length(), bce->objectList.length,
+                       bce->scopeList.length(), bce->numberList.length(), bce->objectList.length,
                        bce->tryNoteList.length(), bce->scopeNoteList.length(),
                        bce->yieldAndAwaitOffsetList.length()))
     {
         return false;
     }
 
     MOZ_ASSERT(script->mainOffset() == 0);
     script->mainOffset_ = prologueLength;
@@ -2978,18 +2978,18 @@ JSScript::fullyInitFromEmitter(JSContext
     PodCopy<jsbytecode>(code, bce->prologue.code.begin(), prologueLength);
     PodCopy<jsbytecode>(code + prologueLength, bce->main.code.begin(), mainLength);
     bce->copySrcNotes((jssrcnote*)(code + script->length()), nsrcnotes);
     InitAtomMap(*bce->atomIndices, script->atoms());
 
     if (!script->shareScriptData(cx))
         return false;
 
-    if (bce->constList.length() != 0)
-        bce->constList.finish(script->consts());
+    if (bce->numberList.length() != 0)
+        bce->numberList.finish(script->consts());
     if (bce->objectList.length != 0)
         bce->objectList.finish(script->objects());
     if (bce->scopeList.length() != 0)
         bce->scopeList.finish(script->scopes());
     if (bce->tryNoteList.length() != 0)
         bce->tryNoteList.finish(script->trynotes());
     if (bce->scopeNoteList.length() != 0)
         bce->scopeNoteList.finish(script->scopeNotes(), prologueLength);
