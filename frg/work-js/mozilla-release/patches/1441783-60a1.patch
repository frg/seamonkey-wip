# HG changeset patch
# User Jon Coppeard <jcoppeard@mozilla.com>
# Date 1519915649 0
#      Thu Mar 01 14:47:29 2018 +0000
# Node ID 8d5acf4f29bb3d620680cf0f9b0ff89b405445f2
# Parent  6b36c70736ea11f42b2d6aa958a44c50ab8c55eb
Bug 1441783 - Remove unused AutoVectorRooter class and refactor r=sfink

diff --git a/js/public/RootingAPI.h b/js/public/RootingAPI.h
--- a/js/public/RootingAPI.h
+++ b/js/public/RootingAPI.h
@@ -887,29 +887,22 @@ class JS_PUBLIC_API(AutoGCRooter)
      * below.  Any other negative value indicates some deeper problem such as
      * memory corruption.
      */
     ptrdiff_t tag_;
 
     enum {
         VALARRAY =     -2, /* js::AutoValueArray */
         PARSER =       -3, /* js::frontend::Parser */
-        VALVECTOR =   -10, /* js::AutoValueVector */
-        IDVECTOR =    -11, /* js::AutoIdVector */
-        OBJVECTOR =   -14, /* js::AutoObjectVector */
         IONMASM =     -19, /* js::jit::MacroAssembler */
         WRAPVECTOR =  -20, /* js::AutoWrapperVector */
         WRAPPER =     -21, /* js::AutoWrapperRooter */
         CUSTOM =      -26  /* js::CustomAutoRooter */
     };
 
-    static ptrdiff_t GetTag(const Value& value) { return VALVECTOR; }
-    static ptrdiff_t GetTag(const jsid& id) { return IDVECTOR; }
-    static ptrdiff_t GetTag(JSObject* obj) { return OBJVECTOR; }
-
   private:
     AutoGCRooter ** const stackTop;
 
     /* No copy or assignment semantics. */
     AutoGCRooter(AutoGCRooter& ida) = delete;
     void operator=(AutoGCRooter& ida) = delete;
 };
 
diff --git a/js/src/gc/RootMarking.cpp b/js/src/gc/RootMarking.cpp
--- a/js/src/gc/RootMarking.cpp
+++ b/js/src/gc/RootMarking.cpp
@@ -168,23 +168,23 @@ AutoGCRooter::trace(JSTracer* trc)
          * in RemapAllWrappersForObject; see comment there.
          */
         TraceManuallyBarrieredEdge(trc, &static_cast<AutoWrapperRooter*>(this)->value.get(),
                                    "JS::AutoWrapperRooter.value");
         return;
       }
 
       case WRAPVECTOR: {
-        AutoWrapperVector::VectorImpl& vector = static_cast<AutoWrapperVector*>(this)->vector;
+        auto vector = static_cast<AutoWrapperVector*>(this);
         /*
          * We need to use TraceManuallyBarrieredEdge here because we trace
          * wrapper roots in every slice. This is because of some rule-breaking
          * in RemapAllWrappersForObject; see comment there.
          */
-        for (WrapperValue* p = vector.begin(); p < vector.end(); p++)
+        for (WrapperValue* p = vector->begin(); p < vector->end(); p++)
             TraceManuallyBarrieredEdge(trc, &p->get(), "js::AutoWrapperVector.vector");
         return;
       }
 
       case CUSTOM:
         static_cast<JS::CustomAutoRooter*>(this)->trace(trc);
         return;
     }
diff --git a/js/src/jsapi.h b/js/src/jsapi.h
--- a/js/src/jsapi.h
+++ b/js/src/jsapi.h
@@ -97,120 +97,16 @@ class MOZ_RAII AutoValueArray : public A
     MutableHandleValue operator[](unsigned i) {
         MOZ_ASSERT(i < N);
         return MutableHandleValue::fromMarkedLocation(&elements_[i]);
     }
 
     MOZ_DECL_USE_GUARD_OBJECT_NOTIFIER
 };
 
-template<class T>
-class MOZ_RAII AutoVectorRooterBase : protected AutoGCRooter
-{
-    typedef js::Vector<T, 8> VectorImpl;
-    VectorImpl vector;
-
-  public:
-    explicit AutoVectorRooterBase(JSContext* cx, ptrdiff_t tag
-                              MOZ_GUARD_OBJECT_NOTIFIER_PARAM)
-      : AutoGCRooter(cx, tag), vector(cx)
-    {
-        MOZ_GUARD_OBJECT_NOTIFIER_INIT;
-    }
-
-    typedef T ElementType;
-    typedef typename VectorImpl::Range Range;
-
-    size_t length() const { return vector.length(); }
-    bool empty() const { return vector.empty(); }
-
-    MOZ_MUST_USE bool append(const T& v) { return vector.append(v); }
-    MOZ_MUST_USE bool appendN(const T& v, size_t len) { return vector.appendN(v, len); }
-    MOZ_MUST_USE bool append(const T* ptr, size_t len) { return vector.append(ptr, len); }
-    MOZ_MUST_USE bool appendAll(const AutoVectorRooterBase<T>& other) {
-        return vector.appendAll(other.vector);
-    }
-
-    MOZ_MUST_USE bool insert(T* p, const T& val) { return vector.insert(p, val); }
-
-    /* For use when space has already been reserved. */
-    void infallibleAppend(const T& v) { vector.infallibleAppend(v); }
-
-    void popBack() { vector.popBack(); }
-    T popCopy() { return vector.popCopy(); }
-
-    MOZ_MUST_USE bool growBy(size_t inc) {
-        size_t oldLength = vector.length();
-        if (!vector.growByUninitialized(inc))
-            return false;
-        makeRangeGCSafe(oldLength);
-        return true;
-    }
-
-    MOZ_MUST_USE bool resize(size_t newLength) {
-        size_t oldLength = vector.length();
-        if (newLength <= oldLength) {
-            vector.shrinkBy(oldLength - newLength);
-            return true;
-        }
-        if (!vector.growByUninitialized(newLength - oldLength))
-            return false;
-        makeRangeGCSafe(oldLength);
-        return true;
-    }
-
-    void clear() { vector.clear(); }
-
-    MOZ_MUST_USE bool reserve(size_t newLength) {
-        return vector.reserve(newLength);
-    }
-
-    JS::MutableHandle<T> operator[](size_t i) {
-        return JS::MutableHandle<T>::fromMarkedLocation(&vector[i]);
-    }
-    JS::Handle<T> operator[](size_t i) const {
-        return JS::Handle<T>::fromMarkedLocation(&vector[i]);
-    }
-
-    const T* begin() const { return vector.begin(); }
-    T* begin() { return vector.begin(); }
-
-    const T* end() const { return vector.end(); }
-    T* end() { return vector.end(); }
-
-    Range all() { return vector.all(); }
-
-    const T& back() const { return vector.back(); }
-
-    friend void AutoGCRooter::trace(JSTracer* trc);
-
-  private:
-    void makeRangeGCSafe(size_t oldLength) {
-        T* t = vector.begin() + oldLength;
-        for (size_t i = oldLength; i < vector.length(); ++i, ++t)
-            memset(t, 0, sizeof(T));
-    }
-
-    MOZ_DECL_USE_GUARD_OBJECT_NOTIFIER
-};
-
-template <typename T>
-class MOZ_RAII AutoVectorRooter : public AutoVectorRooterBase<T>
-{
-  public:
-    explicit AutoVectorRooter(JSContext* cx
-                             MOZ_GUARD_OBJECT_NOTIFIER_PARAM)
-        : AutoVectorRooterBase<T>(cx, this->GetTag(T()))
-    {
-        MOZ_GUARD_OBJECT_NOTIFIER_INIT;
-    }
-
-    MOZ_DECL_USE_GUARD_OBJECT_NOTIFIER
-};
-
 class AutoValueVector : public Rooted<GCVector<Value, 8>> {
     using Vec = GCVector<Value, 8>;
     using Base = Rooted<Vec>;
   public:
     explicit AutoValueVector(JSContext* cx) : Base(cx, Vec(cx)) {}
 };
 
 class AutoIdVector : public Rooted<GCVector<jsid, 8>> {
diff --git a/js/src/vm/JSCompartment.h b/js/src/vm/JSCompartment.h
--- a/js/src/vm/JSCompartment.h
+++ b/js/src/vm/JSCompartment.h
@@ -1379,26 +1379,30 @@ struct WrapperValue
     Value get() const { return value; }
     operator const Value&() const { return value; }
     JSObject& toObject() const { return value.toObject(); }
 
   private:
     Value value;
 };
 
-class MOZ_RAII AutoWrapperVector : public JS::AutoVectorRooterBase<WrapperValue>
+class MOZ_RAII AutoWrapperVector : public JS::GCVector<WrapperValue, 8>,
+                                   private JS::AutoGCRooter
 {
   public:
     explicit AutoWrapperVector(JSContext* cx
                                MOZ_GUARD_OBJECT_NOTIFIER_PARAM)
-        : AutoVectorRooterBase<WrapperValue>(cx, WRAPVECTOR)
+      : JS::GCVector<WrapperValue, 8>(cx),
+        JS::AutoGCRooter(cx, WRAPVECTOR)
     {
         MOZ_GUARD_OBJECT_NOTIFIER_INIT;
     }
 
+    friend void AutoGCRooter::trace(JSTracer* trc);
+
     MOZ_DECL_USE_GUARD_OBJECT_NOTIFIER
 };
 
 class MOZ_RAII AutoWrapperRooter : private JS::AutoGCRooter {
   public:
     AutoWrapperRooter(JSContext* cx, const WrapperValue& v
                       MOZ_GUARD_OBJECT_NOTIFIER_PARAM)
       : JS::AutoGCRooter(cx, WRAPPER), value(v)
