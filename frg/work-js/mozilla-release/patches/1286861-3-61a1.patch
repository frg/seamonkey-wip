# HG changeset patch
# User Christoph Kerschbaumer <ckerschb@christophkerschbaumer.com>
# Date 1523209988 -7200
# Node ID 525e7c6d62dead02bfd4887aee6ae9bc983939a0
# Parent  4d722ac90e3e11f2dff532a9ba3ddb7b9b7c9190
 Bug 1286861: Add tests for same site top-level. r=mgoodwin

diff --git a/dom/security/test/general/file_same_site_cookies_toplevel_nav.sjs b/dom/security/test/general/file_same_site_cookies_toplevel_nav.sjs
new file mode 100644
--- /dev/null
+++ b/dom/security/test/general/file_same_site_cookies_toplevel_nav.sjs
@@ -0,0 +1,87 @@
+// Custom *.sjs file specifically for the needs of Bug 1286861
+
+// small red image
+const IMG_BYTES = atob(
+  "iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12" +
+  "P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==");
+
+const FRAME = `
+  <!DOCTYPE html>
+  <html>
+  <head>
+    <title>Bug 1286861 - Add support for same site cookies</title>
+  </head>
+  <body>
+    <script type="application/javascript">
+      let myWin = window.open("http://mochi.test:8888/tests/dom/security/test/general/file_same_site_cookies_toplevel_nav.sjs?loadWin");
+      myWin.onload = function() {
+        myWin.close();
+      }
+    </script>
+  </body>
+  </html>`;
+
+const WIN = `
+  <!DOCTYPE html>
+  <html>
+  <body>
+    just a dummy window
+  </body>
+  </html>`;
+
+function handleRequest(request, response)
+{
+  // avoid confusing cache behaviors
+  response.setHeader("Cache-Control", "no-cache", false);
+
+  if (request.queryString === "setStrictSameSiteCookie") {
+    response.setHeader("Set-Cookie", "myKey=strictSameSiteCookie; samesite=strict", true);
+    response.setHeader("Content-Type", "image/png");
+    response.write(IMG_BYTES);
+    return;
+  }
+
+  if (request.queryString === "setLaxSameSiteCookie") {
+    response.setHeader("Set-Cookie", "myKey=laxSameSiteCookie; samesite=lax", true);
+    response.setHeader("Content-Type", "image/png");
+    response.write(IMG_BYTES);
+    return;
+  }
+
+  // save the object state of the initial request, which returns
+  // async once the server has processed the img request.
+  if (request.queryString === "queryresult") {
+    response.processAsync();
+    setObjectState("queryResult", response);
+    return;
+  }
+
+  if (request.queryString === "loadFrame") {
+    response.write(FRAME);
+    return;
+  }
+
+  if (request.queryString === "loadWin") {
+    var cookie = "unitialized";
+    if (request.hasHeader("Cookie")) {
+      cookie = request.getHeader("Cookie");
+    }
+    else {
+      cookie = "myKey=noCookie";
+    }
+    response.write(WIN);
+
+    // return the result
+    getObjectState("queryResult", function(queryResponse) {
+      if (!queryResponse) {
+        return;
+      }
+      queryResponse.write(cookie);
+      queryResponse.finish();
+    });
+    return;
+  }
+
+  // we should never get here, but just in case return something unexpected
+  response.write("D'oh");
+}
diff --git a/dom/security/test/general/mochitest.ini b/dom/security/test/general/mochitest.ini
--- a/dom/security/test/general/mochitest.ini
+++ b/dom/security/test/general/mochitest.ini
@@ -3,21 +3,23 @@ support-files =
   file_contentpolicytype_targeted_link_iframe.sjs
   file_nosniff_testserver.sjs
   file_block_script_wrong_mime_server.sjs
   file_block_toplevel_data_navigation.html
   file_block_toplevel_data_navigation2.html
   file_block_toplevel_data_navigation3.html
   file_block_toplevel_data_redirect.sjs
   file_same_site_cookies_subrequest.sjs
+  file_same_site_cookies_toplevel_nav.sjs
 
 [test_contentpolicytype_targeted_link_iframe.html]
 [test_nosniff.html]
 [test_block_script_wrong_mime.html]
 [test_block_toplevel_data_navigation.html]
 skip-if = toolkit == 'android' # intermittent failure
 [test_block_toplevel_data_img_navigation.html]
 skip-if = toolkit == 'android' # intermittent failure
 [test_allow_opening_data_pdf.html]
 skip-if = toolkit == 'android'
 [test_allow_opening_data_json.html]
 skip-if = toolkit == 'android'
 [test_same_site_cookies_subrequest.html]
+[test_same_site_cookies_toplevel_nav.html]
diff --git a/dom/security/test/general/test_same_site_cookies_toplevel_nav.html b/dom/security/test/general/test_same_site_cookies_toplevel_nav.html
new file mode 100644
--- /dev/null
+++ b/dom/security/test/general/test_same_site_cookies_toplevel_nav.html
@@ -0,0 +1,114 @@
+<!DOCTYPE HTML>
+<html>
+<head>
+  <title>Bug 1286861 - Test same site cookies on top-level navigations</title>
+  <script type="text/javascript" src="/tests/SimpleTest/SimpleTest.js"></script>
+  <link rel="stylesheet" type="text/css" href="/tests/SimpleTest/test.css" />
+</head>
+<body>
+<img id="cookieImage">
+<iframe id="testframe"></iframe>
+
+<script class="testbody" type="text/javascript">
+
+/*
+ * Description of the test:
+ * 1) We load an image from http://mochi.test which sets a same site cookie
+ * 2) We open a new window to
+ *    * a same origin location
+ *    * a cross origin location
+ * 3) We observe that the same site cookie is sent in the same origin case,
+ *    but not in the cross origin case, unless the policy = 'lax', which should
+ *    send the cookie in a top-level navigation case.
+ *
+ * In detail:
+ * We perform an XHR request to the *.sjs file which is processed async on
+ * the server and waits till the image request has been processed by the server.
+ * Once the image requets was processed, the server responds to the initial
+ * XHR request with the expecuted result (the cookie value).
+ */
+
+SimpleTest.waitForExplicitFinish();
+
+const SAME_ORIGIN = "http://mochi.test:8888/";
+const CROSS_ORIGIN = "http://example.com/";
+const PATH = "tests/dom/security/test/general/file_same_site_cookies_toplevel_nav.sjs";
+
+let curTest = 0;
+
+var tests = [
+  {
+    description: "same origin navigation using cookie policy 'samesite=strict'",
+    imgSRC: SAME_ORIGIN + PATH + "?setStrictSameSiteCookie",
+    frameSRC: SAME_ORIGIN + PATH + "?loadFrame",
+    result: "myKey=strictSameSiteCookie",
+  },
+  {
+    description: "cross origin navigation using cookie policy 'samesite=strict'",
+    imgSRC: SAME_ORIGIN + PATH + "?setStrictSameSiteCookie",
+    frameSRC: CROSS_ORIGIN + PATH + "?loadFrame",
+    result: "myKey=noCookie",
+  },
+  {
+    description: "same origin navigation using cookie policy 'samesite=lax'",
+    imgSRC: SAME_ORIGIN + PATH + "?setLaxSameSiteCookie",
+    frameSRC: SAME_ORIGIN + PATH + "?loadFrame",
+    result: "myKey=laxSameSiteCookie",
+  },
+  {
+    description: "cross origin navigation using cookie policy 'samesite=lax'",
+    imgSRC: SAME_ORIGIN + PATH + "?setLaxSameSiteCookie",
+    frameSRC: CROSS_ORIGIN + PATH + "?loadFrame",
+    result: "myKey=laxSameSiteCookie",
+  },
+];
+
+function checkResult(aCookieVal) {
+  is(aCookieVal, tests[curTest].result, tests[curTest].description);
+  curTest += 1;
+
+  // lets see if we ran all the tests
+  if (curTest == tests.length) {
+    SimpleTest.finish();
+    return;
+  }
+  // otherwise it's time to run the next test
+  setCookieAndInitTest();
+}
+
+function setupQueryResultAndRunTest() {
+  var myXHR = new XMLHttpRequest();
+  myXHR.open("GET", "file_same_site_cookies_toplevel_nav.sjs?queryresult");
+  myXHR.onload = function(e) {
+    checkResult(myXHR.responseText);
+  }
+  myXHR.onerror = function(e) {
+    ok(false, "could not query results from server (" + e.message + ")");
+  }
+  myXHR.send();
+
+  // give it some time and load the test window
+  SimpleTest.executeSoon(function() {
+    let testframe = document.getElementById("testframe");
+    testframe.src = tests[curTest].frameSRC;
+  });
+}
+
+function setCookieAndInitTest() {
+  var cookieImage = document.getElementById("cookieImage");
+  cookieImage.onload = function() {
+    ok(true, "set cookie for test (" + tests[curTest].description + ")");
+    setupQueryResultAndRunTest();
+  }
+  cookieImage.onerror = function() {
+    ok(false, "could not set cookie for test (" + tests[curTest].description + ")");
+  }
+  cookieImage.src = tests[curTest].imgSRC;
+}
+
+// fire up the test
+setCookieAndInitTest();
+
+</script>
+</body>
+</html>
