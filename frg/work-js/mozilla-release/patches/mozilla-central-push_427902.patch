# HG changeset patch
# User Brian Hackett <bhackett1024@gmail.com>
# Date 1532358013 0
#      Mon Jul 23 15:00:13 2018 +0000
# Node ID 636405b06158a43b7a4c16d1ed9772fc0f7288f1
# Parent  15e1646fafb92669732b05c7b148f6f29a29263f
Bug 1465470 Part 2 - Preserve iteration order for frontend name tables, r=jorendorff.

diff --git a/js/src/frontend/NameCollections.h b/js/src/frontend/NameCollections.h
--- a/js/src/frontend/NameCollections.h
+++ b/js/src/frontend/NameCollections.h
@@ -137,21 +137,33 @@ struct RecyclableAtomMapValueWrapper
         return &wrapped;
     }
 
     const Wrapped* operator->() const {
         return &wrapped;
     }
 };
 
+struct NameMapHasher : public DefaultHasher<JSAtom*>
+{
+    static inline HashNumber hash(const Lookup& l) {
+        // Name maps use the atom's precomputed hash code, which is based on
+        // the atom's contents rather than its pointer value. This is necessary
+        // to preserve iteration order while recording/replaying: iteration can
+        // affect generated script bytecode and the order in which e.g. lookup
+        // property hooks are performed on the associated global.
+        return l->hash();
+    }
+};
+
 template <typename MapValue>
 using RecyclableNameMap = InlineMap<JSAtom*,
                                     RecyclableAtomMapValueWrapper<MapValue>,
                                     24,
-                                    DefaultHasher<JSAtom*>,
+                                    NameMapHasher,
                                     SystemAllocPolicy>;
 
 using DeclaredNameMap = RecyclableNameMap<DeclaredNameInfo>;
 using CheckTDZMap = RecyclableNameMap<MaybeCheckTDZ>;
 using NameLocationMap = RecyclableNameMap<NameLocation>;
 using AtomIndexMap = RecyclableNameMap<uint32_t>;
 
 template <typename RepresentativeTable>
