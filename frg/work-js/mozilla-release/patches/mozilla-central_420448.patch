# HG changeset patch
# User Panos Astithas <past@mozilla.com>
# Date 1526427884 25200
#      Tue May 15 16:44:44 2018 -0700
# Node ID 314e3ae1520f8e1b17e85982e8e2c515dced30a9
# Parent  e8eb0005b50cbcd82856604682d8dfceb7be1ab7
Bug 1461851 - Properly add the source URL in the profiler metadata. r=glandium

MozReview-Commit-ID: 53M4bGolmJk

diff --git a/build/appini_header.py b/build/appini_header.py
--- a/build/appini_header.py
+++ b/build/appini_header.py
@@ -34,30 +34,37 @@ def main(output, file):
     if missing:
         print >>sys.stderr, \
             "Missing values in %s: %s" % (file, ', '.join(missing))
         sys.exit(1)
 
     if 'Crash Reporter:serverurl' not in appdata:
         appdata['Crash Reporter:serverurl'] = ''
 
+    if 'App:sourcerepository' in appdata and 'App:sourcestamp' in appdata:
+        appdata['App:sourceurl'] = '"%(App:sourcerepository)s/rev/%(App:sourcestamp)s"' % appdata
+    else:
+        appdata['App:sourceurl'] = 'NULL'
+
     output.write('''#include "mozilla/XREAppData.h"
              static const mozilla::StaticXREAppData sAppData = {
                  "%(App:vendor)s",
                  "%(App:name)s",
                  "%(App:remotingname)s",
                  "%(App:version)s",
                  "%(App:buildid)s",
                  "%(App:id)s",
                  NULL, // copyright
                  %(flags)s,
                  "%(Gecko:minversion)s",
                  "%(Gecko:maxversion)s",
                  "%(Crash Reporter:serverurl)s",
-                 %(App:profile)s
+                 %(App:profile)s,
+                 NULL, // UAName
+                 %(App:sourceurl)s
              };''' % appdata)
 
 
 if __name__ == '__main__':
     if len(sys.argv) != 1:
         main(sys.stdout, sys.argv[1])
     else:
         print >>sys.stderr, "Usage: %s /path/to/application.ini" % sys.argv[0]
diff --git a/dom/ipc/ContentChild.cpp b/dom/ipc/ContentChild.cpp
--- a/dom/ipc/ContentChild.cpp
+++ b/dom/ipc/ContentChild.cpp
@@ -2700,24 +2700,26 @@ ContentChild::RecvUnlinkGhosts()
   nsWindowMemoryReporter::UnlinkGhostWindows();
 #endif
   return IPC_OK();
 }
 
 mozilla::ipc::IPCResult
 ContentChild::RecvAppInfo(const nsCString& version, const nsCString& buildID,
                           const nsCString& name, const nsCString& UAName,
-                          const nsCString& ID, const nsCString& vendor)
+                          const nsCString& ID, const nsCString& vendor,
+                          const nsCString& sourceURL)
 {
   mAppInfo.version.Assign(version);
   mAppInfo.buildID.Assign(buildID);
   mAppInfo.name.Assign(name);
   mAppInfo.UAName.Assign(UAName);
   mAppInfo.ID.Assign(ID);
   mAppInfo.vendor.Assign(vendor);
+  mAppInfo.sourceURL.Assign(sourceURL);
 
   return IPC_OK();
 }
 
 mozilla::ipc::IPCResult
 ContentChild::RecvRemoteType(const nsString& aRemoteType)
 {
   MOZ_ASSERT(DOMStringIsNull(mRemoteType));
diff --git a/dom/ipc/ContentChild.h b/dom/ipc/ContentChild.h
--- a/dom/ipc/ContentChild.h
+++ b/dom/ipc/ContentChild.h
@@ -95,16 +95,17 @@ public:
   struct AppInfo
   {
     nsCString version;
     nsCString buildID;
     nsCString name;
     nsCString UAName;
     nsCString ID;
     nsCString vendor;
+    nsCString sourceURL;
   };
 
   nsresult
   ProvideWindowCommon(TabChild* aTabOpener,
                       mozIDOMWindowProxy* aOpener,
                       bool aIframeMoz,
                       uint32_t aChromeFlags,
                       bool aCalledFromJS,
@@ -413,17 +414,18 @@ public:
   virtual mozilla::ipc::IPCResult RecvShutdownA11y() override;
 
   virtual mozilla::ipc::IPCResult RecvGarbageCollect() override;
   virtual mozilla::ipc::IPCResult RecvCycleCollect() override;
   virtual mozilla::ipc::IPCResult RecvUnlinkGhosts() override;
 
   virtual mozilla::ipc::IPCResult RecvAppInfo(const nsCString& version, const nsCString& buildID,
                                               const nsCString& name, const nsCString& UAName,
-                                              const nsCString& ID, const nsCString& vendor) override;
+                                              const nsCString& ID, const nsCString& vendor,
+                                              const nsCString& sourceURL) override;
 
   virtual mozilla::ipc::IPCResult RecvRemoteType(const nsString& aRemoteType) override;
 
   const nsAString& GetRemoteType() const;
 
   virtual mozilla::ipc::IPCResult
   RecvInitServiceWorkers(const ServiceWorkerConfiguration& aConfig) override;
 
diff --git a/dom/ipc/ContentParent.cpp b/dom/ipc/ContentParent.cpp
--- a/dom/ipc/ContentParent.cpp
+++ b/dom/ipc/ContentParent.cpp
@@ -2314,19 +2314,20 @@ ContentParent::InitInternal(ProcessPrior
 
   if (gAppData) {
     nsCString version(gAppData->version);
     nsCString buildID(gAppData->buildID);
     nsCString name(gAppData->name);
     nsCString UAName(gAppData->UAName);
     nsCString ID(gAppData->ID);
     nsCString vendor(gAppData->vendor);
+    nsCString sourceURL(gAppData->sourceURL);
 
     // Sending all information to content process.
-    Unused << SendAppInfo(version, buildID, name, UAName, ID, vendor);
+    Unused << SendAppInfo(version, buildID, name, UAName, ID, vendor, sourceURL);
   }
 
   // Send the child its remote type. On Mac, this needs to be sent prior
   // to the message we send to enable the Sandbox (SendStartProcessSandbox)
   // because different remote types require different sandbox privileges.
   Unused << SendRemoteType(mRemoteType);
 
   ScriptPreloader::InitContentChild(*this);
diff --git a/dom/ipc/PContent.ipdl b/dom/ipc/PContent.ipdl
--- a/dom/ipc/PContent.ipdl
+++ b/dom/ipc/PContent.ipdl
@@ -475,17 +475,17 @@ child:
     async ActivateA11y(uint32_t aMainChromeTid, uint32_t aMsaaID);
 
     /**
      * Shutdown accessibility engine in content process (if not in use).
      */
     async ShutdownA11y();
 
     async AppInfo(nsCString version, nsCString buildID, nsCString name, nsCString UAName,
-                  nsCString ID, nsCString vendor);
+                  nsCString ID, nsCString vendor, nsCString sourceURL);
 
     /**
      * Send the remote type associated with the content process.
      */
     async RemoteType(nsString aRemoteType);
 
     /**
      * Send ServiceWorkerRegistrationData to child process.
diff --git a/toolkit/xre/nsAppRunner.cpp b/toolkit/xre/nsAppRunner.cpp
--- a/toolkit/xre/nsAppRunner.cpp
+++ b/toolkit/xre/nsAppRunner.cpp
@@ -711,16 +711,29 @@ nsXULAppInfo::GetUAName(nsACString& aRes
     return NS_OK;
   }
   aResult.Assign(gAppData->UAName);
 
   return NS_OK;
 }
 
 NS_IMETHODIMP
+nsXULAppInfo::GetSourceURL(nsACString& aResult)
+{
+  if (XRE_IsContentProcess()) {
+    ContentChild* cc = ContentChild::GetSingleton();
+    aResult = cc->GetAppInfo().sourceURL;
+    return NS_OK;
+  }
+  aResult.Assign(gAppData->sourceURL);
+
+  return NS_OK;
+}
+
+NS_IMETHODIMP
 nsXULAppInfo::GetLogConsoleErrors(bool *aResult)
 {
   *aResult = gLogConsoleErrors;
   return NS_OK;
 }
 
 NS_IMETHODIMP
 nsXULAppInfo::SetLogConsoleErrors(bool aValue)
diff --git a/tools/profiler/core/platform.cpp b/tools/profiler/core/platform.cpp
--- a/tools/profiler/core/platform.cpp
+++ b/tools/profiler/core/platform.cpp
@@ -1561,20 +1561,16 @@ StreamTaskTracer(PSLockRef aLock, Splice
 static void
 StreamMetaJSCustomObject(PSLockRef aLock, SpliceableJSONWriter& aWriter,
                          bool aIsShuttingDown)
 {
   MOZ_RELEASE_ASSERT(CorePS::Exists() && ActivePS::Exists(aLock));
 
   aWriter.IntProperty("version", 10);
 
-#if defined(MOZ_SOURCE_URL)
-  aWriter.StringProperty("sourceURL", "@MOZ_SOURCE_URL@");
-#endif
-
   // The "startTime" field holds the number of milliseconds since midnight
   // January 1, 1970 GMT. This grotty code computes (Now - (Now -
   // ProcessStartTime)) to convert CorePS::ProcessStartTime() into that form.
   TimeDuration delta = TimeStamp::Now() - CorePS::ProcessStartTime();
   aWriter.DoubleProperty(
     "startTime", static_cast<double>(PR_Now()/1000.0 - delta.ToMilliseconds()));
 
   // Write the shutdownTime field. Unlike startTime, shutdownTime is not an
@@ -1655,16 +1651,20 @@ StreamMetaJSCustomObject(PSLockRef aLock
     nsAutoCString string;
     res = appInfo->GetName(string);
     if (!NS_FAILED(res))
       aWriter.StringProperty("product", string.Data());
 
     res = appInfo->GetAppBuildID(string);
     if (!NS_FAILED(res))
       aWriter.StringProperty("appBuildID", string.Data());
+
+    res = appInfo->GetSourceURL(string);
+    if (!NS_FAILED(res))
+      aWriter.StringProperty("sourceURL", string.Data());
   }
 
   // We should avoid collecting extension metadata for profiler while XPCOM is
   // shutting down since it cannot create a new ExtensionPolicyService.
   if (!gXPCOMShuttingDown) {
     aWriter.StartObjectProperty("extensions");
     {
       {
diff --git a/xpcom/build/XREAppData.h b/xpcom/build/XREAppData.h
--- a/xpcom/build/XREAppData.h
+++ b/xpcom/build/XREAppData.h
@@ -183,16 +183,21 @@ public:
    */
   CharPtr profile;
 
   /**
    * The application name to use in the User Agent string.
    */
   CharPtr UAName;
 
+  /**
+   * The URL to the source revision for this build of the application.
+   */
+  CharPtr sourceURL;
+
 #if defined(XP_WIN) && defined(MOZ_SANDBOX)
   /**
    * Chromium sandbox BrokerServices.
    */
   sandbox::BrokerServices* sandboxBrokerServices = nullptr;
   mozilla::sandboxing::PermissionsService* sandboxPermissionsService;
 #endif
 };
@@ -224,13 +229,14 @@ struct StaticXREAppData
   const char* ID;
   const char* copyright;
   uint32_t flags;
   const char* minVersion;
   const char* maxVersion;
   const char* crashReporterURL;
   const char* profile;
   const char* UAName;
+  const char* sourceURL;
 };
 
 } // namespace mozilla
 
 #endif // XREAppData_h
diff --git a/xpcom/glue/XREAppData.cpp b/xpcom/glue/XREAppData.cpp
--- a/xpcom/glue/XREAppData.cpp
+++ b/xpcom/glue/XREAppData.cpp
@@ -20,16 +20,17 @@ XREAppData::operator=(const StaticXREApp
   ID = aOther.ID;
   copyright = aOther.copyright;
   flags = aOther.flags;
   minVersion = aOther.minVersion;
   maxVersion = aOther.maxVersion;
   crashReporterURL = aOther.crashReporterURL;
   profile = aOther.profile;
   UAName = aOther.UAName;
+  sourceURL = aOther.sourceURL;
 
   return *this;
 }
 
 XREAppData&
 XREAppData::operator=(const XREAppData& aOther)
 {
   directory = aOther.directory;
@@ -42,16 +43,17 @@ XREAppData::operator=(const XREAppData& 
   copyright = aOther.copyright;
   flags = aOther.flags;
   xreDirectory = aOther.xreDirectory;
   minVersion = aOther.minVersion;
   maxVersion = aOther.maxVersion;
   crashReporterURL = aOther.crashReporterURL;
   profile = aOther.profile;
   UAName = aOther.UAName;
+  sourceURL = aOther.sourceURL;
 #if defined(XP_WIN) && defined(MOZ_SANDBOX)
   sandboxBrokerServices = aOther.sandboxBrokerServices;
   sandboxPermissionsService = aOther.sandboxPermissionsService;
 #endif
   return *this;
 }
 
 } // namespace mozilla
diff --git a/xpcom/system/nsIXULAppInfo.idl b/xpcom/system/nsIXULAppInfo.idl
--- a/xpcom/system/nsIXULAppInfo.idl
+++ b/xpcom/system/nsIXULAppInfo.idl
@@ -45,9 +45,15 @@ interface nsIXULAppInfo : nsIPlatformInf
    */
   readonly attribute ACString appBuildID;
 
   /**
    * @see XREAppData.UAName
    * @returns an empty string if XREAppData.UAName is not set.
    */
   readonly attribute ACString UAName;
+
+  /**
+   * @see XREAppData.sourceURL
+   * @returns an empty string if XREAppData.sourceURL is not set.
+   */
+  readonly attribute ACString sourceURL;
 };
