# HG changeset patch
# User Gabriel Luong <gabriel.luong@gmail.com>
# Date 1517979056 18000
# Node ID 782e8d580a84b50bf92a968da58aaa20cb7f55e0
# Parent  73205039038f00f866370b02dadb6f57c7711fc3
Bug 1431900 - Add a display node to indicate the display value of an element in the markup view. r=pbro

diff --git a/devtools/client/inspector/markup/markup.js b/devtools/client/inspector/markup/markup.js
--- a/devtools/client/inspector/markup/markup.js
+++ b/devtools/client/inspector/markup/markup.js
@@ -1010,19 +1010,16 @@ MarkupView.prototype = {
 
       let container = this.getContainer(target);
       if (!container) {
         // Container might not exist if this came from a load event for a node
         // we're not viewing.
         continue;
       }
 
-      if (type === "attributes" && mutation.attributeName === "class") {
-        container.updateIsDisplayed();
-      }
       if (type === "attributes" || type === "characterData"
         || type === "events" || type === "pseudoClassLock") {
         container.update();
       } else if (type === "childList" || type === "nativeAnonymousChildList") {
         container.childrenDirty = true;
         // Update the children to take care of changes in the markup view DOM
         // and update container (and its subtree) DOM tree depth level for
         // accessibility where necessary.
@@ -1057,17 +1054,17 @@ MarkupView.prototype = {
    *
    * @param  {Array} nodes
    *         An array of nodeFronts
    */
   _onDisplayChange: function (nodes) {
     for (let node of nodes) {
       let container = this.getContainer(node);
       if (container) {
-        container.updateIsDisplayed();
+        container.update();
       }
     }
   },
 
   /**
    * Given a list of mutations returned by the mutation observer, flash the
    * corresponding containers to attract attention.
    */
diff --git a/devtools/client/inspector/markup/test/browser.ini b/devtools/client/inspector/markup/test/browser.ini
--- a/devtools/client/inspector/markup/test/browser.ini
+++ b/devtools/client/inspector/markup/test/browser.ini
@@ -90,16 +90,18 @@ skip-if = e10s # scratchpad.xul is not l
 skip-if = stylo # Stylo doesn't support shadow DOM yet, bug 1293844
 [browser_markup_anonymous_04.js]
 [browser_markup_copy_image_data.js]
 subsuite = clipboard
 skip-if = (os == 'linux' && bits == 32 && debug) # bug 1328915, disable linux32 debug devtools for timeouts
 [browser_markup_css_completion_style_attribute_01.js]
 [browser_markup_css_completion_style_attribute_02.js]
 [browser_markup_css_completion_style_attribute_03.js]
+[browser_markup_display_node_01.js]
+[browser_markup_display_node_02.js]
 [browser_markup_dragdrop_autoscroll_01.js]
 [browser_markup_dragdrop_autoscroll_02.js]
 [browser_markup_dragdrop_distance.js]
 [browser_markup_dragdrop_draggable.js]
 [browser_markup_dragdrop_dragRootNode.js]
 [browser_markup_dragdrop_escapeKeyPress.js]
 [browser_markup_dragdrop_invalidNodes.js]
 [browser_markup_dragdrop_reorder.js]
diff --git a/devtools/client/inspector/markup/test/browser_markup_display_node_01.js b/devtools/client/inspector/markup/test/browser_markup_display_node_01.js
new file mode 100644
--- /dev/null
+++ b/devtools/client/inspector/markup/test/browser_markup_display_node_01.js
@@ -0,0 +1,58 @@
+/* vim: set ts=2 et sw=2 tw=80: */
+/* Any copyright is dedicated to the Public Domain.
+ http://creativecommons.org/publicdomain/zero/1.0/ */
+
+"use strict";
+
+// Tests that markup display node shows for only for grid and flex containers.
+
+const TEST_URI = `
+  <style type="text/css">
+    #grid {
+      display: grid;
+    }
+    #flex {
+      display: flex;
+    }
+    #block {
+      display: block;
+    }
+  </style>
+  <div id="grid">Grid</div>
+  <div id="flex">Flex</div>
+  <div id="block">Block</div>
+  <span>HELLO WORLD</span>
+`;
+
+add_task(function* () {
+  let {inspector} = yield openInspectorForURL("data:text/html;charset=utf-8," +
+    encodeURIComponent(TEST_URI));
+
+  info("Check the display node is shown and the value of #grid.");
+  yield selectNode("#grid", inspector);
+  let gridContainer = yield getContainerForSelector("#grid", inspector);
+  let gridDisplayNode = gridContainer.elt.querySelector(".markupview-display");
+  is(gridDisplayNode.textContent, "grid", "Got the correct display type for #grid.");
+  is(gridDisplayNode.style.display, "inline-block", "#grid display node is shown.");
+
+  info("Check the display node is shown and the value of #flex.");
+  yield selectNode("#flex", inspector);
+  let flexContainer = yield getContainerForSelector("#flex", inspector);
+  let flexDisplayNode = flexContainer.elt.querySelector(".markupview-display");
+  is(flexDisplayNode.textContent, "flex", "Got the correct display type for #flex");
+  is(flexDisplayNode.style.display, "inline-block", "#flex display node is shown.");
+
+  info("Check the display node is shown and the value of #block.");
+  yield selectNode("#block", inspector);
+  let blockContainer = yield getContainerForSelector("#block", inspector);
+  let blockDisplayNode = blockContainer.elt.querySelector(".markupview-display");
+  is(blockDisplayNode.textContent, "block", "Got the correct display type for #block");
+  is(blockDisplayNode.style.display, "none", "#block display node is hidden.");
+
+  info("Check the display node is shown and the value of span.");
+  yield selectNode("span", inspector);
+  let spanContainer = yield getContainerForSelector("span", inspector);
+  let spanDisplayNode = spanContainer.elt.querySelector(".markupview-display");
+  is(spanDisplayNode.textContent, "inline", "Got the correct display type for #span");
+  is(spanDisplayNode.style.display, "none", "span display node is hidden.");
+});
diff --git a/devtools/client/inspector/markup/test/browser_markup_display_node_02.js b/devtools/client/inspector/markup/test/browser_markup_display_node_02.js
new file mode 100644
--- /dev/null
+++ b/devtools/client/inspector/markup/test/browser_markup_display_node_02.js
@@ -0,0 +1,129 @@
+/* vim: set ts=2 et sw=2 tw=80: */
+/* Any copyright is dedicated to the Public Domain.
+ http://creativecommons.org/publicdomain/zero/1.0/ */
+
+"use strict";
+
+// Tests that markup display node are updated when their display changes.
+
+const TEST_URI = `
+  <style type="text/css">
+    #grid {
+      display: grid;
+    }
+    #flex {
+      display: flex;
+    }
+    #flex[hidden] {
+      display: none;
+    }
+    #block {
+      display: block;
+    }
+    #flex
+  </style>
+  <div id="grid">Grid</div>
+  <div id="flex" hidden="">Flex</div>
+  <div id="block">Block</div>
+`;
+
+const TEST_DATA = [
+  {
+    desc: "Hiding the #grid display node by changing its style property",
+    selector: "#grid",
+    before: {
+      textContent: "grid",
+      display: "inline-block"
+    },
+    changeStyle: function* (testActor) {
+      yield testActor.eval(`
+        let node = document.getElementById("grid");
+        node.style.display = "block";
+      `);
+    },
+    after: {
+      textContent: "block",
+      display: "none"
+    }
+  },
+  {
+    desc: "Showing a 'grid' node by changing its style property",
+    selector: "#block",
+    before: {
+      textContent: "block",
+      display: "none"
+    },
+    changeStyle: function* (testActor) {
+      yield testActor.eval(`
+        let node = document.getElementById("block");
+        node.style.display = "grid";
+      `);
+    },
+    after: {
+      textContent: "grid",
+      display: "inline-block"
+    }
+  },
+  {
+    desc: "Showing a 'flex' node by removing its hidden attribute",
+    selector: "#flex",
+    before: {
+      textContent: "none",
+      display: "none"
+    },
+    changeStyle: function* (testActor) {
+      yield testActor.eval(`
+        document.getElementById("flex").removeAttribute("hidden");
+      `);
+    },
+    after: {
+      textContent: "flex",
+      display: "inline-block"
+    }
+  }
+];
+
+add_task(function* () {
+  let {inspector, testActor} = yield openInspectorForURL("data:text/html;charset=utf-8," +
+    encodeURIComponent(TEST_URI));
+
+  for (let data of TEST_DATA) {
+    info("Running test case: " + data.desc);
+    yield runTestData(inspector, testActor, data);
+  }
+});
+
+function* runTestData(inspector, testActor,
+                      {selector, before, changeStyle, after}) {
+  yield selectNode(selector, inspector);
+  let container = yield getContainerForSelector(selector, inspector);
+  let displayNode = container.elt.querySelector(".markupview-display");
+
+  is(displayNode.textContent, before.textContent,
+    `Got the correct before display type for ${selector}: ${displayNode.textContent}`);
+  is(displayNode.style.display, before.display,
+    `Got the correct before display style for ${selector}: ${displayNode.style.display}`);
+
+  info("Listening for the display-change event");
+  let onDisplayChanged = inspector.markup.walker.once("display-change");
+  info("Making style changes");
+  yield changeStyle(testActor);
+  let nodes = yield onDisplayChanged;
+
+  info("Verifying that the list of changed nodes include our container");
+  ok(nodes.length, "The display-change event was received with a nodes");
+  let foundContainer = false;
+  for (let node of nodes) {
+    if (getContainerForNodeFront(node, inspector) === container) {
+      foundContainer = true;
+      break;
+    }
+  }
+  ok(foundContainer, "Container is part of the list of changed nodes");
+
+  is(displayNode.textContent, after.textContent,
+    `Got the correct after display type for ${selector}: ${displayNode.textContent}`);
+  is(displayNode.style.display, after.display,
+    `Got the correct after display style for ${selector}: ${displayNode.style.display}`);
+}
+
diff --git a/devtools/client/inspector/markup/views/element-editor.js b/devtools/client/inspector/markup/views/element-editor.js
--- a/devtools/client/inspector/markup/views/element-editor.js
+++ b/devtools/client/inspector/markup/views/element-editor.js
@@ -14,30 +14,42 @@ const {
   truncateString,
 } = require("devtools/client/inspector/markup/utils");
 const {editableField, InplaceEditor} =
       require("devtools/client/shared/inplace-editor");
 const {parseAttribute} =
       require("devtools/client/shared/node-attribute-parser");
 const {getCssProperties} = require("devtools/shared/fronts/css-properties");
 
+// Global tooltip inspector
+const {LocalizationHelper} = require("devtools/shared/l10n");
+const INSPECTOR_L10N =
+  new LocalizationHelper("devtools/client/locales/inspector.properties");
+
 // Page size for pageup/pagedown
 const COLLAPSE_DATA_URL_REGEX = /^data.+base64/;
 const COLLAPSE_DATA_URL_LENGTH = 60;
 
 // Contains only void (without end tag) HTML elements
 const HTML_VOID_ELEMENTS = [
   "area", "base", "br", "col", "command", "embed",
   "hr", "img", "input", "keygen", "link", "meta", "param", "source",
-  "track", "wbr" ];
+  "track", "wbr"
+];
 
-// Global tooltip inspector
-const {LocalizationHelper} = require("devtools/shared/l10n");
-const INSPECTOR_L10N =
-  new LocalizationHelper("devtools/client/locales/inspector.properties");
+// Contains only valid computed display property types of the node to display in the
+// element markup and their respective title tooltip text.
+const DISPLAY_TYPES = {
+  "flex": INSPECTOR_L10N.getStr("markupView.display.flex.tooltiptext"),
+  "inline-flex": INSPECTOR_L10N.getStr("markupView.display.flex.tooltiptext"),
+  "grid": INSPECTOR_L10N.getStr("markupView.display.grid.tooltiptext"),
+  "inline-grid": INSPECTOR_L10N.getStr("markupView.display.inlineGrid.tooltiptext"),
+  "flow-root": INSPECTOR_L10N.getStr("markupView.display.flowRoot.tooltiptext"),
+  "contents": INSPECTOR_L10N.getStr("markupView.display.contents.tooltiptext"),
+};
 
 /**
  * Creates an editor for an Element node.
  *
  * @param  {MarkupContainer} container
  *         The container owning this editor.
  * @param  {Element} node
  *         The node being edited.
@@ -157,16 +169,20 @@ ElementEditor.prototype = {
     close.appendChild(this.doc.createTextNode(">"));
 
     this.eventNode = this.doc.createElement("div");
     this.eventNode.classList.add("markupview-events");
     this.eventNode.dataset.event = "true";
     this.eventNode.textContent = "ev";
     this.eventNode.title = INSPECTOR_L10N.getStr("markupView.event.tooltiptext");
     this.elt.appendChild(this.eventNode);
+
+    this.displayNode = this.doc.createElement("div");
+    this.displayNode.classList.add("markupview-display");
+    this.elt.appendChild(this.displayNode);
   },
 
   set selected(value) {
     if (this.textEditor) {
       this.textEditor.selected = value;
     }
   },
 
@@ -248,18 +264,24 @@ ElementEditor.prototype = {
         // been created.
         if (this.initialized) {
           this.flashAttribute(attr.name);
         }
       }
     }
 
     // Update the event bubble display
-    this.eventNode.style.display = this.node.hasEventListeners ?
-      "inline-block" : "none";
+    this.eventNode.style.display = this.node.hasEventListeners ? "inline-block" : "none";
+
+    // Update the display type node
+    let showDisplayNode = this.node.displayType in DISPLAY_TYPES;
+    this.displayNode.textContent = this.node.displayType;
+    this.displayNode.dataset.display = showDisplayNode ? this.node.displayType : "";
+    this.displayNode.style.display = showDisplayNode ? "inline-block" : "none";
+    this.displayNode.title = showDisplayNode ? DISPLAY_TYPES[this.node.displayType] : "";
 
     this.updateTextEditor();
   },
 
   /**
    * Update the inline text editor in case of a single text child node.
    */
   updateTextEditor: function () {
diff --git a/devtools/client/inspector/markup/views/markup-container.js b/devtools/client/inspector/markup/views/markup-container.js
--- a/devtools/client/inspector/markup/views/markup-container.js
+++ b/devtools/client/inspector/markup/views/markup-container.js
@@ -713,16 +713,18 @@ MarkupContainer.prototype = {
    */
   update: function () {
     if (this.node.pseudoClassLocks.length) {
       this.elt.classList.add("pseudoclass-locked");
     } else {
       this.elt.classList.remove("pseudoclass-locked");
     }
 
+    this.updateIsDisplayed();
+
     if (this.editor.update) {
       this.editor.update();
     }
   },
 
   /**
    * Try to put keyboard focus on the current editor.
    */
@@ -730,18 +732,19 @@ MarkupContainer.prototype = {
     // Elements with tabindex of -1 are not focusable.
     let focusable = this.editor.elt.querySelector("[tabindex='0']");
     if (focusable) {
       focusable.focus();
     }
   },
 
   _onToggle: function (event) {
-    // Prevent the html tree from expanding when an event bubble is clicked.
-    if (event.target.dataset.event) {
+    // Prevent the html tree from expanding when an event bubble or display node is
+    // clicked.
+    if (event.target.dataset.event || event.target.dataset.display) {
       event.stopPropagation();
       return;
     }
 
     this.markup.navigate(this);
     if (this.hasChildren) {
       this.markup.setNodeExpanded(this.node, !this.expanded, event.altKey);
     }
diff --git a/devtools/client/locales/en-US/inspector.properties b/devtools/client/locales/en-US/inspector.properties
--- a/devtools/client/locales/en-US/inspector.properties
+++ b/devtools/client/locales/en-US/inspector.properties
@@ -33,19 +33,49 @@ markupView.more.showing=Some nodes were 
 # See: http://developer.mozilla.org/en/docs/Localization_and_Plurals
 markupView.more.showAll2=Show one more node;Show all #1 nodes
 
 # LOCALIZATION NOTE (markupView.whitespaceOnly)
 # Used in a tooltip that appears when the user hovers over whitespace-only text nodes in
 # the inspector.
 markupView.whitespaceOnly=Whitespace-only text node: %S
 
+# LOCALIZATION NOTE (markupView.display.flex.tooltiptext)
+# Used in a tooltip that appears when the user hovers over the display type button in
+# the markup view.
+markupView.display.flex.tooltiptext=This element behaves like a block element and lays out its content according to the flexbox model.
+
+# LOCALIZATION NOTE (markupView.display.inlineFlex.tooltiptext)
+# Used in a tooltip that appears when the user hovers over the display type button in
+# the markup view.
+markupView.display.inlineFlex.tooltiptext=This element behaves like an inline element and lays out its content according to the flexbox model.
+
+# LOCALIZATION NOTE (markupView.display.grid.tooltiptext)
+# Used in a tooltip that appears when the user hovers over the display type button in
+# the markup view.
+markupView.display.grid.tooltiptext=This element behaves like a block element and lays out its content according to the grid model.
+
+# LOCALIZATION NOTE (markupView.display.flex.tooltiptext)
+# Used in a tooltip that appears when the user hovers over the display type button in
+# the markup view.
+markupView.display.inlineGrid.tooltiptext=This element behaves like an inline element and lays out its content according to the grid model.
+
+# LOCALIZATION NOTE (markupView.display.flowRoot.tooltiptext)
+# Used in a tooltip that appears when the user hovers over the display type button in
+# the markup view.
+markupView.display.flowRoot.tooltiptext=This element generates a block element box that establishes a new block formatting context.
+
+# LOCALIZATION NOTE (markupView.display.contents.tooltiptext)
+# Used in a tooltip that appears when the user hovers over the display type button in
+# the markup view.
+markupView.display.contents.tooltiptext=This element doesn’t produce a specific box by themselves, but renders its contents.
+
 # LOCALIZATION NOTE (markupView.event.tooltiptext)
 # Used in a tooltip that appears when the user hovers over 'ev' button in
-# the inspector.
+# the markup view.
 markupView.event.tooltiptext=Event listener
 
 #LOCALIZATION NOTE: Used in the image preview tooltip when the image could not be loaded
 previewTooltip.image.brokenImage=Could not load the image
 
 # LOCALIZATION NOTE: Used in color picker tooltip when the eyedropper is disabled for
 # non-HTML documents
 eyedropper.disabled.title=Unavailable in non-HTML documents
diff --git a/devtools/client/themes/markup.css b/devtools/client/themes/markup.css
--- a/devtools/client/themes/markup.css
+++ b/devtools/client/themes/markup.css
@@ -258,21 +258,16 @@ ul.children + .tag-line::before {
 .newattr:focus {
   margin-right: 0;
 }
 
 .flash-out {
   transition: background .5s;
 }
 
-.markupview-events {
-  display: none;
-  cursor: pointer;
-}
-
 .editor {
   /* Make sure the editor still appears above the tag-state */
   position: relative;
   z-index: 1;
 }
 
 .editor.comment:not(.doctype) {
   color: var(--theme-highlight-green);
@@ -368,16 +363,17 @@ ul.children + .tag-line::before {
 .theme-firebug .attr-name.theme-fg-color2 {
   color: var(--theme-highlight-purple);
 }
 
 .theme-firebug .attr-value.theme-fg-color6 {
   color: var(--theme-highlight-red);
 }
 
+.theme-firebug .markupview-display,
 .theme-firebug .markupview-events {
   font-size: var(--theme-toolbar-font-size);
 }
 
 /* In case a node isn't displayed in the page, we fade the syntax highlighting */
 .theme-firebug .not-displayed .open,
 .theme-firebug .not-displayed .close {
   opacity: .5;
@@ -409,23 +405,26 @@ ul.children + .tag-line::before {
 .doctype {
   font-style: italic;
 }
 
 .theme-firebug .doctype {
   color: #787878;
 }
 
-/* Events */
+/* Display and Events Bubble */
+.markupview-display,
 .markupview-events {
+  display: none;
   font-size: 8px;
   font-weight: bold;
   line-height: 10px;
   border-radius: 3px;
   padding: 0px 2px;
   margin-inline-start: 5px;
   -moz-user-select: none;
+  background-color: var(--theme-body-color-alt);
+  color: var(--theme-body-background);
 }
 
 .markupview-events {
-  background-color: var(--theme-body-color-alt);
-  color: var(--theme-body-background);
+  cursor: pointer;
 }
diff --git a/devtools/server/actors/inspector/node-actor.js b/devtools/server/actors/inspector/node-actor.js
--- a/devtools/server/actors/inspector/node-actor.js
+++ b/devtools/server/actors/inspector/node-actor.js
@@ -37,18 +37,19 @@ const FONT_FAMILY_PREVIEW_TEXT_SIZE = 20
  */
 const NodeActor = protocol.ActorClassWithSpec(nodeSpec, {
   initialize: function (walker, node) {
     protocol.Actor.prototype.initialize.call(this, null);
     this.walker = walker;
     this.rawNode = node;
     this._eventParsers = new EventParsers().parsers;
 
-    // Storing the original display of the node, to track changes when reflows
-    // occur
+    // Store the original display type and whether or not the node is displayed to
+    // track changes when reflows occur.
+    this.currentDisplayType = this.displayType;
     this.wasDisplayed = this.isDisplayed;
   },
 
   toString: function () {
     return "[NodeActor " + this.actorID + " for " +
       this.rawNode.toString() + "]";
   },
 
@@ -93,16 +94,17 @@ const NodeActor = protocol.ActorClassWit
       parent: parentNode ? parentNode.actorID : undefined,
       nodeType: this.rawNode.nodeType,
       namespaceURI: this.rawNode.namespaceURI,
       nodeName: this.rawNode.nodeName,
       nodeValue: this.rawNode.nodeValue,
       displayName: InspectorActorUtils.getNodeDisplayName(this.rawNode),
       numChildren: this.numChildren,
       inlineTextChild: inlineTextChild ? inlineTextChild.form() : undefined,
+      displayType: this.displayType,
 
       // doctype attributes
       name: this.rawNode.name,
       publicId: this.rawNode.publicId,
       systemId: this.rawNode.systemId,
 
       attrs: this.writeAttrs(),
       isBeforePseudoElement: this.isBeforePseudoElement,
@@ -197,28 +199,49 @@ const NodeActor = protocol.ActorClassWit
     if (numChildren === 0 || hasAnonChildren) {
       numChildren = this.walker.children(this).nodes.length;
     }
 
     return numChildren;
   },
 
   get computedStyle() {
-    return CssLogic.getComputedStyle(this.rawNode);
+    if (!this._computedStyle) {
+      this._computedStyle = CssLogic.getComputedStyle(this.rawNode);
+    }
+    return this._computedStyle;
+  },
+
+  /**
+   * Returns the computed display style property value of the node.
+   */
+  get displayType() {
+    // Consider all non-element nodes as displayed.
+    if (InspectorActorUtils.isNodeDead(this) ||
+        this.rawNode.nodeType !== Ci.nsIDOMNode.ELEMENT_NODE ||
+        this.isAfterPseudoElement ||
+        this.isBeforePseudoElement) {
+      return null;
+    }
+
+    let style = this.computedStyle;
+    if (!style) {
+      return null;
+    }
+
+    return style.display;
   },
 
   /**
    * Is the node's display computed style value other than "none"
    */
   get isDisplayed() {
     // Consider all non-element nodes as displayed.
     if (InspectorActorUtils.isNodeDead(this) ||
-        this.rawNode.nodeType !== Ci.nsIDOMNode.ELEMENT_NODE ||
-        this.isAfterPseudoElement ||
-        this.isBeforePseudoElement) {
+        this.rawNode.nodeType !== Ci.nsIDOMNode.ELEMENT_NODE) {
       return true;
     }
 
     let style = this.computedStyle;
     if (!style) {
       return true;
     }
 
diff --git a/devtools/server/actors/inspector/walker-actor.js b/devtools/server/actors/inspector/walker-actor.js
--- a/devtools/server/actors/inspector/walker-actor.js
+++ b/devtools/server/actors/inspector/walker-actor.js
@@ -305,20 +305,25 @@ var WalkerActor = protocol.ActorClassWit
     // Going through the nodes the walker knows about, see which ones have
     // had their display changed and send a display-change event if any
     let changes = [];
     for (let [node, actor] of this._refMap) {
       if (Cu.isDeadWrapper(node)) {
         continue;
       }
 
+      let displayType = actor.displayType;
       let isDisplayed = actor.isDisplayed;
-      if (isDisplayed !== actor.wasDisplayed) {
+
+      if (displayType !== actor.currentDisplayType ||
+          isDisplayed !== actor.wasDisplayed) {
         changes.push(actor);
+
         // Updating the original value
+        actor.currentDisplayType = displayType;
         actor.wasDisplayed = isDisplayed;
       }
     }
 
     if (changes.length) {
       this.emit("display-change", changes);
     }
   },
diff --git a/devtools/server/tests/mochitest/chrome.ini b/devtools/server/tests/mochitest/chrome.ini
--- a/devtools/server/tests/mochitest/chrome.ini
+++ b/devtools/server/tests/mochitest/chrome.ini
@@ -6,16 +6,17 @@ support-files =
   doc_Debugger.Source.prototype.introductionType.xul
   Debugger.Source.prototype.element.js
   Debugger.Source.prototype.element-2.js
   Debugger.Source.prototype.element.html
   hello-actor.js
   iframe1_makeGlobalObjectReference.html
   iframe2_makeGlobalObjectReference.html
   inspector_css-properties.html
+  inspector_display-type.html
   inspector_getImageData.html
   inspector_getOffsetParent.html
   inspector-delay-image-response.sjs
   inspector-eyedropper.html
   inspector-helpers.js
   inspector-search-data.html
   inspector-styles-data.css
   inspector-styles-data.html
@@ -48,16 +49,17 @@ support-files =
 [test_framerate_05.html]
 [test_framerate_06.html]
 [test_getProcess.html]
 [test_highlighter_paused_debugger.html]
 [test_inspector-anonymous.html]
 [test_inspector-changeattrs.html]
 [test_inspector-changevalue.html]
 [test_inspector-dead-nodes.html]
+[test_inspector-display-type.html]
 [test_inspector-duplicate-node.html]
 [test_inspector_getImageData.html]
 [test_inspector_getImageDataFromURL.html]
 [test_inspector_getImageData-wait-for-load.html]
 [test_inspector_getNodeFromActor.html]
 [test_inspector_getOffsetParent.html]
 [test_inspector-hide.html]
 [test_inspector-insert.html]
diff --git a/devtools/server/tests/mochitest/inspector_display-type.html b/devtools/server/tests/mochitest/inspector_display-type.html
new file mode 100644
--- /dev/null
+++ b/devtools/server/tests/mochitest/inspector_display-type.html
@@ -0,0 +1,17 @@
+<html>
+<head>
+<body>
+    <div id="inline-block" style="display: inline-block">
+      HELLO WORLD
+    </div>
+    <div id="grid" style="display: grid"></div>
+    <div id="block" style="position: fixed"></div>
+    <script>
+       "use strict";
+
+       window.onload = () => {
+         window.opener.postMessage("ready", "*");
+       };
+     </script>
+</body>
+</html>
diff --git a/devtools/server/tests/mochitest/test_inspector-display-type.html b/devtools/server/tests/mochitest/test_inspector-display-type.html
new file mode 100644
--- /dev/null
+++ b/devtools/server/tests/mochitest/test_inspector-display-type.html
@@ -0,0 +1,85 @@
+<!DOCTYPE HTML>
+<html>
+<!--
+https://bugzilla.mozilla.org/show_bug.cgi?id=1431900
+-->
+<head>
+  <meta charset="utf-8">
+  <title>Test for Bug 1431900</title>
+
+  <script type="application/javascript" src="chrome://mochikit/content/tests/SimpleTest/SimpleTest.js"></script>
+  <link rel="stylesheet" type="text/css" href="chrome://mochikit/content/tests/SimpleTest/test.css">
+  <script type="application/javascript" src="inspector-helpers.js"></script>
+  <script type="application/javascript">
+"use strict";
+
+window.onload = function () {
+  SimpleTest.waitForExplicitFinish();
+  runNextTest();
+};
+
+var gWalker;
+
+addTest(function setup() {
+  let url = document.getElementById("inspectorContent").href;
+  attachURL(url, function (err, client, tab, doc) {
+    let {InspectorFront} = require("devtools/shared/fronts/inspector");
+    let inspector = InspectorFront(client, tab);
+
+    promiseDone(inspector.getWalker().then(walker => {
+      gWalker = walker;
+    }).then(runNextTest));
+  });
+});
+
+addAsyncTest(function* testInlineBlockDisplayType() {
+  info("Test getting the display type of an inline block element.");
+  let node = yield gWalker.querySelector(gWalker.rootNode, "#inline-block");
+  let displayType = node.displayType;
+  is(displayType, "inline-block", "The node has a display type of 'inline-block'.");
+  runNextTest();
+});
+
+addAsyncTest(function* testInlineTextChildDisplayType() {
+  info("Test getting the display type of an inline text child.");
+  let node = yield gWalker.querySelector(gWalker.rootNode, "#inline-block");
+  let children = yield gWalker.children(node);
+  let inlineTextChild = children.nodes[0];
+  let displayType = inlineTextChild.displayType;
+  ok(!displayType, "No display type for inline text child.");
+  runNextTest();
+});
+
+addAsyncTest(function* testGridDisplayType() {
+  info("Test getting the display type of an grid container.");
+  let node = yield gWalker.querySelector(gWalker.rootNode, "#grid");
+  let displayType = node.displayType;
+  is(displayType, "grid", "The node has a display type of 'grid'.");
+  runNextTest();
+});
+
+addAsyncTest(function* testBlockDisplayType() {
+  info("Test getting the display type of a block element.");
+  let node = yield gWalker.querySelector(gWalker.rootNode, "#block");
+  let displayType = yield node.displayType;
+  is(displayType, "block", "The node has a display type of 'block'.");
+  runNextTest();
+});
+
+addTest(function () {
+  gWalker = null;
+  runNextTest();
+});
+  </script>
+</head>
+<body>
+<a target="_blank" href="https://bugzilla.mozilla.org/show_bug.cgi?id=1431900">Mozilla Bug 1431900</a>
+<a id="inspectorContent" target="_blank" href="inspector_display-type.html">Test Document</a>
+<p id="display"></p>
+<div id="content" style="display: none">
+
+</div>
+<pre id="test">
+</pre>
+</body>
+</html>
diff --git a/devtools/shared/fronts/node.js b/devtools/shared/fronts/node.js
--- a/devtools/shared/fronts/node.js
+++ b/devtools/shared/fronts/node.js
@@ -326,16 +326,20 @@ const NodeFront = FrontClassWithSpec(nod
 
   get pseudoClassLocks() {
     return this._form.pseudoClassLocks || [];
   },
   hasPseudoClassLock: function (pseudo) {
     return this.pseudoClassLocks.some(locked => locked === pseudo);
   },
 
+  get displayType() {
+    return this._form.displayType;
+  },
+
   get isDisplayed() {
     // The NodeActor's form contains the isDisplayed information as a boolean
     // starting from FF32. Before that, the property is missing
     return "isDisplayed" in this._form ? this._form.isDisplayed : true;
   },
 
   get isTreeDisplayed() {
     let parent = this;
