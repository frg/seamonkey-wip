# HG changeset patch
# User Masayuki Nakano <masayuki@d-toybox.com>
# Date 1516277566 -32400
# Node ID c27c6e2b049402fdbb99281aace76cba667a6926
# Parent  0a958974d2dee7b8b1fc19c32d88cfe776f635ad
Bug 1430982 - part 7: Make mozInlineSpellChecker store EditorSpellCheck directly rather than nsIEditorSpellCheck r=m_kato

mozInlienSpellChecker::mSpellCheck and mozInlineSpellChecker::mPendingSpellCheck
are always instances of EditorSpellCheck because mozInlienSpellChecker creates
its instance.  Therefore, making mozInlineSpellChecker store EditorSpellCheck
directly makes sense.

MozReview-Commit-ID: LTtEMZqMS9E

diff --git a/extensions/spellcheck/src/mozInlineSpellChecker.cpp b/extensions/spellcheck/src/mozInlineSpellChecker.cpp
--- a/extensions/spellcheck/src/mozInlineSpellChecker.cpp
+++ b/extensions/spellcheck/src/mozInlineSpellChecker.cpp
@@ -30,16 +30,17 @@
  * exit if something has changed. If we navigate inside the word, we don't want
  * to do anything. As a result, this flag is cleared in FinishNavigationEvent
  * when we know that we are checking as a result of navigation.
  */
 
 #include "mozInlineSpellChecker.h"
 
 #include "mozilla/EditAction.h"
+#include "mozilla/EditorSpellCheck.h"
 #include "mozilla/EditorUtils.h"
 #include "mozilla/Services.h"
 #include "mozilla/TextEditor.h"
 #include "mozilla/dom/Selection.h"
 #include "mozInlineSpellWordUtil.h"
 #include "mozISpellI18NManager.h"
 #include "nsCOMPtr.h"
 #include "nsCRT.h"
@@ -758,34 +759,26 @@ mozInlineSpellChecker::SetEnableRealTime
     return SpellCheckRange(nullptr);
   }
 
   if (mPendingSpellCheck) {
     // The editor spell checker is already being initialized.
     return NS_OK;
   }
 
-  mPendingSpellCheck =
-    do_CreateInstance("@mozilla.org/editor/editorspellchecker;1");
-  NS_ENSURE_STATE(mPendingSpellCheck);
-
   nsCOMPtr<nsITextServicesFilter> filter =
     do_CreateInstance("@mozilla.org/editor/txtsrvfiltermail;1");
-  if (!filter) {
-    mPendingSpellCheck = nullptr;
-    NS_ENSURE_STATE(filter);
+  if (NS_WARN_IF(!filter)) {
+    return NS_ERROR_FAILURE;
   }
+
+  mPendingSpellCheck = new EditorSpellCheck();
   mPendingSpellCheck->SetFilter(filter);
 
   mPendingInitEditorSpellCheckCallback = new InitEditorSpellCheckCallback(this);
-  if (!mPendingInitEditorSpellCheckCallback) {
-    mPendingSpellCheck = nullptr;
-    NS_ENSURE_STATE(mPendingInitEditorSpellCheckCallback);
-  }
-
   nsresult rv = mPendingSpellCheck->InitSpellChecker(
                   mTextEditor, false, mPendingInitEditorSpellCheckCallback);
   if (NS_FAILED(rv)) {
     mPendingSpellCheck = nullptr;
     mPendingInitEditorSpellCheckCallback = nullptr;
     NS_ENSURE_SUCCESS(rv, rv);
   }
 
@@ -1972,18 +1965,18 @@ private:
 };
 NS_IMPL_ISUPPORTS(UpdateCurrentDictionaryCallback, nsIEditorSpellCheckCallback)
 
 NS_IMETHODIMP mozInlineSpellChecker::UpdateCurrentDictionary()
 {
   // mSpellCheck is null and mPendingSpellCheck is nonnull while the spell
   // checker is being initialized.  Calling UpdateCurrentDictionary on
   // mPendingSpellCheck simply queues the dictionary update after the init.
-  nsCOMPtr<nsIEditorSpellCheck> spellCheck = mSpellCheck ? mSpellCheck :
-                                             mPendingSpellCheck;
+  RefPtr<EditorSpellCheck> spellCheck =
+    mSpellCheck ? mSpellCheck : mPendingSpellCheck;
   if (!spellCheck) {
     return NS_OK;
   }
 
   RefPtr<UpdateCurrentDictionaryCallback> cb =
     new UpdateCurrentDictionaryCallback(this, mDisabledAsyncToken);
   NS_ENSURE_STATE(cb);
   nsresult rv = spellCheck->UpdateCurrentDictionary(cb);
diff --git a/extensions/spellcheck/src/mozInlineSpellChecker.h b/extensions/spellcheck/src/mozInlineSpellChecker.h
--- a/extensions/spellcheck/src/mozInlineSpellChecker.h
+++ b/extensions/spellcheck/src/mozInlineSpellChecker.h
@@ -27,16 +27,17 @@
 class mozInlineSpellWordUtil;
 class mozInlineSpellChecker;
 class mozInlineSpellResume;
 class InitEditorSpellCheckCallback;
 class UpdateCurrentDictionaryCallback;
 class mozInlineSpellResume;
 
 namespace mozilla {
+class EditorSpellCheck;
 enum class EditAction : int32_t;
 } // namespace mozilla
 
 class mozInlineSpellStatus
 {
 public:
   explicit mozInlineSpellStatus(mozInlineSpellChecker* aSpellChecker);
 
@@ -132,18 +133,18 @@ private:
 
   // Access with CanEnableInlineSpellChecking
   enum SpellCheckingState { SpellCheck_Uninitialized = -1,
                             SpellCheck_NotAvailable = 0,
                             SpellCheck_Available = 1};
   static SpellCheckingState gCanEnableSpellChecking;
 
   RefPtr<mozilla::TextEditor> mTextEditor;
-  nsCOMPtr<nsIEditorSpellCheck> mSpellCheck;
-  nsCOMPtr<nsIEditorSpellCheck> mPendingSpellCheck;
+  RefPtr<mozilla::EditorSpellCheck> mSpellCheck;
+  RefPtr<mozilla::EditorSpellCheck> mPendingSpellCheck;
   nsCOMPtr<nsIDOMTreeWalker> mTreeWalker;
   nsCOMPtr<mozISpellI18NUtil> mConverter;
 
   int32_t mNumWordsInSpellSelection;
   int32_t mMaxNumWordsInSpellSelection;
 
   // How many misspellings we can add at once. This is often less than the max
   // total number of misspellings. When you have a large textarea prepopulated
