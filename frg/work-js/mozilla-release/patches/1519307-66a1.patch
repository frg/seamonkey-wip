# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1547222475 0
# Node ID daf50f25895db073e44d50fecf2e4f6fe873865d
# Parent  63aaee03f420dc1aa81f8693f4d0da109d17984b
Bug 1519307 - Add a new project to build useful parts of breakpad independently. r=froydnj

With `ac_add_options --enable-project=tools/crashreporter` in a
mozconfig, `./mach build` builds minidump_stackwalk, dump_syms
and fileid.

One caveat is that due to limitation in how the build system works
currently, it's cumbersome to keep dump_syms as a host program for
Gecko, and to make it a target program for this project. For now,
keep it as a host program. We're not going to use it on automation,
but it's still convenient to have for quick local builds (I've had
to resort to awful hacks downstream).

Differential Revision: https://phabricator.services.mozilla.com/D16299

diff --git a/.flake8 b/.flake8
--- a/.flake8
+++ b/.flake8
@@ -13,8 +13,9 @@ exclude =
     glx/skia/,
     intl/icu/,
     ipc/chromium/,
     js/*.configure,
     memory/moz.configure,
     security/nss/,
     testing/mochitest/pywebsocket,
     tools/lint/test/files,
+    tools/crashreporter/*.configure,
diff --git a/mfbt/moz.build b/mfbt/moz.build
--- a/mfbt/moz.build
+++ b/mfbt/moz.build
@@ -2,18 +2,16 @@
 # vim: set filetype=python:
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 with Files("**"):
     BUG_COMPONENT = ("Core", "MFBT")
 
-TEST_DIRS += ['tests']
-
 Library('mfbt')
 
 EXPORTS.mozilla = [
     'Algorithm.h',
     'Alignment.h',
     'AllocPolicy.h',
     'AlreadyAddRefed.h',
     'Array.h',
@@ -142,16 +140,21 @@ UNIFIED_SOURCES += [
     'JSONWriter.cpp',
     'Poison.cpp',
     'SHA1.cpp',
     'TaggedAnonymousMemory.cpp',
     'Unused.cpp',
     'Utf8.cpp',
 ]
 
+if CONFIG['MOZ_BUILD_APP'] != 'tools/crashreporter':
+    # Building MFBT tests adds a large overhead when building
+    # tools/crashreporter.
+    TEST_DIRS += ['tests']
+
 DEFINES['IMPL_MFBT'] = True
 
 SOURCES += [
     'Compression.cpp',
     'decimal/Decimal.cpp',
     'lz4.c',
 ]
 
diff --git a/old-configure.in b/old-configure.in
--- a/old-configure.in
+++ b/old-configure.in
@@ -1020,16 +1020,20 @@ case "${OS_TARGET}" in
 Darwin)
   ;;
 *)
   STL_FLAGS="-I${DIST}/stl_wrappers"
   WRAP_STL_INCLUDES=1
   ;;
 esac
 
+if test "$MOZ_BUILD_APP" = "tools/crashreporter"; then
+    WRAP_STL_INCLUDES=
+fi
+
 dnl Checks for header files.
 dnl ========================================================
 AC_HEADER_DIRENT
 case "$target_os" in
 bitrig*|dragonfly*|freebsd*|openbsd*)
 # for stuff like -lXshm
     CPPFLAGS="${CPPFLAGS} ${X_CFLAGS}"
     ;;
@@ -1867,28 +1871,32 @@ MOZ_ARG_HEADER(Toolkit Options)
 
 dnl ========================================================
 dnl = Enable the toolkit as needed                         =
 dnl ========================================================
 
 case "$MOZ_WIDGET_TOOLKIT" in
 
 cocoa)
-    LDFLAGS="$LDFLAGS -framework Cocoa -lobjc"
+    LDFLAGS="$LDFLAGS -framework Cocoa"
     # Use -Wl as a trick to avoid -framework and framework names from
     # being separated by AC_SUBST_LIST.
     TK_LIBS='-Wl,-framework,Foundation -Wl,-framework,CoreFoundation -Wl,-framework,CoreLocation -Wl,-framework,QuartzCore -Wl,-framework,Carbon -Wl,-framework,CoreAudio -Wl,-framework,CoreVideo -Wl,-framework,AudioToolbox -Wl,-framework,AudioUnit -Wl,-framework,AddressBook -Wl,-framework,OpenGL -Wl,-framework,Security -Wl,-framework,ServiceManagement -Wl,-framework,CoreServices -Wl,-framework,ApplicationServices -Wl,-framework,AppKit'
     TK_CFLAGS=""
     CFLAGS="$CFLAGS $TK_CFLAGS"
     CXXFLAGS="$CXXFLAGS $TK_CFLAGS"
     MOZ_USER_DIR="Mozilla"
     MOZ_FS_LAYOUT=bundle
     ;;
 esac
 
+if test "$OS_TARGET" = Darwin; then
+    LDFLAGS="$LDFLAGS -lobjc"
+fi
+
 dnl there are a lot of tests on MOZ_ENABLE_GTK below, that are more convenient
 dnl to keep that way than testing against MOZ_WIDGET_TOOLKIT
 case "$MOZ_WIDGET_TOOLKIT" in
 gtk*)
     MOZ_ENABLE_GTK=1
     ;;
 esac
 
diff --git a/testing/tools/fileid/moz.build b/testing/tools/fileid/moz.build
--- a/testing/tools/fileid/moz.build
+++ b/testing/tools/fileid/moz.build
@@ -5,16 +5,18 @@
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 if CONFIG['OS_ARCH'] == 'WINNT':
     Program('fileid')
     USE_STATIC_LIBS = True
     SOURCES += ['win_fileid.cpp']
     OS_LIBS += ['dbghelp']
     NO_PGO = True
+elif CONFIG['MOZ_BUILD_APP'] == 'tools/crashreporter':
+    Program('fileid')
 else:
     # Technically, it's not a GeckoProgram (thus linkage=None), but it needs
     # mozglue because of the breakpad code it uses being compiled for gecko
     # and requiring infallible malloc.
     GeckoProgram('fileid', linkage=None)
 
 if CONFIG['OS_ARCH'] == 'Linux':
     USE_LIBS += [
@@ -23,16 +25,19 @@ if CONFIG['OS_ARCH'] == 'Linux':
     SOURCES += ['linux_fileid.cpp']
 
 
 if CONFIG['OS_ARCH'] == 'Darwin':
     USE_LIBS += [
         'breakpad_mac_common_s',
     ]
     SOURCES += ['mac_fileid.cpp']
+    OS_LIBS += [
+        '-framework Foundation'
+    ]
 
 
 if CONFIG['OS_ARCH'] == 'Linux' or CONFIG['OS_ARCH'] == 'Darwin':
     USE_LIBS += [
         'breakpad_common_s',
     ]
     LOCAL_INCLUDES += [
         '/toolkit/crashreporter/google-breakpad/src',
diff --git a/toolkit/crashreporter/breakpad-client/linux/moz.build b/toolkit/crashreporter/breakpad-client/linux/moz.build
--- a/toolkit/crashreporter/breakpad-client/linux/moz.build
+++ b/toolkit/crashreporter/breakpad-client/linux/moz.build
@@ -25,11 +25,11 @@ if CONFIG['OS_TARGET'] == 'Linux' or CON
 
 if CONFIG['OS_TARGET'] == 'Android':
     DEFINES['ANDROID_NDK_MAJOR_VERSION'] = CONFIG['ANDROID_NDK_MAJOR_VERSION']
     DEFINES['ANDROID_NDK_MINOR_VERSION'] = CONFIG['ANDROID_NDK_MINOR_VERSION']
     LOCAL_INCLUDES += [
         '/toolkit/crashreporter/google-breakpad/src/common/android/include',
     ]
 
-FINAL_LIBRARY = 'xul'
+FINAL_LIBRARY = 'breakpad_client'
 
 include('/toolkit/crashreporter/crashreporter.mozbuild')
diff --git a/toolkit/crashreporter/breakpad-client/mac/crash_generation/moz.build b/toolkit/crashreporter/breakpad-client/mac/crash_generation/moz.build
--- a/toolkit/crashreporter/breakpad-client/mac/crash_generation/moz.build
+++ b/toolkit/crashreporter/breakpad-client/mac/crash_generation/moz.build
@@ -4,17 +4,17 @@
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 UNIFIED_SOURCES += [
     'crash_generation_client.cc',
     'crash_generation_server.cc',
 ]
 
-FINAL_LIBRARY = 'xul'
+FINAL_LIBRARY = 'breakpad_client'
 
 LOCAL_INCLUDES += [
     '/toolkit/crashreporter/breakpad-client',
     '/toolkit/crashreporter/google-breakpad/src',
 ]
 
 if CONFIG['CC_TYPE'] == 'clang':
     CXXFLAGS += ['-Wno-shadow']
diff --git a/toolkit/crashreporter/breakpad-client/mac/handler/moz.build b/toolkit/crashreporter/breakpad-client/mac/handler/moz.build
--- a/toolkit/crashreporter/breakpad-client/mac/handler/moz.build
+++ b/toolkit/crashreporter/breakpad-client/mac/handler/moz.build
@@ -6,15 +6,15 @@
 
 UNIFIED_SOURCES += [
     'breakpad_nlist_64.cc',
     'dynamic_images.cc',
     'exception_handler.cc',
     'minidump_generator.cc',
 ]
 
-FINAL_LIBRARY = 'xul'
+FINAL_LIBRARY = 'breakpad_client'
 
 LOCAL_INCLUDES += [
     '/toolkit/crashreporter/breakpad-client',
     '/toolkit/crashreporter/google-breakpad/src',
 ]
 
diff --git a/toolkit/crashreporter/breakpad-client/moz.build b/toolkit/crashreporter/breakpad-client/moz.build
--- a/toolkit/crashreporter/breakpad-client/moz.build
+++ b/toolkit/crashreporter/breakpad-client/moz.build
@@ -3,16 +3,31 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 SOURCES += [
     'minidump_file_writer.cc',
 ]
 
+Library('breakpad_client')
+
+USE_LIBS += [
+    'breakpad_common_s',
+]
+
+if CONFIG['OS_ARCH'] == 'Darwin':
+    USE_LIBS += [
+        'breakpad_mac_common_s',
+    ]
+elif CONFIG['OS_ARCH'] == 'Linux':
+    USE_LIBS += [
+        'breakpad_linux_common_s',
+    ]
+
 FINAL_LIBRARY = 'xul'
 
 LOCAL_INCLUDES += [
     '/toolkit/crashreporter/google-breakpad/src',
 ]
 
 if CONFIG['CC_TYPE'] in ('clang', 'gcc'):
     CXXFLAGS += ['-Wno-shadow']
diff --git a/toolkit/crashreporter/google-breakpad/src/common/linux/moz.build b/toolkit/crashreporter/google-breakpad/src/common/linux/moz.build
--- a/toolkit/crashreporter/google-breakpad/src/common/linux/moz.build
+++ b/toolkit/crashreporter/google-breakpad/src/common/linux/moz.build
@@ -41,16 +41,14 @@ if CONFIG['OS_TARGET'] == 'Android':
     DEFINES['ANDROID_NDK_MAJOR_VERSION'] = CONFIG['ANDROID_NDK_MAJOR_VERSION']
     DEFINES['ANDROID_NDK_MINOR_VERSION'] = CONFIG['ANDROID_NDK_MINOR_VERSION']
     COMPILE_FLAGS['OS_INCLUDES'] += [
         '-I%s/toolkit/crashreporter/google-breakpad/src/common/android/include' % TOPSRCDIR,
     ]
 
 Library('breakpad_linux_common_s')
 
-FINAL_LIBRARY = 'xul'
-
 HOST_DEFINES['NO_STABS_SUPPORT'] = True
 
 include('/toolkit/crashreporter/crashreporter.mozbuild')
 
 if CONFIG['CC_TYPE'] in ('clang', 'gcc'):
     CXXFLAGS += ['-Wno-shadow']
diff --git a/toolkit/crashreporter/google-breakpad/src/common/mac/moz.build b/toolkit/crashreporter/google-breakpad/src/common/mac/moz.build
--- a/toolkit/crashreporter/google-breakpad/src/common/mac/moz.build
+++ b/toolkit/crashreporter/google-breakpad/src/common/mac/moz.build
@@ -37,13 +37,11 @@ SOURCES += [
     'bootstrap_compat.cc',
     'HTTPMultipartUpload.m',
     'MachIPC.mm',
     'string_utilities.cc',
 ]
 
 Library('breakpad_mac_common_s')
 
-FINAL_LIBRARY = 'xul'
-
 CMFLAGS += ['-std=c99']
 
 include('/toolkit/crashreporter/crashreporter.mozbuild')
diff --git a/toolkit/crashreporter/google-breakpad/src/common/moz.build b/toolkit/crashreporter/google-breakpad/src/common/moz.build
--- a/toolkit/crashreporter/google-breakpad/src/common/moz.build
+++ b/toolkit/crashreporter/google-breakpad/src/common/moz.build
@@ -65,11 +65,9 @@ if CONFIG['OS_TARGET'] == 'Android':
         'android/breakpad_getcontext.S',
     ]
     LOCAL_INCLUDES += [
         '/toolkit/crashreporter/google-breakpad/src/common/android/include',
     ]
 
 Library('breakpad_common_s')
 
-FINAL_LIBRARY = 'xul'
-
 include('/toolkit/crashreporter/crashreporter.mozbuild')
diff --git a/toolkit/crashreporter/google-breakpad/src/tools/windows/dump_syms/moz.build b/toolkit/crashreporter/google-breakpad/src/tools/windows/dump_syms/moz.build
--- a/toolkit/crashreporter/google-breakpad/src/tools/windows/dump_syms/moz.build
+++ b/toolkit/crashreporter/google-breakpad/src/tools/windows/dump_syms/moz.build
@@ -12,20 +12,24 @@ HOST_SOURCES += [
     '../../../common/windows/omap.cc',
     '../../../common/windows/pdb_source_line_writer.cc',
     '../../../common/windows/string_utils.cc',
     'dump_syms.cc',
 ]
 
 HOST_CXXFLAGS += [
     '-O2',
-    '-EHsc',
-    '-MD'
 ]
 
+if CONFIG['HOST_CC_TYPE'] in ('msvc', 'clang-cl'):
+    HOST_CXXFLAGS += [
+        '-EHsc',
+        '-MD'
+    ]
+
 HOST_OS_LIBS += [
     'dbghelp',
     'diaguids',
     'imagehlp',
 ]
 
 LOCAL_INCLUDES += [
     '../../..'
diff --git a/tools/crashreporter/app.mozbuild b/tools/crashreporter/app.mozbuild
new file mode 100644
--- /dev/null
+++ b/tools/crashreporter/app.mozbuild
@@ -0,0 +1,32 @@
+# This Source Code Form is subject to the terms of the Mozilla Public
+# License, v. 2.0. If a copy of the MPL was not distributed with this
+# file, You can obtain one at http://mozilla.org/MPL/2.0/.
+
+DIRS += [
+    '/config/external/zlib',
+    '/testing/tools/fileid',
+    '/toolkit/crashreporter/google-breakpad/src/common',
+    '/toolkit/crashreporter/google-breakpad/src/processor',
+    '/toolkit/crashreporter/rust',
+    '/tools/crashreporter/minidump_stackwalk',
+]
+
+if CONFIG['OS_ARCH'] == 'Linux':
+    DIRS += [
+        '/toolkit/crashreporter/google-breakpad/src/common/linux',
+        '/toolkit/crashreporter/google-breakpad/src/tools/linux/dump_syms',
+    ]
+
+if CONFIG['OS_ARCH'] == 'Darwin':
+    DIRS += [
+        '/toolkit/crashreporter/google-breakpad/src/common/mac',
+        '/toolkit/crashreporter/google-breakpad/src/tools/mac/dump_syms',
+    ]
+
+# While the Linux and Mac dump_syms can be cross-built, the Windows one can't,
+# and at the moment, it's a host tool, so only build it when the host is
+# Windows.
+if CONFIG['HOST_OS_ARCH'] == 'WINNT':
+    DIRS += [
+        '/toolkit/crashreporter/google-breakpad/src/tools/windows/dump_syms',
+    ]
diff --git a/tools/crashreporter/minidump_stackwalk/moz.build b/tools/crashreporter/minidump_stackwalk/moz.build
new file mode 100644
--- /dev/null
+++ b/tools/crashreporter/minidump_stackwalk/moz.build
@@ -0,0 +1,29 @@
+# This Source Code Form is subject to the terms of the Mozilla Public
+# License, v. 2.0. If a copy of the MPL was not distributed with this
+# file, You can obtain one at http://mozilla.org/MPL/2.0/.
+
+Program('minidump_stackwalk')
+
+SOURCES += [
+    '/toolkit/crashreporter/google-breakpad/src/common/path_helper.cc',
+    '/toolkit/crashreporter/google-breakpad/src/processor/minidump_stackwalk.cc',
+    '/toolkit/crashreporter/google-breakpad/src/processor/simple_symbol_supplier.cc',
+]
+
+USE_LIBS += [
+    'breakpad_processor',
+    'zlib',
+]
+
+if CONFIG['OS_TARGET'] == 'WINNT':
+    if CONFIG['CC_TYPE'] in ('clang', 'gcc'):
+        DEFINES['__USE_MINGW_ANSI_STDIO'] = True
+
+        LDFLAGS += [
+            '-static-libgcc',
+            '-static-libstdc++',
+        ]
+
+DisableStlWrapping()
+
+include('/toolkit/crashreporter/crashreporter.mozbuild')
diff --git a/tools/crashreporter/moz.configure b/tools/crashreporter/moz.configure
new file mode 100644
--- /dev/null
+++ b/tools/crashreporter/moz.configure
@@ -0,0 +1,5 @@
+# This Source Code Form is subject to the terms of the Mozilla Public
+# License, v. 2.0. If a copy of the MPL was not distributed with this
+# file, You can obtain one at http://mozilla.org/MPL/2.0/.
+
+include('../../build/moz.configure/rust.configure')
