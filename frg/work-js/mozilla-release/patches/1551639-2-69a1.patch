# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1559795700 0
# Node ID 77a5c54083fa748a42f0c88d9817057a39df6d62
# Parent  3bffa313ec17151c21206219971683f11d5443e8
Bug 1551639 - Don't build generated source files during the export tier. r=nalexander

As established previously, generated source files that are not included
don't need to be built during the export tier.

Differential Revision: https://phabricator.services.mozilla.com/D33771

diff --git a/python/mozbuild/mozbuild/backend/common.py b/python/mozbuild/mozbuild/backend/common.py
--- a/python/mozbuild/mozbuild/backend/common.py
+++ b/python/mozbuild/mozbuild/backend/common.py
@@ -1,14 +1,15 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import, unicode_literals
 
+import itertools
 import json
 import os
 
 import mozpack.path as mozpath
 
 from mozbuild.backend.base import BuildBackend
 
 from mozbuild.frontend.context import (
@@ -159,18 +160,19 @@ class CommonBackend(BuildBackend):
             self._binaries.shared_libraries.append(obj)
             return False
 
         elif isinstance(obj, (GeneratedSources, HostGeneratedSources)):
             self._handle_generated_sources(obj.files)
             return False
 
         elif isinstance(obj, GeneratedFile):
-            if obj.required_for_compile:
-                for f in obj.required_for_compile:
+            if obj.required_during_compile or obj.required_before_compile:
+                for f in itertools.chain(obj.required_before_compile,
+                                         obj.required_during_compile):
                     fullpath = ObjDirPath(obj._context, '!' + f).full_path
                     self._handle_generated_sources([fullpath])
             return False
 
         elif isinstance(obj, Exports):
             objdir_files = [f.full_path for path, files in obj.files.walk() for f in files if isinstance(f, ObjDirPath)]
             if objdir_files:
                 self._handle_generated_sources(objdir_files)
diff --git a/python/mozbuild/mozbuild/backend/recursivemake.py b/python/mozbuild/mozbuild/backend/recursivemake.py
--- a/python/mozbuild/mozbuild/backend/recursivemake.py
+++ b/python/mozbuild/mozbuild/backend/recursivemake.py
@@ -509,23 +509,26 @@ class RecursiveMakeBackend(CommonBackend
                 else:
                     backend_file.write('%s := %s\n' % (k, v))
         elif isinstance(obj, HostDefines):
             self._process_defines(obj, backend_file, which='HOST_DEFINES')
         elif isinstance(obj, Defines):
             self._process_defines(obj, backend_file)
 
         elif isinstance(obj, GeneratedFile):
-            if obj.required_for_compile:
+            if obj.required_before_compile:
                 tier = 'export'
+            elif obj.required_during_compile:
+                tier = None
             elif obj.localized:
                 tier = 'libs'
             else:
                 tier = 'misc'
-            self._no_skip[tier].add(backend_file.relobjdir)
+            if tier:
+                self._no_skip[tier].add(backend_file.relobjdir)
 
             # Localized generated files can use {AB_CD} and {AB_rCD} in their
             # output paths.
             if obj.localized:
                 substs = {'AB_CD': '$(AB_CD)', 'AB_rCD': '$(AB_rCD)'}
             else:
                 substs = {}
             outputs = []
@@ -576,22 +579,23 @@ class RecursiveMakeBackend(CommonBackend
 
             force = ''
             if obj.force:
                 force = ' FORCE'
             elif obj.localized:
                 force = ' $(if $(IS_LANGUAGE_REPACK),FORCE)'
 
             if obj.script:
-                # If we're doing this during export that means we need it during
-                # compile, but if we have an artifact build we don't run compile,
-                # so we can skip it altogether or let the rule run as the result of
-                # something depending on it.
-                if tier != 'export' or not self.environment.is_artifact_build:
-                    if not needs_AB_rCD:
+                # If we are doing an artifact build, we don't run compiler, so
+                # we can skip generated files that are needed during compile,
+                # or let the rule run as the result of something depending on
+                # it.
+                if not (obj.required_before_compile or obj.required_during_compile) or \
+                        not self.environment.is_artifact_build:
+                    if tier and not needs_AB_rCD:
                         # Android localized resources have special Makefile
                         # handling.
                         backend_file.write('%s:: %s\n' % (tier, stub_file))
                 for output in outputs:
                     backend_file.write('%s: %s ;\n' % (output, stub_file))
                     backend_file.write('GARBAGE += %s\n' % output)
                 backend_file.write('GARBAGE += %s\n' % stub_file)
                 backend_file.write('EXTRA_MDDEPEND_FILES += %s\n' % dep_file)
diff --git a/python/mozbuild/mozbuild/frontend/data.py b/python/mozbuild/mozbuild/frontend/data.py
--- a/python/mozbuild/mozbuild/frontend/data.py
+++ b/python/mozbuild/mozbuild/frontend/data.py
@@ -1113,42 +1113,44 @@ class GeneratedFile(ContextDerived):
     """Represents a generated file."""
 
     __slots__ = (
         'script',
         'method',
         'outputs',
         'inputs',
         'flags',
-        'required_for_compile',
+        'required_before_compile',
+        'required_during_compile',
         'localized',
         'force',
     )
 
     def __init__(self, context, script, method, outputs, inputs,
                  flags=(), localized=False, force=False):
         ContextDerived.__init__(self, context)
         self.script = script
         self.method = method
         self.outputs = outputs if isinstance(outputs, tuple) else (outputs,)
         self.inputs = inputs
         self.flags = flags
         self.localized = localized
         self.force = force
 
         suffixes = (
-            '.asm',
-            '.c',
-            '.cpp',
             '.h',
             '.inc',
             '.py',
             '.rs',
         )
-        self.required_for_compile = [f for f in self.outputs if f.endswith(suffixes) or 'stl_wrappers/' in f]
+        self.required_before_compile = [
+            f for f in self.outputs if f.endswith(suffixes) or 'stl_wrappers/' in f]
+
+        self.required_during_compile = [
+            f for f in self.outputs if f.endswith(('.asm', '.c', '.cpp'))]
 
 
 class ChromeManifestEntry(ContextDerived):
     """Represents a chrome.manifest entry."""
 
     __slots__ = (
         'path',
         'entry',
diff --git a/python/mozbuild/mozbuild/test/backend/data/generated-files/moz.build b/python/mozbuild/mozbuild/test/backend/data/generated-files/moz.build
--- a/python/mozbuild/mozbuild/test/backend/data/generated-files/moz.build
+++ b/python/mozbuild/mozbuild/test/backend/data/generated-files/moz.build
@@ -1,12 +1,12 @@
 # -*- Mode: python; indent-tabs-mode: nil; tab-width: 40 -*-
 # Any copyright is dedicated to the Public Domain.
 # http://creativecommons.org/publicdomain/zero/1.0/
 
-GENERATED_FILES += [ 'bar.c', 'foo.c', 'quux.c' ]
+GENERATED_FILES += [ 'bar.c', 'foo.h', 'quux.c' ]
 
 bar = GENERATED_FILES['bar.c']
 bar.script = 'generate-bar.py:baz'
 
-foo = GENERATED_FILES['foo.c']
+foo = GENERATED_FILES['foo.h']
 foo.script = 'generate-foo.py'
 foo.inputs = ['foo-data']
diff --git a/python/mozbuild/mozbuild/test/backend/test_recursivemake.py b/python/mozbuild/mozbuild/test/backend/test_recursivemake.py
--- a/python/mozbuild/mozbuild/test/backend/test_recursivemake.py
+++ b/python/mozbuild/mozbuild/test/backend/test_recursivemake.py
@@ -391,60 +391,57 @@ class TestRecursiveMakeBackend(BackendTe
     def test_generated_files(self):
         """Ensure GENERATED_FILES is handled properly."""
         env = self._consume('generated-files', RecursiveMakeBackend)
 
         backend_path = mozpath.join(env.topobjdir, 'backend.mk')
         lines = [l.strip() for l in open(backend_path, 'rt').readlines()[2:]]
 
         expected = [
-            'export:: $(MDDEPDIR)/bar.c.stub',
             'bar.c: $(MDDEPDIR)/bar.c.stub ;',
             'GARBAGE += bar.c',
             'GARBAGE += $(MDDEPDIR)/bar.c.stub',
             'EXTRA_MDDEPEND_FILES += bar.c.pp',
             '$(MDDEPDIR)/bar.c.stub: %s/generate-bar.py' % env.topsrcdir,
             '$(REPORT_BUILD)',
             '$(call py_action,file_generate,%s/generate-bar.py baz bar.c $(MDDEPDIR)/bar.c.pp $(MDDEPDIR)/bar.c.stub)' % env.topsrcdir,
             '@$(TOUCH) $@',
             '',
-            'export:: $(MDDEPDIR)/foo.c.stub',
-            'foo.c: $(MDDEPDIR)/foo.c.stub ;',
-            'GARBAGE += foo.c',
-            'GARBAGE += $(MDDEPDIR)/foo.c.stub',
-            'EXTRA_MDDEPEND_FILES += foo.c.pp',
-            '$(MDDEPDIR)/foo.c.stub: %s/generate-foo.py $(srcdir)/foo-data' % (env.topsrcdir),
+            'export:: $(MDDEPDIR)/foo.h.stub',
+            'foo.h: $(MDDEPDIR)/foo.h.stub ;',
+            'GARBAGE += foo.h',
+            'GARBAGE += $(MDDEPDIR)/foo.h.stub',
+            'EXTRA_MDDEPEND_FILES += foo.h.pp',
+            '$(MDDEPDIR)/foo.h.stub: %s/generate-foo.py $(srcdir)/foo-data' % (env.topsrcdir),
             '$(REPORT_BUILD)',
-            '$(call py_action,file_generate,%s/generate-foo.py main foo.c $(MDDEPDIR)/foo.c.pp $(MDDEPDIR)/foo.c.stub $(srcdir)/foo-data)' % (env.topsrcdir),
+            '$(call py_action,file_generate,%s/generate-foo.py main foo.h $(MDDEPDIR)/foo.h.pp $(MDDEPDIR)/foo.h.stub $(srcdir)/foo-data)' % (env.topsrcdir),  # noqa
             '@$(TOUCH) $@',
             '',
         ]
 
         self.maxDiff = None
         self.assertEqual(lines, expected)
 
     def test_generated_files_force(self):
         """Ensure GENERATED_FILES with .force is handled properly."""
         env = self._consume('generated-files-force', RecursiveMakeBackend)
 
         backend_path = mozpath.join(env.topobjdir, 'backend.mk')
         lines = [l.strip() for l in open(backend_path, 'rt').readlines()[2:]]
 
         expected = [
-            'export:: $(MDDEPDIR)/bar.c.stub',
             'bar.c: $(MDDEPDIR)/bar.c.stub ;',
             'GARBAGE += bar.c',
             'GARBAGE += $(MDDEPDIR)/bar.c.stub',
             'EXTRA_MDDEPEND_FILES += bar.c.pp',
             '$(MDDEPDIR)/bar.c.stub: %s/generate-bar.py FORCE' % env.topsrcdir,
             '$(REPORT_BUILD)',
             '$(call py_action,file_generate,%s/generate-bar.py baz bar.c $(MDDEPDIR)/bar.c.pp $(MDDEPDIR)/bar.c.stub)' % env.topsrcdir,
             '@$(TOUCH) $@',
             '',
-            'export:: $(MDDEPDIR)/foo.c.stub',
             'foo.c: $(MDDEPDIR)/foo.c.stub ;',
             'GARBAGE += foo.c',
             'GARBAGE += $(MDDEPDIR)/foo.c.stub',
             'EXTRA_MDDEPEND_FILES += foo.c.pp',
             '$(MDDEPDIR)/foo.c.stub: %s/generate-foo.py $(srcdir)/foo-data' % (env.topsrcdir),
             '$(REPORT_BUILD)',
             '$(call py_action,file_generate,%s/generate-foo.py main foo.c $(MDDEPDIR)/foo.c.pp $(MDDEPDIR)/foo.c.stub $(srcdir)/foo-data)' % (env.topsrcdir),
             '@$(TOUCH) $@',
