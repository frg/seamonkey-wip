# HG changeset patch
# User Jason Orendorff <jorendorff@mozilla.com>
# Date 1520462433 21600
#      Wed Mar 07 16:40:33 2018 -0600
# Node ID c2d7e417c6fbf4fe6d5584be183f6e0f10c0a524
# Parent  7ac2b5156b596d8337ba49899ec11413c7c576ea
Bug 1437537 - Fix assertion when forcing a return while paused on JSOP_EXCEPTION. r=jimb.

diff --git a/js/src/jit-test/tests/debug/bug1437537.js b/js/src/jit-test/tests/debug/bug1437537.js
new file mode 100644
--- /dev/null
+++ b/js/src/jit-test/tests/debug/bug1437537.js
@@ -0,0 +1,18 @@
+// Don't assert when pausing for onStep at JSOP_EXCEPTION.
+
+var g = newGlobal();
+var dbg = new Debugger(g);
+let f = g.Function(`try { throw new Error; } catch (e) { return 'natural'; }`);
+
+let limit = -1;
+dbg.onEnterFrame = function (frame) {
+    frame.onStep = function () {
+        if (this.offset > limit) {
+            limit = this.offset;
+            return { return: 'forced' };
+        }
+    };
+};
+
+while (f() === 'forced') {
+}
diff --git a/js/src/vm/Debugger.cpp b/js/src/vm/Debugger.cpp
--- a/js/src/vm/Debugger.cpp
+++ b/js/src/vm/Debugger.cpp
@@ -1956,17 +1956,17 @@ Debugger::slowPathOnNewWasmInstance(JSCo
 
     MOZ_ASSERT(resumeMode == ResumeMode::Continue);
 }
 
 /* static */ ResumeMode
 Debugger::onTrap(JSContext* cx, MutableHandleValue vp)
 {
     FrameIter iter(cx);
-    JS::AutoSaveExceptionState saveExc(cx);
+    JS::AutoSaveExceptionState savedExc(cx);
     Rooted<GlobalObject*> global(cx);
     BreakpointSite* site;
     bool isJS; // true when iter.hasScript(), false when iter.isWasm()
     jsbytecode* pc; // valid when isJS == true
     uint32_t bytecodeOffset; // valid when isJS == false
     if (iter.hasScript()) {
         RootedScript script(cx, iter.script());
         MOZ_ASSERT(script->isDebuggee());
@@ -2023,18 +2023,20 @@ Debugger::onTrap(JSContext* cx, MutableH
             RootedValue scriptFrame(cx);
             if (!dbg->getFrame(cx, iter, &scriptFrame))
                 return dbg->reportUncaughtException(ac);
             RootedValue rv(cx);
             Rooted<JSObject*> handler(cx, bp->handler);
             bool ok = CallMethodIfPresent(cx, handler, "hit", 1, scriptFrame.address(), &rv);
             ResumeMode resumeMode = dbg->processHandlerResult(ac, ok, rv,  iter.abstractFramePtr(),
                                                               iter.pc(), vp);
-            if (resumeMode != ResumeMode::Continue)
+            if (resumeMode != ResumeMode::Continue) {
+                savedExc.drop();
                 return resumeMode;
+            }
 
             /* Calling JS code invalidates site. Reload it. */
             if (isJS)
                 site = iter.script()->getBreakpointSite(pc);
             else
                 site = iter.wasmInstance()->debug().getOrCreateBreakpointSite(cx, bytecodeOffset);
         }
     }
@@ -2054,17 +2056,17 @@ Debugger::onSingleStep(JSContext* cx, Mu
     FrameIter iter(cx);
 
     /*
      * We may be stepping over a JSOP_EXCEPTION, that pushes the context's
      * pending exception for a 'catch' clause to handle. Don't let the
      * onStep handlers mess with that (other than by returning a resumption
      * value).
      */
-    JS::AutoSaveExceptionState saveExc(cx);
+    JS::AutoSaveExceptionState savedExc(cx);
 
     /*
      * Build list of Debugger.Frame instances referring to this frame with
      * onStep handlers.
      */
     Rooted<DebuggerFrameVector> frames(cx, DebuggerFrameVector(cx));
     if (!getDebuggerFrames(iter.abstractFramePtr(), &frames))
         return ResumeMode::Terminate;
@@ -2115,18 +2117,20 @@ Debugger::onSingleStep(JSContext* cx, Mu
 
         Maybe<AutoCompartment> ac;
         ac.emplace(cx, dbg->object);
 
         ResumeMode resumeMode = ResumeMode::Continue;
         bool success = handler->onStep(cx, frame, resumeMode, vp);
         resumeMode = dbg->processParsedHandlerResult(ac, iter.abstractFramePtr(), iter.pc(), success,
                                                      resumeMode, vp);
-        if (resumeMode != ResumeMode::Continue)
+        if (resumeMode != ResumeMode::Continue) {
+            savedExc.drop();
             return resumeMode;
+        }
     }
 
     vp.setUndefined();
     return ResumeMode::Continue;
 }
 
 ResumeMode
 Debugger::fireNewGlobalObject(JSContext* cx, Handle<GlobalObject*> global, MutableHandleValue vp)
