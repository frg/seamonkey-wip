# HG changeset patch
# User Ryan VanderMeulen <ryanvm@gmail.com>
# Date 1521727475 14400
#      Thu Mar 22 10:04:35 2018 -0400
# Node ID 64276e10f5964127fc64f802c01bd3c25bb3021c
# Parent  4139bc57dacf2979a00596ba6fbf2f78a4485d83
Bug 1447763 - Remove some MSVC workarounds in the JS engine that aren't needed anymore. r=Waldo, r=jandem

diff --git a/js/src/frontend/FoldConstants.cpp b/js/src/frontend/FoldConstants.cpp
--- a/js/src/frontend/FoldConstants.cpp
+++ b/js/src/frontend/FoldConstants.cpp
@@ -1003,28 +1003,23 @@ ComputeBinary(ParseNodeKind kind, double
 
     if (kind == ParseNodeKind::Sub)
         return left - right;
 
     if (kind == ParseNodeKind::Star)
         return left * right;
 
     if (kind == ParseNodeKind::Mod)
-        return right == 0 ? GenericNaN() : js_fmod(left, right);
+        return right == 0 ? GenericNaN() : fmod(left, right);
 
     if (kind == ParseNodeKind::Ursh)
         return ToUint32(left) >> (ToUint32(right) & 31);
 
     if (kind == ParseNodeKind::Div) {
         if (right == 0) {
-#if defined(XP_WIN)
-            /* XXX MSVC miscompiles such that (NaN == 0) */
-            if (IsNaN(right))
-                return GenericNaN();
-#endif
             if (left == 0 || IsNaN(left))
                 return GenericNaN();
             if (IsNegative(left) != IsNegative(right))
                 return NegativeInfinity<double>();
             return PositiveInfinity<double>();
         }
 
         return left / right;
diff --git a/js/src/jit/SharedIC.cpp b/js/src/jit/SharedIC.cpp
--- a/js/src/jit/SharedIC.cpp
+++ b/js/src/jit/SharedIC.cpp
@@ -2408,34 +2408,28 @@ DoTypeMonitorFallback(JSContext* cx, Bas
                    *GetNextPc(pc) == JSOP_CHECKRETURN);
         if (stub->monitorsThis())
             TypeScript::SetThis(cx, script, TypeSet::UnknownType());
         else
             TypeScript::Monitor(cx, script, pc, TypeSet::UnknownType());
         return true;
     }
 
-    // Note: ideally we would merge this if-else statement with the one below,
-    // but that triggers an MSVC 2015 compiler bug. See bug 1363054.
     StackTypeSet* types;
     uint32_t argument;
-    if (stub->monitorsArgument(&argument))
-        types = TypeScript::ArgTypes(script, argument);
-    else if (stub->monitorsThis())
-        types = TypeScript::ThisTypes(script);
-    else
-        types = TypeScript::BytecodeTypes(script, pc);
-
     if (stub->monitorsArgument(&argument)) {
         MOZ_ASSERT(pc == script->code());
+        types = TypeScript::ArgTypes(script, argument);
         TypeScript::SetArgument(cx, script, argument, value);
     } else if (stub->monitorsThis()) {
         MOZ_ASSERT(pc == script->code());
+        types = TypeScript::ThisTypes(script);
         TypeScript::SetThis(cx, script, value);
     } else {
+        types = TypeScript::BytecodeTypes(script, pc);
         TypeScript::Monitor(cx, script, pc, types, value);
     }
 
     if (MOZ_UNLIKELY(stub->invalid()))
         return true;
 
     return stub->addMonitorStubForValue(cx, frame, types, value);
 }
diff --git a/js/src/jslibmath.h b/js/src/jslibmath.h
--- a/js/src/jslibmath.h
+++ b/js/src/jslibmath.h
@@ -23,45 +23,24 @@
 #ifdef __GNUC__
 #define js_copysign __builtin_copysign
 #elif defined _WIN32
 #define js_copysign _copysign
 #else
 #define js_copysign copysign
 #endif
 
-/* Consistency wrapper for platform deviations in fmod() */
-static inline double
-js_fmod(double d, double d2)
-{
-#ifdef XP_WIN
-    /*
-     * Workaround MS fmod bug where 42 % (1/0) => NaN, not 42.
-     * Workaround MS fmod bug where -0 % -N => 0, not -0.
-     */
-    if ((mozilla::IsFinite(d) && mozilla::IsInfinite(d2)) ||
-        (d == 0 && mozilla::IsFinite(d2))) {
-        return d;
-    }
-#endif
-    return fmod(d, d2);
-}
-
 namespace js {
 
 inline double
 NumberDiv(double a, double b)
 {
     AutoUnsafeCallWithABI unsafe;
     if (b == 0) {
-        if (a == 0 || mozilla::IsNaN(a)
-#ifdef XP_WIN
-            || mozilla::IsNaN(b) /* XXX MSVC miscompiles such that (NaN == 0) */
-#endif
-        )
+        if (a == 0 || mozilla::IsNaN(a))
             return JS::GenericNaN();
 
         if (mozilla::IsNegative(a) != mozilla::IsNegative(b))
             return mozilla::NegativeInfinity<double>();
         return mozilla::PositiveInfinity<double>();
     }
 
     return a / b;
diff --git a/js/src/jsmath.cpp b/js/src/jsmath.cpp
--- a/js/src/jsmath.cpp
+++ b/js/src/jsmath.cpp
@@ -891,22 +891,16 @@ js::math_sin_impl(MathCache* cache, doub
     AutoUnsafeCallWithABI unsafe;
     return cache->lookup(math_sin_uncached, x, MathCache::Sin);
 }
 
 double
 js::math_sin_uncached(double x)
 {
     AutoUnsafeCallWithABI unsafe;
-#ifdef _WIN64
-    // Workaround MSVC bug where sin(-0) is +0 instead of -0 on x64 on
-    // CPUs without FMA3 (pre-Haswell). See bug 1076670.
-    if (IsNegativeZero(x))
-        return -0.0;
-#endif
     return sin(x);
 }
 
 bool
 js::math_sin_handle(JSContext* cx, HandleValue val, MutableHandleValue res)
 {
     double in;
     if (!ToNumber(cx, val, &in))
diff --git a/js/src/util/DoubleToString.cpp b/js/src/util/DoubleToString.cpp
--- a/js/src/util/DoubleToString.cpp
+++ b/js/src/util/DoubleToString.cpp
@@ -307,21 +307,17 @@ js_dtobasestr(DtoaState* state, int base
     MOZ_ASSERT(base >= 2 && base <= 36);
 
     dval(d) = dinput;
     buffer = (char*) js_malloc(DTOBASESTR_BUFFER_SIZE);
     if (!buffer)
         return nullptr;
     p = buffer;
 
-    if (dval(d) < 0.0
-#if defined(XP_WIN)
-        && !((word0(d) & Exp_mask) == Exp_mask && ((word0(d) & Frac_mask) || word1(d))) /* Visual C++ doesn't know how to compare against NaN */
-#endif
-       ) {
+    if (dval(d) < 0.0) {
         *p++ = '-';
         dval(d) = -dval(d);
     }
 
     /* Check for Infinity and NaN */
     if ((word0(d) & Exp_mask) == Exp_mask) {
         strcpy(p, !word1(d) && !(word0(d) & Frac_mask) ? "Infinity" : "NaN");
         return buffer;
