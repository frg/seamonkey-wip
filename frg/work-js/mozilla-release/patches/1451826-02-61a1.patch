# HG changeset patch
# User Eric Faust <efaustbmo@gmail.com>
# Date 1524866551 25200
#      Fri Apr 27 15:02:31 2018 -0700
# Node ID b945db303abc6ae585aaf8d518af225225aba2ba
# Parent  9fd873b9615f6433408fc4d597d1789279ccd07c
Bug 1451826 - Part 2: Don't call parser.reportError from the BCE, as it uses the current token offset. (r=Waldo)

diff --git a/js/src/frontend/BytecodeEmitter.cpp b/js/src/frontend/BytecodeEmitter.cpp
--- a/js/src/frontend/BytecodeEmitter.cpp
+++ b/js/src/frontend/BytecodeEmitter.cpp
@@ -4497,17 +4497,17 @@ BytecodeEmitter::emitSwitch(ParseNode* p
     // After entering the scope, push the switch control.
     BreakableControl controlInfo(this, StatementKind::Switch);
 
     ptrdiff_t top = offset();
 
     // Switch bytecodes run from here till end of final case.
     uint32_t caseCount = cases->pn_count;
     if (caseCount > JS_BIT(16)) {
-        parser.reportError(JSMSG_TOO_MANY_CASES);
+        reportError(pn, JSMSG_TOO_MANY_CASES);
         return false;
     }
 
     // Try for most optimal, fall back if not dense ints.
     JSOp switchOp = JSOP_TABLESWITCH;
     uint32_t tableLength = 0;
     int32_t low, high;
     bool hasDefault = false;
@@ -9282,17 +9282,17 @@ BytecodeEmitter::emitCallOrNew(ParseNode
      * Then (or in a call case that has no explicit reference-base
      * object) we emit JSOP_UNDEFINED to produce the undefined |this|
      * value required for calls (which non-strict mode functions
      * will box into the global object).
      */
     uint32_t argc = pn->pn_count - 1;
 
     if (argc >= ARGC_LIMIT) {
-        parser.reportError(callop ? JSMSG_TOO_MANY_FUN_ARGS : JSMSG_TOO_MANY_CON_ARGS);
+        reportError(pn, callop ? JSMSG_TOO_MANY_FUN_ARGS : JSMSG_TOO_MANY_CON_ARGS);
         return false;
     }
 
     ParseNode* pn2 = pn->pn_head;
     bool spread = JOF_OPTYPE(pn->getOp()) == JOF_BYTE;
 
     if (pn2->isKind(ParseNodeKind::Name) && emitterMode == BytecodeEmitter::SelfHosting && !spread) {
         // Calls to "forceInterpreter", "callFunction",
@@ -11160,17 +11160,17 @@ BytecodeEmitter::addToSrcNoteDelta(jssrc
     }
     return true;
 }
 
 bool
 BytecodeEmitter::setSrcNoteOffset(unsigned index, unsigned which, ptrdiff_t offset)
 {
     if (!SN_REPRESENTABLE_OFFSET(offset)) {
-        parser.reportError(JSMSG_NEED_DIET, js_script_str);
+        reportError(nullptr, JSMSG_NEED_DIET, js_script_str);
         return false;
     }
 
     SrcNotesVector& notes = this->notes();
 
     /* Find the offset numbered which (i.e., skip exactly which offsets). */
     jssrcnote* sn = &notes[index];
     MOZ_ASSERT(SN_TYPE(sn) != SRC_XDELTA);
diff --git a/js/src/frontend/EitherParser.h b/js/src/frontend/EitherParser.h
--- a/js/src/frontend/EitherParser.h
+++ b/js/src/frontend/EitherParser.h
@@ -129,24 +129,16 @@ template<class Handler>
 struct HandlerIsSuperBase
 {
     static constexpr auto get() -> decltype(&Handler::isSuperBase) {
         return &Handler::isSuperBase;
     }
 };
 
 template<class TokenStream>
-struct TokenStreamReportError
-{
-    static constexpr auto get() -> decltype(&TokenStream::reportError) {
-        return &TokenStream::reportError;
-    }
-};
-
-template<class TokenStream>
 struct TokenStreamReportExtraWarning
 {
     static constexpr auto get() -> decltype(&TokenStream::reportExtraWarningErrorNumberVA) {
         return &TokenStream::reportExtraWarningErrorNumberVA;
     }
 };
 
 // Generic matchers.
@@ -244,23 +236,16 @@ class EitherParser
     bool isSuperBase(Node node) {
         InvokeMemberFunction<detail::GetParseHandler, detail::HandlerIsSuperBase,
                              Node>
             matcher { node };
         return parser.match(mozilla::Move(matcher));
     }
 
     template<typename... Args>
-    void reportError(Args&&... args) {
-        InvokeMemberFunction<detail::GetTokenStream, detail::TokenStreamReportError, Args...>
-            matcher { mozilla::Forward<Args>(args)... };
-        return parser.match(mozilla::Move(matcher));
-    }
-
-    template<typename... Args>
     MOZ_MUST_USE bool warningNoOffset(Args&&... args) {
         return parser.match(detail::ParserBaseMatcher()).warningNoOffset(mozilla::Forward<Args>(args)...);
     }
 
     template<typename... Args>
     MOZ_MUST_USE bool reportExtraWarningErrorNumberVA(Args&&... args) {
         InvokeMemberFunction<detail::GetTokenStream, detail::TokenStreamReportExtraWarning, Args...>
             matcher { mozilla::Forward<Args>(args)... };
