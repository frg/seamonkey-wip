# HG changeset patch
# User Ted Campbell <tcampbell@mozilla.com>
# Date 1534962482 14400
#      Wed Aug 22 14:28:02 2018 -0400
# Node ID a6aa090e485ef4e81fc6e090e12ab6d2019f21aa
# Parent  893f7c9a80c0640d16803a05137e7081b8ccf12d
Bug 1485347 - Part 1: Remove nTypeSets argument from JSScript::partiallyInit. r=jandem

MozReview-Commit-ID: I6xBHG8FLlq

diff --git a/js/src/vm/JSScript.cpp b/js/src/vm/JSScript.cpp
--- a/js/src/vm/JSScript.cpp
+++ b/js/src/vm/JSScript.cpp
@@ -545,25 +545,28 @@ js::XDRScript(XDRState<mode>* xdr, Handl
     } else {
         // When encoding, we do not mutate any of the JSScript or LazyScript, so
         // we can safely unwrap it here.
         sourceObject = &script->scriptSourceUnwrap();
     }
 
     if (mode == XDR_DECODE) {
         if (!JSScript::partiallyInit(cx, script, nscopes, nconsts, nobjects, ntrynotes,
-                                     nscopenotes, nyieldoffsets, nTypeSets))
+                                     nscopenotes, nyieldoffsets))
         {
             return xdr->fail(JS::TranscodeResult_Throw);
         }
 
         MOZ_ASSERT(!script->mainOffset());
         script->mainOffset_ = prologueLength;
         script->funLength_ = funLength;
 
+        MOZ_ASSERT(nTypeSets <= UINT16_MAX);
+        script->nTypeSets_ = uint16_t(nTypeSets);
+
         scriptp.set(script);
 
         if (scriptBits & (1 << Strict))
             script->bitFields_.strict_ = true;
         if (scriptBits & (1 << ExplicitUseStrict))
             script->bitFields_.explicitUseStrict_ = true;
         if (scriptBits & (1 << ContainsDynamicNameAccess))
             script->bitFields_.bindingsAccessedDynamically_ = true;
@@ -2753,31 +2756,28 @@ AllocScriptData(JSContext* cx, size_t si
         return nullptr;
     MOZ_ASSERT(size_t(data) % sizeof(Value) == 0);
     return data;
 }
 
 /* static */ bool
 JSScript::partiallyInit(JSContext* cx, HandleScript script, uint32_t nscopes,
                         uint32_t nconsts, uint32_t nobjects, uint32_t ntrynotes,
-                        uint32_t nscopenotes, uint32_t nyieldoffsets, uint32_t nTypeSets)
+                        uint32_t nscopenotes, uint32_t nyieldoffsets)
 {
     assertSameCompartment(cx, script);
 
     size_t size = ScriptDataSize(nscopes, nconsts, nobjects, ntrynotes,
                                  nscopenotes, nyieldoffsets);
     script->data = AllocScriptData(cx, size);
     if (size && !script->data)
         return false;
 
     script->dataSize_ = size;
 
-    MOZ_ASSERT(nTypeSets <= UINT16_MAX);
-    script->nTypeSets_ = uint16_t(nTypeSets);
-
     uint8_t* cursor = script->data;
 
     // There must always be at least 1 scope, the body scope.
     MOZ_ASSERT(nscopes != 0);
     cursor += sizeof(ScopeArray);
 
     if (nconsts != 0) {
         script->setHasArray(CONSTS);
@@ -2858,23 +2858,24 @@ JSScript::initFunctionPrototype(JSContex
                                 HandleFunction functionProto)
 {
     uint32_t numScopes = 1;
     uint32_t numConsts = 0;
     uint32_t numObjects = 0;
     uint32_t numTryNotes = 0;
     uint32_t numScopeNotes = 0;
     uint32_t numYieldAndAwaitOffsets = 0;
-    uint32_t numTypeSets = 0;
     if (!partiallyInit(cx, script, numScopes, numConsts, numObjects, numTryNotes,
-                       numScopeNotes, numYieldAndAwaitOffsets, numTypeSets))
+                       numScopeNotes, numYieldAndAwaitOffsets))
     {
         return false;
     }
 
+    script->nTypeSets_ = 0;
+
     RootedScope enclosing(cx, &cx->global()->emptyGlobalScope());
     Scope* functionProtoScope = FunctionScope::create(cx, nullptr, false, false, functionProto,
                                                       enclosing);
     if (!functionProtoScope)
         return false;
     script->scopes()->vector[0].init(functionProtoScope);
 
     uint32_t codeLength = 1;
@@ -2973,24 +2974,24 @@ JSScript::fullyInitFromEmitter(JSContext
     uint32_t prologueLength = bce->prologueOffset();
     uint32_t nsrcnotes;
     if (!bce->finishTakingSrcNotes(&nsrcnotes))
         return false;
     uint32_t natoms = bce->atomIndices->count();
     if (!partiallyInit(cx, script,
                        bce->scopeList.length(), bce->constList.length(), bce->objectList.length,
                        bce->tryNoteList.length(), bce->scopeNoteList.length(),
-                       bce->yieldAndAwaitOffsetList.length(), bce->typesetCount))
+                       bce->yieldAndAwaitOffsetList.length()))
     {
         return false;
     }
 
     MOZ_ASSERT(script->mainOffset() == 0);
     script->mainOffset_ = prologueLength;
-
+    script->nTypeSets_ = bce->typesetCount;
     script->lineno_ = bce->firstLine;
 
     if (!script->createScriptData(cx, prologueLength + mainLength, nsrcnotes, natoms))
         return false;
 
     // Any fallible operation after JSScript::createScriptData should reset
     // JSScript.scriptData_, in order to treat this script as uncompleted,
     // in JSScript::isUncompleted.
diff --git a/js/src/vm/JSScript.h b/js/src/vm/JSScript.h
--- a/js/src/vm/JSScript.h
+++ b/js/src/vm/JSScript.h
@@ -1212,18 +1212,17 @@ class JSScript : public js::gc::TenuredC
                             uint32_t toStringStart, uint32_t toStringEnd);
 
     // Three ways ways to initialize a JSScript. Callers of partiallyInit()
     // are responsible for notifying the debugger after successfully creating
     // any kind (function or other) of new JSScript.  However, callers of
     // fullyInitFromEmitter() do not need to do this.
     static bool partiallyInit(JSContext* cx, JS::Handle<JSScript*> script,
                               uint32_t nscopes, uint32_t nconsts, uint32_t nobjects,
-                              uint32_t ntrynotes, uint32_t nscopenotes, uint32_t nyieldoffsets,
-                              uint32_t nTypeSets);
+                              uint32_t ntrynotes, uint32_t nscopenotes, uint32_t nyieldoffsets);
 
   private:
     static void initFromFunctionBox(js::HandleScript script, js::frontend::FunctionBox* funbox);
     static void initFromModuleContext(js::HandleScript script);
 
   public:
     static bool fullyInitFromEmitter(JSContext* cx, js::HandleScript script,
                                      js::frontend::BytecodeEmitter* bce);
