# HG changeset patch
# User Daniel Holbert <dholbert@cs.stanford.edu>
# Date 1536601241 0
# Node ID 7b420ac55483e977a85fd7a342d60e1a840d776b
# Parent  b5a732c19d57c45def3af12a23e3e3353ee885a0
Bug 1486086: Switch to use logical axes, for stale physical-axis-based flex-item border/padding calculation. r=mats

Differential Revision: https://phabricator.services.mozilla.com/D4974

diff --git a/layout/generic/nsFlexContainerFrame.cpp b/layout/generic/nsFlexContainerFrame.cpp
--- a/layout/generic/nsFlexContainerFrame.cpp
+++ b/layout/generic/nsFlexContainerFrame.cpp
@@ -3912,20 +3912,18 @@ nsFlexContainerFrame::SizeItemInCrossAxi
     MeasureAscentAndBSizeForFlexItem(aItem, aPresContext, aChildReflowInput);
 
   // Save the sizing info that we learned from this reflow
   // -----------------------------------------------------
 
   // Tentatively store the child's desired content-box cross-size.
   // Note that childDesiredSize is the border-box size, so we have to
   // subtract border & padding to get the content-box size.
-  // (Note that at this point in the code, we know our cross axis is vertical,
-  // so we don't bother with making aAxisTracker pick the cross-axis component
-  // for us.)
-  nscoord crossAxisBorderPadding = aItem.GetBorderPadding().TopBottom();
+  nscoord crossAxisBorderPadding =
+    aItem.GetBorderPaddingSizeInAxis(aAxisTracker.GetCrossAxis());
   if (reflowResult.BSize() < crossAxisBorderPadding) {
     // Child's requested size isn't large enough for its border/padding!
     // This is OK for the trivial nsFrame::Reflow() impl, but other frame
     // classes should know better. So, if we get here, the child had better be
     // an instance of nsFrame (i.e. it should return null from GetType()).
     // XXXdholbert Once we've fixed bug 765861, we should upgrade this to an
     // assertion that trivially passes if bug 765861's flag has been flipped.
     NS_WARNING_ASSERTION(
diff --git a/layout/reftests/w3c-css/submitted/flexbox/flexbox-writing-mode-016-ref.html b/layout/reftests/w3c-css/submitted/flexbox/flexbox-writing-mode-016-ref.html
new file mode 100644
--- /dev/null
+++ b/layout/reftests/w3c-css/submitted/flexbox/flexbox-writing-mode-016-ref.html
@@ -0,0 +1,136 @@
+<!DOCTYPE html>
+<!--
+     Any copyright is dedicated to the Public Domain.
+     http://creativecommons.org/publicdomain/zero/1.0/
+-->
+<html>
+<head>
+  <title>CSS Reftest Reference</title>
+  <meta charset="utf-8">
+  <link rel="author" title="Daniel Holbert" href="mailto:dholbert@mozilla.com">
+  <style>
+  .container {
+    display: block;
+    border: 2px solid purple;
+    margin: 3px;
+    /* This red should't be visible, because each container should shrinkwrap
+       its sole child (and the child should cover up this background). */
+    background: red;
+    /* Float the containers, to test in "rows", with 1 row per writing-mode. */
+    float: left;
+  }
+  br { clear: both; }
+
+  .container > * {
+    width: 10px;
+    height: 10px;
+    background: teal;
+    border: 1px solid yellow;
+  }
+  .container > * > * {
+    background: pink;
+    height: 4px;
+    width: 4px;
+    border: 1px solid black;
+  }
+
+  .pad_top    { padding-top:    3px; }
+  .pad_right  { padding-right:  4px; }
+  .pad_bottom { padding-bottom: 5px; }
+  .pad_left   { padding-left:   6px; }
+
+  .hl  { writing-mode: horizontal-tb;  direction: ltr; }
+  .hr  { writing-mode: horizontal-tb;  direction: rtl; }
+  .vl  { writing-mode: vertical-lr;    direction: ltr; }
+  .vr  { writing-mode: vertical-rl;    direction: ltr; }
+  .vl_rtl { writing-mode: vertical-lr; direction: rtl; }
+  .vr_rtl { writing-mode: vertical-rl; direction: rtl; }
+  </style>
+</head>
+<body>
+  <!-- Here, we test padding on each side of a flex item, across 6 different
+       writing-mode combinations (writing-mode X direction). -->
+  <div class="container hl">
+    <div class="pad_top"><div></div></div>
+  </div>
+  <div class="container hl">
+    <div class="pad_right"><div></div></div>
+  </div>
+  <div class="container hl">
+    <div class="pad_bottom"><div></div></div>
+  </div>
+  <div class="container hl">
+    <div class="pad_left"><div></div></div>
+  </div>
+  <br>
+
+  <div class="container hr">
+    <div class="pad_top"><div></div></div>
+  </div>
+  <div class="container hr">
+    <div class="pad_right"><div></div></div>
+  </div>
+  <div class="container hr">
+    <div class="pad_bottom"><div></div></div>
+  </div>
+  <div class="container hr">
+    <div class="pad_left"><div></div></div>
+  </div>
+  <br>
+
+  <div class="container vl">
+    <div class="pad_top"><div></div></div>
+  </div>
+  <div class="container vl">
+    <div class="pad_right"><div></div></div>
+  </div>
+  <div class="container vl">
+    <div class="pad_bottom"><div></div></div>
+  </div>
+  <div class="container vl">
+    <div class="pad_left"><div></div></div>
+  </div>
+  <br>
+
+  <div class="container vr">
+    <div class="pad_top"><div></div></div>
+  </div>
+  <div class="container vr">
+    <div class="pad_right"><div></div></div>
+  </div>
+  <div class="container vr">
+    <div class="pad_bottom"><div></div></div>
+  </div>
+  <div class="container vr">
+    <div class="pad_left"><div></div></div>
+  </div>
+  <br>
+
+  <div class="container vl_rtl">
+    <div class="pad_top"><div></div></div>
+  </div>
+  <div class="container vl_rtl">
+    <div class="pad_right"><div></div></div>
+  </div>
+  <div class="container vl_rtl">
+    <div class="pad_bottom"><div></div></div>
+  </div>
+  <div class="container vl_rtl">
+    <div class="pad_left"><div></div></div>
+  </div>
+  <br>
+
+  <div class="container vr_rtl">
+    <div class="pad_top"><div></div></div>
+  </div>
+  <div class="container vr_rtl">
+    <div class="pad_right"><div></div></div>
+  </div>
+  <div class="container vr_rtl">
+    <div class="pad_bottom"><div></div></div>
+  </div>
+  <div class="container vr_rtl">
+    <div class="pad_left"><div></div></div>
+  </div>
+</body>
+</html>
diff --git a/layout/reftests/w3c-css/submitted/flexbox/flexbox-writing-mode-016.html b/layout/reftests/w3c-css/submitted/flexbox/flexbox-writing-mode-016.html
new file mode 100644
--- /dev/null
+++ b/layout/reftests/w3c-css/submitted/flexbox/flexbox-writing-mode-016.html
@@ -0,0 +1,144 @@
+<!DOCTYPE html>
+<!--
+     Any copyright is dedicated to the Public Domain.
+     http://creativecommons.org/publicdomain/zero/1.0/
+-->
+<html>
+<head>
+  <title>
+    CSS Test: Testing auto-sized flex containers
+    with various 'writing-mode' values
+    and various padding amounts on flex items.
+  </title>
+  <meta charset="utf-8">
+  <link rel="author" title="Daniel Holbert" href="mailto:dholbert@mozilla.com">
+  <link rel="help" href="https://www.w3.org/TR/css-flexbox-1/#flex-direction-property">
+  <link rel="help" href="https://www.w3.org/TR/css-writing-modes-3/#propdef-writing-mode">
+  <link rel="match" href="flexbox-writing-mode-016-ref.html">
+  <style>
+  .container {
+    display: flex;
+    flex-direction: row;
+    border: 2px solid purple;
+    margin: 3px;
+    /* This red should't be visible, because each container should shrinkwrap
+       its sole child (and the child should cover up this background). */
+    background: red;
+    /* Float the containers, to test in "rows", with 1 row per writing-mode. */
+    float: left;
+  }
+  br { clear: both; }
+
+  .container > * {
+    width: 10px;
+    height: 10px;
+    background: teal;
+    border: 1px solid yellow;
+  }
+  .container > * > * {
+    background: pink;
+    height: 4px;
+    width: 4px;
+    border: 1px solid black;
+  }
+
+  .pad_top    { padding-top:    3px; }
+  .pad_right  { padding-right:  4px; }
+  .pad_bottom { padding-bottom: 5px; }
+  .pad_left   { padding-left:   6px; }
+
+  .hl  { writing-mode: horizontal-tb;  direction: ltr; }
+  .hr  { writing-mode: horizontal-tb;  direction: rtl; }
+  .vl  { writing-mode: vertical-lr;    direction: ltr; }
+  .vr  { writing-mode: vertical-rl;    direction: ltr; }
+  .vl_rtl { writing-mode: vertical-lr; direction: rtl; }
+  .vr_rtl { writing-mode: vertical-rl; direction: rtl; }
+  </style>
+</head>
+<body>
+  <!-- Here, we test padding on each side of a flex item, across 6 different
+       writing-mode combinations (writing-mode X direction). -->
+  <div class="container hl">
+    <div class="pad_top"><div></div></div>
+  </div>
+  <div class="container hl">
+    <div class="pad_right"><div></div></div>
+  </div>
+  <div class="container hl">
+    <div class="pad_bottom"><div></div></div>
+  </div>
+  <div class="container hl">
+    <div class="pad_left"><div></div></div>
+  </div>
+  <br>
+
+  <div class="container hr">
+    <div class="pad_top"><div></div></div>
+  </div>
+  <div class="container hr">
+    <div class="pad_right"><div></div></div>
+  </div>
+  <div class="container hr">
+    <div class="pad_bottom"><div></div></div>
+  </div>
+  <div class="container hr">
+    <div class="pad_left"><div></div></div>
+  </div>
+  <br>
+
+  <div class="container vl">
+    <div class="pad_top"><div></div></div>
+  </div>
+  <div class="container vl">
+    <div class="pad_right"><div></div></div>
+  </div>
+  <div class="container vl">
+    <div class="pad_bottom"><div></div></div>
+  </div>
+  <div class="container vl">
+    <div class="pad_left"><div></div></div>
+  </div>
+  <br>
+
+  <div class="container vr">
+    <div class="pad_top"><div></div></div>
+  </div>
+  <div class="container vr">
+    <div class="pad_right"><div></div></div>
+  </div>
+  <div class="container vr">
+    <div class="pad_bottom"><div></div></div>
+  </div>
+  <div class="container vr">
+    <div class="pad_left"><div></div></div>
+  </div>
+  <br>
+
+  <div class="container vl_rtl">
+    <div class="pad_top"><div></div></div>
+  </div>
+  <div class="container vl_rtl">
+    <div class="pad_right"><div></div></div>
+  </div>
+  <div class="container vl_rtl">
+    <div class="pad_bottom"><div></div></div>
+  </div>
+  <div class="container vl_rtl">
+    <div class="pad_left"><div></div></div>
+  </div>
+  <br>
+
+  <div class="container vr_rtl">
+    <div class="pad_top"><div></div></div>
+  </div>
+  <div class="container vr_rtl">
+    <div class="pad_right"><div></div></div>
+  </div>
+  <div class="container vr_rtl">
+    <div class="pad_bottom"><div></div></div>
+  </div>
+  <div class="container vr_rtl">
+    <div class="pad_left"><div></div></div>
+  </div>
+</body>
+</html>
diff --git a/layout/reftests/w3c-css/submitted/flexbox/reftest.list b/layout/reftests/w3c-css/submitted/flexbox/reftest.list
--- a/layout/reftests/w3c-css/submitted/flexbox/reftest.list
+++ b/layout/reftests/w3c-css/submitted/flexbox/reftest.list
@@ -223,16 +223,17 @@ fails == flexbox-min-height-auto-002b.ht
 == flexbox-writing-mode-008.html flexbox-writing-mode-008-ref.html
 == flexbox-writing-mode-009.html flexbox-writing-mode-009-ref.html
 == flexbox-writing-mode-010.html flexbox-writing-mode-010-ref.html
 == flexbox-writing-mode-011.html flexbox-writing-mode-011-ref.html
 == flexbox-writing-mode-012.html flexbox-writing-mode-012-ref.html
 == flexbox-writing-mode-013.html flexbox-writing-mode-013-ref.html
 == flexbox-writing-mode-014.html flexbox-writing-mode-014-ref.html
 == flexbox-writing-mode-015.html flexbox-writing-mode-015-ref.html
+== flexbox-writing-mode-016.html flexbox-writing-mode-016-ref.html
 
 # Single-line size clamping
 == flexbox-single-line-clamp-1.html flexbox-single-line-clamp-1-ref.html
 == flexbox-single-line-clamp-2.html flexbox-single-line-clamp-2-ref.html
 == flexbox-single-line-clamp-3.html flexbox-single-line-clamp-3-ref.html
 
 # Flexbox as an absolute containing block.
 == position-absolute-containing-block-001.html position-absolute-containing-block-001-ref.html
