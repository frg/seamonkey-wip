# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1526994157 -7200
#      Tue May 22 15:02:37 2018 +0200
# Node ID 98ad6a9038624b04f643e141745a0d6be2c892cd
# Parent  9908d03b685cc2829476735ab12ae830c9315965
Bug 1461938 part 13 - Move dtoaCache and newProxyCache from JSCompartment to JS::Realm. r=anba

diff --git a/js/src/gc/GC.cpp b/js/src/gc/GC.cpp
--- a/js/src/gc/GC.cpp
+++ b/js/src/gc/GC.cpp
@@ -4065,18 +4065,18 @@ GCRuntime::purgeRuntimeForMinorGC()
     rt->caches().purgeForMinorGC(rt);
 }
 
 void
 GCRuntime::purgeRuntime()
 {
     gcstats::AutoPhase ap(stats(), gcstats::PhaseKind::PURGE);
 
-    for (GCCompartmentsIter comp(rt); !comp.done(); comp.next())
-        comp->purge();
+    for (GCRealmsIter realm(rt); !realm.done(); realm.next())
+        realm->purge();
 
     for (GCZonesIter zone(rt); !zone.done(); zone.next()) {
         zone->atomCache().clearAndShrink();
         zone->externalStringCache().purge();
         zone->functionToStringCache().purge();
     }
 
     JSContext* cx = rt->mainContextFromOwnThread();
diff --git a/js/src/gc/PrivateIterators-inl.h b/js/src/gc/PrivateIterators-inl.h
--- a/js/src/gc/PrivateIterators-inl.h
+++ b/js/src/gc/PrivateIterators-inl.h
@@ -84,17 +84,18 @@ class GCZonesIter
         MOZ_ASSERT(!done());
         return zone;
     }
 
     operator JS::Zone*() const { return get(); }
     JS::Zone* operator->() const { return get(); }
 };
 
-typedef CompartmentsIterT<GCZonesIter> GCCompartmentsIter;
+using GCCompartmentsIter = CompartmentsIterT<GCZonesIter>;
+using GCRealmsIter = RealmsIterT<GCZonesIter>;
 
 /* Iterates over all zones in the current sweep group. */
 class SweepGroupZonesIter {
     JS::Zone* current;
 
   public:
     explicit SweepGroupZonesIter(JSRuntime* rt) {
         MOZ_ASSERT(CurrentThreadIsPerformingGC());
diff --git a/js/src/jsapi-tests/testIndexToString.cpp b/js/src/jsapi-tests/testIndexToString.cpp
--- a/js/src/jsapi-tests/testIndexToString.cpp
+++ b/js/src/jsapi-tests/testIndexToString.cpp
@@ -51,17 +51,17 @@ static const struct TestPair {
 BEGIN_TEST(testIndexToString)
 {
     for (size_t i = 0, sz = ArrayLength(tests); i < sz; i++) {
         uint32_t u = tests[i].num;
         JSString* str = js::IndexToString(cx, u);
         CHECK(str);
 
         if (!js::StaticStrings::hasUint(u))
-            CHECK(cx->compartment()->dtoaCache.lookup(10, u) == str);
+            CHECK(cx->realm()->dtoaCache.lookup(10, u) == str);
 
         bool match = false;
         CHECK(JS_StringEqualsAscii(cx, str, tests[i].expected, &match));
         CHECK(match);
     }
 
     return true;
 }
diff --git a/js/src/jsnum.cpp b/js/src/jsnum.cpp
--- a/js/src/jsnum.cpp
+++ b/js/src/jsnum.cpp
@@ -563,30 +563,30 @@ ToCStringBuf::~ToCStringBuf()
 {
     js_free(dbuf);
 }
 
 MOZ_ALWAYS_INLINE
 static JSFlatString*
 LookupDtoaCache(JSContext* cx, double d)
 {
-    if (JSCompartment* comp = cx->compartment()) {
-        if (JSFlatString* str = comp->dtoaCache.lookup(10, d))
+    if (Realm* realm = cx->realm()) {
+        if (JSFlatString* str = realm->dtoaCache.lookup(10, d))
             return str;
     }
 
     return nullptr;
 }
 
 MOZ_ALWAYS_INLINE
 static void
 CacheNumber(JSContext* cx, double d, JSFlatString* str)
 {
-    if (JSCompartment* comp = cx->compartment())
-        comp->dtoaCache.cache(10, d, str);
+    if (Realm* realm = cx->realm())
+        realm->dtoaCache.cache(10, d, str);
 }
 
 MOZ_ALWAYS_INLINE
 static JSFlatString*
 LookupInt32ToString(JSContext* cx, int32_t si)
 {
     if (si >= 0 && StaticStrings::hasInt(si))
         return cx->staticStrings().getInt(si);
@@ -1322,40 +1322,40 @@ template <AllowGC allowGC>
 static JSString*
 NumberToStringWithBase(JSContext* cx, double d, int base)
 {
     MOZ_ASSERT(2 <= base && base <= 36);
 
     ToCStringBuf cbuf;
     char* numStr;
 
-    JSCompartment* comp = cx->compartment();
+    Realm* realm = cx->realm();
 
     int32_t i;
     bool isBase10Int = false;
     if (mozilla::NumberEqualsInt32(d, &i)) {
         isBase10Int = (base == 10);
         if (isBase10Int && StaticStrings::hasInt(i))
             return cx->staticStrings().getInt(i);
         if (unsigned(i) < unsigned(base)) {
             if (i < 10)
                 return cx->staticStrings().getInt(i);
             char16_t c = 'a' + i - 10;
             MOZ_ASSERT(StaticStrings::hasUnit(c));
             return cx->staticStrings().getUnit(c);
         }
 
-        if (JSFlatString* str = comp->dtoaCache.lookup(base, d))
+        if (JSFlatString* str = realm->dtoaCache.lookup(base, d))
             return str;
 
         size_t len;
         numStr = Int32ToCString(&cbuf, i, &len, base);
         MOZ_ASSERT(!cbuf.dbuf && numStr >= cbuf.sbuf && numStr < cbuf.sbuf + cbuf.sbufSize);
     } else {
-        if (JSFlatString* str = comp->dtoaCache.lookup(base, d))
+        if (JSFlatString* str = realm->dtoaCache.lookup(base, d))
             return str;
 
         numStr = FracNumberToCString(cx, &cbuf, d, base);
         if (!numStr) {
             ReportOutOfMemory(cx);
             return nullptr;
         }
         MOZ_ASSERT_IF(base == 10,
@@ -1366,17 +1366,17 @@ NumberToStringWithBase(JSContext* cx, do
 
     JSFlatString* s = NewStringCopyZ<allowGC>(cx, numStr);
     if (!s)
         return nullptr;
 
     if (isBase10Int && i >= 0)
         s->maybeInitializeIndex(i);
 
-    comp->dtoaCache.cache(base, d, s);
+    realm->dtoaCache.cache(base, d, s);
     return s;
 }
 
 template <AllowGC allowGC>
 JSString*
 js::NumberToString(JSContext* cx, double d)
 {
     return NumberToStringWithBase<allowGC>(cx, d, 10);
@@ -1417,32 +1417,32 @@ js::NumberToAtom(JSContext* cx, double d
 }
 
 JSFlatString*
 js::IndexToString(JSContext* cx, uint32_t index)
 {
     if (StaticStrings::hasUint(index))
         return cx->staticStrings().getUint(index);
 
-    JSCompartment* c = cx->compartment();
-    if (JSFlatString* str = c->dtoaCache.lookup(10, index))
+    Realm* realm = cx->realm();
+    if (JSFlatString* str = realm->dtoaCache.lookup(10, index))
         return str;
 
     Latin1Char buffer[JSFatInlineString::MAX_LENGTH_LATIN1 + 1];
     RangedPtr<Latin1Char> end(buffer + JSFatInlineString::MAX_LENGTH_LATIN1,
                               buffer, JSFatInlineString::MAX_LENGTH_LATIN1 + 1);
     *end = '\0';
     RangedPtr<Latin1Char> start = BackfillIndexInCharBuffer(index, end);
 
     mozilla::Range<const Latin1Char> chars(start.get(), end - start);
     JSInlineString* str = NewInlineString<CanGC>(cx, chars);
     if (!str)
         return nullptr;
 
-    c->dtoaCache.cache(10, index, str);
+    realm->dtoaCache.cache(10, index, str);
     return str;
 }
 
 bool JS_FASTCALL
 js::NumberValueToStringBuffer(JSContext* cx, const Value& v, StringBuffer& sb)
 {
     /* Convert to C-string. */
     ToCStringBuf cbuf;
diff --git a/js/src/vm/JSCompartment.cpp b/js/src/vm/JSCompartment.cpp
--- a/js/src/vm/JSCompartment.cpp
+++ b/js/src/vm/JSCompartment.cpp
@@ -754,17 +754,19 @@ JSCompartment::sweepAfterMinorGC(JSTrace
 {
     globalWriteBarriered = 0;
 
     InnerViewTable& table = innerViews.get();
     if (table.needsSweepAfterMinorGC())
         table.sweepAfterMinorGC();
 
     crossCompartmentWrappers.sweepAfterMinorGC(trc);
-    dtoaCache.purge();
+
+    Realm* realm = JS::GetRealmForCompartment(this);
+    realm->dtoaCache.purge();
 }
 
 void
 JSCompartment::sweepSavedStacks()
 {
     savedStacks_.sweep();
 }
 
@@ -897,17 +899,17 @@ JSCompartment::fixupCrossCompartmentWrap
 
 void
 JSCompartment::fixupAfterMovingGC()
 {
     MOZ_ASSERT(zone()->isGCCompacting());
 
     Realm* realm = JS::GetRealmForCompartment(this);
 
-    purge();
+    realm->purge();
     realm->fixupGlobal();
     objectGroups.fixupTablesAfterMovingGC();
     realm->fixupScriptMapsAfterMovingGC();
 
     // Sweep the wrapper map to update values (wrapper objects) in this
     // compartment that may have been moved.
     sweepCrossCompartmentWrappers();
 }
@@ -989,17 +991,17 @@ Realm::checkScriptMapsAfterMovingGC()
             auto ptr = debugScriptMap->lookup(script);
             MOZ_RELEASE_ASSERT(ptr.found() && &*ptr == &r.front());
         }
     }
 }
 #endif
 
 void
-JSCompartment::purge()
+Realm::purge()
 {
     dtoaCache.purge();
     newProxyCache.purge();
     objectGroups.purge();
     iteratorCache.clearAndShrink();
     arraySpeciesLookup.purge();
 }
 
diff --git a/js/src/vm/JSCompartment.h b/js/src/vm/JSCompartment.h
--- a/js/src/vm/JSCompartment.h
+++ b/js/src/vm/JSCompartment.h
@@ -849,28 +849,23 @@ struct JSCompartment
     void sweepSavedStacks();
     void sweepSelfHostingScriptSource();
     void sweepJitCompartment();
     void sweepRegExps();
     void sweepDebugEnvironments();
     void sweepNativeIterators();
     void sweepTemplateObjects();
 
-    void purge();
-
     static void fixupCrossCompartmentWrappersAfterMovingGC(JSTracer* trc);
     void fixupAfterMovingGC();
 
     js::SavedStacks& savedStacks() { return savedStacks_; }
 
     void findOutgoingEdges(js::gc::ZoneComponentFinder& finder);
 
-    js::DtoaCache dtoaCache;
-    js::NewProxyCache newProxyCache;
-
     // Random number generator for Math.random().
     mozilla::Maybe<mozilla::non_crypto::XorShift128PlusRNG> randomNumberGenerator;
 
     // Initialize randomNumberGenerator if needed.
     void ensureRandomNumberGenerator();
 
   private:
     mozilla::non_crypto::XorShift128PlusRNG randomKeyGenerator_;
@@ -879,20 +874,16 @@ struct JSCompartment
     js::HashNumber randomHashCode();
 
     mozilla::HashCodeScrambler randomHashCodeScrambler();
 
     static size_t offsetOfRegExps() {
         return offsetof(JSCompartment, regExps);
     }
 
-  private:
-    JSCompartment* thisForCtor() { return this; }
-
-  public:
     //
     // The Debugger observes execution on a frame-by-frame basis. The
     // invariants of JSCompartment's debug mode bits, JSScript::isDebuggee,
     // InterpreterFrame::isDebuggee, and BaselineFrame::isDebuggee are
     // enumerated below.
     //
     // 1. When a compartment's isDebuggee() == true, relazification and lazy
     //    parsing are disabled.
@@ -1069,16 +1060,19 @@ class JS::Realm : public JSCompartment
     unsigned enterRealmDepth_ = 0;
 
     bool isAtomsRealm_ = false;
 
   public:
     // WebAssembly state for the realm.
     js::wasm::Realm wasm;
 
+    js::DtoaCache dtoaCache;
+    js::NewProxyCache newProxyCache;
+
     js::ScriptCountsMap* scriptCountsMap = nullptr;
     js::ScriptNameMap* scriptNameMap = nullptr;
     js::DebugScriptMap* debugScriptMap = nullptr;
 
     Realm(JS::Zone* zone, const JS::RealmOptions& options);
     ~Realm();
 
     MOZ_MUST_USE bool init(JSContext* maybecx);
@@ -1152,16 +1146,18 @@ class JS::Realm : public JSCompartment
     /*
      * This method clears out tables of roots in preparation for the final GC.
      */
     void finishRoots();
 
     void clearScriptCounts();
     void clearScriptNames();
 
+    void purge();
+
     void fixupScriptMapsAfterMovingGC();
 
 #ifdef JSGC_HASH_TABLE_CHECKS
     void checkScriptMapsAfterMovingGC();
 #endif
 
     // Add a name to [[VarNames]].  Reports OOM on failure.
     MOZ_MUST_USE bool addToVarNames(JSContext* cx, JS::Handle<JSAtom*> name);
diff --git a/js/src/vm/ProxyObject.cpp b/js/src/vm/ProxyObject.cpp
--- a/js/src/vm/ProxyObject.cpp
+++ b/js/src/vm/ProxyObject.cpp
@@ -163,31 +163,32 @@ ProxyObject::nuke()
 }
 
 /* static */ JS::Result<ProxyObject*, JS::OOM&>
 ProxyObject::create(JSContext* cx, const Class* clasp, Handle<TaggedProto> proto,
                     gc::AllocKind allocKind, NewObjectKind newKind)
 {
     MOZ_ASSERT(clasp->isProxy());
 
-    JSCompartment* comp = cx->compartment();
+    Realm* realm = cx->realm();
     RootedObjectGroup group(cx);
     RootedShape shape(cx);
 
     // Try to look up the group and shape in the NewProxyCache.
-    if (!comp->newProxyCache.lookup(clasp, proto, group.address(), shape.address())) {
+    if (!realm->newProxyCache.lookup(clasp, proto, group.address(), shape.address())) {
         group = ObjectGroup::defaultNewGroup(cx, clasp, proto, nullptr);
         if (!group)
             return cx->alreadyReportedOOM();
 
         shape = EmptyShape::getInitialShape(cx, clasp, proto, /* nfixed = */ 0);
         if (!shape)
             return cx->alreadyReportedOOM();
 
-        comp->newProxyCache.add(group, shape);
+        MOZ_ASSERT(group->realm() == realm);
+        realm->newProxyCache.add(group, shape);
     }
 
     gc::InitialHeap heap = GetInitialHeap(newKind, clasp);
     debugCheckNewObject(group, shape, allocKind, heap);
 
     JSObject* obj = js::Allocate<JSObject>(cx, allocKind, /* nDynamicSlots = */ 0, heap, clasp);
     if (!obj)
         return cx->alreadyReportedOOM();
