# HG changeset patch
# User MakeMyDay <makemyday@gmx-topmail.de>
# Date 1514574149 -3600
# Node ID 34c27412bf320d5a061db968fdab398a2cab06cf
# Parent  1345fa82caf288bae98011969a7bfca20b16661b
Bug 1324454 - Make long item descriptions in conflict dialog scrolling to not overflow the dialog controls. r=philipp

diff --git a/calendar/base/content/dialogs/calendar-conflicts-dialog.xul b/calendar/base/content/dialogs/calendar-conflicts-dialog.xul
--- a/calendar/base/content/dialogs/calendar-conflicts-dialog.xul
+++ b/calendar/base/content/dialogs/calendar-conflicts-dialog.xul
@@ -14,19 +14,22 @@
         persist="screenX screenY"
         xmlns="http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul">
   <script type="application/javascript" src="chrome://calendar/content/mouseoverPreviews.js"/>
   <script type="application/javascript"><![CDATA[
     Components.utils.import("resource://calendar/modules/calUtils.jsm");
     function onLoad() {
         let docEl = document.documentElement;
         let item = window.arguments[0].item;
-        let vbox = getPreviewForEvent(item);
+        let vbox = getPreviewForItem(item, false);
+        if (vbox) {
+            document.getElementById("item-box").replaceWith(vbox);
+        }
+
         let descr = document.getElementById("conflicts-description");
-        descr.parentNode.insertBefore(vbox, descr);
 
         // TODO These strings should move to DTD files, but we don't want to
         // disrupt string freeze right now. For that matter, this dialog
         // should be reworked!
         docEl.title =  cal.calGetString("calendar", "itemModifiedOnServerTitle");
         descr.textContent = cal.calGetString("calendar", "itemModifiedOnServer");
 
         if (window.arguments[0].mode == "modify") {
@@ -46,13 +49,14 @@
         window.arguments[0].overwrite = true;
     }
 
     function onCancel() {
         window.arguments[0].overwrite = false;
     }
   ]]></script>
 
-  <vbox id="conflicts-vbox">
+  <vbox id="conflicts-vbox" flex="1">
+    <vbox id="item-box" flex="1"/>
     <description id="conflicts-description"
                  style="max-width: 40em; margin-top: 1ex"/>
   </vbox>
 </dialog>
diff --git a/calendar/base/themes/common/calendar-views.css b/calendar/base/themes/common/calendar-views.css
--- a/calendar/base/themes/common/calendar-views.css
+++ b/calendar/base/themes/common/calendar-views.css
@@ -787,16 +787,21 @@ calendar-event-box[gripBars="both"]:not(
 }
 
 .tooltipBody {
     font-weight: normal;
     white-space: pre-wrap;
     margin: 0pt;
 }
 
+#conflicts-vbox .tooltipBody {
+    overflow: auto;
+    min-height: 250px;
+}
+
 #calendar-view-context-menu[type="event"] .todo-only,
 #calendar-view-context-menu[type="todo"] .event-only,
 #calendar-view-context-menu[type="mixed"] .event-only,
 #calendar-view-context-menu[type="mixed"] .todo-only,
 #calendar-item-context-menu[type="event"] .todo-only,
 #calendar-item-context-menu[type="todo"] .event-only,
 #calendar-item-context-menu[type="mixed"] .event-only,
 #calendar-item-context-menu[type="mixed"] .todo-only {
diff --git a/calendar/resources/content/mouseoverPreviews.js b/calendar/resources/content/mouseoverPreviews.js
--- a/calendar/resources/content/mouseoverPreviews.js
+++ b/calendar/resources/content/mouseoverPreviews.js
@@ -6,64 +6,141 @@
  * Code which generates event and task (todo) preview tooltips/titletips
  *  when the mouse hovers over either the event list, the task list, or
  *  an event or task box in one of the grid views.
  *
  *   (Portions of this code were previously in calendar.js and unifinder.js,
  *   some of it duplicated.)
  */
 
-/* exported onMouseOverItem, showToolTip */
+/* exported onMouseOverItem, showToolTip, getPreviewForItem,
+             getEventStatusString, getToDoStatusString */
 
-/** PUBLIC
- *
- *  This changes the mouseover preview based on the start and end dates
- *  of an occurrence of a (one-time or recurring) calEvent or calToDo.
- *  Used by all grid views.
+/**
+ * PUBLIC: This changes the mouseover preview based on the start and end dates
+ * of an occurrence of a (one-time or recurring) calEvent or calToDo.
+ * Used by all grid views.
  */
 
 Components.utils.import("resource://calendar/modules/calUtils.jsm");
 
+/**
+ * PUBLIC: Displays a tooltip with details when hovering over an item in the views
+ *
+ * @param   {DOMEvent} occurrenceBoxMouseEvent  the triggering event
+ * @returns {boolean}                           true, if the tooltip is displayed
+ */
 function onMouseOverItem(occurrenceBoxMouseEvent) {
     if ("occurrence" in occurrenceBoxMouseEvent.currentTarget) {
         // occurrence of repeating event or todo
         let occurrence = occurrenceBoxMouseEvent.currentTarget.occurrence;
         const toolTip = document.getElementById("itemTooltip");
         return showToolTip(toolTip, occurrence);
     }
     return false;
 }
 
+/**
+ * PUBLIC: Displays a tooltip for a given item
+ *
+ * @param  {Node}               aTooltip  the node to hold the tooltip
+ * @param  {CalIEvent|calIToDo} aItem     the item to create the tooltip for
+ * @returns {boolean}                     true, if the tooltip is displayed
+ */
 function showToolTip(aToolTip, aItem) {
     if (aItem) {
-        let holderBox;
-        if (cal.isEvent(aItem)) {
-            holderBox = getPreviewForEvent(aItem);
-        } else if (cal.isToDo(aItem)) {
-            holderBox = getPreviewForTask(aItem);
-        }
+        let holderBox = getPreviewForItem(aItem);
         if (holderBox) {
             removeChildren(aToolTip);
             aToolTip.appendChild(holderBox);
             return true;
         }
     }
     return false;
 }
 
 /**
- *  Called when a user hovers over a todo element and the text for the mouse over is changed.
+ * PUBLIC:  Called when a user hovers over a todo element and the text for the
+ * mouse over is changed.
+ *
+ * @param {calIToDo} toDoItem    the item to create the preview for
+ * @param {boolean}  aIsTooltip  enabled if used for tooltip composition (default)
+ */
+function getPreviewForItem(aItem, aIsTooltip=true) {
+    if (cal.isEvent(aItem)) {
+        return getPreviewForEvent(aItem, aIsTooltip);
+    } else if (cal.isToDo(aItem)) {
+        return getPreviewForTask(aItem, aIsTooltip);
+    } else {
+        return null;
+    }
+}
+
+/**
+ * PUBLIC: Returns the string for status (none), Tentative, Confirmed, or
+ * Cancelled for a given event
+ *
+ * @param   {calIEvent} aEvent The event
+ * @returns {String}           The string for the status property of the event
  */
-function getPreviewForTask(toDoItem) {
+function getEventStatusString(aEvent) {
+    switch (aEvent.status) {
+        // Event status value keywords are specified in RFC2445sec4.8.1.11
+        case "TENTATIVE":
+            return cal.calGetString("calendar", "statusTentative");
+        case "CONFIRMED":
+            return cal.calGetString("calendar", "statusConfirmed");
+        case "CANCELLED":
+            return cal.calGetString("calendar", "eventStatusCancelled");
+        default:
+            return "";
+    }
+}
+
+/**
+ * PUBLIC: Returns the string for status (none), NeedsAction, InProcess,
+ * Cancelled, orCompleted for a given ToDo
+ *
+ * @param   {calIToDo} aToDo   The ToDo
+ * @returns {String}           The string for the status property of the event
+ */
+function getToDoStatusString(aToDo) {
+    switch (aToDo.status) {
+        // Todo status keywords are specified in RFC2445sec4.8.1.11
+        case "NEEDS-ACTION":
+            return cal.calGetString("calendar", "statusNeedsAction");
+        case "IN-PROCESS":
+            return cal.calGetString("calendar", "statusInProcess");
+        case "CANCELLED":
+            return cal.calGetString("calendar", "todoStatusCancelled");
+        case "COMPLETED":
+            return cal.calGetString("calendar", "statusCompleted");
+        default:
+            return "";
+    }
+}
+
+/**
+ * PRIVATE: Called when a user hovers over a todo element and the text for the
+ * mouse overis changed.
+ *
+ * @param {calIToDo} toDoItem    the item to create the preview for
+ * @param {boolean}  aIsTooltip  enabled if used for tooltip composition (default)
+ */
+function getPreviewForTask(toDoItem, aIsTooltip=true) {
     if (toDoItem) {
         const vbox = document.createElement("vbox");
         vbox.setAttribute("class", "tooltipBox");
-        // tooltip appears above or below pointer, so may have as little as
-        // one half the screen height available (avoid top going off screen).
-        vbox.maxHeight = Math.floor(screen.height / 2);
+        if (aIsTooltip) {
+            // tooltip appears above or below pointer, so may have as little as
+            // one half the screen height available (avoid top going off screen).
+            vbox.maxHeight = Math.floor(screen.height / 2);
+        } else {
+            vbox.setAttribute("flex", "1");
+        }
         boxInitializeHeaderGrid(vbox);
 
         let hasHeader = false;
 
         if (toDoItem.title) {
             boxAppendLabeledText(vbox, "tooltipTitle", toDoItem.title);
             hasHeader = true;
         }
@@ -125,39 +202,45 @@ function getPreviewForTask(toDoItem) {
         }
 
         let description = toDoItem.getProperty("DESCRIPTION");
         if (description) {
             // display wrapped description lines like body of message below headers
             if (hasHeader) {
                 boxAppendBodySeparator(vbox);
             }
-            boxAppendBody(vbox, description);
+            boxAppendBody(vbox, description, aIsTooltip);
         }
 
         return vbox;
     } else {
         return null;
     }
 }
 
 /**
- *  Called when mouse moves over a different, or
- *  when mouse moves over event in event list.
- *  The instStartDate is date of instance displayed at event box
- *  (recurring or multiday events may be displayed by more than one event box
- *  for different days), or null if should compute next instance from now.
+ * PRIVATE: Called when mouse moves over a different, or  when mouse moves over
+ * event in event list. The instStartDate is date of instance displayed at event
+ * box (recurring or multiday events may be displayed by more than one event box
+ * for different days), or null if should compute next instance from now.
+ *
+ * @param {calIEvent} aEvent       the item to create the preview for
+ * @param {boolean}   aIsTooltip   enabled if used for tooltip composition (default)
  */
-function getPreviewForEvent(aEvent) {
+function getPreviewForEvent(aEvent, aIsTooltip=true) {
     let event = aEvent;
     const vbox = document.createElement("vbox");
     vbox.setAttribute("class", "tooltipBox");
-    // tooltip appears above or below pointer, so may have as little as
-    // one half the screen height available (avoid top going off screen).
-    vbox.maxHeight = Math.floor(screen.height / 2);
+    if (aIsTooltip) {
+        // tooltip appears above or below pointer, so may have as little as
+        // one half the screen height available (avoid top going off screen).
+        vbox.maxHeight = Math.floor(screen.height / 2);
+    } else {
+        vbox.setAttribute("flex", "1");
+    }
     boxInitializeHeaderGrid(vbox);
 
     if (event) {
         if (event.title) {
             boxAppendLabeledText(vbox, "tooltipTitle", aEvent.title);
         }
 
         let location = event.getProperty("LOCATION");
@@ -186,107 +269,86 @@ function getPreviewForEvent(aEvent) {
             let organizer = event.organizer;
             boxAppendLabeledText(vbox, "tooltipOrganizer", organizer);
         }
 
         let description = event.getProperty("DESCRIPTION");
         if (description) {
             boxAppendBodySeparator(vbox);
             // display wrapped description lines, like body of message below headers
-            boxAppendBody(vbox, description);
+            boxAppendBody(vbox, description, aIsTooltip);
         }
         return vbox;
     } else {
         return null;
     }
 }
 
-
-/** String for event status: (none), Tentative, Confirmed, or Cancelled **/
-function getEventStatusString(calendarEvent) {
-    switch (calendarEvent.status) {
-        // Event status value keywords are specified in RFC2445sec4.8.1.11
-        case "TENTATIVE":
-            return cal.calGetString("calendar", "statusTentative");
-        case "CONFIRMED":
-            return cal.calGetString("calendar", "statusConfirmed");
-        case "CANCELLED":
-            return cal.calGetString("calendar", "eventStatusCancelled");
-        default:
-            return "";
-    }
-}
-
-/** String for todo status: (none), NeedsAction, InProcess, Cancelled, or Completed **/
-function getToDoStatusString(iCalToDo) {
-    switch (iCalToDo.status) {
-        // Todo status keywords are specified in RFC2445sec4.8.1.11
-        case "NEEDS-ACTION":
-            return cal.calGetString("calendar", "statusNeedsAction");
-        case "IN-PROCESS":
-            return cal.calGetString("calendar", "statusInProcess");
-        case "CANCELLED":
-            return cal.calGetString("calendar", "todoStatusCancelled");
-        case "COMPLETED":
-            return cal.calGetString("calendar", "statusCompleted");
-        default:
-            return "";
-    }
-}
-
 /**
  * PRIVATE: Append a separator, a thin space between header and body.
  *
- * @param vbox      box to which to append separator.
+ * @param {Node}  vbox  box to which to append separator.
  */
 function boxAppendBodySeparator(vbox) {
     const separator = document.createElement("separator");
     separator.setAttribute("class", "tooltipBodySeparator");
     vbox.appendChild(separator);
 }
 
 /**
  * PRIVATE: Append description to box for body text.  Text may contain
  * paragraphs; line indent and line breaks will be preserved by CSS.
+ *
  * @param box           box to which to append body
  * @param textString    text of body
+ * @param aIsTooltip    true for "tooltip" and false for "conflict-dialog" case
  */
-function boxAppendBody(box, textString) {
+function boxAppendBody(box, textString, aIsTooltip) {
+    let type = (aIsTooltip) ? "description": "vbox";
     let textNode = document.createTextNode(textString);
-    let xulDescription = document.createElement("description");
+    let xulDescription = document.createElement(type);
     xulDescription.setAttribute("class", "tooltipBody");
+    if (!aIsTooltip) {
+        xulDescription.setAttribute("flex", "1");
+    }
     xulDescription.appendChild(textNode);
     box.appendChild(xulDescription);
 }
 
 /**
  * PRIVATE: Use dateFormatter to format date and time,
  * and to header grid append a row containing localized Label: date.
+ *
+ * @param {Node}         box            The node to add the date label to
+ * @param {String}       labelProperty  The label
+ * @param {calIDateTime} date           The datetime object to format and add
  */
 function boxAppendLabeledDateTime(box, labelProperty, date) {
     date = date.getInTimezone(cal.calendarDefaultTimezone());
     let formattedDateTime = cal.getDateFormatter().formatDateTime(date);
     boxAppendLabeledText(box, labelProperty, formattedDateTime);
 }
 
 /**
  * PRIVATE: Use dateFormatter to format date and time interval,
  * and to header grid append a row containing localized Label: interval.
+ *
  * @param box               contains header grid.
  * @param labelProperty     name of property for localized field label.
  * @param item              the event or task
  */
 function boxAppendLabeledDateTimeInterval(box, labelProperty, item) {
     let dateString = cal.getDateFormatter().formatItemInterval(item);
     boxAppendLabeledText(box, labelProperty, dateString);
 }
 
 /**
- * PRIVATE: create empty 2-column grid for header fields,
- * and append it to box.
+ * PRIVATE: create empty 2-column grid for header fields, and append it to box.
+ *
+ * @param  {Node}  box  The node to create a column grid for
  */
 function boxInitializeHeaderGrid(box) {
     let grid = document.createElement("grid");
     grid.setAttribute("class", "tooltipHeaderGrid");
     let rows;
     {
         let columns = document.createElement("columns");
         {
@@ -300,53 +362,69 @@ function boxInitializeHeaderGrid(box) {
         grid.appendChild(columns);
         rows = document.createElement("rows");
         grid.appendChild(rows);
     }
     box.appendChild(grid);
 }
 
 /**
- * PRIVATE: To headers grid, append a row containing Label: value,
- * where label is localized text for labelProperty.
+ * PRIVATE: To headers grid, append a row containing Label: value, where label
+ * is localized text for labelProperty.
+ *
  * @param box               box containing headers grid
  * @param labelProperty     name of property for localized name of header
  * @param textString        value of header field.
  */
 function boxAppendLabeledText(box, labelProperty, textString) {
     let labelText = cal.calGetString("calendar", labelProperty);
     let rows = box.getElementsByTagNameNS(box.namespaceURI, "rows")[0];
     let row = document.createElement("row");
 
     row.appendChild(createTooltipHeaderLabel(labelText));
     row.appendChild(createTooltipHeaderDescription(textString));
 
     rows.appendChild(row);
 }
 
-/** PRIVATE: create element for field label (for header grid). **/
+/**
+ * PRIVATE: Creates an element for field label (for header grid)
+ *
+ * @param   {String} text  The text to display in the node
+ * @returns {Node}         The node
+ */
 function createTooltipHeaderLabel(text) {
     let label = document.createElement("label");
     label.setAttribute("class", "tooltipHeaderLabel");
     label.appendChild(document.createTextNode(text));
     return label;
 }
 
-/** PRIVATE: create element for field value (for header grid). **/
+/**
+ * PRIVATE: Creates an element for field value (for header grid)
+ *
+ * @param   {String} text  The text to display in the node
+ * @returns {Node}         The node
+ */
 function createTooltipHeaderDescription(text) {
     let label = document.createElement("description");
     label.setAttribute("class", "tooltipHeaderDescription");
     label.appendChild(document.createTextNode(text));
     return label;
 }
 
 /**
- * If now is during an occurrence, return the occurrence.
- * Else if now is before an occurrence, return the next occurrence.
- * Otherwise return the previous occurrence.
+ * PRIVATE: If now is during an occurrence, return the occurrence. If now is
+ * before an occurrence, return the next occurrence or otherwise the previous
+ * occurrence.
+ *
+ * @param   {calIEvent}  calendarEvent   The text to display in the node
+ * @returns {mixed}                      Returns a calIDateTime for the detected
+ *                                        occurence or calIEvent, if this is a
+ *                                        non-recurring event
  */
 function getCurrentNextOrPreviousRecurrence(calendarEvent) {
     if (!calendarEvent.recurrenceInfo) {
         return calendarEvent;
     }
 
     let dur = calendarEvent.duration.clone();
     dur.isNegative = true;
