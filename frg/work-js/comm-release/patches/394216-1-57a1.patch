# HG changeset patch
# User Aaron Brubacher <aaron_bru@hotmail.com> with Jorg K and aceman
# Date 1503934440 -7200
# Node ID 76ceed2ef472c8932daf05b085d1a6114622236c
# Parent  59c1028d7f64bcad5dad798db1320829e2a40f0c
Bug 394216 - Search for correct identity when draft identity key does not match From: address. r=jorgk,aceman

diff --git a/mail/components/compose/content/MsgComposeCommands.js b/mail/components/compose/content/MsgComposeCommands.js
--- a/mail/components/compose/content/MsgComposeCommands.js
+++ b/mail/components/compose/content/MsgComposeCommands.js
@@ -2460,29 +2460,18 @@ function ComposeStartup(aParams)
           params.format = Ci.nsIMsgCompFormat.HTML;
         else if (args.format.toLowerCase().trim() == "text")
           params.format = Ci.nsIMsgCompFormat.PlainText;
       }
       if (args.originalMsgURI)
         params.originalMsgURI = args.originalMsgURI;
       if (args.preselectid)
         params.identity = getIdentityForKey(args.preselectid);
-      else if (args.from) {
-        let identities = MailServices.accounts.allIdentities;
-        let enumerator = identities.enumerate();
-        let ident = {};
-
-        while (enumerator.hasMoreElements()) {
-          ident = enumerator.getNext();
-          if (args.from.toLowerCase().trim() == ident.email.toLowerCase()) {
-            params.identity = ident;
-            break;
-          }
-        }
-      }
+      if (args.from)
+        composeFields.from = args.from;
       if (args.to)
         composeFields.to = args.to;
       if (args.cc)
         composeFields.cc = args.cc;
       if (args.bcc)
         composeFields.bcc = args.bcc;
       if (args.newsgroups)
         composeFields.newsgroups = args.newsgroups;
@@ -2596,31 +2585,74 @@ function ComposeStartup(aParams)
         gBodyFromArgs = true;
         composeFields.body = args.body;
       }
     }
   }
 
   gComposeType = params.type;
 
+  // Detect correct identity when missing or mismatched.
   // An identity with no email is likely not valid.
-  if (!params.identity || !params.identity.email) {
-    // No pre-selected identity, so use the default account.
-    let identities = MailServices.accounts.defaultAccount.identities;
-    if (identities.length == 0)
-      identities = MailServices.accounts.allIdentities;
-    params.identity = identities.queryElementAt(0, Ci.nsIMsgIdentity);
+  // When editing a draft, 'params.identity' is pre-populated with the identity
+  // that created the draft or the identity owning the draft folder for a "foreign",
+  // draft, see ComposeMessage() in mailCommands.js. We don't want the latter,
+  // so use the creator identity which could be null.
+  if (gComposeType == nsIMsgCompType.Draft) {
+    let creatorKey = params.composeFields.creatorIdentityKey;
+    params.identity = creatorKey ? getIdentityForKey(creatorKey) : null;
+  }
+  let from = [];
+  if (params.composeFields.from)
+    from = MailServices.headerParser
+                       .parseEncodedHeader(params.composeFields.from, null);
+  from = (from.length && from[0] && from[0].email) ?
+    from[0].email.toLowerCase().trim() : null;
+  if (!params.identity || !params.identity.email ||
+      (from && !emailSimilar(from, params.identity.email))) {
+    let identities = MailServices.accounts.allIdentities;
+    let suitableCount = 0;
+
+    // Search for a matching identity.
+    if (from) {
+      for (let ident of fixIterator(identities, Ci.nsIMsgIdentity)) {
+        if (from == ident.email.toLowerCase()) {
+          if (suitableCount == 0)
+            params.identity = ident;
+          suitableCount++;
+          if (suitableCount > 1)
+            break; // No need to find more, it's already not unique.
+        }
+      }
+    }
+
+    if (!params.identity || !params.identity.email) {
+      // No preset identity and no match, so use the default account.
+      let identity = MailServices.accounts.defaultAccount.defaultIdentity;
+      if (!identity) {
+        let identities = MailServices.accounts.allIdentities;
+        if (identities.length > 0)
+          identity = identities.queryElementAt(0, Ci.nsIMsgIdentity);
+      }
+      params.identity = identity;
+    }
+
+    // Warn if no or more than one match was found.
+    // But don't warn for +suffix additions (a+b@c.com).
+    if (from && (suitableCount > 1 ||
+        (suitableCount == 0 && !emailSimilar(from, params.identity.email))))
+      gComposeNotificationBar.setIdentityWarning(params.identity.identityName);
   }
 
   identityList.selectedItem =
     identityList.getElementsByAttribute("identitykey", params.identity.key)[0];
 
   // Here we set the From from the original message, be it a draft or another
   // message, for example a template, we want to "edit as new".
-  // Only do this the message is our own draft or template.
+  // Only do this if the message is our own draft or template.
   if (params.composeFields.creatorIdentityKey && params.composeFields.from)
   {
     let from = MailServices.headerParser.parseEncodedHeader(params.composeFields.from, null).join(", ");
     if (from != identityList.value)
     {
       MakeFromFieldEditable(true);
       identityList.value = from;
     }
@@ -2737,16 +2769,30 @@ function ComposeStartup(aParams)
     getPref("mail.compose.autosaveinterval") * 60000 : 0;
 
   if (gAutoSaveInterval)
     gAutoSaveTimeout = setTimeout(AutoSave, gAutoSaveInterval);
 
   gAutoSaveKickedIn = false;
 }
 
+function splitEmailAddress(aEmail) {
+  let at = aEmail.lastIndexOf("@");
+  return (at != -1) ? [aEmail.slice(0, at), aEmail.slice(at + 1)] : [aEmail, ""];
+}
+
+// Emails are equal ignoring +suffixes (email+suffix@example.com).
+function emailSimilar(a, b) {
+  if (!a || !b)
+    return a == b;
+  a = splitEmailAddress(a.toLowerCase());
+  b = splitEmailAddress(b.toLowerCase());
+  return a[1] == b[1] && a[0].split("+", 1)[0] == b[0].split("+", 1)[0];
+}
+
 // The new, nice, simple way of getting notified when a new editor has been created
 var gMsgEditorCreationObserver =
 {
   observe: function(aSubject, aTopic, aData)
   {
     if (aTopic == "obs_documentCreated")
     {
       var editor = GetCurrentEditor();
@@ -3799,31 +3845,31 @@ function FillIdentityList(menulist)
 
   menulist.menupopup.appendChild(document.createElement("menuseparator"));
   menulist.menupopup.appendChild(document.createElement("menuitem"))
           .setAttribute("command", "cmd_customizeFromAddress");
 }
 
 function getCurrentAccountKey()
 {
-    // get the accounts key
-    var identityList = document.getElementById("msgIdentity");
-    return identityList.selectedItem.getAttribute("accountkey");
+  // Get the account's key.
+  let identityList = GetMsgIdentityElement();
+  return identityList.selectedItem.getAttribute("accountkey");
 }
 
 function getCurrentIdentityKey()
 {
-  // get the identity key
-  var identityList = GetMsgIdentityElement();
+  // Get the identity key.
+  let identityList = GetMsgIdentityElement();
   return identityList.selectedItem.getAttribute("identitykey");
 }
 
 function getIdentityForKey(key)
 {
-    return MailServices.accounts.getIdentity(key);
+  return MailServices.accounts.getIdentity(key);
 }
 
 function getCurrentIdentity()
 {
   return getIdentityForKey(getCurrentIdentityKey());
 }
 
 function AdjustFocus()
@@ -4641,16 +4687,18 @@ function LoadIdentity(startup)
 
           try {
             gMsgCompose.identity = gCurrentIdentity;
           } catch (ex) { dump("### Cannot change the identity: " + ex + "\n");}
 
           var event = document.createEvent('Events');
           event.initEvent('compose-from-changed', false, true);
           document.getElementById("msgcomposeWindow").dispatchEvent(event);
+
+          gComposeNotificationBar.clearIdentityWarning();
         }
 
       if (!startup) {
           if (getPref("mail.autoComplete.highlightNonMatches"))
             document.getElementById('addressCol2#1').highlightNonMatches = true;
 
           // Only do this if we aren't starting up...
           // It gets done as part of startup already.
@@ -5660,18 +5708,43 @@ var gComposeNotificationBar = {
         .setAttribute("label", msg);
     }
   },
 
   isShowingBlockedContentNotification: function() {
     return !!this.notificationBar.getNotificationWithValue("blockedContent");
   },
 
+  clearBlockedContentNotification: function() {
+    this.notificationBar.removeNotification(
+      this.notificationBar.getNotificationWithValue("blockedContent"));
+  },
+
   clearNotifications: function(aValue) {
     this.notificationBar.removeAllNotifications(true);
+  },
+
+  setIdentityWarning: function(aIdentityName) {
+    if (!this.notificationBar.getNotificationWithValue("identityWarning")) {
+      let text = getComposeBundle().getString("identityWarning")
+                                   .split("%S");
+      let label = new DocumentFragment();
+      label.appendChild(document.createTextNode(text[0]));
+      label.appendChild(document.createElement("b"));
+      label.lastChild.appendChild(document.createTextNode(aIdentityName));
+      label.appendChild(document.createTextNode(text[1]));
+      this.notificationBar.appendNotification(label, "identityWarning", null,
+        this.notificationBar.PRIORITY_WARNING_HIGH, null);
+    }
+  },
+
+  clearIdentityWarning: function() {
+    let idWarning = this.notificationBar.getNotificationWithValue("identityWarning");
+    if (idWarning)
+      this.notificationBar.removeNotification(idWarning);
   }
 };
 
 /**
  * Populate the menuitems of what blocked content to unblock.
  */
 function onBlockedContentOptionsShowing(aEvent) {
   let urls = aEvent.target.value ? aEvent.target.value.split(" ") : [];
@@ -5710,17 +5783,17 @@ function onUnblockResource(aURL, aNode) 
   } finally {
     // Remove it from the list on success and failure.
     let urls = aNode.value.split(" ");
     for (let i = 0; i < urls.length; i++) {
       if (urls[i] == aURL) {
         urls.splice(i, 1);
         aNode.value = urls.join(" ");
         if (urls.length == 0) {
-          gComposeNotificationBar.clearNotifications();
+          gComposeNotificationBar.clearBlockedContentNotification();
         }
         break;
       }
     }
   }
 }
 
 /**
diff --git a/mail/locales/en-US/chrome/messenger/messengercompose/composeMsgs.properties b/mail/locales/en-US/chrome/messenger/messengercompose/composeMsgs.properties
--- a/mail/locales/en-US/chrome/messenger/messengercompose/composeMsgs.properties
+++ b/mail/locales/en-US/chrome/messenger/messengercompose/composeMsgs.properties
@@ -440,8 +440,11 @@ blockedAllowResource=Unblock %S
 blockedContentMessage=%S has blocked a file from loading into this message. Unblocking the file will include it in your sent message.;%S has blocked some files from loading into this message. Unblocking a file will include it in your sent message.
 
 blockedContentPrefLabel=Options
 blockedContentPrefAccesskey=O
 
 blockedContentPrefLabelUnix=Preferences
 blockedContentPrefAccesskeyUnix=P
 
+## Identity matching warning notification bar.
+## LOCALIZATION NOTE(identityWarning): %S will be replaced with the identity name.
+identityWarning=A unique identity matching the From address was not found. The message will be sent using the current From field and settings from identity %S.
diff --git a/mail/themes/linux/mail/compose/messengercompose.css b/mail/themes/linux/mail/compose/messengercompose.css
--- a/mail/themes/linux/mail/compose/messengercompose.css
+++ b/mail/themes/linux/mail/compose/messengercompose.css
@@ -209,17 +209,21 @@ toolbar[brighttext] menulist > .menulist
 
 #attachmentBucketSize {
   color: #888a85;
   padding-inline-end: 1px;
 }
 
 /* ::::: attachment reminder ::::: */
 
-#attachmentNotificationBox > notification .messageImage {
+#attachmentNotificationBox b {
+  font-weight: bold;
+}
+
+#attachmentNotificationBox > notification[image="null"] .messageImage {
   width: 18px;
   height: 18px;
   background-image: url(chrome://messenger/skin/messengercompose/compose-toolbar.svg#attach);
 }
 
 #attachmentReminderText {
   margin-inline-start: 0px;
   cursor: pointer;
diff --git a/mail/themes/osx/mail/compose/messengercompose.css b/mail/themes/osx/mail/compose/messengercompose.css
--- a/mail/themes/osx/mail/compose/messengercompose.css
+++ b/mail/themes/osx/mail/compose/messengercompose.css
@@ -95,17 +95,17 @@ toolbar[nowindowdrag="true"] {
   #paste-button {
     list-style-image: url("chrome://messenger/skin/messengercompose/compose-toolbar-osxlion.svg#paste");
   }
 
   #button-print {
     list-style-image: url("chrome://messenger/skin/messengercompose/compose-toolbar-osxlion.svg#print");
   }
 
-  #attachmentNotificationBox > notification .messageImage {
+  #attachmentNotificationBox > notification[image="null"] .messageImage {
     background-image: url("chrome://messenger/skin/messengercompose/compose-toolbar-osxlion.svg#attach");
   }
 }
 
 @media (-moz-mac-yosemite-theme) {
   #button-send {
     list-style-image: url("chrome://messenger/skin/messengercompose/compose-toolbar.svg#send-flat");
   }
@@ -146,17 +146,17 @@ toolbar[nowindowdrag="true"] {
   #paste-button {
     list-style-image: url("chrome://messenger/skin/messengercompose/compose-toolbar.svg#paste-flat");
   }
 
   #button-print {
     list-style-image: url("chrome://messenger/skin/messengercompose/compose-toolbar.svg#print-flat");
   }
 
-  #attachmentNotificationBox > notification .messageImage {
+  #attachmentNotificationBox > notification[image="null"] .messageImage {
     background-image: url("chrome://messenger/skin/messengercompose/compose-toolbar.svg#attach-flat");
   }
 }
 
 toolbar[brighttext] #button-send {
   list-style-image: url("chrome://messenger/skin/messengercompose/compose-toolbar.svg#send-inverted");
 }
 
@@ -290,17 +290,21 @@ toolbar[brighttext] #button-print {
 }
 
 #attachmentBucketSize {
   color: #888a85;
 }
 
 /* ::::: attachment reminder ::::: */
 
-#attachmentNotificationBox > notification .messageImage {
+#attachmentNotificationBox b {
+  font-weight: bold;
+}
+
+#attachmentNotificationBox > notification[image="null"] .messageImage {
   width: 18px;
   height: 18px;
 }
 
 #attachmentReminderText {
   margin-inline-start: 0px;
   cursor: pointer;
 }
diff --git a/mail/themes/windows/mail/compose/messengercompose.css b/mail/themes/windows/mail/compose/messengercompose.css
--- a/mail/themes/windows/mail/compose/messengercompose.css
+++ b/mail/themes/windows/mail/compose/messengercompose.css
@@ -795,17 +795,21 @@ toolbar[brighttext] #button-print {
 #attachments-box {
   padding-top: 4px;
 }
 
 #attachmentBucketSize {
   color: #888a85;
 }
 
-#attachmentNotificationBox > notification .messageImage {
+#attachmentNotificationBox b {
+  font-weight: bold;
+}
+
+#attachmentNotificationBox > notification[image="null"] .messageImage {
   width: 18px;
   height: 18px;
 }
 
 #compose-toolbar-sizer {
   position: relative;
   z-index: 10;
   min-height: 0;
@@ -969,17 +973,17 @@ treechildren::-moz-tree-image(subscribed
 }
 
 @media (-moz-os-version: windows-win7) {
   #compose-toolbox:not(:-moz-lwtheme) {
   background-image: linear-gradient(rgba(255, 255, 255, .5),
                     rgba(255, 255, 255, 0));
   }
 
-  #attachmentNotificationBox > notification .messageImage {
+  #attachmentNotificationBox > notification[image="null"] .messageImage {
     background-image: url(chrome://messenger/skin/messengercompose/compose-toolbar.svg#attach);
   }
 }
 
 @media (-moz-os-version: windows-win7) and (-moz-windows-default-theme) {
   #headers-box {
     border-bottom-color: #a9b7c9;
   }
@@ -1016,17 +1020,17 @@ treechildren::-moz-tree-image(subscribed
 }
 
 @media (-moz-os-version: windows-win8),
        (-moz-os-version: windows-win10) {
   #msgIdentity > .menulist-dropmarker > .dropmarker-icon {
     list-style-image: url("chrome://messenger/skin/icons/dropmarker.svg#win8");
   }
 
-  #attachmentNotificationBox > notification .messageImage {
+  #attachmentNotificationBox > notification[image="null"] .messageImage {
     background-image: url(chrome://messenger/skin/messengercompose/compose-toolbar.svg#attach-flat);
   }
 }
 
 .notification-button {
   margin-top: 2px;
   margin-bottom: 2px;
 }
