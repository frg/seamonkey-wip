# HG changeset patch
# User Bill Gianopoulos <wgianopoulos@gmail.com>
# Date 1547386372 18000
#      Sun Jan 13 08:32:52 2019 -0500
# Node ID ec1e8d9385524a2b36a4fec23528310d505388e3
# Parent  22ce1fb7afd4fc626f90c481a6e234e6013ade2c
Bug 1519872 - Replace configure.in with moz.configure in SeaMMonkey. r=frg
Port Bug 1490765 [delete code for application-specific subconfigure scripts].
See Bug 1490867 for Thunderbird and previous temporary solution.

diff --git a/suite/confvars.sh b/suite/confvars.sh
--- a/suite/confvars.sh
+++ b/suite/confvars.sh
@@ -1,53 +1,28 @@
 #! /bin/sh
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
-export moztopsrcdir=${srcdir}
-export commtopsrcdir=${srcdir}/comm
-export mozreltopsrcdir=.
-export commreltopsrcdir=comm
-export commtopobjdir=${_objdir}/comm
-tmpscript=`$PYTHON -c 'import os, tempfile; print tempfile.mktemp(prefix="subscript.").replace(os.sep, "/")'` || exit 1
-m4 "${srcdir}/build/autoconf/subconfigure.m4" \
-    "${srcdir}/build/autoconf/altoptions.m4" \
-    "${srcdir}/${MOZ_BUILD_APP}/configure.in" > $tmpscript
-. $tmpscript
-rm -f $tmpscript
-
 MOZ_APP_BASENAME=SeaMonkey
 MOZ_APP_VENDOR=Mozilla
 MOZ_APP_NAME=seamonkey
 MOZ_APP_DISPLAYNAME=SeaMonkey
+
 MOZ_BRANDING_DIRECTORY=comm/suite/branding/seamonkey
 MOZ_OFFICIAL_BRANDING_DIRECTORY=comm/suite/branding/seamonkey
 
 MOZ_UPDATER=1
 # This should usually be the same as the value MAR_CHANNEL_ID.
 # If more than one ID is needed, then you should use a comma separated list
 # of values.
 ACCEPTED_MAR_CHANNEL_IDS=seamonkey-comm-central
 # The MAR_CHANNEL_ID must not contain the following 3 characters: ",\t "
 MAR_CHANNEL_ID=seamonkey-comm-central
 
-MOZ_APP_VERSION_TXT=${_topsrcdir}/$MOZ_BUILD_APP/config/version.txt
-MOZ_APP_VERSION=`cat $MOZ_APP_VERSION_TXT`
-MOZ_APP_VERSION_DISPLAY_TXT=${_topsrcdir}/$MOZ_BUILD_APP/config/version_display.txt
-MOZ_APP_VERSION_DISPLAY=`cat $MOZ_APP_VERSION_DISPLAY_TXT`
-MOZ_PKG_VERSION_TXT=${_topsrcdir}/$MOZ_BUILD_APP/config/version_package.txt
-MOZ_PKG_VERSION=`cat $MOZ_PKG_VERSION_TXT`
-SEAMONKEY_VERSION=$MOZ_APP_VERSION
-SEAMONKEY_VERSION_DISPLAY=$MOZ_APP_VERSION_DISPLAY
-
 MOZ_APP_ID={92650c4d-4b8e-4d2a-b7eb-24ecf4f6b63a}
 MOZ_PROFILE_MIGRATOR=1
 
-if test "$OS_ARCH" = "WINNT" -o \
-        "$OS_ARCH" = "Linux"; then
-  MOZ_BUNDLED_FONTS=1
-fi
-
 # Include the DevTools client, not just the server (which is the default)
 MOZ_DEVTOOLS=all
 
 NSS_EXTRA_SYMBOLS_FILE=../comm/mailnews/nss-extra.symbols
diff --git a/suite/moz.configure b/suite/moz.configure
--- a/suite/moz.configure
+++ b/suite/moz.configure
@@ -2,16 +2,75 @@
 # vim: set filetype=python:
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 set_config('MOZ_SUITE', True)
 set_define('MOZ_SUITE', True)
 
+@depends(target_is_windows, target_is_linux)
+def bundled_fonts(is_windows, is_linux):
+    if is_windows or is_linux:
+        return True
+
+set_config('MOZ_BUNDLED_FONTS', bundled_fonts)
+add_old_configure_assignment('MOZ_BUNDLED_FONTS', bundled_fonts)
+
+@depends(check_build_environment, '--help')
+def comm_paths(build_env, _):
+    topsrcdir = build_env.topsrcdir
+    topobjdir = build_env.topobjdir
+
+    moztopsrcdir=topsrcdir
+    commtopsrcdir='%s/comm' % topsrcdir
+    mozreltopsrcdir='.'
+    commreltopsrcdir='comm'
+    commtopobjdir='%s/comm' % topobjdir
+
+    return namespace(moztopsrcdir=moztopsrcdir,
+                     commtopsrcdir=commtopsrcdir,
+                     mozreltopsrcdir=mozreltopsrcdir,
+                     commreltopsrcdir=commreltopsrcdir,
+                     commtopobjdir=commtopobjdir)
+
+@template
+def set_defconf(k,v):
+    set_config(k,v)
+    set_define(k,v)
+    add_old_configure_assignment(k,v)
+
+set_defconf('moztopsrcdir', comm_paths.moztopsrcdir)
+set_defconf('commtopsrcdir', comm_paths.commtopsrcdir)
+set_defconf('mozreltopsrcdir', comm_paths.mozreltopsrcdir)
+set_defconf('commreltopsrcdir', comm_paths.commreltopsrcdir)
+set_defconf('commtopobjdir', comm_paths.commtopobjdir)
+
+@depends(check_build_environment, application)
+@imports(_from='os.path', _import='exists')
+@imports(_from='__builtin__', _import='open')
+def seamonkey_version(build_env, app_path):
+    version_file = os.path.join(build_env.topsrcdir, app_path[0],
+                                'config', 'version.txt')
+    version_file_display = os.path.join(build_env.topsrcdir, app_path[0],
+                                        'config', 'version_display.txt')
+    rv = []
+    for f in [ version_file, version_file_display]:
+        if exists(f):
+            f_value = open(f).read().strip()
+        else:
+            f_value = 'unknown'
+        rv.append(f_value)
+
+    return namespace(version=rv[0],
+                     version_display=rv[1])
+
+set_defconf('SEAMONKEY_VERSION', seamonkey_version.version)
+set_defconf('SEAMONKEY_VERSION_DISPLAY', seamonkey_version.version_display)
+
 set_define('MOZ_SEPARATE_MANIFEST_FOR_THEME_OVERRIDES', True)
 
 imply_option('MOZ_PLACES', True)
 imply_option('MOZ_SERVICES_SYNC', False)
 
 # Building extensions is enabled by default.
 # Bug 1231349 needs to be fixed for successful l10n builds.
 
@@ -46,22 +105,10 @@ option('--enable-debugqa', default=False
        help='Enable building of the DebugQA extension')
 
 @depends_if('--enable-debugqa')
 def debugqa(arg):
     return True
 
 set_config('MOZ_DEBUGQA', debugqa)
 
-@depends(application)
-def is_comm(app):
-    return app[0].startswith('comm/')
-
-
-@depends(is_comm)
-def toolkit_configure(is_comm):
-    if is_comm:
-        return '../../toolkit/moz.configure'
-    else:
-        return '../mozilla/toolkit/moz.configure'
-
 include('../mailnews/moz.configure')
-include(toolkit_configure)
+include('../../toolkit/moz.configure')
