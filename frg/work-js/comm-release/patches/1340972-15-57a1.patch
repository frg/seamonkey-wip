# HG changeset patch
# User Jorg K <jorgk@jorgk.com>
# Date 1505428482 -7200
#      Fri Sep 15 00:34:42 2017 +0200
# Node ID ebe355c1d925132c3dd4ce6acca6506079518d89
# Parent  f9888ec16b1fd60cb676d5ad8a8d16aa659cd9e3
Bug 1340972 - Part 15: Replace error-prone Addref()/Release() with using RefPtr in mailnews/base. r=aceman

diff --git a/mailnews/base/src/nsMessenger.cpp b/mailnews/base/src/nsMessenger.cpp
--- a/mailnews/base/src/nsMessenger.cpp
+++ b/mailnews/base/src/nsMessenger.cpp
@@ -666,18 +666,16 @@ nsresult nsMessenger::SaveAttachment(nsI
   nsSaveAllAttachmentsState *saveState= (nsSaveAllAttachmentsState*) closure;
   nsCOMPtr<nsIMsgMessageFetchPartService> fetchService;
   nsAutoCString urlString;
   nsAutoCString fullMessageUri(aMessageUri);
 
   // This instance will be held onto by the listeners, and will be released once
   // the transfer has been completed.
   RefPtr<nsSaveMsgListener> saveListener(new nsSaveMsgListener(aFile, this, aListener));
-  if (!saveListener)
-    return NS_ERROR_OUT_OF_MEMORY;
 
   saveListener->m_contentType = aContentType;
   if (saveState)
   {
     saveListener->m_saveAllAttachmentsState = saveState;
     if (saveState->m_detachingAttachments)
     {
       nsCOMPtr<nsIURI> outputURI;
@@ -1037,17 +1035,17 @@ nsresult nsMessenger::AdjustFileIfNameTo
 
 NS_IMETHODIMP
 nsMessenger::SaveAs(const nsACString& aURI, bool aAsFile,
                     nsIMsgIdentity *aIdentity, const nsAString& aMsgFilename,
                     bool aBypassFilePicker)
 {
   nsCOMPtr<nsIMsgMessageService> messageService;
   nsCOMPtr<nsIUrlListener> urlListener;
-  nsSaveMsgListener *saveListener = nullptr;
+  RefPtr<nsSaveMsgListener> saveListener;
   nsCOMPtr<nsIStreamListener> convertedListener;
   int32_t saveAsFileType = EML_FILE_TYPE;
 
   nsresult rv = GetMessageServiceFromURI(aURI, getter_AddRefs(messageService));
   if (NS_FAILED(rv))
     goto done;
 
   if (aAsFile)
@@ -1084,21 +1082,17 @@ nsMessenger::SaveAs(const nsACString& aU
 
     rv = PromptIfFileExists(saveAsFile);
     if (NS_FAILED(rv)) {
       goto done;
     }
 
     // After saveListener goes out of scope, the listener will be owned by
     // whoever the listener is registered with, usually a URL.
-    RefPtr<nsSaveMsgListener> saveListener = new nsSaveMsgListener(saveAsFile, this, nullptr);
-    if (!saveListener) {
-      rv = NS_ERROR_OUT_OF_MEMORY;
-      goto done;
-    }
+    saveListener = new nsSaveMsgListener(saveAsFile, this, nullptr);
     rv = saveListener->QueryInterface(NS_GET_IID(nsIUrlListener), getter_AddRefs(urlListener));
     if (NS_FAILED(rv))
       goto done;
 
     if (saveAsFileType == EML_FILE_TYPE)
     {
       nsCOMPtr<nsIURI> dummyNull;
       rv = messageService->SaveMessageToDisk(PromiseFlatCString(aURI).get(), saveAsFile, false,
@@ -1184,20 +1178,16 @@ nsMessenger::SaveAs(const nsACString& aU
 
     // For temp file, we should use restrictive 00600 instead of ATTACHMENT_PERMISSION
     rv = tmpFile->CreateUnique(nsIFile::NORMAL_FILE_TYPE, 00600);
     if (NS_FAILED(rv)) goto done;
 
     // The saveListener is owned by whoever we ultimately register the
     // listener with, generally a URL.
     saveListener = new nsSaveMsgListener(tmpFile, this, nullptr);
-    if (!saveListener) {
-      rv = NS_ERROR_OUT_OF_MEMORY;
-      goto done;
-    }
 
     if (aIdentity)
       rv = aIdentity->GetStationeryFolder(saveListener->m_templateUri);
     if (NS_FAILED(rv))
       goto done;
 
     bool needDummyHeader = StringBeginsWith(saveListener->m_templateUri, NS_LITERAL_CSTRING("mailbox://"));
     bool canonicalLineEnding = StringBeginsWith(saveListener->m_templateUri, NS_LITERAL_CSTRING("imap://"));
@@ -1213,17 +1203,16 @@ nsMessenger::SaveAs(const nsACString& aU
       needDummyHeader,
       urlListener, getter_AddRefs(dummyNull),
       canonicalLineEnding, mMsgWindow);
   }
 
 done:
   if (NS_FAILED(rv))
   {
-    NS_IF_RELEASE(saveListener);
     Alert("saveMessageFailed");
   }
   return rv;
 }
 
 nsresult
 nsMessenger::GetSaveAsFile(const nsAString& aMsgFilename, int32_t *aSaveAsFileType,
                            nsIFile **aSaveAsFile)
@@ -1425,40 +1414,32 @@ nsMessenger::SaveMessages(uint32_t aCoun
 
     rv = GetMessageServiceFromURI(nsDependentCString(aMessageUriArray[i]),
                                   getter_AddRefs(messageService));
     if (NS_FAILED(rv)) {
       Alert("saveMessageFailed");
       return rv;
     }
 
-    nsSaveMsgListener *saveListener = new nsSaveMsgListener(saveToFile, this, nullptr);
-    if (!saveListener) {
-      NS_IF_RELEASE(saveListener);
-      Alert("saveMessageFailed");
-      return NS_ERROR_OUT_OF_MEMORY;
-    }
-    NS_ADDREF(saveListener);
+    RefPtr<nsSaveMsgListener> saveListener = new nsSaveMsgListener(saveToFile, this, nullptr);
 
     rv = saveListener->QueryInterface(NS_GET_IID(nsIUrlListener),
                                       getter_AddRefs(urlListener));
     if (NS_FAILED(rv)) {
-      NS_IF_RELEASE(saveListener);
       Alert("saveMessageFailed");
       return rv;
     }
 
     // Ok, now save the message.
     nsCOMPtr<nsIURI> dummyNull;
     rv = messageService->SaveMessageToDisk(aMessageUriArray[i],
                                            saveToFile, false,
                                            urlListener, getter_AddRefs(dummyNull),
                                            true, mMsgWindow);
     if (NS_FAILED(rv)) {
-      NS_IF_RELEASE(saveListener);
       Alert("saveMessageFailed");
       return rv;
     }
   }
   return rv;
 }
 
 nsresult
diff --git a/mailnews/base/src/nsMsgFolderCacheElement.cpp b/mailnews/base/src/nsMsgFolderCacheElement.cpp
--- a/mailnews/base/src/nsMsgFolderCacheElement.cpp
+++ b/mailnews/base/src/nsMsgFolderCacheElement.cpp
@@ -11,19 +11,16 @@
 nsMsgFolderCacheElement::nsMsgFolderCacheElement()
 {
   m_mdbRow = nullptr;
   m_owningCache = nullptr;
 }
 
 nsMsgFolderCacheElement::~nsMsgFolderCacheElement()
 {
-  NS_IF_RELEASE(m_mdbRow);
-  // circular reference, don't do it.
-  // NS_IF_RELEASE(m_owningCache);
 }
 
 NS_IMPL_ISUPPORTS(nsMsgFolderCacheElement, nsIMsgFolderCacheElement)
 
 NS_IMETHODIMP nsMsgFolderCacheElement::GetKey(nsACString& aFolderKey)
 {
   aFolderKey = m_folderKey;
   return NS_OK;
@@ -149,12 +146,10 @@ NS_IMETHODIMP nsMsgFolderCacheElement::S
   // by integer wrapping them (e.g. -1 -> "ffffffffffffffff").
   nsAutoCString propertyStr;
   propertyStr.AppendInt(propertyValue, 16);
   return SetStringProperty(propertyName, propertyStr);
 }
 
 void nsMsgFolderCacheElement::SetMDBRow(nsIMdbRow *row)
 {
-  if (m_mdbRow)
-    NS_RELEASE(m_mdbRow);
-  NS_IF_ADDREF(m_mdbRow = row);
+  m_mdbRow = row;
 }
diff --git a/mailnews/base/src/nsMsgFolderCacheElement.h b/mailnews/base/src/nsMsgFolderCacheElement.h
--- a/mailnews/base/src/nsMsgFolderCacheElement.h
+++ b/mailnews/base/src/nsMsgFolderCacheElement.h
@@ -19,17 +19,17 @@ public:
   NS_DECL_ISUPPORTS
   NS_DECL_NSIMSGFOLDERCACHEELEMENT
 
   void SetMDBRow(nsIMdbRow *row);
   void SetOwningCache(nsMsgFolderCache *owningCache);
 protected:
   virtual ~nsMsgFolderCacheElement();
 
-  nsIMdbRow *m_mdbRow;
+  RefPtr<nsIMdbRow> m_mdbRow;
 
   nsMsgFolderCache *m_owningCache; // this will be ref-counted. Is this going to be a problem?
   // I want to avoid circular references, but since this is
   // scriptable, I think I have to ref-count it.
   nsCString m_folderKey;
 };
 
 #endif
diff --git a/mailnews/base/src/nsMsgGroupThread.cpp b/mailnews/base/src/nsMsgGroupThread.cpp
--- a/mailnews/base/src/nsMsgGroupThread.cpp
+++ b/mailnews/base/src/nsMsgGroupThread.cpp
@@ -340,17 +340,17 @@ public:
   int32_t MsgKeyFirstChildIndex(nsMsgKey inMsgKey);
 
 protected:
   virtual ~nsMsgGroupThreadEnumerator();
 
   nsresult                Prefetch();
 
   nsCOMPtr<nsIMsgDBHdr>   mResultHdr;
-  nsMsgGroupThread*       mThread;
+  RefPtr<nsMsgGroupThread> mThread;
   nsMsgKey                mThreadParentKey;
   nsMsgKey                mFirstMsgKey;
   int32_t                 mChildIndex;
   bool                    mDone;
   bool                    mNeedToPrefetch;
   nsMsgGroupThreadEnumeratorFilter     mFilter;
   void*                   mClosure;
   bool                    mFoundChildren;
@@ -412,23 +412,20 @@ nsMsgGroupThreadEnumerator::nsMsgGroupTh
       // if not, we will tell him there are no children.
       child->GetMessageKey(&msgKey);
       child->GetThreadParent(&threadParent);
 
       printf("index = %ld key = %ld parent = %lx\n", childIndex, msgKey, threadParent);
     }
   }
 #endif
-
-  NS_ADDREF(thread);
 }
 
 nsMsgGroupThreadEnumerator::~nsMsgGroupThreadEnumerator()
 {
-    NS_RELEASE(mThread);
 }
 
 NS_IMPL_ISUPPORTS(nsMsgGroupThreadEnumerator, nsISimpleEnumerator)
 
 int32_t nsMsgGroupThreadEnumerator::MsgKeyFirstChildIndex(nsMsgKey inMsgKey)
 {
   // look through rest of thread looking for a child of this message.
   // If the inMsgKey is the first message in the thread, then all children
@@ -655,31 +652,30 @@ nsresult nsMsgGroupThread::GetChildHdrFo
 
   nsresult rv = NS_OK;        // XXX or should this default to an error?
   uint32_t numChildren = 0;
   GetNumChildren(&numChildren);
 
   uint32_t childIndex;
   for (childIndex = 0; childIndex < numChildren; childIndex++)
   {
-    rv = GetChildHdrAt(childIndex, result);
-    if (NS_SUCCEEDED(rv) && *result)
+    nsCOMPtr<nsIMsgDBHdr> child;
+    rv = GetChildHdrAt(childIndex, getter_AddRefs(child));
+    if (NS_SUCCEEDED(rv) && child)
     {
       nsMsgKey msgKey;
       // we're only doing one level of threading, so check if caller is
       // asking for children of the first message in the thread or not.
       // if not, we will tell him there are no children.
-      (*result)->GetMessageKey(&msgKey);
+      child->GetMessageKey(&msgKey);
 
-      if (msgKey == desiredKey)
+      if (msgKey == desiredKey) {
+        child.forget(result);
         break;
-
-      // XXX Hack: since GetChildHdrAt() addref'ed result, we need to
-      // release any unwanted result before continuing in the loop.
-      NS_RELEASE(*result);
+      }
     }
   }
   if (resultIndex)
     *resultIndex = (int32_t) childIndex;
 
   return rv;
 }
 
diff --git a/mailnews/base/src/nsMsgPrintEngine.cpp b/mailnews/base/src/nsMsgPrintEngine.cpp
--- a/mailnews/base/src/nsMsgPrintEngine.cpp
+++ b/mailnews/base/src/nsMsgPrintEngine.cpp
@@ -367,19 +367,16 @@ nsMsgPrintEngine::ShowProgressDialog(boo
                                             getter_AddRefs(mPrintProgressParams),
                                             &aDoNotify);
       if (NS_SUCCEEDED(rv)) {
 
         showProgressDialog = mPrintProgressListener != nullptr && mPrintProgressParams != nullptr;
 
         if (showProgressDialog)
         {
-          nsIWebProgressListener* wpl = static_cast<nsIWebProgressListener*>(mPrintProgressListener.get());
-          NS_ASSERTION(wpl, "nsIWebProgressListener is NULL!");
-          NS_ADDREF(wpl);
           nsString msg;
           if (mIsDoingPrintPreview) {
             GetString(u"LoadingMailMsgForPrintPreview", msg);
           } else {
             GetString(u"LoadingMailMsgForPrint", msg);
           }
           if (!msg.IsEmpty())
             mPrintProgressParams->SetDocTitle(msg);
