# HG changeset patch
# User Ian Neal <iann_cvs@blueyonder.co.uk>
# Date 1621772372 -3600
# Parent  161688c3c45a42d1a55542e1df1128f7c9ea2817
Bug 104973 - Port |Bug 431217 - Send button should be disabled until we have a recipient| to SeaMonkey. r=frg a=frg
Port relevant changes to addressing widget:
* Bug 431217 - Send button should be disabled until we have a recipient
* Bug 863231 - Enable Send button properly when filling recipients via Contacts sidebar
* Bug 933101 - Properly enable Send button if recipients are drag'n'dropped into the address widget or filled in by switching identities
* Bug 1290733 - Speed up UpdateSendLock() in compose window by only looking for one valid recipient
* Bug 1296535 - Enable Send button when recipient is a mailinglist
* Bug 1362130 - enable Send button when mailinglist name gets quoted

diff --git a/suite/mailnews/components/compose/content/MsgComposeCommands.js b/suite/mailnews/components/compose/content/MsgComposeCommands.js
--- a/suite/mailnews/components/compose/content/MsgComposeCommands.js
+++ b/suite/mailnews/components/compose/content/MsgComposeCommands.js
@@ -6,16 +6,17 @@
 ChromeUtils.import("resource://gre/modules/Services.jsm");
 ChromeUtils.import("resource://gre/modules/AppConstants.jsm");
 ChromeUtils.import("resource://gre/modules/PluralForm.jsm");
 ChromeUtils.import("resource://gre/modules/InlineSpellChecker.jsm");
 ChromeUtils.import("resource:///modules/folderUtils.jsm");
 ChromeUtils.import("resource:///modules/iteratorUtils.jsm");
 ChromeUtils.import("resource:///modules/mailServices.js");
 ChromeUtils.import("resource:///modules/MailUtils.js");
+ChromeUtils.import("resource:///modules/mimeParser.jsm");
 
 ChromeUtils.defineModuleGetter(this, "OS", "resource://gre/modules/osfile.jsm");
 
 /**
  * interfaces
  */
 var nsIMsgCompDeliverMode = Ci.nsIMsgCompDeliverMode;
 var nsIMsgCompSendFormat = Ci.nsIMsgCompSendFormat;
@@ -54,16 +55,17 @@ var gMessenger;
 /**
  * Global variables, need to be re-initialized every time mostly because
  * we need to release them when the window closes.
  */
 var gHideMenus;
 var gMsgCompose;
 var gOriginalMsgURI;
 var gWindowLocked;
+var gSendLocked;
 var gContentChanged;
 var gAutoSaving;
 var gCurrentIdentity;
 var defaultSaveOperation;
 var gSendOrSaveOperationInProgress;
 var gCloseWindowAfterSave;
 var gSavedSendNowKey;
 var gSendFormat;
@@ -162,16 +164,17 @@ function enableEditableFields()
 function isSignature(aNode) {
   return ["DIV","PRE"].includes(aNode.nodeName) &&
          aNode.classList.contains("moz-signature");
 }
 
 var stateListener = {
   NotifyComposeFieldsReady: function() {
     ComposeFieldsReady();
+    updateSendCommands(true);
   },
 
   NotifyComposeBodyReady: function() {
     this.useParagraph = gMsgCompose.composeHTML &&
                         Services.prefs.getBoolPref("mail.compose.default_to_paragraph");
     this.editor = GetCurrentEditor();
     this.paragraphState = document.getElementById("cmd_paragraphState");
 
@@ -497,25 +500,27 @@ var defaultController =
       //File Menu
       case "cmd_attachFile":
       case "cmd_attachPage":
       case "cmd_close":
       case "cmd_save":
       case "cmd_saveAsFile":
       case "cmd_saveAsDraft":
       case "cmd_saveAsTemplate":
-      case "cmd_sendButton":
-      case "cmd_sendLater":
       case "cmd_printSetup":
       case "cmd_printpreview":
       case "cmd_print":
+        return !gWindowLocked;
+      case "cmd_sendButton":
+      case "cmd_sendLater":
       case "cmd_sendWithCheck":
-        return !gWindowLocked;
+      case "cmd_sendButton":
+        return !gWindowLocked && !gSendLocked;
       case "cmd_sendNow":
-        return !(gWindowLocked || Services.io.offline);
+        return !gWindowLocked && !Services.io.offline && !gSendLocked;
 
       //Edit Menu
       case "cmd_account":
       case "cmd_preferences":
         return true;
 
       //Options Menu
       case "cmd_selectAddress":
@@ -760,16 +765,38 @@ function updateEditItems()
   goUpdateCommand("cmd_findPrev");
 }
 
 function updateOptionItems()
 {
   goUpdateCommand("cmd_quoteMessage");
 }
 
+/**
+ * Update all the commands for sending a message to reflect their current state.
+ */
+function updateSendCommands(aHaveController) {
+  updateSendLock();
+  if (aHaveController) {
+    goUpdateCommand("cmd_sendButton");
+    goUpdateCommand("cmd_sendNow");
+    goUpdateCommand("cmd_sendLater");
+    goUpdateCommand("cmd_sendWithCheck");
+  } else {
+    goSetCommandEnabled("cmd_sendButton",
+                        defaultController.isCommandEnabled("cmd_sendButton"));
+    goSetCommandEnabled("cmd_sendNow",
+                        defaultController.isCommandEnabled("cmd_sendNow"));
+    goSetCommandEnabled("cmd_sendLater",
+                        defaultController.isCommandEnabled("cmd_sendLater"));
+    goSetCommandEnabled("cmd_sendWithCheck",
+                        defaultController.isCommandEnabled("cmd_sendWithCheck"));
+  }
+}
+
 var messageComposeOfflineQuitObserver = {
   observe: function(aSubject, aTopic, aState) {
     // sanity checks
     if (aTopic == "network:offline-status-changed")
     {
       MessageComposeOfflineStateChanged(aState == "offline");
     }
     // check whether to veto the quit request (unless another observer already
@@ -806,17 +833,17 @@ function MessageComposeOfflineStateChang
     var sendButton = document.getElementById("button-send");
     var sendNowMenuItem = document.getElementById("menu_sendNow");
 
     if (!gSavedSendNowKey) {
       gSavedSendNowKey = sendNowMenuItem.getAttribute('key');
     }
 
     // don't use goUpdateCommand here ... the defaultController might not be installed yet
-    goSetCommandEnabled("cmd_sendNow", defaultController.isCommandEnabled("cmd_sendNow"));
+    updateSendCommands(false);
 
     if (goingOffline)
     {
       sendButton.label = sendButton.getAttribute('later_label');
       sendButton.setAttribute('tooltiptext', sendButton.getAttribute('later_tooltiptext'));
       sendNowMenuItem.removeAttribute('key');
     }
     else
@@ -1920,16 +1947,59 @@ function GenericSendMessage(msgType) {
     gWindowLocked = false;
     enableEditableFields();
     updateComposeItems();
   }
   if (gMsgCompose && originalCharset != gMsgCompose.compFields.characterSet)
     SetDocumentCharacterSet(gMsgCompose.compFields.characterSet);
 }
 
+/**
+ * Check if the given address is valid (contains a @).
+ *
+ * @param aAddress  The address string to check.
+ */
+function isValidAddress(aAddress) {
+  return (aAddress.includes("@", 1) && !aAddress.endsWith("@"));
+}
+
+/**
+ * Keep the Send buttons disabled until any recipient is entered.
+ */
+function updateSendLock() {
+  gSendLocked = true;
+  if (!gMsgCompose)
+    return;
+
+  // Helper function to check for a valid list name.
+  function isValidListName(aInput) {
+    let listNames = MimeParser.parseHeaderField(aInput,
+                                                MimeParser.HEADER_ADDRESS);
+    return listNames > 0 &&
+           MailServices.ab.mailListNameExists(listNames[0].name);
+  }
+
+  const mailTypes = [ "addr_to", "addr_cc", "addr_bcc" ];
+
+  // Enable the send buttons if anything usable was entered into at least one
+  // recipient field.
+  for (let row = 1; row <= top.MAX_RECIPIENTS; row ++) {
+    let popupValue = awGetPopupElement(row).value;
+    let inputValue = awGetInputElement(row).value.trim();
+    // Check for a valid looking email address or a valid mailing list name
+    // from one of our addressbooks.
+    if ((mailTypes.includes(popupValue) &&
+         (isValidAddress(inputValue) || isValidListName(inputValue))) ||
+        ((popupValue == "addr_newsgroups") && (inputValue != ""))) {
+      gSendLocked = false;
+      break;
+    }
+  }
+}
+
 function CheckValidEmailAddress(aTo, aCC, aBCC)
 {
   var invalidStr = null;
   // crude check that the to, cc, and bcc fields contain at least one '@'.
   // We could parse each address, but that might be overkill.
   if (aTo.length > 0 && (aTo.indexOf("@") <= 0 && aTo.toLowerCase() != "postmaster" || aTo.indexOf("@") == aTo.length - 1))
     invalidStr = aTo;
   else if (aCC.length > 0 && (aCC.indexOf("@") <= 0 && aCC.toLowerCase() != "postmaster" || aCC.indexOf("@") == aCC.length - 1))
@@ -2176,16 +2246,37 @@ function addRecipientsToIgnoreList(aAddr
     }
     else
     {
       InlineSpellCheckerUI.mInlineSpellChecker.ignoreWords(tokenizedNames, tokenizedNames.length);
     }
   }
 }
 
+function onAddressColCommand(aWidgetId) {
+  gContentChanged = true;
+  awSetAutoComplete(aWidgetId.slice(aWidgetId.lastIndexOf('#') + 1));
+  updateSendCommands(true);
+}
+
+/**
+ * Called if the list of recipients changed in any way.
+ *
+ * @param aAutomatic  Set to true if the change of recipients was invoked
+ *                    programatically and should not be considered a change
+ *                    of message content.
+ */
+function onRecipientsChanged(aAutomatic) {
+  if (!aAutomatic) {
+    gContentChanged = true;
+    setupAutocomplete();
+  }
+  updateSendCommands(true);
+}
+
 function InitLanguageMenu()
 {
   var languageMenuList = document.getElementById("languageMenuList");
   if (!languageMenuList)
     return;
 
   var spellChecker = Cc["@mozilla.org/spellchecker/engine;1"]
                        .getService(mozISpellCheckingEngine);
diff --git a/suite/mailnews/components/compose/content/addressingWidgetOverlay.js b/suite/mailnews/components/compose/content/addressingWidgetOverlay.js
--- a/suite/mailnews/components/compose/content/addressingWidgetOverlay.js
+++ b/suite/mailnews/components/compose/content/addressingWidgetOverlay.js
@@ -217,16 +217,18 @@ function awSetInputAndPopupValue(inputEl
   inputElem.value = inputValue.trimLeft();
 
   popupElem.selectedItem = popupElem.childNodes[0].childNodes[awGetSelectItemIndex(popupValue)];
 
   if (rowNumber >= 0)
     awSetInputAndPopupId(inputElem, popupElem, rowNumber);
 
   _awSetAutoComplete(popupElem, inputElem);
+
+  onRecipientsChanged(true);
 }
 
 function _awSetInputAndPopup(inputValue, popupValue, parentNode, templateNode)
 {
     top.MAX_RECIPIENTS++;
 
     var newNode = templateNode.cloneNode(true);
     parentNode.appendChild(newNode); // we need to insert the new node before we set the value of the select element!
diff --git a/suite/mailnews/components/compose/content/messengercompose.xul b/suite/mailnews/components/compose/content/messengercompose.xul
--- a/suite/mailnews/components/compose/content/messengercompose.xul
+++ b/suite/mailnews/components/compose/content/messengercompose.xul
@@ -524,17 +524,17 @@
               <listcol id="textcol-addressingWidget" flex="1"/>
             </listcols>
 
             <listitem class="addressingWidgetItem" allowevents="true">
               <listcell class="addressingWidgetCell" align="stretch">
                 <menulist id="addressCol1#1" disableonsend="true"
                           class="aw-menulist menulist-compact" flex="1"
                           onkeypress="awMenulistKeyPress(event, this);"
-                          oncommand="gContentChanged=true; awSetAutoComplete(this.id.slice(this.id.lastIndexOf('#') + 1));">
+                          oncommand="onAddressColCommand(this.id);">
                   <menupopup>
                     <menuitem value="addr_to" label="&toAddr.label;"/>
                     <menuitem value="addr_cc" label="&ccAddr.label;"/>
                     <menuitem value="addr_bcc" label="&bccAddr.label;"/>
                     <menuitem value="addr_reply" label="&replyAddr.label;"/>
                     <menuitem value="addr_newsgroups"
                               label="&newsgroupsAddr.label;"/>
                     <menuitem value="addr_followup"
@@ -550,20 +550,21 @@
                          type="autocomplete" flex="1" maxrows="4"
                          newlines="replacewithcommas"
                          autocompletesearch="mydomain addrbook ldap news"
                          timeout="300" autocompletesearchparam="{}"
                          completedefaultindex="true" forcecomplete="true"
                          minresultsforpopup="2" ignoreblurwhilesearching="true"
                          ontextentered="awRecipientTextCommand(eventParam, this);"
                          onerrorcommand="awRecipientErrorCommand(eventParam, this);"
-                         oninput="gContentChanged=true; setupAutocomplete();"
-                         disableonsend="true"
+                         onchange="onRecipientsChanged();"
+                         oninput="onRecipientsChanged();"
                          onkeypress="awRecipientKeyPress(event, this);"
-                         onkeydown="awRecipientKeyDown(event, this);">
+                         onkeydown="awRecipientKeyDown(event, this);"
+                         disableonsend="true">
                   <image class="person-icon"
                          onclick="this.parentNode.select();"/>
                 </textbox>
               </listcell>
             </listitem>
           </listbox>
           <hbox align="center">
             <label value="&subject.label;" accesskey="&subject.accesskey;" control="msgSubject"/>
