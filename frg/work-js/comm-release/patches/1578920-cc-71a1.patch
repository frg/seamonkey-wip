# HG changeset patch
# User Rob Lemley <rob@thunderbird.net>
# Date 1567633180 14400
#      Wed Sep 04 17:39:40 2019 -0400
# Node ID 64740b4f12701019ace6d13c8e9df9aed8d5d0f4
# Parent  5f3bd0c1707d7ca70a0d78840fc67306d13749ee
Bug 1578920 - Bug 1507754 follow-up: fix mach configure failures on local builds. r=darkrojan DONTBUILD

- Change the "hg" command used to identify a revision to the one used by
  Firefox in build/variables.py.
- If "hg" still fails, don't abort the entire configure process.
- Only terminate configure in automation when source repository information
  cannot be determined.

diff --git a/build/moz.configure/gecko_source.configure b/build/moz.configure/gecko_source.configure
--- a/build/moz.configure/gecko_source.configure
+++ b/build/moz.configure/gecko_source.configure
@@ -4,16 +4,34 @@
 #  License, v. 2.0. If a copy of the MPL was not distributed with this
 #  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 # Attempt to ascertain the Gecko source repository information. This is
 # necessary because MOZ_SOURCE_REPOSITORY and MOZ_SOURCE_CHANGESET both refer
 # to the Thunderbird (comm) repository.
 # We need to have accurate source repository information for MPL compliance.
 
+
+def get_fail_msg(source_name, repo_name, rev_name):
+    return """Unable to determine {} source repository.
+Try setting {} and {}
+environment variables or build from a Mercurial checkout.""".format(
+        source_name, repo_name, rev_name)
+
+
+# Wrap check_cmd_output so that it does not fatally end configure on command
+# failure.
+def hg_cmd_output(*args, **kwargs):
+    def hg_error():
+        return None
+    kwargs['onerror'] = hg_error
+
+    return check_cmd_output(*args, **kwargs)
+
+
 @template
 def read_sourcestamp(repository):
     """
     Last resort, look for the revision data in the sourcestamp file.
     This file only exists in release tar files created in CI.
     repository must be one of "GECKO" or "COMM".
     """
     log.info('Determining %s source information from sourcestamp.txt...'
@@ -123,20 +141,20 @@ def comm_repo_heuristics(comm_environ, p
     set the environment variables the same as Taskcluster would.
     """
     if not comm_environ:
         comm_repo = comm_rev = None
         if hg:
             log.info(
                 "Determining COMM source information from "
                 "Mercurial...")
-            comm_rev = check_cmd_output(hg, '-R', paths.commtopsrcdir,
-                                        'id', '-T', '{p1.node}')
-            comm_repo = check_cmd_output(hg, '-R', paths.commtopsrcdir,
-                                         'path', 'default')
+            comm_rev = hg_cmd_output(hg, '-R', paths.commtopsrcdir,
+                                     'parent', '--template={node}')
+            comm_repo = hg_cmd_output(hg, '-R', paths.commtopsrcdir,
+                                      'path', 'default')
             if comm_repo:
                 comm_repo = comm_repo.strip()
                 if comm_repo.startswith('ssh://'):
                     comm_repo = 'https://' + comm_repo[6:]
                 comm_repo = comm_repo.rstrip('/')
         # TODO: git-cinnabar support?
 
         if not comm_repo or not comm_rev:
@@ -149,27 +167,29 @@ def comm_repo_heuristics(comm_environ, p
         if comm_repo and comm_rev:
             environ['MOZ_SOURCE_REPO'] = comm_repo
             environ['MOZ_SOURCE_CHANGESET'] = comm_rev
 
         if comm_repo and comm_rev:
             return namespace(comm_repo=comm_repo, comm_rev=comm_rev)
 
 
-@depends(comm_repo_from_environ, comm_repo_heuristics)
-def comm_source_repo(from_environ, from_config):
+@depends(comm_repo_from_environ, comm_repo_heuristics, 'MOZ_AUTOMATION')
+def comm_source_repo(from_environ, from_config, automation):
     rv = None
     if from_environ:
         rv = from_environ
     elif from_config:
         rv = from_config
+    elif automation:
+        die(get_fail_msg("COMM", "MOZ_SOURCE_REPO", "MOZ_SOURCE_CHANGESET"))
     else:
-        die("Unable to determine COMM source repository."
-            "Try setting COMM_HEAD_REPOSITORY and COMM_HEAD_REV"
-            "environment variables or build from a Mercurial checkout.")
+        log.info(
+            get_fail_msg("COMM", "MOZ_SOURCE_REPO", "MOZ_SOURCE_CHANGESET"))
+        rv = namespace(comm_repo="unknown", comm_rev="unknown")
 
     log.info('COMM_SOURCE_REPOSITORY: {}'.format(rv.comm_repo))
     log.info('COMM_SOURCE_CHANGESET: {}'.format(rv.comm_rev))
     return rv
 
 
 @depends(application)
 @imports(_from='os', _import="environ")
@@ -218,45 +238,47 @@ def gecko_repo_heuristics(gecko_environ,
             if not gecko_rev and gecko_ref:
                 # gecko_repo is known, but we have a branch ref like
                 # "default" when a revision hash is needed. Try to query
                 # Mercurial first.
                 if hg:
                     log.info(
                         "Determining GECKO source information from "
                         "Mercurial...")
-                    gecko_rev = check_cmd_output(hg, '-R', paths.moztopsrcdir,
-                                                 'id', '-T', '{p1.node}')
+                    gecko_rev = hg_cmd_output(hg, '-R', paths.moztopsrcdir,
+                                              'parent', '--template={node}')
                 # TODO: git-cinnabar support?
 
         if not gecko_repo or not gecko_rev:
             # See if we have a sourcestamo file. Last ditch effort!
             try:
                 gecko_repo, gecko_rev = sourcestamp.repo_url, \
                                         sourcestamp.repo_rev
             except:
                 pass
 
         # Check one last time to see if both gecko_repo and gecko_rev
         # are set
         if gecko_repo and gecko_rev:
             return namespace(gecko_repo=gecko_repo, gecko_rev=gecko_rev)
 
 
-@depends(gecko_repo_from_environ, gecko_repo_heuristics)
-def gecko_source_repo(from_environ, from_heuristics):
+@depends(gecko_repo_from_environ, gecko_repo_heuristics, 'MOZ_AUTOMATION')
+def gecko_source_repo(from_environ, from_heuristics, automation):
     rv = None
     if from_environ:
         rv = from_environ
     elif from_heuristics:
         rv = from_heuristics
+    elif automation:
+        die(get_fail_msg("GECKO", "GECKO_HEAD_REPOSITORY", "GECKO_HEAD_REV"))
     else:
-        die("Unable to determine GECKO source repository."
-            "Try setting GECKO_HEAD_REPOSITORY and GECKO_HEAD_REV"
-            "environment variables or build from a Mercurial checkout.")
+        log.info(
+            get_fail_msg("GECKO", "GECKO_HEAD_REPOSITORY", "GECKO_HEAD_REV"))
+        rv = namespace(gecko_repo="unknown", gecko_rev="unknown")
 
     log.info('GECKO_SOURCE_REPOSITORY: {}'.format(rv.gecko_repo))
     log.info('GECKO_SOURCE_CHANGESET: {}'.format(rv.gecko_rev))
     return rv
 
 
 set_config('MOZ_COMM_SOURCE_REPO', comm_source_repo.comm_repo)
 set_config('MOZ_COMM_SOURCE_CHANGESET', comm_source_repo.comm_rev)
