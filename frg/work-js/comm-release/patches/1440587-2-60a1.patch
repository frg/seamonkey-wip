# HG changeset patch
# User Philipp Kewisch <mozilla@kewis.ch>
# Date 1519311748 -3600
# Node ID 0f768b93b1a7897f34f66ed2a3845f3e954e382a
# Parent  dc3f92e409e6eae612c1ef555f214f1352e59d65
Bug 1440587 - Move calProviderUtils to the cal.provider namespace - rfc3339 automatic changes. r=MakeMyDay

MozReview-Commit-ID: BiUGFO9JdB6

diff --git a/calendar/providers/gdata/components/calGoogleCalendar.js b/calendar/providers/gdata/components/calGoogleCalendar.js
--- a/calendar/providers/gdata/components/calGoogleCalendar.js
+++ b/calendar/providers/gdata/components/calGoogleCalendar.js
@@ -751,17 +751,17 @@ calGoogleCalendar.prototype = {
         let tasksRequest = new calGoogleRequest();
         let tasksPromise = Promise.resolve();
         tasksRequest.calendar = this;
         tasksRequest.type = tasksRequest.GET;
         tasksRequest.uri = this.createTasksURI("tasks");
         tasksRequest.addQueryParameter("maxResults", maxResults);
         let lastUpdated = this.getUpdatedMin("tasks");
         if (lastUpdated) {
-            tasksRequest.addQueryParameter("updatedMin", cal.toRFC3339(lastUpdated));
+            tasksRequest.addQueryParameter("updatedMin", cal.dtz.toRFC3339(lastUpdated));
             tasksRequest.addQueryParameter("showDeleted", "true");
         }
         if (tasksRequest.uri && this.checkThrottle("tasks")) {
             let saver = new ItemSaver(this);
             let newLastUpdated = null;
             tasksPromise = this.session.asyncPaginatedRequest(tasksRequest, (aData) => {
                 // On the first request...
                 newLastUpdated = tasksRequest.requestDate.icalString;
diff --git a/calendar/providers/gdata/modules/gdataSession.jsm b/calendar/providers/gdata/modules/gdataSession.jsm
--- a/calendar/providers/gdata/modules/gdataSession.jsm
+++ b/calendar/providers/gdata/modules/gdataSession.jsm
@@ -427,18 +427,18 @@ calGoogleSession.prototype = {
 
         if (aRangeStart) {
             aRangeStart = aRangeStart.getInTimezone(cal.dtz.UTC);
         }
         if (aRangeEnd) {
             aRangeEnd = aRangeEnd.getInTimezone(cal.dtz.UTC);
         }
 
-        let rfcRangeStart = cal.toRFC3339(aRangeStart);
-        let rfcRangeEnd = cal.toRFC3339(aRangeEnd);
+        let rfcRangeStart = cal.dtz.toRFC3339(aRangeStart);
+        let rfcRangeEnd = cal.dtz.toRFC3339(aRangeEnd);
         /* 7 is the length of "mailto:", we've asserted this above */
         let strippedCalId = aCalId.substr(7);
 
         let requestData = {
             timeMin: rfcRangeStart,
             timeMax: rfcRangeEnd,
             items: [{ id: strippedCalId }]
         };
@@ -458,18 +458,18 @@ calGoogleSession.prototype = {
                 let reason = calData.errors && calData.errors[0] && calData.errors[0].reason;
                 if (reason) {
                     cal.LOG("[calGoogleCalendar] Could not request freebusy for " + strippedCalId + ": " + reason);
                     failSync(Components.results.NS_ERROR_FAILURE, reason);
                 } else {
                     let utcZone = cal.dtz.UTC;
                     cal.LOG("[calGoogleCalendar] Found " + calData.busy.length + " busy slots within range for " + strippedCalId);
                     let busyRanges = calData.busy.map((entry) => {
-                        let start = cal.fromRFC3339(entry.start, utcZone);
-                        let end = cal.fromRFC3339(entry.end, utcZone);
+                        let start = cal.dtz.fromRFC3339(entry.start, utcZone);
+                        let end = cal.dtz.fromRFC3339(entry.end, utcZone);
                         let interval = new cal.FreeBusyInterval(aCalId, cIFBI.BUSY, start, end);
                         LOGinterval(interval);
                         return interval;
                     });
                     completeSync(busyRanges);
                 }
             } else {
                 cal.ERROR("[calGoogleCalendar] Invalid freebusy response: " + aData.toSource());
diff --git a/calendar/providers/gdata/modules/gdataUtils.jsm b/calendar/providers/gdata/modules/gdataUtils.jsm
--- a/calendar/providers/gdata/modules/gdataUtils.jsm
+++ b/calendar/providers/gdata/modules/gdataUtils.jsm
@@ -132,17 +132,17 @@ function getItemMetadata(aOfflineStorage
  * Covnvert a calIDateTime date to the JSON object expected by Google.
  *
  * @param aDate     The date to convert.
  * @return          The converted JS Object.
  */
 function dateToJSON(aDate) {
     let jsonData = {};
     let tzid = aDate.timezone.tzid;
-    jsonData[aDate.isDate ? "date" : "dateTime"] = cal.toRFC3339(aDate);
+    jsonData[aDate.isDate ? "date" : "dateTime"] = cal.dtz.toRFC3339(aDate);
     if (!aDate.isDate && tzid != "floating") {
         if (tzid in windowsTimezoneMap) {
             // A Windows timezone, likely an outlook invitation.
             jsonData.timeZone = windowsTimezoneMap[tzid];
         } else if (tzid.match(/^[^\/ ]+(\/[^\/ ]+){1,2}$/)) { // eslint-disable-line no-useless-escape
             // An Olson timezone id
             jsonData.timeZone = aDate.timezone.tzid;
         } else {
@@ -201,17 +201,17 @@ function JSONToDate(aEntry, aTimezone) {
         if (zone) {
             dateTime = dateTime.getInTimezone(zone);
         }
     }
     return dateTime;
 }
 
 /**
- * Like cal.fromRFC3339(), but assumes that the passed timezone is the timezone
+ * Like cal.dtz.fromRFC3339(), but assumes that the passed timezone is the timezone
  * for the date. A quick check is done to make sure the offset matches the
  * timezone.
  *
  * @param aStr          The RFC3339 compliant Date String
  * @param aTimezone     The timezone this date string is in
  * @return              A calIDateTime object
  */
 function fromRFC3339FixedZone(aStr, aTimezone) {
@@ -241,37 +241,37 @@ function fromRFC3339FixedZone(aStr, aTim
                 (matches[11] * 3600 + matches[12] * 60);
         }
 
         if (dateTime.timezoneOffset != offset_in_s) {
             // Warn here, since this shouldn't be happening. Then use the
             // original fromRFC3339, which goes through the timezone list and
             // finds the first matching zone.
             cal.WARN("[calGoogleCalendar] " + aStr + " does not match timezone offset for " + aTimezone.tzid);
-            dateTime = cal.fromRFC3339(aStr, aTimezone);
+            dateTime = cal.dtz.fromRFC3339(aStr, aTimezone);
         }
     }
 
     return dateTime;
 }
 fromRFC3339FixedZone.regex = new RegExp(
     "^([0-9]{4})-([0-9]{2})-([0-9]{2})" +
     "([Tt]([0-9]{2}):([0-9]{2}):([0-9]{2})(\\.[0-9]+)?)?" +
     "([Zz]|([+-])([0-9]{2}):([0-9]{2}))?"
 );
 
 /**
- * Like cal.toRFC3339, but include milliseconds. Google timestamps require
+ * Like cal.dtz.toRFC3339, but include milliseconds. Google timestamps require
  * this.
  *
  * @param date      The calIDateTime to convert.
  * @return          The RFC3339 string stamp.
  */
 function toRFC3339Fraction(date) {
-    let str = cal.toRFC3339(date);
+    let str = cal.dtz.toRFC3339(date);
     return str ? str.replace(/(Z?)$/, ".000$1") : null;
 }
 
 /**
  * Converts a calIEvent to a JS Object that can be serialized to JSON.
  *
  * @param aItem         The item to convert.
  * @return              A JS Object representing the item.
@@ -427,31 +427,31 @@ function EventToJSON(aItem, aOfflineStor
     }
 
     if (!alarms.length && aItem.getProperty("X-DEFAULT-ALARM") == "TRUE") {
         delete itemData.reminders.overrides;
         itemData.reminders.useDefault = true;
     }
 
     // gd:extendedProperty (alarmLastAck)
-    addExtendedProperty("X-MOZ-LASTACK", cal.toRFC3339(aItem.alarmLastAck), true);
+    addExtendedProperty("X-MOZ-LASTACK", cal.dtz.toRFC3339(aItem.alarmLastAck), true);
 
     // XXX While Google now supports multiple alarms and alarm values, we still
     // need to fix bug 353492 first so we can better take care of finding out
     // what alarm is used for snoozing.
 
     // gd:extendedProperty (snooze time)
     let itemSnoozeTime = aItem.getProperty("X-MOZ-SNOOZE-TIME");
     let icalSnoozeTime = null;
     if (itemSnoozeTime) {
         // The propery is saved as a string, translate back to calIDateTime.
         icalSnoozeTime = cal.createDateTime();
         icalSnoozeTime.icalString = itemSnoozeTime;
     }
-    addExtendedProperty("X-MOZ-SNOOZE-TIME", cal.toRFC3339(icalSnoozeTime), true);
+    addExtendedProperty("X-MOZ-SNOOZE-TIME", cal.dtz.toRFC3339(icalSnoozeTime), true);
 
     // gd:extendedProperty (snooze recurring alarms)
     let snoozeValue = "";
     if (aItem.recurrenceInfo) {
         // This is an evil workaround since we don't have a really good system
         // to save the snooze time for recurring alarms or even retrieve them
         // from the event. This should change when we have multiple alarms
         // support.
@@ -514,19 +514,19 @@ function TaskToJSON(aItem, aOfflineStora
     setIf(itemData, "title", aItem.title);
     setIf(itemData, "notes", aItem.getProperty("DESCRIPTION"));
     setIf(itemData, "position", aItem.getProperty("X-SORTKEY"));
     itemData.status = (aItem.isCompleted ? "completed" : "needsAction");
 
     if (aItem.dueDate) {
         let dueDate = aItem.dueDate.getInTimezone(cal.dtz.UTC);
         dueDate.isDate = false;
-        itemData.due = cal.toRFC3339(dueDate);
+        itemData.due = cal.dtz.toRFC3339(dueDate);
     }
-    setIf(itemData, "completed", cal.toRFC3339(aItem.completedDate));
+    setIf(itemData, "completed", cal.dtz.toRFC3339(aItem.completedDate));
 
     for (let relation of aItem.getRelations({})) {
         if (relation.relId &&
             (!relation.relType || relation.relType == "PARENT")) {
             itemData.parent = relation.relId;
             break;
         }
     }
@@ -702,17 +702,17 @@ function JSONToEvent(aEntry, aCalendar, 
         item.status = (aEntry.status ? aEntry.status.toUpperCase() : null);
         item.title = aEntry.summary;
         if (accessRole == "freeBusyReader") {
             item.title = getProviderString("busyTitle", aCalendar.name);
         }
         item.privacy = (aEntry.visibility ? aEntry.visibility.toUpperCase() : null);
 
         item.setProperty("URL", aEntry.htmlLink && aCalendar.uri.schemeIs("https") ? aEntry.htmlLink.replace(/^http:/, "https:") : aEntry.htmlLink);
-        item.setProperty("CREATED", (aEntry.created ? cal.fromRFC3339(aEntry.created, calendarZone).getInTimezone(cal.dtz.UTC) : null));
+        item.setProperty("CREATED", (aEntry.created ? cal.dtz.fromRFC3339(aEntry.created, calendarZone).getInTimezone(cal.dtz.UTC) : null));
         item.setProperty("DESCRIPTION", aEntry.description);
         item.setProperty("LOCATION", aEntry.location);
         item.setProperty("TRANSP", (aEntry.transparency ? aEntry.transparency.toUpperCase() : null));
         item.setProperty("SEQUENCE", aEntry.sequence);
         aMetadata.etag = aEntry.etag;
         aMetadata.path = aEntry.id;
 
         // organizer
@@ -797,20 +797,20 @@ function JSONToEvent(aEntry, aCalendar, 
             if (aEntry.reminders.overrides) {
                 for (let reminderEntry of aEntry.reminders.overrides) {
                     item.addAlarm(JSONToAlarm(reminderEntry));
                 }
             }
         }
 
         // extendedProperty (alarmLastAck)
-        item.alarmLastAck = cal.fromRFC3339(privateProps["X-MOZ-LASTACK"], calendarZone);
+        item.alarmLastAck = cal.dtz.fromRFC3339(privateProps["X-MOZ-LASTACK"], calendarZone);
 
         // extendedProperty (snooze time)
-        let dtSnoozeTime = cal.fromRFC3339(privateProps["X-MOZ-SNOOZE-TIME"], calendarZone);
+        let dtSnoozeTime = cal.dtz.fromRFC3339(privateProps["X-MOZ-SNOOZE-TIME"], calendarZone);
         let snoozeProperty = (dtSnoozeTime ? dtSnoozeTime.icalString : null);
         item.setProperty("X-MOZ-SNOOZE-TIME", snoozeProperty);
 
         // extendedProperty (snooze recurring alarms)
         if (item.recurrenceInfo) {
             // Transform back the string into our snooze properties
             let snoozeObj;
             try {
@@ -830,17 +830,17 @@ function JSONToEvent(aEntry, aCalendar, 
         // Google does not support categories natively, but allows us to store
         // data as an "extendedProperty", and here it's going to be retrieved
         // again
         let categories = cal.category.stringToArray(sharedProps["X-MOZ-CATEGORIES"]);
         item.setCategories(categories.length, categories);
 
         // updated (This must be set last!)
         if (aEntry.updated) {
-            let updated = cal.fromRFC3339(aEntry.updated, calendarZone).getInTimezone(cal.dtz.UTC);
+            let updated = cal.dtz.fromRFC3339(aEntry.updated, calendarZone).getInTimezone(cal.dtz.UTC);
             item.setProperty("DTSTAMP", updated);
             item.setProperty("LAST-MODIFIED", updated);
         }
     } catch (e) {
         cal.ERROR(stringException(e));
         throw e;
     }
     return item;
@@ -876,22 +876,22 @@ function JSONToTask(aEntry, aCalendar, a
         item.setProperty("X-GOOGLE-SORTKEY", aEntry.position);
         item.isCompleted = (aEntry.status == "completed");
 
         aMetadata.etag = aEntry.etag;
         aMetadata.path = aEntry.id;
 
         // Google Tasks don't have a due time, but still use 0:00 UTC. They
         // should really be using floating time.
-        item.dueDate = cal.fromRFC3339(aEntry.due, cal.dtz.floating);
+        item.dueDate = cal.dtz.fromRFC3339(aEntry.due, cal.dtz.floating);
         if (item.dueDate) {
             item.dueDate.timezone = cal.dtz.floating;
             item.dueDate.isDate = true;
         }
-        item.completedDate = cal.fromRFC3339(aEntry.completed, calendarZone);
+        item.completedDate = cal.dtz.fromRFC3339(aEntry.completed, calendarZone);
         if (aEntry.deleted) {
             item.status = "CANCELLED";
         } else if (aEntry.status == "needsAction") {
             item.status = "NEEDS-ACTION";
         } else {
             item.status = "COMPLETED";
         }
 
@@ -908,18 +908,18 @@ function JSONToTask(aEntry, aCalendar, a
                 attach.uri = Services.io.newURI(link.link);
                 attach.setParameter("FILENAME", link.description);
                 attach.setParameter("X-GOOGLE-TYPE", link.type);
                 item.addAttachment(attach);
             }
         }
 
         // updated (This must be set last!)
-        item.setProperty("DTSTAMP", cal.fromRFC3339(aEntry.updated, calendarZone).getInTimezone(cal.dtz.UTC));
-        item.setProperty("LAST-MODIFIED", cal.fromRFC3339(aEntry.updated, calendarZone).getInTimezone(cal.dtz.UTC));
+        item.setProperty("DTSTAMP", cal.dtz.fromRFC3339(aEntry.updated, calendarZone).getInTimezone(cal.dtz.UTC));
+        item.setProperty("LAST-MODIFIED", cal.dtz.fromRFC3339(aEntry.updated, calendarZone).getInTimezone(cal.dtz.UTC));
     } catch (e) {
         cal.ERROR("[calGoogleCalendar] Error parsing JSON tasks stream: " + stringException(e));
         throw e;
     }
 
     return item;
 }
 
diff --git a/calendar/test/unit/test_gdata_provider.js b/calendar/test/unit/test_gdata_provider.js
--- a/calendar/test/unit/test_gdata_provider.js
+++ b/calendar/test/unit/test_gdata_provider.js
@@ -393,35 +393,35 @@ GDataServer.prototype = {
         jsonData.kind = "calendar#event";
         jsonData.etag = this.nextEtag || '"' + (new Date()).getTime() + '"';
         jsonData.id = generateID();
         if (!isImport) { jsonData.htmlLink = this.baseUri + "/calendar/event?eid=" + jsonData.id; }
         if (!isImport || !jsonData.iCalUID) {
             jsonData.iCalUID = jsonData.id + "@google.com";
         }
         if (!isImport || !jsonData.created) {
-            jsonData.created = cal.toRFC3339(cal.dtz.now());
+            jsonData.created = cal.dtz.toRFC3339(cal.dtz.now());
         }
         if (!isImport || !jsonData.updated) {
-            jsonData.updated = cal.toRFC3339(cal.dtz.now());
+            jsonData.updated = cal.dtz.toRFC3339(cal.dtz.now());
         }
         if (!isImport || !jsonData.creator) {
             jsonData.creator = this.creator;
         }
         if (!isImport || !jsonData.organizer) {
             jsonData.organizer = this.creator;
         }
         this.nextEtag = null;
         return jsonData;
     },
 
     processModifyEvent: function(jsonData, id) {
         jsonData.kind = "calendar#event";
         jsonData.etag = this.nextEtag || '"' + (new Date()).getTime() + '"';
-        jsonData.updated = cal.toRFC3339(cal.dtz.now());
+        jsonData.updated = cal.dtz.toRFC3339(cal.dtz.now());
         jsonData.id = id;
         jsonData.iCalUID = (jsonData.recurringEventId || jsonData.id) + "@google.com";
         if (!jsonData.creator) {
             jsonData.creator = this.creator;
         }
         if (!jsonData.organizer) {
             jsonData.organizer = this.creator;
         }
@@ -434,32 +434,32 @@ GDataServer.prototype = {
         jsonData.kind = "tasks#task";
         jsonData.etag = this.nextEtag || '"' + (new Date()).getTime() + '"';
         jsonData.id = generateID();
         jsonData.position = generateID(); // Not a real position, but we don't really use this at the moment
         if (!jsonData.status) {
             jsonData.status = "needsAction";
         }
         if (!jsonData.updated) {
-            jsonData.updated = cal.toRFC3339(cal.dtz.now());
+            jsonData.updated = cal.dtz.toRFC3339(cal.dtz.now());
         }
 
         this.nextEtag = null;
         return jsonData;
     },
 
     processModifyTask: function(jsonData) {
         jsonData.kind = "tasks#task";
         jsonData.etag = this.nextEtag || '"' + (new Date()).getTime() + '"';
-        jsonData.updated = cal.toRFC3339(cal.dtz.now());
+        jsonData.updated = cal.dtz.toRFC3339(cal.dtz.now());
         if (!jsonData.status) {
             jsonData.status = "needsAction";
         }
         if (!jsonData.updated) {
-            jsonData.updated = cal.toRFC3339(cal.dtz.now());
+            jsonData.updated = cal.dtz.toRFC3339(cal.dtz.now());
         }
 
         this.nextEtag = null;
         return jsonData;
     },
 };
 
 function findKey(container, key, searchKey) {
diff --git a/calendar/test/unit/test_rfc3339_parser.js b/calendar/test/unit/test_rfc3339_parser.js
--- a/calendar/test/unit/test_rfc3339_parser.js
+++ b/calendar/test/unit/test_rfc3339_parser.js
@@ -10,17 +10,17 @@ function run_test() {
 
 function really_run_test() {
     // Check if the RFC 3339 date and timezone are properly parsed to the
     // expected result and if the result is properly mapped back into the RFC
     // 3339 date.
     function testRfc3339(aRfc3339Date, aTimezone, aExpectedDateTime,
                           aExpectedRfc3339Date=aRfc3339Date) {
         // Test creating a dateTime object from an RFC 3339 string.
-        let dateTime = cal.fromRFC3339(aRfc3339Date, aTimezone);
+        let dateTime = cal.dtz.fromRFC3339(aRfc3339Date, aTimezone);
 
         // Check that each property is as expected.
         let expectedDateProps = {
             year: aExpectedDateTime[0],
             month: aExpectedDateTime[1] - 1, // 0 based month.
             day: aExpectedDateTime[2],
             hour: aExpectedDateTime[3],
             minute: aExpectedDateTime[4],
@@ -35,17 +35,17 @@ function really_run_test() {
             if (prop == "timezone") {
                 equal(dateTime[prop].tzid, expectedDateProps[prop].tzid);
             } else {
                 equal(dateTime[prop], expectedDateProps[prop]);
             }
         }
 
         // Test round tripping that dateTime object back to an RFC 3339 string.
-        let rfc3339Date = cal.toRFC3339(dateTime);
+        let rfc3339Date = cal.dtz.toRFC3339(dateTime);
 
         // In theory this should just match the input RFC 3339 date, but there are
         // multiple ways of generating the same time, e.g. 2006-03-14Z is
         // equivalent to 2006-03-14.
         equal(rfc3339Date, aExpectedRfc3339Date);
     }
 
     /*
