# HG changeset patch
# User Markus Adrario <mozilla@adrario.de>
# Date 1542880311 -3600
# Node ID 5af3c13ff2eeaa2889348519ff7e40353c84deb1
# Parent  def167470374c2a08072a4e6f45214a08596ac52
Bug 1496602 [mozmill] move timezone setting code from timezone-test to item-editing-helpers. r=darktrojan

diff --git a/calendar/test/mozmill/shared-modules/test-item-editing-helpers.js b/calendar/test/mozmill/shared-modules/test-item-editing-helpers.js
--- a/calendar/test/mozmill/shared-modules/test-item-editing-helpers.js
+++ b/calendar/test/mozmill/shared-modules/test-item-editing-helpers.js
@@ -98,16 +98,17 @@ function installInto(module) {
     module.REC_DLG_UNTIL_INPUT = REC_DLG_UNTIL_INPUT;
     // Now copy helper functions.
     module.helpersForEditUI = helpersForEditUI;
     module.setData = setData;
     module.setReminderMenulist = setReminderMenulist;
     module.setCategories = setCategories;
     module.handleAddingAttachment = handleAddingAttachment;
     module.acceptSendingNotificationMail = acceptSendingNotificationMail;
+    module.setTimezone = setTimezone;
 }
 
 function helpersForEditUI(controller) {
     function selector(sel) {
         return sel.trim().replace(/\n(\s*)/g, "");
     }
 
     let isEvent = cal.isEvent(controller.window.calendarItem);
@@ -184,16 +185,17 @@ function helpersForEditUI(controller) {
  *                      categories - array of category names
  *                      calendar - Calendar the item should be in.
  *                      allday - boolean value
  *                      startdate - Date object
  *                      starttime - Date object
  *                      enddate - Date object
  *                      endtime - Date object
  *                      timezonedisplay - False for hidden, true for shown.
+ *                      timezone - String identifying the Timezone.
  *                      repeat - reccurrence value, one of none/daily/weekly/
  *                               every.weekday/bi.weekly/
  *                               monthly/yearly
  *                               (Custom is not supported.)
  *                      repeatuntil - Date object
  *                      reminder - none/0minutes/5minutes/15minutes/30minutes
  *                                 1hour/2hours/12hours/1day/2days/1week
  *                                 (Custom is not supported.)
@@ -258,16 +260,21 @@ function setData(dialog, iframe, data) {
     // timezonedisplay
     if (data.timezonedisplay !== undefined) {
         let menuitem = eid("options-timezones-menuitem");
         if (menuitem.getNode().getAttribute("checked") != data.timezonedisplay) {
             dialog.click(menuitem);
         }
     }
 
+    // timezone
+    if (data.timezone !== undefined) {
+        setTimezone(dialog, data.timezone);
+    }
+
     // startdate
     if (data.startdate != undefined && data.startdate.constructor.name == "Date") {
         let startdate = dateFormatter.formatDateShort(
             cal.dtz.jsDateToDateTime(data.startdate, cal.dtz.floating)
         );
 
         if (!isEvent) {
             dialog.check(iframeid("todo-has-entrydate"), true);
@@ -476,35 +483,43 @@ function setCategories(dialog, iframe, c
                 item.removeAttribute("checked");
             }
         }
     }
     dialog.click(categoryMenulist);
 }
 
 /**
- * Add an attachment with url.
+ * Add an URL attachment.
  *
  * @param controller        Mozmill window controller
+ * @param url               URL to be added
  */
 function handleAddingAttachment(controller, url) {
     let { eid } = helpersForController(controller);
     plan_for_modal_dialog("commonDialog", (attachment) => {
         let { lookup: cdlglookup, eid: cdlgid } = helpersForController(attachment);
         attachment.waitForElement(cdlgid("loginTextbox"));
         cdlgid("loginTextbox").getNode().value = url;
         attachment.click(cdlglookup(`
             /id("commonDialog")/anon({"anonid":"buttons"})/{"dlgtype":"accept"}
         `));
     });
     controller.click(eid("button-url"));
 
     wait_for_modal_dialog("commonDialog", TIMEOUT_MODAL_DIALOG);
 }
 
+/**
+ * Add attendees to the event.
+ *
+ * @param dialog            The controller of the Edit Dialog.
+ * @param innerFrame        The controller of the item iframe.
+ * @param attendeesString   Comma separated list of eMail-Addresses to add.
+ */
 function addAttendees(dialog, innerFrame, attendeesString) {
     let { eid: dlgid } = helpersForController(dialog);
 
     let attendees = attendeesString.split(",");
     for (let attendee of attendees) {
         let calAttendee = innerFrame.window.attendees.find(
             aAtt => aAtt.id == `mailto:${attendee}`
         );
@@ -528,24 +543,68 @@ function addAttendees(dialog, innerFrame
                 `));
             });
             dialog.click(dlgid("button-attendees"));
             wait_for_modal_dialog("Calendar:EventDialog:Attendees", TIMEOUT_MODAL_DIALOG);
         }
     }
 }
 
+/**
+ * Delete attendees from the event.
+ *
+ * @param dialog            The controller of the Edit Dialog.
+ * @param innerFrame        The controller of the item iframe.
+ * @param attendeesString   Comma separated list of eMail-Addresses to delete.
+ */
 function deleteAttendees(event, innerFrame, attendeesString) {
     let { iframeLookup } = helpersForEditUI(innerFrame);
 
     // Now delete the attendees.
     let attendees = attendeesString.split(",");
     for (let attendee of attendees) {
         let attendeeToDelete = iframeLookup(`${ATTENDEES_ROW}/{"attendeeid":"mailto:${attendee}"}`);
         // Unfortunately the context menu of the attendees is not working in
         // Mozmill tests. Thus we have to use the JS-functions.
         let calAttendee = innerFrame.window.attendees.find(aAtt => aAtt.id == `mailto:${attendee}`);
         if (calAttendee) {
             innerFrame.window.removeAttendee(calAttendee);
         }
         event.waitForElementNotPresent(attendeeToDelete);
     }
 }
+
+/**
+ * Set the timezone for the item
+ *
+ * @param event           The controller of the Edit Dialog.
+ * @param timezone        String identifying the Timezone.
+ */
+function setTimezone(event, timezone) {
+    let { eid: eventid } = helpersForController(event);
+    let eventCallback = function(zone, tzcontroller) {
+        let { lookup: tzlookup, xpath: tzpath } = helpersForController(tzcontroller);
+
+        let item = tzpath(`
+            /*[name()='dialog']/*[name()='menulist'][1]/*[name()='menupopup'][1]/
+            *[@value='${zone}']
+        `);
+        tzcontroller.waitForElement(item);
+        tzcontroller.click(item);
+        tzcontroller.click(tzlookup(`
+            /id("calendar-event-dialog-timezone")/anon({"anonid":"buttons"})/
+            {"dlgtype":"accept"}
+        `));
+    };
+
+    if (eventid("timezone-starttime").getNode().collapsed) {
+        let menuitem = eventid("options-timezones-menuitem");
+        event.click(menuitem);
+    }
+
+    plan_for_modal_dialog("Calendar:EventDialog:Timezone", eventCallback.bind(null, timezone));
+    event.waitForElement(eventid("timezone-starttime"));
+    event.click(eventid("timezone-starttime"));
+    event.click(eventid("timezone-starttime"));
+    event.waitForElement(eventid("timezone-custom-menuitem"));
+    event.click(eventid("timezone-custom-menuitem"));
+    wait_for_modal_dialog("Calendar:EventDialog:Timezone", TIMEOUT_MODAL_DIALOG);
+}
diff --git a/calendar/test/mozmill/testTimezones.js b/calendar/test/mozmill/testTimezones.js
--- a/calendar/test/mozmill/testTimezones.js
+++ b/calendar/test/mozmill/testTimezones.js
@@ -1,56 +1,51 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 var MODULE_NAME = "testTimezones";
 var RELATIVE_ROOT = "./shared-modules";
 var MODULE_REQUIRES = ["calendar-utils", "item-editing-helpers", "window-helpers"];
 
-var TIMEOUT_MODAL_DIALOG, CANVAS_BOX, DAY_VIEW;
+var CANVAS_BOX, DAY_VIEW;
 var helpersForController, invokeEventDialog, switchToView, goToDate;
 var findEventsInNode, viewForward, viewBack;
 var setData;
-var plan_for_modal_dialog, wait_for_modal_dialog;
 
 var DATES = [
     [2009, 1, 1], [2009, 4, 2], [2009, 4, 16], [2009, 4, 30],
     [2009, 7, 2], [2009, 10, 15], [2009, 10, 29], [2009, 11, 5]
 ];
 
 var TIMEZONES = [
     "America/St_Johns", "America/Caracas", "America/Phoenix", "America/Los_Angeles",
     "America/Argentina/Buenos_Aires", "Europe/Paris", "Asia/Kathmandu", "Australia/Adelaide"
 ];
 
 ChromeUtils.import("resource://gre/modules/Services.jsm");
 
 function setupModule(module) {
     controller = mozmill.getMail3PaneController();
     ({
-        TIMEOUT_MODAL_DIALOG,
         CANVAS_BOX,
         DAY_VIEW,
         helpersForController,
         invokeEventDialog,
         switchToView,
         goToDate,
         findEventsInNode,
         viewForward,
         viewBack
     } = collector.getModule("calendar-utils"));
     collector.getModule("calendar-utils").setupModule(controller);
     Object.assign(module, helpersForController(controller));
 
     ({ setData } = collector.getModule("item-editing-helpers"));
     collector.getModule("item-editing-helpers").setupModule(module);
-
-    ({ plan_for_modal_dialog, wait_for_modal_dialog } =
-        collector.getModule("window-helpers"));
 }
 
 function testTimezones1_SetGMT() {
     Services.prefs.setStringPref("calendar.timezone.local", "Europe/London");
 }
 
 function testTimezones2_CreateEvents() {
     goToDate(controller, 2009, 1, 1);
@@ -59,21 +54,23 @@ function testTimezones2_CreateEvents() {
     let times = [[4, 30], [5, 0], [3, 0], [3, 0], [9, 0], [14, 0], [19, 45], [1, 30]];
     let time = new Date();
     for (let i = 0; i < TIMEZONES.length; i++) {
         let eventBox = lookupEventBox("day", CANVAS_BOX, null, 1, i + 11);
         invokeEventDialog(controller, eventBox, (event, iframe) => {
             time.setHours(times[i][0]);
             time.setMinutes(times[i][1]);
 
-            // Set timezone.
-            setTimezone(event, TIMEZONES[i]);
-
-            // Set title and repeat.
-            setData(event, iframe, { title: TIMEZONES[i], repeat: "weekly", starttime: time });
+            // Set event data.
+            setData(event, iframe, {
+                title: TIMEZONES[i],
+                repeat: "weekly",
+                starttime: time,
+                timezone: TIMEZONES[i]
+            });
 
             // save
             let { eid: eventid } = helpersForController(event);
             event.click(eventid("button-saveandclose"));
         });
     }
 }
 
@@ -229,48 +226,16 @@ function testTimezones10_checkAdelaide()
     goToDate(controller, 2009, 1, 1);
 
     verify(controller, DATES, TIMEZONES, times);
 }
 
 function teardownTest(module) {
 }
 
-function setTimezone(event, timezone) {
-    let { eid: eventid } = helpersForController(event);
-
-    if (eventid("timezone-starttime").getNode().collapsed) {
-        let menuitem = eventid("options-timezones-menuitem");
-        event.click(menuitem);
-    }
-
-    plan_for_modal_dialog("Calendar:EventDialog:Timezone", eventCallback.bind(null, timezone));
-    event.waitForElement(eventid("timezone-starttime"));
-    event.click(eventid("timezone-starttime"));
-    event.click(eventid("timezone-starttime"));
-    event.waitForElement(eventid("timezone-custom-menuitem"));
-    event.click(eventid("timezone-custom-menuitem"));
-    wait_for_modal_dialog("Calendar:EventDialog:Timezone", TIMEOUT_MODAL_DIALOG);
-}
-
-function eventCallback(zone, tzcontroller) {
-    let { lookup: tzlookup, xpath: tzpath } = helpersForController(tzcontroller);
-
-    let item = tzpath(`
-        /*[name()='dialog']/*[name()='menulist'][1]/*[name()='menupopup'][1]/
-        *[@value='${zone}']
-    `);
-    tzcontroller.waitForElement(item);
-    tzcontroller.click(item);
-    tzcontroller.click(tzlookup(`
-        /id("calendar-event-dialog-timezone")/anon({"anonid":"buttons"})/
-        {"dlgtype":"accept"}
-    `));
-}
-
 function verify(controller, dates, timezones, times) {
     function* datetimes() {
         for (let idx = 0; idx < dates.length; idx++) {
             yield [dates[idx][0], dates[idx][1], dates[idx][2], times[idx]];
         }
     }
 
     let { lookup } = helpersForController(controller);
