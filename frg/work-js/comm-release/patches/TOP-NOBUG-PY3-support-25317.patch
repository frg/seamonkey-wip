# HG changeset patch
# User Myckel Habets <gentoo-bugs@habets-dobben.nl>
# Date 1684418805 -7200
# Parent  ed7b5904f5e692f52fbe2858b061ab87931e3207
No Bug - Python 3 support in calendar/timezones/*.py. r=frg a=frg

diff --git a/calendar/timezones/update-zones.py b/calendar/timezones/update-zones.py
--- a/calendar/timezones/update-zones.py
+++ b/calendar/timezones/update-zones.py
@@ -12,17 +12,29 @@ You can also have the latest tzdata down
 
 IMPORTANT: Make sure your local copy of zones.json is in sync with Hg before running this script.
 Otherwise manual corrections will get dropped when pushing the update.
 
 """
 
 from __future__ import absolute_import, print_function, unicode_literals
 
-import argparse, ftplib, json, os, os.path, re, shutil, subprocess, sys, tarfile, tempfile
+import argparse
+import ftplib
+import io
+import json
+import os
+import os.path
+import re
+import shutil
+import subprocess
+import sys
+import tarfile
+import tempfile
+
 from collections import OrderedDict
 
 
 class TimezoneUpdater(object):
     """ Timezone updater class, use the run method to do everything automatically"""
     def __init__(self, tzdata_path, zoneinfo_path, zoneinfo_pure_path):
         self.tzdata_path = tzdata_path
         self.zoneinfo_path = zoneinfo_path
@@ -30,28 +42,28 @@ class TimezoneUpdater(object):
 
     def download_tzdata(self):
         """Download the latest tzdata from ftp.iana.org"""
         tzdata_download_path = tempfile.mktemp(".tar.gz", prefix="zones")
         sys.stderr.write("Downloading tzdata-latest.tar.gz from"
                          " ftp.iana.org to %s\n" % tzdata_download_path)
         ftp = ftplib.FTP("ftp.iana.org")
         ftp.login()
-        ftp.retrbinary("RETR /tz/tzdata-latest.tar.gz", open(tzdata_download_path, "wb").write)
+        ftp.retrbinary("RETR /tz/tzdata-latest.tar.gz", io.open(tzdata_download_path, "wb").write)
         ftp.quit()
 
         self.tzdata_path = tempfile.mkdtemp(prefix="zones")
         sys.stderr.write("Extracting %s to %s\n" % (tzdata_download_path, self.tzdata_path))
         tarfile.open(tzdata_download_path).extractall(path=self.tzdata_path)
         os.unlink(tzdata_download_path)
 
     def get_tzdata_version(self):
         """Extract version number of tzdata files."""
         version = None
-        with open(os.path.join(self.tzdata_path, "version"), "r") as versionfile:
+        with io.open(os.path.join(self.tzdata_path, "version"), "r", encoding='utf-8') as versionfile:
             for line in versionfile:
                 match = re.match(r"\w+", line)
                 if match is not None:
                     version = "2." + match.group(0)
                     break
         return version
 
     def run_vzic(self, vzic_path):
@@ -71,47 +83,47 @@ class TimezoneUpdater(object):
             "--olson-dir", self.tzdata_path,
             "--output-dir", self.zoneinfo_pure_path,
             "--pure"
         ], stdout=sys.stderr)
 
     def read_backward(self):
         """Read the 'backward' file, which contains timezone identifier links"""
         links = {}
-        with open(os.path.join(self.tzdata_path, "backward"), "r") as backward:
+        with io.open(os.path.join(self.tzdata_path, "backward"), "r", encoding='utf-8') as backward:
             for line in backward:
                 parts = line.strip().split()
                 if len(parts) == 3 and parts[0] == "Link":
                     _, tgt, src = parts
                     links[src] = tgt
         return links
 
     def read_zones_tab(self):
         """Read zones.tab for latitude and longitude data."""
         lat_long_data = {}
-        with open(os.path.join(self.zoneinfo_path, "zones.tab"), "r") as tab:
+        with io.open(os.path.join(self.zoneinfo_path, "zones.tab"), "r", encoding='utf-8') as tab:
             for line in tab:
                 if len(line) < 19:
                     sys.stderr.write("Line in zones.tab not long enough: %s\n" % line.strip())
                     continue
 
                 [latitude, longitude, name] = line.rstrip().split(" ", 2)
                 lat_long_data[name] = (latitude, longitude)
 
         return lat_long_data
 
     def read_ics(self, filename, lat_long_data):
         """Read a single zone's ICS files.
 
         We keep only the lines we want, and we use the pure version of RRULE if
         the versions differ. See Asia/Jerusalem for an example."""
-        with open(os.path.join(self.zoneinfo_path, filename), "r") as zone:
+        with io.open(os.path.join(self.zoneinfo_path, filename), "r", encoding='utf-8') as zone:
             zoneinfo = zone.readlines()
 
-        with open(os.path.join(self.zoneinfo_pure_path, filename), "r") as zone:
+        with io.open(os.path.join(self.zoneinfo_pure_path, filename), "r", encoding='utf-8') as zone:
             zoneinfo_pure = zone.readlines()
 
         ics_data = []
         for i in range(0, len(zoneinfo)):
             line = zoneinfo[i]
             key = line[:line.find(":")]
 
             if key == "BEGIN":
@@ -166,52 +178,52 @@ class TimezoneUpdater(object):
         return aliases
 
     @staticmethod
     def update_timezones_properties(tzprops_file, version, newzones, aliases):
         TZ_LINE = re.compile(r'^(?P<name>pref.timezone.[^=]+)=(?P<value>.*)$')
         outlines = []
         zoneprops = {}
 
-        with open(tzprops_file) as fp:
+        with io.open(tzprops_file, encoding='utf-8') as fp:
             for line in fp.readlines():
                 match = TZ_LINE.match(line.rstrip("\n"))
                 if match:
                     zoneprops[match.group('name')] = match.group('value')
 
         for zone in newzones:
             propname = 'pref.timezone.' + zone.replace('/', '.')
             if propname not in zoneprops:
                 outlines.append(propname + "=" + zone.replace("_", " "))
 
         if len(outlines):
-            with open(tzprops_file, 'a') as fp:
+            with io.open(tzprops_file, 'a', encoding='utf-8', newline='\n') as fp:
                 fp.write("\n#added with %s\n" % version)
                 fp.write("\n".join(outlines) + "\n")
 
     @staticmethod
     def write_output(version, aliases, zones, filename):
         """Write the data to zones.json."""
         data = OrderedDict()
         data["version"] = version
         data["aliases"] = OrderedDict(sorted(aliases.items()))
         data["zones"] = OrderedDict(sorted(zones.items()))
 
-        with open(filename, "w") as jsonfile:
+        with io.open(filename, "w", encoding='utf-8', newline='\n') as jsonfile:
             json.dump(data, jsonfile, indent=2, separators=(",", ": "))
             jsonfile.write("\n")
 
     def run(self, zones_json_file, tzprops_file, vzic_path):
         """Run the timezone updater, with a zones.json file and the path to vzic"""
 
         need_download_tzdata = self.tzdata_path is None
         if need_download_tzdata:
             self.download_tzdata()
 
-        with open(zones_json_file, "r") as jsonfile:
+        with io.open(zones_json_file, "r", encoding='utf-8') as jsonfile:
             zonesjson = json.load(jsonfile)
 
         version = self.get_tzdata_version()
         if version == zonesjson["version"]:
             sys.stderr.write("zones.json is already up to date (%s)\n" % version)
             return
         else:
             sys.stderr.write("You are using tzdata %s\n" % version[2:])
@@ -255,23 +267,23 @@ def create_test_data(zones_file):
     """Creating test data."""
 
     previous_file = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                  "..", "test", "unit", "data", "previous.json")
 
     previous_version = "no previous version"
     current_version = "no current version"
     if (os.path.isfile(zones_file) and os.access(zones_file, os.R_OK)):
-        with open(zones_file, "r") as rzf:
+        with io.open(zones_file, "r", encoding='utf-8') as rzf:
             current_data = json.load(rzf)
             current_version = current_data["version"]
             current_zones = current_data["zones"]
             current_aliases = current_data["aliases"]
     if (os.path.isfile(previous_file) and os.access(previous_file, os.R_OK)):
-        with open(previous_file, "r") as rpf:
+        with io.open(previous_file, "r", encoding='utf-8') as rpf:
             previous_data = json.load(rpf)
             previous_version = previous_data["version"]
 
     if (current_version == "no current version"):
         """Test data creation not possible - currently no zones.json file available."""
 
     elif (current_version != previous_version):
         """Extracting data from zones.json"""
@@ -280,17 +292,17 @@ def create_test_data(zones_file):
         test_zones = current_zones.keys()
 
         test_data = OrderedDict()
         test_data["version"] = current_version
         test_data["aliases"] = sorted(test_aliases)
         test_data["zones"] = sorted(test_zones)
 
         """Writing test data"""
-        with open(previous_file, "w") as wpf:
+        with io.open(previous_file, "w", encoding='utf-8', newline='\n') as wpf:
             json.dump(test_data, wpf, indent=2, separators=(",", ": "))
             wpf.write("\n")
 
         """Please run calendar xpshell test 'test_timezone_definition.js' to check the updated
         timezone definition for any glitches."""
 
     else:
         # This may happen if the script is executed multiple times without new tzdata available
diff --git a/calendar/timezones/version.py b/calendar/timezones/version.py
--- a/calendar/timezones/version.py
+++ b/calendar/timezones/version.py
@@ -1,14 +1,16 @@
 #!/usr/bin/python
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import, print_function, unicode_literals
 
-import json, os.path
+import io
+import json
+import os.path
 
 json_file = os.path.join(os.path.dirname(os.path.realpath(__file__)), "zones.json")
 
-with open(json_file, "r") as fp:
+with io.open(json_file, "r", encoding='utf-8') as fp:
     data = json.load(fp)
     print(data["version"])
