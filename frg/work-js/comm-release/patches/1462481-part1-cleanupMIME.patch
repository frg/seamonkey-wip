# HG changeset patch
# User Ben Bucksch <ben.bucksch@beonex.com>
# Date 1526562660 -7200
# Node ID 1e39ffad84f571a0f4300940639e9da391eb71ea
# Parent  0caac25e4bc56aa0af02484eae2324c3aa3bc742
Bug 1462481 - clean up MIME's HTML sanitizer class. r=mkmelin,jorgk a=jorgk

diff --git a/mailnews/mailnews.js b/mailnews/mailnews.js
--- a/mailnews/mailnews.js
+++ b/mailnews/mailnews.js
@@ -585,17 +585,16 @@ pref("mail.display_glyph", true);   // T
 pref("mail.display_struct", true);  // TXT->HTML *bold* etc. in viewer; ditto
 pref("mail.send_struct", false);   // HTML->HTML *bold* etc. during Send; ditto
 // display time and date in message pane using senders timezone
 pref("mailnews.display.date_senders_timezone", false);
 // For the next 4 prefs, see <http://www.bucksch.org/1/projects/mozilla/108153>
 pref("mailnews.display.prefer_plaintext", false);  // Ignore HTML parts in multipart/alternative
 pref("mailnews.display.html_as", 0);  // How to display HTML/MIME parts. 0 = Render the sender's HTML; 1 = HTML->TXT->HTML; 2 = Show HTML source; 3 = Sanitize HTML; 4 = Show all body parts
 pref("mailnews.display.show_all_body_parts_menu", false); // Whether the View > Message body as > All body parts menu item is available
-pref("mailnews.display.html_sanitizer.allowed_tags.migrated", false); // whether legacy mailnews.display.html_sanitizer.allowed_tags pref has been migrated to values of the two prefs below
 pref("mailnews.display.html_sanitizer.drop_non_css_presentation", true); // whether to drop <font>, <center>, align='...', etc.
 pref("mailnews.display.html_sanitizer.drop_media", false); // whether to drop <img>, <video> and <audio>
 pref("mailnews.display.disallow_mime_handlers", 0);  /* Let only a few classes process incoming data. This protects from bugs (e.g. buffer overflows) and from security loopholes (e.g. allowing unchecked HTML in some obscure classes, although the user has html_as > 0).
 This option is mainly for the UI of html_as.
 0 = allow all available classes
 1 = Use hardcoded blacklist to avoid rendering (incoming) HTML
 2 = ... and inline images
 3 = ... and some other uncommon content types
diff --git a/mailnews/mime/src/mimemoz2.cpp b/mailnews/mime/src/mimemoz2.cpp
--- a/mailnews/mime/src/mimemoz2.cpp
+++ b/mailnews/mime/src/mimemoz2.cpp
@@ -2053,69 +2053,47 @@ nsresult GetMailNewsFont(MimeObject *obj
     *fontSizePercentage = originalSize ?
                           (int32_t)((float)*fontPixelSize / (float)originalSize * 100) : 0;
 
   }
 
   return NS_OK;
 }
 
-/* This function syncronously converts an HTML document (as string)
-   to plaintext (as string) using the Gecko converter.
-
-   flags: see nsIDocumentEncoder.h
-*/
+/**
+ * This function synchronously converts an HTML document (as string)
+ * to plaintext (as string) using the Gecko converter.
+ *
+ * @param flags see nsIDocumentEncoder.h
+ */
 nsresult
 HTML2Plaintext(const nsString& inString, nsString& outString,
                uint32_t flags, uint32_t wrapCol)
 {
   nsCOMPtr<nsIParserUtils> utils =
     do_GetService(NS_PARSERUTILS_CONTRACTID);
   return utils->ConvertToPlainText(inString, flags, wrapCol, outString);
 }
-// </copy>
-
-
-
-/* This function syncronously sanitizes an HTML document (string->string)
-   using the Gecko nsTreeSanitizer.
-*/
-// copied from HTML2Plaintext above
+ 
+/**
+ * This function synchronously sanitizes an HTML document (string->string)
+ * using the Gecko nsTreeSanitizer.
+ * Compare HTMLSanitizeQuote() in nsMsgCompose.cpp.
+ */
 nsresult
 HTMLSanitize(const nsString& inString, nsString& outString)
 {
   // If you want to add alternative sanitization, you can insert a conditional
   // call to another sanitizer and an early return here.
 
   uint32_t flags = nsIParserUtils::SanitizerCidEmbedsOnly |
                    nsIParserUtils::SanitizerDropForms;
 
   nsCOMPtr<nsIPrefBranch> prefs(do_GetService(NS_PREFSERVICE_CONTRACTID));
 
-  // Start pref migration. This would make more sense in a method that runs
-  // once at app startup.
-  bool migrated = false;
-  nsresult rv = prefs->GetBoolPref(
-    "mailnews.display.html_sanitizer.allowed_tags.migrated",
-    &migrated);
-  if (NS_SUCCEEDED(rv) && !migrated) {
-    prefs->SetBoolPref("mailnews.display.html_sanitizer.allowed_tags.migrated",
-                       true);
-    nsAutoCString legacy;
-    rv = prefs->GetCharPref("mailnews.display.html_sanitizer.allowed_tags",
-                            getter_Copies(legacy));
-    if (NS_SUCCEEDED(rv)) {
-      prefs->SetBoolPref("mailnews.display.html_sanitizer.drop_non_css_presentation",
-                         legacy.Find("font") < 0);
-      prefs->SetBoolPref("mailnews.display.html_sanitizer.drop_media",
-                         legacy.Find("img") < 0);
-    }
-  }
-  // End pref migration.
-
   bool dropPresentational = true;
   bool dropMedia = false;
   prefs->GetBoolPref(
     "mailnews.display.html_sanitizer.drop_non_css_presentation",
     &dropPresentational);
   prefs->GetBoolPref(
     "mailnews.display.html_sanitizer.drop_media",
     &dropMedia);
diff --git a/mailnews/mime/src/mimethsa.cpp b/mailnews/mime/src/mimethsa.cpp
--- a/mailnews/mime/src/mimethsa.cpp
+++ b/mailnews/mime/src/mimethsa.cpp
@@ -2,26 +2,45 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 /* Most of this code is copied from mimethpl; see there for source comments.
    If you find a bug here, check that class, too.
 */
 
+/* The MimeInlineTextHTMLSanitized class cleans up HTML
+
+   This removes offending HTML features that have no business in mail.
+   It is a low-level stop gap for many classes of attacks,
+   and intended for security conscious users.
+   Paranoia is a feature here, and has served very well in practice.
+
+   It has already prevented countless serious exploits.
+
+   It pushes the HTML that we get from the sender of the message
+   through a sanitizer (nsTreeSanitizer), which lets only allowed tags through.
+   With the appropriate configuration, this protects from most of the
+   security and visual-formatting problems that otherwise usually come with HTML
+   (and which partly gave HTML in email the bad reputation that it has).
+
+   However, due to the parsing and serializing (and later parsing again)
+   required, there is an inherent, significant performance hit, when doing the
+   santinizing here at the MIME / HTML source level. But users of this class
+   will most likely find it worth the cost.
+ */
+
 #include "mimethsa.h"
 #include "prmem.h"
 #include "prlog.h"
 #include "msgCore.h"
 #include "mimemoz2.h"
 #include "nsIPrefBranch.h"
 #include "nsString.h"
 
-//#define DEBUG_BenB
-
 #define MIME_SUPERCLASS mimeInlineTextHTMLClass
 MimeDefClass(MimeInlineTextHTMLSanitized, MimeInlineTextHTMLSanitizedClass,
        mimeInlineTextHTMLSanitizedClass, &MIME_SUPERCLASS);
 
 static int MimeInlineTextHTMLSanitized_parse_line (const char *, int32_t,
                                                    MimeObject *);
 static int MimeInlineTextHTMLSanitized_parse_begin (MimeObject *obj);
 static int MimeInlineTextHTMLSanitized_parse_eof (MimeObject *, bool);
@@ -38,43 +57,26 @@ MimeInlineTextHTMLSanitizedClassInitiali
   oclass->finalize    = MimeInlineTextHTMLSanitized_finalize;
 
   return 0;
 }
 
 static int
 MimeInlineTextHTMLSanitized_parse_begin (MimeObject *obj)
 {
-#ifdef DEBUG_BenB
-printf("parse_begin\n");
-#endif
-  MimeInlineTextHTMLSanitized *textHTMLSan =
-                                       (MimeInlineTextHTMLSanitized *) obj;
-  textHTMLSan->complete_buffer = new nsString();
-#ifdef DEBUG_BenB
-printf(" B1\n");
-printf(" cbp: %d\n", textHTMLSan->complete_buffer);
-#endif
+  MimeInlineTextHTMLSanitized *me = (MimeInlineTextHTMLSanitized *) obj;
+  me->complete_buffer = new nsString();
   int status = ((MimeObjectClass*)&MIME_SUPERCLASS)->parse_begin(obj);
   if (status < 0)
     return status;
-#ifdef DEBUG_BenB
-printf(" B2\n");
-#endif
 
-  // charset
-  /* honestly, I don't know how that charset stuff works in libmime.
-     The part in mimethtm doesn't make much sense to me either.
-     I'll just dump the charset we get in the mime headers into a
-     HTML meta http-equiv.
-     XXX Not sure, if that is correct, though. */
-  char *content_type =
-    (obj->headers
-     ? MimeHeaders_get(obj->headers, HEADER_CONTENT_TYPE, false, false)
-     : 0);
+  // Dump the charset we get from the mime headers into a HTML <meta http-equiv>.
+  char *content_type = obj->headers
+    ? MimeHeaders_get(obj->headers, HEADER_CONTENT_TYPE, false, false)
+    : 0;
   if (content_type)
   {
     char* charset = MimeHeaders_get_parameter(content_type,
                                               HEADER_PARM_CHARSET,
                                               NULL, NULL);
     PR_Free(content_type);
     if (charset)
     {
@@ -86,159 +88,77 @@ printf(" B2\n");
                                     charsetline.get(),
                                     charsetline.Length(),
                                     true);
       PR_Free(charset);
       if (status < 0)
         return status;
     }
   }
-#ifdef DEBUG_BenB
-printf("/parse_begin\n");
-#endif
   return 0;
 }
 
 static int
 MimeInlineTextHTMLSanitized_parse_eof (MimeObject *obj, bool abort_p)
 {
-#ifdef DEBUG_BenB
-printf("parse_eof\n");
-#endif
-
   if (obj->closed_p)
     return 0;
   int status = ((MimeObjectClass*)&MIME_SUPERCLASS)->parse_eof(obj, abort_p);
   if (status < 0)
     return status;
-  MimeInlineTextHTMLSanitized *textHTMLSan =
-                                       (MimeInlineTextHTMLSanitized *) obj;
+  MimeInlineTextHTMLSanitized *me = (MimeInlineTextHTMLSanitized *) obj;
 
-#ifdef DEBUG_BenB
-printf(" cbp: %d\n", textHTMLSan->complete_buffer);
-printf(" closed_p: %s\n", obj->closed_p?"true":"false");
-#endif
-  if (!textHTMLSan || !textHTMLSan->complete_buffer)
-  {
-#ifdef DEBUG_BenB
-printf("/parse_eof (early exit)\n");
-#endif
+  // We have to cache all lines and parse the whole document at once.
+  // There's a useful sounding function parseFromStream(), but it only allows XML
+  // mimetypes, not HTML. Methinks that's because the HTML soup parser
+  // needs the entire doc to make sense of the glibberish that people write.
+  if (!me || !me->complete_buffer)
     return 0;
-  }
-#ifdef DEBUG_BenB
-printf(" E1\n");
-printf("buffer: -%s-\n", NS_LossyConvertUTF16toASCII(*textHTMLSan->complete_buffer).get());
-#endif
 
-#ifdef DEBUG_BenB
-printf(" E2\n");
-#endif
-  nsString& cb = *(textHTMLSan->complete_buffer);
-#ifdef DEBUG_BenB
-printf(" E3\n");
-#endif
+  nsString& cb = *(me->complete_buffer);
   nsString sanitized;
-#ifdef DEBUG_BenB
-printf(" E4\n");
-#endif
+
+  // Sanitize.
   HTMLSanitize(cb, sanitized);
-#ifdef DEBUG_BenB
-printf(" E5\n");
-#endif
 
+  // Write it out.
   NS_ConvertUTF16toUTF8 resultCStr(sanitized);
-#ifdef DEBUG_BenB
-printf(" E6\n");
-#endif
-  // TODO parse each line independently
-  /* That function doesn't work correctly, if the first META tag is no
-     charset spec. (It assumes that it's on its own line.)
-     Most likely not fatally wrong, however. */
   status = ((MimeObjectClass*)&MIME_SUPERCLASS)->parse_line(
                              resultCStr.BeginWriting(),
                              resultCStr.Length(),
                              obj);
-#ifdef DEBUG_BenB
-printf(" E7\n");
-#endif
-
-#ifdef DEBUG_BenB
-printf(" E8\n");
-#endif
-
   cb.Truncate();
-
-#ifdef DEBUG_BenB
-printf("/parse_eof\n");
-#endif
-
   return status;
 }
 
 void
 MimeInlineTextHTMLSanitized_finalize (MimeObject *obj)
 {
-#ifdef DEBUG_BenB
-printf("finalize\n");
-#endif
-  MimeInlineTextHTMLSanitized *textHTMLSan =
+  MimeInlineTextHTMLSanitized *me =
                                         (MimeInlineTextHTMLSanitized *) obj;
-#ifdef DEBUG_BenB
-printf(" cbp: %d\n", textHTMLSan->complete_buffer);
-printf(" F1\n");
-#endif
 
-  if (textHTMLSan && textHTMLSan->complete_buffer)
+  if (me && me->complete_buffer)
   {
     obj->clazz->parse_eof(obj, false);
-#ifdef DEBUG_BenB
-printf(" F2\n");
-#endif
-    delete textHTMLSan->complete_buffer;
-#ifdef DEBUG_BenB
-printf(" cbp: %d\n", textHTMLSan->complete_buffer);
-printf(" F3\n");
-#endif
-    textHTMLSan->complete_buffer = NULL;
+    delete me->complete_buffer;
+    me->complete_buffer = NULL;
   }
 
-#ifdef DEBUG_BenB
-printf(" cbp: %d\n", textHTMLSan->complete_buffer);
-printf(" F4\n");
-#endif
   ((MimeObjectClass*)&MIME_SUPERCLASS)->finalize (obj);
-#ifdef DEBUG_BenB
-printf("/finalize\n");
-#endif
 }
 
 static int
 MimeInlineTextHTMLSanitized_parse_line (const char *line, int32_t length,
                                           MimeObject *obj)
 {
-#ifdef DEBUG_BenB
-printf("p");
-#endif
-  MimeInlineTextHTMLSanitized *textHTMLSan =
-                                       (MimeInlineTextHTMLSanitized *) obj;
-#ifdef DEBUG_BenB
-printf("%d", textHTMLSan->complete_buffer);
-#endif
+  MimeInlineTextHTMLSanitized *me = (MimeInlineTextHTMLSanitized *) obj;
 
-  if (!textHTMLSan || !(textHTMLSan->complete_buffer))
-  {
-#ifdef DEBUG
-printf("Can't output: %s\n", line);
-#endif
+  if (!me || !(me->complete_buffer))
     return -1;
-  }
 
   nsCString linestr(line, length);
   NS_ConvertUTF8toUTF16 line_ucs2(linestr.get());
   if (length && line_ucs2.IsEmpty())
     CopyASCIItoUTF16(linestr, line_ucs2);
-  (textHTMLSan->complete_buffer)->Append(line_ucs2);
+  (me->complete_buffer)->Append(line_ucs2);
 
-#ifdef DEBUG_BenB
-printf("l ");
-#endif
   return 0;
 }
diff --git a/mailnews/mime/src/mimethsa.h b/mailnews/mime/src/mimethsa.h
--- a/mailnews/mime/src/mimethsa.h
+++ b/mailnews/mime/src/mimethsa.h
@@ -1,28 +1,13 @@
 /* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
-/* The MimeInlineTextHTMLSanitized class cleans up HTML
-
-   This class pushes the HTML that we get from the
-   sender of the message through a sanitizer (nsTreeSanitizer),
-   which lets only allowed tags through. With the appropriate configuration,
-   this protects from most of the security and visual-formatting problems
-   that otherwise usually come with HTML (and which partly gave HTML in email
-   the bad reputation that it has).
-
-   However, due to the parsing and serializing (and later parsing again)
-   required, there is an inherent, significant performance hit, when doing the
-   santinizing here at the MIME / HTML source level. But users of this class
-   will most likely find it worth the cost.
- */
-
 #ifndef _MIMETHSA_H_
 #define _MIMETHSA_H_
 
 #include "mimethtm.h"
 #include "nsString.h"
 
 typedef struct MimeInlineTextHTMLSanitizedClass MimeInlineTextHTMLSanitizedClass;
 typedef struct MimeInlineTextHTMLSanitized      MimeInlineTextHTMLSanitized;
