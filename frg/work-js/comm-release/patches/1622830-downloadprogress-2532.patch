# HG changeset patch
# User Frank-Rainer Grahl <frgrahl@gmx.net>
# Date 1585081799 -3600
# Parent  f65e79ad61940f7059e15168776bdf4704900bcf
Bug 1622830 - Calculate download values in progress window the same as in the DM. r=IanN a=IanN

diff --git a/suite/components/downloads/DownloadsCommon.jsm b/suite/components/downloads/DownloadsCommon.jsm
--- a/suite/components/downloads/DownloadsCommon.jsm
+++ b/suite/components/downloads/DownloadsCommon.jsm
@@ -271,16 +271,81 @@ var DownloadsCommon = {
         verboseState = s.stateUnknown;
         break;
     }
 
     return verboseState;
   },
 
   /**
+   * Returns the transfer progress text for the provided Download object.
+   */
+  getTransferredBytes(download) {
+    let currentBytes;
+    let totalBytes;
+    // Download in progress.
+    // Download paused / canceled and has partial data.
+    if (!download.stopped ||
+        (download.canceled && download.hasPartialData)) {
+      currentBytes = download.currentBytes,
+      totalBytes = download.hasProgress ? download.totalBytes : -1;
+    // Download done but file missing.
+    } else if (download.succeeded && !download.exists) {
+      currentBytes = download.totalBytes ? download.totalBytes : -1;
+      totalBytes = -1;
+    // For completed downloads, show the file size
+    } else if (download.succeeded && download.target.size !== undefined) {
+       currentBytes = download.target.size;
+       totalBytes = -1;
+    // Some local files saves e.g. from attachments also have no size.
+    // They only have a target in downloads.json but no target.path.
+    // FIX ME later.
+    } else {
+      currentBytes = -1;
+      totalBytes = -1;
+    }
+
+    // We do not want to show 0 of xxx bytes.
+    if (currentBytes == 0) {
+      currentBytes = -1;
+    }
+
+    if (totalBytes == 0) {
+      totalBytes = -1;
+    }
+
+    // We tried everything.
+    if (currentBytes == -1 && totalBytes == -1) {
+      return "";
+    }
+
+    return DownloadUtils.getTransferTotal(currentBytes, totalBytes);
+  },
+
+  /**
+   * Returns the time remaining text for the provided Download object.
+   * For calculation a variable is stored in it.
+   */
+  getTimeRemaining(download) {
+    // If you do changes here please check progressDialog.js.
+    if (!download.stopped) {
+      let lastSec = (download.lastSec == null) ? Infinity : download.lastSec;
+      // Calculate the time remaining if we have valid values
+      let seconds = (download.speed > 0) && (download.totalBytes > 0)
+                    ? (download.totalBytes - download.currentBytes) / download.speed
+                    : -1;
+      let [timeLeft, newLast] = DownloadUtils.getTimeLeft(seconds, lastSec);
+      // Store it back for next calculation.
+      download.lastSec = newLast;
+      return timeLeft;
+    }
+    return "";
+  },
+
+  /**
    * Opens a downloaded file.
    *
    * @param aFile
    *        the downloaded file to be opened.
    * @param aMimeInfo
    *        the mime type info object.  May be null.
    * @param aOwnerWindow
    *        the window with which this action is associated.
diff --git a/suite/components/downloads/content/progressDialog.js b/suite/components/downloads/content/progressDialog.js
--- a/suite/components/downloads/content/progressDialog.js
+++ b/suite/components/downloads/content/progressDialog.js
@@ -7,44 +7,39 @@ ChromeUtils.import("resource://gre/modul
 
 XPCOMUtils.defineLazyModuleGetters(this, {
   DownloadUtils: "resource://gre/modules/DownloadUtils.jsm",
   DownloadsCommon: "resource:///modules/DownloadsCommon.jsm",
 });
 
 var gDownload;
 var gDownloadBundle;
-var gTkDlBundle;
 
 var gDlList;
 var gDlStatus;
 var gDlListener;
 var gDlSize;
-var gTimeElapsed;
+var gTimeLeft;
 var gProgressMeter;
 var gProgressText;
 var gCloseWhenDone;
 
-var gLastSec = Infinity;
-var gDlActive = false;
-
 function progressStartup() {
   gDownload = window.arguments[0].wrappedJSObject;
   Downloads.getList(gDownload.source.isPrivate ? Downloads.PRIVATE : Downloads.PUBLIC).then(progressAsyncStartup);
 }
 
 function progressAsyncStartup(aList) {
   gDlList = aList;
 
   // cache elements to save .getElementById() calls
   gDownloadBundle = document.getElementById("dmBundle");
-  gTkDlBundle = document.getElementById("tkdlBundle");
   gDlStatus = document.getElementById("dlStatus");
   gDlSize = document.getElementById("dlSize");
-  gTimeElapsed = document.getElementById("timeElapsed");
+  gTimeLeft = document.getElementById("timeLeft");
   gProgressMeter = document.getElementById("progressMeter");
   gProgressText = document.getElementById("progressText");
   gCloseWhenDone = document.getElementById("closeWhenDone");
 
   // Insert as first controller on the whole window
   window.controllers.insertControllerAt(0, ProgressDlgController);
 
   if (gDownload.isPrivate)
@@ -101,85 +96,39 @@ function updateDownload() {
     gProgressMeter.mode = "undetermined";
   }
   if (gDownload.stopped) {
     gProgressMeter.style.opacity = 0.5;
   } else {
     gProgressMeter.style.opacity = 1;
   }
   // Update window title
-  let statusString = DownloadsCommon.stateOfDownloadText(gDownloadBundle);
+  let statusString = DownloadsCommon.stateOfDownloadText(gDownload);
 
   if (gDownload.hasProgress) {
     document.title = gDownloadBundle.getFormattedString("progressTitlePercent",
                                                         [gDownload.progress,
                                                          gDownload.displayName,
                                                          statusString]);
   }
   else {
     document.title = gDownloadBundle.getFormattedString("progressTitle",
                                                         [gDownload.displayName,
                                                          statusString]);
   }
 
-  // download size
-  var transfer = DownloadUtils.getTransferTotal(gDownload.currentBytes,
-                                                gDownload.totalBytes);
-  if (!gDownload.stopped) {
-    var [rate, unit] = DownloadUtils.convertByteUnits(gDownload.speed);
-    var dlSpeed = gDownloadBundle.getFormattedString("speedFormat", [rate, unit]);
-    gDlSize.value = gDownloadBundle.getFormattedString("sizeSpeed",
-                                                       [transfer, dlSpeed]);
-  }
-  else
-    gDlSize.value = transfer;
+  // download size / transferred bytes
+  gDlSize.value = DownloadsCommon.getTransferredBytes(gDownload);
+
+  // time remaining
+  gTimeLeft.value = DownloadsCommon.getTimeRemaining(gDownload);
 
   // download status
-  if (!gDownload.stopped) {
-    // Calculate the time remaining if we have valid values
-    var seconds = (gDownload.speed > 0) && (gDownload.totalBytes > 0)
-                  ? (gDownload.totalBytes - gDownload.currentBytes) / gDownload.speed
-                  : -1;
-    var [timeLeft, newLast] = DownloadUtils.getTimeLeft(seconds, gLastSec);
-    gLastSec = newLast;
-  }
+  gDlStatus.value = statusString;
 
-  let state = DownloadsCommon.stateOfDownload(gDownload);
-  switch (state) {
-    case DownloadsCommon.DOWNLOAD_BLOCKED_PARENTAL: // Parental Controls
-      gDlStatus.value = gTkDlBundle.getString("stateBlocked");
-      break;
-    case DownloadsCommon.DOWNLOAD_BLOCKED_POLICY:   // Security Zone Policy
-      gDlStatus.value = gTkDlBundle.getString("stateBlockedPolicy");
-      break;
-    case DownloadsCommon.DOWNLOAD_DIRTY:            // possible virus/spyware
-      gDlStatus.value = gTkDlBundle.getString("stateDirty");
-      break;
-    default:
-      if (gDlActive)
-        gDlStatus.value = gDownloadBundle.getFormattedString("statusActive",
-                                                             [statusString, timeLeft]);
-      else
-        gDlStatus.value = statusString;
-      break;
-  }
-
-  // time elapsed
-  if (gDownload.startTime && gDownload.endTime && (gDownload.endTime > gDownload.startTime)) {
-    var seconds = (gDownload.endTime - gDownload.startTime) / 1000;
-    var [time1, unit1, time2, unit2] =
-      DownloadUtils.convertTimeUnits(seconds);
-    if (seconds < 3600 || time2 == 0)
-      gTimeElapsed.value = gDownloadBundle.getFormattedString("timeElapsedSingle", [time1, unit1]);
-    else
-      gTimeElapsed.value = gDownloadBundle.getFormattedString("timeElapsedDouble", [time1, unit1, time2, unit2]);
-  }
-  else {
-    gTimeElapsed.value = "";
-  }
 }
 
 function updateButtons() {
   document.getElementById("pauseButton").hidden = !ProgressDlgController.isCommandEnabled("cmd_pause");
   document.getElementById("resumeButton").hidden = !ProgressDlgController.isCommandEnabled("cmd_resume");
   document.getElementById("retryButton").hidden = !ProgressDlgController.isCommandEnabled("cmd_retry");
   document.getElementById("cancelButton").hidden = !ProgressDlgController.isCommandEnabled("cmd_cancel");
 }
diff --git a/suite/components/downloads/content/progressDialog.xul b/suite/components/downloads/content/progressDialog.xul
--- a/suite/components/downloads/content/progressDialog.xul
+++ b/suite/components/downloads/content/progressDialog.xul
@@ -21,18 +21,16 @@
   <script type="application/javascript"
           src="chrome://communicator/content/downloads/downloadmanager.js"/>
   <script type="application/javascript"
           src="chrome://communicator/content/downloads/progressDialog.js"/>
 
   <stringbundleset id="stringbundleset">
     <stringbundle id="dmBundle"
                   src="chrome://communicator/locale/downloads/downloadmanager.properties"/>
-    <stringbundle id="tkdlBundle"
-                  src="chrome://global/locale/mozapps/downloads/downloads.properties"/>
   </stringbundleset>
 
   <commandset id="dlProgressCommands">
     <commandset id="commandUpdate_DlProgress"
                 commandupdater="true"
                 events="focus,dlstate-change"
                 oncommandupdate="ProgressDlgController.onCommandUpdate();"/>
 
@@ -85,17 +83,17 @@
                     command="cmd_openReferrer"/>
           <menuitem id="dlContext-copyLocation"
                     label="&cmd.copyDownloadLink.label;"
                     accesskey="&cmd.copyDownloadLink.accesskey;"
                     command="cmd_copyLocation"/>
         </menupopup>
       </button>
       <label id="dlSize" value=""/>
-      <label id="timeElapsed" value=""/>
+      <label id="timeLeft" value=""/>
       <label id="dlStatus" value=""/>
     </vbox>
     <button id="pauseButton" class="mini-button"
             command="cmd_pause" tooltiptext="&cmd.pause.tooltip;"/>
     <button id="resumeButton" class="mini-button"
             command="cmd_resume" tooltiptext="&cmd.resume.tooltip;"/>
     <button id="retryButton" class="mini-button"
             command="cmd_retry" tooltiptext="&cmd.retry.tooltip;"/>
diff --git a/suite/components/downloads/content/treeView.js b/suite/components/downloads/content/treeView.js
--- a/suite/components/downloads/content/treeView.js
+++ b/suite/components/downloads/content/treeView.js
@@ -95,85 +95,37 @@ DownloadTreeView.prototype = {
 
   getCellValue: function(aRow, aColumn) {
     if (aColumn.id == "Progress")
       return this._dlList[aRow].progress;
     return "";
   },
 
   getCellText: function(aRow, aColumn) {
-    var dl = this._dlList[aRow];
+    let dl = this._dlList[aRow];
     switch (aColumn.id) {
       case "Name":
         return dl.displayName;
       case "Status":
         return DownloadsCommon.stateOfDownloadText(dl);
       case "Progress":
         if (dl.isActive)
           return dl.progress;
         return DownloadsCommon.stateOfDownloadText(dl);
       case "ProgressPercent":
         return dl.succeeded ? 100 : dl.progress;
       case "TimeRemaining":
-        if (!dl.stopped) {
-          var lastSec = (dl.lastSec == null) ? Infinity : dl.lastSec;
-          // Calculate the time remaining if we have valid values
-          var seconds = (dl.speed > 0) && (dl.totalBytes > 0)
-                        ? (dl.totalBytes - dl.currentBytes) / dl.speed
-                        : -1;
-          var [timeLeft, newLast] = DownloadUtils.getTimeLeft(seconds, lastSec);
-          this._dlList[aRow].lastSec = newLast;
-          return timeLeft;
-        }
-        return "";
+        return DownloadsCommon.getTimeRemaining(dl);
       case "Transferred":
-        let currentBytes;
-        let totalBytes;
-        // Download in progress.
-        // Download paused / canceled and has partial data.
-        if (!dl.stopped ||
-            (dl.canceled && dl.hasPartialData)) {
-          currentBytes = dl.currentBytes,
-          totalBytes = dl.hasProgress ? dl.totalBytes : -1;
-        // Download done but file missing.
-        } else if (dl.succeeded && !dl.exists) {
-          currentBytes = dl.totalBytes ? dl.totalBytes : -1;
-          totalBytes = -1;
-        // For completed downloads, show the file size
-        } else if (dl.succeeded && dl.target.size !== undefined) {
-           currentBytes = dl.target.size;
-           totalBytes = -1;
-        // Some local files saves e.g. from attachments also have no size.
-        // They only have a target in downloads.json but no target.path.
-        // FIX ME later.
-        } else {
-          currentBytes = -1;
-          totalBytes = -1;
-        }
-
-        // We do not want to show 0 of xxx bytes.
-        if (currentBytes == 0) {
-          currentBytes = -1;
-        }
-
-        if (totalBytes == 0) {
-          totalBytes = -1;
-        }
-
-        // We tried everything.
-        if (currentBytes == -1 && totalBytes == -1) {
-          return "";
-        }
-
-        return DownloadUtils.getTransferTotal(currentBytes, totalBytes);
+        return DownloadsCommon.getTransferredBytes(dl);
       case "TransferRate":
         let state = DownloadsCommon.stateOfDownload(dl);
         switch (state) {
           case DownloadsCommon.DOWNLOAD_DOWNLOADING:
-            var [rate, unit] = DownloadUtils.convertByteUnits(dl.speed);
+            let [rate, unit] = DownloadUtils.convertByteUnits(dl.speed);
             return this._dlbundle.getFormattedString("speedFormat", [rate, unit]);
           case DownloadsCommon.DOWNLOAD_PAUSED:
             return this._dlbundle.getString("statePaused");
           case DownloadsCommon.DOWNLOAD_NOTSTARTED:
             return this._dlbundle.getString("stateNotStarted");
         }
         return "";
       case "TimeElapsed":
diff --git a/suite/locales/en-US/chrome/common/downloads/downloadmanager.properties b/suite/locales/en-US/chrome/common/downloads/downloadmanager.properties
--- a/suite/locales/en-US/chrome/common/downloads/downloadmanager.properties
+++ b/suite/locales/en-US/chrome/common/downloads/downloadmanager.properties
@@ -127,24 +127,16 @@ speedFormat=%1$S %2$S/sec
 # LOCALIZATION NOTE (timeSingle): %1$S time number; %2$S time unit
 # example: 1 minute; 11 hours
 timeSingle=%1$S %2$S
 # LOCALIZATION NOTE (timeDouble):
 # %1$S time number; %2$S time unit; %3$S time sub number; %4$S time sub unit
 # example: 11 hours, 2 minutes; 1 day, 22 hours
 timeDouble=%1$S %2$S, %3$S %4$S
 
-# LOCALIZATION NOTE (timeElapsedSingle): %1$S time number; %2$S time unit
-# example: 1 minute elapsed; 11 hours elapsed
-timeElapsedSingle=%1$S %2$S elapsed
-# LOCALIZATION NOTE (timeElapsedDouble):
-# %1$S time number; %2$S time unit; %3$S time sub number; %4$S time sub unit
-# example: 11 hours, 2 minutes elapsed; 1 day, 22 hours elapsed
-timeElapsedDouble=%1$S %2$S, %3$S %4$S elapsed
-
 # LOCALIZATION NOTE (sizeSpeed):
 # %1$S is transfer progress; %2$S download speed
 # example: 1.1 of 11.1 GB (2.2 MB/sec)
 sizeSpeed=%1$S (%2$S)
 
 # LOCALIZATION NOTE (statusActive): — is the "em dash" (long dash)
 # %1$S download status; %2$S time remaining
 # example: Paused — 11 hours, 2 minutes remaining
