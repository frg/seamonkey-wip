# HG changeset patch
# User Philipp Kewisch <mozilla@kewis.ch>
# Date 1519212332 -3600
#      Mi Feb 21 12:25:32 2018 +0100
# Node ID 58d96bc8c66c4ac711cfe9198eff27e044fe30c6
# Parent  3a9a16b2c941c31883abf294f18cd7472b8fab52
Bug 1439868 - Move email/scheduling related functions into calEmailUtils.jsm and calItipUtils.jsm - unit tests. r=MakeMyDay

MozReview-Commit-ID: HHlW5te70Ki

diff --git a/calendar/test/unit/test_calutils.js b/calendar/test/unit/test_calutils.js
deleted file mode 100644
--- a/calendar/test/unit/test_calutils.js
+++ /dev/null
@@ -1,351 +0,0 @@
-/* This Source Code Form is subject to the terms of the Mozilla Public
- * License, v. 2.0. If a copy of the MPL was not distributed with this
- * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
-
-ChromeUtils.import("resource://calendar/modules/calUtils.jsm");
-
-// tests for calUtils.jsm
-
-function run_test() {
-    getAttendeeEmail_test();
-    getAttendeesBySender_test();
-    getRecipientList_test();
-    prependMailTo_test();
-    removeMailTo_test();
-    resolveDelegation_test();
-    validateRecipientList_test();
-}
-
-function getAttendeeEmail_test() {
-    let data = [{
-        input: { id: "mailto:first.last@example.net", cname: "Last, First", email: null, useCn: true },
-        expected: "\"Last, First\" <first.last@example.net>"
-    }, {
-        input: { id: "mailto:first.last@example.net", cname: "Last; First", email: null, useCn: true },
-        expected: "\"Last; First\" <first.last@example.net>"
-    }, {
-        input: { id: "mailto:first.last@example.net", cname: "First Last", email: null, useCn: true },
-        expected: "First Last <first.last@example.net>"
-    }, {
-        input: { id: "mailto:first.last@example.net", cname: "Last, First", email: null, useCn: false },
-        expected: "first.last@example.net"
-    }, {
-        input: { id: "mailto:first.last@example.net", cname: null, email: null, useCn: true },
-        expected: "first.last@example.net"
-    }, {
-        input: { id: "urn:uuid:first.last.example.net", cname: null, email: "first.last@example.net", useCn: false },
-        expected: "first.last@example.net"
-    }, {
-        input: { id: "urn:uuid:first.last.example.net", cname: null, email: "first.last@example.net", useCn: true },
-        expected: "first.last@example.net"
-    }, {
-        input: { id: "urn:uuid:first.last.example.net", cname: "First Last", email: "first.last@example.net", useCn: true },
-        expected: "First Last <first.last@example.net>"
-    }, {
-        input: { id: "urn:uuid:first.last.example.net", cname: null, email: null, useCn: false },
-        expected: ""
-    }];
-    let i = 0;
-    for (let test of data) {
-        i++;
-        let attendee = cal.createAttendee();
-        attendee.id = test.input.id;
-        if (test.input.cname) {
-            attendee.commonName = test.input.cname;
-        }
-        if (test.input.email) {
-            attendee.setProperty("EMAIL", test.input.email);
-        }
-        equal(cal.email.getAttendeeEmail(attendee, test.input.useCn), test.expected, "(test #" + i + ")");
-    }
-}
-
-function getAttendeesBySender_test() {
-    let data = [{
-        input: {
-            attendees: [{ id: "mailto:user1@example.net", sentBy: null },
-                        { id: "mailto:user2@example.net", sentBy: null }],
-            sender: "user1@example.net"
-        },
-        expected: ["mailto:user1@example.net"]
-    }, {
-        input: {
-            attendees: [{ id: "mailto:user1@example.net", sentBy: null },
-                        { id: "mailto:user2@example.net", sentBy: null }],
-            sender: "user3@example.net"
-        },
-        expected: []
-    }, {
-        input: {
-            attendees: [{ id: "mailto:user1@example.net", sentBy: "mailto:user3@example.net" },
-                        { id: "mailto:user2@example.net", sentBy: null }],
-            sender: "user3@example.net"
-        },
-        expected: ["mailto:user1@example.net"]
-    }, {
-        input: {
-            attendees: [{ id: "mailto:user1@example.net", sentBy: null },
-                        { id: "mailto:user2@example.net", sentBy: "mailto:user1@example.net" }],
-            sender: "user1@example.net"
-        },
-        expected: ["mailto:user1@example.net", "mailto:user2@example.net"]
-    }, {
-        input: { attendees: [], sender: "user1@example.net" },
-        expected: []
-    }, {
-        input: {
-            attendees: [{ id: "mailto:user1@example.net", sentBy: null },
-                        { id: "mailto:user2@example.net", sentBy: null }],
-            sender: ""
-        },
-        expected: []
-    }, {
-        input: {
-            attendees: [{ id: "mailto:user1@example.net", sentBy: null },
-                        { id: "mailto:user2@example.net", sentBy: null }],
-            sender: null
-        },
-        expected: []
-    }];
-
-    for (let i = 1; i <= data.length; i++) {
-        let test = data[i - 1];
-        let attendees = [];
-        for (let att of test.input.attendees) {
-            let attendee = cal.createAttendee();
-            attendee.id = att.id;
-            if (att.sentBy) {
-                attendee.setProperty("SENT-BY", att.sentBy);
-            }
-            attendees.push(attendee);
-        }
-        let detected = [];
-        cal.itip.getAttendeesBySender(attendees, test.input.sender).forEach(att => {
-            detected.push(att.id);
-        });
-        ok(detected.every(aId => test.expected.includes(aId)), "(test #" + i + " ok1)");
-        ok(test.expected.every(aId => detected.includes(aId)), "(test #" + i + " ok2)");
-    }
-}
-
-function getRecipientList_test() {
-    let data = [{
-        input: [{ id: "mailto:first@example.net", cname: null },
-                { id: "mailto:second@example.net", cname: null },
-                { id: "mailto:third@example.net", cname: null }],
-        expected: "first@example.net, second@example.net, third@example.net"
-    }, {
-        input: [{ id: "mailto:first@example.net", cname: "first example" },
-                { id: "mailto:second@example.net", cname: "second example" },
-                { id: "mailto:third@example.net", cname: "third example" }],
-        expected: "first example <first@example.net>, second example <second@example.net>, " +
-                  "third example <third@example.net>"
-    }, {
-        input: [{ id: "mailto:first@example.net", cname: "example, first" },
-                { id: "mailto:second@example.net", cname: "example, second" },
-                { id: "mailto:third@example.net", cname: "example, third" }],
-        expected: "\"example, first\" <first@example.net>, \"example, second\" <second@example.net>, " +
-                  "\"example, third\" <third@example.net>"
-    }, {
-        input: [{ id: "mailto:first@example.net", cname: null },
-                { id: "urn:uuid:second.example.net", cname: null },
-                { id: "mailto:third@example.net", cname: null }],
-        expected: "first@example.net, third@example.net"
-    }, {
-        input: [{ id: "mailto:first@example.net", cname: "first" },
-                { id: "urn:uuid:second.example.net", cname: "second" },
-                { id: "mailto:third@example.net", cname: "third" }],
-        expected: "first <first@example.net>, third <third@example.net>"
-    }];
-
-    let i = 0;
-    for (let test of data) {
-        i++;
-        let attendees = [];
-        for (let att of test.input) {
-            let attendee = cal.createAttendee();
-            attendee.id = att.id;
-            if (att.cname) {
-                attendee.commonName = att.cname;
-            }
-            attendees.push(attendee);
-        }
-        equal(cal.email.createRecipientList(attendees), test.expected, "(test #" + i + ")");
-    }
-}
-
-function prependMailTo_test() {
-    let data = [{ input: "mailto:first.last@example.net", expected: "mailto:first.last@example.net" },
-                { input: "MAILTO:first.last@example.net", expected: "mailto:first.last@example.net" },
-                { input: "first.last@example.net", expected: "mailto:first.last@example.net" },
-                { input: "first.last.example.net", expected: "first.last.example.net" }];
-    let i = 0;
-    for (let test of data) {
-        i++;
-        equal(cal.email.prependMailTo(test.input), test.expected, "(test #" + i + ")");
-    }
-}
-
-function removeMailTo_test() {
-    let data = [{ input: "mailto:first.last@example.net", expected: "first.last@example.net" },
-                { input: "MAILTO:first.last@example.net", expected: "first.last@example.net" },
-                { input: "first.last@example.net", expected: "first.last@example.net" },
-                { input: "first.last.example.net", expected: "first.last.example.net" }];
-    let i = 0;
-    for (let test of data) {
-        i++;
-        equal(cal.email.removeMailTo(test.input), test.expected, "(test #" + i + ")");
-    }
-}
-
-function resolveDelegation_test() {
-    let data = [{
-        input: {
-            attendee:
-                'ATTENDEE;DELEGATED-FROM="mailto:attendee2@example.net";CN="Attendee 1":mailto:at' +
-                "tendee1@example.net",
-            attendees: [
-                'ATTENDEE;DELEGATED-FROM="mailto:attendee2@example.net";CN="Attendee 1":mailto:at' +
-                "tendee1@example.net",
-                'ATTENDEE;DELEGATED-TO="mailto:attendee1@example.net";CN="Attendee 2":mailto:atte' +
-                "ndee2@example.net"
-            ]
-        },
-        expected: {
-            delegatees: "",
-            delegators: "Attendee 2 <attendee2@example.net>"
-        }
-    }, {
-        input: {
-            attendee:
-                'ATTENDEE;DELEGATED-FROM="mailto:attendee2@example.net":mailto:attendee1@example.net',
-            attendees: [
-                'ATTENDEE;DELEGATED-FROM="mailto:attendee2@example.net":mailto:attendee1@example.net',
-                'ATTENDEE;DELEGATED-TO="mailto:attendee1@example.net":mailto:attendee2@example.net'
-            ]
-        },
-        expected: {
-            delegatees: "",
-            delegators: "attendee2@example.net"
-        }
-    }, {
-        input: {
-            attendee:
-                'ATTENDEE;DELEGATED-TO="mailto:attendee2@example.net";CN="Attendee 1":mailto:atte' +
-                "ndee1@example.net",
-            attendees: [
-                'ATTENDEE;DELEGATED-TO="mailto:attendee2@example.net";CN="Attendee 1":mailto:atte' +
-                "ndee1@example.net",
-                'ATTENDEE;DELEGATED-FROM="mailto:attendee1@example.net";CN="Attendee 2":mailto:at' +
-                "tendee2@example.net"
-            ]
-        },
-        expected: {
-            delegatees: "Attendee 2 <attendee2@example.net>",
-            delegators: ""
-        }
-    }, {
-        input: {
-            attendee:
-                'ATTENDEE;DELEGATED-TO="mailto:attendee2@example.net":mailto:attendee1@example.net',
-            attendees: [
-                'ATTENDEE;DELEGATED-TO="mailto:attendee2@example.net":mailto:attendee1@example.net',
-                'ATTENDEE;DELEGATED-FROM="mailto:attendee1@example.net":mailto:attendee2@example.net'
-            ]
-        },
-        expected: {
-            delegatees: "attendee2@example.net",
-            delegators: ""
-        }
-    }, {
-        input: {
-            attendee:
-                "ATTENDEE:mailto:attendee1@example.net",
-            attendees: [
-                "ATTENDEE:mailto:attendee1@example.net",
-                "ATTENDEE:mailto:attendee2@example.net"
-            ]
-        },
-        expected: {
-            delegatees: "",
-            delegators: ""
-        }
-    }, {
-        input: {
-            attendee:
-                'ATTENDEE;DELEGATED-FROM="mailto:attendee2@example.net";DELEGATED-TO="mailto:atte' +
-                'ndee3@example.net":mailto:attendee1@example.net',
-            attendees: [
-                'ATTENDEE;DELEGATED-FROM="mailto:attendee2@example.net";DELEGATED-TO="mailto:atte' +
-                'ndee3@example.net":mailto:attendee1@example.net',
-                'ATTENDEE;DELEGATED-TO="mailto:attendee1@example.net":mailto:attendee2@example.net',
-                'ATTENDEE;DELEGATED-FROM="mailto:attendee1@example.net":mailto:attendee3@example.net'
-            ]
-        },
-        expected: {
-            delegatees: "attendee3@example.net",
-            delegators: "attendee2@example.net"
-        }
-    }];
-    let i = 0;
-    for (let test of data) {
-        i++;
-        let attendees = [];
-        for (let att of test.input.attendees) {
-            let attendee = cal.createAttendee();
-            attendee.icalString = att;
-            attendees.push(attendee);
-        }
-        let attendee = cal.createAttendee();
-        attendee.icalString = test.input.attendee;
-        let result = cal.itip.resolveDelegation(attendee, attendees);
-        equal(result.delegatees, test.expected.delegatees, "(test #" + i + " - delegatees)");
-        equal(result.delegators, test.expected.delegators, "(test #" + i + " - delegators)");
-    }
-}
-
-function validateRecipientList_test() {
-    let data = [{
-        input: "first.last@example.net",
-        expected: "first.last@example.net"
-    }, {
-        input: "first last <first.last@example.net>",
-        expected: "first last <first.last@example.net>"
-    }, {
-        input: "\"last, first\" <first.last@example.net>",
-        expected: "\"last, first\" <first.last@example.net>"
-    }, {
-        input: "last, first <first.last@example.net>",
-        expected: "\"last, first\" <first.last@example.net>"
-    }, {
-        input: "\"last; first\" <first.last@example.net>",
-        expected: "\"last; first\" <first.last@example.net>"
-    }, {
-        input: "first1.last1@example.net,first2.last2@example.net,first3.last2@example.net",
-        expected: "first1.last1@example.net, first2.last2@example.net, first3.last2@example.net"
-    }, {
-        input: "first1.last1@example.net, first2.last2@example.net, first3.last2@example.net",
-        expected: "first1.last1@example.net, first2.last2@example.net, first3.last2@example.net"
-    }, {
-        input: "first1.last1@example.net, first2 last2 <first2.last2@example.net>, \"last3, first" +
-               "3\" <first3.last2@example.net>",
-        expected: "first1.last1@example.net, first2 last2 <first2.last2@example.net>, \"last3, fi" +
-               "rst3\" <first3.last2@example.net>"
-    }, {
-        input: "first1.last1@example.net, last2; first2 <first2.last2@example.net>, \"last3; first" +
-               "3\" <first3.last2@example.net>",
-        expected: "first1.last1@example.net, \"last2; first2\" <first2.last2@example.net>, \"last" +
-               "3; first3\" <first3.last2@example.net>"
-    }, {
-        input: "first1 last2 <first1.last1@example.net>, last2, first2 <first2.last2@example.net>" +
-               ", \"last3, first3\" <first3.last2@example.net>",
-        expected: "first1 last2 <first1.last1@example.net>, \"last2, first2\" <first2.last2@examp" +
-                  "le.net>, \"last3, first3\" <first3.last2@example.net>"
-    }];
-    let i = 0;
-    for (let test of data) {
-        i++;
-        equal(cal.email.validateRecipientList(test.input), test.expected,
-              "(test #" + i + ")");
-    }
-}
diff --git a/calendar/test/unit/test_email_utils.js b/calendar/test/unit/test_email_utils.js
new file mode 100644
--- /dev/null
+++ b/calendar/test/unit/test_email_utils.js
@@ -0,0 +1,177 @@
+/* This Source Code Form is subject to the terms of the Mozilla Public
+ * License, v. 2.0. If a copy of the MPL was not distributed with this
+ * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
+
+function run_test() {
+    test_prependMailTo();
+    test_removeMailTo();
+    test_getAttendeeEmail();
+    test_createRecipientList();
+    test_validateRecipientList();
+    test_attendeeMatchesAddresses();
+}
+
+function test_prependMailTo() {
+    let data = [{ input: "mailto:first.last@example.net", expected: "mailto:first.last@example.net" },
+                { input: "MAILTO:first.last@example.net", expected: "mailto:first.last@example.net" },
+                { input: "first.last@example.net", expected: "mailto:first.last@example.net" },
+                { input: "first.last.example.net", expected: "first.last.example.net" }];
+    for (let [i, test] of Object.entries(data)) {
+        equal(cal.email.prependMailTo(test.input), test.expected, "(test #" + i + ")");
+    }
+}
+
+function test_removeMailTo() {
+    let data = [{ input: "mailto:first.last@example.net", expected: "first.last@example.net" },
+                { input: "MAILTO:first.last@example.net", expected: "first.last@example.net" },
+                { input: "first.last@example.net", expected: "first.last@example.net" },
+                { input: "first.last.example.net", expected: "first.last.example.net" }];
+    for (let [i, test] of Object.entries(data)) {
+        equal(cal.email.removeMailTo(test.input), test.expected, "(test #" + i + ")");
+    }
+}
+
+function test_getAttendeeEmail() {
+    let data = [{
+        input: { id: "mailto:first.last@example.net", cname: "Last, First", email: null, useCn: true },
+        expected: "\"Last, First\" <first.last@example.net>"
+    }, {
+        input: { id: "mailto:first.last@example.net", cname: "Last; First", email: null, useCn: true },
+        expected: "\"Last; First\" <first.last@example.net>"
+    }, {
+        input: { id: "mailto:first.last@example.net", cname: "First Last", email: null, useCn: true },
+        expected: "First Last <first.last@example.net>"
+    }, {
+        input: { id: "mailto:first.last@example.net", cname: "Last, First", email: null, useCn: false },
+        expected: "first.last@example.net"
+    }, {
+        input: { id: "mailto:first.last@example.net", cname: null, email: null, useCn: true },
+        expected: "first.last@example.net"
+    }, {
+        input: { id: "urn:uuid:first.last.example.net", cname: null, email: "first.last@example.net", useCn: false },
+        expected: "first.last@example.net"
+    }, {
+        input: { id: "urn:uuid:first.last.example.net", cname: null, email: "first.last@example.net", useCn: true },
+        expected: "first.last@example.net"
+    }, {
+        input: { id: "urn:uuid:first.last.example.net", cname: "First Last", email: "first.last@example.net", useCn: true },
+        expected: "First Last <first.last@example.net>"
+    }, {
+        input: { id: "urn:uuid:first.last.example.net", cname: null, email: null, useCn: false },
+        expected: ""
+    }];
+    for (let [i, test] of Object.entries(data)) {
+        let attendee = cal.createAttendee();
+        attendee.id = test.input.id;
+        if (test.input.cname) {
+            attendee.commonName = test.input.cname;
+        }
+        if (test.input.email) {
+            attendee.setProperty("EMAIL", test.input.email);
+        }
+        equal(cal.email.getAttendeeEmail(attendee, test.input.useCn), test.expected, "(test #" + i + ")");
+    }
+}
+
+function test_createRecipientList() {
+    let data = [{
+        input: [{ id: "mailto:first@example.net", cname: null },
+                { id: "mailto:second@example.net", cname: null },
+                { id: "mailto:third@example.net", cname: null }],
+        expected: "first@example.net, second@example.net, third@example.net"
+    }, {
+        input: [{ id: "mailto:first@example.net", cname: "first example" },
+                { id: "mailto:second@example.net", cname: "second example" },
+                { id: "mailto:third@example.net", cname: "third example" }],
+        expected: "first example <first@example.net>, second example <second@example.net>, " +
+                  "third example <third@example.net>"
+    }, {
+        input: [{ id: "mailto:first@example.net", cname: "example, first" },
+                { id: "mailto:second@example.net", cname: "example, second" },
+                { id: "mailto:third@example.net", cname: "example, third" }],
+        expected: "\"example, first\" <first@example.net>, \"example, second\" <second@example.net>, " +
+                  "\"example, third\" <third@example.net>"
+    }, {
+        input: [{ id: "mailto:first@example.net", cname: null },
+                { id: "urn:uuid:second.example.net", cname: null },
+                { id: "mailto:third@example.net", cname: null }],
+        expected: "first@example.net, third@example.net"
+    }, {
+        input: [{ id: "mailto:first@example.net", cname: "first" },
+                { id: "urn:uuid:second.example.net", cname: "second" },
+                { id: "mailto:third@example.net", cname: "third" }],
+        expected: "first <first@example.net>, third <third@example.net>"
+    }];
+
+    let i = 0;
+    for (let test of data) {
+        i++;
+        let attendees = [];
+        for (let att of test.input) {
+            let attendee = cal.createAttendee();
+            attendee.id = att.id;
+            if (att.cname) {
+                attendee.commonName = att.cname;
+            }
+            attendees.push(attendee);
+        }
+        equal(cal.email.createRecipientList(attendees), test.expected, "(test #" + i + ")");
+    }
+}
+
+function test_validateRecipientList() {
+    let data = [{
+        input: "first.last@example.net",
+        expected: "first.last@example.net"
+    }, {
+        input: "first last <first.last@example.net>",
+        expected: "first last <first.last@example.net>"
+    }, {
+        input: "\"last, first\" <first.last@example.net>",
+        expected: "\"last, first\" <first.last@example.net>"
+    }, {
+        input: "last, first <first.last@example.net>",
+        expected: "\"last, first\" <first.last@example.net>"
+    }, {
+        input: "\"last; first\" <first.last@example.net>",
+        expected: "\"last; first\" <first.last@example.net>"
+    }, {
+        input: "first1.last1@example.net,first2.last2@example.net,first3.last2@example.net",
+        expected: "first1.last1@example.net, first2.last2@example.net, first3.last2@example.net"
+    }, {
+        input: "first1.last1@example.net, first2.last2@example.net, first3.last2@example.net",
+        expected: "first1.last1@example.net, first2.last2@example.net, first3.last2@example.net"
+    }, {
+        input: "first1.last1@example.net, first2 last2 <first2.last2@example.net>, \"last3, first" +
+               "3\" <first3.last2@example.net>",
+        expected: "first1.last1@example.net, first2 last2 <first2.last2@example.net>, \"last3, fi" +
+               "rst3\" <first3.last2@example.net>"
+    }, {
+        input: "first1.last1@example.net, last2; first2 <first2.last2@example.net>, \"last3; first" +
+               "3\" <first3.last2@example.net>",
+        expected: "first1.last1@example.net, \"last2; first2\" <first2.last2@example.net>, \"last" +
+               "3; first3\" <first3.last2@example.net>"
+    }, {
+        input: "first1 last2 <first1.last1@example.net>, last2, first2 <first2.last2@example.net>" +
+               ", \"last3, first3\" <first3.last2@example.net>",
+        expected: "first1 last2 <first1.last1@example.net>, \"last2, first2\" <first2.last2@examp" +
+                  "le.net>, \"last3, first3\" <first3.last2@example.net>"
+    }];
+
+    for (let [i, test] of Object.entries(data)) {
+        equal(cal.email.validateRecipientList(test.input), test.expected,
+              "(test #" + i + ")");
+    }
+}
+
+function test_attendeeMatchesAddresses() {
+    let a = cal.createAttendee("ATTENDEE:mailto:horst");
+    ok(cal.email.attendeeMatchesAddresses(a, ["HORST", "peter"]));
+    ok(!cal.email.attendeeMatchesAddresses(a, ["HORSTpeter", "peter"]));
+    ok(!cal.email.attendeeMatchesAddresses(a, ["peter"]));
+
+    a = cal.createAttendee("ATTENDEE;EMAIL=\"horst\":urn:uuid:horst");
+    ok(cal.email.attendeeMatchesAddresses(a, ["HORST", "peter"]));
+    ok(!cal.email.attendeeMatchesAddresses(a, ["HORSTpeter", "peter"]));
+    ok(!cal.email.attendeeMatchesAddresses(a, ["peter"]));
+}
diff --git a/calendar/test/unit/test_calitiputils.js b/calendar/test/unit/test_itip_utils.js
rename from calendar/test/unit/test_calitiputils.js
rename to calendar/test/unit/test_itip_utils.js
--- a/calendar/test/unit/test_calitiputils.js
+++ b/calendar/test/unit/test_itip_utils.js
@@ -3,22 +3,24 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 ChromeUtils.import("resource://calendar/modules/calUtils.jsm");
 ChromeUtils.import("resource://testing-common/mailnews/mailTestUtils.js");
 
 // tests for calItipUtils.jsm
 
 function run_test() {
-    getMessageSender_test();
-    getSequence_test();
-    getStamp_test();
-    compareSequence_test();
-    compareStamp_test();
-    compare_test();
+    test_getMessageSender();
+    test_getSequence();
+    test_getStamp();
+    test_compareSequence();
+    test_compareStamp();
+    test_compare();
+    test_getAttendeesBySender();
+    test_resolveDelegation();
 }
 
 /*
  * Helper function to get an ics for testing sequence and stamp comparison
  *
  * @param {String} aAttendee              A serialized ATTENDEE property
  * @param {String} aSequence              A serialized SEQUENCE property
  * @param {String} aDtStamp               A serialized DTSTAMP property
@@ -132,34 +134,34 @@ function getSeqStampTestItems(aTest) {
                 att.setProperty("RECEIVED-DTSTAMP", input.attendee.receivedStamp);
             }
             items.push(att);
         }
     }
     return items;
 }
 
-function getMessageSender_test() {
+function test_getMessageSender() {
     let data = [{
         input: null,
         expected: null
     }, {
         input: { },
         expected: null
     }, {
         input: { author: "Sender 1 <sender1@example.net>" },
         expected: "sender1@example.net"
     }];
     for (let i = 1; i <= data.length; i++) {
         let test = data[i - 1];
         equal(cal.itip.getMessageSender(test.input), test.expected, "(test #" + i + ")");
     }
 }
 
-function getSequence_test() {
+function test_getSequence() {
     // assigning an empty string results in not having the property in the ics here
     let data = [{
         input: [{ item: { sequence: "", xMozReceivedSeq: "" } }],
         expected: 0
     }, {
         input: [{ item: { sequence: "0", xMozReceivedSeq: "" } }],
         expected: 0
     }, {
@@ -183,17 +185,17 @@ function getSequence_test() {
     }];
     for (let i = 1; i <= data.length; i++) {
         let test = data[i - 1];
         testItems = getSeqStampTestItems(test);
         equal(cal.itip.getSequence(testItems[0], testItems[1]), test.expected, "(test #" + i + ")");
     }
 }
 
-function getStamp_test() {
+function test_getStamp() {
     // assigning an empty string results in not having the property in the ics here. However, there
     // must be always an dtStamp for item - if it's missing it will be set by the test code to make
     // sure we get a valid ics
     let data = [{
         // !dtStamp && !xMozReceivedStamp => test default value
         input: [{ item: { dtStamp: "", xMozReceivedStamp: "" } }],
         expected: "20150909T181048Z"
     }, {
@@ -216,19 +218,19 @@ function getStamp_test() {
         let result = cal.itip.getStamp(getSeqStampTestItems(test)[0]);
         if (result) {
             result = result.icalString;
         }
         equal(result, test.expected, "(test #" + i + ")");
     }
 }
 
-function compareSequence_test() {
+function test_compareSequence() {
     // it is sufficient to test here with sequence for items - full test coverage for
-    // x-moz-received-sequence is already provided by compareSequence_test
+    // x-moz-received-sequence is already provided by test_compareSequence
     let data = [{
         // item1.seq == item2.seq
         input: [{ item: { sequence: "2" } },
                 { item: { sequence: "2" } }],
         expected: 0
     }, {
         // item1.seq > item2.seq
         input: [{ item: { sequence: "3" } },
@@ -275,19 +277,19 @@ function compareSequence_test() {
         testItems = getSeqStampTestItems(test);
         equal(cal.itip.compareSequence(testItems[0], testItems[1]),
               test.expected,
               "(test #" + i + ")"
         );
     }
 }
 
-function compareStamp_test() {
+function test_compareStamp() {
     // it is sufficient to test here with dtstamp for items - full test coverage for
-    // x-moz-received-stamp is already provided by compareStamp_test
+    // x-moz-received-stamp is already provided by test_compareStamp
     let data = [{
         // item1.stamp == item2.stamp
         input: [{ item: { dtStamp: "20150910T181048Z" } },
                 { item: { dtStamp: "20150910T181048Z" } }],
         expected: 0
     }, {
         // item1.stamp > item2.stamp
         input: [{ item: { dtStamp: "20150911T181048Z" } },
@@ -334,19 +336,19 @@ function compareStamp_test() {
         testItems = getSeqStampTestItems(test);
         equal(cal.itip.compareStamp(testItems[0], testItems[1]),
               test.expected,
               "(test #" + i + ")"
         );
     }
 }
 
-function compare_test() {
+function test_compare() {
     // it is sufficient to test here with items only - full test coverage for attendees or
-    // item/attendee is already provided by compareSequence_test and compareStamp_test
+    // item/attendee is already provided by test_compareSequence and test_compareStamp
     let data = [{
         // item1.seq == item2.seq && item1.stamp == item2.stamp
         input: [{ item: { sequence: "2", dtStamp: "20150910T181048Z" } },
                 { item: { sequence: "2", dtStamp: "20150910T181048Z" } }],
         expected: 0
     }, {
         // item1.seq == item2.seq && item1.stamp > item2.stamp
         input: [{ item: { sequence: "2", dtStamp: "20150911T181048Z" } },
@@ -392,8 +394,182 @@ function compare_test() {
         let test = data[i - 1];
         testItems = getSeqStampTestItems(test);
         equal(cal.itip.compare(testItems[0], testItems[1]),
               test.expected,
               "(test #" + i + ")"
         );
     }
 }
+
+function test_getAttendeesBySender() {
+    let data = [{
+        input: {
+            attendees: [{ id: "mailto:user1@example.net", sentBy: null },
+                        { id: "mailto:user2@example.net", sentBy: null }],
+            sender: "user1@example.net"
+        },
+        expected: ["mailto:user1@example.net"]
+    }, {
+        input: {
+            attendees: [{ id: "mailto:user1@example.net", sentBy: null },
+                        { id: "mailto:user2@example.net", sentBy: null }],
+            sender: "user3@example.net"
+        },
+        expected: []
+    }, {
+        input: {
+            attendees: [{ id: "mailto:user1@example.net", sentBy: "mailto:user3@example.net" },
+                        { id: "mailto:user2@example.net", sentBy: null }],
+            sender: "user3@example.net"
+        },
+        expected: ["mailto:user1@example.net"]
+    }, {
+        input: {
+            attendees: [{ id: "mailto:user1@example.net", sentBy: null },
+                        { id: "mailto:user2@example.net", sentBy: "mailto:user1@example.net" }],
+            sender: "user1@example.net"
+        },
+        expected: ["mailto:user1@example.net", "mailto:user2@example.net"]
+    }, {
+        input: { attendees: [], sender: "user1@example.net" },
+        expected: []
+    }, {
+        input: {
+            attendees: [{ id: "mailto:user1@example.net", sentBy: null },
+                        { id: "mailto:user2@example.net", sentBy: null }],
+            sender: ""
+        },
+        expected: []
+    }, {
+        input: {
+            attendees: [{ id: "mailto:user1@example.net", sentBy: null },
+                        { id: "mailto:user2@example.net", sentBy: null }],
+            sender: null
+        },
+        expected: []
+    }];
+
+    for (let i = 1; i <= data.length; i++) {
+        let test = data[i - 1];
+        let attendees = [];
+        for (let att of test.input.attendees) {
+            let attendee = cal.createAttendee();
+            attendee.id = att.id;
+            if (att.sentBy) {
+                attendee.setProperty("SENT-BY", att.sentBy);
+            }
+            attendees.push(attendee);
+        }
+        let detected = [];
+        cal.itip.getAttendeesBySender(attendees, test.input.sender).forEach(att => {
+            detected.push(att.id);
+        });
+        ok(detected.every(aId => test.expected.includes(aId)), "(test #" + i + " ok1)");
+        ok(test.expected.every(aId => detected.includes(aId)), "(test #" + i + " ok2)");
+    }
+}
+
+function test_resolveDelegation() {
+    let data = [{
+        input: {
+            attendee:
+                'ATTENDEE;DELEGATED-FROM="mailto:attendee2@example.net";CN="Attendee 1":mailto:at' +
+                "tendee1@example.net",
+            attendees: [
+                'ATTENDEE;DELEGATED-FROM="mailto:attendee2@example.net";CN="Attendee 1":mailto:at' +
+                "tendee1@example.net",
+                'ATTENDEE;DELEGATED-TO="mailto:attendee1@example.net";CN="Attendee 2":mailto:atte' +
+                "ndee2@example.net"
+            ]
+        },
+        expected: {
+            delegatees: "",
+            delegators: "Attendee 2 <attendee2@example.net>"
+        }
+    }, {
+        input: {
+            attendee:
+                'ATTENDEE;DELEGATED-FROM="mailto:attendee2@example.net":mailto:attendee1@example.net',
+            attendees: [
+                'ATTENDEE;DELEGATED-FROM="mailto:attendee2@example.net":mailto:attendee1@example.net',
+                'ATTENDEE;DELEGATED-TO="mailto:attendee1@example.net":mailto:attendee2@example.net'
+            ]
+        },
+        expected: {
+            delegatees: "",
+            delegators: "attendee2@example.net"
+        }
+    }, {
+        input: {
+            attendee:
+                'ATTENDEE;DELEGATED-TO="mailto:attendee2@example.net";CN="Attendee 1":mailto:atte' +
+                "ndee1@example.net",
+            attendees: [
+                'ATTENDEE;DELEGATED-TO="mailto:attendee2@example.net";CN="Attendee 1":mailto:atte' +
+                "ndee1@example.net",
+                'ATTENDEE;DELEGATED-FROM="mailto:attendee1@example.net";CN="Attendee 2":mailto:at' +
+                "tendee2@example.net"
+            ]
+        },
+        expected: {
+            delegatees: "Attendee 2 <attendee2@example.net>",
+            delegators: ""
+        }
+    }, {
+        input: {
+            attendee:
+                'ATTENDEE;DELEGATED-TO="mailto:attendee2@example.net":mailto:attendee1@example.net',
+            attendees: [
+                'ATTENDEE;DELEGATED-TO="mailto:attendee2@example.net":mailto:attendee1@example.net',
+                'ATTENDEE;DELEGATED-FROM="mailto:attendee1@example.net":mailto:attendee2@example.net'
+            ]
+        },
+        expected: {
+            delegatees: "attendee2@example.net",
+            delegators: ""
+        }
+    }, {
+        input: {
+            attendee:
+                "ATTENDEE:mailto:attendee1@example.net",
+            attendees: [
+                "ATTENDEE:mailto:attendee1@example.net",
+                "ATTENDEE:mailto:attendee2@example.net"
+            ]
+        },
+        expected: {
+            delegatees: "",
+            delegators: ""
+        }
+    }, {
+        input: {
+            attendee:
+                'ATTENDEE;DELEGATED-FROM="mailto:attendee2@example.net";DELEGATED-TO="mailto:atte' +
+                'ndee3@example.net":mailto:attendee1@example.net',
+            attendees: [
+                'ATTENDEE;DELEGATED-FROM="mailto:attendee2@example.net";DELEGATED-TO="mailto:atte' +
+                'ndee3@example.net":mailto:attendee1@example.net',
+                'ATTENDEE;DELEGATED-TO="mailto:attendee1@example.net":mailto:attendee2@example.net',
+                'ATTENDEE;DELEGATED-FROM="mailto:attendee1@example.net":mailto:attendee3@example.net'
+            ]
+        },
+        expected: {
+            delegatees: "attendee3@example.net",
+            delegators: "attendee2@example.net"
+        }
+    }];
+    let i = 0;
+    for (let test of data) {
+        i++;
+        let attendees = [];
+        for (let att of test.input.attendees) {
+            let attendee = cal.createAttendee();
+            attendee.icalString = att;
+            attendees.push(attendee);
+        }
+        let attendee = cal.createAttendee();
+        attendee.icalString = test.input.attendee;
+        let result = cal.itip.resolveDelegation(attendee, attendees);
+        equal(result.delegatees, test.expected.delegatees, "(test #" + i + " - delegatees)");
+        equal(result.delegators, test.expected.delegators, "(test #" + i + " - delegators)");
+    }
+}
diff --git a/calendar/test/unit/test_utils.js b/calendar/test/unit/test_utils.js
--- a/calendar/test/unit/test_utils.js
+++ b/calendar/test/unit/test_utils.js
@@ -4,17 +4,16 @@
 
 function run_test() {
     do_calendar_startup(really_run_test);
 }
 
 function really_run_test() {
     test_recentzones();
     test_formatcss();
-    test_attendeeMatchesAddresses();
     test_getDefaultStartDate();
     test_getStartEndProps();
     test_OperationGroup();
     test_sameDay();
     test_binarySearch();
 }
 
 function test_recentzones() {
@@ -53,28 +52,16 @@ function test_recentzones() {
 }
 
 function test_formatcss() {
     equal(cal.view.formatStringForCSSRule(" "), "_");
     equal(cal.view.formatStringForCSSRule("ü"), "-uxfc-");
     equal(cal.view.formatStringForCSSRule("a"), "a");
 }
 
-function test_attendeeMatchesAddresses() {
-    let a = cal.createAttendee("ATTENDEE:mailto:horst");
-    ok(cal.email.attendeeMatchesAddresses(a, ["HORST", "peter"]));
-    ok(!cal.email.attendeeMatchesAddresses(a, ["HORSTpeter", "peter"]));
-    ok(!cal.email.attendeeMatchesAddresses(a, ["peter"]));
-
-    a = cal.createAttendee("ATTENDEE;EMAIL=\"horst\":urn:uuid:horst");
-    ok(cal.email.attendeeMatchesAddresses(a, ["HORST", "peter"]));
-    ok(!cal.email.attendeeMatchesAddresses(a, ["HORSTpeter", "peter"]));
-    ok(!cal.email.attendeeMatchesAddresses(a, ["peter"]));
-}
-
 function test_getDefaultStartDate() {
     function transform(nowString, refDateString) {
         now = cal.createDateTime(nowString);
         let refDate = refDateString ? cal.createDateTime(refDateString) : null;
         return cal.dtz.getDefaultStartDate(refDate);
     }
 
     let oldNow = cal.dtz.now;
diff --git a/calendar/test/unit/xpcshell-shared.ini b/calendar/test/unit/xpcshell-shared.ini
--- a/calendar/test/unit/xpcshell-shared.ini
+++ b/calendar/test/unit/xpcshell-shared.ini
@@ -13,25 +13,25 @@
 [test_bug485571.js]
 [test_bug486186.js]
 [test_bug494140.js]
 [test_bug523860.js]
 [test_bug653924.js]
 [test_bug668222.js]
 [test_bug759324.js]
 [test_calmgr.js]
-[test_calutils.js]
-[test_calitiputils.js]
+[test_itip_utils.js]
 [test_data_bags.js]
 [test_datetime.js]
 [test_datetime_before_1970.js]
 [test_datetimeformatter.js]
 [test_deleted_items.js]
 [test_duration.js]
 [test_extract.js]
+[test_email_utils.js]
 [test_freebusy.js]
 [test_freebusy_service.js]
 #[test_gdata_provider.js]
 #requesttimeoutfactor = 2
 [test_hashedarray.js]
 [test_ics.js]
 [test_ics_parser.js]
 [test_ics_service.js]
