# HG changeset patch
# User Frank-Rainer Grahl <frgrahl@gmx.net>
# Date 1630448597 -7200
# Parent  3f7edf5122004b58579ffd2a7f8d73ea83e63d9e
Bug 1728911 - Bring clear private data section in help up to spec. r=IanN a=IanN

diff --git a/suite/locales/en-US/chrome/common/help/suite-toc.rdf b/suite/locales/en-US/chrome/common/help/suite-toc.rdf
--- a/suite/locales/en-US/chrome/common/help/suite-toc.rdf
+++ b/suite/locales/en-US/chrome/common/help/suite-toc.rdf
@@ -915,17 +915,17 @@
     </nc:subheadings>
   </rdf:Description>
 
 <rdf:Description about="#using-priv-help-private-data">
     <nc:subheadings>
       <rdf:Seq>
         <rdf:li><rdf:Description ID="types_of_private_data" nc:name="Types of Private Data" nc:link="using_priv_help.xhtml#types_of_private_data"/> </rdf:li>
         <rdf:li><rdf:Description ID="privatedata_prefs" nc:name="Private Data Preferences" nc:link="using_priv_help.xhtml#private_data_prefs"/> </rdf:li>
-        <rdf:li><rdf:Description ID="clear_private_data_now" nc:name="Clear Private Data Now" nc:link="using_priv_help.xhtml#clear_private_data_now"/> </rdf:li>
+        <rdf:li><rdf:Description ID="clear_private_data_now" nc:name="Clear Private Data Dialog" nc:link="using_priv_help.xhtml#clear_private_data_now"/> </rdf:li>
       </rdf:Seq>
     </nc:subheadings>
   </rdf:Description>
 
 <rdf:Description about="#private-browsing">
     <nc:subheadings>
       <rdf:Seq>
         <rdf:li><rdf:Description ID="opening_a_private_window" nc:name="Opening a Private Window" nc:link="using_priv_help.xhtml#opening_a_private_window"/> </rdf:li>
diff --git a/suite/locales/en-US/chrome/common/help/using_priv_help.xhtml b/suite/locales/en-US/chrome/common/help/using_priv_help.xhtml
--- a/suite/locales/en-US/chrome/common/help/using_priv_help.xhtml
+++ b/suite/locales/en-US/chrome/common/help/using_priv_help.xhtml
@@ -584,17 +584,17 @@
   may be gathered and stored by &brandShortName;. This section describes the
   types of such private data and options to remove them either manually by
   request or automatically when shutting down &brandShortName;.</p>
 
 <div class="contentsBox">In this section:
   <ul>
     <li><a href="#types_of_private_data">Types of Private Data</a></li>
     <li><a href="#private_data_prefs">Private Data Preferences</a></li>
-    <li><a href="#clear_private_data_now">Clear Private Data Now</a></li>
+    <li><a href="#clear_private_data_now">Clear Private Data Dialog</a></li>
   </ul>
 </div>
 
 <h2 id="types_of_private_data">Types of Private Data</h2>
 
 <p>Several types of information are gathered and kept by &brandShortName; while
   you are browsing websites. Some of these data are necessary for those sites to
   function properly or more efficiently, others are for your convenience.</p>
@@ -685,16 +685,22 @@
     <tr>
       <td>Authenticated Sessions</td>
       <td>Websites may require <em>authentication</em> (username and password,
         asked for with a pop-up dialog) and can keep track of such by
         authenticated sessions. A site will ask you for your credentials
         again when you proceed to the next page after this information is
         cleared.</td>
     </tr>
+    <tr>
+      <td>Site Preferences</td>
+      <td>Site preferences are locally stored preferences for the specific
+        website. These usually are the individual zoom level and the last
+        location of a downloaded file from this site on your local disk.</td>
+    </tr>
   </tbody>
 </table>
 
 <p>[<a href="#clearing_private_data">Return to beginning of section</a>]</p>
 
 <h2 id="private_data_prefs">Privacy &amp; Security Preferences
   - Private Data</h2>
 
@@ -709,84 +715,73 @@
     subcategories are visible, double-click Privacy &amp; Security to expand
     the list.)</li>
 </ol>
 
 <p>The <strong>Clear Private Data</strong> section provides the following
   options:</p>
 
 <ul>
+  <li><strong>Clear Now</strong>: Click this button to initiate clearing of
+    private data. The Clear Private Data dialog will open where you can revisit
+    your choices.
+  </li>
   <li><strong>Always clear my private data when I close
     &brandShortName;</strong>: Check this option to always initiate clearing
     of the selected private data when the application is shut down.</li>
-  <li><strong>Ask me before clearing private data</strong>: Check this option
-    for &brandShortName; to prompt you with a dialog box before clearing the
-    selected private data. The dialog box also allows you to change the
-    selection, rather than clearing them without any interaction.</li>
-  <li><strong>When I ask &brandShortName; to clear my private data, it should
-    erase</strong>: For each <a href="#types_of_private_data">type of private
-    data</a>, a separate option is provided whether or not to clear any
-    stored items of this specific type. Check the respective box for each
-    item to be deleted by default when clearing is requested by you or
-    initiated on shutdown.</li>
-  <li><strong>Clear Now</strong>: Click this button to initiate clearing of
-    private data immediately, depending on the preferences selected in
-    this panel:
-    <ul>
-      <li>When the <q>Ask me before clearing private data</q> option is checked,
-        &brandShortName; opens a dialog box where you can confirm and change
-        any items to be cleared.</li>
-      <li>When <q>Ask me before clearing private data</q> is <em>not</em>
-        checked, all types selected to be cleared by default will be deleted
-        immediately without the dialog box being shown.</li>
-    </ul>
-  </li>
+  <li><strong>Clear these private data items</strong>: For each
+    <a href="#types_of_private_data">type of private data</a>, a separate
+    option is provided whether or not to clear any stored items of this
+    specific type. Check the respective box for each item to be deleted. The
+    choices are split between manually clearing via dialog and automatically
+    initiated when closing &brandShortName;.</li>
 </ul>
 
 <!-- link up "Data Manager" below once bug 599097 has been taken care of -->
 
 <p><strong>Note</strong>: Also consider more selective alternatives to delete
   private data. For example, the individual preference panels for each type may
   provide additional options, and the Data Manager allows to clear private data
   by type and the specific domain of a website. Rather than clearing all cookies
   when shutting down &brandShortName;, you could specify to allow cookies for
   sessions only, thus giving you the opportunity to establish exceptions for
   selected websites for which you want to retain cookies.</p>
 
 <p>[<a href="#clearing_private_data">Return to beginning of section</a>]</p>
 
-<h2 id="clear_private_data_now">Clear Private Data Now</h2>
+<h2 id="clear_private_data_now">Clear Private Data Dialog</h2>
 
 <p>Private data can be cleared at any time, either from the
   <a href="#private_data_prefs">Private Data preferences</a> by clicking
   the Clear Now button, or by selecting Clear Private Data from the Tools
-  menu of a browser window. This initiates one of the following actions:</p>
+  menu of a browser window.</p>
 
-<ul>
-  <li>When the <q>Ask me before clearing private data</q> option is checked
-    in the <a href="#private_data_prefs">Private Data preference panel</a>,
-    &brandShortName; opens a dialog window where you can confirm and change
-    the <a href="#types_of_private_data">types of private data</a> to be
-    cleared as follows:
+  <p>In the dialog window you can confirm and change the
+    <a href="#types_of_private_data">types of private data</a> to be cleared
+    as follows:
     <ul>
+      <li>Select the time range to clear if you don't want to clear all the
+      data. Only the following items honor this setting:
+        <ul>
+          <li>Browsing History</li>
+          <li>Download History</li>
+          <li>Cookies</li>
+          <li>Saved Form and Search History</li>
+          <li>Site Preferences.</li>
+        </ul>
+        All other items will always be fully cleared!</li>
       <li>The defaults for the individual types are determined by the
-        <a href="#private_data_prefs">Private Data preferences</a>.
-        Note that individual boxes may be disabled if no items of that
-        type are available for deletion.</li>
+        <a href="#private_data_prefs">Private Data preferences</a> in the
+        Clear Manually column.</li>
       <li>Check or uncheck boxes as desired if you want to clear a different
         set of private data.</li>
-      <li>Click <q>Clear Private Data Now</q> to clear the selected items,
+      <li>Click <q>Clear Now</q> to clear the selected items,
         or Cancel to quit the dialog.</li>
     </ul>
-  </li>
-  <li>When <q>Ask me before clearing private data</q> is <em>not</em> checked,
-    all types selected in the <a href="#private_data_prefs">Private Data
-    preferences</a> to be cleared by default will be deleted immediately
-    without the dialog box being shown.</li>
-</ul>
+  </p>
 
 <p>[<a href="#clearing_private_data">Return to beginning of section</a>]</p>
 
 <h1 id="browsing_in_a_private_window">Browsing in a Private Window</h1>
 
 <p>There may be occasions where you don&apos;t want &brandShortName; to keep
   track of your browsing activities. For example, when someone else quickly
   wants to use your computer and you don&apos;t want your current browsing
